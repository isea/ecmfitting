<div align=right>
<a href="https://www.carl.rwth-aachen.de/?lidx=1" target="_blank">
    <img src="src/documentation/GUI_images/CARL_ISEA_RWTH.svg" alt="CARL Logo"  width="500"/>
</a>
</div>


# Autofit

This GUI has evolved from the described FittingGUI. It includes the possibility to import, fit and export impedance models with automated algorithms. It includes changes to simplify the usage for new users by deleting functionalities which were not used in practice. Further changes were made to boost the efficiency of the implementations.

# FittingGUI

GUI for Fitting Models to measured Impedance Spectra with focus on Lithium Ion Batteries

This graphical interface for Matlab has been used at ISEA for
the evaluation of impedance spectra and time domain characterization measurements for many years.
It includes a comprehensive model database, which allows you to easily integrate
your own impedance model structures. For the individual elements, both formulations
in the frequency domain for the impedance fitting as well as implementations in the
time domain are stored. The latter are required for the fitting of the time
domain large signal behavior. The unique feature of this tool is the possibility
to parameterize a physical Newman model separately for the anode and the cathode.
Separation takes place using the DRT (distribution of relaxation times).
The individual model elements, the physical model, the time domain implementations
and the procedure for the DRT process fitting are described in detail in the dissertation
of Heiko Witzenhausen and are further developed by Jonas Rinner and Stephan Bihn.

# Overview

A brief overview of how the original FittingGUI looked like is presented below

[![Teaser](https://img.youtube.com/vi/918e_LqCn5g/0.jpg)](https://www.youtube.com/watch?v=918e_LqCn5g "Teaser FittingGUI")

## How To Start
After running the "AUTOFIT.mlapp", the GUI will start up and the Landing Page is shown.



[<img src="src/documentation/Overview/LandingPage.png" width="1000"/>](LandingPage)


## Main Page (Global Fit)
The left side of the GUI states the main parameters of the investigated battery. It stays over all different sub-applications. The main page on the right side states the GlobalFit sub-application. Here all the parameters for the global fitting approach can be set.

[<img src="src/documentation/Overview/overview.png" width="1000"/>](Overview)

## Frequency Domain Fitting
The Frequency Domain Fitting tab can be used for the hands-on fitting going through ever SOC and every temperature step-by-step. In addition it is useful to look at the Nyquist plot or to get an insight into the DRT of the EIS measurements.

[<img src="src/documentation/Overview/Plot_DRT_1.png" width="600"/>](FrequencyDomainFitting)

## Time Domain Fit
The Time Domain Fitting tab can be used to see the results of the global time domain fit or to fit the time domain part by hand. it can be toggled between relaxation measurement and pulse mesurement, but if you want to use the pulse data make sure you have currentless parts after the pulse. 

Using the relaxation phase, it can look like this:

[<img src="src/documentation/Overview/TDF_relax.png" width="600"/>](TimeDomainFitRelax)

## Fitting results
The FittingResult tab allows to see an overview of the fitting results over several states (Temperature, SOC)

[<img src="src/documentation/Overview/FittingResults.png" width="600"/>](FittingResults)

## File Read
The File Read tab offers the possibility to read complete testset if the data has the right structure and notation. The different phases like capacity test, EIS measurement and pulse measurement can be assigned to the procedures of the data. 

[<img src="src/documentation/Overview/FileRead.png" width="600"/>](FileRead)

# User Manual

See this [documentation](src/documentation/documentation.md) for usage of the GUI.

# Fitting Algorithm

The GUI relies on the fitting algorithm easyFit published by Jean-Luc Dellis
See also: https://de.mathworks.com/matlabcentral/fileexchange/10625-easyfit-x-y-varargin

# Featured Projects

Parts of the tool are used in the following projects:
1. Toolbox Speichersysteme, Energy Storage Toolbox
  * Grant No. 64.65.69-EM-2011A

2. Offenes Batterie-Alterungs-Tool zur Lebensdauerprognose von Batteriepacks unter Berücksichtigung von Unsicherheiten und Streuung von Zellqualität,
  * Grant No. 0325905
  
3. SimBAS - Simulation von Batteriezellen und Anwendungen in Speichersystemen, Simulation of battery cells and applications in storage systems
  * Grant No. 03XP0338B

4. greenBattNutzung  - Begleitprojekt Batterielebenszyklus, Supporting project battery life cycle
  * Grant No. 03XP0303C
  
5. METABatt - Automatisierte und Reproduzierbare Charakterisierung und Modellbildung des Elektrischen, Thermischen und Alterungsverhaltens von Batterien, Automated and Reproducible Characterisation and Modelling of the Electrical, Thermal and Ageing Behaviour of Batteries
  * Grant No. 16BZF315B
  
# Required Software

+ MATLAB

# Authors

Please acknowledge the [authors](src/documentation/DevelopmentTeam.md).

# License

The ISEA-ECMFitting is published under the MIT License.
For commercial use, a different license is available.
Further information can be requested at batteries@isea.rwth-aachen.de .

More information can be found [here](src/documentation/LICENSE.TXT).
