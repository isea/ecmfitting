% % MIT License
% % 
% % Copyright (c) 2016 ISEA, Heiko Witzenhausen
% % 
% % Permission is hereby granted, free of charge, to any person obtaining a copy
% % of this software and associated documentation files (the "Software"), to deal
% % in the Software without restriction, including without limitation the rights
% % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% % copies of the Software, and to permit persons to whom the Software is
% % furnished to do so, subject to the following conditions:
% % 
% % The above copyright notice and this permission notice shall be included in all
% % copies or substantial portions of the Software. The Text "Developed by ISEA @ RWTH Aachen" 
% % may not be removed from the graphical user interface.
% % 
% % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% % SOFTWARE.
% % 
function config()
old_path = cd;
cd([old_path , '\' , 'FittingGUIFunctions\FileReadFunctions'])
global Pattern
temp = readtable('MasterProgram.xlsx');
for n = 1:length(temp.MasterProgram)
    Pattern.MasterProgram{n} = temp.MasterProgram{n,1};
end
temp = readtable('Temperatur.xlsx');
for n = 1:length(temp.Temperatur)
    Pattern.Temperatur{n} = temp.Temperatur{n,1};
end
temp = readtable('SOC.xlsx');
for n = 1:length(temp.SOC)
    Pattern.SOC{n} = temp.SOC{n,1};
end
temp = readtable('Zustand.xlsx');
for n = 1:length(temp.Zustand)
    Pattern.Zustand{n} = temp.Zustand{n,1};
end
temp = readtable('Batterie.xlsx');
for n = 1:length(temp.Batterie)
    Pattern.Batterie{n} = temp.Batterie{n,1};
end
cd(old_path);
rmpath(genpath('FittingGUIFunctions/SimpleFit/SimpleFitFunctions'))
global Modellliste
addpath('ersatzschaltbilder')
addpath('ESBElemente')
addpath('utils')
folders = dir('utils');
for folder = folders'
    if folder.isdir && ~strcmp(folder.name,'.') && ~strcmp(folder.name,'..')
        addpath(['utils/' folder.name])
    end
end
addpath('utils/peakfinder')
Modellliste.Modell = {'DRT_Komp','MF*R0 + HF*1i.*w*L0 + LF*1./(1i.*w.*C0)','R0,L0,C0',{0 1e-6 1e4},{0 0 0},{inf inf inf},{0 0 0} ,{};};                                         
global DRT_Config 
DRT_Config.Schwingfaktor = 1;
DRT_Config.InterpolationsFaktor = 10;
DRT_Config.FilterFaktor_ext = 0.3;
DRT_Config.ZeroPadding = 20;
DRT_Config.PeakSensitivitaet = 0.3;
DRT_Config.Prozesse = 3;
DRT_Config.ZarcHN=1;
files = dir('ESBElemente/ESBe*.m');
for i = 1:numel(files)
    eval(files(i).name(1:end-2));
end
files = dir('ersatzschaltbilder/ESB*.m');
for i = 1:numel(files)
    Modellliste.Modell = [Modellliste.Modell ; eval([files(i).name(1:end-2) '(Modellliste.Implementierung)'])];
    if strcmp(Modellliste.Modell{i+1,1},'R2RC')
        Modellliste.standard_modell = i+1;
    end
end

addpath(genpath('FittingGUIFunctions/SimpleFit/SimpleFitFunctions'))

end
