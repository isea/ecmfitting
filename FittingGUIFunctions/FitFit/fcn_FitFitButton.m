function fcn_FitFitButton(app,event)
global FitFitData;
Par=cell2struct(FitFitData.ParameterTable ,{'ParameterFix' 'ParameterNamen' 'Functions' 'Results'},2);
useindex = find(cell2mat(FitFitData.SOCTable(:,1))==1);
if isempty(useindex)
    error('Bitte w�hlen Sie mindestens einen Ladezustand zum Fitten aus.')
end
SOC_vektor = FitFitData.SOCs(useindex)';
Par_orig_vektor = zeros(numel(SOC_vektor),numel(Par));
FitFitData.linear.indices = [];
FitFitData.linear.koeffizient_init=[];
FitFitData.linear.pinit=[];
FitFitData.linear.pmin=[];
FitFitData.linear.pmax=[];
FitFitData.constant=FitFitData.linear;
FitFitData.square=FitFitData.linear;
FitFitData.cubic=FitFitData.linear;
FitFitData.exponential=FitFitData.linear;
FitFitData.individual=FitFitData.linear;
for i = 1:numel(Par)
    for j = 1:numel(useindex)
        Par_orig_vektor(j,i) = FitFitData.Model{1,useindex(j)}.Fit.Parameter(i);
    end
    if ~FitFitData.ParameterTable{i,1}
        if strcmpi('linear',FitFitData.ParameterTable{i,3})
            FitFitData.linear.function='p(1).*w+p(2)';
            b = ([SOC_vektor' ones(numel(SOC_vektor),1) ]\Par_orig_vektor(:,i));
            FitFitData.linear.indices = [ FitFitData.linear.indices i];
            FitFitData.linear.koeffizient_init = [FitFitData.linear.koeffizient_init b];
            FitFitData.linear.pinit=[FitFitData.linear.pinit b];
            FitFitData.linear.pmin=[FitFitData.linear.pmin b'-0.3*b'  ];
            FitFitData.linear.pmax=[FitFitData.linear.pmax b'+0.3*b' ];
        elseif strcmpi('constant',FitFitData.ParameterTable{i,3})
            FitFitData.constant.function='p(1)';
            b = (ones(numel(SOC_vektor),1)\Par_orig_vektor(:,i));
            FitFitData.constant.indices = [ FitFitData.constant.indices i];
            FitFitData.constant.koeffizient_init = [FitFitData.constant.koeffizient_init b];
            FitFitData.constant.pinit=[FitFitData.constant.pinit b];
            FitFitData.constant.pmin=[FitFitData.constant.pmin FitFitData.aktuell_Modell.ModellCell{5}{i} ];
            FitFitData.constant.pmax=[FitFitData.constant.pmax FitFitData.aktuell_Modell.ModellCell{6}{i} ];
        elseif strcmpi('square',FitFitData.ParameterTable{i,3})
            FitFitData.square.function='p(1)+p(2).*w+p(3).*w.^2';
            b = ([ ones(numel(SOC_vektor),1) SOC_vektor']\Par_orig_vektor(:,i));
            p=[ b' 0.1];
            options = optimset('MaxIter',10000,'MaxFunEvals',10000,'TolX',1e-12,'TolFun',1e-12);
            w = SOC_vektor';
            p_min = [ -abs(b'*10) -inf ];
            p_max = [ abs(b'*10) inf ];
            [p,~,~,~]=function_fit_easyfit2(app,w,[Par_orig_vektor(:,i), zeros(size(Par_orig_vektor(:,i)))],...
                p,@function_model_all_types2, ...
                p_min,  p_max ,options, FitFitData.square.function);
            FitFitData.square.indices = [ FitFitData.square.indices i];
            FitFitData.square.koeffizient_init = [FitFitData.square.koeffizient_init p'];
            FitFitData.square.pinit=[FitFitData.square.pinit p'];
            FitFitData.square.pmin=[FitFitData.square.pmin p_min' ];
            FitFitData.square.pmax=[FitFitData.square.pmax p_max' ];
        elseif strcmpi('cubic',FitFitData.ParameterTable{i,3})
            FitFitData.cubic.function='p(1)+p(2).*w+p(3).*w.^2+p(4).*w.^3';
            b = ([ ones(numel(SOC_vektor),1) SOC_vektor']\Par_orig_vektor(:,i));
            p=[ b' 0.01];
            options = optimset('MaxIter',10000,'MaxFunEvals',10000,'TolX',1e-12,'TolFun',1e-12);
            w = SOC_vektor';
            p_min = [ -abs(b'*10) -inf ];
            p_max = [ abs(b'*10) inf ];
            [p,~,~,~]=function_fit_easyfit2(app,w,[Par_orig_vektor(:,i), zeros(size(Par_orig_vektor(:,i)))],...
                p,@function_model_all_types2, ...
                p_min,  p_max ,options, 'p(1)+p(2).*w+p(3).*w.^2');
            p_min = [ -abs(p*10) -inf ];
            p_max = [ abs(p*10) inf ];
            p=[ p 0.01];
            [p,~,~,~]=function_fit_easyfit2(app,w,[Par_orig_vektor(:,i), zeros(size(Par_orig_vektor(:,i)))],...
                p,@function_model_all_types2, ...
                p_min,  p_max ,options, FitFitData.square.function);
            [p,~,~,~]=function_fit_easyfit2(app,w,[Par_orig_vektor(:,i), zeros(size(Par_orig_vektor(:,i)))],...
                p,@function_model_all_types2, ...
                p_min,  p_max ,options, FitFitData.cubic.function);
            FitFitData.cubic.indices = [ FitFitData.cubic.indices i];
            FitFitData.cubic.koeffizient_init = [FitFitData.cubic.koeffizient_init p'];
            FitFitData.cubic.pinit=[FitFitData.cubic.pinit p'];
            FitFitData.cubic.pmin=[FitFitData.cubic.pmin p_min' ];
            FitFitData.cubic.pmax=[FitFitData.cubic.pmax p_max' ];
        elseif strcmpi('exponential',FitFitData.ParameterTable{i,3})
            FitFitData.exponential.function='p(1)+p(2).*exp((w-p(4))./p(3))';
            b = ([SOC_vektor' ones(numel(SOC_vektor),1) ]\Par_orig_vektor(:,i));
            if b(1)<0
                p=[ 0.1 0.1 -10 5 ];
                p_min=[ 0 1e-6 -1000 -10 ];
                p_max=[inf 1000 -.1 110 ];
            else
                p=[ 0.1 0.1 10 5 ];
                p_min=[ 0 1e-6 0.1 -10 ];
                p_max=[inf 1000 1000 110 ];
            end
            options = optimset('MaxIter',10000,'MaxFunEvals',10000,'TolX',1e-12,'TolFun',1e-12);
            w = SOC_vektor';
            [p,~,~,~]=function_fit_easyfit2(app,w,[Par_orig_vektor(:,i), zeros(size(Par_orig_vektor(:,i)))],...
                p,@function_model_all_types2, ...
                p_min,  p_max ,options, FitFitData.exponential.function);
            Fitted = eval(FitFitData.exponential.function);
            FitFitData.exponential.indices = [ FitFitData.exponential.indices i];
            FitFitData.exponential.koeffizient_init = [FitFitData.exponential.koeffizient_init p'];
            FitFitData.exponential.pinit=[FitFitData.exponential.pinit p'];
            FitFitData.exponential.pmin=[FitFitData.exponential.pmin p_min' ];
            FitFitData.exponential.pmax=[FitFitData.exponential.pmax p_max' ];
        elseif strcmpi('individual',FitFitData.ParameterTable{i,3})
            FitFitData.individual.indices = [ FitFitData.individual.indices i];
            FitFitData.individual.koeffizient_init = [FitFitData.individual.koeffizient_init ones(numel(useindex),1)];
            for j = 1:numel(useindex)
                pmax(j,1) = FitFitData.Model{useindex(j)}.Fit.Parameter_max(i);
                pmin(j,1) = FitFitData.Model{useindex(j)}.Fit.Parameter_min(i);
                pinit(j,1) = FitFitData.Model{useindex(j)}.Fit.Parameter(i);
            end
            FitFitData.individual.pinit=[FitFitData.individual.pinit pinit];
            FitFitData.individual.pmin=[FitFitData.individual.pmin pmin];
            FitFitData.individual.pmax=[FitFitData.individual.pmax pmax];
        end
    end
end
fitfitparnr=0;
pinit_ges=[];
pmin_ges=[];
pmax_ges=[];


for variante = {'linear' 'constant' 'square' 'cubic' 'exponential' 'individual'}
    ParIndices = fitfitparnr+(1:numel(FitFitData.(variante{1}).koeffizient_init));
    FitFitData.(variante{1}).fitfitparind = reshape(ParIndices,size(FitFitData.(variante{1}).koeffizient_init));
    FitFitData.(variante{1}).Ersetzfunction={};
    if ~strcmp(variante{1},'individual')
        for k = 1:numel(FitFitData.(variante{1}).indices)
            FitFitData.(variante{1}).Ersetzfunction{k}=FitFitData.(variante{1}).function;
            for k2 = 1:size(FitFitData.(variante{1}).koeffizient_init,1)
                FitFitData.(variante{1}).Ersetzfunction{k} = strrep(FitFitData.(variante{1}).Ersetzfunction{k},['p(' num2str(k2) ')'],['r(' num2str(k2) ')']);
            end
            for k2 = 1:size(FitFitData.(variante{1}).koeffizient_init,1)
                FitFitData.(variante{1}).Ersetzfunction{k} = ...
                    strrep(FitFitData.(variante{1}).Ersetzfunction{k},...
                    ['r(' num2str(k2) ')'],['p(' num2str(FitFitData.(variante{1}).fitfitparind(k2,k)) ')']);
            end
        end
    end
    fitfitparnr = fitfitparnr + numel(FitFitData.(variante{1}).koeffizient_init);
    pinit_ges = [pinit_ges ; FitFitData.(variante{1}).pinit(:)];
    pmin_ges = [pmin_ges ; FitFitData.(variante{1}).pmin(:)];
    pmax_ges = [pmax_ges ; FitFitData.(variante{1}).pmax(:)];
    
end

GesamtFitFunction='';
GesamtFitFunction_MF='';
w_ges=[];
Z_ges=[];
Z_origFit=[];
Z_origFit_MF=[];

start_w=zeros(size(useindex));
end_w=zeros(size(useindex));
for j=1:numel(useindex)
    f2 = dir(['output/' FitFitData.BatterieName '/'  FitFitData.Zustand '/' FitFitData.TString '/*_' FitFitData.SOCString{useindex(j)} '.mat']);
    LoadedData = load(['output/' FitFitData.BatterieName '/'  FitFitData.Zustand '/' FitFitData.TString '/' f2.name]);
    if ~isempty(LoadedData) && ismember('DRT_GUI',fieldnames(LoadedData)) ...
            && ~isempty(LoadedData.DRT_GUI) && ismember('Messdaten',fieldnames(LoadedData.DRT_GUI))
        w=LoadedData.DRT_GUI.Messdaten.omega(LoadedData.DRT_GUI.Messdaten.aktiv==1) ;
        w_ges = [w_ges ; reshape(w,numel(w),1)];
        Zreal=LoadedData.DRT_GUI.Messdaten.Zreal(LoadedData.DRT_GUI.Messdaten.aktiv==1) ;
        Zimg=LoadedData.DRT_GUI.Messdaten.Zimg(LoadedData.DRT_GUI.Messdaten.aktiv==1) ;
        Z_origFit_real=LoadedData.DRT_GUI.Fit.Zreal ;
        Z_origFit_img=LoadedData.DRT_GUI.Fit.Zimg ;
        Z_origFitMF_real=LoadedData.DRT_GUI.Fit.korrigiert.Zreal ;
        Z_origFitMF_img=LoadedData.DRT_GUI.Fit.korrigiert.Zimg ;
        Z_ges = [Z_ges ; [reshape(Zreal,numel(w),1) reshape(Zimg,numel(w),1)]];
        Z_origFit = [Z_origFit ; [reshape(Z_origFit_real,numel(w),1) reshape(Z_origFit_img,numel(w),1)]];
        Z_origFit_MF = [Z_origFit_MF ; [reshape(Z_origFitMF_real,numel(w),1) reshape(Z_origFitMF_img,numel(w),1)]];
        if j==1
            start_w(j) = 1;
        else
            start_w(j) = end_w(j-1)+1;
        end
        end_w(j) = start_w(j)+numel(w)-1;
        
        
        EinzelFunction = FitFitData.aktuell_Modell.Rechnen_Modell;
        EinzelFunction_MF = FitFitData.aktuell_Modell.Rechnen_Modell_MF;
        for i = 1:numel(Par)
            EinzelFunction = strrep(EinzelFunction,['p(' num2str(i) ')'] ,['q(' num2str(i) ')']);
            EinzelFunction_MF = strrep(EinzelFunction_MF,['p(' num2str(i) ')'] ,['q(' num2str(i) ')']);
        end
        EinzelFunction = strrep(EinzelFunction,'w',['w(' num2str(start_w(j)) ':' num2str(end_w(j)) ')']);
        EinzelFunction_MF = strrep(EinzelFunction_MF,'w',['w(' num2str(start_w(j)) ':' num2str(end_w(j)) ')']);
        for i = 1:numel(Par)
            if Par(i).ParameterFix
                EinzelFunction = strrep(EinzelFunction,['q(' num2str(i) ')'],['(' num2str(Par_orig_vektor(j,i)) ')']);
                EinzelFunction_MF = strrep(EinzelFunction_MF,['q(' num2str(i) ')'],['(' num2str(Par_orig_vektor(j,i)) ')']);
            elseif ismember(i,FitFitData.individual.indices)
                EinzelFunction = strrep(EinzelFunction,['q(' num2str(i) ')'],['p(' ...
                    num2str(FitFitData.individual.fitfitparind(j,FitFitData.individual.indices==i)) ...
                    ')']);
                EinzelFunction_MF = strrep(EinzelFunction_MF,['q(' num2str(i) ')'],['p(' ...
                    num2str(FitFitData.individual.fitfitparind(j,FitFitData.individual.indices==i)) ...
                    ')']);
            else
                for variante = {'linear' 'constant' 'square' 'cubic' 'exponential' }
                    if ismember(i,FitFitData.(variante{1}).indices)
                        EinzelFunction = strrep(EinzelFunction,['q(' num2str(i) ')'],['(' ...
                            strrep(FitFitData.(variante{1}).Ersetzfunction{FitFitData.(variante{1}).indices==i},'w',['(' num2str(FitFitData.SOCs(useindex(j))) ')']) ...
                            ')']);
                        EinzelFunction_MF = strrep(EinzelFunction_MF,['q(' num2str(i) ')'],['(' ...
                            strrep(FitFitData.(variante{1}).Ersetzfunction{FitFitData.(variante{1}).indices==i},'w',['(' num2str(FitFitData.SOCs(useindex(j))) ')']) ...
                            ')']);
                        break;
                    end
                end
            end
        end
        GesamtFitFunction = [GesamtFitFunction ' ; ' EinzelFunction];
        GesamtFitFunction_MF = [GesamtFitFunction_MF ' ; ' EinzelFunction_MF];
        
    else
        error(['Datei fehlerhaft:' 'output/' FitFitData.BatterieName '/'  FitFitData.Zustand '/' FitFitData.TString '/' f2.name ])
    end
    
    
end

GesamtFitFunction = ['[' GesamtFitFunction ']'];
GesamtFitFunction_MF = ['[' GesamtFitFunction_MF ']'];
options = optimset('MaxIter',50000,'MaxFunEvals',500000,'TolX',1e-8,'TolFun',1e-8);
w=w_ges;
[p,~,~,~]=function_fit_easyfit2(app,w_ges,Z_ges,pinit_ges,@function_model_all_types2, pmin_ges, pmax_ges,options, GesamtFitFunction);
Z_FitFit = eval(GesamtFitFunction);
Z_FitFit_MF = eval(GesamtFitFunction_MF);

Par_neu_vektor = Par_orig_vektor;
Par_init_vektor = Par_orig_vektor;
for i=1:numel(Par)
    
    if Par(i).ParameterFix
        
    else
        %         figure;plot(FitFitData.SOCs(useindex),Par_orig_vektor(:,i),'DisplayName',[FitFitData.ParameterTable{i,2} ' original']);
        %         hold all; grid on
        if ismember(i,FitFitData.individual.indices)
            Par_neu_vektor(:,i) = p(FitFitData.individual.fitfitparind(:,FitFitData.individual.indices==i));
            %             plot(FitFitData.SOCs(useindex),Par_neu_vektor(:,i),...
            %                 'DisplayName',[FitFitData.ParameterTable{i,2} ' individual fitfit'])
        else
            for variante = {'linear' 'constant' 'square' 'cubic' 'exponential' }
                if ismember(i,FitFitData.(variante{1}).indices)
                    w=FitFitData.SOCs(useindex);
                    
                    Par_neu_vektor(:,i)=eval(FitFitData.(variante{1}).Ersetzfunction{FitFitData.(variante{1}).indices==i});
                    p_old = p;p=pinit_ges;
                    Par_init_vektor(:,i) = eval(FitFitData.(variante{1}).Ersetzfunction{FitFitData.(variante{1}).indices==i});
                    %                     plot(FitFitData.SOCs(useindex),Par_init_vektor(:,i),...
                    %                         'DisplayName',[FitFitData.ParameterTable{i,2} ' initial guess'])
                    p=p_old;
                    %                     plot(FitFitData.SOCs(useindex),Par_neu_vektor(:,i),...
                    %                         'DisplayName',[FitFitData.ParameterTable{i,2} ' ' variante{1} ' fitfit'])
                    break;
                end
            end
        end
        %         l=legend('show');
        %         set(l,'Interpreter','none')
        
    end
end
FitFitData.FitFit.Par_orig_vektor = Par_orig_vektor;
FitFitData.FitFit.Par_neu_vektor = Par_neu_vektor;
FitFitData.FitFit.Par_init_vektor = Par_init_vektor;
FitFitData.FitFit.pinit_ges = pinit_ges;
FitFitData.FitFit.pbest = p;
FitFitData.FitFit.w_ges = w_ges;
FitFitData.FitFit.start_w = start_w;
FitFitData.FitFit.end_w = end_w;
FitFitData.FitFit.Z_ges = Z_ges;
FitFitData.FitFit.Z_origFit = Z_origFit;
FitFitData.FitFit.Z_origFit_MF = Z_origFit_MF;
FitFitData.FitFit.Z_FitFit = Z_FitFit;
FitFitData.FitFit.Z_FitFit_MF = Z_FitFit_MF;
FitFitData.FitFit.GesamtFitFunction = GesamtFitFunction;
FitFitData.FitFit.GesamtFitFunction_MF = GesamtFitFunction_MF;
FitFitData.FitFit.useindex = useindex;
FitFitData.FitFit.SOC_vektor = SOC_vektor;
eventdata2.Indices=[find(cell2mat(FitFitData.ParameterTable(:,1))==false,1,'first') 2];
fcn_ParameterTable_CellSelectionCallback(app,eventdata2)
eventdata2.Indices=[useindex(1) 2];
fcn_SOCTable_CellSelectionCallback(app,eventdata2)
end