function fcn_ParameterTable_CellSelection(app, event)
global FitFitData
plot(app.ParameterAxes,0,0)
legend(app.ParameterAxes,'show')
legend(app.ParameterAxes,'off')
cla(app.ParameterAxes)
try
if isempty(FitFitData) || ~isstruct(FitFitData) || ~ismember('FitFit',fieldnames(FitFitData)) || isempty(FitFitData.FitFit) || ~isstruct(FitFitData.FitFit)...
        || isempty(event) || isnumerictype(event) || ~ismember('Indices',fieldnames(event)) %|| numel(event.Indices)~=2
    return
end
catch 
    return
end
i = event.Indices(1);
plot(app.ParameterAxes,FitFitData.FitFit.SOC_vektor',FitFitData.FitFit.Par_orig_vektor(:,i),'DisplayName',[FitFitData.ParameterTable{i,2} ' original']);
hold(app.ParameterAxes,'on'); grid(app.ParameterAxes,'on');
if ismember(i,FitFitData.individual.indices)
    plot(app.ParameterAxes,FitFitData.FitFit.SOC_vektor',FitFitData.FitFit.Par_neu_vektor(:,i),...
        'DisplayName',[FitFitData.ParameterTable{i,2} ' individual fitfit'])
else
    for variante = {'linear' 'constant' 'square' 'cubic' 'exponential' }
        if ismember(i,FitFitData.(variante{1}).indices)
            plot(app.ParameterAxes,FitFitData.FitFit.SOC_vektor',FitFitData.FitFit.Par_neu_vektor(:,i),...
                'DisplayName',[FitFitData.ParameterTable{i,2} ' ' variante{1} ' fitfit'])
            plot(app.ParameterAxes,FitFitData.FitFit.SOC_vektor',FitFitData.FitFit.Par_init_vektor(:,i),...
                'DisplayName',[FitFitData.ParameterTable{i,2} ' initial guess'])
            break;
        end
    end
end
l=legend(app.ParameterAxes,'show');
set(l,'Interpreter','none');
app.ParameterAxes.XLabel.String='SOC in %';app.ParameterAxes.XLabel.Interpreter='none';