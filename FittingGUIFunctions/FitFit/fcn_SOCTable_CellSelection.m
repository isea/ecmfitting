function fcn_SOCTable_CellSelection(app,event)
global FitFitData
FarbenLaden()
plot(app.NyquistAxes,0,0)
legend(app.NyquistAxes,'show')
legend(app.NyquistAxes,'off')
cla(app.NyquistAxes)

plot(app.NyquistMFAxes,0,0)
legend(app.NyquistMFAxes,'show')
legend(app.NyquistMFAxes,'off')
cla(app.NyquistMFAxes)

plot(app.RealAxes,0,0)
legend(app.RealAxes,'show')
legend(app.RealAxes,'off')
cla(app.RealAxes)

plot(app.ImagAxes,0,0)
legend(app.ImagAxes,'show')
legend(app.ImagAxes,'off')
cla(app.ImagAxes)

if isempty(FitFitData) || ~isstruct(FitFitData) || ~ismember('FitFit',fieldnames(FitFitData)) || isempty(FitFitData.FitFit) || ~isstruct(FitFitData.FitFit)...
        || isempty(event)  || isnumerictype(event) || ~ismember('Indices',fieldnames(event)) %|| numel(event.Indices)~=2
    return
end

j = find(FitFitData.FitFit.useindex==event.Indices(1));
if isempty(j),return;end
start = FitFitData.FitFit.start_w(j);
ende =  FitFitData.FitFit.end_w(j);
w = FitFitData.FitFit.w_ges(start:ende);
Z_mess = FitFitData.FitFit.Z_ges(start:ende,1)+1i*FitFitData.FitFit.Z_ges(start:ende,2);
Z_origFit = FitFitData.FitFit.Z_origFit(start:ende,1)+1i*FitFitData.FitFit.Z_origFit(start:ende,2);
Z_origFit_MF = FitFitData.FitFit.Z_origFit_MF(start:ende,1)+1i*FitFitData.FitFit.Z_origFit_MF(start:ende,2);

plot(app.NyquistAxes,Z_mess,'color',RWTHBlau,'DisplayName','Messung');
hold(app.NyquistAxes,'on'); grid(app.NyquistAxes,'on');axis(app.NyquistAxes,'equal');  set(app.NyquistAxes,'ydir','reverse')
plot(app.NyquistAxes,Z_origFit,'color',RWTHTuerkis,'DisplayName','Fit');
if ~isempty(FitFitData.FitFit.pbest)
    plot(app.NyquistAxes,FitFitData.FitFit.Z_FitFit(start:ende,1),'color',RWTHRot,'DisplayName','FitFit');
end
l=legend(app.NyquistAxes,'show');
set(l,'Interpreter','none','Location','Northwest');

semilogx(app.RealAxes,w/2/pi,real(Z_mess),'color',RWTHBlau,'DisplayName','Messung');
hold(app.RealAxes,'on'); grid(app.RealAxes,'on'); set(app.RealAxes,'xdir','reverse')
semilogx(app.RealAxes,w/2/pi,real(Z_origFit),'color',RWTHTuerkis,'DisplayName','Fit');
if ~isempty(FitFitData.FitFit.pbest)
    semilogx(app.RealAxes,w/2/pi,real(FitFitData.FitFit.Z_FitFit(start:ende,1)),'color',RWTHRot,'DisplayName','FitFit');
end
l=legend(app.RealAxes,'show');
set(l,'Interpreter','none','Location','Northwest');
app.RealAxes.XLabel.String='f in Hz';

ZCLim=1./(1i.*w.*(1./(w(end)*(-imag(Z_mess(end))))));
semilogx(app.ImagAxes,w/2/pi,imag(Z_mess-ZCLim),'color',RWTHBlau,'DisplayName','Messung');
hold(app.ImagAxes,'on'); grid(app.ImagAxes,'on'); set(app.ImagAxes,'xdir','reverse'),set(app.ImagAxes,'ydir','reverse')
semilogx(app.ImagAxes,w/2/pi,imag(Z_origFit-ZCLim),'color',RWTHTuerkis,'DisplayName','Fit');
if ~isempty(FitFitData.FitFit.pbest)
    semilogx(app.ImagAxes,w/2/pi,imag(FitFitData.FitFit.Z_FitFit(start:ende,1)-ZCLim),'color',RWTHRot,'DisplayName','FitFit');
end
l=legend(app.ImagAxes,'show');
set(l,'Interpreter','none','Location','Northwest');
app.ImagAxes.XLabel.String='f in Hz';

plot(app.NyquistMFAxes,Z_mess-Z_origFit+Z_origFit_MF,'color',RWTHBlau,'DisplayName','Messung');
hold(app.NyquistMFAxes,'on'); grid(app.NyquistMFAxes,'on');axis(app.NyquistMFAxes,'equal'); set(app.NyquistMFAxes,'ydir','reverse')
plot(app.NyquistMFAxes,Z_origFit_MF,'color',RWTHTuerkis,'DisplayName','Fit');
if ~isempty(FitFitData.FitFit.pbest)
    plot(app.NyquistMFAxes,FitFitData.FitFit.Z_FitFit_MF(start:ende,1),'color',RWTHRot,'DisplayName','FitFit');
end
l=legend(app.NyquistMFAxes,'show');
set(l,'Interpreter','none','Location','South');
end