function fcn_LoadDataButton(app,event)
global DRT_GUI
global FitFitData
ColFormat=get(app.ParameterTable,'ColumnFormat');
ColFormat(1,[1:2,4])={''};
ColFormat{1,3}={' ','linear','constant', 'square', 'cubic', 'exponential', 'individual'};
set(app.ParameterTable,'ColumnFormat',ColFormat)
if isempty(DRT_GUI) || ~isstruct(DRT_GUI) ...
        || ~ismember('Fit',fieldnames(DRT_GUI)) || isempty(DRT_GUI.Fit) || ~isstruct(DRT_GUI.Fit) ...
        || ~ismember('aktuell_Modell',fieldnames(DRT_GUI.Fit)) ...
        || ~ismember('P_Name',fieldnames(DRT_GUI.Fit.aktuell_Modell))
    return
end
BatterieName = DRT_GUI.Testparameter.Batterie;
Zustand = DRT_GUI.Testparameter.Zustand;
Temperatur = DRT_GUI.Testparameter.Temperatur;
TString = [strrep(num2str(Temperatur),'-','m') 'grad'];
OrigSOC = DRT_GUI.Testparameter.SOC;
OrigSOCString = [strrep(num2str(OrigSOC),'-','m') 'SOC'];
f = dir(['output/' BatterieName '/'  Zustand '/' TString '/*_' OrigSOCString '_Modell.mat']);
if ~isempty(f)
    OriginalModell = load(['output/' BatterieName '/'  Zustand '/' TString '/' f.name]);
else
    error('Bitte speichern Sie zuerst den aktuellen Datensatz in der DRT_GUI.')
end
ParameterNamen = DRT_GUI.Fit.aktuell_Modell.P_Name(1,:)';
%ParameterNamen = {ParameterNamen{:}, DRT_GUI.Fit.Implementierung.OCV{5:end,1}}';
ParameterFix = num2cell(true(numel(ParameterNamen),1));%num2cell(logical(DRT_GUI.Fit.ParFix'));
Functions = repmat({''},numel(ParameterNamen),1);
Results = repmat({''},numel(ParameterNamen),1);
FitFitData.ParameterTable=[ParameterFix ParameterNamen Functions Results];
set(app.ParameterTable,'Data',FitFitData.ParameterTable);

SOCs = [];
SOCString = {};
f = dir(['output/' BatterieName '/'  Zustand '/' TString '/*_*SOC_Modell.mat']);
for j = 1:numel(f)
    if ~strcmp(f(j).name,'.') && ~strcmp(f(j).name,'..') && ~f(j).isdir
        SOCString = [SOCString;strrep(strrep(strrep(strrep(strrep(f(j).name,TString,''),BatterieName,''),Zustand,''),'_',''),'Modell.mat','')];
    end
end
for j = 1:numel(SOCString)
    if ~sum(find(SOCs == str2num(strrep(strrep(SOCString{j},'SOC',''),'m','-'))))
        SOCs = [SOCs ; str2num(strrep(strrep(SOCString{j},'SOC',''),'m','-'))];
    end
end
[SOCs,sortIndex] = sort(SOCs);
SOCString = SOCString(sortIndex);
gueltig = repmat({false},numel(SOCs),1);

Model = cell(1,numel(SOCs));

richtigesModell = zeros(size(SOCs));

for j = 1:numel(SOCs)
    f = dir(['output/' BatterieName '/'  Zustand '/' TString '/*_' SOCString{j} '_Modell.mat']);
    f2 = dir(['output/' BatterieName '/'  Zustand '/' TString '/*_' SOCString{j} '.mat']);
    if ~isempty(f) && ~isempty(f2)
        Model{1,j} = load(['output/' BatterieName '/'  Zustand '/' TString '/' f.name]);
        CompareName =Model{1,j}.Fit.Modell.Modellname;
        if strcmp(CompareName(end-3:end),'_OCV'), CompareName=CompareName(1:end-4);end
        if strcmp(CompareName,DRT_GUI.Fit.aktuell_Modell.Modellname) ...
                && size(Model{1,j}.Fit.Modell.P_Name(1,~strcmp((Model{1,j}.Fit.Modell.P_Name(2,:)),'')),2) == numel(ParameterNamen)
            richtigesModell(j)=1;
            if Model{1,j}.Fit.gueltig
                gueltig{j}=true;
            end
        end
    end
end
FitFitData.SOCTable=[ gueltig(richtigesModell==1,1) num2cell(SOCs(richtigesModell==1)) gueltig(richtigesModell==1,1)];
FitFitData.BatterieName=BatterieName;
FitFitData.Zustand=Zustand;
FitFitData.Temperatur=Temperatur;
FitFitData.TString=TString;
FitFitData.SOCs=SOCs(richtigesModell==1);
FitFitData.SOCString=SOCString(richtigesModell==1);
FitFitData.Model=Model(1,richtigesModell==1);
FitFitData.gueltig=gueltig(richtigesModell==1);
FitFitData.aktuell_Modell = DRT_GUI.Fit.aktuell_Modell;
set(app.SOCTable,'Data',FitFitData.SOCTable)

FitFitData.FitFit=[];
fcn_FitFitButton_Callback(app,event)
end