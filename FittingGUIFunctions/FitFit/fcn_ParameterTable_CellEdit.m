function fcn_ParameterTable_CellEdit(app,event)
global FitFitData
ParameterTable = get(app.ParameterTable,'Data');
if isempty(event) || ~ismember('Indices',fieldnames(event))
    for i = 1:size(ParameterTable,1)
        if ParameterTable{i,1}
            ParameterTable{i,3}=[{' '},{'linear'},{'constant'}, {'square'}, {'cubic'}, {'exponential'}];
            ParameterTable{i,4}='';
        else
            ParameterTable{i,3}='linear';
            ParameterTable{i,4}='';
        end
    end
else
    if event.Indices(2) ==1
        if ParameterTable{event.Indices(1),1}
            ParameterTable{event.Indices(1),3}=' ';
            ParameterTable{event.Indices(1),4}='';
        else
            ParameterTable{event.Indices(1),3}='linear';
            ParameterTable{event.Indices(1),4}='';
        end
    elseif event.Indices(2) ==3
        if ParameterTable{event.Indices(1),3}==' '
            ParameterTable{event.Indices(1),1}=true;
            ParameterTable{event.Indices(1),4}='';
        else
            ParameterTable{event.Indices(1),1}=false;
            ParameterTable{event.Indices(1),4}='';
        end
    end
end
set(app.ParameterTable,'Data',ParameterTable)
FitFitData.ParameterTable = ParameterTable;
end