function fcn_InitialTDF(app,event)
tic
SOCs = get(app.SOCPopup ,'Items');
global DRT_GUI
% try
    for i_SOC = 2:numel(SOCs)
        set(app.SOCPopup,'Value',SOCs{i_SOC})
        fcn_SOCPopup(app,event)
%         DRT_GUI.Messdaten.relax.zeit=DRT_GUI.Messdaten.nach_eis.zeit;
%         DRT_GUI.Messdaten.relax.spannung=DRT_GUI.Messdaten.nach_eis.spannung;
%         DRT_GUI.Messdaten.relax.strom=DRT_GUI.Messdaten.nach_eis.strom;
        fcn_ZeitbereichsFittingButton(app,event)
        fcn_TDF_PasteButton(app,event)
%         fcn_TDF_ResetOCVFitButton(app,event)
%         fcn_TDF_PasteButton(app,event)
%         TDF_Table=get(app.TDF_ImplementierungsTable,'Data');
%         TempCell=cell(size(TDF_Table(end-3:end-1,2),1),1);
%         for i1=1:size(TempCell,1)
%             TempCell{i1,1}='Re-Fit';
%         end
%         TDF_Table(end-3:end-1,2)=TempCell;
%         set(app.TDF_ImplementierungsTable,'Data',TDF_Table)
%         fcn_TDF_ImplementierungsTable_CellEdit(app,event)
%         TempRelax=get(app.TDF_RelaxTable,'Data');
%         TempTable={1e-7, Inf;0.1, 100; 1e-5, Inf; 1, 1000; 1e-5, Inf; 10, 10000};
%         TempRelax(5:end,4:5)=TempTable;
%         set(app.TDF_RelaxTable,'Data',TempRelax)
%         set(app.RelaxTable,'Data',TempRelax)
%         fcn_TDF_RelaxTable_CellEdit(app,event)
%         TempTable=app.TDF_RelaxTable.Data;
%         TempTable(5:end,4)=cellfun(@(x) {x*0.1}, TempTable(5:end,2));
%         TempTable(5:end,5)=cellfun(@(x) {x*10}, TempTable(5:end,2));
%         app.TDF_RelaxTable.Data=TempTable;
%         fcn_TDF_RelaxTable_CellEdit(app,event);

        for i=1:6
        fcn_TDF_OCVFitButton(app,event)
        pause(0.001)
        end
        fcn_TDF_CopyButton(app,event)
        fcn_TDF_SpeichernButton(app,event)
    end
% catch
% end
toc
end