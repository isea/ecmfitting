%% Create Fake Data to Test the FittingGUI 
Rser = 30e-3;
R1 = 1e-3;
Tau1 = 5e-2;
R2 =2e-3;
Tau2 = 5;

Voigt.Rser = Rser;
Voigt.R_RC(1) = R1;
Voigt.R_RC(2) = R2;
Voigt.R_RC(3) = 0.5*R2;
Voigt.R_RC(4) = 1.1*R2;
Voigt.R_RC(5) = 1.3*R2;
Voigt.C_RC(1) = Tau1/R1;
Voigt.C_RC(2) = Tau2/R2;
Voigt.C_RC(3) = Tau2/R2;
Voigt.C_RC(4) = Tau2/R2;
Voigt.C_RC(5) = Tau2/R2;
Voigt.CD = [0 1 2 3 4];

t = 0:0.1:4000;
I = zeros(size(t));
I(301:400) = 5 * ones(100,1);
I(9401:9500) = -5 *ones(100,1);
I(18501:18600) = 10 * ones(100,1);
I(27501:27600) = -10 * ones(100,1);

tdf_data.zeit = t;
tdf_data.strom = I;

stromindex = [1 300 401 9400 9501 18500 18601 27500 27601 40001];
strom = [0 5 0 -5 0 10 0 -10 0];
stromindex_to_CD_idx = [1 1 1 2 2 3 3 4 4];
cd_parameter_idx = [1 2 3 4];

U_RC = zeros(numel(tdf_data.strom),numel(Voigt.R_RC));
for i = 1:numel(stromindex)-1
    if stromindex(i)==1
        U0 = zeros(size(Voigt.R_RC));
    else
        U0 = U_RC(stromindex(i),:);
    end
    Umax = strom(i).*Voigt.R_RC;
    t = tdf_data.zeit(stromindex(i)+1:stromindex(i+1))-tdf_data.zeit(stromindex(i));
    for k = 1:numel(Voigt.R_RC)        
        if ~Voigt.CD(k)
            if abs(U0(k)-abs(Umax(k)))<=0.0001
                U_RC(stromindex(i)+1:stromindex(i+1),k) = Umax(k);
            else
                U_RC(stromindex(i)+1:stromindex(i+1),k) = U0(k)+(Umax(k)-U0(k)).*(1-exp(-t./(Voigt.R_RC(k).*Voigt.C_RC(k))));
            end
        elseif cd_parameter_idx(stromindex_to_CD_idx(i)) == Voigt.CD(k)
            if abs(U0(k)-Umax(k))<=0.0001
                U_RC(stromindex(i)+1:stromindex(i+1),k) = Umax(k);
            else
                U_RC(stromindex(i)+1:stromindex(i+1),k) = U0(k)+(Umax(k)-U0(k)).*(1-exp(-t./(Voigt.R_RC(k).*Voigt.C_RC(k))));
            end
        end
    end
end
Spannung = sum(U_RC,2);
plot(tdf_data.zeit,Spannung)