function fcn_PeakfinderSlider(app,event)
global DRT_Config
DRT_Config.PeakSensitivitaet = get(app.PeakfinderSlider,'Value');
set(app.PeakfinderEdit,'Value',num2str(DRT_Config.PeakSensitivitaet))
fcn_DRTButton_Callback(app, event)
end