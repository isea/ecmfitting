function [FindString] =fcn_evalconfiguration(ParamTable,FixTable,ConfigString)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
FindString={};
ConfigString = string(ConfigString);
for i = 1:size(ConfigString)
        FindString{i,1}=transpose(extractBetween(ConfigString(i),'"','"'));
        FindString{i,4} = ConfigString(i);
        index = strfind(FindString{i,4},'"');
        indexmat=[index(1:2:end);index(2:2:end)];
        for j=size(FindString{i,1},2):-1:1
            FindString{i,2}(:,j) = contains(ParamTable,FindString{i,1}(j));
            FindString{i,3}{j,1} = mat2str(FindString{i,2}(:,j));
            FindString{i,4}= replaceBetween(FindString{i,4},indexmat(1,j),indexmat(2,j),FindString{i,3}{j,1},'Boundaries','inclusive');
        end
        FindString{i,5} = eval(FindString{i,4});
        %Add fixed parameters
        FindString{i,6} = or(FindString{i,5},cell2mat(FixTable)==1);
end

end

