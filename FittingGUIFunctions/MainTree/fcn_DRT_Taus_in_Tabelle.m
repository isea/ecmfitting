function app = fcn_DRT_Taus_in_Tabelle(app)
global DRT_GUI;
if ~sum(ismember(fieldnames(DRT_GUI),'DRT')) || isempty(DRT_GUI.DRT) || numel(DRT_GUI.DRT.EI_DRT.peaks.tau)==0
    %msgbox(['Bitte zuerst eine DRT erstellen'])
    %set(app.TauTable,'Visible','off')
    %set(app.TauCaption,'Visible','off')
    return
end
if app.GUI
    set(app.TauTable,'Visible','on')
    ColFormat = get(app.TauTable,'ColumnFormat');
else
    app.TauTable.Visible = 'on';
    ColFormat = app.TauTable.ColumnFormat;
end
% set(app.TauCaption,'Visible','on')
Parameter_Taus = textscan(strrep(DRT_GUI.Fit.aktuell_Modell.ModellCell{1,3},' ',''),'%s','delimiter',',');
Parameter_Taus = Parameter_Taus{1,1}(~cellfun(@isempty,strfind(Parameter_Taus{1,1}','Tau')));
if isempty(Parameter_Taus) , Parameter_Taus = {' '};end
ColFormat{3} = Parameter_Taus';
if app.GUI
    set(app.TauTable,'ColumnFormat',ColFormat);
    TauCell = get(app.TauTable,'Data');
else
    app.TauTable.ColumnFormat = ColFormat;
    TauCell = app.TauTable.Data;    
end
if size(TauCell,1)>numel(DRT_GUI.DRT.EI_DRT.peaks.tau)
    TauCell = TauCell(1:numel(DRT_GUI.DRT.EI_DRT.peaks.tau),:);
end
if ~isempty(TauCell)
    TauCell = cell2struct(TauCell',{'Tau','Used','Parameter'});
end
for i = 1:numel(DRT_GUI.DRT.EI_DRT.peaks.tau)
    TauCell(i,1).Tau = DRT_GUI.DRT.EI_DRT.peaks.tau(i);
    if sum(ismember(fieldnames(DRT_GUI.DRT.EI_DRT.peaks),'used')) && ...
            sum(ismember(fieldnames(DRT_GUI.DRT.EI_DRT.peaks),'used_parname')) && ...
            DRT_GUI.DRT.EI_DRT.peaks.used(i)
        TauCell(i,1).Used = true;
        TauCell(i,1).Parameter = DRT_GUI.DRT.EI_DRT.peaks.used_parname{i};
    else
        TauCell(i,1).Used = false;
        TauCell(i,1).Parameter = '';
    end
end
TauCell = struct2cell(TauCell)';
if app.GUI
    set(app.TauTable,'Data',TauCell)
else
    app.TauTable.Data = TauCell;
end
end