function app = fcn_PunkteWegnehmeButton(app, event)
global DRT_GUI;
if isempty(DRT_GUI) || isempty(cell2mat(strfind(fieldnames(DRT_GUI),'Messdaten'))) || isempty(DRT_GUI.Messdaten)
    return
end
if app.GUI
    Typ = 'Nyquist';
    hold(app.axes1,'off')
    fcn_plot_Auswaehl(app,DRT_GUI.Messdaten,[],[],DRT_GUI.Testparameter.Batterie,Typ);
end
LP_str = app.PunkteWegnehmenTextBox.Value;
DRT_GUI.Messdaten.aktiv = ones(size(DRT_GUI.Messdaten.Z));
if ~isempty(app.PunkteWegnehmenTextBox.Value)
    eval(['DRT_GUI.Messdaten.aktiv([' LP_str ']) = 0;']);
end
DRT_GUI.Messdaten.low_Punkte_Weg = LP_str;
Typ = 'Messung';
% neu plot
%             if ~strcmp(event.Value, 'kein_plot')
if app.GUI
    hold(app.axes1,'off')
    fcn_plot_Auswaehl(app,DRT_GUI.Messdaten,[],[],DRT_GUI.Testparameter.Batterie,Typ);
    fcn_PlotFittedParametersButton(app,event);
end
end