function app = fcn_ProzesseEdit(app, event)
global DRT_Config
try
    if app.GUI
        Wert = round(str2num(get(app.ProzesseEdit,'Value')));
    else
        Wert = round(str2num(app.ProzesseEdit.Value));    
    end 
catch error_msg
    if app.GUI
        set(app.ProzesseEdit,'Value',num2str(DRT_Config.Prozesse))
    else
        app.ProzesseEdit.Value = num2str(DRT_Config.Prozesse);
    end
    errordlg(error_msg)
end
if isempty(Wert)|| Wert<app.ProzesseSlider.Limits(1) || Wert>app.ProzesseSlider.Limits(2)
    if app.GUI
        set(app.ProzesseEdit,'Value',num2str(DRT_Config.Prozesse))
    else
        app.ProzesseEdit.Value = num2str(DRT_Config.Prozesse);
    end
    errordlg('Ungültiger Wert')
else
    DRT_Config.Prozesse = Wert;
    if app.GUI
        set(app.ProzesseSlider,'Value',DRT_Config.Prozesse)
    else
        app.ProzesseSlider.Value = num2str(DRT_Config.Prozesse);
    end
    app = fcn_DRT_Button(app, event);
end
end