function fcn_CXMLDatabaseExportButtonPushed(app, event)
%Einlesen der FittingGUI Daten
autofit_path = cd;
global DRT_GUI
app.user = char(java.lang.System.getProperty('user.name'));
app.actBattery = Battery('');
app.pulse = PulsData();
load_models(app);
cp=pwd;
app.default_path=cp;
path=[cp,'/','output/',DRT_GUI.Testparameter.Batterie,'/',app.StateString,'/'];

app.actBattery = Battery('');

app.SOC = [];
app.SOH = [];
app.Temp = [];

app.actSOC = 1;
app.actTemp = 1;
app.actSOH = 1;
app.actParameter = 1;

ReFit_Whitelist =string(missing);
sSOH = '100';
tSOH = round(str2double(sSOH));
if isempty(app.SOH)
   app.SOH = tSOH;
   app.actSOH = 1;
else
   iSOH = find(app.SOH==tSOH);
   if iSOH
       app.actSOH = iSOH;
   else
       app.SOH = [app.SOH,tSOH];
       app.actSOH = length(app.SOH);
   end
end

app.actBattery = app.actBattery.setAuswertung('FittingGUI Daten',app.actSOH);   
   
current_path = pwd;
cd(path);
list = dir('*grad*');

for i=1:length(list)
    cd([path,'\',list(i).name])
    act_temp_list = dir('*SOC.mat');
    cd (current_path);
    for j=1:length(act_temp_list)
        warning('off', 'MATLAB:dispatcher:UnresolvedFunctionHandle');
        load([path,'\',list(i).name,'\',act_temp_list(j).name],'DRT_GUI');
        % Lade allgemeine Parameter beim ersten Aufruf
        if(i==1 && j==1)            
           app.actBattery.name = DRT_GUI.Testparameter.Batterie;
           app.SF_BatName.Text = DRT_GUI.Testparameter.Batterie;            
           iModell = find(strcmp(app.Modellliste,['ESB_',DRT_GUI.Fit.aktuell_Modell.Modellname]));
           change_act_model(app,iModell);
           app.SF_ModellauswahlDropDown.Value = app.SF_ModellauswahlDropDown.Items(iModell);
        end
        if(j==1)
               % Temperatur finden 
               temp = DRT_GUI.Testparameter.Temperatur;
               if isempty(app.Temp)
                   app.Temp = temp;
                   app.actTemp = 1;
               else
                   iTemp = find(app.Temp==temp);
                   if iTemp
                       app.actTemp = iTemp;
                   else
                       app.Temp = [app.Temp,temp];
                       app.actTemp = length(app.Temp);
                   end
               end
               
               % Kapazit�t speichern
               if isfield(DRT_GUI.Testparameter,'Cap')
                   app.SF_NomCap.Text = [num2str(DRT_GUI.Testparameter.Cap) ' Ah'];
                   app.actBattery = app.actBattery.addCapacity(DRT_GUI.Testparameter.Cap,app.actTemp,app.actSOH);
               else
                   sCap = inputdlg('Bitte Kapazit�t eingeben:','Kapazit�t');
                   app.SF_NomCap.Text = [sCap{1},' Ah'];
                   app.actBattery = app.actBattery.addCapacity(str2double(sCap),app.actTemp,app.actSOH);
               end
               qOCV = QOCV();
               app.actBattery = app.actBattery.addqOCV_CH(qOCV,app.actTemp,app.actSOH);
               app.actBattery = app.actBattery.addqOCV_DCH(qOCV,app.actTemp,app.actSOH);
        end
        % SOC finden
         SOC = DRT_GUI.Testparameter.SOC;
         if isempty(app.SOC)
             app.SOC = SOC;
             app.actSOC = 1;
         else
             % Einsortieren weiterer Messungen in aktuelles SOC Array, falls neue SOCs
             % auftreten, wird hinten angehangen
             iSOC = find(app.SOC==SOC);
             if iSOC
                 app.actSOC = iSOC;
             else
                 app.SOC = [app.SOC,SOC];
                 app.actSOC = length(app.SOC);
             end
         end
         % EIS Daten ablegen
         data = EISData();
         data.Zreal = DRT_GUI.Messdaten.Zreal;
         data.Zimg = DRT_GUI.Messdaten.Zimg;
         data.frequenz = DRT_GUI.Messdaten.frequenz;
         app.actBattery = app.actBattery.addEISdata(data,app.actSOC,app.actTemp,app.actSOH);
         % Fit Ablegen
         if ~isempty(iModell)
                                  
             fit = Fit();
             fit.Parameter = DRT_GUI.Fit.Parameter;
             fit.Parameter_min = DRT_GUI.Fit.Parameter_min;
             fit.Parameter_max = DRT_GUI.Fit.Parameter_max;
             fit.ParFix = DRT_GUI.Fit.ParFix;
             fit.Zreal = DRT_GUI.Fit.Zreal;
             fit.Zimag = DRT_GUI.Fit.Zimg;
             fit.Residuum = DRT_GUI.Fit.residuum;
             app.actBattery = app.actBattery.addFit(fit,app.actSOC,app.actTemp,app.actSOH);    
         end
         if isfield(DRT_GUI.Messdaten,'relax')
             if ~isempty(DRT_GUI.Messdaten.relax.zeit)
                 relax = RelaxData();
                 relax.time = DRT_GUI.Messdaten.relax.zeit;
                 relax.voltage = DRT_GUI.Messdaten.relax.spannung;
                 relax.current = DRT_GUI.Messdaten.relax.strom;
                 app.actBattery = app.actBattery.addRelaxdata(relax,app.actSOC,app.actTemp,app.actSOH);
             end
         else
             relax = RelaxData();
             app.actBattery = app.actBattery.addRelaxdata(relax,app.actSOC,app.actTemp,app.actSOH);
         end
    end
end
% Aktualisieren der Index Vektoren
app.actBattery = app.actBattery.addSOCtable(app.SOC);
app.actBattery = app.actBattery.addTemptable(app.Temp);
app.actBattery = app.actBattery.addSOHtable(app.SOH);

app.actParameter = 1;
sort_SOC(app);
sort_temp(app);
if app.offButton_temperature_depencendy.Value
    if app.referencetemperatureEditField.Value ~= -Inf
        f = dir(['output/' app.BatterieNamePopup.Value '/'  app.StateString '/*']);
        
        for reference_idx = 3:length(f)
            reference_temperature = f(reference_idx).name;
            reference_temperature = reference_temperature(1:end -4 );
            if contains(reference_temperature,'m')
                reference_temperature =strcat('-',reference_temperature(2:end));
            end
            if strcmp(num2str(app.referencetemperatureEditField.Value),reference_temperature)
                reference_counter = 1;
            end
        end
        if reference_counter == 0
            msgbox('Please select a valid reference temperature')
            return
        end
                  
    else
        msgbox('Please select a valid reference temperature')
        return
    end
end

%% Auswahlboxen aktualisieren
update_listboxes(app);

%% Aktualisieren der Plots
try
%     plot_nyquist(app);
%     plot_datapoints(app);
%     plot_relax(app);
catch
end

%% ReFit Parameter
for i=1:length(list)
cd([path,'\',list(i).name])
act_temp_list = dir('*SOC.mat');
cd (current_path);
for j=1:length(act_temp_list)
    warning('off', 'MATLAB:dispatcher:UnresolvedFunctionHandle');
    load([path,'\',list(i).name,'\',act_temp_list(j).name],'DRT_GUI');

    if(j==1)
       % Temperatur finden 
       temp = DRT_GUI.Testparameter.Temperatur;               
       iTemp = find(app.Temp==temp);
       if iTemp
           app.actTemp = iTemp;
       else
           app.Temp = [app.Temp,temp];
           app.actTemp = length(app.Temp);
       end              
    end
    % SOC finden
     SOC = DRT_GUI.Testparameter.SOC;         
     % Einsortieren weiterer Messungen in aktuelles SOC Array, falls neue SOCs
     % auftreten, wird hinten angehangen
     iSOC = find(app.SOC==SOC);
     if iSOC
         app.actSOC = iSOC;
     else
         app.SOC = [app.SOC,SOC];
         app.actSOC = length(app.SOC);
     end

    if ~isempty(DRT_GUI.Fit.aktuell_Modell.P_Name)&&~isempty(DRT_GUI.Fit.Implementierung.OCV)
        for k = 1:length(DRT_GUI.Fit.aktuell_Modell.P_Name)                  
            % Gibt es passenden ReFit Parameter?
            DRT_GUI.Fit.aktuell_Modell.P_Name{3,k}=find(transpose(strcmp(['ReFit_',DRT_GUI.Fit.aktuell_Modell.P_Name{1,k}],DRT_GUI.Fit.Implementierung.OCV(:,1))));
            % Soll Parameter verwendet werden?
            if isempty(DRT_GUI.Fit.aktuell_Modell.P_Name{3,k}) %falls kein ReFit gefunden
                DRT_GUI.Fit.aktuell_Modell.P_Name{4,k}=0;
            elseif sum(strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name{1,k},ReFit_Whitelist))>0             
                DRT_GUI.Fit.aktuell_Modell.P_Name{4,k}=1;
            else               
                DRT_GUI.Fit.aktuell_Modell.P_Name{4,k}=1;
                ReFit_Whitelist = [ReFit_Whitelist;DRT_GUI.Fit.aktuell_Modell.P_Name{1,k}];               
            end
        end

        change_several_fit_parameters(app,DRT_GUI.Fit.aktuell_Modell.P_Name,DRT_GUI.Fit.Implementierung.OCV);
    else
        disp(['Keine Tabelle mit Refitwerten gefunden f�r ',temp,' grad and ',SOC,'%.'])
    end
end    
end
cd(autofit_path)
fcn_Automatic_qOCV_Import(app,event);
fcn_ExportelectricalModellasMATButtonPushed(app,event);
end


