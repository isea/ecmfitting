function fcn_SchwingungEdit(app,event)
global DRT_Config
try
    Wert = str2num(get(app.SchwingungEdit,'Value'));
catch error_msg
    set(app.SchwingungEdit,'Value',num2str(DRT_Config.Schwingfaktor))
    errordlg(error_msg)
end
if isempty(Wert)|| (Wert<app.SchwingungSlider.Limits(1) || Wert>app.SchwingungSlider.Limits(2))
    set(app.SchwingungEdit,'Value',num2str(DRT_Config.Schwingfaktor))
    errordlg('Ungültiger Wert')
else
    DRT_Config.Schwingfaktor = Wert;
    set(app.SchwingungSlider,'Value',DRT_Config.Schwingfaktor)
    fcn_DRTButton_Callback(app, event)
end
end