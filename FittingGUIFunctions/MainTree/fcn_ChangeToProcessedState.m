function fcn_ChangeToProcessedState(app,index1,index2)

if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
    app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
    app.SOCPopup.Value=app.startingSOCDropDown.Value;
    fcn_SOCPopup(app, event);
    if ~strcmp(app.ModellAuswahlPopup.Value,'R2RC')
        app.ModellAuswahlPopup.Value='R2RC';
        fcn_ModellAuswahlPopup(app);
        fcn_BatterieNamePopup_Callback(app);
    end
end

end

