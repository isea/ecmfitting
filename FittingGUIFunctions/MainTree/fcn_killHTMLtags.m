function outstring = fcn_killHTMLtags(inputstring)
if iscell(inputstring)
    for i = 1:numel(inputstring)
        position1 = find(inputstring{i} == '<',1);
        position2 = find(inputstring{i} == '>',1);
        while ~(isempty(position1) || isempty(position2) || position1>position2)
            inputstring{i} = [ inputstring{i}(1:position1-1) inputstring{i}(position2+1:end)];
            position1 = find(inputstring{i} == '<',1);
            position2 = find(inputstring{i} == '>',1);
        end
    end
else
    position1 = find(inputstring == '<',1);
    position2 = find(inputstring == '>',1);
    while ~(isempty(position1) || isempty(position2) || position1>position2)
        inputstring = [ inputstring(1:position1-1) inputstring(position2+1:end)];
        position1 = find(inputstring == '<',1);
        position2 = find(inputstring == '>',1);
    end
end
outstring = inputstring;
end