function app = fcn_ModellAktualisierenButton(app, event)
%%% Modell in Popmenu aktualisieren
global Modellliste
global DRT_GUI
config();
getModellname_temp = size(Modellliste.Modell);
for getModellname_i = 1:getModellname_temp(1)
    getModellname{getModellname_i,1} = Modellliste.Modell{getModellname_i,1};
end
if app.GUI
    set(app.ModellAuswahlPopup,'Items',getModellname);
    set(app.GlobalFitUsedModelDropDown,'Items',getModellname);
    %%%%% Zu bearbeiten
    if ~isempty(DRT_GUI) && sum(ismember(fieldnames(DRT_GUI),'Fit')) &&  ~isempty(DRT_GUI.Fit) && sum(ismember(fieldnames(DRT_GUI.Fit),'aktuell_Modell')) &&  ~isempty(DRT_GUI.Fit.aktuell_Modell) &&  sum(ismember(fieldnames(DRT_GUI.Fit.aktuell_Modell),'Modellname'))
        akt_index = find(ismember(getModellname,DRT_GUI.Fit.aktuell_Modell.Modellname));
        if isempty(akt_index)
            akt_index = numel(getModellname)+1;
            getModellname{akt_index} = DRT_GUI.Fit.aktuell_Modell.Modellname;            
            set(app.ModellAuswahlPopup,'Items',getModellname);
            set(app.GlobalFitUsedModelDropDown,'Items',getModellname);            
            Modellliste.Modell(akt_index,1:numel(DRT_GUI.Fit.aktuell_Modell.ModellCell)) = DRT_GUI.Fit.aktuell_Modell.ModellCell;
        end
        %%%%%
    else
        akt_index = Modellliste.standard_modell;
    end
    app.ModellAuswahlPopup.Value=app.ModellAuswahlPopup.Items{akt_index};
    app.GlobalFitUsedModelDropDown.Value=app.ModellAuswahlPopup.Items{akt_index};
    fcn_ModellAuswahlPopup_Callback(app);
else
    app.ModellAuswahlPopup.Items = getModellname;
    app.GlobalFitUsedModelDropDown.Items = getModellname;
        %%%%% Zu bearbeiten
    if ~isempty(DRT_GUI) && sum(ismember(fieldnames(DRT_GUI),'Fit')) &&  ~isempty(DRT_GUI.Fit) && sum(ismember(fieldnames(DRT_GUI.Fit),'aktuell_Modell')) &&  ~isempty(DRT_GUI.Fit.aktuell_Modell) &&  sum(ismember(fieldnames(DRT_GUI.Fit.aktuell_Modell),'Modellname'))
        akt_index = find(ismember(getModellname,DRT_GUI.Fit.aktuell_Modell.Modellname));
        if isempty(akt_index)
            akt_index = numel(getModellname)+1;
            getModellname{akt_index} = DRT_GUI.Fit.aktuell_Modell.Modellname;
            app.ModellAuswahlPopup.Items= getModellname;
            app.GlobalFitUsedModelDropDown.Items = getModellname;            
            Modellliste.Modell(akt_index,1:numel(DRT_GUI.Fit.aktuell_Modell.ModellCell)) = DRT_GUI.Fit.aktuell_Modell.ModellCell;
        end
        %%%%%
    else
        akt_index = Modellliste.standard_modell;
    end
    app.ModellAuswahlPopup.Value=app.ModellAuswahlPopup.Items{akt_index};
    app.GlobalFitUsedModelDropDown.Value=app.ModellAuswahlPopup.Items{akt_index};
    app = fcn_ModellAuswahlPopup_Callback(app);
end
end