function fcn_FastForwardModelSwitch(app, event)
SOCs = get(app.SOCPopup ,'Items');
try
    tic
    for i_SOC = 2:numel(SOCs)
        set(app.SOCPopup,'Value',SOCs{i_SOC})
        fcn_SOCPopup_Callback(app,event)
        if ~strcmp(get(app.ModellAuswahlPopup,'Value'),'LiIon4')
            TableCell=get(app.ParamTable,'Data');
            set(app.ModellAuswahlPopup,'Value','LiIon4')
            fcn_ModellAuswahlPopup(app)
            TableCell2=get(app.ParamTable,'Data');
            set(app.PunkteWegnehmenTextBox,'Value','1:2 end-1:end')
            fcn_PunkteWegnehmeButton(app,event)
            TableCell2(1:size(TableCell,1),:)=TableCell;
            TableCell2(size(TableCell,1)+1:end,2)=num2cell(logical(zeros(1,size(TableCell2,1)-size(TableCell,1))));
            set(app.ParamTable,'Data',TableCell2)
            fcn_FitButtonFunction(app,event)
            fcn_FitButtonFunction(app,event)
            set(app.GueltigeMessungCheck,'Value',1)
            fcn_GueltigeMessungCheck(app, event)
            if app.KeinPlotCheckBox.Value~=1
                pause(0.01)
            end
            fcn_SpeichernButton(app,event)
        else
            set(app.GueltigeMessungCheck,'Value',1)
            fcn_GueltigeMessungCheck(app, event)
            fcn_SpeichernButton(app,event)
        end
    end
    toc
catch
end
end