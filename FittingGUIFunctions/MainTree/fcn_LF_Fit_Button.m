function fcn_LF_Fit_Button(app, event)
global DRT_GUI

if 0 && sum(strcmp(fields(DRT_GUI.Messdaten),'relax')) ...
        && sum(strcmp(fields(DRT_GUI.Messdaten.relax),'spannung'))  ...
        && sum(strcmp(fields(DRT_GUI.Messdaten.relax),'zeit'))  ...
        && numel(DRT_GUI.Messdaten.relax.spannung)>0 ...
        && ( sum(strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'CLim')) ||...
        sum(strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Cser'))||...
        sum(strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'CSer'))||...
        sum(strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'C0')))
    % es gibt eine gespeicherte Relaxation vor der Messung
    % Das Modell enthällt eine Serienkapazität
    % Die letzten 60 Minuten sollen gefittet werden
    FitIndex = find((DRT_GUI.Messdaten.relax.zeit(end)-DRT_GUI.Messdaten.relax.zeit)<3600);
    
    FitSpannung = DRT_GUI.Messdaten.relax.spannung(FitIndex);
    FitZeit = DRT_GUI.Messdaten.relax.zeit(FitIndex)-DRT_GUI.Messdaten.relax.zeit(FitIndex(1));
    
    p_init = [1000 FitSpannung(end) FitSpannung(1)-FitSpannung(end)];
    modelformel = 'p(2)+p(3).*exp(-w./p(1))';
    options = optimset('MaxIter',10000,'MaxFunEvals',10000,'TolX',1e-12,'TolFun',1e-12);
    [p,fval,exitflag,output]=function_fit_easyfit2(FitZeit,[FitSpannung, zeros(size(FitSpannung))],p_init,@function_model_all_types2, [0 0 -inf ], [inf,inf,inf] ,options, modelformel);
    w = FitZeit;
    NeuSpannung = eval(modelformel);
    w = 1e19;
    EndSpannung = eval(modelformel);
    if ~strcmp(eventdata,'kein_plot')
        figure;
        plot(DRT_GUI.Messdaten.relax.zeit,DRT_GUI.Messdaten.relax.spannung)
        grid on,hold on
        plot(DRT_GUI.Messdaten.relax.zeit(FitIndex(1))+FitZeit,NeuSpannung,'r')
        plot(DRT_GUI.Messdaten.relax.zeit,repmat(EndSpannung, size(DRT_GUI.Messdaten.relax.zeit)),'--k')
        plot(DRT_GUI.Messdaten.relax.zeit,repmat(DRT_GUI.Messdaten.relax.spannung(1), size(DRT_GUI.Messdaten.relax.zeit)),'--k')
    end
    
    CLimNr = find(strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'CLim'));
    if isempty(CLimNr),CLimNr = find(strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Cser'));end
    if isempty(CLimNr),CLimNr = find(strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'CSer'));end
    if isempty(CLimNr),CLimNr = find(strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'C0'));end
    Ladung = [0 cumsum(DRT_GUI.Messdaten.relax.strom(1:end-1) .* diff(DRT_GUI.Messdaten.relax.zeit))];
    TableCell = get(app.ParamTable,'Data');
    
    TableCell{CLimNr,2}=true;
    TableCell{CLimNr,3}=Ladung(end)/(EndSpannung-DRT_GUI.Messdaten.relax.spannung(1));
    
    set(app.ParamTable,'Data',TableCell);
    FitButton_Callback(app.FitButton, eventdata, handles)
    
else
    
    PunkteWegVorher = get(app.PunkteWegnehmenTextBox,'Value');
    
    set(app.PunkteWegnehmenTextBox,'Value','1:end-5')
    fcn_PunkteWegnehmenButton_Callback(app, event)
    Parameter_liste = textscan(strrep(DRT_GUI.Fit.aktuell_Modell.ModellCell{1,3},' ',''),'%s','delimiter',',');
    Parameter_liste = Parameter_liste{1};
    R_index = find(~cellfun(@isempty,regexp(Parameter_liste','R(2|3|4|5|6|7|8|9|10)')));
    Tau1_index = find(~cellfun(@isempty,regexp(Parameter_liste','Tau1')));
    R1_index = find(~cellfun(@isempty,regexp(Parameter_liste','R1')));
    Rser_index = find(~cellfun(@isempty,regexp(Parameter_liste','Rser')));
    Cser_index = find(~cellfun(@isempty,regexp(Parameter_liste','Cser')));
    HF_index = find(~cellfun(@isempty,regexp(Parameter_liste','Rser|Kskin|Lser')));
    %Zarc1_index =find(~cellfun(@isempty,regexp(Parameter_liste','R1|Tau1|Phi1')));
    TableCell = get(app.ParamTable,'Data');
    %TableCell = cell2struct(TableCell',{'Name','Fix','Value','Min','Max','Lim'});
    alt_R_fix = TableCell(R_index,2);
    alt_R_values = TableCell(R_index,3);
    alt_R1_fix = TableCell(R1_index,2);
    alt_R1_values = TableCell(R1_index,3);
    alt_Tau1_fix = TableCell(Tau1_index,2);
    alt_Tau1_values = TableCell(Tau1_index,3);
    alt_HF_fix = TableCell(HF_index,2);
    alt_HF_values = TableCell(HF_index,3);
    alt_Rser_fix = TableCell(Rser_index,2);
    alt_Rser_values = TableCell(Rser_index,3);
    TableCell(R_index,2) = {true};
    TableCell(R_index,3) = {0};
    TableCell(R1_index,2) = {false};
    TableCell(Tau1_index,3) = {0};
    TableCell(Tau1_index,2) = {true};
    TableCell(Rser_index,2) = {true};
    TableCell(HF_index,2) = {true};
    
    TableCell(Cser_index,2) = {false};
    set(app.ParamTable,'Data',TableCell);
    
    fcn_FitButton_Callback(app, event)
    fcn_FitButton_Callback(app, event)
    fcn_FitButton_Callback(app, event)
    % pause(2)
    TableCell = get(app.ParamTable,'Data');
    
    TableCell(R_index,2) = alt_R_fix;
    TableCell(R_index,3) = alt_R_values;
    TableCell(R1_index,2) = alt_R1_fix;
    TableCell(R1_index,3) = alt_R1_values;
    TableCell(Tau1_index,2) = alt_Tau1_fix;
    TableCell(Tau1_index,3) = alt_Tau1_values;
    TableCell(Rser_index,2) = alt_Rser_fix;
    TableCell(Rser_index,3) = alt_Rser_values;
    TableCell(HF_index,2) = alt_HF_fix;
    TableCell(HF_index,3) = alt_HF_values;
    % TableCell(Cser_index,2) = {true};
    
    set(app.ParamTable,'Data',TableCell);
    
    set(app.PunkteWegnehmenTextBox,'Value',PunkteWegVorher)
    fcn_PunkteWegnehmenButton_Callback(app, event)
end
end