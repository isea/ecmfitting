function fcn_TimeDomainPlotButton(app, event)
global DRT_GUI;
FarbenLaden;
figure
plot(DRT_GUI.Messdaten.relax.zeit,DRT_GUI.Messdaten.relax.spannung,'color',RWTHBlau,'displayname','Messung')
title('Relax')
grid on
hold on
if sum(strcmp(fieldnames(DRT_GUI.Fit),'Implementierung')) && ~isempty(DRT_GUI.Fit.Implementierung)
    R_RC_all = [];
    C_RC_all = [];
    R_ser_all = 0;
    C_ser_all = [];
    R_RC_ReFit_all = [];
    C_RC_ReFit_all = [];
    R_ser_ReFit_all = 0;
    C_ser_ReFit_all = [];for i = 1:numel(DRT_GUI.Fit.Implementierung.Info)-1
        args = cell(1,numel(DRT_GUI.Fit.Implementierung.Info{i}.inputs));
        argliste = '';
        for k = 1:numel(DRT_GUI.Fit.Implementierung.Info{i}.inputs)
            ParNr = find(strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),DRT_GUI.Fit.Implementierung.Table{i,k+4}));
            if isempty(ParNr)
                warning('Konnte Parameter aus Implementierung nicht finden. Implementierungsinfos werden reseted!')
                rmfield(DRT_GUI.Fit,'Implementierung');
                return
            end
            args{k} = DRT_GUI.Fit.Parameter(ParNr);
            argliste = [argliste 'args{' num2str(k) '},' ];
        end
        
        argliste = argliste(1:end-1);
        if strcmp(DRT_GUI.Fit.Implementierung.Table{i,1},'OCV_source')
            DeltaU = DRT_GUI.Messdaten.relax.spannung(end)-DRT_GUI.Messdaten.relax.spannung(1);
            Ladung = [0 cumsum(DRT_GUI.Messdaten.relax.strom(1:end-1) .* diff(DRT_GUI.Messdaten.relax.zeit))];
            C_OCV = Ladung(end) / DeltaU;
            if ismember('Funktionsname',fieldnames(DRT_GUI.Fit.Implementierung.Info{i}))
                [R_RC, C_RC, C_ser, R_ser, ~, ~] = RunLocalESBeFunction(DRT_GUI.Fit.Implementierung.Info{i}.Funktionsname,C_OCV);
            elseif ismember('f',fieldnames(DRT_GUI.Fit.Implementierung.Info{i}))
                [R_RC, C_RC, C_ser, R_ser, ~, ~] = DRT_GUI.Fit.Implementierung.Info{i}.f(C_OCV);
            end
        elseif isempty(argliste)
            if ismember('Funktionsname',fieldnames(DRT_GUI.Fit.Implementierung.Info{i}))
                [R_RC, C_RC, C_ser, R_ser, ~, ~] = RunLocalESBeFunction(DRT_GUI.Fit.Implementierung.Info{i}.Funktionsname);
            elseif ismember('f',fieldnames(DRT_GUI.Fit.Implementierung.Info{i}))
            elseif ismember('f',fieldnames(DRT_GUI.Fit.Implementierung.Info{i}))
                eval(['[R_RC, C_RC, C_ser, R_ser, ~, ~] = DRT_GUI.Fit.Implementierung.Info{i}.f;'])
            end
        else
            if ismember('Funktionsname',fieldnames(DRT_GUI.Fit.Implementierung.Info{i}))
                [R_RC, C_RC, C_ser, R_ser, ~, ~] = RunLocalESBeFunction(DRT_GUI.Fit.Implementierung.Info{i}.Funktionsname,args{:});
            elseif ismember('f',fieldnames(DRT_GUI.Fit.Implementierung.Info{i}))
                eval(['[R_RC, C_RC, C_ser, R_ser, ~, ~] = DRT_GUI.Fit.Implementierung.Info{i}.f(' argliste ');'])
            end
        end
        if strcmp(DRT_GUI.Fit.Implementierung.Table{i,2},'Re-Fit')
            args_ReFit = cell(1,numel(DRT_GUI.Fit.Implementierung.Info{i}.inputs));
            argliste_ReFit = '';
            for k = 1:numel(DRT_GUI.Fit.Implementierung.Info{i}.inputs)
                ParNr_ReFit = find(strcmp(DRT_GUI.Fit.Implementierung.OCV(:,1),['ReFit_' DRT_GUI.Fit.Implementierung.Table{i,k+4}]));
                args_ReFit{k} = DRT_GUI.Fit.Implementierung.OCV{ParNr_ReFit,2};
                argliste_ReFit = [argliste_ReFit 'args_ReFit{' num2str(k) '},' ];
            end
            argliste_ReFit = argliste_ReFit(1:end-1);
            if ismember('Funktionsname',fieldnames(DRT_GUI.Fit.Implementierung.Info{i}))
                [R_RC_ReFit, C_RC_ReFit, C_ser_ReFit, R_ser_ReFit,~ , ~] = RunLocalESBeFunction(DRT_GUI.Fit.Implementierung.Info{i}.Funktionsname,args_ReFit{:});
            elseif ismember('f',fieldnames(DRT_GUI.Fit.Implementierung.Info{i}))
                eval(['[R_RC_ReFit, C_RC_ReFit, C_ser_ReFit, R_ser_ReFit, ~, ~] = DRT_GUI.Fit.Implementierung.Info{i}.f(' argliste_ReFit ');'])
            end
        else
            R_RC_ReFit = R_RC;
            C_RC_ReFit = C_RC;
            C_ser_ReFit = C_ser;
            R_ser_ReFit = R_ser;
        end
        R_RC_all = [R_RC_all ;R_RC'];
        C_RC_all = [C_RC_all ;C_RC'];
        if ~isempty(R_ser) , R_ser_all = R_ser_all + R_ser; end
        if ~isempty(C_ser)
            if isempty(C_ser_all) || abs(C_ser_all) > 1e16
                C_ser_all = C_ser;
            else
                C_ser_all = (C_ser_all .* C_ser)./(C_ser_all + C_ser);
            end
        end
        
        R_RC_ReFit_all = [R_RC_ReFit_all ;R_RC_ReFit'];
        C_RC_ReFit_all = [C_RC_ReFit_all ;C_RC_ReFit'];
        if ~isempty(R_ser_ReFit) , R_ser_ReFit_all = R_ser_ReFit_all + R_ser_ReFit; end
        if ~isempty(C_ser_ReFit)
            if isempty(C_ser_ReFit_all) || abs(C_ser_ReFit_all) > 1e16
                C_ser_ReFit_all = C_ser_ReFit;
            else
                C_ser_ReFit_all = (C_ser_ReFit_all .* C_ser_ReFit)./(C_ser_ReFit_all + C_ser_ReFit);
            end
        end
    end
    stromindex = [1, 1+find(abs(diff(DRT_GUI.Messdaten.relax.strom))>0.01)]; %selbst
    if stromindex(end) < numel(DRT_GUI.Messdaten.relax.strom),
        stromindex(end+1) = numel(DRT_GUI.Messdaten.relax.strom);
    end
    strom = zeros(1,numel(stromindex));
    timediff = [ diff(DRT_GUI.Messdaten.relax.zeit), 0];%selbst
    for i = 1:numel(stromindex)-1
        if numel(stromindex(i):stromindex(i+1)-1) == 1
            strom(i) = DRT_GUI.Messdaten.relax.strom(stromindex(i));
        else
            strom(i) = sum(DRT_GUI.Messdaten.relax.strom(stromindex(i):stromindex(i+1)-1).*timediff(stromindex(i):stromindex(i+1)-1)./sum(timediff(stromindex(i):stromindex(i+1)-1)));
        end
    end
    zeit = DRT_GUI.Messdaten.relax.zeit(stromindex);
    
    if ~isempty(C_ser_all)
        ModellSpannung = DRT_GUI.Messdaten.relax.strom * R_ser_all + ...
        DRT_GUI.Messdaten.relax.spannung(1)+...
        [0, cumsum(diff(DRT_GUI.Messdaten.relax.zeit).*DRT_GUI.Messdaten.relax.strom(1:end-1)./C_ser_all)];%selbst
    else
        ModellSpannung = DRT_GUI.Messdaten.relax.strom * R_ser_all + ...
        DRT_GUI.Messdaten.relax.spannung(1);
    end
    U_RC = zeros(numel(R_RC_all),numel(DRT_GUI.Messdaten.relax.strom));
    for i = 1:numel(stromindex)-1
        if stromindex(i)==1
            U0 = zeros(size(R_RC_all));
        else
            U0 = U_RC(:,stromindex(i));
        end
        Umax = strom(i).*R_RC_all;
        t = DRT_GUI.Messdaten.relax.zeit(stromindex(i)+1:stromindex(i+1))-DRT_GUI.Messdaten.relax.zeit(stromindex(i));
        for k = 1:numel(R_RC_all)
            if abs(U0(k)-Umax(k))<=0.0001
                U_RC(k,stromindex(i)+1:stromindex(i+1)) = Umax(k);
            else
                U_RC(k,stromindex(i)+1:stromindex(i+1)) = U0(k)+(Umax(k)-U0(k)).*(1-exp(-t./(R_RC_all(k).*C_RC_all(k))));
            end
        end
    end
    ModellSpannung = ModellSpannung + sum(U_RC,1);
    
    ModellSpannung_ReFit = DRT_GUI.Messdaten.relax.strom * R_ser_ReFit_all + ...
        DRT_GUI.Messdaten.relax.spannung(1)+...
        [0, cumsum(diff(DRT_GUI.Messdaten.relax.zeit).*DRT_GUI.Messdaten.relax.strom(1:end-1)./C_ser_ReFit_all)];
    U_RC_ReFit = zeros(numel(R_RC_ReFit_all),numel(DRT_GUI.Messdaten.relax.strom));%selbst
    for i = 1:numel(stromindex)-1
        if stromindex(i)==1
            U0_ReFit = zeros(size(R_RC_ReFit_all));
        else
            U0_ReFit = U_RC_ReFit(:,stromindex(i));
        end
        Umax_ReFit = strom(i).*R_RC_ReFit_all;
        t = DRT_GUI.Messdaten.relax.zeit(stromindex(i)+1:stromindex(i+1))-DRT_GUI.Messdaten.relax.zeit(stromindex(i));
        for k = 1:numel(R_RC_ReFit_all)
            if abs(U0_ReFit(k)-Umax_ReFit(k))<=0.0001
                U_RC_ReFit(k,stromindex(i)+1:stromindex(i+1)) = Umax_ReFit(k);
            else
                U_RC_ReFit(k,stromindex(i)+1:stromindex(i+1)) = U0_ReFit(k)+(Umax_ReFit(k)-U0_ReFit(k)).*(1-exp(-t./(R_RC_ReFit_all(k).*C_RC_ReFit_all(k))));
            end
        end
    end
    ModellSpannung_ReFit = ModellSpannung_ReFit + sum(U_RC_ReFit,1);
    
    
    plot(DRT_GUI.Messdaten.relax.zeit,ModellSpannung,'Color',RWTHTuerkis,'DisplayName','EISModell');
    plot(DRT_GUI.Messdaten.relax.zeit,ModellSpannung_ReFit,'Color',RWTHRot,'DisplayName','TDMModell');
    l=legend('show');
    set(l,'Location','SouthEast')
    
end

figure
plot(DRT_GUI.Messdaten.eis.zeit,DRT_GUI.Messdaten.eis.spannung)
grid on
title('EIS')
if sum(strcmp(fieldnames(DRT_GUI.Messdaten),'vor_relax')) && ~isempty(DRT_GUI.Messdaten.vor_relax)&& ~isempty(DRT_GUI.Messdaten.vor_relax.zeit)
    figure
    plot(DRT_GUI.Messdaten.vor_relax.zeit,DRT_GUI.Messdaten.vor_relax.spannung)
    grid on;hold on
    title('Messungen vor der Relaxation')
    if sum(strcmp(fieldnames(DRT_GUI.Fit),'Implementierung')) && ~isempty(DRT_GUI.Fit.Implementierung)
        stromindex = [1 1+find(abs(diff(DRT_GUI.Messdaten.vor_relax.strom))>0.0005)];
        if stromindex(end) < numel(DRT_GUI.Messdaten.vor_relax.strom),
            stromindex(end+1) = numel(DRT_GUI.Messdaten.vor_relax.strom);
        end
        strom = zeros(1,numel(stromindex));
        timediff = [ diff(DRT_GUI.Messdaten.vor_relax.zeit) 0];
        for i = 1:numel(stromindex)-1
            if numel(stromindex(i):stromindex(i+1)-1) == 1
                strom(i) = DRT_GUI.Messdaten.vor_relax.strom(stromindex(i));
            else
                strom(i) = sum(DRT_GUI.Messdaten.vor_relax.strom(stromindex(i):stromindex(i+1)-1).*timediff(stromindex(i):stromindex(i+1)-1)./sum(timediff(stromindex(i):stromindex(i+1)-1)));
            end
        end
        zeit = DRT_GUI.Messdaten.vor_relax.zeit(stromindex);
        ModellSpannung = DRT_GUI.Messdaten.vor_relax.strom * R_ser_all + ...
            DRT_GUI.Messdaten.vor_relax.spannung(1)+...
            [0 cumsum(diff(DRT_GUI.Messdaten.vor_relax.zeit).*DRT_GUI.Messdaten.vor_relax.strom(1:end-1)./C_ser_all)];
        U_RC = zeros(numel(R_RC_all),numel(DRT_GUI.Messdaten.vor_relax.strom));
        for i = 1:numel(stromindex)-1
            if stromindex(i)==1
                U0 = zeros(size(R_RC_all));
            else
                U0 = U_RC(:,stromindex(i));
            end
            Umax = strom(i).*R_RC_all;
            t = DRT_GUI.Messdaten.vor_relax.zeit(stromindex(i)+1:stromindex(i+1))-DRT_GUI.Messdaten.vor_relax.zeit(stromindex(i));
            for k = 1:numel(R_RC_all)
                if abs(U0(k)-Umax(k))<=0.0001
                    U_RC(k,stromindex(i)+1:stromindex(i+1)) = Umax(k);
                else
                    U_RC(k,stromindex(i)+1:stromindex(i+1)) = U0(k)+(Umax(k)-U0(k)).*(1-exp(-t./(R_RC_all(k).*C_RC_all(k))));
                end
            end
            
        end
        
        ModellSpannung = ModellSpannung + sum(U_RC,1);
        ModellSpannung = ModellSpannung + DRT_GUI.Messdaten.vor_relax.spannung(end) - ModellSpannung(end);
        plot(DRT_GUI.Messdaten.vor_relax.zeit,ModellSpannung,'r');
    end
    
end

if sum(strcmp(fieldnames(DRT_GUI.Messdaten),'nach_eis')) && ~isempty(DRT_GUI.Messdaten.nach_eis) && ~isempty(DRT_GUI.Messdaten.nach_eis.zeit)
    p_index = 0;
    p_i=0;
    ind2 = p_index(end);
    ind1 = find(~DRT_GUI.Messdaten.nach_eis.strom(ind2+1:end)==0,1,'first')-1;
    ind2 = numel(DRT_GUI.Messdaten.nach_eis.strom);
    
    start = find(DRT_GUI.Messdaten.nach_eis.zeit>DRT_GUI.Messdaten.nach_eis.zeit(ind1)-30,1,'first');
    if isempty(start), start = 1;end
    p_index =  start:ind2;
    p_i=p_i+1;
    
    
    
    figure
    plot(DRT_GUI.Messdaten.nach_eis.zeit(p_index)-DRT_GUI.Messdaten.nach_eis.zeit(p_index(1)),DRT_GUI.Messdaten.nach_eis.spannung(p_index),'color',RWTHBlau,'LineWidth',1,'DisplayName','Messung')
    grid on;hold on
    title(['Messungen nach der EIS-Messung'])
    if sum(strcmp(fieldnames(DRT_GUI.Fit),'Implementierung')) && ~isempty(DRT_GUI.Fit.Implementierung)
        stromindex = [1 1+find(abs(diff(DRT_GUI.Messdaten.nach_eis.strom(p_index)))>0.0005)];
        if stromindex(end) < numel(DRT_GUI.Messdaten.nach_eis.strom(p_index)),
            stromindex(end+1) = numel(DRT_GUI.Messdaten.nach_eis.strom(p_index));
        end
        strom = zeros(1,numel(stromindex));
        timediff = [ diff(DRT_GUI.Messdaten.nach_eis.zeit(p_index)) 0];
        for i = 1:numel(stromindex)-1
            if numel(stromindex(i):stromindex(i+1)-1) == 1
                strom(i) = DRT_GUI.Messdaten.nach_eis.strom(p_index(stromindex(i)));
            else
                strom(i) = sum(DRT_GUI.Messdaten.nach_eis.strom(p_index(stromindex(i):stromindex(i+1)-1)).*timediff(stromindex(i):stromindex(i+1)-1)./sum(timediff(stromindex(i):stromindex(i+1)-1)));
            end
        end
        zeit = DRT_GUI.Messdaten.nach_eis.zeit(p_index(stromindex));
        ModellSpannung = DRT_GUI.Messdaten.nach_eis.strom(p_index) * R_ser_all + ...
            DRT_GUI.Messdaten.nach_eis.spannung(p_index(1))+...
            [0 cumsum(diff(DRT_GUI.Messdaten.nach_eis.zeit(p_index)).*DRT_GUI.Messdaten.nach_eis.strom(p_index(1:end-1))./C_ser_all)];
        U_RC = zeros(numel(R_RC_all),numel(DRT_GUI.Messdaten.nach_eis.strom(p_index)));
        for i = 1:numel(stromindex)-1
            if stromindex(i)==1
                U0 = zeros(size(R_RC_all));
            else
                U0 = U_RC(:,stromindex(i));
            end
            Umax = strom(i).*R_RC_all;
            t = DRT_GUI.Messdaten.nach_eis.zeit(p_index(stromindex(i)+1:stromindex(i+1)))-DRT_GUI.Messdaten.nach_eis.zeit(p_index(stromindex(i)));
            for k = 1:numel(R_RC_all)
                if abs(U0(k)-Umax(k))<=0.0001
                    U_RC(k,stromindex(i)+1:stromindex(i+1)) = Umax(k);
                else
                    U_RC(k,stromindex(i)+1:stromindex(i+1)) = U0(k)+(Umax(k)-U0(k)).*(1-exp(-t./(R_RC_all(k).*C_RC_all(k))));
                end
            end
            
        end
        ModellSpannung = ModellSpannung + sum(U_RC,1);
        ModellSpannung = ModellSpannung + DRT_GUI.Messdaten.nach_eis.spannung(p_index(1)) - ModellSpannung(1);
        plot(DRT_GUI.Messdaten.nach_eis.zeit(p_index)-DRT_GUI.Messdaten.nach_eis.zeit(p_index(1)),ModellSpannung,'color',RWTHTuerkis,'LineWidth',1,'DisplayName','EISFit');
        
        
        ModellSpannung_ReFit = DRT_GUI.Messdaten.nach_eis.strom(p_index) * R_ser_ReFit_all + ...
            DRT_GUI.Messdaten.nach_eis.spannung(p_index(1))+...
            [0 cumsum(diff(DRT_GUI.Messdaten.nach_eis.zeit(p_index)).*DRT_GUI.Messdaten.nach_eis.strom(p_index(1:end-1))./C_ser_ReFit_all)];
        U_RC_ReFit = zeros(numel(R_RC_ReFit_all),numel(DRT_GUI.Messdaten.nach_eis.strom(p_index)));
        for i = 1:numel(stromindex)-1
            if stromindex(i)==1
                U0_ReFit = zeros(size(R_RC_ReFit_all));
            else
                U0_ReFit = U_RC_ReFit(:,stromindex(i));
            end
            Umax_ReFit = strom(i).*R_RC_ReFit_all;
            t = DRT_GUI.Messdaten.nach_eis.zeit(p_index(stromindex(i)+1:stromindex(i+1)))-DRT_GUI.Messdaten.nach_eis.zeit(p_index(stromindex(i)));
            for k = 1:numel(R_RC_ReFit_all)
                if abs(U0_ReFit(k)-Umax_ReFit(k))<=0.0001
                    U_RC_ReFit(k,stromindex(i)+1:stromindex(i+1)) = Umax_ReFit(k);
                else
                    U_RC_ReFit(k,stromindex(i)+1:stromindex(i+1)) = U0_ReFit(k)+(Umax_ReFit(k)-U0_ReFit(k)).*(1-exp(-t./(R_RC_ReFit_all(k).*C_RC_ReFit_all(k))));
                end
            end
            
        end
        ModellSpannung_ReFit = ModellSpannung_ReFit + sum(U_RC_ReFit,1);
        ModellSpannung_ReFit = ModellSpannung_ReFit + DRT_GUI.Messdaten.nach_eis.spannung(p_index(1)) - ModellSpannung_ReFit(1);
        plot(DRT_GUI.Messdaten.nach_eis.zeit(p_index)-DRT_GUI.Messdaten.nach_eis.zeit(p_index(1)),ModellSpannung_ReFit,'color',RWTHRot,'LineWidth',1,'DisplayName','TDMFit');
        legend('show')
    end
    
    
    
end

if sum(strcmp(fieldnames(DRT_GUI.Messdaten),'nach_eis')) && ~isempty(DRT_GUI.Messdaten.nach_eis) && ~isempty(DRT_GUI.Messdaten.nach_eis.zeit)
    p_index = 0;
    p_i=0;
    while true
        ind2 = p_index(end);
        while true
            ind1 = ind2+find(DRT_GUI.Messdaten.nach_eis.strom(ind2+1:end)==0,1,'first');
            ind2 = ind1+find(~DRT_GUI.Messdaten.nach_eis.strom(ind1:end)==0,1,'first')-2;
            if isempty(ind2),break;end
            if ind1>1 && (DRT_GUI.Messdaten.nach_eis.zeit(ind2)-DRT_GUI.Messdaten.nach_eis.zeit(ind1))>655
                if p_index(end)==0
                    p_index = [1 find(~DRT_GUI.Messdaten.nach_eis.strom(1:ind2)==0,1,'first')-1];
                end
                start = p_index(1)+find(DRT_GUI.Messdaten.nach_eis.zeit(p_index(1)+1:ind2)>DRT_GUI.Messdaten.nach_eis.zeit(p_index(end)+1)-30,1,'first');
                if isempty(start), start = p_index(end)+1;end
                p_index =  start:ind2;
                p_i=p_i+1;
                break
            end
        end
        if isempty(ind2),break;end
        
        figure
        plot(DRT_GUI.Messdaten.nach_eis.zeit(p_index)-DRT_GUI.Messdaten.nach_eis.zeit(p_index(1)),DRT_GUI.Messdaten.nach_eis.spannung(p_index),'color',RWTHBlau,'LineWidth',1,'DisplayName','Messung')
        grid on;hold on
        title(['Messungen nach der EIS-Messung Teil' num2str(p_i)])
        if sum(strcmp(fieldnames(DRT_GUI.Fit),'Implementierung')) && ~isempty(DRT_GUI.Fit.Implementierung)
            stromindex = [1 1+find(abs(diff(DRT_GUI.Messdaten.nach_eis.strom(p_index)))>0.0005)];
            if stromindex(end) < numel(DRT_GUI.Messdaten.nach_eis.strom(p_index)),
                stromindex(end+1) = numel(DRT_GUI.Messdaten.nach_eis.strom(p_index));
            end
            strom = zeros(1,numel(stromindex));
            timediff = [ diff(DRT_GUI.Messdaten.nach_eis.zeit(p_index)) 0];
            for i = 1:numel(stromindex)-1
                if numel(stromindex(i):stromindex(i+1)-1) == 1
                    strom(i) = DRT_GUI.Messdaten.nach_eis.strom(p_index(stromindex(i)));
                else
                    strom(i) = sum(DRT_GUI.Messdaten.nach_eis.strom(p_index(stromindex(i):stromindex(i+1)-1)).*timediff(stromindex(i):stromindex(i+1)-1)./sum(timediff(stromindex(i):stromindex(i+1)-1)));
                end
            end
            zeit = DRT_GUI.Messdaten.nach_eis.zeit(p_index(stromindex));
            ModellSpannung = DRT_GUI.Messdaten.nach_eis.strom(p_index) * R_ser_all + ...
                DRT_GUI.Messdaten.nach_eis.spannung(p_index(1))+...
                [0 cumsum(diff(DRT_GUI.Messdaten.nach_eis.zeit(p_index)).*DRT_GUI.Messdaten.nach_eis.strom(p_index(1:end-1))./C_ser_all)];
            U_RC = zeros(numel(R_RC_all),numel(DRT_GUI.Messdaten.nach_eis.strom(p_index)));
            for i = 1:numel(stromindex)-1
                if stromindex(i)==1
                    U0 = zeros(size(R_RC_all));
                else
                    U0 = U_RC(:,stromindex(i));
                end
                Umax = strom(i).*R_RC_all;
                t = DRT_GUI.Messdaten.nach_eis.zeit(p_index(stromindex(i)+1:stromindex(i+1)))-DRT_GUI.Messdaten.nach_eis.zeit(p_index(stromindex(i)));
                for k = 1:numel(R_RC_all)
                    if abs(U0(k)-Umax(k))<=0.0001
                        U_RC(k,stromindex(i)+1:stromindex(i+1)) = Umax(k);
                    else
                        U_RC(k,stromindex(i)+1:stromindex(i+1)) = U0(k)+(Umax(k)-U0(k)).*(1-exp(-t./(R_RC_all(k).*C_RC_all(k))));
                    end
                end
                
            end
            ModellSpannung = ModellSpannung + sum(U_RC,1);
            ModellSpannung = ModellSpannung + DRT_GUI.Messdaten.nach_eis.spannung(p_index(1)) - ModellSpannung(1);
            plot(DRT_GUI.Messdaten.nach_eis.zeit(p_index)-DRT_GUI.Messdaten.nach_eis.zeit(p_index(1)),ModellSpannung,'color',RWTHTuerkis,'LineWidth',1,'DisplayName','EISFit');
            
            
            ModellSpannung_ReFit = DRT_GUI.Messdaten.nach_eis.strom(p_index) * R_ser_ReFit_all + ...
                DRT_GUI.Messdaten.nach_eis.spannung(p_index(1))+...
                [0 cumsum(diff(DRT_GUI.Messdaten.nach_eis.zeit(p_index)).*DRT_GUI.Messdaten.nach_eis.strom(p_index(1:end-1))./C_ser_ReFit_all)];
            U_RC_ReFit = zeros(numel(R_RC_ReFit_all),numel(DRT_GUI.Messdaten.nach_eis.strom(p_index)));
            for i = 1:numel(stromindex)-1
                if stromindex(i)==1
                    U0_ReFit = zeros(size(R_RC_ReFit_all));
                else
                    U0_ReFit = U_RC_ReFit(:,stromindex(i));
                end
                Umax_ReFit = strom(i).*R_RC_ReFit_all;
                t = DRT_GUI.Messdaten.nach_eis.zeit(p_index(stromindex(i)+1:stromindex(i+1)))-DRT_GUI.Messdaten.nach_eis.zeit(p_index(stromindex(i)));
                for k = 1:numel(R_RC_ReFit_all)
                    if abs(U0_ReFit(k)-Umax_ReFit(k))<=0.0001
                        U_RC_ReFit(k,stromindex(i)+1:stromindex(i+1)) = Umax_ReFit(k);
                    else
                        U_RC_ReFit(k,stromindex(i)+1:stromindex(i+1)) = U0_ReFit(k)+(Umax_ReFit(k)-U0_ReFit(k)).*(1-exp(-t./(R_RC_ReFit_all(k).*C_RC_ReFit_all(k))));
                    end
                end
                
            end
            ModellSpannung_ReFit = ModellSpannung_ReFit + sum(U_RC_ReFit,1);
            ModellSpannung_ReFit = ModellSpannung_ReFit + DRT_GUI.Messdaten.nach_eis.spannung(p_index(1)) - ModellSpannung_ReFit(1);
            plot(DRT_GUI.Messdaten.nach_eis.zeit(p_index)-DRT_GUI.Messdaten.nach_eis.zeit(p_index(1)),ModellSpannung_ReFit,'color',RWTHRot,'LineWidth',1,'DisplayName','TDMFit');
            legend('show')
        end
    end
end
end