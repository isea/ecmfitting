function fcn_Einlesen(app, event)
global DRT_GUI;
global Pattern
DC_Current = 0 ;
Index100percent = -1;
alter_pfad = pwd;
[filename, pathname] = uigetfile({'*.mat' '�Eis oder Eiskarte'; '*.txt;*.csv' 'Zahner';'*.log' 'EISlog';'*.xls;*.xlsx' 'Excel'}, 'Die Daten ausw�hlen.', 'MultiSelect','on');
cd(alter_pfad);
if isempty(filename) || (~iscell(filename) && sum(filename == 0) )
    error('keine Daten eingelesen!!!');
    return
end
if ~iscell(filename)
    files{1} = filename;
else
    files = filename;
end
for i = 1:numel(files)
    filename = files{i};
    %     set(app.fileName_text,'Value',strcat(pathname,filename),'ForegroundColor','k');
    
    DRT_GUI = [];
    DRT_GUI.Testparameter.fileName = strcat(pathname,filename);
    
    set(app.PunkteWegnehmenTextBox,'Value','');
    set(app.ModellFormelTextBox,'Value','');
    if strcmp(filename(end-3:end),'.txt') || strcmp(filename(end-3:end),'.csv')
        fid = fopen(strcat(pathname,filename));
        C = textscan(fid,'%s');
        C = C{1};
        fclose(fid);
        SpaltenAnzahl = str2double(C(find(strcmp(C,'Spalten:'),1)+1));
        if isempty(SpaltenAnzahl),
            SpaltenAnzahl = str2double(C(find(strcmp(C,'Columns:'),1)+1));
        end
        ZeilenAnzahl = str2double(C(find(strcmp(C,'Zeilen:'),1)+1));
        if isempty(ZeilenAnzahl),
            ZeilenAnzahl = str2double(C(find(strcmp(C,'Lines:'),1)+1));
        end
        if ~isempty(SpaltenAnzahl) && ~isempty(ZeilenAnzahl)
            StartIndex = find(strcmp(C,'Nummer'),1);
            if isempty(StartIndex),
                StartIndex = find(strcmp(C,'Number'),1);
            end
            StartIndex = StartIndex + find(strcmp(C(StartIndex+1:end),'1'),1);
            Daten = reshape(cellfun(@str2double,C(StartIndex:end)), SpaltenAnzahl+1,numel(C(StartIndex:end))/(SpaltenAnzahl+1));
            messung.diga.daten.ActFreq = Daten(2,:);
            messung.diga.daten.Zreal1 = Daten(3,:);
            messung.diga.daten.Zimg1 = Daten(4,:);
        else
            % kein Zahner-Format
            fid = fopen(strcat(pathname,filename));
            C = textscan(fid,'%s %s %s %s %s %s %s %s %s %s %s','delimiter',';');
            fclose(fid);
            for spaltenIndex = 1:numel(C)
                if isempty(C{spaltenIndex}{1}) , break,end
                messung.diga.daten.(C{spaltenIndex}{1}) = str2double(strrep(C{spaltenIndex}(3:end),',','.'))';
            end
        end
    elseif ( strcmp(filename(end-3:end),'.xls') || strcmp(filename(end-4:end),'.xlsx'))
        
        Daten=xlsread(strcat(pathname,filename));
        if sum((Daten(1:4,1)' ~= 1:4))==0 ,
            messung.diga.daten.ActFreq = Daten(:,2);
            messung.diga.daten.Zreal1 = Daten(:,3);
            messung.diga.daten.Zimg1 = -Daten(:,4);
        else
            messung.diga.daten.ActFreq = Daten(:,1);
            messung.diga.daten.Zreal1 = Daten(:,2);
            messung.diga.daten.Zimg1 = Daten(:,3);
        end
    elseif strcmp(filename(end-3:end),'.log')
        messung.eis = geteis10(strcat(pathname,filename));
    elseif strcmp(filename(end-3:end),'.mat')
        messung=load(strcat(pathname,filename));
    elseif strcmp(filename(end-3:end),'.mpt')
        fid = fopen(strcat(pathname,filename),'r');
        delimiter= '\t';
        startRow = 58;
        endRow = 141;
        formatSpec = '%f%f%f%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%*s%f%f%*s%*s%f%f%*s%*s%*s%*s%[^\n\r]';
        
        textscan(fid, '%[^\n\r]', startRow(1)-1, 'ReturnOnError', false);
        dataArray = textscan(fid, formatSpec, endRow(1)-startRow(1)+1, 'Delimiter', delimiter, 'ReturnOnError', false);
        for block=2:length(startRow)
            frewind(fid);
            textscan(fid, '%[^\n\r]', startRow(block)-1, 'ReturnOnError', false);
            dataArrayBlock = textscan(fid, formatSpec, endRow(block)-startRow(block)+1, 'Delimiter', delimiter, 'ReturnOnError', false);
            for col=1:length(dataArray)
                dataArray{col} = [dataArray{col};dataArrayBlock{col}];
            end
        end
        fclose(fid);
        Daten=[dataArray{1:end-1}];
        list={'Zelle', 'Kathode', 'Anode'}; %Auswahlliste
        [Selection,~]=listdlg('ListString',list,'ListSize',[160,90],'PromptString','Welches Spektrum?','SelectionMode','single');
        if Selection == 1
            messung.diga.daten.ActFreq = Daten(:,1).';
            messung.diga.daten.Zreal1 = Daten(:,6).';
            messung.diga.daten.Zimg1 = -Daten(:,7).';
            Impedanzart = 'Zelle';
        elseif Selection ==2
            messung.diga.daten.ActFreq = Daten(:,1).';
            messung.diga.daten.Zreal1 = Daten(:,2).';
            messung.diga.daten.Zimg1 = -Daten(:,3).';
            Impedanzart = 'Kathode';
        elseif Selection ==3
            messung.diga.daten.ActFreq = Daten(:,1).';
            messung.diga.daten.Zreal1 = Daten(:,4).';
            messung.diga.daten.Zimg1 = -Daten(:,5).';
            Impedanzart = 'Anode';
        end
    end
    % FittingGUI = Extrakt;
    
    %%% die Batteriezustand durch die Pathname und Filename bekommen, (BatterieNr, SOC, Temperatur)
    % suchungsname initialisieren
    getTemp=0;
    datefound = 0;
    [DString DStringOrig] = FindDateInStr(filename);
    if ~isempty(DString)
        datefound = 1;
        set(app.DatumTextBox,'Value',datestr(datenum(DString)))
        fcn_DatumTextBox_Callback(app,event)
    end
    
    Batteriefound = 0;
    for i = 1:numel(Pattern.Batterie)
        [k,l] = regexp(filename,Pattern.Batterie{i});
        if isempty(k) && Pattern.Batterie{i}(1)=='_'
            [k,l] = regexp(filename(1:numel(Pattern.Batterie{i})),Pattern.Batterie{i}(2:end));
        end
        if k > 0
            set(app.BatterieTextBox,'Value',filename(k:l));
            fcn_BatterieTextBox_Callback(app, event);
            Batteriefound = 1;
            break
        end
    end
    if Batteriefound==0;
        while true
            Batteriename= inputdlg('Wie hei�t die Batterie?','Batteriename',[1],{'dummy'});
            if  isempty(Batteriename) ||    strcmp(Batteriename,'') ,
                return
            else
                Batteriename=Batteriename{1};
                break
            end
        end
        set(app.BatterieTextBox,'Value',Batteriename);
        fcn_BatterieTextBox_Callback(app, event);
        Batteriefound = 1;
    end
    
    Temperaturfound = 0;
    for i = 1:numel(Pattern.Temperatur)
        [k,l] = regexp(filename,Pattern.Temperatur{i});
        if k > 0
            if k==1
                Temperatur = regexprep(regexprep(regexprep(regexprep(regexprep(regexprep(regexprep(filename(k:l),'low',''),'grad',''),'Grad',''),'m','-'),'_',''),'T',''),'Messung',''); %Erg�nzung: Eine Stelle zu wenig betrachtet: Minus wurde nicht erkannt
            else
                Temperatur = regexprep(regexprep(regexprep(regexprep(regexprep(regexprep(regexprep(filename(k:l),'low',''),'grad',''),'Grad',''),'m','-'),'_',''),'T',''),'Messung',''); %Erg�nzung: Eine Stelle zu wenig betrachtet: Minus wurde nicht erkannt
            end
            set(app.TemperaturTextBox,'Value',Temperatur);
            fcn_TemperaturTextBox_Callback(app, event);
            Temperaturfound = 1;
            break
        end
    end
    
    Zustandfound = 0;
    for i = 1:numel(Pattern.Zustand)
        [k,l] = regexp(filename,Pattern.Zustand{i});
        if k > 0
            Zustandfound = 1;
            Zustand = filename(k:l);
            if strcmp(Zustand(end-3:end) , '_psa') && strcmp('=',Zustand(1)) && size(Zustand,2)==7
                Zustand = ['CU' Zustand(2:3)];
            end
            set(app.ZustandTextBox,'Value',Zustand);
            fcn_ZustandTextBox_Callback(app, event);
            break
        end
    end
    if ~Zustandfound
        if strcmp(filename(end-3:end),'.mpt');
            Zustandfound= 1;
            Zustand = Impedanzart;
            set(app.ZustandTextBox,'Value',Zustand);
            fcn_ZustandTextBox_Callback(app, event);
        else
            Zustand = 'default';
            set(app.ZustandTextBox,'Value',Zustand);
            fcn_ZustandTextBox_Callback(app, event);
        end
    end
    
    SOCfound = 0;
    for i = 1:numel(Pattern.SOC)
        [k,l] = regexp(filename,Pattern.SOC{i});
        if k > 0
            SOCfound = 1;
            SOC = regexprep(regexprep(regexprep(filename(k:l),'SOC',''),'m','-'),'_','');
            set(app.SOCTextBox,'Value',SOC);
            fcn_SOCTextBox_Callback(app, event);
            break
        end
    end
    if sum(ismember(fieldnames(messung),'diga'))
        if sum(~diff(messung.diga.daten.ActFreq)~=0)
            index = find(messung.diga.daten.ActFreq>0 & [1, diff(messung.diga.daten.ActFreq)~=0]);
        else
            index = 1:numel(messung.diga.daten.ActFreq);
        end
        [k,l] = regexp(filename,'EIS\d\d\d\d\d');
        if ~isempty(k)
            eisnumber = str2double(filename(k+3:l));
        else
            eisnumber=0;
        end
        ind2 = find(diff(messung.diga.daten.ActFreq(index))>0);
        if ~isempty(ind2)
            ind2 = [1 ind2+1 numel(index)+1];
            if eisnumber>0 && eisnumber < numel(ind2)
                index = index(ind2(eisnumber):(ind2(eisnumber+1)-1));
            else
                eisnumber = inputdlg(['Which spectrum should be imported? ( 1 to '  num2str(numel(ind2)-1) ' )'],'number of spectrum',1,{'1'});
                if isempty(eisnumber), return , end
                eisnumber = str2num(cell2mat(eisnumber));
%                 index = ndex(ind2(eisnumber):(ind2(eisnumber+1)-1));
                index = eisnumber;
            end
        end
        DRT_GUI.Messdaten.frequenz = messung.diga.daten.ActFreq(index)';
        DRT_GUI.Messdaten.omega = 2*pi*messung.diga.daten.ActFreq(index)';
        DRT_GUI.Messdaten.tau = 1./DRT_GUI.Messdaten.omega;
        DRT_GUI.Messdaten.Zreal = messung.diga.daten.Zreal1(index)';
        DRT_GUI.Messdaten.Zimg = messung.diga.daten.Zimg1(index)';
        if ~isempty(find(strcmp(fieldnames(DRT_GUI.Messdaten),'U1'), 1))
            DRT_GUI.Messdaten.U1 = messung.diga.daten.U1(index)';
        end
    elseif sum(ismember(fieldnames(messung),'data')) && sum(ismember(fieldnames(messung.data),'values')) && sum(ismember(fieldnames(messung.data),'header'))
        f_index = find(strcmp(messung.data.header,'f [Hz]'));
        Zreal_index = find(strcmp(messung.data.header,'Zreal [Ohm]'));
        Zimag_index = find(strcmp(messung.data.header,'-Zimag [Ohm]'));
        
        DRT_GUI.Messdaten.frequenz = messung.data.values(:,f_index);
        DRT_GUI.Messdaten.omega = 2*pi*messung.data.values(:,f_index);
        DRT_GUI.Messdaten.tau = 1./DRT_GUI.Messdaten.omega;
        DRT_GUI.Messdaten.Zreal =messung.data.values(:,Zreal_index);
        DRT_GUI.Messdaten.Zimg = messung.data.values(:,Zimag_index);
    elseif sum(ismember(fieldnames(messung),'eis'))
        if size(messung.eis.batdef.daten,1)>1
            BatNumber = listdlg('PromptString','Select a Battery:','SelectionMode','single','ListString',messung.eis.batdef.daten(:,1));
            set(app.BatterieTextBox,'Value',messung.eis.batdef.daten{BatNumber,1})
            fcn_BatterieTextBox_Callback(app, event);
            Batteriefound = 1;
        else
            BatNumber = 1;
        end
        
        ChannelNumber = str2num(messung.eis.batdef.daten{BatNumber,find(strcmp(messung.eis.batdef.spalten_namen,'connected voltage channels'))});
        if numel(messung.eis.SpektrumNamen) > 1
            SpecNumber = listdlg('PromptString','Select a spectrum:','SelectionMode','single','ListString',messung.eis.SpektrumNamen);
            SpecString = messung.eis.SpektrumNamen{SpecNumber};
            SOCfound = 0;
            for i = 1:numel(Pattern.SOC)
                [k,l] = regexp(SpecString,Pattern.SOC{i});
                if k > 0
                    SOCfound = 1;
                    SOC = regexprep(regexprep(regexprep(regexprep(SpecString(k:l),'SOC',''),'SO',''),'m','-'),'_','');
                    set(app.SOCTextBox,'Value',SOC);
                    fcn_SOCTextBox_Callback(app, event);
                    break
                end
            end
            
        elseif  numel(messung.eis.SpektrumNamen)==1
            SpecNumber = 1;
            if ~isempty(messung.eis.SpektrumNamen)
                SpecString = messung.eis.SpektrumNamen{SpecNumber};
            else
                SpecString = '';
            end
            SOCfound = 0;
        else
            msgbox('kein Spektrum vorhanden');
            return
        end
        if ~datefound
            [DString DStringOrig]  = FindDateInStr(messung.eis.(SpecString){1}.datum);
            if ~isempty(DString)
                datefound = 1;
                set(app.DatumTextBox,'Value',datestr(datenum(DString)))
                fcn_DatumTextBox_Callback(app, event);
            end
        end
        freqindex = find(strcmp(messung.eis.(SpecString){1}.spalten_namen,'frequency'));
        nonzeros = find(messung.eis.(SpecString){1}.daten(:,freqindex)~=0 & ~isnan(messung.eis.(SpecString){1}.daten(:,freqindex)));
        DRT_GUI.Messdaten.frequenz = messung.eis.(SpecString){1}.daten(nonzeros,freqindex);
        
        DRT_GUI.Messdaten.omega = 2*pi*DRT_GUI.Messdaten.frequenz;
        DRT_GUI.Messdaten.tau = 1./DRT_GUI.Messdaten.omega;
        realindex = find(strcmp(messung.eis.(SpecString){1}.spalten_namen,['U' num2str(ChannelNumber) 'R']));
        if isempty (realindex)
            realindex = find(strcmp(messung.eis.(SpecString){1}.spalten_namen,['Re.' num2str(ChannelNumber-1)]));
        end
        DRT_GUI.Messdaten.Zreal = messung.eis.(SpecString){1}.daten(nonzeros,realindex);
        imagindex = find(strcmp(messung.eis.(SpecString){1}.spalten_namen,['U' num2str(ChannelNumber) 'I']));
        if isempty(imagindex)
            imagindex = find(strcmp(messung.eis.(SpecString){1}.spalten_namen,['Im.' num2str(ChannelNumber-1)]));
        end
        DRT_GUI.Messdaten.Zimg = messung.eis.(SpecString){1}.daten(nonzeros,imagindex);
    else
        error('Dateiformat nicht bekannt!')
        return
    end
    if ~datefound
        while true
            DString = cell2mat(inputdlg('Bitte Testdatum angeben! (yyyy-mm-dd)','Datum',[1],{datestr(now,'yyyy-mm-dd')}));
            if  isempty(DString) , return,end
            if isdate(DString)
                set(app.DatumTextBox,'Value',datestr(datenum(DString,'yymmdd')))
                datefound = 1;
                break,
            end
        end
    end
    if ~Temperaturfound
        while true            
            Temperatur = str2double(inputdlg('Welche Testtemperatur?','Temperatur',[1],{'25'}));            
            choice = num2str(Temperatur);
            if  strcmp(choice,'') , return,end
            if ~strcmp(choice,'NaN'), break, end;
        end
        set(app.TemperaturTextBox,'Value',choice)
        fcn_TemperaturTextBox_Callback(app, event);
        Temperaturfound = 1;
    end
    if SOCfound == 0
        if ~exist('eisnumber')
            [k,l] = regexp(filename,'EIS\d\d\d\d\d');
            eisnumber = str2double(filename(k+3:l));
        end
        masterfound = 0;
        master_file = dir([pathname '*' DStringOrig '*.mat']);
        %        master_file = dir([pathname '*.mat']); % selbst
        for Master_i = 1:numel(Pattern.MasterProgram)
            for j = 1:numel(master_file)
                if strfind(master_file(j).name,DRT_GUI.Testparameter.Batterie) & strfind(master_file(j).name,Pattern.MasterProgram{Master_i}) & strfind(master_file(j).name,'Format01') & isempty(strfind(master_file(j).name,'EIS00'))
                    
                    master = load([pathname master_file(j).name]);
                    Master_Pattern = Pattern.MasterProgram{Master_i};
                    masterfound = 1;
                    break
                end
                
            end
            if masterfound, break, end
        end
        if masterfound == 0 && strcmp('Ja',questdlg('Es wurde keine Digatron-Masterdatei gefunden. Wollen Sie selbst danach suchen?', ...
                'Masterfile', ...
                'Ja','Nein','Nein'))
            
            alter_pfad = pwd;
            [filename, pathname] = uigetfile({'*.mat' 'Matlab'}, 'MasterFile w�hlen.', 'MultiSelect','off');
            cd(alter_pfad);
            if isempty(filename) || (~iscell(filename) && sum(filename == 0) )
                masterfound = 0;
            else
                master = load([pathname filename]);
                masterfound = 1;
            end
            if masterfound
                Master_Pattern = inputdlg('Wie lautet Ihr Master-Programm-Name?');
            end
        end
        if masterfound
            if isempty(find(strcmp('EISstart',fieldnames(master.diga.daten)), 1))
                EIS_Dateien = dir([pathname '*' DStringOrig '*' Master_Pattern '*EIS00*.mat']);
                for EISD_i = 1:numel(EIS_Dateien)
                    EISFile = load([pathname '/' EIS_Dateien(EISD_i).name]);
                    
                    EIS_indexes(EISD_i) = find(master.diga.daten.Programmdauer <= EISFile.diga.daten.Programmdauer(1),1,'last');
                    nach_EIS_indexes(EISD_i) = find(master.diga.daten.Programmdauer > EISFile.diga.daten.Programmdauer(end),1,'first');
                end
                EIS_index = EIS_indexes(eisnumber);
                nach_EIS_index = find(master.diga.daten.Programmdauer > messung.diga.daten.Programmdauer(end),1,'first');
            elseif ~isempty(find(diff(master.diga.daten.EISstart)>0, 1)) && numel(find(diff(master.diga.daten.EISstart)>0))>=eisnumber
                EIS_indexes = find(diff(master.diga.daten.EISstart)>0);
                % ab hier �berarbeitet
                EIS_SOC_STEP_indexes(1)= EIS_indexes(1);
                for EISj=2:numel(EIS_indexes)
                    temp_idx=master.diga.daten.Programmdauer < master.diga.daten.Programmdauer(EIS_indexes(EISj));
                    EIS_SOC_STEP_indexes(EISj) = find(strcmp(master.diga.daten.Zustand(temp_idx),'DCH'),1,'last');
                end
                % Ende der �berarbeitung
                EIS_index = EIS_indexes(eisnumber);
                if sum(strcmp(fieldnames(master.diga.daten),'EISready'))
                    nach_EIS_index = EIS_index+find(master.diga.daten.EISready(EIS_index+2:end),1,'first');
                    if isempty(nach_EIS_index), nach_EIS_index = numel(master.diga.daten.EISstart); end;
                else
                    nach_EIS_index = EIS_index-2+find(master.diga.daten.EISstart(EIS_index+1:end)==0,1,'first');
                    if isempty(nach_EIS_index), nach_EIS_index = numel(master.diga.daten.EISstart); end;
                end
            else
                masterfound = 0;
            end
            if getTemp
                Temperatur=master.diga.daten.Temperatur(EIS_indexes(1):EIS_indexes(end));
                Temperatur=mean(Temperatur);
                Temperatur=round(Temperatur*2)/2;
                set(app.TemperaturTextBox,'Value',num2str(Temperatur))
                fcn_TemperaturTextBox_Callback(app, event);
            end
        end
        if masterfound
            StepIndex = [ 1 1+find(abs(diff(master.diga.daten.Schritt)) > 0) numel(master.diga.daten.Schritt)+1];
            AhPrev = 0;
            for i_step = 2:(numel(StepIndex)-1)
                master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1)) = ...
                    AhPrev + master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1))-master.diga.daten.AhAkku(StepIndex(i_step));
                AhPrev=master.diga.daten.AhAkku(StepIndex(i_step+1)-1);
            end
            if isempty(find(strcmp('EISstart',fieldnames(master.diga.daten)), 1))
                Vor_EIS_index = find(master.diga.daten.AhStep(1:EIS_index)>0.01*(max(master.diga.daten.AhAkku)-min(master.diga.daten.AhAkku)) & ...
                    abs(master.diga.daten.Strom(1:EIS_index))>0.1*(max(master.diga.daten.AhAkku)-min(master.diga.daten.AhAkku)) & (...
                    (strcmp(master.diga.daten.Zustand(1:EIS_index),'DCH') & (master.diga.daten.AhAkku(1:EIS_index)-master.diga.daten.AhAkku(EIS_index)) > 0.01*(max(master.diga.daten.AhAkku)-min(master.diga.daten.AhAkku))) | ...
                    (strcmp(master.diga.daten.Zustand(1:EIS_index),'CHA') & (master.diga.daten.AhAkku(1:EIS_index)-master.diga.daten.AhAkku(EIS_index)) < -0.01*(max(master.diga.daten.AhAkku)-min(master.diga.daten.AhAkku))) ...
                    ),1,'last');
                Start_Vor_EIS_index = find(strcmp(master.diga.daten.Zustand(1:Vor_EIS_index),'PAU'),1,'last')-5;
            else
                Vor_EIS_index = find(master.diga.daten.EISstart(1:EIS_index)==0 & master.diga.daten.AhStep(1:EIS_index)>0.01*(max(master.diga.daten.AhAkku)-min(master.diga.daten.AhAkku)) & ...
                    abs(master.diga.daten.Strom(1:EIS_index))>0.1*(max(master.diga.daten.AhAkku)-min(master.diga.daten.AhAkku)) & (...
                    (strcmp(master.diga.daten.Zustand(1:EIS_index),'DCH') & (master.diga.daten.AhAkku(1:EIS_index)-master.diga.daten.AhAkku(EIS_index)) > 0.01*(max(master.diga.daten.AhAkku)-min(master.diga.daten.AhAkku))) | ...
                    (strcmp(master.diga.daten.Zustand(1:EIS_index),'CHA') & (master.diga.daten.AhAkku(1:EIS_index)-master.diga.daten.AhAkku(EIS_index)) < -0.01*(max(master.diga.daten.AhAkku)-min(master.diga.daten.AhAkku))) ...
                    ),1,'last');
                Start_Vor_EIS_index = find(strcmp(master.diga.daten.Zustand(1:Vor_EIS_index),'PAU'),1,'last')-5;
            end
            if isempty(find(strcmp('EISstart',fieldnames(master.diga.daten)), 1))
                Naechste_SOC_Einstellung = EIS_index+find(master.diga.daten.AhStep(EIS_index+1:end)>0.02*(max(master.diga.daten.AhAkku)-min(master.diga.daten.AhAkku)) & ...
                    abs(master.diga.daten.Strom(EIS_index+1:end))>0.2*(max(master.diga.daten.AhAkku)-min(master.diga.daten.AhAkku)) & (...
                    (strcmp(master.diga.daten.Zustand(EIS_index+1:end),'CHA') & (master.diga.daten.AhAkku(EIS_index+1:end)-master.diga.daten.AhAkku(EIS_index)) > 0.02*(max(master.diga.daten.AhAkku)-min(master.diga.daten.AhAkku))) | ...
                    (strcmp(master.diga.daten.Zustand(EIS_index+1:end),'DCH') & (master.diga.daten.AhAkku(EIS_index+1:end)-master.diga.daten.AhAkku(EIS_index)) < -0.02*(max(master.diga.daten.AhAkku)-min(master.diga.daten.AhAkku))) ...
                    ) ,1,'first');
                Ende_Naechste_SOC_Einstellung = Naechste_SOC_Einstellung+find(strcmp(master.diga.daten.Zustand(Naechste_SOC_Einstellung+1:end),'PAU'),1,'first')-1;
            else
                Naechste_SOC_Einstellung = EIS_index+find(master.diga.daten.EISstart(EIS_index+1:end)==0 & master.diga.daten.AhStep(EIS_index+1:end)>0.02*(max(master.diga.daten.AhAkku)-min(master.diga.daten.AhAkku)) & ...
                    abs(master.diga.daten.Strom(EIS_index+1:end))>0.2*(max(master.diga.daten.AhAkku)-min(master.diga.daten.AhAkku)) & (...
                    (strcmp(master.diga.daten.Zustand(EIS_index+1:end),'CHA') & (master.diga.daten.AhAkku(EIS_index+1:end)-master.diga.daten.AhAkku(EIS_index)) > 0.02*(max(master.diga.daten.AhAkku)-min(master.diga.daten.AhAkku))) | ...
                    (strcmp(master.diga.daten.Zustand(EIS_index+1:end),'DCH') & (master.diga.daten.AhAkku(EIS_index+1:end)-master.diga.daten.AhAkku(EIS_index)) < -0.02*(max(master.diga.daten.AhAkku)-min(master.diga.daten.AhAkku))) ...
                    ) ,1,'first');
                Ende_Naechste_SOC_Einstellung = Naechste_SOC_Einstellung+find(strcmp(master.diga.daten.Zustand(Naechste_SOC_Einstellung+1:end),'PAU'),1,'first')-1;
            end
            if ((master.diga.daten.Programmdauer(EIS_index)-master.diga.daten.Programmdauer(Vor_EIS_index)) > 1e5) || max(master.diga.daten.Programmdauer)>3600*24*200
                master.diga.daten.Programmdauer = master.diga.daten.Programmdauer/1000;
            end
            while true
                Kapazitaet = 0 ;
                %                 steps = find([diff(master.diga.daten.AhStep(1:ceil(numel(master.diga.daten.AhStep)/4)))<0 -1] & ...
                %                     strcmp(master.diga.daten.Zustand(1:ceil(numel(master.diga.daten.AhStep)/4)),'DCH') & ...
                %                     master.diga.daten.AhStep(1:ceil(numel(master.diga.daten.AhStep)/4))>0.7*(max(master.diga.daten.AhAkku)-min(master.diga.daten.AhAkku))) ;
                % �berarbeitet
                steps = find([diff(master.diga.daten.AhStep(1:ceil(numel(master.diga.daten.AhStep)/4)))<0 -1] & ...
                    strcmp(master.diga.daten.Zustand(1:ceil(numel(master.diga.daten.AhStep)/4)),'DCH') & ...
                    master.diga.daten.AhStep(1:ceil(numel(master.diga.daten.AhStep)/4))>0.7*abs(abs(max(master.diga.daten.AhAkku))-abs(min(master.diga.daten.AhAkku)))) ;
                % ende
                for step_i = 1:numel(steps)
                    StepStart = find(master.diga.daten.Schritt(1:steps(step_i))~=master.diga.daten.Schritt(steps(step_i)),1,'last');
                    StepDuration = (master.diga.daten.Programmdauer(steps(step_i))-master.diga.daten.Programmdauer(StepStart));
                    if step_i==1 || (StepDuration<(3600*1.2+10) )
                        stepCap = str2double(num2str(master.diga.daten.AhStep(steps(step_i))));
                        if stepCap > Kapazitaet ,
                            Kapazitaet = stepCap ;
                            KapTestIndex  = steps(step_i);
                            Index100percent = find(~strcmp(master.diga.daten.Zustand(1:KapTestIndex),'DCH'),1,'last');
                            %                             indexes = KapTestIndex-100:KapTestIndex+100
                            %                             figure;plot(master.diga.daten.Programmdauer(indexes),master.diga.daten.Strom(indexes)/7);
                            %                             hold on; grid on;
                            %                             plot(master.diga.daten.Programmdauer(indexes),master.diga.daten.Spannung(indexes),'r')
                            %                             plot(master.diga.daten.Programmdauer(KapTestIndex),master.diga.daten.Spannung(KapTestIndex),'ro')
                            %                             plot(master.diga.daten.Programmdauer(indexes),master.diga.daten.AhStep(indexes)/7,'k')
                            %                             plot(master.diga.daten.Programmdauer(KapTestIndex),master.diga.daten.AhStep(KapTestIndex)/7,'ko')
                        end
                        
                    end
                end
                
                if ~get(app.AutoCapCheckBox,'Value')
                    Kapazitaet = str2double(inputdlg('Kapazit�t der Zelle [Ah]','Zellkapazit�t',[1],{num2str(Kapazitaet)}));
                end
                choice = num2str(Kapazitaet);
                if  strcmp(choice,'') , return,end
                if ~strcmp(choice,'NaN'), break, end
            end
            set(app.KapTextBox,'Value',choice)
            fcn_KapTextBox_Callback(app, event);
            %Vor_EIS_index = find(master.diga.daten.Programmdauer>master.diga.daten.Programmdauer(Vor_EIS_index)+180,1,'first');
            relax_time = master.diga.daten.Programmdauer(Start_Vor_EIS_index:EIS_index)-master.diga.daten.Programmdauer(Start_Vor_EIS_index);
            if sum(cell2mat(strfind(fieldnames(master.diga.daten),'Spannung')))
                relax_spannung = master.diga.daten.Spannung(Start_Vor_EIS_index:EIS_index);
            elseif sum(cell2mat(strfind(fieldnames(master.diga.daten),'Span')))
                relax_spannung = master.diga.daten.Span(Start_Vor_EIS_index:EIS_index);
            end
            if sum(cell2mat(strfind(fieldnames(master.diga.daten),'Strom')))
                relax_strom = master.diga.daten.Strom(Start_Vor_EIS_index:EIS_index);
            elseif sum(cell2mat(strfind(fieldnames(master.diga.daten),'Current')))
                relax_strom = master.diga.daten.Current(Start_Vor_EIS_index:EIS_index);
            end
            DRT_GUI.Messdaten.relax.zeit = relax_time;
            DRT_GUI.Messdaten.relax.spannung = relax_spannung;
            DRT_GUI.Messdaten.relax.strom = relax_strom;
            
            if isempty(find(strcmp('EISstart',fieldnames(master.diga.daten))))
                eis_time = messung.diga.daten.Programmdauer-messung.diga.daten.Programmdauer(1);
                if sum(cell2mat(strfind(fieldnames(messung.diga.daten),'Spannung')))
                    eis_spannung = messung.diga.daten.Spannung;
                elseif sum(cell2mat(strfind(fieldnames(messung.diga.daten),'Span')))
                    eis_spannung = messung.diga.daten.Span;
                end
                if sum(cell2mat(strfind(fieldnames(messung.diga.daten),'Strom')))
                    eis_strom = messung.diga.daten.Strom;
                elseif sum(cell2mat(strfind(fieldnames(messung.diga.daten),'Current')))
                    eis_strom = messung.diga.daten.Current;
                end
                DRT_GUI.Messdaten.eis.zeit = eis_time;
                DRT_GUI.Messdaten.eis.spannung = eis_spannung;
                DRT_GUI.Messdaten.eis.strom = eis_strom;
            else
                eis_time = master.diga.daten.Programmdauer(EIS_index:nach_EIS_index)-master.diga.daten.Programmdauer(EIS_index);
                if sum(cell2mat(strfind(fieldnames(master.diga.daten),'Spannung')))
                    eis_spannung = master.diga.daten.Spannung(EIS_index:nach_EIS_index);
                elseif sum(cell2mat(strfind(fieldnames(master.diga.daten),'Span')))
                    eis_spannung = master.diga.daten.Span(EIS_index:nach_EIS_index);
                end
                if sum(cell2mat(strfind(fieldnames(master.diga.daten),'Strom')))
                    eis_strom = master.diga.daten.Strom(EIS_index:nach_EIS_index);
                elseif sum(cell2mat(strfind(fieldnames(master.diga.daten),'Current')))
                    eis_strom = master.diga.daten.Current(EIS_index:nach_EIS_index);
                end
                DRT_GUI.Messdaten.eis.zeit = eis_time;
                DRT_GUI.Messdaten.eis.spannung = eis_spannung;
                DRT_GUI.Messdaten.eis.strom = eis_strom;
            end
            nach_eis_time = master.diga.daten.Programmdauer(nach_EIS_index+1:Ende_Naechste_SOC_Einstellung)-master.diga.daten.Programmdauer(nach_EIS_index+1);
            if sum(cell2mat(strfind(fieldnames(master.diga.daten),'Spannung')))
                nach_eis_spannung = master.diga.daten.Spannung(nach_EIS_index+1:Ende_Naechste_SOC_Einstellung);
            elseif sum(cell2mat(strfind(fieldnames(master.diga.daten),'Span')))
                nach_eis_spannung = master.diga.daten.Span(nach_EIS_index+1:Ende_Naechste_SOC_Einstellung);
            end
            if sum(cell2mat(strfind(fieldnames(master.diga.daten),'Strom')))
                nach_eis_strom = master.diga.daten.Strom(nach_EIS_index+1:Ende_Naechste_SOC_Einstellung);
            elseif sum(cell2mat(strfind(fieldnames(master.diga.daten),'Current')))
                nach_eis_strom = master.diga.daten.Current(nach_EIS_index+1:Ende_Naechste_SOC_Einstellung);
            end
            DRT_GUI.Messdaten.nach_eis.zeit = nach_eis_time;
            DRT_GUI.Messdaten.nach_eis.spannung = nach_eis_spannung;
            DRT_GUI.Messdaten.nach_eis.strom = nach_eis_strom;
            
            if isempty(find(strcmp('EISstart',fieldnames(master.diga.daten))))
                if eisnumber == 1
                    Prev_EIS_index = 1;
                else
                    Prev_EIS_index = nach_EIS_indexes(eisnumber-1);
                end
                
                
                vor_relax_time = master.diga.daten.Programmdauer(Prev_EIS_index:Start_Vor_EIS_index)-master.diga.daten.Programmdauer(Prev_EIS_index);
                if sum(cell2mat(strfind(fieldnames(master.diga.daten),'Spannung')))
                    vor_relax_spannung = master.diga.daten.Spannung(Prev_EIS_index:Start_Vor_EIS_index);
                elseif sum(cell2mat(strfind(fieldnames(master.diga.daten),'Span')))
                    vor_relax_spannung = master.diga.daten.Span(Prev_EIS_index:Start_Vor_EIS_index);
                end
                if sum(cell2mat(strfind(fieldnames(master.diga.daten),'Strom')))
                    vor_relax_strom = master.diga.daten.Strom(Prev_EIS_index:Start_Vor_EIS_index);
                elseif sum(cell2mat(strfind(fieldnames(master.diga.daten),'Current')))
                    vor_relax_strom = master.diga.daten.Current(Prev_EIS_index:Start_Vor_EIS_index);
                end
                DRT_GUI.Messdaten.vor_relax.zeit = vor_relax_time;
                DRT_GUI.Messdaten.vor_relax.spannung = vor_relax_spannung;
                DRT_GUI.Messdaten.vor_relax.strom = vor_relax_strom;
            elseif sum(strcmp(fieldnames(master.diga.daten),'EISready'))
                if eisnumber == 1
                    Prev_EIS_index = 1;
                else
                    Prev_EIS_index = EIS_indexes(eisnumber-1);
                end
                Prev_EIS_index = Prev_EIS_index+find(master.diga.daten.EISready(Prev_EIS_index+2:end),1,'first');
                
                vor_relax_time = master.diga.daten.Programmdauer(Prev_EIS_index:Start_Vor_EIS_index)-master.diga.daten.Programmdauer(Prev_EIS_index);
                if sum(cell2mat(strfind(fieldnames(master.diga.daten),'Spannung')))
                    vor_relax_spannung = master.diga.daten.Spannung(Prev_EIS_index:Start_Vor_EIS_index);
                elseif sum(cell2mat(strfind(fieldnames(master.diga.daten),'Span')))
                    vor_relax_spannung = master.diga.daten.Span(Prev_EIS_index:Start_Vor_EIS_index);
                end
                if sum(cell2mat(strfind(fieldnames(master.diga.daten),'Strom')))
                    vor_relax_strom = master.diga.daten.Strom(Prev_EIS_index:Start_Vor_EIS_index);
                elseif sum(cell2mat(strfind(fieldnames(master.diga.daten),'Current')))
                    vor_relax_strom = master.diga.daten.Current(Prev_EIS_index:Start_Vor_EIS_index);
                end
                DRT_GUI.Messdaten.vor_relax.zeit = vor_relax_time;
                DRT_GUI.Messdaten.vor_relax.spannung = vor_relax_spannung;
                DRT_GUI.Messdaten.vor_relax.strom = vor_relax_strom;
            end
            % �berlagerten DC-Strom berechnen
            DC_Current = sum(diff(DRT_GUI.Messdaten.eis.zeit).*DRT_GUI.Messdaten.eis.strom(2:end))./sum(diff(DRT_GUI.Messdaten.eis.zeit));
            
            if sum(cell2mat(strfind(fieldnames(master.diga.daten),'Strom')))
                strom = master.diga.daten.Strom;
            elseif sum(cell2mat(strfind(fieldnames(master.diga.daten),'Current')))
                strom = master.diga.daten.Current;
            end
            
            %TestAhAkku = [0 cumsum(diff(master.diga.daten.Programmdauer/3600).*strom(1:end-1))];
            
            % Ab hier ver�ndert
            if Index100percent == -1
                AhAkku_werte = master.diga.daten.AhStep(EIS_indexes)-max(abs(master.diga.daten.AhAkku));
            else
                %AhAkku_werte = master.diga.daten.AhStep(EIS_indexes)-master.diga.daten.AhStep(Index100percent);
                AhAkku_werte = abs(master.diga.daten.AhAkku(EIS_SOC_STEP_indexes-1)-master.diga.daten.AhStep(Index100percent));
            end
            %             SOC = round((DRT_GUI.Testparameter.Cap + AhAkku_werte(eisnumber))/DRT_GUI.Testparameter.Cap *100*10)/10;
            DRT_GUI.Testparameter.AhSteps=zeros(1,numel(AhAkku_werte));
            for idx=2:numel(AhAkku_werte)
                DRT_GUI.Testparameter.AhSteps(idx)=DRT_GUI.Testparameter.AhSteps(idx-1)+AhAkku_werte(idx);
            end
            if AhAkku_werte(1)~=0
                DRT_GUI.Testparameter.AhSteps(1)=AhAkku_werte(1);
            end
            SOC = round((DRT_GUI.Testparameter.Cap - DRT_GUI.Testparameter.AhSteps(eisnumber))/DRT_GUI.Testparameter.Cap *100*10)/10;
            SOCfound = 1;
            set(app.SOCTextBox,'Value',num2str(SOC));
            fcn_SOCTextBox_Callback(app,event);
            %                 p_init = [1000 40 relax_spannung(1) (relax_spannung(end)-relax_spannung(1))/2 (relax_spannung(end)-relax_spannung(1))/2];
            %                 modelformel = 'p(3)+p(4).*exp(-w./p(1))+p(5).*exp(-w./p(2))';
            %                 options = optimset('MaxIter',10000,'MaxFunEvals',10000,'TolX',1e-12,'TolFun',1e-12);
            %                 [p,fval,exitflag,output]=function_fit_easyfit2(relax_time,[relax_spannung, zeros(size(relax_spannung))],p_init,@function_model_all_types2, [0 0 -inf -inf -inf], [inf,inf,inf,inf,inf] ,options, modelformel);
            %                 figure;
            %                     %plot(relax_time,relax_spannung,relax_time,relax_strom)
            %                     plot(relax_time,relax_spannung,'o')
            %                     hold on
            %                     w = relax_time;
            %                     fit_spannung = eval(modelformel);
            %                     plot(relax_time,fit_spannung,'r')
            %                     title({'Relaxationsvorgang vor der Messung',['tau1=' num2str(p(1)) ' s    tau2=' num2str(p(2)) 's']})
            clear master
        end
    end
    if ~SOCfound
        while true
            SOC = str2double(inputdlg('SOC','SOC',[1],{'25'}));
            choice = num2str(SOC);
            if  strcmp(choice,'') , return,end
            if ~strcmp(choice,'NaN'), break, end;
        end
        set(app.SOCTextBox,'Value',choice)
        fcn_SOCTextBox_Callback(app,event);
        SOCfound = 1;
    end
    if ~Batteriefound
        while true
            BatterieName = inputdlg('BatterieName','Name',[1],{'Dummy'});
            choice = BatterieName;
            if  isempty(choice) ||strcmp(choice,'') , return,end
            if ~strcmp(choice,'NaN'), break, end;
        end
        set(app.BatterieTextBox,'Value',choice);
        fcn_BatterieTextBox_Callback(app, event);
        Batteriefound = 1;
    end
    DRT_GUI.Messdaten.DC_Overlay = DC_Current;
    
    clear messung
    
    if ~isempty(find(strcmp(fieldnames(DRT_GUI.Messdaten),'DC_Overlay'),1)) && ~isempty(find(strcmp(fieldnames(DRT_GUI.Testparameter),'Cap'),1))
        if ~isempty(DRT_GUI.Testparameter.Zustand)
            DRT_GUI.Testparameter.Zustand = [DRT_GUI.Testparameter.Zustand '_' ];
        end
        Rounded_Current = round(4*DRT_GUI.Messdaten.DC_Overlay./DRT_GUI.Testparameter.Cap)/4;
        if Rounded_Current == 0 && DRT_GUI.Messdaten.DC_Overlay > 0.01
            Rounded_Current_Str= 'p0C_DC';
        elseif Rounded_Current == 0 && DRT_GUI.Messdaten.DC_Overlay < -0.01
            Rounded_Current_Str= 'm0C_DC';
        else
            Rounded_Current_Str= [strrep(num2str(Rounded_Current),'-','m') 'C_DC'];
        end
        DRT_GUI.Testparameter.Zustand = [DRT_GUI.Testparameter.Zustand Rounded_Current_Str];
    end
    
    set(app.fminTextBox,'Value',num2str(min(DRT_GUI.Messdaten.frequenz)))
    set(app.fmaxTextBox,'Value',num2str(max(DRT_GUI.Messdaten.frequenz)))
    set(app.MessPunkteTextBox,'Value',num2str(numel(DRT_GUI.Messdaten.frequenz)))
    
    if mean(DRT_GUI.Messdaten.Zreal)>0.1 && strcmp(filename(end-3:end),'.mat')
        DRT_GUI.Messdaten.Zreal = DRT_GUI.Messdaten.Zreal/1000;
        DRT_GUI.Messdaten.Zimg = DRT_GUI.Messdaten.Zimg/1000;
    end
    DRT_GUI.Messdaten.Z = DRT_GUI.Messdaten.Zreal + 1i .* DRT_GUI.Messdaten.Zimg;
    
    
    Typ = 'Nyquist';
    %     if ~strcmp(eventdata, 'kein_plot')
    hold(app.axes1,'off');
    fcn_plot_Auswaehl(app,DRT_GUI.Messdaten,[],[],DRT_GUI.Testparameter.Batterie,Typ);
    %     else
    %     end
    
    
    fcn_ModellAktualisierenButton(app,event);
    fcn_DRT_Config_reload_Button(app, event);
    set(app.PunkteWegnehmenTextBox,'Value','')
    fcn_PunkteWegnehmeButton(app, event);
    % fcn_HFcorrectionButton_Callback(app,event)
    
    fcn_PasteButton(app,event);
    set(app.GueltigeMessungCheck,'Value',1)
    fcn_GueltigeMessungCheck(app, event);
    fcn_FitButtonFunction(app, event);
    %fcn_FitButton_Callback(app, event)
    fcn_DRT_Button(app, event);
    fcn_CopyButton(app,event);
    if isempty(cell2mat(strfind(fieldnames(DRT_GUI.Testparameter),'Zustand'))) || isempty(DRT_GUI.Testparameter.Zustand)
        DRT_GUI.Testparameter.Zustand = 'default';
        set(app.ZustandTextBox,'Value','default')
    end
    
    choice = 'Ja';
    pfad = ['output' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Zustand '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad/' ...
        DRT_GUI.Testparameter.Batterie '_' DRT_GUI.Testparameter.Zustand '_' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad_' [strrep(num2str(DRT_GUI.Testparameter.SOC),'-','m')] 'SOC.mat'];
    if iscell(pfad) , pfad = cell2mat(pfad);end
    if ~isempty(dir(pfad))
        choice = questdlg('Die Datei existiert bereits. Soll sie �berschrieben werden?', ...
            '�berschreiben', ...
            'Ja','Nein','Nein');
    end
    if strcmp(choice,'Ja')
        fcn_SpeichernButton(app,event);
    end
end
end