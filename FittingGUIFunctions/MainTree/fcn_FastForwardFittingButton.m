function fcn_FastForwardFittingButton(app, event)
set(app.FastForwardFitting,'Text','Running...')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%% Vorher einzustellende Werte %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic
set(app.FortschrittGauge,'Value',0)
runs=5;
alle_an = ones(1,13);
if app.RserconstCheckBox.Value==1
    alle_aus = [1,0,0,1,0,0,1,0,0,0,1,0,0];
    fix_hf=[1,0,0,alle_an(4:13)];
    fix_fit=[1,1,1,alle_aus(4:13)];
    fix_pfit=[fix_fit(1:5),1,fix_fit(7:9),1,fix_fit(11:13)];
else
    alle_aus = [0,0,0,1,0,0,1,0,0,0,1,0,0];
    fix_hf=[0,0,0,alle_an(4:13)];
    fix_fit=[0,1,1,alle_aus(4:13)];
    fix_pfit=[fix_fit(1:5),1,fix_fit(7:9),1,fix_fit(11:13)];
end
% if app.RserconstCheckBox.Value==1
%     alle_aus = [1,0,0,0,0];
%     fix_hf=[1,0,0,1,1];
%     fix_fit=[1,0,0,0,0];
%     fix_pfit=[fix_fit(1:5)];
% else
%     alle_aus = [0,0,0,0,0];
%     fix_hf=[0,0,0,0,0];
%     fix_fit=[1,0,0,0,0];
%     fix_pfit=[fix_fit(1:5)];
% end
SOCs = get(app.SOCPopup ,'Items');
%Temps = get(app.TemperaturPopup,'Items');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% ROUTINE F�R JEDEN SOC %%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
try
    for i_SOC = 2:numel(SOCs)
        set(app.SOCPopup,'Value',SOCs{i_SOC})
        fcn_SOCPopup_Callback(app,event)
        set(app.Korrigiert_Punkte_Weg_TextBox,'Value','')
        fcn_Korrigiert_Punkte_Weg_Button_Callback(app, event)
        TableCell_Temp=get(app.ParamTable,'Data');
        Rser=TableCell_Temp{1,3};
        Rind=TableCell_Temp{2,3};
        Lind=TableCell_Temp{3,3};
        %%% PASTE
        fcn_PasteButton_Callback(app,event)
        %%% Fixe Werte deaktivieren
        TableCell=get(app.ParamTable,'Data');
%         TableCell(:,4)=cellfun(@(x) {x*0.8}, TableCell(:,3));
%         TableCell(:,5)=cellfun(@(x) {x*1.2}, TableCell(:,3));
        if app.RserconstCheckBox.Value==1
            TableCell{1,3}=Rser;
        end
        if app.HFinclCheckBox==1
            for P_i = 1:numel(alle_aus)
                TableCell{P_i,2} = logical(alle_aus(P_i));
            end
        else
            for P_i = 1:numel(alle_aus)
                TableCell{P_i,2} = logical(fix_fit(P_i));
            end
            TableCell{2,3}=Rind;
            TableCell{3,3}=Lind;
        end
        set(app.ParamTable,'Data',TableCell)
        %%% FIT x20
        for i=1:20
            fcn_FitButton_Callback(app, event)
            if app.KeinPlotCheckBox.Value~=1
                pause(0.01)
            end
        end
        
        for j=1:runs
            %%% EIS-Indices ignorieren auf end-30
            if app.HFinclCheckBox.Value==1
                weg_string='1:2 end-30:end';
                set(app.PunkteWegnehmenTextBox,'Value',weg_string)
                fcn_PunkteWegnehmenButton_Callback(app, event)
                %%% Alles fix au�er erste 3
                TableCell=get(app.ParamTable,'Data');
                for P_i = 1:numel(alle_aus)
                    TableCell{P_i,2} = logical(fix_hf(P_i));
                end
                set(app.ParamTable,'Data',TableCell)
                %%% FIT x2
                for i=1:2
                    fcn_FitButton_Callback(app, event)
                    if app.KeinPlotCheckBox.Value~=1
                        pause(0.01)
                    end
                end
                %%% Erste 3 fix
                TableCell=get(app.ParamTable,'Data');
                for P_i = 1:numel(alle_aus)
                    TableCell{P_i,2} = logical(fix_fit(P_i));
                end
                set(app.ParamTable,'Data',TableCell)
            end
            %%% EIS-Indices ignorieren auf end-7
            if strcmp(SOCs{i_SOC},'100')
                weg_string='1:2 end-15:end';
            else
                weg_string='1:2 end-7:end';
            end
            set(app.PunkteWegnehmenTextBox,'Value',weg_string)
            fcn_PunkteWegnehmenButton_Callback(app, event)
            %%% FIT x2
            for i=1:2
                fcn_FitButton_Callback(app, event)
                if app.KeinPlotCheckBox.Value~=1
                    pause(0.01)
                end
            end
            %%% Taus fix setzen
            TableCell=get(app.ParamTable,'Data');
            for P_i = 1:numel(alle_aus)
                TableCell{P_i,2} = logical(fix_pfit(P_i));
            end
            set(app.ParamTable,'Data',TableCell)
            %%% Fit Routine x10
            for i=1:10
                fcn_DRTButton_Callback(app, event)
                fcn_Prozesse_fitten_button_Callback(app, event)
                fcn_DRT_Prozesse_use_button_Callback(app, event)
                fcn_FitButton_Callback(app, event)
                if app.KeinPlotCheckBox.Value~=1
                    pause(0.01)
                end
            end
            count=((i_SOC-2)*runs+j)/((numel(SOCs)-1)*runs);
            set(app.FortschrittGauge,'Value',count*100)
            pause(0.01)
            %toc
        end
        %%% Wenn fertig:
        fcn_CopyButton_Callback(app,event)
        fcn_SpeichernButton_Callback(app, event)
        %toc
    end
    set(app.FastForwardFitting,'Text','FastForwardFitting')
    set(app.FortschrittGauge,'Value',0)
    toc
catch
    set(app.FastForwardFitting,'Text','FastForwardFitting')
    toc
end