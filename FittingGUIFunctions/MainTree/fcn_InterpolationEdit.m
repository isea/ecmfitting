function fcn_InterpolationEdit(app,event)
global DRT_Config
try
    Wert = str2num(get(app.InterpolationEdit,'Value'));
catch error_msg
    set(app.InterpolationEdit,'Value',num2str(DRT_Config.InterpolationsFaktor))
    errordlg(error_msg)
end
if isempty(Wert)|| (Wert<10^app.InterpolationSlider.Limits(1) || Wert>10^app.InterpolationSlider.Limits(2))
    set(app.InterpolationEdit,'Value',num2str(DRT_Config.InterpolationsFaktor))
    errordlg('Ungültiger Wert')
else
    DRT_Config.InterpolationsFaktor = Wert;
    set(app.InterpolationSlider,'Value',log10(DRT_Config.InterpolationsFaktor))
    fcn_DRTButton_Callback(app, event)
end
end