function app = fcn_BatterieNamePopup(app, event)
global DRT_GUI
Folders = {''};
if nargin==1
    Batterien = app.BatterieNamePopup.Items;
    if numel(Batterien) == 1
        Batterien = [];
    end
    if isempty(Batterien) 
        if app.GUI
            f = dir('output/*');
        else
            f = dir([app.OutputPath '/*']);
        end
        for i = 1:numel(f)
            if ~strcmp(f(i).name,'.') && ~strcmp(f(i).name,'..') && f(i).isdir
                Folders = [Folders;f(i).name];
            end
        end
        app.BatterieNamePopup.Items=Folders;
    end

elseif nargin==2
    Batterien = app.BatterieNamePopup.Items;
    if strcmp(app.BatterieNamePopup.Value, app.BatterieNamePopup.Items{1}) && ~isempty(DRT_GUI) && sum(ismember(fieldnames(DRT_GUI),'Testparameter')) && ~isempty(DRT_GUI.Testparameter) &&...
            sum(ismember(fieldnames(DRT_GUI.Testparameter),'Batterie')) && ~isempty(DRT_GUI.Testparameter.Batterie)
        if ~isempty(find(ismember(Batterien,DRT_GUI.Testparameter.Batterie)))
            if app.GUI
                set(app.BatterieNamePopup,'Value',app.BatterieNamePopup.Items{find(ismember(Batterien,DRT_GUI.Testparameter.Batterie))})
            else
                app.BatterieNamePopup.Value = app.BatterieNamePopup.Items{find(ismember(Batterien,DRT_GUI.Testparameter.Batterie))};
            end
        end
    end
    try
        if ismember(event.Value,app.BatterieNamePopup.Items)
            string1=event.Value;
        else
            string1=app.BatterieNamePopup.Value;
        end
    catch
        string1=app.BatterieNamePopup.Value;
    end
    if app.GUI
        f = dir(['output/' string1 '/*']);
    else
        f = dir([app.OutputPath '/' string1 '/*']);
    end
    for i = 1:numel(f)
        if ~strcmp(f(i).name,'.') && ~strcmp(f(i).name,'..') && f(i).isdir
            %             if ~isempty(DRT_GUI) && sum(ismember(fieldnames(DRT_GUI),'Testparameter')) && ~isempty(DRT_GUI.Testparameter) && sum(ismember(fieldnames(DRT_GUI.Testparameter),'Temperatur')) && ~isempty(DRT_GUI.Testparameter.Temperatur) && sum(ismember(fieldnames(DRT_GUI.Testparameter),'SOC'))&& ~isempty(DRT_GUI.Testparameter.SOC)
            %                 f2 = dir(['output/' string1 '/' f(i).name '/' [strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad'] '/' '*SOC.mat']);
            % %                 f2 = dir(['output/' string1 '/' f(i).name '/*'])
            %                 gefunden = 0;
            %                 for fi = 1:numel(f2)
            %                     File_SOC = str2double(strrep(f2(fi).name(1+find(f2(fi).name=='_',1,'last'):end-numel('SOC.mat')),'m','-'));
            %                     if File_SOC > DRT_GUI.Testparameter.SOC-2.5 && File_SOC < DRT_GUI.Testparameter.SOC+2.5
            %                         gefunden = 1;
            %                         break
            %                     end
            %                 end
            %                 if gefunden
            %                     Folders = [ Folders;f(i).name];
            %                 else
            %                     Folders = [ Folders; '<HTML><FONT COLOR="gray">' f(i).name '</HTML>'];
            %                 end
            %             else
            %                 Folders = [Folders;f(i).name];
            %             end
            Folders = [Folders;f(i).name];
        end
    end
    app.default_hyst_index = 1;
    app.default_crate_index = 1;
    %check whether Hysteresis or DC states are given
    if strcmp(Folders{2}(end),'H')
        app.text77_2.Visible = 'on';
        app.ZustandPopup_2.Enable = 'on';
        app.ZustandPopup_2.Visible = 'on';
        app.ZustandPopup.Position = [app.ZustandPopup.Position(1),app.ZustandPopup.Position(2),75,app.ZustandPopup.Position(4)];
        app.ZustandPopup_2.Items=Folders;        
        app.ZustandPopup_2.Value=app.ZustandPopup_2.Items(2);
        
        Folders = {''};
        f = dir(['output/' string1 '/' app.ZustandPopup_2.Value]);
        for i = 1:numel(f)
            if ~strcmp(f(i).name,'.') && ~strcmp(f(i).name,'..') && f(i).isdir
                Folders = [Folders;f(i).name];
            end
        end
        app.HysteresisFlag = 1;
        if strcmp(Folders{2}(end),'C')
            app.CRateFlag = 1;
            app.ZustandPopup.Enable = 'on';
            app.ZustandPopup.Items=Folders; 
            app.ZustandPopup.Value=app.ZustandPopup.Items(2);
            app.StateString = [app.ZustandPopup_2.Value '/' app.ZustandPopup.Value];
        else
            app.ZustandPopup.Enable = 'off';
            app.ZustandPopup.Items=[]; 
            app.ZustandPopup.Value = [];
            app.StateString = [app.ZustandPopup_2.Value];
        end
    else
        app.text77_2.Visible = 'off';
        app.ZustandPopup_2.Visible = 'off';
        app.ZustandPopup_2.Enable = 'off';
        app.ZustandPopup.Enable = 'on';
        app.ZustandPopup.Position = [app.ZustandPopup.Position(1),app.ZustandPopup.Position(2),151,app.ZustandPopup.Position(4)];
        app.ZustandPopup.Items=Folders;        
        app.ZustandPopup.Value=app.ZustandPopup.Items(1);        
        app.HysteresisFlag = 0;
        app.CRateFlag = 1;             
        app.StateString = [app.ZustandPopup.Value];
    end
    app = fcn_ZustandPopup(app, event);    
end
end