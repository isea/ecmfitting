function fcn_HF_Fit(app, event)
global DRT_GUI;

if isempty(DRT_GUI) || isempty(cell2mat(strfind(fieldnames(DRT_GUI),'Messdaten')))
    return
end



formula = DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell;
formula_HF = DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_HF;
if  isempty(cell2mat(strfind(fieldnames(DRT_GUI.Messdaten),'relax_fft'))) || isempty(DRT_GUI.Messdaten.relax_fft)
    m_w = DRT_GUI.Messdaten.omega(DRT_GUI.Messdaten.aktiv==1) ;
    m_real = DRT_GUI.Messdaten.Zreal(DRT_GUI.Messdaten.aktiv==1);
    m_imag = DRT_GUI.Messdaten.Zimg(DRT_GUI.Messdaten.aktiv==1);
else
    m_w = [DRT_GUI.Messdaten.omega(DRT_GUI.Messdaten.aktiv==1) ; DRT_GUI.Messdaten.relax_fft.omega(DRT_GUI.Messdaten.relax_fft.aktiv==1)] ;
    m_real = [DRT_GUI.Messdaten.Zreal(DRT_GUI.Messdaten.aktiv==1); DRT_GUI.Messdaten.relax_fft.Zreal(DRT_GUI.Messdaten.relax_fft.aktiv==1)+DRT_GUI.Messdaten.relax_fft.Zreal_korrektur];
    m_imag = [DRT_GUI.Messdaten.Zimg(DRT_GUI.Messdaten.aktiv==1); DRT_GUI.Messdaten.relax_fft.Zimg(DRT_GUI.Messdaten.relax_fft.aktiv==1)];
end
TableCell = get(app.ParamTable,'Data');
TableCell = cell2struct(TableCell',{'Name','Fix','Value','Min','Max','Lim'});


% die initialisierte Werte aus Feld bekommen
DRT_GUI.Fit.ParFix = zeros(size(DRT_GUI.Fit.Parameter));
for Par_init_i = 1:length(DRT_GUI.Fit.Parameter)
    
    
    if ~isempty(TableCell(Par_init_i).Value)
        DRT_GUI.Fit.Parameter(Par_init_i) = TableCell(Par_init_i).Value;
    else
        DRT_GUI.Fit.Parameter(Par_init_i) = 0;
        TableCell(Par_init_i).Value = 0;
    end
    
    if ~isempty(TableCell(Par_init_i).Min) && ~TableCell(Par_init_i).Fix
        p_min(Par_init_i)= TableCell(Par_init_i).Min;
    elseif TableCell(Par_init_i).Fix
        DRT_GUI.Fit.ParFix(Par_init_i) = 1 ;
        p_min(Par_init_i) = TableCell(Par_init_i).Value;
    else
        p_min(Par_init_i) = 0;
        TableCell(Par_init_i).Min = 0 ;
    end
    if ~isempty(TableCell(Par_init_i).Min)
        DRT_GUI.Fit.Parameter_min(Par_init_i) = TableCell(Par_init_i).Min;
    else
        DRT_GUI.Fit.Parameter_min(Par_init_i) = 0;
    end
    if ~isempty(TableCell(Par_init_i).Max) && ~TableCell(Par_init_i).Fix
        p_max(Par_init_i)= TableCell(Par_init_i).Max;
    elseif TableCell(Par_init_i).Fix
        p_max(Par_init_i) = TableCell(Par_init_i).Value;
    else
        p_max(Par_init_i) = inf;
        TableCell(Par_init_i).Max = Inf;
    end
    if ~isempty(TableCell(Par_init_i).Max)
        DRT_GUI.Fit.Parameter_max(Par_init_i) = TableCell(Par_init_i).Max;
    else
        DRT_GUI.Fit.Parameter_max(Par_init_i) = inf;
    end
end

p_init = DRT_GUI.Fit.Parameter;
if  get(app.cont_process_checkbox,'Value') || strcmp(get(app.MetaFitButton,'Text'),'Stop')
    options = optimset('MaxIter',5000,'MaxFunEvals',100000,'TolX',1e-10,'TolFun',1e-10);
else
    options = optimset('MaxIter',20000,'MaxFunEvals',20000,'TolX',1e-10,'TolFun',1e-10);
end
p_min_model = cell2mat(DRT_GUI.Fit.aktuell_Modell.ModellCell{5});
p_max_model = cell2mat(DRT_GUI.Fit.aktuell_Modell.ModellCell{6});
set(app.ParamTable,'Data',struct2cell(TableCell)')
[p_best,fval,exitflag,output]=function_fit_easyfit2(m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min, p_max ,options, formula);
index_min = find((p_best<=p_min*1.001 & p_min~=p_max & p_min~=p_min_model & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Rser') & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'C')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Cser')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Kskin')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Lser')));
index_max = find((p_best>=p_max*0.999 & p_min~=p_max & p_max~=p_max_model & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Rser') & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'C')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Cser')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Kskin')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Lser')));

DRT_GUI.Fit.Parameter = p_best;
DRT_GUI.Fit.Limit_Reached.index_min = index_min;
DRT_GUI.Fit.Limit_Reached.index_max = index_max;

% die Fittwerte(p-best) in Feld zeigen
for P_i = 1:length(p_best)
    
    TableCell(P_i).Value = p_best(P_i);
    if ~isempty(find([index_min index_max]==P_i,1,'first'))
        TableCell(P_i).Lim = true;
    else
        TableCell(P_i).Lim = false;
    end
end
TableCell = struct2cell(TableCell)';
set(app.ParamTable,'Data',TableCell)
end