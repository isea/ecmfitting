function fcn_ProzesseSlider(app,event)
global DRT_Config
DRT_Config.Prozesse = get(app.ProzesseSlider,'Value');
set(app.ProzesseEdit,'Value',num2str(DRT_Config.Prozesse))
fcn_DRTButton_Callback(app, event)
end