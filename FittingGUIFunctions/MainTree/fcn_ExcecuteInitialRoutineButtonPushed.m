function fcn_ExcecuteInitialRoutineButtonPushed(app, event)
    repeat = app.InitialSpinner.Value;
    if repeat ==0
        return
    end
    
    
    TableConfig=get(app.InitialRoutineTable,'Data');
    TableCell=get(app.ParamTable,'Data');
    FixConfig = fcn_evalconfiguration(TableCell(:,1),TableCell(:,2),TableConfig(:,3));
    
    % create routine struct
    configuration=struct('Type',{},'Number',{},'Configuration',{},'weg_string',{});
    for i = 1:size(FixConfig,1)
        configuration(i).Type = 'Fit';
        configuration(i).Number = TableConfig{i,5};
        configuration(i).Configuration = FixConfig{i,6};
        configuration(i).weg_string = TableConfig{i,4};
    end
    routine = struct('Name',{},'Number',{},'Configuration',{});
    routine(1).Name = 'Initial';
    routine.Number = repeat;
    routine.Configuration = configuration;
    
    
    %excecute Fits
    fcn_ExcecuteRoutine(app,event,routine)    
end