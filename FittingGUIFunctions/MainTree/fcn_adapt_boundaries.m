function fcn_adapt_boundaries(app,temp_index,SOC_index,sorted_temperature_array,sorted_SOC_array)
%Calculate new boundaries for the parameters 

global DRT_GUI
app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
fcn_ModellAuswahlPopup(app);
fcn_BatterieNamePopup_Callback(app);
if  isempty(cell2mat(strfind(fieldnames(DRT_GUI.Messdaten),'relax_fft'))) || isempty(DRT_GUI.Messdaten.relax_fft)
     m_w = DRT_GUI.Messdaten.omega(DRT_GUI.Messdaten.aktiv==1);
      m_real = DRT_GUI.Messdaten.Zreal(DRT_GUI.Messdaten.aktiv==1);
     m_imag = DRT_GUI.Messdaten.Zimg(DRT_GUI.Messdaten.aktiv==1);
else
     m_w = [DRT_GUI.Messdaten.omega(DRT_GUI.Messdaten.aktiv==1) ; DRT_GUI.Messdaten.relax_fft.omega(DRT_GUI.Messdaten.relax_fft.aktiv==1)] ; %selbst
     m_real = [DRT_GUI.Messdaten.Zreal(DRT_GUI.Messdaten.aktiv==1); DRT_GUI.Messdaten.relax_fft.Zreal(DRT_GUI.Messdaten.relax_fft.aktiv==1)+DRT_GUI.Messdaten.relax_fft.Zreal_korrektur];%selbst
     m_imag = [DRT_GUI.Messdaten.Zimg(DRT_GUI.Messdaten.aktiv==1); DRT_GUI.Messdaten.relax_fft.Zimg(DRT_GUI.Messdaten.relax_fft.aktiv==1)];%selbst
end

%switch statement to adapt for R2RC or LiIon-sbi 

switch app.GlobalUsedModell

%Get Initial guesses from Nyquist-Plot
    case 'R2RC'
        if sum(sum(cellfun(@isempty,app.Boundaries{temp_index,SOC_index})))==(length(app.ParamTable.Data(:,1))*2)

            try
                [pks_o,locs_o]=findpeaks(-m_imag);
                [pks_u,locs_u]=findpeaks(m_imag);
                if isempty(pks_u) && isempty(locs_u)
                    locs_u=find(min(-m_imag(locs_o:end)));
                    pks_u=-m_imag(locs_o+locs_u); 
                    index=find(-m_imag>0);
                    index=find(min(-m_imag(index)))+index(1)-1;
                    Rser_Init=m_real(index-1)+(m_real(index)-m_real(index-1))*(m_imag(index-1))/(-m_imag(index)+m_imag(index-1));
                    R1_Init=abs(m_real(index)-m_real(locs_u(1)));
                    C1_Init=1/(R1_Init*2*pi*m_w(locs_o(1)));
                    Tau1_Init=R1_Init*C1_Init;
                    app.Boundaries{temp_index,SOC_index}(2,1)={R1_Init*0.8};
                    app.Boundaries{temp_index,SOC_index}(2,2)=app.ParamTable.Data(2,5);

                else
                    index=find(-m_imag>0);
                    index=find(min(-m_imag(index)))+index(1)-1;
                    Rser_Init=m_real(index-1)+(m_real(index)-m_real(index-1))*(m_imag(index-1))/(-m_imag(index)+m_imag(index-1));
        %             R1_Init=abs(m_real(index)-m_real(locs_u(1)));
        %             C1_Init=1/(R1_Init*2*pi*m_w(locs_o(1)));
        %             Tau1_Init=R1_Init*C1_Init;
        %             app.Boundaries{temp_index,SOC_index}(2,1)={R1_Init*0.5};
        %             app.Boundaries{temp_index,SOC_index}(2,2)={R1_Init*1.5};

        %nur sinnvol wenn es nicht h�ufiger vorkommt,dass das langsame RC-Glied
        %sein Maximum erreicht 
                    loc_interpolated_maximum = 0;
                    interpolated_maximum = 0;

                    considered_maxima=1;
                    if length(locs_o)>1
                        if length(locs_u)~=length(locs_o)
                            considered_maxima=length(locs_u);
                        else
                            considered_maxima=length(locs_o);
                        end
                    end

                    for n=1:length(locs_o)
                        loc_interpolated_maximum = loc_interpolated_maximum + m_real(locs_o(n));
                        interpolated_maximum = interpolated_maximum + pks_o(n);
                    end
                    loc_interpolated_maximum = loc_interpolated_maximum/length(locs_o);
                    index_w_u = find(m_real <= loc_interpolated_maximum);
                    index_w_u = index_w_u(end);
                    index_offset=find(-m_imag>0);
                    index_offset=index_offset(1)-1;
                    index_w_o = find(m_real(index_offset:end) >= loc_interpolated_maximum);
                    index_w_o = index_offset + index_w_o(1)-1;
                    w_interpolated_maximum = m_w(index_w_u)+ (m_w(index_w_o)+m_w(index_w_u))*0.5;
                    R1_Init=abs(m_real(index)-m_real(locs_u(end)));
                    C1_Init=1/(R1_Init*2*pi*w_interpolated_maximum);
                    Tau1_Init=R1_Init*C1_Init;
                    app.Boundaries{temp_index,SOC_index}(2,1)={R1_Init*0.8};
                    app.Boundaries{temp_index,SOC_index}(2,2)={R1_Init*1.2};
                end



                %save initial boundaries
                app.Boundaries{temp_index,SOC_index}(1,1)={Rser_Init*0.8};
                app.Boundaries{temp_index,SOC_index}(1,2)={Rser_Init*1.2};
                if(strcmp(app.ParamTable.Data(3,1),'Tau1'))
                    app.Boundaries{temp_index,SOC_index}(3,1)={Tau1_Init*0.8};
                    app.Boundaries{temp_index,SOC_index}(3,2)={Tau1_Init*1.2};
                elseif(strcmp(app.ParamTable.Data(3,1),'C1'))
                    app.Boundaries{temp_index,SOC_index}(3,1)={C1_Init*0.8};
                    app.Boundaries{temp_index,SOC_index}(3,2)={C1_Init*1.2};
                end
                for n=4:length(app.ParamTable.Data(:,1))
                    if(mod(length(app.ParamTable.Data(4:end,1)),2)~=0)
                        msgbox('Das Ersatzschaltbild ist keine RnRC-Schaltung!')
                        return
                    end
                    app.Boundaries{temp_index,SOC_index}(n,1)=app.ParamTable.Data(n,4);
                    app.Boundaries{temp_index,SOC_index}(n,2)=app.ParamTable.Data(n,5);
                end


            catch
                if SOC_index+1<=length(sorted_SOC_array{temp_index}) && SOC_index-1>=1 && app.Optimization_done(temp_index,SOC_index+1)==1 && app.Optimization_done(temp_index,SOC_index-1)==1
                    for n=1:length(app.ParamTable.Data(:,4))
                        app.Boundaries{temp_index,SOC_index}(n,1)={((app.Boundaries{temp_index,SOC_index+1}{n,1}+app.Boundaries{temp_index,SOC_index-1}{n,1})/2)*1.1};
                        app.Boundaries{temp_index,SOC_index}(n,2)={((app.Boundaries{temp_index,SOC_index+1}{n,2}+app.Boundaries{temp_index,SOC_index-1}{n,2})/2)*0.9};
                    end
                elseif SOC_index+1<=length(sorted_SOC_array{temp_index}) && app.Optimization_done(temp_index,SOC_index+1)==1
                    for n=1:length(app.ParamTable.Data(:,4))
                        app.Boundaries{temp_index,SOC_index}(n,1)={app.Boundaries{temp_index,SOC_index+1}{n,1}/1.1};
                        app.Boundaries{temp_index,SOC_index}(n,2)={app.Boundaries{temp_index,SOC_index+1}{n,2}/0.9}; 
                    end
                elseif SOC_index-1>=1 && app.Optimization_done(temp_index,SOC_index-1)==1
                    for n=1:length(app.ParamTable.Data(:,4))
                        app.Boundaries{temp_index,SOC_index}(n,1)={app.Boundaries{temp_index,SOC_index-1}{n,1}*1.1};
                        app.Boundaries{temp_index,SOC_index}(n,2)={app.Boundaries{temp_index,SOC_index-1}{n,2}*0.9}; 
                    end
                else 
                    app.Boundaries{temp_index,SOC_index}(:,1)=app.ParamTable.Data(:,4);
                    app.Boundaries{temp_index,SOC_index}(:,2)=app.ParamTable.Data(:,5);
                end
            end


        end
    case 'LiIon4_sbi'
        
        %Set Phi to fixed value 1 
        index=find(-m_imag>0);
        index=find(min(-m_imag(index)))+index(1)-1;
        Rser_Init=m_real(index-1)+(m_real(index)-m_real(index-1))*(m_imag(index-1))/(-m_imag(index)+m_imag(index-1));
        
        DataMatrix = app.ParamTable.Data;
        DataMatrix(1,4)={[Rser_Init*1.2]};
        DataMatrix(1,5)={[Rser_Init*0.8]};
        DataMatrix(4,2)={[1]};
        DataMatrix(4,3)={[1]};
        DataMatrix(4,4)={[1]};
        DataMatrix(4,5)={[1]};
        DataMatrix(7,2)={[1]};
        DataMatrix(7,3)={[1]};
        DataMatrix(7,4)={[1]};
        DataMatrix(7,5)={[1]};
        DataMatrix(11,2)={[1]};
        DataMatrix(11,3)={[1]};
        DataMatrix(11,4)={[1]};
        DataMatrix(11,5)={[1]};
        set(app.ParamTable,'Data',DataMatrix);
        app.Boundaries{temp_index,SOC_index}(:,1)=app.ParamTable.Data(:,4);
        app.Boundaries{temp_index,SOC_index}(:,2)=app.ParamTable.Data(:,5);
end


    extrapolation_count=1;
    potential_new_boundaries=[];
    %Check if respective SOC/T Values have been optimized for current SOC/T
    %values 
    if SOC_index>1
        sl_index=SOC_index-1;
        if app.Optimization_done(temp_index,sl_index)
            for n=1:length(app.Boundaries{temp_index,SOC_index}(:,1))
                 potential_new_boundaries{extrapolation_count}{n,1}=(1-((app.ContinousStep.Value/(100*10))*(sorted_SOC_array{temp_index}(SOC_index)-sorted_SOC_array{temp_index}(sl_index))))*app.optimal_parameters{temp_index,sl_index}(n);
                 potential_new_boundaries{extrapolation_count}{n,2}=(1+((app.ContinousStep.Value/(100*10))*(sorted_SOC_array{temp_index}(SOC_index)-sorted_SOC_array{temp_index}(sl_index))))*app.optimal_parameters{temp_index,sl_index}(n);
            end
            extrapolation_count=extrapolation_count+1;
        end
    end

    %hier
    if SOC_index<length(sorted_SOC_array{temp_index})
        su_index=SOC_index+1;
        if app.Optimization_done(temp_index,su_index)
            for n=1:length(app.Boundaries{temp_index,SOC_index}(:,1))
                %nicht von boundaries sondern von Optimalem Wert
    %              potential_new_boundaries{2}{n,1}=(app.Boundaries{temp_index,su_index}(n,1))/(1-((app.ContinousStep.Value/10)*abs(sorted_SOC_array{temp}(SOC_index)-sorted_SOC_array{temp}(su_index))));
    %              potential_new_boundaries{2}{n,2}=(app.Boundaries{temp_index,su_index}(n,2))/(1+((app.ContinousStep.Value/10)*abs(sorted_SOC_array{temp}(SOC_index)-sorted_SOC_array{temp}(su_index))));
                 potential_new_boundaries{extrapolation_count}{n,1}=(app.optimal_parameters{temp_index,su_index}(n))*(1-((app.ContinousStep.Value/(100*10))*(sorted_SOC_array{temp_index}(SOC_index)-sorted_SOC_array{temp_index}(su_index))));
                 potential_new_boundaries{extrapolation_count}{n,2}=(app.optimal_parameters{temp_index,su_index}(n))*(1+((app.ContinousStep.Value/(100*10))*(sorted_SOC_array{temp_index}(SOC_index)-sorted_SOC_array{temp_index}(su_index))));
            end
            extrapolation_count=extrapolation_count+1;
        end
    end

    %n�chste niedrigere Temperatur
    interpolated_value_lower_temp=[];
    if temp_index>1
        tl_index=find(min(abs(sorted_SOC_array{temp_index}(SOC_index)-sorted_SOC_array{temp_index-1})));
        if abs(sorted_SOC_array{temp_index}(SOC_index)-sorted_SOC_array{temp_index-1}(tl_index))<10
            if round(sorted_SOC_array{temp_index}(SOC_index)/5)*5==round(sorted_SOC_array{temp_index-1}(tl_index)/5)*5
                if app.Optimization_done(temp_index-1,tl_index)
                    interpolated_value_lower_temp=app.optimal_parameters{temp_index-1,tl_index};
                end
            elseif round(sorted_SOC_array{temp_index-1}(tl_index)/5)*5<round(sorted_SOC_array{temp_index}(SOC_index)/5)*5
                if tl_index+1<=length(sorted_SOC_array{temp_index-1})
                    temp_SOC_count=tl_index+1;
                    if app.Optimization_done(temp_index-1,tl_index) && app.Optimization_done(temp_index-1,temp_SOC_count) 
                        %Interpolation zwischen den SOC Punkten tl_index+1>tl_index
                        interpolated_value_lower_temp=zeros(1,length(app.Boundaries{temp_index-1,tl_index}(:,1)));
                        for n=1:length(app.Boundaries{temp_index-1,tl_index}(:,1))
                            interpolated_value_lower_temp(n)=(app.optimal_parameters{temp_index-1,temp_SOC_count}(n)-app.optimal_parameters{temp_index-1,tl_index}(n))*(((sorted_SOC_array{temp_index}(SOC_index)-sorted_SOC_array{temp_index-1}(tl_index))/(sorted_SOC_array{temp_index-1}(temp_SOC_count)-sorted_SOC_array{temp_index-1}(tl_index))))+app.optimal_parameters{temp_index-1,tl_index}(n);
                        end
                    end
                end
            elseif round(sorted_SOC_array{temp_index-1}(tl_index)/5)*5>round(sorted_SOC_array{temp_index}(SOC_index)/5)*5
                if tl_index-1>=1
                    temp_SOC_count=tl_index-1;
                    if app.Optimization_done(temp_index-1,tl_index) && app.Optimization_done(temp_index-1,temp_SOC_count)
                        %Interpolation zwischen den SOC Punkten %tl_index>tl_index-1
                        interpolated_value_lower_temp=zeros(1,length(app.Boundaries{temp_index-1,tl_index}(:,1)));
                        for n=1:length(app.Boundaries{temp_index-1,tl_index}(:,1))
                            interpolated_value_lower_temp(n)=(app.optimal_parameters{temp_index-1,tl_index}(n)-app.optimal_parameters{temp_index-1,temp_SOC_count}(n))*(((sorted_SOC_array{temp_index}(SOC_index)-sorted_SOC_array{temp_index-1}(temp_SOC_count))/(sorted_SOC_array{temp_index-1}(tl_index)-sorted_SOC_array{temp_index-1}(temp_SOC_count))))+app.optimal_parameters{temp_index-1,temp_SOC_count}(n);
                        end
                    end
                end
            end

            %hier bin ich
            %Extrapolation auf n�chsten Punkt
    %         b2(n)=exp((log(b2(n-1))+1/t(n)-1/t(n-1))/(1+0.3));
    %         c2(n)=exp((log(c2(n-1))+1/t(n)-1/t(n-1))/(1-0.3));
              if ~isempty(interpolated_value_lower_temp)
                 for n=1:length(app.Boundaries{temp_index,SOC_index}(:,1))
                     %alte Formel
    %                  potential_new_boundaries{extrapolation_count}{n,1}=exp((log(interpolated_value_lower_temp(n))+1/sorted_temperature_array(temp_index)-1/sorted_temperature_array(temp_index-1))/((1+(app.ContinousStep.Value)/100)));
    %                  potential_new_boundaries{extrapolation_count}{n,2}=exp((log(interpolated_value_lower_temp(n))+1/sorted_temperature_array(temp_index)-1/sorted_temperature_array(temp_index-1))/((1-(app.ContinousStep.Value)/100)));
                    %neue Formel
                       potential_new_boundaries{extrapolation_count}{n,1}=exp((log(interpolated_value_lower_temp(n))-(1/sorted_temperature_array(temp_index-1)-1/sorted_temperature_array(temp_index)))*((app.ContinousStep.Value)/100*1.5));
                       potential_new_boundaries{extrapolation_count}{n,2}=exp((log(interpolated_value_lower_temp(n))-(1/sorted_temperature_array(temp_index-1)-1/sorted_temperature_array(temp_index)))*((app.ContinousStep.Value)/100*0.5));
                 end
                 extrapolation_count=extrapolation_count+1;
              end
        end
    end

    interpolated_value_upper_temp=[];
    if temp_index<length(sorted_temperature_array)
        tu_index=find(min(abs(sorted_SOC_array{temp_index}(SOC_index)-sorted_SOC_array{temp_index+1})));
        if abs(sorted_SOC_array{temp_index}(SOC_index)-sorted_SOC_array{temp_index+1}(tu_index))<10
            if round(sorted_SOC_array{temp_index+1}(tu_index)/5)*5==round(sorted_SOC_array{temp_index}(SOC_index)/5)*5
                if app.Optimization_done(temp_index+1,tu_index)
                    interpolated_value_upper_temp=app.optimal_parameters{temp_index+1,tu_index};
                end
            elseif round(sorted_SOC_array{temp_index+1}(tu_index)/5)*5<round(sorted_SOC_array{temp_index}(SOC_index)/5)*5
                if tu_index+1<=length(sorted_SOC_array{temp_index+1})
                    temp_SOC_count=tu_index+1;
                    if app.Optimization_done(temp_index+1,tu_index) && app.Optimization_done(temp_index+1,temp_SOC_count)
                        %Interpolation zwischen den SOC Punkten tl_index+1>tl_index
                        interpolated_value_upper_temp=zeros(1,length(app.Boundaries{temp_index+1,tu_index}(:,1)));
                        for n=1:length(app.Boundaries{temp_index+1,tu_index}(:,1))
                            interpolated_value_upper_temp(n)=(app.optimal_parameters{temp_index+1,temp_SOC_count}(n)-app.optimal_parameters{temp_index+1,tu_index}(n))*(((sorted_SOC_array{temp_index}(SOC_index)-sorted_SOC_array{temp_index+1}(tu_index))/(sorted_SOC_array{temp_index+1}(temp_SOC_count)-sorted_SOC_array{temp_index+1}(tu_index))))+app.optimal_parameters{temp_index+1,tu_index}(n);
                        end
                    end

                end
            elseif round(sorted_SOC_array{temp_index+1}(tu_index)/5)*5>round(sorted_SOC_array{temp_index}(SOC_index)/5)*5
                if tu_index-1>=1
                    temp_SOC_count=tu_index-1;
                    if app.Optimization_done(temp_index+1,tu_index) && app.Optimization_done(temp_index+1,temp_SOC_count)
                        %Interpolation zwischen den SOC Punkten %tl_index>tl_index-1
                        interpolated_value_upper_temp=zeros(1,length(app.Boundaries{temp_index+1,tu_index}(:,1)));
                        for n=1:length(app.Boundaries{temp_index+1,tu_index}(:,1))
                            interpolated_value_upper_temp(n)=(app.optimal_parameters{temp_index+1,tu_index}(n)-app.optimal_parameters{temp_index+1,temp_SOC_count}(n))*(((sorted_SOC_array{temp_index}(SOC_index)-sorted_SOC_array{temp_index+1}(temp_SOC_count))/(sorted_SOC_array{temp_index+1}(tu_index)-sorted_SOC_array{temp_index+1}(temp_SOC_count))))+app.optimal_parameters{temp_index+1,temp_SOC_count}(n);
                        end
                    end

                end
            end

            %Extrapolation
            if ~isempty(interpolated_value_upper_temp)
                for n=1:length(app.Boundaries{temp_index,SOC_index}(:,1))
                    %alte Formel
    %                  potential_new_boundaries{extrapolation_count}{n,1}=exp((log(interpolated_value_upper_temp(n))+1/sorted_temperature_array(temp_index)-1/sorted_temperature_array(temp_index+1))/((1+(app.ContinousStep.Value)/100)));
    %                  potential_new_boundaries{extrapolation_count}{n,2}=exp((log(interpolated_value_upper_temp(n))+1/sorted_temperature_array(temp_index)-1/sorted_temperature_array(temp_index+1))/((1-(app.ContinousStep.Value)/100)));
                    %neue Formel
                       potential_new_boundaries{extrapolation_count}{n,1}=exp((log(interpolated_value_upper_temp(n))+(1/sorted_temperature_array(temp_index)-1/sorted_temperature_array(temp_index+1)))*((app.ContinousStep.Value)/100*1.5));
                       potential_new_boundaries{extrapolation_count}{n,2}=exp((log(interpolated_value_upper_temp(n))+(1/sorted_temperature_array(temp_index)-1/sorted_temperature_array(temp_index+1)))*((app.ContinousStep.Value)/100*0.5));
                end
                extrapolation_count=extrapolation_count+1;
            end


        end
    end


    if ~isempty(potential_new_boundaries)
        extrapolated_new_boundaries=cell2mat(potential_new_boundaries{1});
        for m=2:length(potential_new_boundaries)
            for k=1:length(app.Boundaries{temp_index,SOC_index}(:,1))
                if max(extrapolated_new_boundaries(k,1),potential_new_boundaries{m}{k,1})<min(extrapolated_new_boundaries(k,2),potential_new_boundaries{m}{k,2})
                    extrapolated_new_boundaries(k,1)=max(extrapolated_new_boundaries(k,1),potential_new_boundaries{m}{k,1});
                    extrapolated_new_boundaries(k,2)=min(extrapolated_new_boundaries(k,2),potential_new_boundaries{m}{k,2});
                end
            end
        end

        for n=1:length(app.Boundaries{temp_index,SOC_index}(:,1))
            if max(app.Boundaries{temp_index,SOC_index}{n,1},extrapolated_new_boundaries(n,1))<min(app.Boundaries{temp_index,SOC_index}{n,2},extrapolated_new_boundaries(n,2))
                if max(app.Boundaries{temp_index,SOC_index}{n,1},extrapolated_new_boundaries(n,1))>min(app.Boundaries{temp_index,SOC_index}{n,2},extrapolated_new_boundaries(n,2))
                    app.Boundaries{temp_index,SOC_index}{n,1}=max(app.Boundaries{temp_index,SOC_index}{n,1},extrapolated_new_boundaries(n,1));
                    app.Boundaries{temp_index,SOC_index}{n,2}=min(app.Boundaries{temp_index,SOC_index}{n,2},extrapolated_new_boundaries(n,2));
                end
            end
        end
    end
end

