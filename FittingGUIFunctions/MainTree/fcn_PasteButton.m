function fcn_PasteButton(app, event)
global DRT_GUI
global CopyFit
global DRT_Config
if ~isempty(CopyFit)
    DRT_GUI.Fit = CopyFit.Fit;
    set(app.PunkteWegnehmenTextBox,'Value',CopyFit.PunkteWegnehmenTextBox)
    fcn_PunkteWegnehmeButton(app, event);
    set(app.Korrigiert_Punkte_Weg_TextBox,'Value',CopyFit.Korrigiert_Punkte_Weg_TextBox)
    fcn_Korrigiert_Punkte_Weg_Button(app, event);
    set(app.RelaxFFT_PunkteWegnehmenTextBox,'Value',CopyFit.RelaxFFT_PunkteWegnehmenTextBox)
    if isfield(CopyFit,'DRT')
        DRT_GUI.DRT = CopyFit.DRT;
        DRT_Config=DRT_GUI.DRT.Config;
    else
        DRT_GUI.DRT = [];
        DRT_Config= [];
    end
    
    fcn_Alles_Laden(app);
end
end