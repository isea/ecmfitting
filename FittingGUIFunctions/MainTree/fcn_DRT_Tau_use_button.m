function fcn_DRT_Tau_use_button(app, event)
global DRT_GUI;
if isempty(DRT_GUI) || isempty(cell2mat(strfind(fieldnames(DRT_GUI),'Messdaten')))
    return
end
TableCell = get(app.ParamTable,'Data');
if ~isempty(TableCell)
TableCell = cell2struct(TableCell',{'Name','Fix','Value','Min','Max','Lim'});
end
TauCell = get(app.TauTable,'Data');
if ~isempty(TauCell)
TauCell = cell2struct(TauCell',{'tau','used','parname'});
end
%Schreibt die Tau1, Tau2 Werte aus der Tautabelle in die Daten_Fix_Tabelle
for i = 1:numel(TauCell)
    parnummer = find(strcmp(TauCell(i).parname,DRT_GUI.Fit.aktuell_Modell.P_Name(1,:)));
    if ~isempty(parnummer)
        TableCell(parnummer).Value = TauCell(i).tau;
        TableCell(parnummer).Min = TauCell(i).tau / 1.2;
        TableCell(parnummer).Max = TauCell(i).tau * 1.2;
    end
end
TableCell = struct2cell(TableCell)';
set(app.ParamTable,'Data',TableCell)
fcn_PlotFittedParametersButton(app, event)
end