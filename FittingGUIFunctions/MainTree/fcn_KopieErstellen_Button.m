function fcn_KopieErstellen_Button(app, event)
global DRT_GUI
DRT_GUI_prev = DRT_GUI;
BatterieName = inputdlg('neuer BatterieName','Name',[1],{DRT_GUI.Testparameter.Batterie});
choice = BatterieName;
if  isempty(choice) ||strcmp(choice,'') || strcmp(choice{1},DRT_GUI.Testparameter.Batterie) , return,end
copyfile(['output/' DRT_GUI.Testparameter.Batterie],['output/' BatterieName{1}])
addpath(['output/' BatterieName{1}]);

Change_BatterieName_inFolder(app,['output/',BatterieName{1}] ,DRT_GUI.Testparameter.Batterie,BatterieName{1})
DRT_GUI = DRT_GUI_prev ;
fcn_aktualisieren_Button_Callback(app,event)
msgbox('Kopie wurde erstellt')
end