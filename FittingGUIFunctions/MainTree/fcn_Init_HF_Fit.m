function fcn_Init_HF_Fit(app, event)
global DRT_GUI
NullPunkt = find(DRT_GUI.Messdaten.Zreal==min(DRT_GUI.Messdaten.Zreal),1,'first');
%if NullPunkt == 1, return,end
NullPunkt = NullPunkt+4;
Schnittpunkt = find(DRT_GUI.Messdaten.Zreal>min(DRT_GUI.Messdaten.Zreal) & DRT_GUI.Messdaten.Zimg<0,1,'first');
if Schnittpunkt == 1, return,end
Schnittpunkt=Schnittpunkt+4;
PunkteWegVorher = get(app.PunkteWegnehmenTextBox,'Value');
set(app.PunkteWegnehmenTextBox,'Value',[num2str(Schnittpunkt) ':' num2str(numel(DRT_GUI.Messdaten.frequenz)) ])
fcn_PunkteWegnehmenButton_Callback(app, event)
KorrigiertPunkteWegVorher = get(app.Korrigiert_Punkte_Weg_TextBox,'Value');
set(app.Korrigiert_Punkte_Weg_TextBox,'Value','')
fcn_Korrigiert_Punkte_Weg_Button_Callback(app, event)


if ~isempty(find(strcmp(fieldnames(DRT_GUI.Messdaten),'relax_fft'), 1)) && ~isempty(DRT_GUI.Messdaten.relax_fft)
    Relax_fft_PunkteWegVorher = get(app.RelaxFFT_PunkteWegnehmenTextBox,'Value');
    set(app.RelaxFFT_PunkteWegnehmenTextBox,'Value','1:end')
    fcn_RelaxFFT_PunkteWegnehmenButton_Callback(app, event)
end

Parameter_liste = textscan(strrep(DRT_GUI.Fit.aktuell_Modell.ModellCell{1,3},' ',''),'%s','delimiter',',');
Parameter_liste = Parameter_liste{1};
R_index = find(~cellfun(@isempty,regexp(Parameter_liste','(Sigma|R)(2|3|4|5|6|7|8|9|10|_|por)')));
Cser_index = find(~cellfun(@isempty,regexp(Parameter_liste','Cser|CLim(1|2|3|4|5|6|7|8|9|10)|CLim|C_|CLim_')));
HF_index = find(~cellfun(@isempty,regexp(Parameter_liste','Kskin|Lser|RL|PhiL|L0|Rind|Lind|Phiind')));
Zarc1_index =find(~cellfun(@isempty,regexp(Parameter_liste','R1|Tau1|Phi1')));
if isempty(Zarc1_index)
    Zarc1_index = R_index(1);
    R_index(1)=[];
end


TableCell = get(app.ParamTable,'Data');
%TableCell = cell2struct(TableCell',{'Name','Fix','Value','Min','Max','Lim'});
alt_R_fix = TableCell(R_index,2);
alt_R_values = TableCell(R_index,3);
alt_Cser_fix = TableCell(Cser_index,2);
alt_Cser_values = TableCell(Cser_index,3);
alt_Zarc1_fix = TableCell(Zarc1_index,2);
alt_Zarc1_values = TableCell(Zarc1_index,3);
alt_Zarc1_min = TableCell(Zarc1_index,4);
alt_Zarc1_max = TableCell(Zarc1_index,5);

TableCell(R_index,2) = {true};
TableCell(R_index,3) = {0};
TableCell(Cser_index,2) = {true};
TableCell(Cser_index,3) = {1e19};
TableCell(HF_index,2) = {false};
TableCell(Zarc1_index,2) = {false};
TableCell(Zarc1_index,4) = {0};
TableCell(Zarc1_index,5) = {inf};

set(app.ParamTable,'Data',TableCell);

fcn_FitButton_Callback(app, event)
fcn_FitButton_Callback(app, event)
fcn_FitButton_Callback(app, event)
% pause(2)
TableCell = get(app.ParamTable,'Data');

TableCell(R_index,2) = alt_R_fix;
TableCell(R_index,3) = alt_R_values;
TableCell(Cser_index,2) = alt_Cser_fix;
TableCell(Cser_index,3) = alt_Cser_values;
TableCell(HF_index,2) = {true};
TableCell(Zarc1_index,2) = alt_Zarc1_fix;
TableCell(Zarc1_index,3) = alt_Zarc1_values;
TableCell(Zarc1_index,4) = alt_Zarc1_min;
TableCell(Zarc1_index,5) = alt_Zarc1_max;

set(app.ParamTable,'Data',TableCell);

set(app.PunkteWegnehmenTextBox,'Value',PunkteWegVorher)
fcn_PunkteWegnehmenButton_Callback(app, event)


fcn_FitButton_Callback(app, event)

set(app.Korrigiert_Punkte_Weg_TextBox,'Value',KorrigiertPunkteWegVorher)
fcn_Korrigiert_Punkte_Weg_Button_Callback(app, event)


if ~isempty(find(strcmp(fieldnames(DRT_GUI.Messdaten),'relax_fft'), 1)) && ~isempty(DRT_GUI.Messdaten.relax_fft)
    set(app.RelaxFFT_PunkteWegnehmenTextBox,'Value',Relax_fft_PunkteWegVorher);
    fcn_RelaxFFT_PunkteWegnehmenButton_Callback(app, event)
end
end