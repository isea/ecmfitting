function app = fcn_ZarcHNPopup(app,event)
global DRT_GUI
global DRT_Config
for ix=1:numel(app.ZarcHNPopup.Items)
    if app.GUI
        if strcmp(get(app.ZarcHNPopup,'Value'),app.ZarcHNPopup.Items{ix})
            DRT_Config.ZarcHN=ix;
            DRT_GUI.DRT.Config.ZarcHN=ix;
            break;
        end
    else
        if strcmp(app.ZarcHNPopup.Value,app.ZarcHNPopup.Items{ix})
            DRT_Config.ZarcHN=ix;
            DRT_GUI.DRT.Config.ZarcHN=ix;
            break;
        end
    end
end
end