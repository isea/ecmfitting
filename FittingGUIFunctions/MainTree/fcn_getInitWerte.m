function [akt_P_out, std_P_out] = fcn_getInitWerte(app, tempFit)


Folders = {};
BatterieName = get(app.BatterieTextBox,'Value');
if iscell(BatterieName),BatterieName = BatterieName{1}; end
NonDigit = regexp(BatterieName,'\D');
if iscell(NonDigit),NonDigit = NonDigit{1}; end
BatterieTyp = BatterieName(1:NonDigit(end));
if BatterieTyp(end)=='_' , BatterieTyp = BatterieTyp(1:end-1); end
Zustand = get(app.ZustandTextBox,'Value');
akt_SOC = str2num(get(app.SOCTextBox,'Value'));
akt_T = str2num(get(app.TemperaturTextBox,'Value'));

Batterien = {};

if isempty(Zustand ), Zustand = 'default'; end
f = dir(['output/' BatterieTyp '*']);
for i = 1:numel(f)
    if ~strcmp(f(i).name,'.') && ~strcmp(f(i).name,'..') && f(i).isdir
        Batterien = [Batterien;f(i).name];
    end
end
for j = 1:numel(Batterien)
    f = dir(['output/' Batterien{j} '/'  Zustand '/*grad']);
    for i = 1:numel(f)
        if ~strcmp(f(i).name,'.') && ~strcmp(f(i).name,'..') && f(i).isdir
            if isempty(Folders) | ~ismember(Folders,f(i).name)
                Folders = [Folders;f(i).name];
            end
        end
    end
end
Temperaturen = zeros(size(Folders));
SOCs = [];
SOCFiles = {};
for i = 1:numel(Folders)
    Temperaturen(i) = str2num(strrep(strrep(Folders{i},'grad',''),'m','-'));
    for k = 1:numel(Batterien)
        f = dir(['output/' Batterien{k} '/'  Zustand '/' Folders{i} '/*_*SOC_Modell.mat']);
        for j = 1:numel(f)
            if ~strcmp(f(j).name,'.') && ~strcmp(f(j).name,'..') && ~f(j).isdir
                SOCFiles = [SOCFiles;strrep(strrep(strrep(strrep(strrep(f(j).name,Folders{i},''),Batterien{k} ,''),  Zustand,''),'_',''),'Modell.mat','')];
            end
        end
    end
end
for j = 1:numel(SOCFiles)
    if ~sum(find(SOCs == str2num(strrep(strrep(SOCFiles{j},'SOC',''),'m','-'))))
        SOCs = [SOCs ; str2num(strrep(strrep(SOCFiles{j},'SOC',''),'m','-'))];
    end
end

Temperaturen = sort(Temperaturen,1);
SOCs = sort(SOCs,1);
akt_x(1,1) = (akt_T - min(Temperaturen) ) / (max(Temperaturen) - min(Temperaturen));
akt_x(1,2) = (akt_SOC - min(SOCs) ) / (max(SOCs) - min(SOCs));

FitStruct = struct([]);

for batnr = 1:numel(Batterien)
    for i = 1:numel(Temperaturen)
        for j = 1:numel(SOCs)
            TString = [strrep(num2str(Temperaturen(i)),'-','m') 'grad'];
            SOCString = [strrep(num2str(SOCs(j)),'-','m') 'SOC'];
            f = dir(['output/' Batterien{batnr} '/'  Zustand '/' TString '/*_' SOCString '_Modell.mat']);
            if ~isempty(f)
                tempModel = load(['output/' Batterien{batnr} '/'  Zustand '/' TString '/' f.name]);
                if ~isempty(tempModel) && sum(ismember(fieldnames(tempModel),'Fit'))
                    tempModel = tempModel.Fit;
                    if ~isempty(tempModel) &&  strcmp(tempModel.Modell.Modellname,tempFit.aktuell_Modell.Modellname) &&...
                            numel(tempModel.Parameter) == numel(tempFit.Parameter) && sum(ismember(fieldnames(tempModel),'gueltig')) && tempModel.gueltig &&...
                            ~(strcmp(Batterien{batnr},BatterieName) && Temperaturen(i)== akt_T && SOCs(j)==akt_SOC)
                        FitStruct(numel(FitStruct)+1).Batterie = Batterien{batnr};
                        FitStruct(end).x(1,1) = (Temperaturen(i) - min(Temperaturen) ) / (max(Temperaturen) - min(Temperaturen));
                        FitStruct(end).x(1,2) = (SOCs(j) - min(SOCs) ) / (max(SOCs) - min(SOCs));
                        FitStruct(end).Temperatur = Temperaturen(i);
                        FitStruct(end).SOC = SOCs(j);
                        FitStruct(end).Parameter = tempModel.Parameter;
                        FitStruct(end).Distance = sqrt(sum((akt_x-FitStruct(end).x).^2));
                    end
                end
            end
            
        end
    end
end
if isempty(FitStruct)
    akt_P_out=[]; std_P_out=[];
    return
end
distances = cell2mat({(FitStruct(:).Distance)});
sorted_distances = sort(distances);
if numel(FitStruct)>9
    FitStruct = FitStruct(distances <= sorted_distances(ceil(numel(distances)*0.6)));
end
distances = cell2mat({(FitStruct(:).Distance)});
if numel(distances)>5 , nachbaranzahl = 5; else  nachbaranzahl = numel(distances)-1;end
if nachbaranzahl<1
    akt_P_out=[]; std_P_out=[];
    
    return,
end
sorted_distances = sort(distances);
closest = find(distances <= sorted_distances(nachbaranzahl));

% Gradient y(x) an der stelle x0 (gesuchter Zustand) berechnen
for p = 1:numel(tempFit.Parameter)
    y=[];  dist=[]; SOC_y = []; T_y=[]; guete = [];
    for k = 1:numel(FitStruct)
        A=[];b=[];
        for c = closest
            if c == k, continue,end
            A(end+1,:) = (FitStruct(c).x-FitStruct(k).x) ...
                .* 10000.^-FitStruct(c).Distance;
            b(end+1,:) = (FitStruct(c).Parameter(p)-FitStruct(k).Parameter(p)) .* 10000.^-FitStruct(c).Distance;
        end
        for i = 1:size(A,2)
            if isempty(find(A(:,i)~=0))
                A(end+1,:) = [zeros(1,i-1) 1 zeros(1,size(A,2)-i)];
                b(end+1,:) = 0;
            end
        end
        gradY = A\b;
        if isempty(gradY),continue,end
        y(end+1,1) = FitStruct(k).Parameter(p)+(akt_x-FitStruct(k).x)*gradY;
        dist(end+1,1) = FitStruct(k).Distance;
        SOC_y(end+1,1) = FitStruct(k).SOC;
        T_y(end+1,1) = FitStruct(k).Temperatur;
        guete(end+1,1) = 10000^-FitStruct(k).Distance;
        if y(end) < tempFit.aktuell_Modell.ModellCell{5}{p} || y(end) > tempFit.aktuell_Modell.ModellCell{6}{p}
            y(end) = [];
            dist(end) = [];
            guete(end) = [];
            SOC_y(end) = [];
            T_y(end) = [];
        end
    end
    % num2cell([y dist guete./sum(guete)*100 SOC_y T_y])
    y0=sum(y.*(10000.^-dist))/sum(10000.^-dist);
    akt_P_out(p) = y0;
    alle_P = cell2mat({FitStruct.Parameter}');
    std_P_out(p) = std(alle_P(:,p));
    
end
end