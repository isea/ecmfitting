function fcn_GuessButton(app, event)
global DRT_GUI;
if isempty(DRT_GUI) || isempty(cell2mat(strfind(fieldnames(DRT_GUI),'Messdaten')))
    return
end
TableCell = get(app.ParamTable,'Data');
if ~isempty(TableCell)
TableCell = cell2struct(TableCell',{'Name','Fix','Value','Min','Max','Lim'});
end
TauCell = get(app.TauTable,'Data');
if ~isempty(TauCell)
TauCell = cell2struct(TauCell',{'tau','used','parname'});
end
[akt_P std_p] = fcn_getInitWerte(app,DRT_GUI.Fit);
if isempty(akt_P)
    akt_P = cell2mat({TableCell.Value});
end
DRT_GUI.Fit.GrenzenBandbreite = 0.1*ones(numel(DRT_GUI.Fit.Parameter),2);
%Schreibt die Tau1, Tau2 Werte aus der Tautabelle in die Daten_Fix_Tabelle
for i = 1:numel(TauCell)
    parnummer = find(strcmp(TauCell(i).parname,DRT_GUI.Fit.aktuell_Modell.P_Name(1,:)));
    if ~isempty(parnummer)
        akt_P(parnummer) = TauCell(i).tau;
    end
end
DRT_GUI.Fit.Guessed_Parameters = akt_P;
DRT_GUI.Fit.Guessed_std = std_p;
for i_p = 1:numel(akt_P)
    if ~TableCell(i_p).Fix
        TableCell(i_p).Value = akt_P(i_p) ;
        if ~isempty(find(strcmp(TableCell(i_p).Name,{TauCell.parname}), 1))
            TableCell(i_p).Min = akt_P(i_p)/ 1.5;
            TableCell(i_p).Max = akt_P(i_p)* 1.5;
        end
    end
end
TableCell = struct2cell(TableCell)';
set(app.ParamTable,'Data',TableCell)
fcn_PlotFittedParametersButton_Callback(app, event)
end