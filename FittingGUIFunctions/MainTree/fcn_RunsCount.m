function [start, val, len, rn] = fcn_RunsCount(v)

 n = length(v);
 val = zeros(1,n);           % run value
 len = zeros(1,n);           % run length
 start = zeros(1,n);         % pos. in v where run starts
 start(1) = 1;
 rk = 1;                     % number in run
 rn = 1;                     % number of runs
 for k = 2:n
     if  v(k) == v(k-1)      % in run
         rk = rk+1;
     else                    % end of run
         val(rn) = v(k-1);
         len(rn) = rk;
         rk = 1;             % v(k) is start of
         rn = rn+1;          % next run
         start(rn) = k;      % position of start in v
     end
 end
 val(rn) = v(n);             % last run
 len(rn) = n - sum(len);
end

