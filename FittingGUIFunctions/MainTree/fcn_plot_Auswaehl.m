function fcn_plot_Auswaehl(app, plotDaten, Fdaten, Ddaten, Plot_Title, Typ)
global DRT_GUI
FarbenLaden()
switch Typ
    case 'Nyquist'

        app.axes1.YLim=[floor(min(plotDaten.Zimg*100000))/100000, ceil(max(plotDaten.Zimg*100000))/100000];
        app.axes1.XLimMode='auto';
        plot(app.axes1,plotDaten.Zreal,plotDaten.Zimg,'o-','DisplayName','Measurement','color',RWTHBlau,'LineWidth',2);
        grid(app.axes1,'on'); hold(app.axes1,'on'); axis(app.axes1,'square'); set(app.axes1,'ydir', 'reverse'); %axis(app.axes1,'equal');
        if ~isempty(find(strcmp(fieldnames(plotDaten),'relax_fft'),1)) && ~isempty(plotDaten.relax_fft)
            plot(app.axes1,plotDaten.relax_fft.Zreal+plotDaten.relax_fft.Zreal_korrektur,plotDaten.relax_fft.Zimg,'o-','DisplayName','RelaxFFT','color',RWTHSchwarz,'LineWidth',1,'MarkerSize',7);
        end
        app.axes1.XLabel.Interpreter='latex'; app.axes1.YLabel.Interpreter='latex'; app.axes1.XLabel.String='$\Re\{\underline Z\}$ in $\Omega$'; app.axes1.YLabel.String='$\Im\{\underline Z\}$ in $\Omega$'; %title(Plot_Title,'Interpreter','none');
        h1 = legend(app.axes1,'show');
        set(h1,'Interpreter','none','Location','NorthWest');
        axis(app.axes1,'equal')
        
    case 'Messung'

        app.axes1.YLim=[floor(min(plotDaten.Zimg*100000))/100000, ceil(max(plotDaten.Zimg*100000))/100000];
        app.axes1.XLimMode='auto';
        plot(app.axes1,plotDaten.Zreal(plotDaten.aktiv==1),plotDaten.Zimg(plotDaten.aktiv==1),'o','DisplayName','Measurement','color',RWTHBlau,'LineWidth',1,'MarkerSize',7);hold(app.axes1,'on');
        plot(app.axes1,plotDaten.Zreal(plotDaten.aktiv==0),plotDaten.Zimg(plotDaten.aktiv==0),'o','DisplayName','ignored','color',RWTHMittelgrau,'LineWidth',1,'MarkerSize',7); % 'Marker', 'LineStyle','-'
        if ~isempty(find(strcmp(fieldnames(plotDaten),'relax_fft'),1)) && ~isempty(plotDaten.relax_fft)
            plot(app.axes1,plotDaten.relax_fft.Zreal(plotDaten.relax_fft.aktiv==1)+plotDaten.relax_fft.Zreal_korrektur,plotDaten.relax_fft.Zimg(plotDaten.relax_fft.aktiv==1),'o','DisplayName','RelaxFFT','color',RWTHSchwarz,'LineWidth',1,'MarkerSize',7);hold(app.axes1,'on');
            plot(app.axes1,plotDaten.relax_fft.Zreal(plotDaten.relax_fft.aktiv==0)+plotDaten.relax_fft.Zreal_korrektur,plotDaten.relax_fft.Zimg(plotDaten.relax_fft.aktiv==0),'o','DisplayName','ignoriert','color',RWTHDunkelgrau,'LineWidth',1,'MarkerSize',7); % 'Marker', 'LineStyle','-'
        end
        grid(app.axes1,'on'); hold(app.axes1,'on'); axis(app.axes1,'square'); set(app.axes1,'ydir', 'reverse'); %axis(app.axes1,'equal');
        app.axes1.XLabel.Interpreter='latex'; app.axes1.YLabel.Interpreter='latex'; app.axes1.XLabel.String='$\Re\{\underline Z\}$ in $\Omega$'; app.axes1.YLabel.String='$\Im\{\underline Z\}$ in $\Omega$'; %title(Plot_Title,'Interpreter','none');
        h1 = legend(app.axes1,'show');
        set(h1,'Interpreter','none','Location','NorthWest');
        axis(app.axes1,'equal')
      
    case 'Hochpass'
        
        hold off
        plot(plotDaten.Zreal(plotDaten.HF_Ast==1),plotDaten.Zimg(plotDaten.HF_Ast==1),'o','DisplayName','Measurement','color',RWTHBlau,'LineWidth',1,'MarkerSize',7);hold on;
        plot(plotDaten.Zreal(plotDaten.HF_Ast==0),plotDaten.Zimg(plotDaten.HF_Ast==0),'o','DisplayName','ignored','color',RWTHMittelgrau,'LineWidth',1,'MarkerSize',7); % 'Marker', 'LineStyle','-'
        grid on; axis square; axis equal; set(gca,'ydir', 'reverse');
        xlabel('$\Re\{\underline Z\}$ in $\Omega$','Interpreter','latex');ylabel('$\Im\{\underline Z\}$ in $\Omega$','Interpreter','latex'); %title(Plot_Title,'Interpreter','none');
        h1 = legend(Plot_Title,'Location','NorthWest');
        set(h1,'Interpreter','none');
        axis equal
        
    case 'Fit'
        
%         app.axes1.YLim=[floor(min(plotDaten.Zimg*100000))/100000, ceil(max(plotDaten.Zimg*100000))/100000];
%         app.axes1.XLim = [floor(min(plotDaten.Zreal*100000))/100000 (floor(min(plotDaten.Zreal*100000))/100000+app.axes1.YLim(2)-app.axes1.YLim(1))];
        app.axes1.XLim = [min(floor(min(plotDaten.Zreal*100000))/100000, floor(min(Fdaten.Zreal*100000))/100000) max((ceil(max(plotDaten.Zreal*100000))/100000),ceil(max(Fdaten.Zreal*100000))/100000)];
        app.axes1.YLimMode='auto';
        plot(app.axes1,plotDaten.Zreal(plotDaten.aktiv==1),plotDaten.Zimg(plotDaten.aktiv==1),'o','DisplayName','Measurement','color',RWTHBlau,'LineWidth',1,'MarkerSize',7);hold(app.axes1,'on');
        plot(app.axes1,Fdaten.Zreal,Fdaten.Zimg,'x','DisplayName','Fit','color',RWTHRot,'LineWidth',1,'MarkerSize',7);
        plot(app.axes1,plotDaten.Zreal(plotDaten.aktiv==0),plotDaten.Zimg(plotDaten.aktiv==0),'o','DisplayName','ignored','color',RWTHMittelgrau,'LineWidth',1,'MarkerSize',7); % 'Marker', 'LineStyle','-'
        if ~isempty(find(strcmp(fieldnames(plotDaten),'relax_fft'),1)) && ~isempty(plotDaten.relax_fft)
            plot(app.axes1,plotDaten.relax_fft.Zreal(plotDaten.relax_fft.aktiv==1)+plotDaten.relax_fft.Zreal_korrektur,plotDaten.relax_fft.Zimg(plotDaten.relax_fft.aktiv==1),'o','DisplayName','RelaxFFT','color',RWTHSchwarz,'LineWidth',1,'MarkerSize',7);hold(app.axes1,'on');
            plot(app.axes1,plotDaten.relax_fft.Zreal(plotDaten.relax_fft.aktiv==0)+plotDaten.relax_fft.Zreal_korrektur,plotDaten.relax_fft.Zimg(plotDaten.relax_fft.aktiv==0),'o','DisplayName','ignoriert','color',RWTHDunkelgrau,'LineWidth',1,'MarkerSize',7); % 'Marker', 'LineStyle','-'
        end
        grid(app.axes1,'on'); hold(app.axes1,'on');  set(app.axes1,'ydir', 'reverse'); %axis(app.axes1,'equal'); %axis(app.axes1,'square');
        app.axes1.XLabel.Interpreter='latex'; app.axes1.YLabel.Interpreter='latex'; app.axes1.XLabel.String='$\Re\{\underline Z\}$ in $\Omega$'; app.axes1.YLabel.String='$\Im\{\underline Z\}$ in $\Omega$'; %title(Plot_Title,'Interpreter','none');
        h1 = legend(app.axes1,'Measurement','Fit','Location','NorthWest');
        set(h1,'Interpreter','none');
        axis(app.axes1,'equal')
        p = Fdaten.Parameter;
        w = plotDaten.omega;
        for TauNr = find(~cellfun(@isempty,regexp(Fdaten.aktuell_Modell.P_Name(1,:),'Tau\d')))
            ZarcNr = str2mat(strrep(Fdaten.aktuell_Modell.P_Name{1,TauNr},'Tau',''));
            RNr=find(strcmp(Fdaten.aktuell_Modell.P_Name(1,:)',['R' num2str(ZarcNr)]));
            if ~isempty(ZarcNr) && ~isempty(RNr)
                PhiNr=find(strcmp(Fdaten.aktuell_Modell.P_Name(1,:)',['Phi' num2str(ZarcNr)]));
                if ~isempty(PhiNr)
                    schonen = [TauNr RNr PhiNr];
                else
                    schonen = [TauNr RNr];
                end
                formel_temp = strrep(strrep(Fdaten.aktuell_Modell.Rechnen_Modell,' ',''),'.','');
                for Alle_R = find(~cellfun(@isempty,regexp(Fdaten.aktuell_Modell.P_Name(1,:),'R*')))
                    if sum(schonen==Alle_R)==0
                        formel_temp = strrep(formel_temp,Fdaten.aktuell_Modell.P_Name{2,Alle_R},'0');
                    end
                end
                for Alle_C = find(~cellfun(@isempty,regexp(Fdaten.aktuell_Modell.P_Name(1,:),'C*')))
                    if sum(schonen==Alle_C)==0
                        formel_temp = strrep(formel_temp,['1/(1i*w*' Fdaten.aktuell_Modell.P_Name{2,Alle_C} ')'],'0');
                        formel_temp = strrep(formel_temp,['1/(i*w*' Fdaten.aktuell_Modell.P_Name{2,Alle_C} ')'],'0');
                    end
                end
                for Alle_L = find(~cellfun(@isempty,regexp(Fdaten.aktuell_Modell.P_Name(1,:),'L*')))
                    if sum(schonen==Alle_L)==0
                        formel_temp = strrep(formel_temp,['1i*w*' Fdaten.aktuell_Modell.P_Name{2,Alle_L}],'0');
                        formel_temp = strrep(formel_temp,['i*w*' Fdaten.aktuell_Modell.P_Name{2,Alle_L} ],'0');
                    end
                end
                formel_temp = strrep(strrep(strrep(formel_temp,'*','.*'),'/','./'),'^','.^');
                w = 1./p(TauNr);
                real_offset = real(eval(Fdaten.aktuell_Modell.Rechnen_Modell)) - ...
                    p(RNr)/2;
                w = plotDaten.omega;
                if isempty(real_offset) || isnan(real_offset), real_offset =0;end
                temp_real = real(eval(formel_temp))+real_offset;
                temp_img = imag(eval(formel_temp));
                %                 plot(temp_real,temp_img,'k')
            end
        end

    case 'HF_Fit'

        hold off
        plot(plotDaten.Zreal(plotDaten.HF_Ast==1),plotDaten.Zimg(plotDaten.HF_Ast==1),'o','DisplayName','Measurement','color',RWTHBlau,'LineWidth',1,'MarkerSize',7);hold on;
        plot(Fdaten.Zreal,Fdaten.Zimg,'x','DisplayName','Fit','color',RWTHRot,'LineWidth',1,'MarkerSize',7);
        plot(plotDaten.Zreal(plotDaten.HF_Ast==0),plotDaten.Zimg(plotDaten.HF_Ast==0),'o','DisplayName','ignored','color',RWTHMittelgrau,'LineWidth',1,'MarkerSize',7); % 'Marker', 'LineStyle','-'
        if ~isempty(find(strcmp(fieldnames(plotDaten),'relax_fft'),1)) && ~isempty(plotDaten.relax_fft)
            plot(plotDaten.relax_fft.Zreal(plotDaten.relax_fft.aktiv==1)+plotDaten.relax_fft.Zreal_korrektur,plotDaten.relax_fft.Zimg(plotDaten.relax_fft.aktiv==1),'o','DisplayName','RelaxFFT','color',RWTHSchwarz,'LineWidth',1,'MarkerSize',7);hold on;
            plot(plotDaten.relax_fft.Zreal(plotDaten.relax_fft.aktiv==0)+plotDaten.relax_fft.Zreal_korrektur,plotDaten.relax_fft.Zimg(plotDaten.relax_fft.aktiv==0),'o','DisplayName','ignoriert','color',RWTHDunkelgrau,'LineWidth',1,'MarkerSize',7); % 'Marker', 'LineStyle','-'
        end
        grid on; axis square; axis equal; set(gca,'ydir', 'reverse');
        xlabel('$\Re\{\underline Z\}$ in $\Omega$','Interpreter','latex');ylabel('$\Im\{\underline Z\}$ in $\Omega$','Interpreter','latex'); %title(Plot_Title,'Interpreter','none');
        %xlim([min(Fdaten.Zreal) max(Fdaten.Zreal)]);
        ylim([min(Fdaten.Zimg) max(Fdaten.Zimg)]);
        h1 = legend('Messung','Fit','Location','NorthWest');
        set(h1,'Interpreter','none');
        axis equal
       
    case 'korrigiert'
        
        hold off
        plot(plotDaten.Zreal(plotDaten.aktiv==1),plotDaten.Zimg(plotDaten.aktiv==1),'o','DisplayName','Measurement','color',RWTHBlau,'LineWidth',1,'MarkerSize',7);hold on;
        plot(plotDaten.Zreal(plotDaten.aktiv==0),plotDaten.Zimg(plotDaten.aktiv==0),'o','DisplayName','ignored','color',RWTHMittelgrau,'LineWidth',1,'MarkerSize',7); % 'Marker', 'LineStyle','-'
        grid on; axis square; axis equal; set(gca,'ydir', 'reverse');
        xlabel('$\Re\{\underline Z\}$ in $\Omega$','Interpreter','latex');ylabel('$\Im\{\underline Z\}$ in $\Omega$','Interpreter','latex'); %title(Plot_Title,'Interpreter','none');
        %h1 = legend(Plot_Title,'Location','NorthWest');
        %        set(h1,'Interpreter','none');
        axis equal
        
%    case 'Korrigiert_Fit'
%       
%        app.axes2.YLim=[floor(min(plotDaten.Zimg*100000))/100000, ceil(max(plotDaten.Zimg*100000))/100000];
%       app.axes2.XLimMode='auto';
%        plot(app.axes2,plotDaten.Zreal(plotDaten.aktiv==1),plotDaten.Zimg(plotDaten.aktiv==1),'o','DisplayName','Measurement','color',RWTHBlau,'LineWidth',1,'MarkerSize',7);hold(app.axes2,'on');
%        plot(app.axes2,Fdaten.korrigiert.Zreal,Fdaten.korrigiert.Zimg,'xr');
%        plot(app.axes2,plotDaten.Zreal(plotDaten.aktiv==0),plotDaten.Zimg(plotDaten.aktiv==0),'o','DisplayName','ignored','color',RWTHMittelgrau,'LineWidth',1,'MarkerSize',7); % 'Marker', 'LineStyle','-'
%        grid(app.axes2,'on'); hold(app.axes1,'on'); set(app.axes2,'ydir', 'reverse'); %axis(app.axes2,'equal');
%        app.axes2.XLabel.Interpreter='latex'; app.axes2.YLabel.Interpreter='latex'; app.axes2.XLabel.String='$\Re\{\underline Z\}$ in $\Omega$'; app.axes2.YLabel.String='$\Im\{\underline Z\}$ in $\Omega$'; %title(Plot_Title,'Interpreter','none');
%        if ~isempty(find(strcmp(fieldnames(plotDaten),'spaps'),1)) && ~isempty(plotDaten.spaps)
%           plot(app.axes2,plotDaten.spaps.Zreal,plotDaten.spaps.Zimg,'-','DisplayName','ignoriert','color',RWTHBlau,'LineWidth',2,'MarkerSize',7)
%           h1 = legend(app.axes2,'MF-Messung','MF-Fit','Location','South');
%        else
%            h1 = legend(app.axes2,'MF-Messung','MF-Fit','Location','South');
%        end
%        set(h1,'Interpreter','none');
%        % axis(app.axes2,'equal')
%        p = Fdaten.Parameter;
%        w = plotDaten.omega;
%       if isempty(Fdaten.aktuell_Modell.ModellCell{8})
%           for TauNr = find(~cellfun(@isempty,regexp(Fdaten.aktuell_Modell.P_Name(1,:),'Tau\d')))
%               ZarcNr = str2mat(strrep(Fdaten.aktuell_Modell.P_Name{1,TauNr},'Tau',''));
%               RNr=find(strcmp(Fdaten.aktuell_Modell.P_Name(1,:)',['R' num2str(ZarcNr)]));
%               if ~isempty(ZarcNr) && ~isempty(RNr)
%                   PhiNr=find(strcmp(Fdaten.aktuell_Modell.P_Name(1,:)',['Phi' num2str(ZarcNr)]));
%                   if ~isempty(PhiNr)
%                       schonen = [TauNr RNr PhiNr];
%                    else
%                       schonen = [TauNr RNr];
%                   end
%                   formel_temp = Fdaten.aktuell_Modell.Rechnen_Modell_komp;
%                   for Alle_R = find(~cellfun(@isempty,regexp(Fdaten.aktuell_Modell.P_Name(1,:),'R*')))
%                       if sum(schonen==Alle_R)==0
%                           formel_temp = strrep(formel_temp,Fdaten.aktuell_Modell.P_Name{2,Alle_R},'0');
%                       end
%                   end
%                   w = 1./p(TauNr);
%                   real_offset = real(eval(Fdaten.aktuell_Modell.Rechnen_Modell_komp)) - p(RNr)/2;
%                   w = plotDaten.omega;
%                   if isempty(real_offset) , real_offset =0;end
%                    temp_real = real(eval(formel_temp))+real_offset;
%                   temp_img = imag(eval(formel_temp));
%                   %if sum(abs(temp_img))>0
%                   %    plot(app.axes2,temp_real,temp_img,'k')
%                   %end
%                end
%            end
%        end
       
    case 'DRT'
        cla(app.axes3)
        
        hold(app.axes3,'on');
        linien = get(app.axes3,'Children');
        for i = 1:numel(linien)
            set(linien(i),'linewidth',0.5)
            set(linien(i),'color',[0.5 0.5 0.5])
        end
            %copyobj(linien,app.axes4)
        
        app.axes3.XLimMode='auto'; app.axes3.YLimMode='auto';
        H1=plot(app.axes3,Ddaten.tau(Ddaten.aktiv),Ddaten.g(Ddaten.aktiv)); hold(app.axes3,'on');
        app.axes3.XScale='log';
        set(H1,'color',RWTHBlau,'linewidth',2,'DisplayName','DRT')
        set(app.axes3,'YColor',RWTHBlau)
        app.axes3.YLabel.Interpreter='latex'; app.axes3.YLabel.String='DRT$(\underline Z)$';
        grid(app.axes3,'on');
        yyaxis(app.axes3,'right')
        hold(app.axes3,'off')
        H2=plot(app.axes3,plotDaten.tau(plotDaten.aktiv),plotDaten.Zimg(plotDaten.aktiv)); hold(app.axes3,'on');
        set(H2,'color',RWTHOrange,'linewidth',2,'DisplayName','Imaginärteil')
        set(app.axes3,'YColor',RWTHOrange)
        app.axes3.YLabel.Interpreter='latex'; app.axes3.YLabel.String='$\Im\{\underline Z\}$ in $\Omega$';
        set(app.axes3,'YDir','reverse')
        app.axes3.XLabel.Interpreter='latex'; app.axes3.XLabel.String='$\tau$ in s';
        %set(app.axes3,'ButtonDownFcn',{@ClickInDRT});
        for ch_i = get(app.axes3,'Children')
            set(ch_i,'HitTest','off');
        end
        yyaxis(app.axes3,'left')
        H3=plot(app.axes3,Ddaten.peaks.tau,Ddaten.peaks.g,'o','DisplayName','Peaks','color',RWTHSchwarz,'LineWidth',1,'MarkerSize',7);
        %semilogx(Fdaten.tau(Fdaten.aktiv),Fdaten.g(Fdaten.aktiv),'-k');
        %         tau_diff = Ddaten.tau(1:end-1); g_diff = diff(Ddaten.g);
        %         index = find(Ddaten.aktiv(1:end-1) & abs([0 diff(g_diff)])<(mean(diff(g_diff))+5*std(diff(g_diff)))) ;
        %         g_diff = g_diff(index);tau_diff = tau_diff(index);
        %         tau_diff2 = Ddaten.tau(1:end-2); g_diff2 = diff(diff(Ddaten.g));
        %         index = find(Ddaten.aktiv(1:end-2) & abs([0 diff(g_diff2)])<(mean(diff(g_diff2))+5*std(diff(g_diff2)))) ;
        %         g_diff2 = g_diff2(index);tau_diff2 = tau_diff2(index);
        %         tau_diff3 = Ddaten.tau(1:end-2); g_diff3 = diff(diff(diff(Ddaten.g)));
        %         index = find(Ddaten.aktiv(1:end-3) & abs([0 diff(g_diff3)])<(mean(diff(g_diff3))+5*std(diff(g_diff3)))) ;
        %         g_diff3 = g_diff3(index);tau_diff3 = tau_diff3(index);
        %         semilogx(tau_diff,max(Ddaten.g(Ddaten.aktiv))/max(g_diff)*abs(g_diff),'-r');
        %         %semilogx(tau_diff2,max(Ddaten.g(Ddaten.aktiv))/max(g_diff2)*g_diff2,'-r');
        %         semilogx(tau_diff3,max(Ddaten.g(Ddaten.aktiv))/max(g_diff3)*g_diff3,'-g');
        if sum(ismember(fieldnames(Ddaten.peaks),'used')) && sum(Ddaten.peaks.used)>0
            yyaxis(app.axes3,'left')
            H4=plot(app.axes3,Ddaten.peaks.tau(logical(Ddaten.peaks.used)),Ddaten.peaks.g(logical(Ddaten.peaks.used)),'o','DisplayName','benutzte Peaks','color',RWTHRot,'LineWidth',1,'MarkerSize',7);
            used_ParNr = [];
            for ip = find(Ddaten.peaks.used)'
                used_ParNr = [used_ParNr ; find(strcmp(Ddaten.peaks.used_parname(ip),DRT_GUI.Fit.aktuell_Modell.P_Name(1,:))) ];
            end
            yyaxis(app.axes3,'left')
            H5=plot(app.axes3,Ddaten.peaks.tau(logical(Ddaten.peaks.used)),Ddaten.peaks.g(logical(Ddaten.peaks.used)),'o','DisplayName','benutzte Peaks','color',RWTHRot,'LineWidth',1,'MarkerSize',7);
            if numel(used_ParNr)==numel(find(Ddaten.peaks.used))
                yyaxis(app.axes3,'left')
                H6=plot(app.axes3,DRT_GUI.Fit.Parameter(used_ParNr),Ddaten.peaks.g(logical(Ddaten.peaks.used)),'x','DisplayName','Fit','color',RWTHBlau,'LineWidth',1,'MarkerSize',7);
            end
        end
        if sum(ismember(fieldnames(DRT_GUI),'DRT')) && ~isempty(DRT_GUI.DRT) && ...
                sum(ismember(fieldnames(DRT_GUI.DRT),'EI_DRT')) && ...
                ~isempty(DRT_GUI.DRT.EI_DRT) && ...
                sum(ismember(fieldnames(DRT_GUI.DRT.EI_DRT),'ProzessFit')) ...
                && ~isempty(DRT_GUI.DRT.EI_DRT.ProzessFit) && ...
                sum(ismember(fieldnames(DRT_GUI.DRT.EI_DRT.ProzessFit),'tau')) ...
                && (~isempty(DRT_GUI.DRT.EI_DRT.ProzessFit.tau) || ( ...
                sum(ismember(fieldnames(DRT_GUI.DRT.EI_DRT.ProzessFit),'val')) && ...
                ~isempty(DRT_GUI.DRT.EI_DRT.ProzessFit.val)...
                ))
            DRT_Summe=zeros(size(Ddaten.tau));
            for iP = 1:numel(DRT_GUI.DRT.EI_DRT.ProzessFit.tau)
                if ~ismember('ZarcHN',fieldnames(DRT_GUI.DRT.Config))
                    DRT_GUI.DRT.Config.ZarcHN=1;
                end
                if DRT_GUI.DRT.Config.ZarcHN==1
                    DRT_einzeln = fcn_Calc_Zarc_DRT(DRT_GUI.DRT.EI_DRT.ProzessFit.r(iP),...
                        DRT_GUI.DRT.EI_DRT.ProzessFit.tau(iP),...
                        DRT_GUI.DRT.EI_DRT.ProzessFit.phi(iP),...
                        Ddaten.tau);
                elseif DRT_GUI.DRT.Config.ZarcHN==2 || DRT_GUI.DRT.Config.ZarcHN==3
                    DRT_einzeln = fcn_Calc_HN_DRT(DRT_GUI.DRT.EI_DRT.ProzessFit.r(iP),...
                        DRT_GUI.DRT.EI_DRT.ProzessFit.tau(iP),...
                        DRT_GUI.DRT.EI_DRT.ProzessFit.phi(iP),...
                        Ddaten.tau);
                elseif DRT_GUI.DRT.Config.ZarcHN==4 % PorEl
                    DRT_einzeln = fcn_Calc_PorEl_DRT(DRT_GUI.DRT.EI_DRT.ProzessFit.r(iP),...
                        DRT_GUI.DRT.EI_DRT.ProzessFit.tau(iP),...
                        DRT_GUI.DRT.EI_DRT.ProzessFit.phi(iP),...
                        Ddaten.tau);
                elseif DRT_GUI.DRT.Config.ZarcHN==5 % MF-Model
                    break;
                end
                if DRT_GUI.DRT.EI_DRT.ProzessFit.used(iP)
                    yyaxis(app.axes3,'left')
                    H7=plot(app.axes3,Ddaten.tau(Ddaten.aktiv),DRT_einzeln(Ddaten.aktiv),'--','color',RWTHRot,'LineWidth',2);
                else
                    yyaxis(app.axes3,'left')
                    H8=plot(app.axes3,Ddaten.tau(Ddaten.aktiv),DRT_einzeln(Ddaten.aktiv),'--','color',RWTHSchwarz,'LineWidth',2);
                end
                DRT_Summe = DRT_Summe+DRT_einzeln;
            end
            if DRT_GUI.DRT.Config.ZarcHN==5 && sum(ismember(fieldnames(DRT_GUI.DRT.EI_DRT.ProzessFit),'ElementFormeln')) && numel(DRT_GUI.DRT.EI_DRT.ProzessFit.ElementFormeln)>0
                for iP = 1:numel(DRT_GUI.DRT.EI_DRT.ProzessFit.ElementFormeln)
                    DRT_einzeln = fcn_Calc_Model_DRT( DRT_GUI.DRT.EI_DRT.ProzessFit.ElementFormeln{iP},DRT_GUI.DRT.EI_DRT.ProzessFit.val',Ddaten.tau);
                    if DRT_GUI.DRT.EI_DRT.ProzessFit.used(iP)
                        yyaxis(app.axes3,'left')
                        H9=plot(app.axes3,Ddaten.tau(Ddaten.aktiv),DRT_einzeln(Ddaten.aktiv),'--','color',RWTHRot,'LineWidth',2);
                    else
                        yyaxis(app.axes3,'left')
                        H10=plot(app.axes3,Ddaten.tau(Ddaten.aktiv),DRT_einzeln(Ddaten.aktiv),'--','color',RWTHSchwarz,'LineWidth',2);
                    end
                    DRT_Summe = DRT_Summe+DRT_einzeln;
                end
            end
            if ismember('OrigIndex',fieldnames(Ddaten))
                yyaxis(app.axes3,'left')
                H11=plot(app.axes3,Ddaten.tau(Ddaten.OrigIndex),DRT_Summe(Ddaten.OrigIndex),'x','color',RWTHRot,'LineWidth',2);
            end
        end
        %h1 = legend('EI-DRT','E-DRT','Location','NorthWest');
        %set(h1,'Interpreter','none');
        
    case 'All_Fit'
        
        hold off
        plot(plotDaten.Zreal,plotDaten.Zimg,'o','DisplayName','Measurement','color',RWTHBlau,'LineWidth',1,'MarkerSize',7);hold on;
        plot(Fdaten.Zreal+plotDaten.Zreal-Ddaten.Zreal,Fdaten.Zimg+plotDaten.Zimg-Ddaten.Zimg,'x','DisplayName','Fit','color',RWTHRot,'LineWidth',1,'MarkerSize',7);
        grid on; axis square; axis equal; set(gca,'ydir', 'reverse');
        xlabel('$\Re\{\underline Z\}$ in $\Omega$','Interpreter','latex');ylabel('$\Im\{\underline Z\}$ in $\Omega$','Interpreter','latex'); %title(Plot_Title,'Interpreter','none');
        h1 = legend(app.axes1,'Measurement','Fit','Location','NorthWest');
        set(h1,'Interpreter','none');
        axis equal

    otherwise
        return
end
end