function fcn_PeakfinderEdit(app,event)
global DRT_Config
try
    Wert = str2num(get(app.PeakfinderEdit,'Value'));
catch error_msg
    set(app.PeakfinderEdit,'Value',num2str(DRT_Config.PeakSensitivitaet))
    errordlg(error_msg)
end
if isempty(Wert)|| Wert<app.PeakfinderSlider.Limits(1) || Wert>app.PeakfinderSlider.Limits(2)
    set(app.PeakfinderEdit,'Value',num2str(DRT_Config.PeakSensitivitaet))
    errordlg('Ungültiger Wert')
else
    DRT_Config.PeakSensitivitaet = Wert;
    set(app.PeakfinderSlider    ,'Value',DRT_Config.PeakSensitivitaet)
    fcn_DRTButton_Callback(app, event)
end
end