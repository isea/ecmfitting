function fcn_configFittingScript(app)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
myHFRoutine = {'HF elements except Phi','Fit','~or("Rser",and("ind",~"Phi"))','1:2 end-30:end',0;
               'HF elements including Phi','Fit','~or("Rser","ind")','1:2 end-30:end',0};
app.HFRoutineTable.Data = myHFRoutine;

myInitialRoutine = {'electrode reactions except Phi','Fit','~or("ct",or("dl","RMP"))','1:2 end-1:end',0;
               'MF elements except Phi','Fit','~or("ct",or("dl",or("RMP","DB")))','1:2 end-1:end',0;
               'MF and HF elements except Phi','Fit','and(or("slow","DP"),"~Phi")','1:2 end-1:end',0;
               'only LF elements','Fit','~or("slow","DP")','1:2 end-1:end',0;
               'only Phi fixed','Fit','"Phi"','1:2 end-30:end',0};
app.InitialRoutineTable.Data = myInitialRoutine;

myMFRoutine = {'electrode reactions except Phi','DRT','~or("ct",or("dl","RMP"))','1:10 end-7:end',0;
               'MF elements except Phi','Fit','~or("ct",or("dl",or("RMP","DB")))','1:2 end-1:end',0;
               'MF and HF elements except Phi','Fit','and(or("slow","DP"),"~Phi")','1:2 end-1:end',0};
app.MFRoutineTable.Data = myMFRoutine;
end
