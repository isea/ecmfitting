function fcn_Relax_FFT_Button(app, event)
global DRT_GUI;
[DRT_GUI.Messdaten.relax.zeit,unique_td_data_index]= unique(DRT_GUI.Messdaten.relax.zeit);
DRT_GUI.Messdaten.relax.strom = DRT_GUI.Messdaten.relax.strom(unique_td_data_index);
DRT_GUI.Messdaten.relax.spannung = DRT_GUI.Messdaten.relax.spannung(unique_td_data_index);

extrapolationfactor = 1;
sampleTime = 10;
strom = squeeze([DRT_GUI.Messdaten.relax.strom, 0]);  %selbst + n�chste 2
span  = squeeze([DRT_GUI.Messdaten.relax.spannung, DRT_GUI.Messdaten.relax.spannung(end) ]);
zeit  = squeeze([DRT_GUI.Messdaten.relax.zeit, DRT_GUI.Messdaten.relax.zeit(end)*extrapolationfactor + 0.001]) ;
AhStep  = squeeze(trapz(zeit,strom));
xDaten = zeit;
zeit = zeit(1):0.01:zeit(end); % x-Interpolationswerte
strom = interp1(xDaten,strom,zeit,'linear');
span= interp1(xDaten,span,zeit,'linear');
%figure; plot(FittingGUI.Messdaten.relax.zeit,FittingGUI.Messdaten.relax.strom);grid on
[f, fft_imp, Cap, CompensatedVoltage, U_ges] = CreateTDM(zeit(1:end), strom(1:end), span(1:end), 1,1);
Pulsdauer = DRT_GUI.Messdaten.relax.zeit(find(abs(DRT_GUI.Messdaten.relax.strom)>1e-3,1,'last'))-DRT_GUI.Messdaten.relax.zeit(find(abs(DRT_GUI.Messdaten.relax.strom)>1e-3,1,'first'));
strom_fft = fft( strom );
strom_fft = strom_fft(1:numel(f));
[peakheight , peakindex ] = findpeaks(mag2db(abs(strom_fft)));
peaks = zeros(size(f));
peaks(peakindex)=1;
index = find(f ~= 0 & ( mag2db(abs(strom_fft))>0.98*max( mag2db(abs(strom_fft))) | peaks) & f < 1./sampleTime);
DRT_GUI.Messdaten.relax_fft.frequenz = flipdim(f(index),2)';
DRT_GUI.Messdaten.relax_fft.omega = flipdim(2*pi*f(index),2)';
DRT_GUI.Messdaten.relax_fft.tau = flipdim(1./(2*pi*f(index)),2)';
DRT_GUI.Messdaten.relax_fft.Z = transpose(flipdim(real( fft_imp(index) )+ 1i* (imag( fft_imp(index))- 1 ./ (2.*pi.*f(index) * Cap)),2));
DRT_GUI.Messdaten.relax_fft.Zreal = flipdim(real( fft_imp(index) ),2)';
DRT_GUI.Messdaten.relax_fft.Zimg = flipdim(imag( fft_imp(index))- 1 ./ (2.*pi.*f(index) * Cap),2)';
DRT_GUI.Messdaten.relax_fft.fitted_Cap = Cap;
DRT_GUI.Messdaten.relax_fft.aktiv = ones(size(DRT_GUI.Messdaten.relax_fft.frequenz));
DRT_GUI.Messdaten.relax_fft.Zreal_korrektur = 0;
if isempty(strfind(DRT_GUI.Testparameter.Zustand,'_RelaxFFT'))
    DRT_GUI.Testparameter.Zustand = [DRT_GUI.Testparameter.Zustand '_RelaxFFT'];
end
fcn_Alles_Laden(app,event)


%         figure;semilogx(f,mag2db(abs(strom_fft)));grid on
%         hold on
%         semilogx(f(index),mag2db(abs(strom_fft(index))),'r')
%         semilogx(f(peakindex),mag2db(abs(strom_fft(peakindex))),'or')
%
%
%         figure;plot(zeit,strom)


%index = find(f < 1./sampleTime, 1, 'last');
% figure;
% semilogx(f(index), imag( fft_imp(index))- 1 ./ (2.*pi.*f(index) * Cap), 'bo') % - 1 ./ (2.*pi.*f(index) * Cap), 'ro')
% hold on;
% grid on;
% semilogx(FittingGUI.Messdaten.frequenz,FittingGUI.Messdaten.Zimg,'rx')


% semilogx(f(index), real( fft_imp(index) )+FittingGUI.Messdaten.relax_fft.Zreal_korrektur, 'bo')
% semilogx(FittingGUI.Messdaten.frequenz,FittingGUI.Messdaten.Zreal,'rx')

%
%         figure;
%         plot(real( fft_imp(index)),imag( fft_imp(index)) - 1 ./ (2.*pi.*f(index) * Cap),'o-b');
%         grid on;hold on; axis square; axis equal; set(gca,'ydir', 'reverse');
%         plot(FittingGUI.Messdaten.Zreal,FittingGUI.Messdaten.Zimg,'xr')
%         xlabel('$\Re\{\underline Z\}$ in $\Omega$','Interpreter','latex');ylabel('$\Im\{\underline Z\}$ in $\Omega$','Interpreter','latex'); %title(Plot_Title,'Interpreter','none');
% %         h1 = legend(Plot_Title,'Location','NorthWest');
% %         set(h1,'Interpreter','none');
%         axis equal

fcn_SpeichernButton_Callback(app,event)
end