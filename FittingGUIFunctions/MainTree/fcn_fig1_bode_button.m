function fcn_fig1_bode_button(app, event)
% hObject    handle to fig1_bode_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global DRT_GUI
if isempty(DRT_GUI) || isempty(cell2mat(strfind(fieldnames(DRT_GUI),'Messdaten')))
    return
end
figure
subplot(2,1,1)
semilogx(DRT_GUI.Messdaten.frequenz,abs(DRT_GUI.Messdaten.Z),'o')
grid on, hold on
if ~isempty(cell2mat(strfind(fieldnames(DRT_GUI.Messdaten),'relax_fft'))) && ~isempty(DRT_GUI.Messdaten.relax_fft)
    semilogx(DRT_GUI.Messdaten.relax_fft.frequenz,abs(DRT_GUI.Messdaten.relax_fft.Z+DRT_GUI.Messdaten.relax_fft.Zreal_korrektur),'ko')
end
ylabel('|Z| [\Omega]')
xlabel('f [Hz]')
subplot(2,1,2)
semilogx(DRT_GUI.Messdaten.frequenz,angle(DRT_GUI.Messdaten.Z),'o')

grid on, hold on
if ~isempty(cell2mat(strfind(fieldnames(DRT_GUI.Messdaten),'relax_fft'))) && ~isempty(DRT_GUI.Messdaten.relax_fft)
    semilogx(DRT_GUI.Messdaten.relax_fft.frequenz,angle(DRT_GUI.Messdaten.relax_fft.Z+DRT_GUI.Messdaten.relax_fft.Zreal_korrektur),'ko')
end
ylabel('angle(Z) [�]')
xlabel('f [Hz]')

if isempty(DRT_GUI) || isempty(cell2mat(strfind(fieldnames(DRT_GUI),'Fit'))) ||  ...
        isempty(DRT_GUI.Fit) || isempty(cell2mat(strfind(fieldnames(DRT_GUI.Fit),'Zreal'))) || isempty(DRT_GUI.Fit.Zreal)
    return
end

subplot(2,1,1)
semilogx(DRT_GUI.Fit.frequenz,abs(DRT_GUI.Fit.Z),'rx')
grid on, hold on


subplot(2,1,2)
semilogx(DRT_GUI.Fit.frequenz,angle(DRT_GUI.Fit.Z),'rx')


subplot(2,1,1)

if ~isempty(cell2mat(strfind(fieldnames(DRT_GUI.Messdaten),'relax_fft'))) && ~isempty(DRT_GUI.Messdaten.relax_fft)
    legend('EIS','Relax\_FFT','Fitpunkte')
else
    legend('Messung','Fit')
end

subplot(2,1,2)
if ~isempty(cell2mat(strfind(fieldnames(DRT_GUI.Messdaten),'relax_fft'))) && ~isempty(DRT_GUI.Messdaten.relax_fft)
    legend('EIS','Relax\_FFT','Fitpunkte','Location','SouthEast')
else
    legend('Messung','Fit','Location','SouthEast')
end
end