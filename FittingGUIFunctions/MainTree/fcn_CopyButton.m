function fcn_CopyButton(app, event)
global CopyFit
global DRT_GUI
if isempty(DRT_GUI) || isempty(cell2mat(strfind(fieldnames(DRT_GUI),'Fit')))
    return
end
CopyFit.Fit = DRT_GUI.Fit;
CopyFit.PunkteWegnehmenTextBox = get(app.PunkteWegnehmenTextBox,'Value');
CopyFit.Korrigiert_Punkte_Weg_TextBox = get(app.Korrigiert_Punkte_Weg_TextBox,'Value');
CopyFit.RelaxFFT_PunkteWegnehmenTextBox = get(app.RelaxFFT_PunkteWegnehmenTextBox,'Value');
if isfield(DRT_GUI,'DRT')
    CopyFit.DRT = DRT_GUI.DRT;
end
end