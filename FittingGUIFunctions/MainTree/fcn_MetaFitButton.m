function fcn_MetaFitButton(app, event)
global DRT_GUI;

if strcmp(get(app.MetaFitButton,'Text'),'Stop')
    set(app.MetaFitButton,'Text','Meta-Fit')
    return
else
    set(app.MetaFitButton,'Text','Stop')
end

if isempty(DRT_GUI) || isempty(cell2mat(strfind(fieldnames(DRT_GUI),'Messdaten')))
    return
end


if isempty(cell2mat(strfind(fieldnames(DRT_GUI.Fit),'Guessed_std')))
    fcn_GuessButton_Callback(app, event)
end

TableCell = get(app.ParamTable,'Data');
TableCell = cell2struct(TableCell',{'Name','Fix','Value','Min','Max','Lim'});

akt_P = cell2mat({TableCell.Value});
std_p = DRT_GUI.Fit.Guessed_std;
while strcmp(get(app.MetaFitButton,'Text'),'Stop')
    TableCell = get(app.ParamTable,'Data');
    TableCell = cell2struct(TableCell',{'Name','Fix','Value','Min','Max','Lim'});
    for i_p = 1:numel(akt_P)
        if ~TableCell(i_p).Fix
            %TableCell(i_p).Value = akt_P(i_p) ;
            if strfind(TableCell(i_p).Name,'Tau')
                TableCell(i_p).Min = akt_P(i_p)*(10^(-DRT_GUI.Fit.GrenzenBandbreite(i_p,1)/20));
                TableCell(i_p).Max = akt_P(i_p)*(10^(DRT_GUI.Fit.GrenzenBandbreite(i_p,2)/20));
            elseif strfind(TableCell(i_p).Name,'Phi')
                TableCell(i_p).Min = akt_P(i_p)-DRT_GUI.Fit.GrenzenBandbreite(i_p,1)*0.1;
                TableCell(i_p).Max = akt_P(i_p)+DRT_GUI.Fit.GrenzenBandbreite(i_p,2)*0.1;
            elseif ~strcmp(TableCell(i_p).Name,'Rser') && ~strcmp(TableCell(i_p).Name,'C') && ~strcmp(TableCell(i_p).Name,'Cser')&& ~strcmp(TableCell(i_p).Name,'Kskin')&& ~strcmp(TableCell(i_p).Name,'Lser')
                TableCell(i_p).Min = akt_P(i_p)-DRT_GUI.Fit.GrenzenBandbreite(i_p,1)*std_p(i_p);
                TableCell(i_p).Max = akt_P(i_p)+DRT_GUI.Fit.GrenzenBandbreite(i_p,2)*std_p(i_p);
            end
            
            if TableCell(i_p).Min < DRT_GUI.Fit.aktuell_Modell.ModellCell{5}{i_p}
                TableCell(i_p).Min = DRT_GUI.Fit.aktuell_Modell.ModellCell{5}{i_p};
            end
            if TableCell(i_p).Max > DRT_GUI.Fit.aktuell_Modell.ModellCell{6}{i_p}
                TableCell(i_p).Max = DRT_GUI.Fit.aktuell_Modell.ModellCell{6}{i_p};
            end
            set(app.GrenzenBandbreiteText,'Text',[num2str(max(DRT_GUI.Fit.GrenzenBandbreite(:))*100) '%'])
        end
    end
    set(app.ParamTable,'Data',struct2cell(TableCell)')
    
    for Fittingdurchgaenge = 1:3
        
        
        fcn_FitButton_Callback(app, event)
        
        
        index_min = DRT_GUI.Fit.Limit_Reached.index_min;
        index_max = DRT_GUI.Fit.Limit_Reached.index_max;
        if  ~(numel(index_min) == 0  && numel(index_max) == 0)
            DRT_GUI.Fit.GrenzenBandbreite(index_min,1) = DRT_GUI.Fit.GrenzenBandbreite(index_min,1)+0.1;
            DRT_GUI.Fit.GrenzenBandbreite(index_max,2) = DRT_GUI.Fit.GrenzenBandbreite(index_max,2)+0.1;
            break;
        end
    end
    if  numel(index_min) == 0  && numel(index_max) == 0
        set(app.MetaFitButton,'Text','Meta-Fit')
    end
end
end