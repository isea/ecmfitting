function fcn_ParamTable_CellEdit(app, event)
global DRT_GUI
ColNames = get(app.ParamTable,'ColumnName');
if strcmp(ColNames(event.Indices(2)),'Min')
    DRT_GUI.Fit.aktuell_Modell.ModellCell{1,5}{1,event.Indices(1)} = event.NewData;
elseif strcmp(ColNames(event.Indices(2)),'Max')
    DRT_GUI.Fit.aktuell_Modell.ModellCell{1,6}{1,event.Indices(1)} = event.NewData;
elseif strcmp(ColNames(event.Indices(2)),'Fitted Value')
    DRT_GUI.Fit.Parameter(event.Indices(1)) = event.NewData;
end
end