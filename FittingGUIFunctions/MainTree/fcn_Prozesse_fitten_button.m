function app = fcn_Prozesse_fitten_button(app, event)
global DRT_GUI;
global DRT_Config;
global Cont_Process_Fit_Counter;
global Cont_Process_Fit;
DRT_GUI.DRT.Config = DRT_Config;
if isempty(DRT_GUI) || isempty(cell2mat(strfind(fieldnames(DRT_GUI),'DRT'))) || isempty(DRT_GUI.DRT) || isempty(cell2mat(strfind(fieldnames(DRT_GUI.DRT),'EI_DRT'))) || isempty(DRT_GUI.DRT.EI_DRT)
    return
end
if DRT_Config.ZarcHN==5
    DRT_GUI.DRT.Config.Prozesse=1;
    if app.GUI
        set(app.ProzesseEdit,'Value',num2str(1))
    else
        app.ProzesseEdit.Value = num2str(1);
    end
    app = fcn_ProzesseEdit(app, event);
end
if ~(DRT_Config.ZarcHN==5) && (isempty(cell2mat(strfind(fieldnames(DRT_GUI.DRT),'UserTau'))) || numel(DRT_GUI.DRT.UserTau)<DRT_GUI.DRT.Config.Prozesse)
%    if app.GUI
%        set(app.UserTauText,'Visible','On')
%    else
%       app.UserTauText.Visible= 'On';
%    end
    if isempty(cell2mat(strfind(fieldnames(DRT_GUI.DRT),'UserTau')))
        DRT_GUI.DRT.UserTau=[];
    end
    return
end
% if ~get(app.ProzessFittingChooseInitialValuesCheckbox,'Value')
    if ~isempty(cell2mat(strfind(fieldnames(DRT_GUI.DRT),'UserTau')))
        DRT_GUI.DRT=rmfield(DRT_GUI.DRT,'UserTau');
    end
    %     set(app.UserTauText,'Visible','Off')
    %     set(app.UserTauList,'Visible','Off')
% end
g_DRT = DRT_GUI.DRT.EI_DRT.g(DRT_GUI.DRT.EI_DRT.aktiv)';
x_DRT = DRT_GUI.DRT.EI_DRT.x(DRT_GUI.DRT.EI_DRT.aktiv)';
Rpol = trapz(x_DRT,g_DRT);
Tau_DRT = DRT_GUI.DRT.EI_DRT.tau(DRT_GUI.DRT.EI_DRT.aktiv);
OrigIndex = DRT_GUI.DRT.EI_DRT.OrigIndex;
g_orig = DRT_GUI.DRT.EI_DRT.g(OrigIndex)';
x_orig = DRT_GUI.DRT.EI_DRT.x(OrigIndex)';
Tau_orig = DRT_GUI.DRT.EI_DRT.tau(OrigIndex);
indices = find(g_orig>0);
g_orig = g_orig(indices);
x_orig = x_orig(indices);
Tau_orig = Tau_orig(indices);
p_init = ones(1,3*DRT_GUI.DRT.Config.Prozesse);
p_min =  zeros(1,3*DRT_GUI.DRT.Config.Prozesse);
p_max = ones(1,3*DRT_GUI.DRT.Config.Prozesse);
modelformel = '';
for i = 1:DRT_GUI.DRT.Config.Prozesse
    if DRT_Config.ZarcHN==1 %% Zarc-Elemente
        modelformel = [modelformel 'fcn_Calc_Zarc_DRT(p(' num2str((i-1)*3+1) ...
            '),p(' num2str((i-1)*3+2) ...
            '),p(' num2str((i-1)*3+3) ...
            '),tau)'];
        p_max((i-1)*3+2)=max(Tau_DRT);
        p_min((i-1)*3+2)=min(Tau_DRT);
        p_min((i-1)*3+3)=0.7;
        p_init((i-1)*3+3)=0.8;
    elseif DRT_Config.ZarcHN==2 %% HN-Elemente
        modelformel = [modelformel 'fcn_Calc_HN_DRT(p(' num2str((i-1)*3+1) ...
            '),p(' num2str((i-1)*3+2) ...
            '),p(' num2str((i-1)*3+3) ...
            '),tau)'];
        p_max((i-1)*3+2)=max(Tau_DRT);
        p_min((i-1)*3+2)=min(Tau_DRT);
        p_min((i-1)*3+3)=0.5;
        p_init((i-1)*3+3)=0.1;
    elseif DRT_Config.ZarcHN==3 %% RC-Glieder
        modelformel = [modelformel 'fcn_Calc_HN_DRT(p(' num2str((i-1)*3+1) ...
            '),p(' num2str((i-1)*3+2) ...
            '),p(' num2str((i-1)*3+3) ...
            '),tau)'];
        p_max((i-1)*3+2)=max(Tau_DRT);
        p_min((i-1)*3+2)=min(Tau_DRT);
        p_min((i-1)*3+3)=1;
        p_max((i-1)*3+3)=1;
        p_init((i-1)*3+3)=0.1;
    elseif DRT_Config.ZarcHN==4 %% Por�se Elektroden
        R_ct = ['p(' num2str((i-1)*3+1) ')'];
        Tau_dl = ['p(' num2str((i-1)*3+2) ')'];
        R_MP = ['p(' num2str((i-1)*3+3) ')'];
        modelformel = [modelformel 'fcn_Calc_PorEl_DRT(p(' num2str((i-1)*3+1) ...
            '),p(' num2str((i-1)*3+2) ...
            '),p(' num2str((i-1)*3+3) ...
            '),tau)'];
        p_max((i-1)*3+2)=max(Tau_DRT);
        p_min((i-1)*3+2)=min(Tau_DRT);
        p_min((i-1)*3+3)=0;
        p_max((i-1)*3+3)=1000;
        p_init((i-1)*3+3)=0.1;
    elseif DRT_Config.ZarcHN==5 %% MF-Model
        ModellElemente=DRT_GUI.Fit.aktuell_Modell.ModellCell{8};
        ParIndices = [];
        p_init=[];
        p_min=[];
        p_max=[];
        modelformel = '';
        EinzelFormeln={};
        for m_i = 1:numel(ModellElemente)
            if ~strcmp(ModellElemente{m_i}.Zfun_MF,'0')
                p_vorher = numel(ParIndices);
                Newindices = ModellElemente{m_i}.ParameterIndexes(~ismember(ModellElemente{m_i}.ParameterIndexes,ParIndices));
                delete_list =[];
                for s_i = 1:numel(Newindices)
                    if isempty(strfind(ModellElemente{m_i}.Zfun_MF,DRT_GUI.Fit.aktuell_Modell.P_Name{1,Newindices(s_i)}))
                        delete_list = [delete_list s_i] ;
                    end
                end
                Newindices(delete_list)=[];
                ParIndices = [ParIndices Newindices];
                p_init = [p_init DRT_GUI.Fit.Parameter(Newindices)];
                p_min = [p_min DRT_GUI.Fit.Parameter_min(Newindices)];
                p_max = [p_max DRT_GUI.Fit.Parameter_max(Newindices)];
                for m_k = 1:numel(Newindices)
                    if DRT_GUI.Fit.ParFix(Newindices(m_k)) && isempty(strfind(lower(DRT_GUI.Fit.aktuell_Modell.P_Name{1,Newindices(m_k)}),'tau'))
                        p_min(p_vorher+m_k) =  p_init(p_vorher+m_k);
                        p_max(p_vorher+m_k) =  p_init(p_vorher+m_k);
                    end
                end
                ElementFormel = ModellElemente{m_i}.Zfun_MF;
                for m_k = 1:numel(ModellElemente{m_i}.ParameterIndexes)
                    ElementFormel=strrep(ElementFormel,...
                        DRT_GUI.Fit.aktuell_Modell.P_Name{1,ModellElemente{m_i}.ParameterIndexes(m_k)},...
                        ['p(' num2str(find(strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,ParIndices),DRT_GUI.Fit.aktuell_Modell.P_Name{1,ModellElemente{m_i}.ParameterIndexes(m_k)}))) ')']);
                end
                EinzelFormeln=[EinzelFormeln ; ElementFormel];
                modelformel = [modelformel '+fcn_Calc_Model_DRT(''' ElementFormel ''',p,tau)' ];
            end
        end
        if modelformel(1)=='+',modelformel=modelformel(2:end);end
    end
    if app.UseDRTCheckBox.Value
        app.DRT_indices = ParIndices;
    end
    if i < DRT_GUI.DRT.Config.Prozesse
        modelformel = [modelformel '+'];
    end
    %Widerst�nde sollen anfangs gleichverteilt sein
    %     if numel(FittingGUI.DRT.EI_DRT.ProzessFit.r)==FittingGUI.DRT.Config.Prozesse
    %         p_init((i-1)*3+1)= FittingGUI.DRT.EI_DRT.ProzessFit.r(i);
    %     else
    if ~DRT_Config.ZarcHN==5
        p_init((i-1)*3+1)=Rpol./DRT_GUI.DRT.Config.Prozesse;
        %     end
        p_max((i-1)*3+1)=sum(g_DRT);
        %Taus auch
        if sum(strcmp(fieldnames(DRT_GUI.DRT.EI_DRT),'ProzessFit')) && numel(DRT_GUI.DRT.EI_DRT.ProzessFit.tau)==DRT_GUI.DRT.Config.Prozesse
            p_init((i-1)*3+2)= DRT_GUI.DRT.EI_DRT.ProzessFit.tau(i);
        else
            p_init((i-1)*3+2)=10.^((log10(max(Tau_DRT))-log10(min(Tau_DRT)))/(DRT_GUI.DRT.Config.Prozesse+1)*i+log10(min(Tau_DRT)));
        end
    end
end
%nur weil function_model_all_types2 ein 'w' erwartet
modelformel = strrep(modelformel,'tau','w');
options = optimset('MaxIter',2000000,'MaxFunEvals',2000000,'TolX',1e-15,'TolFun',1e-15);
[p,fval,exitflag,output]=function_fit_easyfit2(app,Tau_orig,[g_orig, zeros(size(g_orig))],p_init,@function_model_all_types2,p_min, p_max ,options, modelformel);
if sum(ismember(fieldnames(DRT_GUI),'DRT')) && ~isempty(DRT_GUI.DRT) && ...
        sum(ismember(fieldnames(DRT_GUI.DRT),'EI_DRT')) && ...
        ~isempty(DRT_GUI.DRT.EI_DRT) && ...
        sum(ismember(fieldnames(DRT_GUI.DRT.EI_DRT),'ProzessFit')) ...
        && ~isempty(DRT_GUI.DRT.EI_DRT.ProzessFit)
    old_peaks = DRT_GUI.DRT.EI_DRT.ProzessFit;
else
    old_peaks.used = 0;
end
if DRT_Config.ZarcHN==5 % MF-Model
    DRT_GUI.DRT.EI_DRT.ProzessFit.val = p';
    DRT_GUI.DRT.EI_DRT.ProzessFit.ParIndices = ParIndices';
    DRT_GUI.DRT.EI_DRT.ProzessFit.r =  [];
    DRT_GUI.DRT.EI_DRT.ProzessFit.tau =  [];
    DRT_GUI.DRT.EI_DRT.ProzessFit.phi= [];
    DRT_GUI.DRT.EI_DRT.ProzessFit.used = ones(size(DRT_GUI.DRT.EI_DRT.ProzessFit.val));
    DRT_GUI.DRT.EI_DRT.ProzessFit.used_parname =DRT_GUI.Fit.aktuell_Modell.P_Name(1,ParIndices)' ;
    DRT_GUI.DRT.EI_DRT.ProzessFit.ElementFormeln = EinzelFormeln;
else
    DRT_GUI.DRT.EI_DRT.ProzessFit.val = [];
    DRT_GUI.DRT.EI_DRT.ProzessFit.ParIndices = [];
    DRT_GUI.DRT.EI_DRT.ProzessFit.ElementFormeln = [];
    DRT_GUI.DRT.EI_DRT.ProzessFit.r =  p(((1:DRT_GUI.DRT.Config.Prozesse)-1)*3+1)';
    DRT_GUI.DRT.EI_DRT.ProzessFit.tau = p(((1:DRT_GUI.DRT.Config.Prozesse)-1)*3+2)';
    DRT_GUI.DRT.EI_DRT.ProzessFit.phi= p(((1:DRT_GUI.DRT.Config.Prozesse)-1)*3+3)';
    [DRT_GUI.DRT.EI_DRT.ProzessFit.tau , IX] = sort(DRT_GUI.DRT.EI_DRT.ProzessFit.tau);
    DRT_GUI.DRT.EI_DRT.ProzessFit.phi = DRT_GUI.DRT.EI_DRT.ProzessFit.phi(IX);
    DRT_GUI.DRT.EI_DRT.ProzessFit.r = DRT_GUI.DRT.EI_DRT.ProzessFit.r(IX);
    DRT_GUI.DRT.EI_DRT.ProzessFit.used = zeros(size(DRT_GUI.DRT.EI_DRT.ProzessFit.tau));
    DRT_GUI.DRT.EI_DRT.ProzessFit.used_parname = repmat({''},size(DRT_GUI.DRT.EI_DRT.ProzessFit.tau));
    if numel(old_peaks.used) == numel(old_peaks.tau)
        for i=find(old_peaks.used'==1)
            newpeak=find(abs(DRT_GUI.DRT.EI_DRT.ProzessFit.tau-old_peaks.tau(i)) == min(abs(DRT_GUI.DRT.EI_DRT.ProzessFit.tau-old_peaks.tau(i))),1,'first');
            if isempty(newpeak),continue,end
            oldfound = cell2mat(strfind(old_peaks.used_parname(1:i),DRT_GUI.DRT.EI_DRT.ProzessFit.used_parname{newpeak}));
            if isempty(oldfound)
                DRT_GUI.DRT.EI_DRT.ProzessFit.used(newpeak) = 1;
                DRT_GUI.DRT.EI_DRT.ProzessFit.used_parname(newpeak) = old_peaks.used_parname(i);
            else
                if abs(old_peaks.tau(oldfound)-DRT_GUI.DRT.EI_DRT.ProzessFit.tau(newpeak))>abs(old_peaks.tau(i)-DRT_GUI.DRT.EI_DRT.ProzessFit.tau(newpeak))
                    DRT_GUI.DRT.EI_DRT.ProzessFit.used(newpeak) = 1;
                    DRT_GUI.DRT.EI_DRT.ProzessFit.used_parname(newpeak) = old_peaks.used_parname(i);
                end
            end
        end
    end
end
% if get(app.ProzessFittingChooseInitialValuesCheckbox,'Value')
%     DRT_GUI.DRT=rmfield(DRT_GUI.DRT,'UserTau');
    %     set(app.UserTauText,'Visible','Off')
    %     set(app.UserTauList,'Visible','Off')
    
% end
app= fcn_Prozess_Taus_in_Tabelle(app);
if app.GUI
    if ~app.GlobalFitInProgress
        Typ = 'DRT';
        % if ~strcmp(eventdata,'kein_plot')
        %     axes(app.axes3);
        fcn_plot_Auswaehl(app,DRT_GUI.Korrigiert,DRT_GUI.DRT.E_DRT,DRT_GUI.DRT.EI_DRT,[],Typ)
    end
end
% end
% if DRT_Config.ZarcHN==5 && get(app.cont_process_checkbox,'Value')
%     disp('F�r MF-Model ist cont. process fitting nicht implementiert')
% else
%     if get(app.cont_process_checkbox,'Value')
%         if Cont_Process_Fit_Counter < 15
%             if Cont_Process_Fit_Counter == 0, Cont_Process_Fit = [];end
%             Cont_Process_Fit_Counter = Cont_Process_Fit_Counter + 1;
%             display(sprintf('Durchgang %d, rel. Standardabweichung %2.4e',Cont_Process_Fit_Counter,mean(std(Cont_Process_Fit,0,2)./mean(Cont_Process_Fit,2))));
%             try
%                 Cont_Process_Fit(:,Cont_Process_Fit_Counter) = [DRT_GUI.DRT.EI_DRT.ProzessFit.r; DRT_GUI.DRT.EI_DRT.ProzessFit.tau; DRT_GUI.DRT.EI_DRT.ProzessFit.phi];
%             catch
%             end
%             if numel(find(old_peaks.used'==1)) > numel(find(DRT_GUI.DRT.EI_DRT.ProzessFit.used'==1))
%                 set(app.cont_process_checkbox,'Value',0)
%                 set(app.GueltigeMessungCheck,'Value',0)
%                 fcn_GueltigeMessungCheck_Callback(app,event)
%             elseif Cont_Process_Fit_Counter > 2 && mean(std(Cont_Process_Fit(:,end-2:end),0,2)./mean(Cont_Process_Fit(:,end-2:end),2)) < 0.02
%                 set(app.cont_process_checkbox,'Value',0)
%                 set(app.GueltigeMessungCheck,'Value',1)
%                 fcn_GueltigeMessungCheck_Callback(app,event)
%             else
%                 fcn_DRT_Prozesse_use_button_Callback(app, event)
%             end
%         else
%             set(app.cont_process_checkbox,'Value',0)
%             set(app.GueltigeMessungCheck,'Value',0)
%             fcn_GueltigeMessungCheck_Callback(app,event)
%         end
%     end
% end
end