function fcn_fit_all(app,event)
timeVal=tic;
app.DoTheMagicShitButton.Text='Drink Some Coffee';
Modell=app.ModellAuswahlPopup.Value;
Temps = get(app.TemperaturPopup ,'Items');
for i_Temp = 2:numel(Temps)
    SOCs = get(app.SOCPopup ,'Items');
    set(app.SOCPopup,'Value',SOCs{2})
    fcn_SOCPopup(app,event)
    app.ModellAuswahlPopup.Value=Modell;
    fcn_ModellAuswahlPopup(app)
    fcn_CopyButton(app,event)
    fcn_ZeitbereichsFittingButton(app,event)
    fcn_TDF_CopyButton(app,event)
    fcn_TDF_SpeichernButton(app,event)
    set(app.TemperaturPopup,'Value',Temps(i_Temp));
    fcn_TemperaturPopup(app,event)
    SOCs = get(app.SOCPopup ,'Items');
    
    for i_SOC= 2:numel(SOCs)
        set(app.SOCPopup,'Value',SOCs{i_SOC})
        fcn_SOCPopup(app,event)
        fcn_PasteButton(app,event)
        for i=1:5
            fcn_FitButton_Callback(app, event)
        end
        
        set(app.SOCPopup,'Value',SOCs{i_SOC})
        fcn_SOCPopup(app,event)
        fcn_ZeitbereichsFittingButton(app,event)
        fcn_TDF_PasteButton(app,event)
        
        TempTable=app.TDF_RelaxTable.Data;
        TempTable(5:end,4)=cellfun(@(x) {x*0.1}, TempTable(5:end,2));
        TempTable(5:end,5)=cellfun(@(x) {x*10}, TempTable(5:end,2));
        app.TDF_RelaxTable.Data=TempTable;
        fcn_TDF_RelaxTable_CellEdit(app,event);
        
        for i=1:6
            fcn_TDF_OCVFitButton(app,event)
            pause(0.001)
        end
        fcn_TDF_CopyButton(app,event)
        fcn_TDF_SpeichernButton(app,event)
    end
    
end
app.DoTheMagicShitButton.Text='Do The Magic Shit';
toc(timeVal)
end