function Implementierung_Neu_Laden()
global DRT_GUI
if numel(DRT_GUI.Fit.aktuell_Modell.ModellCell)<8 || isempty(DRT_GUI.Fit.aktuell_Modell.ModellCell{8})
    DRT_GUI.Fit.Implementierung=[];
    return
end
Modell=DRT_GUI.Fit.aktuell_Modell.ModellCell{8};
DRT_GUI.Fit.Implementierung.Table=cell(numel(Modell)+1,5);
DRT_GUI.Fit.Implementierung.Info=cell(numel(Modell)+1,1);
for i = 1:numel(Modell)
    TableZeile=[];
    if ismember('f',fieldnames(Modell{i})),
        Funktionsname = func2str(Modell{i}.f)    ;
        index = strfind(Funktionsname,'/');
        if ~isempty(index),Funktionsname = Funktionsname(index(end)+1:end); end
    else
        Funktionsname = Modell{i}.Funktionsname;
    end
    TableZeile{1}= Funktionsname;
    TableZeile{2}='';
    TableZeile{3}=0;
    TableZeile{4}=0;
    for j=1:numel(Modell{i}.inputs)
        TableZeile{4+j}=Modell{i}.inputs{j};
    end
    DRT_GUI.Fit.Implementierung.Table(i,1:numel(TableZeile))=TableZeile;
    DRT_GUI.Fit.Implementierung.Info{i,1}=Modell{i};
end
DRT_GUI.Fit.Implementierung.Table(size(DRT_GUI.Fit.Implementierung.Table,1),1:size(DRT_GUI.Fit.Implementierung.Table,2))={''};
global Modellliste
end