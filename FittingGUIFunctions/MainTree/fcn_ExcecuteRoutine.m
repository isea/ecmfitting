function [] = fcn_ExcecuteRoutine(app,event,routine)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
TableCell=get(app.ParamTable,'Data');
weg_string = get(app.PunkteWegnehmenTextBox,'Value');
DRT_weg_string = get(app.Korrigiert_Punkte_Weg_TextBox,'Value');
for i_routine = 1:size(routine,1)
    outstring = ['Excecuting ',num2str(routine.Number), 'x ',routine.Name, ' routine.'];
    disp(outstring)
    for i_runroutine=1:routine(i_routine).Number
        for i_step=1:size(routine(i_routine).Configuration,1)
             if routine(i_routine).Configuration(i_step).Type =='Fit'
                    set(app.PunkteWegnehmenTextBox,'Value',routine(i_routine).Configuration(i_step).weg_string);
                    fcn_PunkteWegnehmenButton_Callback(app, event);
                    TableCell_temp=get(app.ParamTable,'Data');
                    TableCell_temp = fcn_Setfix(TableCell,routine(i_routine).Configuration(i_step).Configuration);
                    set(app.ParamTable,'Data',TableCell_temp);
                elseif routine(i_routine).Configuration(i_step).Type =='DRT'
                    set(app.Korrigiert_Punkte_Weg_TextBox,'Value',routine(i_routine).Configuration(i_step).weg_string);
                    fcn_Korrigiert_Punkte_Weg_Button(app, event)
                    TableCell_temp=get(app.ParamTable,'Data');
                    TableCell_temp = fcn_Setfix(TableCell,routine(i_routine).Configuration(i_step).Configuration);
                    set(app.ParamTable,'Data',TableCell_temp);                    
                elseif routine(i_routine).Configuration(i_step).Type =='Initialize'
             end
            for i_runstep =1:routine(i_routine).Configuration(i_step).Number
                if routine(i_routine).Configuration(i_step).Type =='Fit'   
                    fcn_FitButton_Callback(app, event)
                    elseif routine(i_routine).Configuration(i_step).Type =='DRT'
                        fcn_DRTButton_Callback(app, event)
                        fcn_Prozesse_fitten_button_Callback(app, event)
                        fcn_DRT_Prozesse_use_button_Callback(app, event)
                    elseif routine(i_routine).Configuration(i_step).Type =='Initialize'
                end
                
            end
            if app.KeinPlotCheckBox.Value~=1
                    pause(0.01)
            end
        end
    end
end
% Reset weg_strings and fixing
TableCell_temp=get(app.ParamTable,'Data');
fcn_Setfix(TableCell,TableCell_temp(:,2));
set(app.ParamTable,'Data',TableCell);
set(app.PunkteWegnehmenTextBox,'Value',weg_string);
fcn_PunkteWegnehmenButton_Callback(app, event);
set(app.Korrigiert_Punkte_Weg_TextBox,'Value',DRT_weg_string);
fcn_Korrigiert_Punkte_Weg_Button(app, event)

end

