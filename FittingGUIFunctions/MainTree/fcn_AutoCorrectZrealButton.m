function fcn_AutoCorrectZrealButton(app, event)
global DRT_GUI
try isempty(DRT_GUI.Messdaten.relax_fft)
    [f_sorted ,order ] = sort(DRT_GUI.Messdaten.frequenz);
    Zreal_sorted = DRT_GUI.Messdaten.Zreal(order);
    indices = 1:ceil(numel(DRT_GUI.Messdaten.relax_fft.frequenz)*0.2);
    DRT_GUI.Messdaten.relax_fft.Zreal_korrektur = mean(spline(log(f_sorted),Zreal_sorted,log(DRT_GUI.Messdaten.relax_fft.frequenz(indices))) - DRT_GUI.Messdaten.relax_fft.Zreal(indices));
    fcn_Alles_Laden(app,event);
catch
   disp('Zun�chst muss die Relax-FFT erstellt werden.')
end
end