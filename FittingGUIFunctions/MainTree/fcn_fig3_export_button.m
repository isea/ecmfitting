function fcn_fig3_export_button(app, event)
global DRT_GUI
warning('off','MATLAB:handle_graphics:Canvas:RenderingException')
yyaxis(app.axes3,'left');
AltePosition = get(app.axes3,'Position');
AltePosition(1:2)=5;
AltePosition2=[AltePosition(1:2),AltePosition(3)+10,AltePosition(4)+10];
newfig=figure('Units','pixels','Position',AltePosition2); %'Position',AltePosition+10,
plot(1,1);
neueAchse = gca;
cNames=fieldnames(app.axes3);
cValues=get(app.axes3,cNames);
% neueAchse=axes('Parent',newfig);
% yyaxis(neueAchse,'left');
% for ix1=1:length(cNames)
%     try 
%         set(neueAchse,cNames{ix1},cValues{ix1})
%     catch
%     end
% end
plot(-rand(length(app.axes3.Children))*1e-3,'Parent',neueAchse);
for ix2=1:length(app.axes3.Children)
    cNames2=fieldnames(app.axes3.Children(ix2));
    cValues2=get(app.axes3.Children(ix2),cNames2);
    for ix3=1:length(cNames2)
        if strcmp(cNames2{ix3},'Parent'), continue; end
        try
            set(neueAchse.Children(ix2),cNames2{ix3},cValues2{ix3})
        catch
        end
    end
end
h1=legend();
cNames3=fieldnames(app.axes3.Legend);
cValues3=get(app.axes3.Legend,cNames3);
for ix4=1:length(cNames3)
    try 
        set(h1,cNames3{ix4},cValues3{ix4})
    catch
    end
end
app.axes3.XLabel.Interpreter='latex'; app.axes3.YLabel.Interpreter='latex'; app.axes3.XLabel.String='$\Re\{\underline Z\}$ in $\Omega$'; app.axes3.YLabel.String='$\Im\{\underline Z\}$ in $\Omega$'; %title(Plot_Title,'Interpreter','none');
yyaxis(app.axes3,'right');
yyaxis(neueAchse,'right');
cNames=fieldnames(app.axes3);
cValues=get(app.axes3,cNames);
for ix1=1:length(cNames)
    if strcmp(cNames{ix1},'Parent'), continue; end
    try 
            set(neueAchse,cNames{ix1},cValues{ix1})
    catch
    end
end
plot(-rand(length(app.axes3.Children))*1e-3,'Parent',neueAchse);
for ix2=1:length(app.axes3.Children)
    cNames2=fieldnames(app.axes3.Children(ix2));
    cValues2=get(app.axes3.Children(ix2),cNames2);
    for ix3=1:length(cNames2)
        if strcmp(cNames2{ix3},'Parent'), continue; end
        try
            set(neueAchse.Children(ix2),cNames2{ix3},cValues2{ix3})
        catch
        end
    end
end
h1=legend();
cNames3=fieldnames(app.axes3.Legend);
cValues3=get(app.axes3.Legend,cNames3);
for ix4=1:length(cNames3)
    try 
        set(h1,cNames3{ix4},cValues3{ix4})
    catch
    end
end
app.axes3.XLabel.Interpreter='latex'; app.axes3.YLabel.Interpreter='latex'; app.axes3.XLabel.String='$\Re\{\underline Z\}$ in $\Omega$'; app.axes3.YLabel.String='$\Im\{\underline Z\}$ in $\Omega$'; %title(Plot_Title,'Interpreter','none');
if isempty(dir('export'))
    mkdir('export')
end
if isempty( dir(['export' '/' DRT_GUI.Testparameter.Batterie]))
    mkdir(['export' '/' DRT_GUI.Testparameter.Batterie])
end
savefig(['export/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie '_DRT_' ...
    DRT_GUI.Testparameter.Zustand '_' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad_'...
    strrep(num2str(DRT_GUI.Testparameter.SOC),'-','m') 'SOC'...
    '.fig'])
warning('on','MATLAB:handle_graphics:Canvas:RenderingException')
end