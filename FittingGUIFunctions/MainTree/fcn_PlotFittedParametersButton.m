function fcn_PlotFittedParametersButton(app, event)
global DRT_GUI;
if isempty(DRT_GUI) || isempty(cell2mat(strfind(fieldnames(DRT_GUI),'Messdaten')))
    return
end
app.GlobalFitInProgress = 0;
if app.GUI
    hold(app.axes1,'off');
end
Typ = 'Fit';
formula = DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell;
formula_komp = DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_komp;
if  isempty(cell2mat(strfind(fieldnames(DRT_GUI.Messdaten),'relax_fft'))) || isempty(DRT_GUI.Messdaten.relax_fft)
    m_w = DRT_GUI.Messdaten.omega(DRT_GUI.Messdaten.aktiv==1) ;
    m_real = DRT_GUI.Messdaten.Zreal(DRT_GUI.Messdaten.aktiv==1);
    m_imag = DRT_GUI.Messdaten.Zimg(DRT_GUI.Messdaten.aktiv==1);
else
    m_w = [DRT_GUI.Messdaten.omega(DRT_GUI.Messdaten.aktiv==1); DRT_GUI.Messdaten.relax_fft.omega(DRT_GUI.Messdaten.relax_fft.aktiv==1)] ; %selbst
    m_real = [DRT_GUI.Messdaten.Zreal(DRT_GUI.Messdaten.aktiv==1); DRT_GUI.Messdaten.relax_fft.Zreal(DRT_GUI.Messdaten.relax_fft.aktiv==1)+DRT_GUI.Messdaten.relax_fft.Zreal_korrektur];%selbst
    m_imag = [DRT_GUI.Messdaten.Zimg(DRT_GUI.Messdaten.aktiv==1); DRT_GUI.Messdaten.relax_fft.Zimg(DRT_GUI.Messdaten.relax_fft.aktiv==1)];%selbst
end
if app.GUI
    TableCell = get(app.ParamTable,'Data');
else
    TableCell = app.ParamTable.Data;
end
TableCell = cell2struct(TableCell',{'Name','Fix','Value','Min','Max','Lim'});
p_best = cell2mat({TableCell.Value});
% Residuum
distances=zeros(1,length(m_real));
p = p_best; w = m_w;
f_real = real(eval(formula));
f_imag = imag(eval(formula));
for m=1:length(m_real)
    distances(m)=sqrt((m_real(m)-f_real(m))^2+(m_imag(m)-f_imag(m))^2);
end
resid=sum(distances)/length(distances);
DRT_GUI.Fit.residuum = resid;
set(app.Residuum,'Text',num2str(resid));
p = p_best;
w = m_w;
f_real = real(eval(formula));
f_imag = imag(eval(formula));
DRT_GUI.Fit.Zreal = f_real;
DRT_GUI.Fit.Zimg = f_imag;
DRT_GUI.Fit.frequenz = w/2/pi;
DRT_GUI.Fit.omega = w;
DRT_GUI.Fit.tau = 1./w;
DRT_GUI.Fit.Z = f_real + 1i * f_imag;
hold(app.axes1,'off');
fcn_plot_Auswaehl(app,DRT_GUI.Messdaten,DRT_GUI.Fit,[],DRT_GUI.Testparameter.Batterie,Typ)
% Modellelemente plotten, falls der ESB-Generator genutzt wurde

Z_LF = eval(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_LF);
Z_HF = eval(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_HF);
Z_MF = eval(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_MF);
FarbenLaden
%hold(app.axes6,'off');
%%app.axes6.YLimMode = 'auto';
%app.axes6.YLim=[floor(min((m_imag-imag(Z_HF+Z_MF))*100000))/100000, ceil(max((m_imag-imag(Z_HF+Z_MF))*100000))/100000];
%app.axes6.XLimMode='auto';
%plot(app.axes6,m_real-real(Z_HF+Z_MF),m_imag-imag(Z_HF+Z_MF),'o','color',RWTHBlau,'displayname','LF-Messung','LineWidth',1,'MarkerSize',7)
%grid(app.axes6,'on');  set(app.axes6,'ydir', 'reverse'); hold(app.axes6,'on'); %axis(app.axes6,'square'); axis(app.axes6,'equal');
%app.axes6.XLabel.Interpreter='latex'; app.axes6.YLabel.Interpreter='latex'; app.axes6.XLabel.String='$\Re\{\underline Z\}$ in $\Omega$'; app.axes6.YLabel.String='$\Im\{\underline Z\}$ in $\Omega$'; %title(Plot_Title,'Interpreter','none');
%plot(app.axes6,real(Z_LF),imag(Z_LF),'x','color',RWTHRot,'displayname','LF-Fit','LineWidth',1,'MarkerSize',7)
%h1 = legend(app.axes6,'LF-Messung','LF-Fit');
%set(h1,'Interpreter','none','Location','NorthWest');
%hold(app.axes5,'off');
%%app.axes5.YLimMode = 'auto';
%app.axes5.YLim=[floor(min((m_imag-imag(Z_LF+Z_MF))*100000))/100000, ceil(max((m_imag-imag(Z_LF+Z_MF))*100000))/100000];
%pp.axes5.XLimMode='auto';
%plot(app.axes5,m_real-real(Z_LF+Z_MF),m_imag-imag(Z_LF+Z_MF),'o','color',RWTHBlau,'displayname','HF-Messung','LineWidth',1,'MarkerSize',7)
%grid(app.axes5,'on');  set(app.axes5,'ydir', 'reverse');hold(app.axes5,'on');% axis(app.axes5,'square'); axis(app.axes5,'equal');
%app.axes5.XLabel.Interpreter='latex'; app.axes5.YLabel.Interpreter='latex'; app.axes5.XLabel.String='$\Re\{\underline Z\}$ in $\Omega$'; app.axes5.YLabel.String='$\Im\{\underline Z\}$ in $\Omega$'; %title(Plot_Title,'Interpreter','none');
%plot(app.axes5,real(Z_HF),imag(Z_HF),'x','color',RWTHRot,'displayname','HF-Fit','LineWidth',1,'MarkerSize',7)
%h1 = legend(app.axes5,'HF-Messung','HF-Fit');
%set(h1,'Interpreter','none','Location','NorthEast');
%Zreal_vorher=0;Zreal_vorher_LF=0;Zreal_vorher_HF=0;

%plot impact of single ECEs if selected
if get(app.PlotElementsCheckbox,'Value') && ~isempty(DRT_GUI.Fit.aktuell_Modell.ModellCell{8})
    if numel(DRT_GUI.Fit.aktuell_Modell.ModellCell)>=8
        for i = 1:numel(DRT_GUI.Fit.aktuell_Modell.ModellCell{8})
            Modell = DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{i};
            Zfun = Modell.Zfun;
            for j = 1:numel(Modell.ParameterIndexes)
                Zfun = strrep(Zfun,Modell.inputs{j},['p(' num2str(Modell.ParameterIndexes(j)) ')']);
            end
            Z = eval(Zfun);
            index = find(abs(imag(Z))>(mean(abs(imag(Z)))*3),1,'first');
            if isempty(index), index = 1; end
            if index>numel(Zreal_vorher)
                index2=numel(Zreal_vorher);
            else
                index2 = index + find(Zreal_vorher(index:end)<Zreal_vorher(index),1,'last')-1;
            end
            if isempty(index2)
                realverschiebung = Zreal_vorher(index);
            else
                realverschiebung =  Zreal_vorher(index2);
                Zreal_vorher(1:index2) = Zreal_vorher(index2);
            end
            if Modell.Plot
                plot(app.axes1,real(Z)+realverschiebung,imag(Z),'k','display',Modell.Name)
            end
            Zreal_vorher = Zreal_vorher(end)+real(Z);
            Zfun_LF = Modell.Zfun_LF;
            for j = 1:numel(Modell.ParameterIndexes)
                Zfun_LF = strrep(Zfun_LF,Modell.inputs{j},['p(' num2str(Modell.ParameterIndexes(j)) ')']);
            end
            if ~strcmp(Zfun_LF,'0')
                Z = eval(Zfun_LF);
                index = find(abs(imag(Z))>(mean(abs(imag(Z)))*3),1,'first');
                if isempty(index), index = 1; end
                if index>numel(Zreal_vorher_LF),
                    index2=numel(Zreal_vorher_LF);
                else
                    index2 = index + find(Zreal_vorher_LF(index:end)<Zreal_vorher_LF(index),1,'last')-1;
                end
                if isempty(index2), index2 = index; end
                realverschiebung =  Zreal_vorher_LF(index2);
%                if Modell.Plot
%                    plot(app.axes6,real(Z)+realverschiebung,imag(Z),'k','display',Modell.Name)
%                end
                Zreal_vorher_LF = Zreal_vorher_LF+real(Z);
            end
            Zfun_HF = Modell.Zfun_HF;
            for j = 1:numel(Modell.ParameterIndexes)
                Zfun_HF = strrep(Zfun_HF,Modell.inputs{j},['p(' num2str(Modell.ParameterIndexes(j)) ')']);
            end
            if ~strcmp(Zfun_HF,'0')
                Z = eval(Zfun_HF);
                index = find(abs(imag(Z))>(mean(abs(imag(Z)))*3),1,'first');
                if isempty(index), index = 1; end
                if index>numel(Zreal_vorher_HF),
                    index2=numel(Zreal_vorher_HF);
                else
                    index2 = index + find(Zreal_vorher_HF(index:end)<Zreal_vorher_HF(index),1,'last')-1;
                end
                if isempty(index2), index2 = index; end
                realverschiebung =  Zreal_vorher_HF(index2);
%                if Modell.Plot
%                    plot(app.axes5,real(Z)+realverschiebung,imag(Z),'k','display',Modell.Name)
%                end
                Zreal_vorher_HF = Zreal_vorher_HF+real(Z);
            end
        end
    end
end

%curious data error sets p to array of 1s and 0s
%this destroys the evaluation of the formula in line 154
if isequal(p,[0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0])
    p = [0.000378977594825381,0.000171057438082008,4.90151760382791e-09,1.02406022983695,2.64811058013034e-06,0.000552446921206148,0.994723721320579,1.11680424149202e-07,5.19408034395803e-05,0.00695406783634618,0.550164473310711,0.000766313852957703,13.5743351448210,0.000250941730981743,0.0159762592878554,2.01010742830415e-07,2525.37748424868];
end
f_real = real(eval(formula_komp));
f_imag = imag(eval(formula_komp));
DRT_GUI.Fit.korrigiert.Z = f_real + 1i *f_imag;
DRT_GUI.Fit.korrigiert.Zreal = f_real ;
DRT_GUI.Fit.korrigiert.Zimg =  f_imag;
Hz50 = find(w==(2*pi*50));
DRT_GUI.Korrigiert = DRT_GUI.Messdaten;
if  ~isempty(cell2mat(strfind(fieldnames(DRT_GUI.Messdaten),'relax_fft'))) && ~isempty(DRT_GUI.Messdaten.relax_fft)
    DRT_GUI.Korrigiert.omega = [DRT_GUI.Messdaten.omega(DRT_GUI.Messdaten.aktiv==1) ; DRT_GUI.Messdaten.relax_fft.omega(DRT_GUI.Messdaten.relax_fft.aktiv==1)] ; %selbst
    DRT_GUI.Korrigiert.Zreal = [DRT_GUI.Messdaten.Zreal(DRT_GUI.Messdaten.aktiv==1); DRT_GUI.Messdaten.relax_fft.Zreal(DRT_GUI.Messdaten.relax_fft.aktiv==1)+DRT_GUI.Messdaten.relax_fft.Zreal_korrektur];%selbst
    DRT_GUI.Korrigiert.Zimg = [DRT_GUI.Messdaten.Zimg(DRT_GUI.Messdaten.aktiv==1); DRT_GUI.Messdaten.relax_fft.Zimg(DRT_GUI.Messdaten.relax_fft.aktiv==1)];%selbst
    DRT_GUI.Korrigiert.Z = DRT_GUI.Korrigiert.Zreal + 1i * DRT_GUI.Korrigiert.Zimg;
    DRT_GUI.Korrigiert.frequenz = DRT_GUI.Korrigiert.omega /2/pi;
    DRT_GUI.Korrigiert.tau = 1./ DRT_GUI.Korrigiert.omega ;
else
    DRT_GUI.Korrigiert.omega = [DRT_GUI.Messdaten.omega(DRT_GUI.Messdaten.aktiv==1) ] ;
    DRT_GUI.Korrigiert.Zreal = [DRT_GUI.Messdaten.Zreal(DRT_GUI.Messdaten.aktiv==1)];
    DRT_GUI.Korrigiert.Zimg = [DRT_GUI.Messdaten.Zimg(DRT_GUI.Messdaten.aktiv==1)];
    DRT_GUI.Korrigiert.Z = DRT_GUI.Korrigiert.Zreal + 1i * DRT_GUI.Korrigiert.Zimg;
    DRT_GUI.Korrigiert.frequenz = DRT_GUI.Korrigiert.omega /2/pi;
    DRT_GUI.Korrigiert.tau = 1./ DRT_GUI.Korrigiert.omega ;
end
if numel(Hz50 == 1)
    DRT_GUI.Fit.korrigiert.Zreal(Hz50) = spline(log(w([1:(Hz50-1) (Hz50+1):end])),f_real([1:(Hz50-1) (Hz50+1):end]),log(2*pi*50));
    DRT_GUI.Fit.korrigiert.Zimg(Hz50) = spline(log(w([1:(Hz50-1) (Hz50+1):end])),f_imag([1:(Hz50-1) (Hz50+1):end]),log(2*pi*50));
    DRT_GUI.Fit.korrigiert.Z(Hz50) = DRT_GUI.Fit.korrigiert.Zreal(Hz50) + 1i * DRT_GUI.Fit.korrigiert.Zimg(Hz50);
    DRT_GUI.Korrigiert.Zreal(Hz50) = spline(log(w([1:(Hz50-1) (Hz50+1):end])),DRT_GUI.Korrigiert.Zreal([1:(Hz50-1) (Hz50+1):end]),log(2*pi*50));
    DRT_GUI.Korrigiert.Zimg(Hz50) =  spline(log(w([1:(Hz50-1) (Hz50+1):end])),DRT_GUI.Korrigiert.Zimg( [1:(Hz50-1) (Hz50+1):end]),log(2*pi*50));
    DRT_GUI.Korrigiert.Z(Hz50) = DRT_GUI.Korrigiert.Zreal(Hz50) + 1i * DRT_GUI.Korrigiert.Zimg(Hz50);
end
for n=1:length(DRT_GUI.Korrigiert.Z)
    if isnan(DRT_GUI.Korrigiert.Z(n))
        DRT_GUI.Korrigiert.Z(n) = 0;
    end
    if isnan(DRT_GUI.Fit.Z(n))
        DRT_GUI.Fit.Z(n) = 0;
    end
    if isnan(DRT_GUI.Fit.korrigiert.Z(n))
        DRT_GUI.Fit.korrigiert.Z(n) = 0;
    end
end
DRT_GUI.Korrigiert.Z = DRT_GUI.Korrigiert.Z - DRT_GUI.Fit.Z + DRT_GUI.Fit.korrigiert.Z  ;
DRT_GUI.Korrigiert.Zreal = DRT_GUI.Korrigiert.Zreal - DRT_GUI.Fit.Zreal  + DRT_GUI.Fit.korrigiert.Zreal ;
DRT_GUI.Korrigiert.Zimg = DRT_GUI.Korrigiert.Zimg - DRT_GUI.Fit.Zimg + DRT_GUI.Fit.korrigiert.Zimg ;
if isempty(get( app.Korrigiert_Punkte_Weg_TextBox,'Value'))
    Erster_Wert = find(DRT_GUI.Korrigiert.Zimg<0,1,'first');
    if isempty(Erster_Wert)
        String1 = '';
    elseif Erster_Wert == 1
        String1 = ['1:' num2str(Erster_Wert) , ', '];
    else
        String1 = ['1:' num2str(Erster_Wert-1) , ', '];
    end
    Letzter_Wert = Erster_Wert + find(DRT_GUI.Korrigiert.Zimg(Erster_Wert+1:end)>0,1,'first')-1;
    if  ~sum(DRT_GUI.Messdaten.aktiv)
        String2 = [num2str(numel(DRT_GUI.Korrigiert.Z)) ':' num2str(numel(DRT_GUI.Korrigiert.Z)) ];
    elseif Letzter_Wert<0.5*numel(DRT_GUI.Korrigiert.Z)
        String2 = [num2str(find(DRT_GUI.Messdaten.aktiv,1,'first')) ':' num2str(numel(DRT_GUI.Korrigiert.Z)) ];
    elseif isempty(Letzter_Wert)
        String2 = [num2str(find(DRT_GUI.Messdaten.aktiv,1,'last')) ':' num2str(numel(DRT_GUI.Korrigiert.Z)) ];
    else
        String2 = [ num2str(Letzter_Wert+1) ':' num2str(numel(DRT_GUI.Korrigiert.Z))];
    end
    set(app.Korrigiert_Punkte_Weg_TextBox,'Value',[String1 String2])
end
fcn_Korrigiert_Punkte_Weg_Button(app, event);
end