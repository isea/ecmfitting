function app = fcn_Alles_Laden(app, event)
global DRT_GUI
% if strcmp(eventdata,'kein_plot')
%     return
% end
%         Folders = {''};
%         f = dir('output/*');
%         for i = 1:numel(f)
%             if ~strcmp(f(i).name,'.') && ~strcmp(f(i).name,'..') && f(i).isdir
%                 Folders = [Folders;f(i).name];
%             end
%         end
%         app.BatterieNamePopup.Items=Folders;
event = [];
app = fcn_BatterieNamePopup(app);

if isempty(DRT_GUI) || ~sum(ismember(fieldnames(DRT_GUI),'Testparameter')) || ~sum(ismember(fieldnames(DRT_GUI),'Messdaten'))
    return
end
if sum(ismember(fieldnames(DRT_GUI),'Testparameter'))
    if ~isempty(DRT_GUI.Testparameter) && sum(ismember(fieldnames(DRT_GUI.Testparameter),'Messdatum'))
        app.DatumTextBox.Value=DRT_GUI.Testparameter.Messdatum;
    else
        app.DatumTextBox.Value='';
    end
    if ~isempty(DRT_GUI.Testparameter) && sum(ismember(fieldnames(DRT_GUI.Testparameter),'Batterie'))
        app.BatterieTextBox.Value=DRT_GUI.Testparameter.Batterie;
    else
        app.BatterieTextBox.Value='';
    end
    if ~isempty(DRT_GUI.Testparameter) && sum(ismember(fieldnames(DRT_GUI.Testparameter),'Zustand'))
        app.ZustandTextBox.Value=DRT_GUI.Testparameter.Zustand;
    else
        app.ZustandTextBox.Value='';
    end
    if ~isempty(DRT_GUI.Testparameter) && sum(ismember(fieldnames(DRT_GUI.Testparameter),'Cap'))
        app.KapTextBox.Value=num2str(DRT_GUI.Testparameter.Cap);
    else
        app.KapTextBox.Value='';
    end
    if ~isempty(DRT_GUI.Testparameter) && sum(ismember(fieldnames(DRT_GUI.Testparameter),'Temperatur'))
        app.TemperaturTextBox.Value=num2str(DRT_GUI.Testparameter.Temperatur);
    else
        app.TemperaturTextBox.Value='';
    end
    if ~isempty(DRT_GUI.Testparameter) && sum(ismember(fieldnames(DRT_GUI.Testparameter),'SOC'))
        app.SOCTextBox.Value=[strrep(num2str(DRT_GUI.Testparameter.SOC),'m','-')];
    else
        app.SOCTextBox.Value='';
    end
    %GUI sometimes does not apply changes without the pause
    pause(0.1)
end
if  isempty(cell2mat(strfind(fieldnames(DRT_GUI),'Messdaten'))) || isempty(DRT_GUI.Messdaten)|| isempty(cell2mat(strfind(fieldnames(DRT_GUI.Messdaten),'relax_fft'))) || isempty(DRT_GUI.Messdaten.relax_fft)
    %             fcn_Relax_Visible(app,'off')
else
    %fcn_Relax_Visible(app,'on')
    if isempty(cell2mat(strfind(fieldnames(DRT_GUI.Messdaten.relax_fft),'Zreal_korrektur'))) || isempty(DRT_GUI.Messdaten.relax_fft.Zreal_korrektur)
        DRT_GUI.Messdaten.relax_fft.Zreal_korrektur=0;
    end
    app.Zreal_korrektur_textbox.Value=num2str(DRT_GUI.Messdaten.relax_fft.Zreal_korrektur,'%8.4e');
end

if sum(ismember(fieldnames(DRT_GUI),'Messdaten'))
    if ~isempty(DRT_GUI.Messdaten) && sum(ismember(fieldnames(DRT_GUI.Messdaten),'frequenz'))
        app.fmaxTextBox.Value=num2str(max(DRT_GUI.Messdaten.frequenz));
        app.fminTextBox.Value=num2str(min(DRT_GUI.Messdaten.frequenz));
        app.MessPunkteTextBox.Value=num2str(numel(DRT_GUI.Messdaten.frequenz));
        if app.GUI
            if ~app.GlobalFitInProgress 
                Typ = 'Nyquist';
                hold(app.axes1,'off')
        %         app.axes1.NextPlot='add';
                if ~sum(ismember(fieldnames(DRT_GUI),'Fit'))
                    fcn_plot_Auswaehl(app,DRT_GUI.Messdaten,[],[],DRT_GUI.Testparameter.Batterie,Typ);
                end
            end
        end
        if sum(ismember(fieldnames(DRT_GUI.Messdaten),'low_Punkte_Weg')) && ~isempty(DRT_GUI.Messdaten.low_Punkte_Weg)
            app.PunkteWegnehmenTextBox.Value=DRT_GUI.Messdaten.low_Punkte_Weg;
            %Erg�nzung:
            app = fcn_PunkteWegnehmeButton(app, event);            
        end
    end
end
if sum(ismember(fieldnames(DRT_GUI),'Fit'))
    if ~isempty(DRT_GUI.Fit) && sum(ismember(fieldnames(DRT_GUI.Fit),'Parameter')) && ~isempty(DRT_GUI.Fit.Parameter)
        if sum(ismember(fieldnames(DRT_GUI),'Fit')) && ~isempty(DRT_GUI.Fit)
            ModellListe = app.ModellAuswahlPopup.Items;
            if isempty(find(strcmp(DRT_GUI.Fit.aktuell_Modell.Modellname,ModellListe)))
                app.ModellAuswahlPopup.Value=app.ModellAuswahlPopup.Items{1};
            else
                app.ModellAuswahlPopup.Value=app.ModellAuswahlPopup.Items{find(strcmp(DRT_GUI.Fit.aktuell_Modell.Modellname,ModellListe))};
            end
            app = fcn_ModellAuswahlPopup(app);
            if app.GUI
                if ~app.GlobalFitInProgress 
                    Typ = 'Fit';
                    hold(app.axes1,'off')
                    fcn_plot_Auswaehl(app,DRT_GUI.Messdaten,DRT_GUI.Fit,[],DRT_GUI.Testparameter.Batterie,Typ)
                end
            end
            if sum(ismember(fieldnames(DRT_GUI),'Korrigiert')) && ~isempty(DRT_GUI.Korrigiert)
                if app.GUI
                    set(app.Residuum,'Text',num2str(DRT_GUI.Fit.residuum));
                    set(app.Korrigiert_Punkte_Weg_TextBox,'Value',DRT_GUI.Korrigiert.Punkte_Weg)
                else
                    app.Residuum.Text = num2str(DRT_GUI.Fit.residuum);
                    app.Korrigiert_Punkte_Weg_TextBox.Value= DRT_GUI.Korrigiert.Punkte_Weg;
                end
                %Erg�nzung:
%                 app = fcn_Korrigiert_Punkte_Weg_Button(app, event);
                if sum(ismember(fieldnames(DRT_GUI),'DRT')) && ~isempty(DRT_GUI.DRT)
                    if sum(ismember(fieldnames(DRT_GUI.DRT),'Config')) && ~isempty(DRT_GUI.DRT.Config)
                        global DRT_Config
                        DRT_Config = DRT_GUI.DRT.Config;
                        if ~sum(ismember(fieldnames(DRT_Config),'PeakSensitivitaet')) || isempty(DRT_Config.PeakSensitivitaet)
                            DRT_Config.PeakSensitivitaet = 0.3 ;
                        end
                        if ~sum(ismember(fieldnames(DRT_Config),'Prozesse')) || isempty(DRT_Config.Prozesse)
                            DRT_Config.Prozesse = 3 ;
                        end
                        if DRT_Config.Schwingfaktor ~= 1
                            DRT_Config.FilterFaktor_ext = DRT_Config.FilterFaktor_ext * DRT_Config.Schwingfaktor;
                            DRT_Config.Schwingfaktor = 1;
                        end
                        if app.GUI
                            set(app.InterpolationSlider,'Value',log10(DRT_Config.InterpolationsFaktor))
                            set(app.InterpolationEdit,'Value',num2str(DRT_Config.InterpolationsFaktor))
                            set(app.FilterFaktorSlider,'Value',DRT_Config.FilterFaktor_ext)
                            set(app.FilterEdit,'Value',num2str(DRT_Config.FilterFaktor_ext))
                            set(app.ZeropaddingSlider,'Value',log10(DRT_Config.ZeroPadding))
                            set(app.ZeropaddingEdit,'Value',num2str(DRT_Config.ZeroPadding))
                            set(app.PeakfinderSlider,'Value',DRT_Config.PeakSensitivitaet)
                            set(app.PeakfinderEdit,'Value',num2str(DRT_Config.PeakSensitivitaet))
                            set(app.ProzesseSlider,'Value',DRT_Config.Prozesse)
                            set(app.ProzesseEdit,'Value',num2str(DRT_Config.Prozesse))
                        else
                            app.InterpolationSlider.Value=log10(DRT_Config.InterpolationsFaktor);
                            app.InterpolationEdit.Value=num2str(DRT_Config.InterpolationsFaktor);
                            app.FilterFaktorSlider.Value=DRT_Config.FilterFaktor_ext;
                            app.FilterEdit.Value=num2str(DRT_Config.FilterFaktor_ext);
                            app.ZeropaddingSlider.Value=log10(DRT_Config.ZeroPadding);
                            app.ZeropaddingEdit.Value=num2str(DRT_Config.ZeroPadding);
                            app.PeakfinderSlider.Value=DRT_Config.PeakSensitivitaet;
                            app.PeakfinderEdit.Value=num2str(DRT_Config.PeakSensitivitaet);
                            app.ProzesseSlider.Value=DRT_Config.Prozesse;
                            app.ProzesseEdit.Value=num2str(DRT_Config.Prozesse);
                        end
                            
                        if ~sum(ismember(fieldnames(DRT_Config),'ZarcHN'))
                            DRT_Config.ZarcHN=5;
                        end
                        if app.GUI
                            set(app.ZarcHNPopup,'Value',app.ZarcHNPopup.Items{DRT_Config.ZarcHN})
                        else
                            app.ZarcHNPopup.Value = app.ZarcHNPopup.Items{DRT_Config.ZarcHN};
                        end
                        app = fcn_ZarcHNPopup(app, event);
                        minfreq = DRT_GUI.DRT.Messgrenzen(1);
                        maxfreq = DRT_GUI.DRT.Messgrenzen(2);
                        freq_int = DRT_GUI.DRT.EI_DRT.frequenz;
                        DRT_int = DRT_GUI.DRT.EI_DRT.g;
                        freq_ext = DRT_GUI.DRT.E_DRT.frequenz;
                        DRT = DRT_GUI.DRT.E_DRT.g;
                        Typ = 'DRT';
                        if app.GUI
                            if ~app.GlobalFitInProgress 
                                fcn_plot_Auswaehl(app,DRT_GUI.Korrigiert,DRT_GUI.DRT.E_DRT,DRT_GUI.DRT.EI_DRT,[],Typ)
        %                         fcn_DRT_Taus_in_Tabelle(app)
        %                         fcn_Prozess_Taus_in_Tabelle(app)
                            end
                        end
                    end
                end
            end
            if app.GUI
                if ~app.GlobalFitInProgress 
                    fcn_PlotFittedParametersButton(app,event);
                end
            end
        end
    end
end
end