function fcn_fig1_Z_button(app, event)
global DRT_GUI
FarbenLaden
if isempty(DRT_GUI) || isempty(cell2mat(strfind(fieldnames(DRT_GUI),'Messdaten')))
    return
end
if isempty(dir('export'))
    mkdir('export')
end
if isempty( dir(['export' '/' DRT_GUI.Testparameter.Batterie]))
    mkdir(['export' '/' DRT_GUI.Testparameter.Batterie])
end

figure('Name','Vgl. EIS-Relax','NumberTitle','off','UnitS','centimeters','Position',[0, 0, 25, 10])
subplot(2,5,[1:2 6:7])
plot(DRT_GUI.Messdaten.Z,'o','color',RWTHBlau,'DisplayName','Messung','LineWidth',1,'MarkerSize',7);hold on;
if ~isempty(cell2mat(strfind(fieldnames(DRT_GUI.Messdaten),'relax_fft'))) && ~isempty(DRT_GUI.Messdaten.relax_fft)
    plot(DRT_GUI.Messdaten.relax_fft.Z+DRT_GUI.Messdaten.relax_fft.Zreal_korrektur...
        ,'o','color',RWTHSchwarz,'DisplayName','RelaxFFT','LineWidth',1,'MarkerSize',7)
end
grid on; axis square; axis equal; set(gca,'ydir', 'reverse');
xlabel('$\Re\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
ylabel('$\Im\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
h1 = legend('show','Location','NorthWest');
set(h1,'Interpreter','latex');
set(gca,'TickLabelInterpreter','latex')
axis equal
subplot(2,5,3:5)
semilogx(DRT_GUI.Messdaten.frequenz,real(DRT_GUI.Messdaten.Z),'o','color',RWTHBlau,'DisplayName','Messung','LineWidth',1,'MarkerSize',7);hold on;
if ~isempty(cell2mat(strfind(fieldnames(DRT_GUI.Messdaten),'relax_fft'))) && ~isempty(DRT_GUI.Messdaten.relax_fft)
    semilogx(DRT_GUI.Messdaten.relax_fft.frequenz,real(DRT_GUI.Messdaten.relax_fft.Z)+DRT_GUI.Messdaten.relax_fft.Zreal_korrektur...
        ,'o','color',RWTHSchwarz,'DisplayName','RelaxFFT','LineWidth',1,'MarkerSize',7)
end
set(gca,'xdir','reverse'),grid on, hold on
set(gca,'TickLabelInterpreter','latex')
h1 = legend('show','Location','NorthWest');
set(h1,'Interpreter','latex');
ylabel('$\Re\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
xlabel('$f$ in Hz','Interpreter','latex');
subplot(2,5,8:10)
semilogx(DRT_GUI.Messdaten.frequenz,imag(DRT_GUI.Messdaten.Z),'o','color',RWTHBlau,'DisplayName','Messung','LineWidth',1,'MarkerSize',7);hold on;
if ~isempty(cell2mat(strfind(fieldnames(DRT_GUI.Messdaten),'relax_fft'))) && ~isempty(DRT_GUI.Messdaten.relax_fft)
    semilogx(DRT_GUI.Messdaten.relax_fft.frequenz,imag(DRT_GUI.Messdaten.relax_fft.Z)...
        ,'o','color',RWTHSchwarz,'DisplayName','RelaxFFT','LineWidth',1,'MarkerSize',7)
end
set(gca,'xdir','reverse'),grid on, hold on
set(gca,'ydir','reverse')
set(gca,'TickLabelInterpreter','latex')
ylabel('$\Im\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
xlabel('$f$ in Hz','Interpreter','latex');
savefig(['export/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie '_Messung_' ...
    DRT_GUI.Testparameter.Zustand '_' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad_'...
    strrep(num2str(DRT_GUI.Testparameter.SOC),'-','m') 'SOC'...
    '.fig'])


if isempty(DRT_GUI) || isempty(cell2mat(strfind(fieldnames(DRT_GUI),'Fit'))) ||  ...
        isempty(DRT_GUI.Fit) || isempty(cell2mat(strfind(fieldnames(DRT_GUI.Fit),'Zreal'))) || isempty(DRT_GUI.Fit.Zreal)
    return
end

subplot(2,5,[1:2 6:7])
plot(DRT_GUI.Fit.Z,'x','color',RWTHRot,'DisplayName','Fit','LineWidth',1,'MarkerSize',7);
legend('off');
h1 = legend('show','Location','NorthWest');
set(h1,'Interpreter','latex');
subplot(2,5,3:5)
semilogx(DRT_GUI.Fit.frequenz,real(DRT_GUI.Fit.Z),'x','color',RWTHRot,'DisplayName','Fit','LineWidth',1,'MarkerSize',7);
legend('off')
h1 = legend('show','Location','NorthWest');
set(h1,'Interpreter','latex');
subplot(2,5,8:10)
semilogx(DRT_GUI.Fit.frequenz,imag(DRT_GUI.Fit.Z),'x','color',RWTHRot,'DisplayName','Fit','LineWidth',1,'MarkerSize',7);

savefig(['export/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie '_Fit_' ...
    DRT_GUI.Testparameter.Zustand '_' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad_'...
    strrep(num2str(DRT_GUI.Testparameter.SOC),'-','m') 'SOC'...
    '.fig'])
end