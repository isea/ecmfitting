function app = fcn_DRT_Button(app, event)
global DRT_GUI;
warning('off','MATLAB:handle_graphics:Canvas:RenderingException')
if isempty(DRT_GUI) || isempty(cell2mat(strfind(fieldnames(DRT_GUI),'Korrigiert')))
    return
end
global DRT_Config
Schwingfaktor = DRT_Config.Schwingfaktor;
%Signal wird mit InterpolationsFaktor-1 hochgetastet
InterpolationsFaktor = DRT_Config.InterpolationsFaktor;
% Auswirkung in Tiefpassfilter, je höher desto höher und schmaler sind die
% Maxima
FilterFaktor_ext = DRT_Config.FilterFaktor_ext;
%Werte an denr Rändern werden zu Nullangenommen(wie viele Werte werden
%extrapoliert
ZeroPadding = DRT_Config.ZeroPadding;
PeakSensitivitaet = DRT_Config.PeakSensitivitaet;
FilterFaktor_int = FilterFaktor_ext / InterpolationsFaktor * Schwingfaktor;
minfreq = min(DRT_GUI.Korrigiert.frequenz(DRT_GUI.Korrigiert.aktiv==true));
maxfreq = max(DRT_GUI.Korrigiert.frequenz(DRT_GUI.Korrigiert.aktiv==true));
% freq_ext = 10.^(...
%     log10(FittingGUI.Korrigiert.spaps.frequenz(1))-ZeroPadding*mean(diff(log10(FittingGUI.Korrigiert.spaps.frequenz))):...
%     mean(diff(log10(FittingGUI.Korrigiert.spaps.frequenz))):...
%     log10(FittingGUI.Korrigiert.spaps.frequenz(end))+ZeroPadding*mean(diff(log10(FittingGUI.Korrigiert.spaps.frequenz))));
%Z_ext = fnval(fnxtr(FittingGUI.Korrigiert.spaps.sp_real),log10(freq_ext))+1i .* fnval(fnxtr(FittingGUI.Korrigiert.spaps.sp_img),log10(freq_ext));
[freq_ext, Z_ext] = extrapolatesignal(DRT_GUI.Korrigiert.spaps.frequenz',(DRT_GUI.Korrigiert.spaps.Zreal + 1i *DRT_GUI.Korrigiert.spaps.Zimg).',[ZeroPadding ZeroPadding]);
[freq_int,Z_int_imag]=interpolate_signal(InterpolationsFaktor,imag(Z_ext),freq_ext);
[x_int,DRT_int]=makeDRT(Z_int_imag',freq_int',true,FilterFaktor_int);
DRT_int = DRT_int/Schwingfaktor;
[x,DRT]=makeDRT(imag(Z_ext),freq_ext,true,FilterFaktor_ext);
OrigFreqs = DRT_GUI.Korrigiert.frequenz(DRT_GUI.Korrigiert.aktiv==true);
OrigIndex = zeros(size(OrigFreqs));
for i = 1:numel(OrigIndex)
    OrigIndex(i) = find(freq_int>=OrigFreqs(i),1,'first');
end
DRT_GUI.DRT.Messgrenzen = [minfreq maxfreq];
DRT_GUI.DRT.E_DRT.frequenz = freq_ext;
DRT_GUI.DRT.E_DRT.omega= 2*pi*freq_ext;
DRT_GUI.DRT.E_DRT.tau= 1./(2*pi*freq_ext)';
DRT_GUI.DRT.E_DRT.g=real(DRT)';
DRT_GUI.DRT.E_DRT.x=x';
DRT_GUI.DRT.E_DRT.aktiv = freq_ext>=minfreq & freq_ext<=maxfreq;
DRT_GUI.DRT.E_DRT.Rpol = trapz(x(DRT_GUI.DRT.E_DRT.aktiv),abs(DRT(DRT_GUI.DRT.E_DRT.aktiv)));
DRT_GUI.DRT.E_DRT.Rser = min(real(Z_ext));
DRT_GUI.DRT.E_DRT.Rpol_EIS = max(real(Z_ext))-min(real(Z_ext));
DRT_GUI.DRT.EI_DRT.frequenz = freq_int;
DRT_GUI.DRT.EI_DRT.OrigIndex = OrigIndex;
DRT_GUI.DRT.EI_DRT.omega= 2*pi*freq_int;
DRT_GUI.DRT.EI_DRT.tau= 1./(2*pi*freq_int)';
DRT_GUI.DRT.EI_DRT.g=real(DRT_int)';
DRT_GUI.DRT.EI_DRT.x=x_int';
DRT_GUI.DRT.EI_DRT.aktiv = freq_int>=minfreq & freq_int<=maxfreq;
DRT_GUI.DRT.EI_DRT.Rpol = trapz(x_int(DRT_GUI.DRT.EI_DRT.aktiv),abs(DRT_int(DRT_GUI.DRT.EI_DRT.aktiv)));
DRT_GUI.DRT.EI_DRT.Rser = min(real(Z_ext));
DRT_GUI.DRT.EI_DRT.Rpol_EIS = max(real(Z_ext))-min(real(Z_ext));
DRT_GUI.DRT.Config = DRT_Config;
% if app.KeinPlotCheckBox.Value~=1
% display(sprintf('Rpol_E: %0.5e, Rpol_EI: %0.5e, Rpol: %0.5e',trapz(x(DRT_GUI.DRT.E_DRT.aktiv),abs(DRT(DRT_GUI.DRT.E_DRT.aktiv))),trapz(x_int(DRT_GUI.DRT.EI_DRT.aktiv),abs(DRT_int(DRT_GUI.DRT.EI_DRT.aktiv))),max(real(Z_ext))-min(real(Z_ext))));
% end
DRT_int_aktiv = DRT_GUI.DRT.EI_DRT.g(DRT_GUI.DRT.EI_DRT.aktiv);
Peaks = flipdim(peakfinder(DRT_int_aktiv,(max(real(DRT_int_aktiv))-min(DRT_int_aktiv))/(10000)),1);
g_diff1 = diff(DRT_int_aktiv);
index = find(abs([0 diff(g_diff1)])>(mean(diff(g_diff1))+5*std(diff(g_diff1)))) ;
g_diff1(index) = 0;
g_diff2 = diff(diff(DRT_int_aktiv));
index = find(abs([0 diff(g_diff2)])>(mean(diff(g_diff2))+5*std(diff(g_diff2)))) ;
g_diff2(index) = 0;
g_diff3 = diff(diff(diff(DRT_int_aktiv)));
index = find(abs([0 diff(g_diff3)])>(mean(diff(g_diff3))+5*std(diff(g_diff3)))) ;
g_diff3(index) =0;
Peaks2 = flipdim(peakfinder(abs(g_diff3)/max(abs(g_diff3)),1/(20)),1);
Peaks2 = Peaks2(find(abs(g_diff1(Peaks2))'<(mean(abs(g_diff1(Peaks2)))-2*(1-PeakSensitivitaet)*std(abs(g_diff1(Peaks2))))));
%figure; plot(real(DRT_int_aktiv)/max(real(DRT_int_aktiv))); hold on ; grid on;plot(abs(g_diff3)/max(abs(g_diff3)),'r');plot(abs(g_diff2)/max(abs(g_diff2)),'g');plot(abs(g_diff1)/max(abs(g_diff1)),'k') ;plot(Peaks,real(DRT_int_aktiv(Peaks))/max(real(DRT_int_aktiv)),'o');plot(Peaks2,abs(g_diff3(Peaks2))/max(abs(g_diff3)),'ro')
Peaks = sort([Peaks Peaks2],2,'descend');
Peaks(Peaks<DRT_GUI.DRT.Config.InterpolationsFaktor/2)=[];
Peaks(Peaks>(numel(DRT_int_aktiv)-DRT_GUI.DRT.Config.InterpolationsFaktor/2)) = [];
Peaks = Peaks + find(DRT_GUI.DRT.EI_DRT.aktiv,1,'first');
if ~isempty(cell2mat(strfind(fieldnames(DRT_GUI.DRT.EI_DRT),'peaks')))
    old_peaks = DRT_GUI.DRT.EI_DRT.peaks;
else
    old_peaks.used = 0;
end
DRT_GUI.DRT.EI_DRT.peaks.index = Peaks;
DRT_GUI.DRT.EI_DRT.peaks.tau = DRT_GUI.DRT.EI_DRT.tau(Peaks)';
DRT_GUI.DRT.EI_DRT.peaks.g = DRT_GUI.DRT.EI_DRT.g(Peaks)';
DRT_GUI.DRT.EI_DRT.peaks.used = zeros(size(DRT_GUI.DRT.EI_DRT.g(Peaks)'));
DRT_GUI.DRT.EI_DRT.peaks.used_parname = repmat({''},size(DRT_GUI.DRT.EI_DRT.peaks.used));
for i=find(old_peaks.used==1)'
    newpeak=find(abs(DRT_GUI.DRT.EI_DRT.peaks.tau-old_peaks.tau(i)) == min(abs(DRT_GUI.DRT.EI_DRT.peaks.tau-old_peaks.tau(i))),1,'first');
    if isempty(newpeak),continue,end
    oldfound = cell2mat(strfind(old_peaks.used_parname(1:i),DRT_GUI.DRT.EI_DRT.peaks.used_parname{newpeak}));
    if isempty(oldfound)
        DRT_GUI.DRT.EI_DRT.peaks.used(newpeak) = 1;
        DRT_GUI.DRT.EI_DRT.peaks.used_parname(newpeak) = old_peaks.used_parname(i);
    else
        if abs(old_peaks.tau(oldfound)-DRT_GUI.DRT.EI_DRT.peaks.tau(newpeak))>abs(old_peaks.tau(i)-DRT_GUI.DRT.EI_DRT.peaks.tau(newpeak))
            DRT_GUI.DRT.EI_DRT.peaks.used(newpeak) = 1;
            DRT_GUI.DRT.EI_DRT.peaks.used_parname(newpeak) = old_peaks.used_parname(i);
        end
    end
end
app = fcn_DRT_Taus_in_Tabelle(app);
app = fcn_Prozess_Taus_in_Tabelle(app);
if app.GUI
    if ~app.GlobalFitInProgress
        Typ = 'DRT';
        fcn_plot_Auswaehl(app,DRT_GUI.Korrigiert,DRT_GUI.DRT.E_DRT,DRT_GUI.DRT.EI_DRT,[],Typ)
    end
end

warning('on','MATLAB:handle_graphics:Canvas:RenderingException')
end