function fcn_SchwingungSlider(app,event)
global DRT_Config
DRT_Config.Schwingfaktor = get(app.SchwingungSlider,'Value');
set(app.SchwingungEdit,'Value',num2str(DRT_Config.Schwingfaktor))
fcn_DRTButton_Callback(app, event)
end