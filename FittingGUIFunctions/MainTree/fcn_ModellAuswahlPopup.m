function app = fcn_ModellAuswahlPopup(app,event)
%%% Modell ausw�hlen
global DRT_GUI;
global Modellliste;
if isempty(DRT_GUI) || ~sum(ismember(fieldnames(DRT_GUI),'Messdaten'))
    return
end
value=[];
for i=1:numel(app.ModellAuswahlPopup.Items)
    if strcmp(app.ModellAuswahlPopup.Value, app.ModellAuswahlPopup.Items{i})
        value=i;
        break;
    end
end
if isempty(value), value=1;end
app.ModellFormelTextBox.Value=Modellliste.Modell{value,2};
aktuell_Modell_P_str = textscan(strrep(Modellliste.Modell{value,3},' ',''),'%s','delimiter',',');
aktuell_Modell = Modellliste.Modell(value,:);
if ismember('Fit',fieldnames(DRT_GUI))
    oldFit = DRT_GUI.Fit;
else
    oldFit=[];
end
DRT_GUI.Fit.aktuell_Modell = [];
if isempty(DRT_GUI.Fit) || ~sum(ismember(fieldnames(DRT_GUI.Fit),'Parameter')) || isempty(DRT_GUI.Fit.Parameter) || numel(DRT_GUI.Fit.Parameter) ~= length(aktuell_Modell_P_str{1,1})
    DRT_GUI.Fit.Parameter = zeros(1,length(aktuell_Modell_P_str{1,1})); % f�r p_best
    DRT_GUI.Fit.Parameter_min = zeros(1,length(aktuell_Modell_P_str{1,1})); % f�r p_best
    DRT_GUI.Fit.Parameter_max = zeros(1,length(aktuell_Modell_P_str{1,1})); % f�r p_best
%     DRT_GUI.Fit.Parameter = cell2mat(aktuell_Modell{4}); % f�r p_best
%     DRT_GUI.Fit.Parameter_min = cell2mat(aktuell_Modell{5}); % f�r p_best
%     DRT_GUI.Fit.Parameter_max = cell2mat(aktuell_Modell{6}); % f�r p_best
%     DRT_GUI.Fit.gueltig = 0;
    DRT_GUI.Fit.Grenzenautomatik = 0;
    DRT_GUI.Fit.GrenzenBandbreite = zeros(numel(DRT_GUI.Fit.Parameter),2);
end
if ~sum(ismember(fieldnames(DRT_GUI.Fit),'gueltig')),DRT_GUI.Fit.gueltig = 0;end
if ~sum(ismember(fieldnames(DRT_GUI.Fit),'Grenzenautomatik')),DRT_GUI.Fit.Grenzenautomatik = 0;end
if ~sum(ismember(fieldnames(DRT_GUI.Fit),'GrenzenBandbreite')),DRT_GUI.Fit.GrenzenBandbreite = zeros(numel(DRT_GUI.Fit.Parameter),2);end
app.GueltigeMessungCheck.Value=DRT_GUI.Fit.gueltig;
% Die entsprechenden Parameter in Tabelle abgeben
DRT_GUI.Fit.aktuell_Modell.Modellname = Modellliste.Modell{value,1};  % Name des Modells
DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell = Modellliste.Modell{value,2};  % Formel, sp�ter mit p(0)..p(n) umschreiben
DRT_GUI.Fit.aktuell_Modell.ModellCell = Modellliste.Modell(value,:);
p_best = DRT_GUI.Fit.Parameter;
p_min = DRT_GUI.Fit.Parameter_min;
p_max = DRT_GUI.Fit.Parameter_max;
p_min_model = cell2mat(DRT_GUI.Fit.aktuell_Modell.ModellCell{5});
p_max_model = cell2mat(DRT_GUI.Fit.aktuell_Modell.ModellCell{6});
index = find((p_best<=p_min*1.001 & p_min~=p_max & p_min~=p_min_model) | (p_best>=p_max*0.999 & p_min~=p_max & p_max~=p_max_model ));
% die Parameter in Feld gezeigt und mit p(0)..p(i) abbilden
TableCell = cell(length(aktuell_Modell_P_str{1,1}),6);
for aktuell_Modell_P_i = 1: length(aktuell_Modell_P_str{1,1})
    %%%Finde aktuellen ESB-Element-Namen
    P_Name = aktuell_Modell_P_str{1,1}{aktuell_Modell_P_i,1};
    DRT_GUI.Fit.aktuell_Modell.P_Name{1,aktuell_Modell_P_i} = P_Name;
    %Par_temp = strcat('app.Par',num2str(aktuell_Modell_P_i));
    TableCell{aktuell_Modell_P_i,1} = P_Name;
    if numel(find(DRT_GUI.Fit.Parameter~=0))==0
        TableCell{aktuell_Modell_P_i,3} = cell2mat(aktuell_Modell{4}(aktuell_Modell_P_i));
        TableCell{aktuell_Modell_P_i,4} = cell2mat(aktuell_Modell{5}(aktuell_Modell_P_i));
        TableCell{aktuell_Modell_P_i,5} = cell2mat(aktuell_Modell{6}(aktuell_Modell_P_i));
        if ~isempty(aktuell_Modell{7})
            TableCell{aktuell_Modell_P_i,2} = logical(cell2mat(aktuell_Modell{7}(aktuell_Modell_P_i)));
        else
            TableCell{aktuell_Modell_P_i,2} = false;
        end
    else
        TableCell{aktuell_Modell_P_i,3} = DRT_GUI.Fit.Parameter(aktuell_Modell_P_i);
        TableCell{aktuell_Modell_P_i,4} = DRT_GUI.Fit.Parameter_min(aktuell_Modell_P_i);
        TableCell{aktuell_Modell_P_i,5} = DRT_GUI.Fit.Parameter_max(aktuell_Modell_P_i);
        if aktuell_Modell_P_i <= length(DRT_GUI.Fit.ParFix)
            TableCell{aktuell_Modell_P_i,2} = logical(DRT_GUI.Fit.ParFix(aktuell_Modell_P_i));
        else
            TableCell{aktuell_Modell_P_i,2} = 'false';
        end
    end
    if ~isempty(find(index==aktuell_Modell_P_i,1,'first'))
        TableCell{aktuell_Modell_P_i,6} = true;
    else
        TableCell{aktuell_Modell_P_i,6} = false;
    end
    if ~isempty(oldFit) && ismember(P_Name,oldFit.aktuell_Modell.P_Name(1,:)) && ~strcmp(oldFit.aktuell_Modell.Modellname,DRT_GUI.Fit.aktuell_Modell.Modellname)
        OldIndex = find(strcmp(oldFit.aktuell_Modell.P_Name(1,:),P_Name),1,'first');
        TableCell{aktuell_Modell_P_i,3} = oldFit.Parameter(OldIndex);
        TableCell{aktuell_Modell_P_i,4} = oldFit.Parameter_min(OldIndex);
        TableCell{aktuell_Modell_P_i,5} = oldFit.Parameter_max(OldIndex);
        if numel(oldFit.ParFix)>=OldIndex
            TableCell{aktuell_Modell_P_i,2} = logical(oldFit.ParFix(OldIndex));
        end
    end
    % Formel f�r Rechnen umschreiben
    neu_P = strcat('p(',num2str(aktuell_Modell_P_i),')');
    DRT_GUI.Fit.aktuell_Modell.P_Name{2,aktuell_Modell_P_i} = neu_P;
    DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell = strrep(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell,P_Name,neu_P);
end
app.ParamTable.Data=TableCell;
DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_komp=DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell;
DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_komp= strrep(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_komp,'HF','0');
DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_komp = strrep(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_komp,'LF','0');
DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_komp = strrep(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_komp,'MF','1');
DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_HF=DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell;
DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_HF= strrep(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_HF,'HF','1');
DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_HF = strrep(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_HF,'LF','0');
DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_HF = strrep(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_HF,'MF','0');
DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_MF=DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell;
DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_MF= strrep(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_MF,'HF','0');
DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_MF = strrep(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_MF,'LF','0');
DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_MF = strrep(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_MF,'MF','1');
DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_LF=DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell;
DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_LF= strrep(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_LF,'HF','0');
DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_LF = strrep(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_LF,'LF','1');
DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_LF = strrep(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_LF,'MF','0');
DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell      = strrep(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell,'HF','1');
DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell      = strrep(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell,'LF','1');
DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell      = strrep(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell,'MF','1');
% for aktuell_Modell_P_i = 1:length(aktuell_Modell_P_str{1,1})
%     
% end
if ~sum(ismember(fieldnames(DRT_GUI.Fit),'Implementierung')) || isempty(DRT_GUI.Fit.Implementierung) || ~sum(ismember(fieldnames(DRT_GUI.Fit.Implementierung),'Table')) || isempty(DRT_GUI.Fit.Implementierung.Table) || size(DRT_GUI.Fit.Implementierung.Table,1)==1
    Implementierung_Neu_Laden
else
    Mismatch=0;
    for i = 1:size(DRT_GUI.Fit.Implementierung.Table,1)-1
        for j = 5:size(DRT_GUI.Fit.Implementierung.Table,2)
            if ~isempty(DRT_GUI.Fit.Implementierung.Table{i,j}) && isempty(find(strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:) ,  DRT_GUI.Fit.Implementierung.Table{i,j}),1))
                Implementierung_Neu_Laden
                Mismatch = 1;
                break
            end
        end
        if Mismatch , break, end
    end
end
app.GlobalFitUsedModelDropDown.Value = app.ModellAuswahlPopup.Value;
[row,col] = size(app.ParamTable.Data);
if col > 0
    Param_Data = app.ParamTable.Data;
    min_array = app.ParamTable.Data(:,4);
    min_array = transpose(cell2mat(min_array));
    max_array = app.ParamTable.Data(:,5);
    max_array = transpose(cell2mat(max_array));
    act_value_array = app.ParamTable.Data(:,3);
    act_value_array = transpose(cell2mat(act_value_array));
    if isequal(min_array,zeros(1,length(min_array))) || isequal(DRT_GUI.Fit.Parameter_min,zeros(1,length(min_array)))
        for n = 1:length(min_array)
            Param_Data{n,4} = DRT_GUI.Fit.aktuell_Modell.ModellCell{1, 5}{n};
            DRT_GUI.Fit.Parameter_min(n) = DRT_GUI.Fit.aktuell_Modell.ModellCell{1, 5}{n};
        end
    end
    if isequal(max_array,zeros(1,length(max_array))) || isequal(DRT_GUI.Fit.Parameter_max,zeros(1,length(min_array)))
        for n = 1:length(max_array)
            Param_Data{n,5} = DRT_GUI.Fit.aktuell_Modell.ModellCell{1, 6}{n};
            DRT_GUI.Fit.Parameter_max(n) = DRT_GUI.Fit.aktuell_Modell.ModellCell{1, 6}{n};
        end
    end
    if isequal(act_value_array,[0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0]) || isequal(DRT_GUI.Fit.Parameter,zeros(1,length(act_value_array)))
        for n = 1:length(act_value_array)
            Param_Data{n,3} = DRT_GUI.Fit.aktuell_Modell.ModellCell{1, 4}{n};
            DRT_GUI.Fit.Parameter(n) = DRT_GUI.Fit.aktuell_Modell.ModellCell{1, 4}{n};
        end
    end
    if app.GUI
       set(app.ParamTable,'Data',Param_Data);
    else
       app.ParamTable.Data = Param_Data; 
    end
end

%Update TDF Tab if currently shown
if app.GUI
   if strcmp(app.MainTabGroupRight.SelectedTab.Title,'Time Domain Fitting')
       if ~exist('event','var'), event = []; end
       fcn_TDF_OpeningFcn(app, event);
   end
end
pause(0.01)
end

