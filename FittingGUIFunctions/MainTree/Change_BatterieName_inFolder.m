function Change_BatterieName_inFolder(app,Folder, AlterName, NeuerName)
global DRT_GUI
if Folder(end) == '/' || Folder(end) == '\'
    Folder = Folder(1:end-1);
end

FileList = dir([Folder '/*' AlterName '*.mat']);
for i = 1:numel(FileList)
    movefile([Folder '/' FileList(i).name],  [Folder '/' strrep(FileList(i).name,AlterName,NeuerName)])
    if ~strcmp(FileList(i).name(end-10:end),'_Modell.mat')
        display(['Modifiziere ' FileList(i).name]);
        load([Folder '/' strrep(FileList(i).name,AlterName,NeuerName)])
        DRT_GUI.Testparameter.Batterie = NeuerName;
        save([Folder '/' strrep(FileList(i).name,AlterName,NeuerName)],'DRT_GUI')
        clear DRT_GUI Fit
    end
end
FolderList = dir([Folder '/*']);
for i = 1:numel(FolderList)
    if FolderList(i).isdir && ~strcmp(FolderList(i).name,'.') && ~strcmp(FolderList(i).name,'..')
        Change_BatterieName_inFolder(app,[Folder '/' FolderList(i).name],AlterName,NeuerName)
    end
end
end