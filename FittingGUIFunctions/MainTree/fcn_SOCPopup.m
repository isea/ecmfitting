function app = fcn_SOCPopup(app, event)
global DRT_GUI
Batterien = app.BatterieNamePopup.Items;
app.ZustandPopup.Items = fcn_killHTMLtags(app.ZustandPopup.Items);
Temperaturen = app.TemperaturPopup.Items;
SOCs = app.SOCPopup.Items;
SOCsDbl = zeros(numel(SOCs)-1,1);
for i = 2:numel(SOCs)
    SOCsDbl(i-1) = str2num(strrep(SOCs{i},'m','-'));
end
%Erg�nzung:
Folders = {''};
if strcmp(app.SOCPopup.Value,app.SOCPopup.Items{1}) && ~isempty(DRT_GUI) && sum(ismember(fieldnames(DRT_GUI),'Testparameter')) && ~isempty(DRT_GUI.Testparameter) && sum(ismember(fieldnames(DRT_GUI.Testparameter),'SOC')) && ~isempty(DRT_GUI.Testparameter.SOC) && (~strcmp(app.BatterieNamePopup.Value,DRT_GUI.Testparameter.Batterie) || ~strcmp(app.ZustandPopup.Value,DRT_GUI.Testparameter.Zustand) || ~strcmp(app.TemperaturPopup.Value,[strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad']))
    nextSOC = 1+find(abs(SOCsDbl-DRT_GUI.Testparameter.SOC)==min(abs(SOCsDbl-DRT_GUI.Testparameter.SOC)),1,'first');
    if ~isempty(nextSOC)
        app.SOCPopup.Value=app.SOCPopup.Items{nextSOC};
        app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{nextSOC};
    end
elseif strcmp(app.SOCPopup.Value,app.SOCPopup.Items{1}) && ~isempty(DRT_GUI) && sum(ismember(fieldnames(DRT_GUI),'Testparameter')) && ~isempty(DRT_GUI.Testparameter) && sum(ismember(fieldnames(DRT_GUI.Testparameter),'SOC')) && ~isempty(DRT_GUI.Testparameter.SOC)
    nextSOC = 1+find(SOCsDbl==DRT_GUI.Testparameter.SOC,1,'first');
    if ~isempty(nextSOC)
        app.SOCPopup.Value=app.SOCPopup.Items{nextSOC};
        app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{nextSOC};
    end
end
if ~strcmp(app.SOCPopup.Value,app.SOCPopup.Items{1})
    if app.GUI
        f = dir(['output/' app.BatterieNamePopup.Value '/' app.StateString '/' app.TemperaturPopup.Value '/*_' app.SOCPopup.Value 'SOC.mat']);
    else
        f = dir([app.OutputPath '/' app.BatterieNamePopup.Value '/'  app.StateString '/'  app.TemperaturPopup.Value '/*_' app.SOCPopup.Value 'SOC.mat']);
    end
    if ~isempty(f) && numel(f)==1
        if app.GUI
            load(['output/' app.BatterieNamePopup.Value '/' app.StateString '/' app.TemperaturPopup.Value '/' f(1).name]);
        else
            load([app.OutputPath '/' app.BatterieNamePopup.Value '/' app.StateString '/' app.TemperaturPopup.Value '/' f(1).name]);
        end
        fcn_StructReparieren()
        app = fcn_Alles_Laden(app, event);  
    end
end
%fcn_Alles_Laden(app, event)
[row,col] = size(app.ParamTable.Data);
if col > 0
    Param_Data = app.ParamTable.Data;
    min_array = app.ParamTable.Data(:,4);
    min_array = transpose(cell2mat(min_array));
    max_array = app.ParamTable.Data(:,5);
    max_array = transpose(cell2mat(max_array));
    act_value_array = app.ParamTable.Data(:,3);
    act_value_array = transpose(cell2mat(act_value_array));
    if isequal(min_array,zeros(1,length(min_array)))
        for n = 1:length(min_array)
            Param_Data{n,4} = DRT_GUI.Fit.aktuell_Modell.ModellCell{1, 5}{n};
            DRT_GUI.Fit.Parameter_min(n) = DRT_GUI.Fit.aktuell_Modell.ModellCell{1, 5}{n};
        end
    end
    if isequal(max_array,zeros(1,length(max_array)))
        for n = 1:length(max_array)
            Param_Data{n,5} = DRT_GUI.Fit.aktuell_Modell.ModellCell{1, 6}{n};
            DRT_GUI.Fit.Parameter_max(n) = DRT_GUI.Fit.aktuell_Modell.ModellCell{1, 6}{n};
        end
    end
    if isequal(act_value_array,[0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0])
        for n = 1:length(act_value_array)
            Param_Data{n,3} = DRT_GUI.Fit.aktuell_Modell.ModellCell{1, 4}{n};
            DRT_GUI.Fit.Parameter(n) = DRT_GUI.Fit.aktuell_Modell.ModellCell{1, 4}{n};
        end
    end
    if app.GUI
        set(app.ParamTable,'Data',Param_Data)
    else
        app.ParamTable.Data = Param_Data;
    end
end
if ~isempty(DRT_GUI)
    if  isrow(DRT_GUI.Messdaten.Zreal)
        DRT_GUI.Messdaten.Zreal = transpose(DRT_GUI.Messdaten.Zreal);
    end
    if  isrow(DRT_GUI.Messdaten.Zimg)
        DRT_GUI.Messdaten.Zimg = transpose(DRT_GUI.Messdaten.Zimg);
    end
    if  isrow(DRT_GUI.Messdaten.frequenz)
        DRT_GUI.Messdaten.frequenz = transpose(DRT_GUI.Messdaten.frequenz);
    end
    if  isrow(DRT_GUI.Messdaten.omega)
        DRT_GUI.Messdaten.omega = transpose(DRT_GUI.Messdaten.omega);
    end
    if  isrow(DRT_GUI.Messdaten.tau)
        DRT_GUI.Messdaten.tau = transpose(DRT_GUI.Messdaten.tau);
    end
    if  isrow(DRT_GUI.Messdaten.Z)
        DRT_GUI.Messdaten.Z = transpose(DRT_GUI.Messdaten.Z);
    end
    if  isrow(DRT_GUI.Messdaten.aktiv)
        DRT_GUI.Messdaten.aktiv = transpose(DRT_GUI.Messdaten.aktiv);
    end
    if  isrow(DRT_GUI.Korrigiert.Zreal)
        DRT_GUI.Korrigiert.Zreal = transpose(DRT_GUI.Korrigiert.Zreal);
    end
    if  isrow(DRT_GUI.Korrigiert.Zimg)
        DRT_GUI.Korrigiert.Zimg = transpose(DRT_GUI.Korrigiert.Zimg);
    end
    if  isrow(DRT_GUI.Korrigiert.frequenz)
        DRT_GUI.Korrigiert.frequenz = transpose(DRT_GUI.Korrigiert.frequenz);
    end
    if  isrow(DRT_GUI.Korrigiert.omega)
        DRT_GUI.Korrigiert.omega = transpose(DRT_GUI.Korrigiert.omega);
    end
    if  isrow(DRT_GUI.Korrigiert.tau)
        DRT_GUI.Korrigiert.tau = transpose(DRT_GUI.Korrigiert.tau);
    end
    if  isrow(DRT_GUI.Korrigiert.Z)
        DRT_GUI.Korrigiert.Z = transpose(DRT_GUI.Korrigiert.Z);
    end
    if  isrow(DRT_GUI.Korrigiert.aktiv)
        DRT_GUI.Korrigiert.aktiv = transpose(DRT_GUI.Korrigiert.aktiv);
    end
    if  isrow(DRT_GUI.Fit.Zreal)
        DRT_GUI.Fit.Zreal = transpose(DRT_GUI.Fit.Zreal);
    end
    if  isrow(DRT_GUI.Fit.Zimg)
        DRT_GUI.Fit.Zimg = transpose(DRT_GUI.Fit.Zimg);
    end
    if  isrow(DRT_GUI.Fit.frequenz)
        DRT_GUI.Fit.frequenz = transpose(DRT_GUI.Fit.frequenz);
    end
    if  isrow(DRT_GUI.Fit.omega)
        DRT_GUI.Fit.omega = transpose(DRT_GUI.Fit.omega);
    end
    if  isrow(DRT_GUI.Fit.tau)
        DRT_GUI.Fit.tau = transpose(DRT_GUI.Fit.tau);
    end
    if  isrow(DRT_GUI.Fit.Z)
        DRT_GUI.Fit.Z = transpose(DRT_GUI.Fit.Z);
    end
end
if app.GUI
   if strcmp(app.MainTabGroupRight.SelectedTab.Title,'LinKK')
                 plot_linkk(app);
       
   elseif strcmp(app.MainTabGroupRight.SelectedTab.Title,'Time Domain Fitting')
       fcn_ZeitbereichsFittingButton(app, event);
   elseif strcmp(app.MainTabGroupRight.SelectedTab.Title,'Load History Dependency')
       fcn_LHD_TabInitialization(app,event);
   end
end
pause(0.01)
end