function fcn_RelaxFFT_PunkteWegnehmenButton(app,event)
global DRT_GUI;
if isempty(DRT_GUI) || isempty(cell2mat(strfind(fieldnames(DRT_GUI),'Messdaten'))) || isempty(DRT_GUI.Messdaten) || isempty(cell2mat(strfind(fieldnames(DRT_GUI.Messdaten),'relax_fft'))) || isempty(DRT_GUI.Messdaten.relax_fft)
    return
end
Typ = 'Nyquist';
%if ~strcmp(eventdata,'kein_plot')
    hold(app.axes1,'off');
    fcn_plot_Auswaehl(app,DRT_GUI.Messdaten,[],[],DRT_GUI.Testparameter.Batterie,Typ);
%else
%end
LP_str = get(app.RelaxFFT_PunkteWegnehmenTextBox,'Value');
DRT_GUI.Messdaten.relax_fft.aktiv = ones(size(DRT_GUI.Messdaten.relax_fft.Z));
if ~isempty(get(app.RelaxFFT_PunkteWegnehmenTextBox,'Value'))
    eval(['DRT_GUI.Messdaten.relax_fft.aktiv([' LP_str ']) = 0;']);
end
DRT_GUI.Messdaten.relax_fft.low_Punkte_Weg = LP_str;
Typ = 'Messung';
% neu plot
%if ~strcmp(eventdata, 'kein_plot')
    hold(app.axes1,'off');
    fcn_plot_Auswaehl(app,DRT_GUI.Messdaten,[],[],DRT_GUI.Testparameter.Batterie,Typ);
%else
%end
end