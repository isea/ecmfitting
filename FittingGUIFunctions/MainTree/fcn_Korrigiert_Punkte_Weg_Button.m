function app = fcn_Korrigiert_Punkte_Weg_Button(app, event)

global DRT_GUI;
if isempty(DRT_GUI) || isempty(cell2mat(strfind(fieldnames(DRT_GUI),'Korrigiert')))
    return
end
if app.GUI
    LP_str = get(app.Korrigiert_Punkte_Weg_TextBox,'Value');
else
    LP_str = app.Korrigiert_Punkte_Weg_TextBox.Value;
end
DRT_GUI.Korrigiert.aktiv = true(size(DRT_GUI.Korrigiert.Z));
if app.GUI
    if ~isempty(get(app.Korrigiert_Punkte_Weg_TextBox,'Value'))
        if strcmp(LP_str , '0:0'), LP_str = '1:end';        end
        eval(['DRT_GUI.Korrigiert.aktiv([' LP_str ']) = false;']);
        DRT_GUI.Korrigiert.aktiv = DRT_GUI.Korrigiert.aktiv(1:numel(DRT_GUI.Korrigiert.Z));
        if sum(DRT_GUI.Korrigiert.aktiv)<2 , DRT_GUI.Korrigiert.aktiv = true(size(DRT_GUI.Korrigiert.Z));end
    end
else
    if ~isempty(app.Korrigiert_Punkte_Weg_TextBox.Value)
        if strcmp(LP_str , '0:0'), LP_str = '1:end';        end
        eval(['DRT_GUI.Korrigiert.aktiv([' LP_str ']) = false;']);
        DRT_GUI.Korrigiert.aktiv = DRT_GUI.Korrigiert.aktiv(1:numel(DRT_GUI.Korrigiert.Z));
        if sum(DRT_GUI.Korrigiert.aktiv)<2 , DRT_GUI.Korrigiert.aktiv = true(size(DRT_GUI.Korrigiert.Z));end
    end
end
DRT_GUI.Korrigiert.Punkte_Weg = LP_str;
%Smoothing Spline berechnen
if length(DRT_GUI.Korrigiert.aktiv) > length(DRT_GUI.Korrigiert.frequenz)
    DRT_GUI.Korrigiert.aktiv = ones(size(DRT_GUI.Korrigiert.frequenz));
    [f_sorted,order] = sort(DRT_GUI.Korrigiert.frequenz(DRT_GUI.Korrigiert.aktiv));
else
    [f_sorted,order] = sort(DRT_GUI.Korrigiert.frequenz(DRT_GUI.Korrigiert.aktiv));
end
Zreal_sorted = DRT_GUI.Korrigiert.Zreal(DRT_GUI.Korrigiert.aktiv);
Zreal_sorted = Zreal_sorted(order);
Zimg_sorted = DRT_GUI.Korrigiert.Zimg(DRT_GUI.Korrigiert.aktiv);
Zimg_sorted = Zimg_sorted(order);
min_delta_f = min(diff(log10(f_sorted)));
DRT_GUI.Korrigiert.spaps.frequenz = 10.^(log10(f_sorted(1)):min_delta_f:log10(f_sorted(end)));
DRT_GUI.Korrigiert.spaps.omega = 2*pi*DRT_GUI.Korrigiert.spaps.frequenz ;
DRT_GUI.Korrigiert.spaps.tau = 1./ DRT_GUI.Korrigiert.spaps.omega;
% if license('checkout', 'Curve_Fitting_Toolbox') && length(Zreal_sorted)>2
%     DRT_GUI.Korrigiert.spaps.sp_real = spaps(log10(f_sorted),Zreal_sorted,1e-11,[100 ones(1,numel(f_sorted)-2) 100]',3);
%     DRT_GUI.Korrigiert.spaps.sp_img = spaps(log10(f_sorted),Zimg_sorted,1e-11,[100 ones(1,numel(f_sorted)-2) 100]',3);
%     DRT_GUI.Korrigiert.spaps.Zreal = fnval(DRT_GUI.Korrigiert.spaps.sp_real,log10(DRT_GUI.Korrigiert.spaps.frequenz));
%     DRT_GUI.Korrigiert.spaps.Zimg = fnval(DRT_GUI.Korrigiert.spaps.sp_img,log10(DRT_GUI.Korrigiert.spaps.frequenz));
% else
    DRT_GUI.Korrigiert.spaps.Zreal = spline(log10(f_sorted),Zreal_sorted,log10(DRT_GUI.Korrigiert.spaps.frequenz));
    DRT_GUI.Korrigiert.spaps.Zimg = spline(log10(f_sorted),Zimg_sorted,log10(DRT_GUI.Korrigiert.spaps.frequenz));
% end
% neu plot
if app.GUI
    Typ = 'Korrigiert_Fit';
    %             if ~strcmp(event.Value, 'kein_plot')
    % hold(app.axes2,'off');
    fcn_plot_Auswaehl(app,DRT_GUI.Korrigiert,DRT_GUI.Fit,[],DRT_GUI.Testparameter.Batterie,Typ)
end
% Modellelemente plotten, falls der ESB-Generator genutzt wurde
if app.GUI
    a = get(app.PlotElementsCheckbox,'Value');
else
    a = app.PlotElementsCheckbox.Value;
end
if a && ~isempty(DRT_GUI.Fit.aktuell_Modell.ModellCell{8})
    Zreal_vorher=0;
    for i = 1:numel(DRT_GUI.Fit.aktuell_Modell.ModellCell{8})
        Modell = DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{i};
        Zfun = Modell.Zfun_MF;
        for j = 1:numel(Modell.ParameterIndexes)
            Zfun = strrep(Zfun,Modell.inputs{j},['p(' num2str(Modell.ParameterIndexes(j)) ')']);
        end
        p = DRT_GUI.Fit.Parameter;
        w = DRT_GUI.Fit.omega;
        Z = eval(Zfun);       
        realverschiebung =  Zreal_vorher(end);
        % if Modell.Plot
        %     plot(app.axes2,real(Z)+realverschiebung,imag(Z),'k','display',Modell.Name)
        % end
        Zreal_vorher = Zreal_vorher+real(Z);
    end
end
end
