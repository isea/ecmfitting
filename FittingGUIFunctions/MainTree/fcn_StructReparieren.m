function fcn_StructReparieren()
global DRT_GUI
if ~isempty(DRT_GUI) && isstruct(DRT_GUI) && ismember('Fit',fieldnames(DRT_GUI)) && ...
        ~isempty(DRT_GUI.Fit)
    if ismember('aktuell_Modell',fieldnames(DRT_GUI.Fit)) && numel(DRT_GUI.Fit.aktuell_Modell.ModellCell)>8 && ~isempty(DRT_GUI.Fit.aktuell_Modell.ModellCell{8})
        for i = 1:numel(DRT_GUI.Fit.aktuell_Modell.ModellCell{8})
            if ~ismember('Funktionsname',fieldnames(DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{i}))
                DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{i}.Funktionsname = func2str(DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{i}.f);
            end
            if ismember('f',fieldnames(DRT_GUI.Fit.Implementierung.Info{i}))
                DRT_GUI.Fit.Implementierung.Info{i}=rmfield(DRT_GUI.Fit.Implementierung.Info{i},'f');
            end
            if ismember('Z',fieldnames(DRT_GUI.Fit.Implementierung.Info{i}))
                DRT_GUI.Fit.Implementierung.Info{i}=rmfield(DRT_GUI.Fit.Implementierung.Info{i},'Z');
            end
            if ismember('Z_LF',fieldnames(DRT_GUI.Fit.Implementierung.Info{i}))
                DRT_GUI.Fit.Implementierung.Info{i}=rmfield(DRT_GUI.Fit.Implementierung.Info{i},'Z_LF');
            end
            if ismember('Z_MF',fieldnames(DRT_GUI.Fit.Implementierung.Info{i}))
                DRT_GUI.Fit.Implementierung.Info{i}=rmfield(DRT_GUI.Fit.Implementierung.Info{i},'Z_MF');
            end
            if ismember('Z_HF',fieldnames(DRT_GUI.Fit.Implementierung.Info{i}))
                DRT_GUI.Fit.Implementierung.Info{i}=rmfield(DRT_GUI.Fit.Implementierung.Info{i},'Z_HF');
            end
        end
    end
end
end