function fcn_DRT_Config_reload_Button(app, event)
config();
global DRT_Config
% set(app.SchwingungSlider,'Value',DRT_Config.Schwingfaktor)
% set(app.SchwingungEdit,'Value',num2str(DRT_Config.Schwingfaktor))
% set(app.InterpolationSlider,'Value',log10(DRT_Config.InterpolationsFaktor))
set(app.InterpolationEdit,'Value',num2str(DRT_Config.InterpolationsFaktor))
set(app.FilterFaktorSlider,'Value',DRT_Config.FilterFaktor_ext)
set(app.FilterEdit,'Value',num2str(DRT_Config.FilterFaktor_ext))
set(app.ZeropaddingSlider,'Value',log10(DRT_Config.ZeroPadding))
set(app.ZeropaddingEdit,'Value',num2str(DRT_Config.ZeroPadding))
set(app.PeakfinderSlider,'Value',DRT_Config.PeakSensitivitaet)
set(app.PeakfinderEdit,'Value',num2str(DRT_Config.PeakSensitivitaet))
set(app.ProzesseSlider,'Value',DRT_Config.Prozesse)
set(app.ProzesseEdit,'Value',num2str(DRT_Config.Prozesse))
if ~sum(ismember(fieldnames(DRT_Config),'ZarcHN'))
    DRT_Config.ZarcHN=5;
end
set(app.ZarcHNPopup,'Value',app.ZarcHNPopup.Items{DRT_Config.ZarcHN})
fcn_DRT_Button(app, event);
end