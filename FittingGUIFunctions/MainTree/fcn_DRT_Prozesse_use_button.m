function fcn_DRT_Prozesse_use_button(app,event)
global DRT_GUI;
global Cont_Process_Fit_Counter;
if isempty(DRT_GUI) || isempty(cell2mat(strfind(fieldnames(DRT_GUI),'Messdaten')))
    return
end
TableCell = get(app.ParamTable,'Data');
if ~isempty(TableCell)
    TableCell = cell2struct(TableCell',{'Name','Fix','Value','Min','Max','Lim'});
end
ColNames = get(app.ProzesseTable,'ColumnName');
if strcmp(ColNames{1},'Val')
    ValCell = get(app.ProzesseTable,'Data');
    ValCell(~cell2mat(ValCell(:,2)),:)=[];
    for i = 1:size(ValCell,1)
        parnummer = find(strcmp(ValCell{i,3},DRT_GUI.Fit.aktuell_Modell.P_Name(1,:)));
        if ~isempty(parnummer)
            TableCell(parnummer).Value = ValCell{i,1};
        end
    end
else
    TauCell = get(app.ProzesseTable,'Data');
    if ~isempty(TableCell)
        TauCell = cell2struct(TauCell',{'tau','used','parname','r','phi'});
    end
    %Schreibt die Tau1, Tau2 Werte aus der Tautabelle in die Daten_Fix_Tabelle
    for i = 1:numel(TauCell)
        parnummer = find(strcmp(TauCell(i).parname,DRT_GUI.Fit.aktuell_Modell.P_Name(1,:)));
        if ~isempty(parnummer)
            TableCell(parnummer).Value = TauCell(i).tau;
            TableCell(parnummer).Min = TauCell(i).tau / 1.5;
            TableCell(parnummer).Max = TauCell(i).tau * 1.5;
        end
        parnummer = find(strcmpi(strrep(lower(TauCell(i).parname),'tau','r'),DRT_GUI.Fit.aktuell_Modell.P_Name(1,:)));
        if isempty(parnummer)
            parnummer = find(strcmpi(strrep(strrep(lower(TauCell(i).parname),'tau','r'),'dl','ct'),DRT_GUI.Fit.aktuell_Modell.P_Name(1,:)));
        end
        if ~isempty(parnummer)
            TableCell(parnummer).Value = TauCell(i).r;
            %         TableCell(parnummer).Min = TauCell(i).r * 2;
            %         TableCell(parnummer).Max = TauCell(i).r * 0.5;
        end
    end
end
TableCell = struct2cell(TableCell)';
set(app.ParamTable,'Data',TableCell)
fcn_PlotFittedParametersButton(app,event) %fcn_PlotFittedParametersButton_Callback(app, 'kein_plot')
end