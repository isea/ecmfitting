function fcn_ZeropaddingSlider(app,event)
global DRT_Config
DRT_Config.ZeroPadding = round(10^get(app.ZeropaddingSlider,'Value'));
set(app.ZeropaddingEdit,'Value',num2str(DRT_Config.ZeroPadding))
fcn_DRTButton_Callback(app, event)
end