function app = fcn_SpeichernButton(app, event)
% Fitdaten auslesen
global DRT_GUI;
if isempty(DRT_GUI) || isempty(cell2mat(strfind(fieldnames(DRT_GUI),'DRT')))
    msgbox('Bitte erstellen Sie zun�chst eine DRT!')
    return
end
if app.CRateFlag && app.HysteresisFlag
    StateString = [DRT_GUI.Testparameter.Zustand_2 '_' DRT_GUI.Testparameter.Zustand];    
elseif app.CRateFlag
    StateString = DRT_GUI.Testparameter.Zustand;
else
    StateString = DRT_GUI.Testparameter.Zustand_2;
end
app.StateString = StateString;
if app.GUI
    if isempty(dir('output'))
        mkdir('output')
    end
    if iscell(DRT_GUI.Testparameter.Batterie)
        DRT_GUI.Testparameter.Batterie = cell2mat(DRT_GUI.Testparameter.Batterie);
    end
    if isempty( dir(['output' '/' DRT_GUI.Testparameter.Batterie]))
        mkdir(['output' '/' DRT_GUI.Testparameter.Batterie])
    end
    if isempty(cell2mat(strfind(fieldnames(DRT_GUI.Testparameter),'Zustand'))) || isempty(DRT_GUI.Testparameter.Zustand)
        DRT_GUI.Testparameter.Zustand = 'default';
        set(app.ZustandTextBox,'Value','default')
    end
    if ~isempty(cell2mat(strfind(fieldnames(DRT_GUI.DRT),'UserTau')))
        DRT_GUI.DRT=rmfield(DRT_GUI.DRT,'UserTau');
    end
    if isempty( dir(['output' '/' DRT_GUI.Testparameter.Batterie '/' app.StateString]))
        mkdir(['output' '/' DRT_GUI.Testparameter.Batterie '/' app.StateString])
    end
    if isempty( dir(['output' '/' DRT_GUI.Testparameter.Batterie '/' app.StateString '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad']))
        mkdir(['output' '/' DRT_GUI.Testparameter.Batterie '/' app.StateString '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad'])
    end
    save(['output' '/' DRT_GUI.Testparameter.Batterie '/' app.StateString '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad/' ...
        DRT_GUI.Testparameter.Batterie '_' StateString '_' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad_' [strrep(num2str(DRT_GUI.Testparameter.SOC),'-','m')] 'SOC.mat'],'DRT_GUI','-v7.3');
else
    if isempty(dir(app.OutputPath))
        mkdir(app.OutputPath)
    end
    if iscell(DRT_GUI.Testparameter.Batterie)
        DRT_GUI.Testparameter.Batterie = cell2mat(DRT_GUI.Testparameter.Batterie);
    end
    if isempty( dir([app.OutputPath '/' DRT_GUI.Testparameter.Batterie]))
        mkdir([app.OutputPath '/' DRT_GUI.Testparameter.Batterie])
    end
    if isempty(cell2mat(strfind(fieldnames(DRT_GUI.Testparameter),'Zustand'))) || isempty(DRT_GUI.Testparameter.Zustand)
        DRT_GUI.Testparameter.Zustand = 'default';
        app.ZustandTextBox.Value = 'default';
    end
    if ~isempty(cell2mat(strfind(fieldnames(DRT_GUI.DRT),'UserTau')))
        DRT_GUI.DRT=rmfield(DRT_GUI.DRT,'UserTau');       
    end
    if isempty( dir([app.OutputPath '/' DRT_GUI.Testparameter.Batterie '/' app.StateString]))
        mkdir([app.OutputPath' '/' DRT_GUI.Testparameter.Batterie '/' app.StateString])
    end
    if isempty( dir([app.OutputPath '/' DRT_GUI.Testparameter.Batterie '/' app.StateString '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad']))
        mkdir([app.OutputPath '/' DRT_GUI.Testparameter.Batterie '/' app.StateString '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad'])
    end
    save([app.OutputPath '/' DRT_GUI.Testparameter.Batterie '/' app.StateString '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad/' ...
        DRT_GUI.Testparameter.Batterie '_' StateString '_' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad_' [strrep(num2str(DRT_GUI.Testparameter.SOC),'-','m')] 'SOC.mat'],'DRT_GUI','-v7.3');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Fit = [];
if sum(ismember(fieldnames(DRT_GUI),'Fit')) && ~isempty(DRT_GUI.Fit) &&  sum(ismember(fieldnames(DRT_GUI.Fit),'aktuell_Modell')) && ~isempty(DRT_GUI.Fit.aktuell_Modell) && sum(ismember(fieldnames(DRT_GUI.Fit),'Parameter')) && ~isempty(DRT_GUI.Fit.Parameter)
    Fit.Modell = rmfield(DRT_GUI.Fit.aktuell_Modell,'ModellCell');
    Fit.Modell.ModellCell = DRT_GUI.Fit.aktuell_Modell.ModellCell(1:7);
    Fit.Parameter = DRT_GUI.Fit.Parameter;
    Fit.residuum = DRT_GUI.Fit.residuum;
    Fit.Parameter_min = DRT_GUI.Fit.Parameter_min;
    Fit.Parameter_max = DRT_GUI.Fit.Parameter_max;
    if sum(ismember(fieldnames(DRT_GUI.Fit),'gueltig'))
        Fit.gueltig = DRT_GUI.Fit.gueltig;
    else
        Fit.gueltig = 0;
    end
    if sum(ismember(fieldnames(DRT_GUI.Fit),'Implementierung')) && ~isempty(DRT_GUI.Fit.Implementierung)
        %         Fit.Implementierung = FittingGUI.Fit.Implementierung;
        %         if ismember('Sim',fieldnames(Fit.Implementierung));
        %             Fit.Implementierung = rmfield(Fit.Implementierung,'Sim');
        %         end
        if sum(strcmp(fieldnames(DRT_GUI.Fit.Implementierung),'OCV')) && ~isempty(DRT_GUI.Fit.Implementierung.OCV)
            if numel(find(strcmp(DRT_GUI.Fit.Implementierung.OCV(:,1),'ROCV')))==1 && ...
                    numel(find(strcmp(DRT_GUI.Fit.Implementierung.OCV(:,1),'COCV')))==1 && ...
                    numel(find(strcmp(DRT_GUI.Fit.Implementierung.OCV(:,1),'UEnde')))==1
                ROCV = DRT_GUI.Fit.Implementierung.OCV{strcmp(DRT_GUI.Fit.Implementierung.OCV(:,1),'ROCV'),2};
                COCV = DRT_GUI.Fit.Implementierung.OCV{strcmp(DRT_GUI.Fit.Implementierung.OCV(:,1),'COCV'),2};
                UOCV = DRT_GUI.Fit.Implementierung.OCV{strcmp(DRT_GUI.Fit.Implementierung.OCV(:,1),'UEnde'),2};
                if isnumeric(ROCV) && isnumeric(COCV) && isnumeric(UOCV) && ~isnan(ROCV) && ~isnan(COCV) && ~isnan(UOCV)
                    Fit.Modell.Modellname = [Fit.Modell.Modellname '_OCV'];
                    cat_data = cell(size(Fit.Modell.P_Name,1),3);
                    cat_data(1,1:3) = {'ROCV','COCV','UOCV'};
                    Fit.Modell.P_Name = [Fit.Modell.P_Name cat_data];
                    Fit.Modell.ModellCell{1,1}=Fit.Modell.Modellname;
                    Fit.Modell.ModellCell{1,3}=[Fit.Modell.ModellCell{1,3} ',ROCV,COCV,UOCV'];
                    Fit.Modell.ModellCell{1,4}=[Fit.Modell.ModellCell{1,4} zeros(1,3)];
                    Fit.Modell.ModellCell{1,5}=[Fit.Modell.ModellCell{1,5} zeros(1,3)];
                    Fit.Modell.ModellCell{1,6}=[Fit.Modell.ModellCell{1,6} zeros(1,3)];
                    Fit.Modell.ModellCell{1,7}=[Fit.Modell.ModellCell{1,7} ones(1,3)];
                    Fit.Parameter = [Fit.Parameter ROCV COCV UOCV];
                    Fit.Parameter_min = [Fit.Parameter_min ROCV COCV UOCV];
                    Fit.Parameter_max = [Fit.Parameter_max ROCV COCV UOCV];                    
                end
                ReFit_indices = find(~cellfun(@isempty,regexp(DRT_GUI.Fit.Implementierung.OCV(:,1),'ReFit_*')))';
                CD_ReFit_indices = find(~cellfun(@isempty,regexp(DRT_GUI.Fit.Implementierung.OCV(:,1),'(\d+)C')))';
                if ~isempty(ReFit_indices)
                    ReFit_Values = cell2mat(DRT_GUI.Fit.Implementierung.OCV(ReFit_indices,2))';
                    ReFit_Names = DRT_GUI.Fit.Implementierung.OCV(ReFit_indices,1)';
                    if isfield(DRT_GUI.Fit,'')
                        
                    end
                    cat_data = cell(size(Fit.Modell.P_Name,1),length(ReFit_Names));
                    for i = 1:length(ReFit_Names)
                        cat_data{1,i} = ReFit_Names{i};
                    end
%                     Fit.Modell.P_Name = [Fit.Modell.P_Name [ReFit_Names ; repmat({''},1,numel(ReFit_indices))]];
                    Fit.Modell.P_Name = [Fit.Modell.P_Name cat_data];
                    for i_rf = 1:numel(ReFit_indices)
                        Fit.Modell.ModellCell{1,3}=[Fit.Modell.ModellCell{1,3} ',' ReFit_Names{i_rf}];
                    end
                    Fit.Modell.ModellCell{1,4}=[Fit.Modell.ModellCell{1,4} zeros(1,numel(ReFit_indices))];
                    Fit.Modell.ModellCell{1,5}=[Fit.Modell.ModellCell{1,5} zeros(1,numel(ReFit_indices))];
                    Fit.Modell.ModellCell{1,6}=[Fit.Modell.ModellCell{1,6} zeros(1,numel(ReFit_indices))];
                    Fit.Modell.ModellCell{1,7}=[Fit.Modell.ModellCell{1,7} ones(1,numel(ReFit_indices))];
                    Fit.Parameter = [Fit.Parameter ReFit_Values];
                    Fit.Parameter_min = [Fit.Parameter_min ReFit_Values];
                    Fit.Parameter_max = [Fit.Parameter_max ReFit_Values];
                    if ~isempty(CD_ReFit_indices)
                        Fit.Modell.P_Name(3,:) = num2cell(zeros(size(Fit.Modell.P_Name(2,:))));
                        Fit.Modell.P_Name(3,length(find(cellfun(@isempty,regexp(Fit.Modell.P_Name(1,:),'ReFit_*'))))+CD_ReFit_indices-4) = num2cell(ones(size(Fit.Modell.P_Name(3,length(find(cellfun(@isempty,regexp(Fit.Modell.P_Name(1,:),'ReFit_*'))))+CD_ReFit_indices-4))));
                    end
                end
            end
        end
    end    
end
DRT_GUI.FR_Fit = [];
DRT_GUI.FR_Fit = Fit;
if app.GUI
    save(['output' '/' DRT_GUI.Testparameter.Batterie '/' app.StateString '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad/' ...
        DRT_GUI.Testparameter.Batterie '_' StateString '_' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad_' [strrep(num2str(DRT_GUI.Testparameter.SOC),'-','m')] 'SOC.mat'],'DRT_GUI','-v7.3');
    Folders = {''};
    f = dir('output/*');
    for i = 1:numel(f)
        if ~strcmp(f(i).name,'.') && ~strcmp(f(i).name,'..') && f(i).isdir
            Folders = [Folders;f(i).name];
        end
    end
    set(app.BatterieNamePopup,'Items',Folders);
else
    save(['output' '/' DRT_GUI.Testparameter.Batterie '/' app.StateString '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad/' ...
        DRT_GUI.Testparameter.Batterie '_' StateString '_' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad_' [strrep(num2str(DRT_GUI.Testparameter.SOC),'-','m')] 'SOC.mat'],'DRT_GUI','-v7.3');
    Folders = {''};
    f = dir([app.OutputPath '/*']);
    for i = 1:numel(f)
        if ~strcmp(f(i).name,'.') && ~strcmp(f(i).name,'..') && f(i).isdir
            Folders = [Folders;f(i).name];
        end
    end
    app.BatterieNamePopup.Items = Folders;
end
app = fcn_BatterieNamePopup(app);
end