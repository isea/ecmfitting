function app = fcn_ZeitbereichsFittingButton(app, event)
global DRT_GUI
if isempty(DRT_GUI) || isempty(cell2mat(strfind(fieldnames(DRT_GUI),'Fit')))
    return
end
if app.GUI
    set(app.MainTabGroupRight,'SelectedTab',app.TDFTab)
else
    app.MainTabGroupRight.SelectedTab = app.TDFTab;
end
TempTable = [];
if ~isempty(DRT_GUI.Fit.Implementierung)
    TempTable=DRT_GUI.Fit.Implementierung.Table(:,5:end); %Liste der ESB-Elemente
else
    fcn_TDF_PlotOCVButton(app,event);
    return
end 
Array={};
%Erstelle Liste aller Parameter in ESB ohne Dopplungen des aktuellen
%Modells
for i1=1:size(TempTable,1)
    for i2=1:size(TempTable,2)
        temp=TempTable{i1,i2};
        if ~isempty(temp)
            if ~ismember(temp,Array)
                Array=[Array,{temp}];
            end
        end
    end
end
%Kontrolliere, ob in dem oberen Tabelle Parameter enthalten sind, die
%nicht in dem ESB vorkommen <-> ist die Tabelle mit dem Modell kompatibel,
%wenn nein neu laden
if ~sum(ismember(fieldnames(DRT_GUI.Fit),'Implementierung')) || isempty(DRT_GUI.Fit.Implementierung) || ~sum(ismember(fieldnames(DRT_GUI.Fit.Implementierung),'Table')) || isempty(DRT_GUI.Fit.Implementierung.Table) || size(DRT_GUI.Fit.Implementierung.Table,1)==1
    Implementierung_Neu_Laden
elseif size(DRT_GUI.Fit.Parameter,2)~=size(Array,2)
    Implementierung_Neu_Laden
else
    Mismatch=0;
    for i = 1:size(DRT_GUI.Fit.Implementierung.Table,1)-1
        for j = 5:size(DRT_GUI.Fit.Implementierung.Table,2)
            if ~isempty(DRT_GUI.Fit.Implementierung.Table{i,j}) && isempty(find(strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:) ,  DRT_GUI.Fit.Implementierung.Table{i,j}),1))
                Implementierung_Neu_Laden
                Mismatch = 1;
                break
            end
        end
        if Mismatch , break, end
    end
end
if ~sum(strcmp(fieldnames(DRT_GUI.Fit),'Implementierung')) , DRT_GUI.Fit.Implementierung=[];end
% oldData = DRT_GUI.Fit.Implementierung;
app = fcn_TDF_OpeningFcn(app, event);
if app.GUI
    if  ~(app.Nachbereitung_TDF.Value && app.GlobalFitInProgress) && ~(app.ZeitbereichsfittingCheckBox.Value && app.GlobalFitInProgress)             
        fcn_TDF_PlotOCVButton(app,event);
    end
end
end