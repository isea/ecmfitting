function fcn_Zreal_korrektur_textbox(app,event)
global DRT_GUI;
if  isempty(cell2mat(strfind(fieldnames(DRT_GUI),'Messdaten'))) || isempty(DRT_GUI.Messdaten)|| isempty(cell2mat(strfind(fieldnames(DRT_GUI.Messdaten),'relax_fft'))) || isempty(DRT_GUI.Messdaten.relax_fft)
    set(app.Zreal_korrektur_textbox,'Value','0.00');
    %fcn_Relax_Visible(app,'off')
end
try
    Wert = str2num(get(app.Zreal_korrektur_textbox,'Value'));
    if isempty(Wert),Wert=0;end
    DRT_GUI.Messdaten.relax_fft.Zreal_korrektur = Wert;
    set(app.Zreal_korrektur_textbox,'Value',num2str(DRT_GUI.Messdaten.relax_fft.Zreal_korrektur,'%8.4e'))
catch error_msg
    set(app.Zreal_korrektur_textbox,'Value',num2str(DRT_GUI.Messdaten.relax_fft.Zreal_korrektur,'%8.4e'))
    errordlg(error_msg)
end
end