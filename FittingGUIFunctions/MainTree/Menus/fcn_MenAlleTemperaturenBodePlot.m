function fcn_MenAlleTemperaturenBodePlot(app,event)
global DRT_GUI;
FarbenLaden
figure;
firstline = subplot(2,1,1);
grid on;axis square; axis equal; set(gca,'ydir', 'reverse');
new_ax = gca;
TempStrings = get(app.TemperaturPopup ,'Items');
Temps = cellfun(@str2num,strrep(strrep(TempStrings(2:end),'grad',''),'m','-'));
min_T = min(Temps);
max_T = max(Temps);
delete(firstline);
tempNumber = 1000;
map = colormap(jet(tempNumber));



for i_Z = 2:(numel(Temps)+1)
    if isempty(TempStrings{i_Z}) || ~isempty(strfind(TempStrings{i_Z},'gray')) || strcmp(TempStrings{i_Z},' ')
        continue
    end
    
    set(app.TemperaturPopup,'Value',TempStrings{i_Z})
    fcn_TemperaturPopup_Callback(app,event)
    akt_Temp=str2double(app.TemperaturTextBox.Value);
    Zustandstring = DRT_GUI.Testparameter.Zustand;
    FarbenIndex=round((DRT_GUI.Testparameter.Temperatur-min_T)*99/(max_T-min_T))+1;
    counter = 0;
    while FarbenIndex == -Inf && counter < 100000
        counter = counter +1;
        FarbenIndex=round((DRT_GUI.Testparameter.Temperatur-min_T)*99/(max_T-min_T))+1;
    end
    
    subplot(2,1,1)
    semilogx(DRT_GUI.Messdaten.frequenz,abs(DRT_GUI.Messdaten.Z),'-','LineWidth',2,'Color',map(tempToMap(akt_Temp,min_T,max_T,tempNumber),:))
    grid on, hold on
    if ~isempty(cell2mat(strfind(fieldnames(DRT_GUI.Messdaten),'relax_fft'))) && ~isempty(DRT_GUI.Messdaten.relax_fft)
        semilogx(DRT_GUI.Messdaten.relax_fft.frequenz,abs(DRT_GUI.Messdaten.relax_fft.Z+DRT_GUI.Messdaten.relax_fft.Zreal_korrektur),'LineWidth',2,'Color',map(tempToMap(akt_Temp,min_T,max_T,tempNumber),:))
    end
    ylabel('|Z| [\Omega]')
    xlabel('f [Hz]')
    subplot(2,1,2)
    semilogx(DRT_GUI.Messdaten.frequenz,angle(DRT_GUI.Messdaten.Z),'-','LineWidth',2,'Color',map(tempToMap(akt_Temp,min_T,max_T,tempNumber),:))

    grid on, hold on
    if ~isempty(cell2mat(strfind(fieldnames(DRT_GUI.Messdaten),'relax_fft'))) && ~isempty(DRT_GUI.Messdaten.relax_fft)
        semilogx(DRT_GUI.Messdaten.relax_fft.frequenz,angle(DRT_GUI.Messdaten.relax_fft.Z+DRT_GUI.Messdaten.relax_fft.Zreal_korrektur),'LineWidth',2,'Color',map(tempToMap(akt_Temp,min_T,max_T,tempNumber),:))
    end
    ylabel('angle(Z) [�]')
    xlabel('f [Hz]')
end

subplot(2,1,1)
grid on;
caxis([min_T,max_T]);
colorbar
subplot(2,1,2)
grid on;
caxis([min_T,max_T]);
colorbar
end