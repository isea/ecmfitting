function fcn_MenMasterFileEinlesen(app, event)
global DRT_GUI;
alter_pfad = pwd;
[filename, pathname] = uigetfile({'*.mat' 'Digatron-File'}, 'MasterFile ausw�hlen.', 'MultiSelect','off');
cd(alter_pfad);
if isempty(filename) || (~iscell(filename) && sum(filename == 0) )
    return
end
master = load([pathname filename]);
if ~sum(strcmp(fieldnames(master),'diga')) || isempty(master.diga) || ~sum(strcmp(fieldnames(master.diga),'daten')) || isempty(master.diga.daten)
    warning('%s ist kein Digatron-File',filename);
    return
end
if sum(strcmp(fieldnames(master.diga.daten),'Strom'))
    Strom = master.diga.daten.Strom;
elseif sum(strcmp(fieldnames(master.diga.daten),'Current'))
    Strom = master.diga.daten.Current;
else
    warning('Es wurden keine Werte f�r Strom gefunden!');
    return;
end
if max(Strom) > 1000, Strom = Strom/1000;end
if sum(strcmp(fieldnames(master.diga.daten),'Programmdauer'))
    Zeit = master.diga.daten.Programmdauer;
else
    warning('Es wurden keine Werte f�r Programmdauer gefunden!');
    return;
end
if sum(strcmp(fieldnames(master.diga.daten),'Schritt'))
    Schritt = master.diga.daten.Schritt;
else
    warning('Es wurden keine Werte f�r Schritt gefunden!');
    return;
end
if sum(strcmp(fieldnames(master.diga.daten),'Zyklus'))
    Zyklus = master.diga.daten.Zyklus;
else
    warning('Es wurden keine Werte f�r Zyklus gefunden!');
    return;
end
if sum(strcmp(fieldnames(master.diga.daten),'Zustand'))
    Zustand = master.diga.daten.Zustand;
    Zustand=regexprep(Zustand,'ELA','DCH');
    Zustand=regexprep(Zustand,'LAD','CHA');
    
else
    warning('Es wurden keine Werte f�r Zustand gefunden!');
    return;
end
if sum(strcmp(fieldnames(master.diga.daten),'Span'))
    Spannung = master.diga.daten.Span;
elseif sum(strcmp(fieldnames(master.diga.daten),'Spannung'))
    Spannung = master.diga.daten.Spannung;
else
    warning('Es wurden keine Werte f�r Spannung gefunden!');
    return;
end
if max(Spannung) > 1000, Spannung = Spannung/1000;end
AhAkku = [0, cumsum(Strom(1:end-1).*diff(Zeit))]/3600;%selbst
if max(AhAkku)>100
    AhAkku = [0, cumsum(Strom(1:end-1).*diff(Zeit/1000))]/3600;
    Zeit=Zeit/1000;
end
if ~sum(strcmp(fieldnames(master.diga.daten),'EISstart')) || isempty(find(diff(master.diga.daten.EISstart)==1, 1))
    warning('%s enth�lt keine EIS-Messungen, die �ber EISstart getriggert werden.',filename)
    return
end
EIS_indexes = find(diff(master.diga.daten.EISstart)==1);
if isempty(DRT_GUI)
    DRT_GUI.Testparameter.fileName=' ';
end
[k,l] = regexp(DRT_GUI.Testparameter.fileName,'EIS\d\d\d\d\d');
if ~isempty(k) && ~isempty(l)
    eisnumber = str2double(DRT_GUI.Testparameter.fileName(k+3:l));
else
    Prompt = ['Bitte w�hlen Sie eine Eismessung aus!'];
    for i = 1:numel(EIS_indexes);
        Prompt = [Prompt sprintf('\n%3d - nach %5.2f Ah Entladung und IDC = %5.2f',i,...
            max(AhAkku)-AhAkku(EIS_indexes(i)),mean(Strom(Schritt==Schritt(EIS_indexes(i)+1) & Zyklus==Zyklus(EIS_indexes(i)+1))))];
    end
    choice = cell2mat(inputdlg(Prompt,'Sprektrum ausw�hlen'));
    if isempty(choice) || str2num(choice) ==0 || str2num(choice) > numel(EIS_indexes), return,end
    eisnumber = str2num(choice);
end
EIS_index = EIS_indexes(eisnumber);
nach_EIS_index = EIS_index-2+find(strcmp(Zustand(EIS_index:end),'CHA')|strcmp(Zustand(EIS_index:end),'DCH'),1,'first');
if isempty(nach_EIS_index), nach_EIS_index = numel(master.diga.daten.EISstart); end
Vor_EIS_index = find(strcmp(Zustand(1:EIS_index),'CHA')|strcmp(Zustand(1:EIS_index),'DCH'),1,'last');
Start_Vor_EIS_index = find(strcmp(Zustand(1:Vor_EIS_index),'PAU')|strcmp(Zustand(1:Vor_EIS_index),'PAU'),1,'last')-5;
if (Zeit(EIS_index)-Zeit(Vor_EIS_index)) > 1e5
    Zeit = Zeit/1000;
end

relax_time = Zeit(Start_Vor_EIS_index:EIS_index)-Zeit(Start_Vor_EIS_index);
relax_spannung = Spannung(Start_Vor_EIS_index:EIS_index);
relax_strom = Strom(Start_Vor_EIS_index:EIS_index);

DRT_GUI.Messdaten.relax.zeit = relax_time;
DRT_GUI.Messdaten.relax.spannung = relax_spannung;
DRT_GUI.Messdaten.relax.strom = relax_strom;
eis_time = Zeit(EIS_index:nach_EIS_index)-Zeit(EIS_index);
eis_spannung = Spannung(EIS_index:nach_EIS_index);
eis_strom = Strom(EIS_index:nach_EIS_index);
DRT_GUI.Messdaten.eis.zeit = eis_time;
DRT_GUI.Messdaten.eis.spannung = eis_spannung;
DRT_GUI.Messdaten.eis.strom = eis_strom;
fcn_aktualisieren_Button_Callback(app,event)
end