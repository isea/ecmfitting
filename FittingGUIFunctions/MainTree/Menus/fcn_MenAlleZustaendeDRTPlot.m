function fcn_MenAlleZustaendeDRTPlot(app, event)
global DRT_GUI;
% figure;
% firstline = semilogx(1,1);
% new_ax = gca;
Zustaende = get(app.ZustandPopup ,'Items');
% delete(firstline);
Zustandaliases=[];
Batteriename = get(app.BatterieNamePopup,'Value');
% Batteriename = Batteriename{get(app.BatterieNamePopup,'value')};
if ~isempty(dir(['output/' Batteriename '/Zustandaliases.mat']))
    Zustandaliases = load(['output/' Batteriename '/Zustandaliases.mat']);
    Zustandaliases = Zustandaliases.Zustandaliases;
elseif ~isempty(dir(['output/' Batteriename '/Zustandaliases.xlsx']))
    [Zustandndata Zustandtext ZustandXLS] = xlsread(['output/' Batteriename '/Zustandaliases.xlsx']);
    for i = 2:size(ZustandXLS,2)
        Zustandaliases{1,i-1}.name=Zustandtext{1,i};
        Zustandaliases{1,i-1}.unit=Zustandtext{2,i};
        Zustandaliases{1,i-1}.value=cell2mat(ZustandXLS(3:end,i));
        Zustandaliases{1,i-1}.Zustands_name=ZustandXLS(3:end,1);
    end
elseif ~isempty(dir(['output/' Batteriename '/Zustandaliases.xls']))
    [Zustandndata Zustandtext ZustandXLS] = xlsread(['output/' Batteriename '/Zustandaliases.xlsx']);
    for i = 2:size(ZustandXLS,2)
        Zustandaliases{1,i-1}.name=Zustandtext{1,i};
        Zustandaliases{1,i-1}.unit=Zustandtext{2,i};
        Zustandaliases{1,i-1}.value=cell2mat(ZustandXLS(3:end,i));
        Zustandaliases{1,i-1}.Zustands_name=ZustandXLS(3:end,1);
    end
end
Zustandsurf.tau=[];
Zustandsurf.g=[];
Zustandsurf.p={};
Zustandsurf.aliases={};
for i_Z = 1:numel(Zustaende)
    
    if isempty(Zustaende{i_Z}) || ~isempty(strfind(Zustaende{i_Z},'gray'))
        continue
    end
    %     [a,b,c,legende] = legend;
    %     legendPosition = get(a,'Location');
    x_label =  get(get(app.axes3,'xlabel'),'string');
    y_label =  get(get(app.axes3,'ylabel'),'string');
    set(app.ZustandPopup,'Value',app.ZustandPopup.Items{i_Z})
    fcn_ZustandPopup_Callback(app,event)
% fcn_ZustandPopup_Callback(app.ZustandPopup,'kein_plot',handles)
    xlimits = app.axes3.XLim;
    ylimits = app.axes3.YLim;
    if ~isempty(Zustandaliases)
        ZAlias = nan(numel(Zustandaliases),1);
        for i_a = 1:numel(Zustandaliases)
            Zustandsurf.AliasName{i_a}=Zustandaliases{i_a}.name;
            Zustandsurf.AliasUnit{i_a}=Zustandaliases{i_a}.unit;
            Z_index = find(strcmp(DRT_GUI.Testparameter.Zustand,Zustandaliases{i_a}.Zustands_name));
            if ~isempty(Z_index)
                ZAlias(i_a) = Zustandaliases{i_a}.value(Z_index);
            end
            if numel(Zustandsurf.aliases)<i_a
                Zustandsurf.aliases{i_a}=[];
            end
           Zustandsurf.aliases{i_a} = [Zustandsurf.aliases{i_a}; repmat(ZAlias(i_a),numel(DRT_GUI.DRT.EI_DRT.tau(DRT_GUI.DRT.EI_DRT.aktiv)),1)];
        end
        Zustandsurf.tau = [Zustandsurf.tau ; DRT_GUI.DRT.EI_DRT.tau(DRT_GUI.DRT.EI_DRT.aktiv)];
        Zustandsurf.g = [Zustandsurf.g ; DRT_GUI.DRT.EI_DRT.g(DRT_GUI.DRT.EI_DRT.aktiv)'];
        for i_p = 1:numel(DRT_GUI.DRT.EI_DRT.ProzessFit.r)
            if numel(Zustandsurf.p)<i_p
                Zustandsurf.p{i_p}=[];
            end
            Zustandsurf.p{i_p} = [Zustandsurf.p{i_p} ; Calc_Zarc_DRT(DRT_GUI.DRT.EI_DRT.ProzessFit.r(i_p),DRT_GUI.DRT.EI_DRT.ProzessFit.tau(i_p),DRT_GUI.DRT.EI_DRT.ProzessFit.phi(i_p),DRT_GUI.DRT.EI_DRT.tau(DRT_GUI.DRT.EI_DRT.aktiv))];
        end
    end
    Zustandstring = DRT_GUI.Testparameter.Zustand;
    %     Lines = get(app.axes3,'Children');
    %     for i=1:numel(Lines)-2
    %         newlines = copyobj(Lines(i),new_ax);
    %         set(newlines(end),'DisplayName',[Zustandstring ' Prozess ' char('A'+(numel(Lines)-1-i)-1)]);
    %     end
    %     axes(new_ax)
    %     xlabel(x_label);ylabel(y_label);
    %     new_xlim = xlim;
    %     new_ylim = ylim;
    %     if new_xlim(1)< xlimits(1),xlimits(1) = new_xlim(1);end
    %     if new_xlim(2)> xlimits(2),xlimits(2) = new_xlim(2);end
    %
    %     if new_ylim(1)< ylimits(1),ylimits(1) = new_ylim(1);end
    %     if new_ylim(2)> ylimits(2),ylimits(2) = new_ylim(2);end
    %     if ylimits(1)<0,ylimits(1)=0;end
    %
    %xlim(xlimits)
    %ylim(ylimits)
end
% grid on;
% axes(app.axes3);
% legend(legende,'Location',legendPosition);
% axes(new_ax);
if isempty(dir('export'))
    mkdir('export')
end
if isempty( dir(['export' '/' DRT_GUI.Testparameter.Batterie]))
    mkdir(['export' '/' DRT_GUI.Testparameter.Batterie])
end
% achsen=app.axes3;
% achsen = achsen(isgraphics(achsen,'axes'));
% for j = 1:numel(achsen)
%     set(get(achsen(j),'xlabel'),'Fontname','arial','fontsize',15);
%     set(get(achsen(j),'ylabel'),'Fontname','arial','fontsize',15);
%     set(achsen(j),'Fontname','Arial','Fontsize',16)
%     linien = get(achsen(j),'Children');
%     linien = linien(isgraphics(linien,'line'));
%     for i = 1:numel(linien),
%        if isgraphics(linien(i),'line')
%            set(linien(i),'linewidth',2)
%        end
%     end
% end
% title('')
% print(gcf,['export' '/' FittingGUI.Testparameter.Batterie '/' FittingGUI.Testparameter.Batterie ' ' num2str(FittingGUI.Testparameter.Temperatur) '�C ' num2str(round(FittingGUI.Testparameter.SOC/5)*5) '% SOC DRTProzesse2D.png'], '-dpng', '-r900');
% title([FittingGUI.Testparameter.Batterie ' ' num2str(FittingGUI.Testparameter.Temperatur) '�C ' num2str(round(FittingGUI.Testparameter.SOC/5)*5) '% SOC'],'Interpreter','none')
% saveas(gcf,['export' '/' FittingGUI.Testparameter.Batterie '/' FittingGUI.Testparameter.Batterie ' ' num2str(FittingGUI.Testparameter.Temperatur) '�C ' num2str(round(FittingGUI.Testparameter.SOC/5)*5) '% SOC DRTProzesse2D.fig'])
for i = 1:numel(Zustandaliases)
    index = find(~isnan(Zustandsurf.aliases{i}));
    F_g = scatteredInterpolant(log10(Zustandsurf.tau(index)),Zustandsurf.aliases{i}(index),Zustandsurf.g(index));
    F_p={};
    [logtaugrid ,zgrid] = meshgrid(log10(min(Zustandsurf.tau(index))):0.1:log10(max(Zustandsurf.tau(index))),unique(Zustandsurf.aliases{i}(index)));
    gsurf = F_g(logtaugrid,zgrid);
    figure;
    surf(10.^logtaugrid,zgrid,gsurf,'LineStyle',':'); set(gca,'xscale','log')
    xlabel('Tau in s')
    if ~isempty(Zustandsurf.AliasUnit{i})
        ylabel([Zustandsurf.AliasName{i} ' in ' Zustandsurf.AliasUnit{i}])
    else
        ylabel(Zustandsurf.AliasName{i})
    end
    zlabel('DRT(Z)')
    set(gca,'xscale','log')
    hold on; grid on
    achsen = get(gcf,'Children');
    achsen = achsen(isgraphics(achsen,'axes'));
    for j = 1:numel(achsen)
        set(get(achsen(j),'xlabel'),'Fontname','arial','fontsize',15);
        set(get(achsen(j),'ylabel'),'Fontname','arial','fontsize',15);
        set(get(achsen(j),'zlabel'),'Fontname','arial','fontsize',15);
        set(achsen(j),'Fontname','Arial','Fontsize',16)
        linien = get(achsen(j),'Children');
        linien = linien(isgraphics(linien,'line'));
        for li = 1:numel(linien),
            if isgraphics(linien(li),'line')
                set(linien(li),'linewidth',2)
            end
        end
    end
    title('')
    print(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i}  ' DRTSurf.png'], '-dpng', '-r900');
    title([DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC'],'Interpreter','none')
    saveas(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i} ' DRTSurf.fig'])
    for p_i = 1:numel(Zustandsurf.p)
        F_p{p_i} = scatteredInterpolant(log10(Zustandsurf.tau(index)),Zustandsurf.aliases{i}(index),Zustandsurf.p{p_i}(index));
        psurf{p_i} = F_p{p_i}(logtaugrid,zgrid);
        figure;
        surf(10.^logtaugrid,zgrid,psurf{p_i},'LineStyle',':'); set(gca,'xscale','log')
        xlabel('Tau in s')
        if ~isempty(Zustandsurf.AliasUnit{i})
            ylabel([Zustandsurf.AliasName{i} ' in ' Zustandsurf.AliasUnit{i}])
        else
            ylabel(Zustandsurf.AliasName{i})
        end
        zlabel('DRT(Z)')
        set(gca,'xscale','log')
        hold on; grid on
        achsen = get(gcf,'Children');
        achsen = achsen(isgraphics(achsen,'axes'));
        for j = 1:numel(achsen)
            set(get(achsen(j),'xlabel'),'Fontname','arial','fontsize',15);
            set(get(achsen(j),'ylabel'),'Fontname','arial','fontsize',15);
            set(get(achsen(j),'zlabel'),'Fontname','arial','fontsize',15);
            set(achsen(j),'Fontname','Arial','Fontsize',16)
            linien = get(achsen(j),'Children');
            linien = linien(isgraphics(linien,'line'));
            for li = 1:numel(linien),
                if isgraphics(linien(li),'line')
                    set(linien(li),'linewidth',2)
                end
            end
        end
        title('')
        print(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i} ' P' num2str(p_i) 'Surf.png'], '-dpng', '-r900');
        title([DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC'],'Interpreter','none')
        saveas(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i} ' P' num2str(p_i) 'Surf.fig'])
    end
    z_values=unique(Zustandsurf.aliases{i}(index));
    cmap=hot(ceil(numel(z_values)*1.8));
    figure;
    set(gcf,'renderer','opengl');
    surf(logtaugrid,zgrid,gsurf,'LineStyle','none','FaceAlpha',0.4);
    hold on
    for z_index = 1:numel(z_values)
        indices = find(Zustandsurf.aliases{i}==z_values(z_index));
        plot3(log10(Zustandsurf.tau(indices)),repmat(z_values(z_index),size(Zustandsurf.tau(indices))),Zustandsurf.g(indices),'color',cmap(z_index,:),'Displayname',[num2str(z_values(z_index)) ' ' Zustandsurf.AliasUnit{i} ' ' Zustandsurf.AliasName{i}],'LineWidth',3)
        hold on ;grid on
        xlabel('log_{10}(Tau in s)')
        if ~isempty(Zustandsurf.AliasUnit{i})
            ylabel([Zustandsurf.AliasName{i} ' in ' Zustandsurf.AliasUnit{i}])
        else
            ylabel(Zustandsurf.AliasName{i})
        end
        zlabel('DRT(Z)')
    end
    achsen = get(gcf,'Children');
    achsen = achsen(isgraphics(achsen,'axes'));
    for j = 1:numel(achsen)
        set(get(achsen(j),'xlabel'),'Fontname','arial','fontsize',15);
        set(get(achsen(j),'ylabel'),'Fontname','arial','fontsize',15);
        set(get(achsen(j),'zlabel'),'Fontname','arial','fontsize',15);
        set(achsen(j),'Fontname','Arial','Fontsize',16)
        linien = get(achsen(j),'Children');
        linien = linien(isgraphics(linien,'line'));
        for li = 1:numel(linien),
            if isgraphics(linien(li),'line')
                set(linien(li),'linewidth',2)
            end
        end
    end
    title('')
    print(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i} ' DRTPlot3Transp.png'], '-dpng', '-r900');
    title([DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC'],'Interpreter','none')
    saveas(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i} ' DRTPlot3Transp.fig'])
    figure;
    for z_index = 1:numel(z_values)
        indices = find(Zustandsurf.aliases{i}==z_values(z_index));
        plot3(Zustandsurf.tau(indices),repmat(z_values(z_index),size(Zustandsurf.tau(indices))),Zustandsurf.g(indices),'color',cmap(z_index,:),'Displayname',[num2str(z_values(z_index)) ' ' Zustandsurf.AliasUnit{i} ' ' Zustandsurf.AliasName{i}],'LineWidth',3)
        hold on ; grid on
        xlabel('Tau in s')
        if ~isempty(Zustandsurf.AliasUnit{i})
            ylabel([Zustandsurf.AliasName{i} ' in ' Zustandsurf.AliasUnit{i}])
        else
            ylabel(Zustandsurf.AliasName{i})
        end
        zlabel('DRT(Z)')
        set(gca,'xscale','log')
    end
    achsen = get(gcf,'Children');
    achsen = achsen(isgraphics(achsen,'axes'));
    for j = 1:numel(achsen)
        set(get(achsen(j),'xlabel'),'Fontname','arial','fontsize',15);
        set(get(achsen(j),'ylabel'),'Fontname','arial','fontsize',15);
        set(get(achsen(j),'zlabel'),'Fontname','arial','fontsize',15);
        set(achsen(j),'Fontname','Arial','Fontsize',16)
        linien = get(achsen(j),'Children');
        linien = linien(isgraphics(linien,'line'));
        for li = 1:numel(linien),
            if isgraphics(linien(li),'line')
                set(linien(li),'linewidth',2)
            end
        end
    end
    title('')
    print(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i} ' DRTPlot3.png'], '-dpng', '-r900');
    title([DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC'],'Interpreter','none')
    saveas(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i} ' DRTPlot3.fig'])
    figure;
    for z_index = 1:numel(z_values)
        indices = find(Zustandsurf.aliases{i}==z_values(z_index));
        semilogx(Zustandsurf.tau(indices),Zustandsurf.g(indices),'color',cmap(z_index,:),'Displayname',[num2str(z_values(z_index)) ' ' Zustandsurf.AliasUnit{i} ' ' Zustandsurf.AliasName{i}],'LineWidth',3)
        hold on ; grid on
        xlabel('Tau in s')
        ylabel('DRT(Z)')
    end
    achsen = get(gcf,'Children');
    achsen = achsen(isgraphics(achsen,'axes'));
    for j = 1:numel(achsen)
        set(get(achsen(j),'xlabel'),'Fontname','arial','fontsize',15);
        set(get(achsen(j),'ylabel'),'Fontname','arial','fontsize',15);
        set(achsen(j),'Fontname','Arial','Fontsize',16)
        linien = get(achsen(j),'Children');
        linien = linien(isgraphics(linien,'line'));
        for li = 1:numel(linien),
            if isgraphics(linien(li),'line')
                set(linien(li),'linewidth',2)
            end
        end
    end
    lhandle=legend('show','Location','NorthWest');
    LEG = findobj(lhandle,'type','text');
    set(LEG,'FontSize',9)
    title('')
    print(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i} ' DRTPlot2D.png'], '-dpng', '-r900');
    title([DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC'],'Interpreter','none')
    saveas(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i} ' DRTPlot2D.fig'])
    for p_i = 1:numel(Zustandsurf.p)
        figure;
        for z_index = 1:numel(z_values)
            indices = find(Zustandsurf.aliases{i}==z_values(z_index));
            plot3(Zustandsurf.tau(indices),repmat(z_values(z_index),size(Zustandsurf.tau(indices))),Zustandsurf.p{p_i}(indices),'color',cmap(z_index,:),'Displayname',['P' num2str(p_i) ' ' num2str(z_values(z_index)) ' ' Zustandsurf.AliasUnit{i} ' ' Zustandsurf.AliasName{i}],'LineWidth',3)
            set(gca,'xscale','log')
            hold on; grid on
        end
        xlabel('Tau in s')
        if ~isempty(Zustandsurf.AliasUnit{i})
            ylabel([Zustandsurf.AliasName{i} ' in ' Zustandsurf.AliasUnit{i}])
        else
            ylabel(Zustandsurf.AliasName{i})
        end
        zlabel('DRT(Z)')
        achsen = get(gcf,'Children');
        achsen = achsen(isgraphics(achsen,'axes'));
        for j = 1:numel(achsen)
            set(get(achsen(j),'xlabel'),'Fontname','arial','fontsize',15);
            set(get(achsen(j),'ylabel'),'Fontname','arial','fontsize',15);
            set(get(achsen(j),'zlabel'),'Fontname','arial','fontsize',15);
            set(achsen(j),'Fontname','Arial','Fontsize',16)
            linien = get(achsen(j),'Children');
            linien = linien(isgraphics(linien,'line'));
            for li = 1:numel(linien),
                if isgraphics(linien(li),'line')
                    set(linien(li),'linewidth',2)
                end
            end
        end
        title('')
        print(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i} ' P' num2str(p_i) 'Plot3.png'], '-dpng', '-r900');
        title([DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC'],'Interpreter','none')
        saveas(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i} ' P' num2str(p_i) 'Plot3.fig'])
        figure;
        for z_index = 1:numel(z_values)
            indices = find(Zustandsurf.aliases{i}==z_values(z_index));
            semilogx(Zustandsurf.tau(indices),Zustandsurf.p{p_i}(indices),'color',cmap(z_index,:),'Displayname',['P' num2str(p_i) ' ' num2str(z_values(z_index)) ' ' Zustandsurf.AliasUnit{i} ' ' Zustandsurf.AliasName{i}],'LineWidth',3)
            hold on; grid on
        end
        xlabel('Tau in s')
        ylabel('DRT(Z)')
        achsen = get(gcf,'Children');
        achsen = achsen(isgraphics(achsen,'axes'));
        for j = 1:numel(achsen)
            set(get(achsen(j),'xlabel'),'Fontname','arial','fontsize',15);
            set(get(achsen(j),'ylabel'),'Fontname','arial','fontsize',15);
            set(achsen(j),'Fontname','Arial','Fontsize',16)
            linien = get(achsen(j),'Children');
            linien = linien(isgraphics(linien,'line'));
            for li = 1:numel(linien),
                if isgraphics(linien(li),'line')
                    set(linien(li),'linewidth',2)
                end
            end
        end
        lhandle=legend('show','Location','NorthWest');
        LEG = findobj(lhandle,'type','text');
        set(LEG,'FontSize',9)
        title('')
        print(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i} ' P' num2str(p_i) 'Plot2D.png'], '-dpng', '-r900');
        title([DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC'],'Interpreter','none')
        saveas(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i} ' P' num2str(p_i) 'Plot2D.fig'])
        figure;
        set(gcf,'renderer','opengl')
        surf(logtaugrid,zgrid,psurf{p_i},'LineStyle','none','FaceAlpha',0.4);
        hold on;
        for z_index = 1:numel(z_values)
            indices = find(Zustandsurf.aliases{i}==z_values(z_index));
            plot3(log10(Zustandsurf.tau(indices)),repmat(z_values(z_index),size(Zustandsurf.tau(indices))),Zustandsurf.p{p_i}(indices),'color',cmap(z_index,:),'Displayname',['P' num2str(p_i) ' ' num2str(z_values(z_index)) ' ' Zustandsurf.AliasUnit{i} ' ' Zustandsurf.AliasName{i}],'LineWidth',3)
            hold on; grid on
        end
        xlabel('log_{10}(Tau in s)')
        if ~isempty(Zustandsurf.AliasUnit{i})
            ylabel([Zustandsurf.AliasName{i} ' in ' Zustandsurf.AliasUnit{i}])
        else
            ylabel(Zustandsurf.AliasName{i})
        end
        zlabel('DRT(Z)')
        achsen = get(gcf,'Children');
        achsen = achsen(isgraphics(achsen,'axes'));
        for j = 1:numel(achsen)
            set(get(achsen(j),'xlabel'),'Fontname','arial','fontsize',15);
            set(get(achsen(j),'ylabel'),'Fontname','arial','fontsize',15);
            set(get(achsen(j),'zlabel'),'Fontname','arial','fontsize',15);
            set(achsen(j),'Fontname','Arial','Fontsize',16)
            linien = get(achsen(j),'Children');
            linien = linien(isgraphics(linien,'line'));
            for li = 1:numel(linien),
                if isgraphics(linien(li),'line')
                    set(linien(li),'linewidth',2)
                end
            end
        end
        title('')
        print(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i} ' P' num2str(p_i) 'Plot3Transp.png'], '-dpng', '-r900');
        title([DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC'],'Interpreter','none')
        saveas(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i} ' P' num2str(p_i) 'Plot3Transp.fig'])
    end
end
end