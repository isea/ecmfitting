function app = fcn_MenAllSOCsTDFFitMenuSelected(app,event)
%Do Standard TDF for the chosen SOCs

global DRT_GUI;
fcn_TDF_OpeningFcn(app, event);
fcn_CopyButton(app,event)
SOCs = get(app.SOCPopup ,'Items');
% Construct a questdlg with three options
choice = questdlg('Forward oder Backwards?', ...
    'Dessert Menu', ...
    'Forward','Backwards','No thank you','Forward');
for iZ=2:numel(SOCs)
    if strcmp(app.SOCPopup.Value,SOCs(iZ))
        Z_NR=iZ;
    end
end
switch choice
    case 'Forward'
        for i_SOC = Z_NR+1:numel(SOCs)
            if isempty(SOCs{i_SOC}) || ~isempty(strfind(SOCs{i_SOC},'gray'))
                continue
            end
            set(app.SOCPopup,'Value',app.SOCPopup.Items{i_SOC});
            fcn_SOCPopup(app,event);
            fcn_PasteButton(app,event);
            fcn_TDF_OCVFitButton(app,event);
            fcn_TDF_SpeichernButton(app,event);   
            fcn_CopyButton(app,event);
        end
    case 'Backwards'
        for i_SOC = Z_NR+-1:-1:2
            if isempty(SOCs{i_SOC}) || ~isempty(strfind(SOCs{i_SOC},'gray'))
                continue
            end
            set(app.SOCPopup,'Value',app.SOCPopup.Items{i_SOC})
            fcn_SOCPopup(app,event);
            fcn_PasteButton(app,event);
            fcn_TDF_OCVFitButton(app,event);
            fcn_TDF_SpeichernButton(app,event);            
            fcn_CopyButton(app,event);
        end
    case 'No thank you'
        return
end
end

