function app = fcn_MenAllSOCsPulsePlotMenuSelected(app, event)
%Plot the pulse data for all SOCs 
set(app.MainTabGroupRight,'SelectedTab', app.TDFTab)
fcn_TDF_OpeningFcn(app, event);
set(app.UsedTDDataButtonGroup_2,'SelectedObject', app.PulsesButton_2)
fcn_UsedTDDataButtonGroup_2SelectionChanged(app, event);
fcn_TDF_PlotOCVButton(app,event);
global DRT_GUI;
h = figure;
cmap=colormap(jet(100));
firstline = plot(1,1);
grid on;axis square; 
new_ax = gca;
SOCs = get(app.SOCPopup ,'Items');
set(app.SOCPopup,'Value',app.SOCPopup.Items{2})
fcn_SOCPopup(app,event);
min_SOC = DRT_GUI.Testparameter.SOC;
set(app.SOCPopup,'Value',app.SOCPopup.Items{numel(SOCs)})
fcn_SOCPopup(app, event);
max_SOC = DRT_GUI.Testparameter.SOC;
delete(firstline);
for i_Z = 1:numel(SOCs)
    if isempty(SOCs{i_Z}) || ~isempty(strfind(SOCs{i_Z},'gray')) || strcmp(SOCs{i_Z},' ')
        continue
    end
    x_label =  get(get(app.axes7,'xlabel'),'string');
    y_label =  get(get(app.axes7,'ylabel'),'string');
    set(app.SOCPopup,'Value',app.SOCPopup.Items{i_Z})
    fcn_SOCPopup(app,event)
    xlimits = app.axes7.XLim;
    ylimits = app.axes7.YLim;
    Zustandstring = DRT_GUI.Testparameter.Zustand;
    Lines = get(app.axes7,'Children');
    for i_Children = 1:length(Lines)
        if strcmp(app.axes7.Children(i_Children).DisplayName,'Measurement')
            idx = i_Children;
            continue
        end
    end
    newlines = copyobj(Lines(idx),new_ax);
    FarbenIndex=round((DRT_GUI.Testparameter.SOC-min_SOC)*99/(max_SOC-min_SOC))+1;
    counter = 0;
    while FarbenIndex == -Inf && counter < 100000
        counter = counter +1;
        FarbenIndex=round((DRT_GUI.Testparameter.SOC-min_SOC)*99/(max_SOC-min_SOC))+1;
    end
    %colorbar
    set(newlines(end),'DisplayName',[sprintf('%0.2f%% SOC %0.2f°C ',DRT_GUI.Testparameter.SOC,DRT_GUI.Testparameter.Temperatur) strrep(Zustandstring,'_','\_') ],'Color',cmap(FarbenIndex,:),'LineStyle','-','Marker','none');
    axes(new_ax)
    xlabel(x_label);ylabel(y_label);
    new_xlim = xlim;
    new_ylim = ylim;
    if new_xlim(1)< xlimits(1),xlimits(1) = new_xlim(1);end
    if new_xlim(2)> xlimits(2),xlimits(2) = new_xlim(2);end
    if new_ylim(1)< ylimits(1),ylimits(1) = new_ylim(1);end
    if new_ylim(2)> ylimits(2),ylimits(2) = new_ylim(2);end
    if ylimits(1)<0,ylimits(1)=0;end
    %xlim(xlimits)
    %ylim(ylimits)
end
grid on;
colorbar
caxis([min_SOC,max_SOC*1.001]);
new_ax.XLabel.Interpreter='latex'; new_ax.YLabel.Interpreter='latex'; new_ax.XLabel.String='t / s'; new_ax.YLabel.String='U / V'; %title(Plot_Title,'Interpreter','none');
%axes(app.axes7);
% legend(legende,'Location',legendPosition);
axes(new_ax);
end