function fcn_MenScript(app, event)
choice = questdlg('Dieses Script tauscht bei allen SOCs und allen Temperaturen die beiden Diffusionsäste (Geht nur bei LiIon4). Fortfahren?', ...
    'Script', ...
    'Abbrechen','Alle ändern','Abbrechen');
% Handle response
if isempty(choice), return,end
switch choice
    case 'Abbrechen'
        return
end
TList = get(app.TemperaturPopup,'Items');
for k = 2:numel(TList)
    set(app.TemperaturPopup,'Value',TList{k})
    fcn_TemperaturPopup_Callback(app,event)
    SOCList = get(app.SOCPopup,'Items');
    for i = 2:numel(SOCList)
        set(app.SOCPopup,'Value',SOCList{i})
        fcn_SOCPopup_Callback(app,event)
        ParTableAlt = get(app.ParamTable,'Data');
        ParTableNeu = ParTableAlt;
        ParTableNeu(15,2:end) = ParTableAlt(17,2:end);
        ParTableNeu(16,2:end) = ParTableAlt(18,2:end);
        ParTableNeu(17,2:end) = ParTableAlt(15,2:end);
        ParTableNeu(18,2:end) = ParTableAlt(16,2:end);
        set(app.ParamTable,'Data',ParTableNeu)
        fcn_FitButton_Callback(app,event)
        fcn_FitButton_Callback(app,event)
        fcn_SpeichernButton_Callback(app,event)
        %         a = inputdlg('Test');
        %         if isempty(a), return, end
    end
end
end