function fcn_MenRelaxFFTentfernen(app, event)
global DRT_GUI
if ismember('relax_fft',fieldnames(DRT_GUI.Messdaten))
    DRT_GUI.Messdaten = rmfield(DRT_GUI.Messdaten,'relax_fft');
    fcn_Alles_Laden(app,event)
    set(app.PunkteWegnehmenTextBox,'Value','')
    fcn_PunkteWegnehmenButton_Callback(app, event)
end
end