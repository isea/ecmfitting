function fcn_MenExportieren(app,event)
global DRT_GUI
Data = DRT_GUI;
Data2 = load(['output' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Zustand '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad/' ...
    DRT_GUI.Testparameter.Batterie '_' DRT_GUI.Testparameter.Zustand '_' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad_' [strrep(num2str(DRT_GUI.Testparameter.SOC),'-','m')] 'SOC_Modell.mat']);
Data.Messdaten = rmfield(Data.Messdaten,{'aktiv','low_Punkte_Weg'});
Data.Fit = rmfield(Data.Fit,{'Parameter_min','Parameter_max','gueltig','Grenzenautomatik','GrenzenBandbreite','ParFix','Limit_Reached','korrigiert'});
Data.Fit.aktuell_Modell = rmfield(Data.Fit.aktuell_Modell,{'ModellCell','Rechnen_Modell_komp','Rechnen_Modell_HF'});
Data.ReFit = Data2.Fit;
Data.ReFit = rmfield(Data.ReFit,{'Parameter_min','Parameter_max','gueltig'});

if ismember('Implementierung',fieldnames(Data.ReFit))
    Data.ReFit = rmfield(Data.ReFit,{'Implementierung'});
end

Data = rmfield(Data,{'Korrigiert','DRT'});
if isempty(dir('export'))
    mkdir('export')
end
if isempty( dir(['export' '/' DRT_GUI.Testparameter.Batterie]))
    mkdir(['export' '/' DRT_GUI.Testparameter.Batterie])
end
if isempty( dir(['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Zustand]))
    mkdir(['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Zustand])
end
if isempty( dir(['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Zustand '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad']))
    mkdir(['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Zustand '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad'])
end
save(['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Zustand '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad/' ...
    DRT_GUI.Testparameter.Batterie '_' DRT_GUI.Testparameter.Zustand '_' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad_' [strrep(num2str(DRT_GUI.Testparameter.SOC),'-','m')] 'SOC.mat'],'Data');
end