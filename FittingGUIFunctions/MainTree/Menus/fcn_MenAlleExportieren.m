function fcn_MenAlleExportieren(app, event)
choice = questdlg('Dieses Script exportiert den Fit bei allen SOCs und allen Temperaturen. Fortfahren?', ...
    'Script', ...
    'Abbrechen','Alle Exportieren','Abbrechen');
% Handle response
if isempty(choice), return,end
switch choice
    case 'Abbrechen'
        return
end
TList = get(app.TemperaturPopup,'Items');
for k = 2:numel(TList)
    set(app.TemperaturPopup,'Value',app.TemperaturPopup.Items{k})
    fcn_TemperaturPopup_Callback(app,event)
    SOCList = get(app.SOCPopup,'Items');
    for i = 2:numel(SOCList)
        set(app.SOCPopup,'Value',app.SOCPopup.Items{i})
        fcn_SOCPopup_Callback(app,event)
        fcn_MenExportieren_Callback(app,event)
    end
end
end