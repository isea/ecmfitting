function fcn_MenAlleZustaendeEISPlot(app, event)
global DRT_GUI;
% figure;
% firstline = plot(1,1);
% new_ax = gca;
Zustaende = get(app.ZustandPopup ,'Items');
% delete(firstline);
cmap = colormap(hot(ceil(numel(Zustaende)*1.8)));
Batteriename = get(app.BatterieNamePopup,'Value');
Zustandaliases=[];
%Batteriename = Batteriename{get(app.BatterieNamePopup,'value')};
if ~isempty(dir(['output/' Batteriename '/Zustandaliases.mat']))
    Zustandaliases = load(['output/' Batteriename '/Zustandaliases.mat']);
    Zustandaliases = Zustandaliases.Zustandaliases;
elseif ~isempty(dir(['output/' Batteriename '/Zustandaliases.xlsx']))
    [Zustandndata Zustandtext ZustandXLS] = xlsread(['output/' Batteriename '/Zustandaliases.xlsx']);
    for i = 2:size(ZustandXLS,2)
        Zustandaliases{1,i-1}.name=Zustandtext{1,i};
        Zustandaliases{1,i-1}.unit=Zustandtext{2,i};
        Zustandaliases{1,i-1}.value=cell2mat(ZustandXLS(3:end,i));
        Zustandaliases{1,i-1}.Zustands_name=ZustandXLS(3:end,1);
    end
elseif ~isempty(dir(['output/' Batteriename '/Zustandaliases.xls']))
    [Zustandndata Zustandtext ZustandXLS] = xlsread(['output/' Batteriename '/Zustandaliases.xlsx']);
    for i = 2:size(ZustandXLS,2)
        Zustandaliases{1,i-1}.name=Zustandtext{1,i};
        Zustandaliases{1,i-1}.unit=Zustandtext{2,i};
        Zustandaliases{1,i-1}.value=cell2mat(ZustandXLS(3:end,i));
        Zustandaliases{1,i-1}.Zustands_name=ZustandXLS(3:end,1);
    end
end
Zustandsurf.frequenz=[];
Zustandsurf.Zreal=[];
Zustandsurf.Zimg=[];
Zustandsurf.ZrealMF=[];
Zustandsurf.ZimgMF=[];
Zustandsurf.aliases={};
Zustandsurf.aliasesMF={};
for i_Z = 1:numel(Zustaende)
    if isempty(Zustaende{i_Z}) || ~isempty(strfind(Zustaende{i_Z},'gray'))
        continue
    end
    %     [a,b,c,legende] = legend;
    %     legendPosition = get(a,'Location');
    x_label =  get(get(app.axes1,'xlabel'),'string');
    y_label =  get(get(app.axes1,'ylabel'),'string');
    set(app.ZustandPopup,'Value',app.ZustandPopup.Items{i_Z})
    fcn_ZustandPopup_Callback(app,event)
%     ZustandPopup_Callback(app.ZustandPopup,'kein_plot',handles)
    xlimits = app.axes1.XLim;
    ylimits = app.axes1.YLim;
        if ~isempty(Zustandaliases)
        ZAlias = nan(numel(Zustandaliases),1);
        for i_a = 1:numel(Zustandaliases)
            Zustandsurf.AliasName{i_a}=Zustandaliases{i_a}.name;
            Zustandsurf.AliasUnit{i_a}=Zustandaliases{i_a}.unit;
            Z_index = find(strcmp(DRT_GUI.Testparameter.Zustand,Zustandaliases{i_a}.Zustands_name));
            if ~isempty(Z_index),
                ZAlias(i_a) = Zustandaliases{i_a}.value(Z_index);
            end
            if numel(Zustandsurf.aliases)<i_a
                Zustandsurf.aliases{i_a}=[];
            end
            Zustandsurf.aliases{i_a} = [Zustandsurf.aliases{i_a}; repmat(ZAlias(i_a),numel(DRT_GUI.Messdaten.frequenz),1)];
            if numel(Zustandsurf.aliasesMF)<i_a
                Zustandsurf.aliasesMF{i_a}=[];
            end
            Zustandsurf.aliasesMF{i_a} = [Zustandsurf.aliasesMF{i_a}; repmat(ZAlias(i_a),numel(DRT_GUI.Messdaten.frequenz(DRT_GUI.Messdaten.aktiv==1)),1)];
        end
        Zustandsurf.frequenz = [Zustandsurf.frequenz ; reshape(DRT_GUI.Messdaten.frequenz,numel(DRT_GUI.Messdaten.frequenz),1)];
        Zustandsurf.Zreal = [Zustandsurf.Zreal ; reshape(DRT_GUI.Messdaten.Zreal,numel(DRT_GUI.Messdaten.frequenz),1)];
        Zustandsurf.Zimg = [Zustandsurf.Zimg ; reshape(DRT_GUI.Messdaten.Zimg,numel(DRT_GUI.Messdaten.frequenz),1)];
        Zustandsurf.frequenzMF = [Zustandsurf.frequenz ; reshape(DRT_GUI.Messdaten.frequenz(DRT_GUI.Messdaten.aktiv==1),numel(DRT_GUI.Messdaten.frequenz(DRT_GUI.Messdaten.aktiv==1)),1)];
        Zustandsurf.ZrealMF = [Zustandsurf.ZrealMF ; reshape(DRT_GUI.Fit.korrigiert.Zreal,numel(DRT_GUI.Messdaten.frequenz(DRT_GUI.Messdaten.aktiv==1)),1)];
        Zustandsurf.ZimgMF = [Zustandsurf.ZimgMF ; reshape(DRT_GUI.Fit.korrigiert.Zimg,numel(DRT_GUI.Messdaten.frequenz(DRT_GUI.Messdaten.aktiv==1)),1)];
    end
    Zustandstring = DRT_GUI.Testparameter.Zustand;
    %     Lines = get(app.axes1,'Children');
    %         newlines = copyobj(Lines(1),new_ax);
    %     set(newlines(end),'DisplayName',[Zustandstring],'linestyle','-','marker','none','color',cmap(i_Z,:),'linewidth',2);
    %     axes(new_ax)
    %     set(gca,'Fontname','Arial','Fontsize',16)
    %     ylabel('Re\{Z\} in \Omega','Fontname','arial','fontsize',15)
    %     xlabel('Im\{Z\} in \Omega','Fontname','arial','fontsize',15)
    %     new_xlim = xlim;
    %     new_ylim = ylim;
    %     if new_xlim(1)< xlimits(1),xlimits(1) = new_xlim(1);end
    %     if new_xlim(2)> xlimits(2),xlimits(2) = new_xlim(2);end
    %     if new_ylim(1)< ylimits(1),ylimits(1) = new_ylim(1);end
    %     if new_ylim(2)> ylimits(2),ylimits(2) = new_ylim(2);end
    %     if ylimits(1)<0,ylimits(1)=0;end
    %xlim(xlimits)
    %ylim(ylimits)
end
%             set(gca,'Fontname','Arial','Fontsize',16)
%             ylabel('Re\{Z\} in \Omega','Fontname','arial','fontsize',15)
%             xlabel('Im\{Z\} in \Omega','Fontname','arial','fontsize',15)
%             set(gca,'ydir', 'reverse'); %axis square;
%             axis equal;
%             grid on;
%
% grid on;
% axes(app.axes3);
% % legend(legende,'Location',legendPosition);
% axes(new_ax);
if isempty(dir('export'))
    mkdir('export')
end
if isempty( dir(['export' '/' DRT_GUI.Testparameter.Batterie]))
    mkdir(['export' '/' DRT_GUI.Testparameter.Batterie])
end
%     achsen = get(gcf,'Children');
%     achsen = achsen(isgraphics(achsen,'axes'));
%     for j = 1:numel(achsen)
%         set(get(achsen(j),'xlabel'),'Fontname','arial','fontsize',15);
%         set(get(achsen(j),'ylabel'),'Fontname','arial','fontsize',15);
%         set(get(achsen(j),'zlabel'),'Fontname','arial','fontsize',15);
%         set(achsen(j),'Fontname','Arial','Fontsize',16)
%         linien = get(achsen(j),'Children');
%         linien = linien(isgraphics(linien,'line'));
%         for li = 1:numel(linien),
%            if isgraphics(linien(li),'line')
%                set(linien(li),'linewidth',2)
%            end
%         end
%     end
%     title('')
%     print(gcf,['export' '/' FittingGUI.Testparameter.Batterie '/' FittingGUI.Testparameter.Batterie ' ' num2str(FittingGUI.Testparameter.Temperatur) '�C ' num2str(round(FittingGUI.Testparameter.SOC/5)*5) '% SOC EISPlot2D.png'], '-dpng', '-r900');
%     title([FittingGUI.Testparameter.Batterie ' ' num2str(FittingGUI.Testparameter.Temperatur) '�C ' num2str(round(FittingGUI.Testparameter.SOC/5)*5) '% SOC'],'Interpreter','none')
%     saveas(gcf,['export' '/' FittingGUI.Testparameter.Batterie '/' FittingGUI.Testparameter.Batterie ' ' num2str(FittingGUI.Testparameter.Temperatur) '�C ' num2str(round(FittingGUI.Testparameter.SOC/5)*5) '% SOC EISPlot2D.fig'])
for i = 1:numel(Zustandaliases)
    index = find(~isnan(Zustandsurf.aliases{i}));
    z_values=unique(Zustandsurf.aliases{i}(index));
    cmap=hot(ceil(numel(z_values)*1.8));
    figure;
    for z_index = 1:numel(z_values)
        indices = find(Zustandsurf.aliases{i}==z_values(z_index));
        plot3(Zustandsurf.Zreal(indices),repmat(z_values(z_index),size(Zustandsurf.Zreal(indices))),Zustandsurf.Zimg(indices),'color',cmap(z_index,:),'Displayname',[num2str(z_values(z_index)) ' ' Zustandsurf.AliasUnit{i} ' ' Zustandsurf.AliasName{i}],'LineWidth',3)
        hold on
        xlabel('Zreal in \Omega')
        if ~isempty(Zustandsurf.AliasUnit{i})
            ylabel([Zustandsurf.AliasName{i} ' in ' Zustandsurf.AliasUnit{i}])
        else
            ylabel(Zustandsurf.AliasName{i})
        end
        zlabel('Zimg in \Omega')
    end
    set(gca,'Zdir','reverse')
    aspectratio = daspect;
    aspectratio([1 3]) = max(aspectratio([1 3]));
    daspect(gca,aspectratio);
    grid on
    achsen = get(gcf,'Children');
    achsen = achsen(isgraphics(achsen,'axes'));
    for j = 1:numel(achsen)
        set(get(achsen(j),'xlabel'),'Fontname','arial','fontsize',15);
        set(get(achsen(j),'ylabel'),'Fontname','arial','fontsize',15);
        set(get(achsen(j),'zlabel'),'Fontname','arial','fontsize',15);
        set(achsen(j),'Fontname','Arial','Fontsize',16)
        linien = get(achsen(j),'Children');
        linien = linien(isgraphics(linien,'line'));
        for li = 1:numel(linien),
            if isgraphics(linien(li),'line')
                set(linien(li),'linewidth',2)
            end
        end
    end
    title('')
    print(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i} ' EISPlot3.png'], '-dpng', '-r900');
    title([DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC'],'Interpreter','none')
    saveas(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i} ' EISPlot3.fig'])
    figure;
    for z_index = 1:numel(z_values)
        indices = find(Zustandsurf.aliases{i}==z_values(z_index));
        plot(Zustandsurf.Zreal(indices),Zustandsurf.Zimg(indices),'color',cmap(z_index,:),'Displayname',[num2str(z_values(z_index)) ' ' Zustandsurf.AliasUnit{i} ' ' Zustandsurf.AliasName{i}],'LineWidth',3)
        hold on
        xlabel('Zreal in \Omega')
        ylabel('Zimg in \Omega')
    end
    set(gca,'Ydir','reverse')
    aspectratio = daspect;
    aspectratio([1 2]) = max(aspectratio([1 2]));
    daspect(gca,aspectratio);
    grid on
    achsen = get(gcf,'Children');
    achsen = achsen(isgraphics(achsen,'axes'));
    for j = 1:numel(achsen)
        set(get(achsen(j),'xlabel'),'Fontname','arial','fontsize',15);
        set(get(achsen(j),'ylabel'),'Fontname','arial','fontsize',15);
        set(achsen(j),'Fontname','Arial','Fontsize',16)
        linien = get(achsen(j),'Children');
        linien = linien(isgraphics(linien,'line'));
        for li = 1:numel(linien),
            if isgraphics(linien(li),'line')
                set(linien(li),'linewidth',2)
            end
        end
    end
    lhandle=legend('show','Location','SouthEast');
    LEG = findobj(lhandle,'type','text');
    set(LEG,'FontSize',9)
    title('')
    print(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i} ' EISPlot2D.png'], '-dpng', '-r900');
    title([DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC'],'Interpreter','none')
    saveas(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i} ' EISPlot2D.fig'])
    figure;
    for z_index = 1:numel(z_values)
        indices = find(Zustandsurf.aliasesMF{i}==z_values(z_index));
        plot3(Zustandsurf.ZrealMF(indices),repmat(z_values(z_index),size(Zustandsurf.ZrealMF(indices))),Zustandsurf.ZimgMF(indices),'color',cmap(z_index,:),'Displayname',[num2str(z_values(z_index)) ' ' Zustandsurf.AliasUnit{i} ' ' Zustandsurf.AliasName{i}],'LineWidth',3)
        hold on
        xlabel('ZrealMF in \Omega')
        if ~isempty(Zustandsurf.AliasUnit{i})
            ylabel([Zustandsurf.AliasName{i} ' in ' Zustandsurf.AliasUnit{i}])
        else
            ylabel(Zustandsurf.AliasName{i})
        end
        zlabel('ZimgMF in \Omega')
    end
    set(gca,'Zdir','reverse')
    aspectratio = daspect;
    aspectratio([1 3]) = max(aspectratio([1 3]));
    daspect(gca,aspectratio);
    grid on
    achsen = get(gcf,'Children');
    achsen = achsen(isgraphics(achsen,'axes'));
    for j = 1:numel(achsen)
        set(get(achsen(j),'xlabel'),'Fontname','arial','fontsize',15);
        set(get(achsen(j),'ylabel'),'Fontname','arial','fontsize',15);
        set(get(achsen(j),'zlabel'),'Fontname','arial','fontsize',15);
        set(achsen(j),'Fontname','Arial','Fontsize',16)
        linien = get(achsen(j),'Children');
        linien = linien(isgraphics(linien,'line'));
        for li = 1:numel(linien),
            if isgraphics(linien(li),'line')
                set(linien(li),'linewidth',2)
            end
        end
    end
    title('')
    print(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i} ' EISPlot3MF.png'], '-dpng', '-r900');
    title([DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC'],'Interpreter','none')
    saveas(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i} ' EISPlot3MF.fig'])
    figure;
    for z_index = 1:numel(z_values)
        indices = find(Zustandsurf.aliasesMF{i}==z_values(z_index));
        plot(Zustandsurf.ZrealMF(indices),Zustandsurf.ZimgMF(indices),'color',cmap(z_index,:),'Displayname',[num2str(z_values(z_index)) ' ' Zustandsurf.AliasUnit{i} ' ' Zustandsurf.AliasName{i}],'LineWidth',3)
        hold on
        xlabel('ZrealMF in \Omega')
        ylabel('ZimgMF in \Omega')
    end
    set(gca,'Ydir','reverse')
    aspectratio = daspect;
    aspectratio([1 2]) = max(aspectratio([1 2]));
    daspect(gca,aspectratio);
    grid on
    achsen = get(gcf,'Children');
    achsen = achsen(isgraphics(achsen,'axes'));
    for j = 1:numel(achsen)
        set(get(achsen(j),'xlabel'),'Fontname','arial','fontsize',15);
        set(get(achsen(j),'ylabel'),'Fontname','arial','fontsize',15);
        set(achsen(j),'Fontname','Arial','Fontsize',16)
        linien = get(achsen(j),'Children');
        linien = linien(isgraphics(linien,'line'));
        for li = 1:numel(linien),
            if isgraphics(linien(li),'line')
                set(linien(li),'linewidth',2)
            end
        end
    end
    lhandle=legend('show','Location','BestOutside');
    LEG = findobj(lhandle,'type','text');
    set(LEG,'FontSize',9)
    title('')
    print(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i} ' EISPlot2DMF.png'], '-dpng', '-r900');
    title([DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC'],'Interpreter','none')
    saveas(gcf,['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie ' ' num2str(DRT_GUI.Testparameter.Temperatur) '�C ' num2str(round(DRT_GUI.Testparameter.SOC/5)*5) '% SOC ' Zustandsurf.AliasName{i} ' EISPlot2DMF.fig'])
end
end