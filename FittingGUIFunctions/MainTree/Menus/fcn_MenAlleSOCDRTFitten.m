function fcn_MenAlleSOCDRTFitten(app, event)
global DRT_GUI;
fcn_CopyButton_Callback(app,event)
SOCs = get(app.SOCPopup ,'Items');
% Construct a questdlg with three options
choice = questdlg('Vorw�rts oder R�ckw�rts?', ...
    'Dessert Menu', ...
    'Vorw�rts','R�ckw�rts','No thank you','Vorw�rts');
ContChoice = questdlg('Mit Continuous Process Fitting?', ...
    'Dessert Menu', ...
    'Cont. Proc.','No Cont. Proc.','Cont. Proc.');
HFchoice = questdlg('Jedes Mal HF-Fit vorher machen?', ...
    'Dessert Menu', ...
    'HF-Fit','No-HF-Fit','HF-Fit');
% Handle response
for iZ=2:numel(SOCs)
    if strcmp(app.SOCPopup.Value,SOCs(iZ))
        Z_NR=iZ;
    end
end
switch choice
    case 'Vorw�rts'
        for i_SOC = Z_NR+1:numel(SOCs)
            if isempty(SOCs{i_SOC}) || ~isempty(strfind(SOCs{i_SOC},'gray'))
                continue
            end
            set(app.SOCPopup,'Value',app.SOCPopup.Items{i_SOC})
            fcn_SOCPopup_Callback(app,event)
            fcn_PasteButton_Callback(app,event)
            set(app.cont_process_checkbox,'Value',0)
            fcn_cont_process_checkbox_Callback(app,event)
            fcn_DRTButton_Callback(app,event)
            fcn_Prozesse_fitten_button_Callback(app,event)
            if strcmp(HFchoice,'HF-Fit')
                fcn_InitHF_FittButton_Callback(app,event)
            else
                FitButton_Callback(app,event)
            end
            fcn_DRT_Prozesse_use_button_Callback(app,event)
            fcn_DRTButton_Callback(app,event)
            fcn_Prozesse_fitten_button_Callback(app,event)
            fcn_DRT_Prozesse_use_button_Callback(app,event)
            fcn_FitButton_Callback(app,event)
            if strcmp(ContChoice,'Cont. Proc.')
                set(app.cont_process_checkbox,'Value',1)
                fcn_cont_process_checkbox_Callback(app,event)
                fcn_DRTButton_Callback(app,event)
            else
                fcn_DRTButton_Callback(app,event)
                fcn_Prozesse_fitten_button_Callback(app,event)
                fcn_DRT_Prozesse_use_button_Callback(app,event)
                fcn_FitButton_Callback(app,event)
                set(app.GueltigeMessungCheck,'Value',1)
                fcn_GueltigeMessungCheck_Callback(app,event)
            end
            fcn_SpeichernButton_Callback(app,event)
            if get(app.GueltigeMessungCheck,'Value') || ~strcmp(ContChoice,'Cont. Proc.')
                fcn_CopyButton_Callback(app,event)
            end
        end
    case 'R�ckw�rts'
        for i_SOC = get(app.SOCPopup,'Value')+-1:-1:2
            if isempty(SOCs{i_SOC}) || ~isempty(strfind(SOCs{i_SOC},'gray'))
                continue
            end
            set(app.SOCPopup,'Value',i_SOC)
            fcn_SOCPopup_Callback(app,event)
            fcn_PasteButton_Callback(app,event)
            set(app.cont_process_checkbox,'Value',0)
            fcn_cont_process_checkbox_Callback(app,event)
            fcn_DRTButton_Callback(app,event)
            fcn_Prozesse_fitten_button_Callback(app,event)
            if strcmp(HFchoice,'HF-Fit')
                fcn_InitHF_FittButton_Callback(app,event)
            else
                fcn_FitButton_Callback(app,event)
            end
            fcn_DRT_Prozesse_use_button_Callback(app,event)
            if strcmp(ContChoice,'Cont. Proc.')
                set(app.cont_process_checkbox,'Value',1)
                fcn_cont_process_checkbox_Callback(app,event)
                fcn_DRTButton_Callback(app,event)
            else
                fcn_DRTButton_Callback(app,event)
                fcn_Prozesse_fitten_button_Callback(app,event)
                fcn_DRT_Prozesse_use_button_Callback(app,event)
                fcn_FitButton_Callback(app,event)
                set(app.GueltigeMessungCheck,'Value',1)
                fcn_GueltigeMessungCheck_Callback(app,event)
            end
            fcn_SpeichernButton_Callback(app,event)
            if get(app.GueltigeMessungCheck,'Value')|| ~strcmp(ContChoice,'Cont. Proc.')
                fcn_CopyButton_Callback(app,event)
            end
        end
    case 'No thank you'
        disp('I''ll bring you your check.')
        dessert = 0;
end
end