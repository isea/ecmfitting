function fcn_MenRelaxFFTbeiAllenEntfernen(app, event)
set(app.PunkteWegnehmenTextBox,'Value','')
choice = questdlg('Dieses Script entfernt die berechnete RelaxFFT bei allen SOCs und allen Temperaturen. Fortfahren?', ...
    'Script', ...
    'Abbrechen','Alle Speichern','Abbrechen');
% Handle response
if isempty(choice), return,end
switch choice
    case 'Abbrechen'
        return
end
TList = get(app.TemperaturPopup,'Items');
for k = 2:numel(TList)
    set(app.TemperaturPopup,'Value',app.TemperaturPopup.Items{k})
    fcn_TemperaturPopup_Callback(app,event)
    SOCList = get(app.SOCPopup,'Items');
    for i = 2:numel(SOCList)
        set(app.SOCPopup,'Value',app.SOCPopup.Items{i})
        fcn_SOCPopup_Callback(app,event)
        fcn_MenRelaxFFTentfernen_Callback(app, event)
        fcn_SpeichernButton_Callback(app,event)
    end
end
end