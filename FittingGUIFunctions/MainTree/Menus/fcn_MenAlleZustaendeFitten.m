function fcn_MenAlleZustaendeFitten(app, event)
global DRT_GUI;
choice = questdlg('Vorw�rts oder R�ckw�rts?', ...
    'Dessert Menu', ...
    'Vorw�rts','R�ckw�rts','No thank you','Vorw�rts');
if isempty(choice),return;end
HFchoice = questdlg('Jedes Mal HF-Fit vorher machen?', ...
    'Dessert Menu', ...
    'HF-Fit','No-HF-Fit','HF-Fit');
if isempty(HFchoice),return;end
ContChoice = questdlg('Mit Continuous Process Fitting?', ...
    'Dessert Menu', ...
    'Cont. Proc.','No Cont. Proc.','Cont. Proc.');
if isempty(ContChoice),return,end
fcn_CopyButton_Callback(app,event)
Zustaende = get(app.ZustandPopup ,'Items');
for iZ=2:numel(Zustaende)
    if strcmp(app.ZustandPopup.Value,Zustaende(iZ))
        Z_NR=iZ;
    end
end
switch choice
    case 'Vorw�rts'
        Zustandsliste = Z_NR+1:numel(Zustaende);
    case 'R�ckw�rts'
        Zustandsliste = Z_NR-1:-1:2;
end
for i_Z = Zustandsliste
   if isempty(Zustaende{i_Z}) || ~isempty(strfind(Zustaende{i_Z},'gray'))
        continue
    end
    set(app.ZustandPopup,'Value',app.ZustandPopup.Items{i_Z})
    fcn_ZustandPopup_Callback(app, event)
    fcn_PasteButton_Callback(app,event)
    % % % % % %     set(app.cont_process_checkbox,'value',0)
    % % % % % %     cont_process_checkbox_Callback(app.cont_process_checkbox,eventdata,handles)
    % % % % % %     if strcmp(HFchoice,'HF-Fit')
    % % % % % %         InitHF_FittButton_Callback(app.InitHF_FittButton,eventdata,handles)
    % % % % % %     else
    % % % % % %         FitButton_Callback(app.FitButton,eventdata,handles)
    % % % % % %     end
    % % % % % %     set(app.cont_process_checkbox,'value',1)
    % % % % % %     cont_process_checkbox_Callback(app.cont_process_checkbox,eventdata,handles)
    % % % % % %     DRTButton_Callback(app.DRTButton,eventdata,handles)
    % % % % % %     SpeichernButton_Callback(app.SpeichernButton,eventdata,handles)
    % % % % % %     if get(app.GueltigeMessungCheck,'value')
    % % % % % %         CopyButton_Callback(app.CopyButton,eventdata,handles)
    % % % % % %     end
    % % % % % %     set(app.cont_process_checkbox,'value',0)
    % % % % % %
    fcn_cont_process_checkbox_Callback(app,event)
    fcn_DRTButton_Callback(app,event)
    fcn_Prozesse_fitten_button_Callback(app,event)
    if strcmp(HFchoice,'HF-Fit')
        fcn_InitHF_FittButton_Callback(app,event)
    else
        fcn_FitButton_Callback(app,event)
    end
    fcn_DRT_Prozesse_use_button_Callback(app,event)
    fcn_DRTButton_Callback(app,event)
    fcn_Prozesse_fitten_button_Callback(app,event)
    fcn_DRT_Prozesse_use_button_Callback(app,event)
    fcn_FitButton_Callback(app,event)
    if strcmp(ContChoice,'Cont. Proc.')
        set(app.cont_process_checkbox,'Value',1)
        fcn_cont_process_checkbox_Callback(app,event)
        fcn_DRTButton_Callback(app,event)
    else
        fcn_DRTButton_Callback(app,event)
        fcn_Prozesse_fitten_button_Callback(app,event)
        fcn_DRT_Prozesse_use_button_Callback(app,event)
        fcn_FitButton_Callback(app,event)
        set(app.GueltigeMessungCheck,'Value',1)
        fcn_GueltigeMessungCheck_Callback(app,event)
    end
    fcn_SpeichernButton_Callback(app,event)
    if get(app.GueltigeMessungCheck,'Value') || ~strcmp(ContChoice,'Cont. Proc.')
        fcn_CopyButton_Callback(app,event)
    end
end
end