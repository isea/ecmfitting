function fcn_MenAlleSOCDRTPlot(app, event)
global DRT_GUI;
figure;
firstline = semilogx(1,1);
SOCs = get(app.SOCPopup ,'Items');
delete(firstline);
new_ax=gca;
for i_Z = 1:numel(SOCs)
    if isempty(SOCs{i_Z}) || ~isempty(strfind(SOCs{i_Z},'gray')) || strcmp(SOCs{i_Z},' ')
        continue
    end
    %axes(app.axes3);
    %     [a,b,c,legende] = legend;
    %     legendPosition = get(a,'Location');
    x_label =  get(get(app.axes3,'xlabel'),'string');
    y_label =  get(get(app.axes3,'ylabel'),'string');
    set(app.SOCPopup,'Value',app.SOCPopup.Items{i_Z})
    fcn_SOCPopup_Callback(app,event)
    xlimits = app.axes3.XLim;
    ylimits = app.axes3.YLim;
    Zustandstring = DRT_GUI.Testparameter.Zustand;
    Lines = get(app.axes3,'Children');
    for i=1:numel(Lines)-2
        newlines = copyobj(Lines(i),new_ax);
        set(newlines(end),'DisplayName',[sprintf('%0.2f%% SOC ',DRT_GUI.Testparameter.SOC) Zustandstring ' Prozess ' char('A'+(numel(Lines)-1-i)-1)]);
    end
    axes(new_ax)
    xlabel(x_label);ylabel(y_label);
    new_xlim = xlim;
    new_ylim = ylim;
    if new_xlim(1)< xlimits(1),xlimits(1) = new_xlim(1);end
    if new_xlim(2)> xlimits(2),xlimits(2) = new_xlim(2);end
    
    if new_ylim(1)< ylimits(1),ylimits(1) = new_ylim(1);end
    if new_ylim(2)> ylimits(2),ylimits(2) = new_ylim(2);end
    if ylimits(1)<0,ylimits(1)=0;end
    %xlim(xlimits)
    %ylim(ylimits)
    grid on;
end
% legend(legende,'Location',legendPosition);
end