function fcn_MenDRTExportieren(app, event)
global DRT_GUI
Data = DRT_GUI;
Data2 = load(['output' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Zustand '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad/' ...
    DRT_GUI.Testparameter.Batterie '_' DRT_GUI.Testparameter.Zustand '_' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad_' [strrep(num2str(DRT_GUI.Testparameter.SOC),'-','m')] 'SOC_Modell.mat']);
Data.Testparameter = rmfield(Data.Testparameter,{'fileName'});
Data.Messdaten = rmfield(Data.Messdaten,{'aktiv','low_Punkte_Weg'});
Data.DRT = Data.DRT.EI_DRT;
Data.DRT.peaks = rmfield(Data.DRT.peaks,{'used','used_parname'});
Data.Prozesse = rmfield(Data.DRT.ProzessFit,{'used','used_parname'});
Data.Prozesse.r = Data.Prozesse.r ./ Data.DRT.Rpol .* Data.DRT.Rpol_EIS;
Data.DRT = rmfield(Data.DRT,{'aktiv','Rpol','Rpol_EIS','ProzessFit','peaks'});
Data = rmfield(Data,{'Fit','Korrigiert'});
if isempty(dir('export'))
    mkdir('export')
end
if isempty( dir(['export' '/' DRT_GUI.Testparameter.Batterie]))
    mkdir(['export' '/' DRT_GUI.Testparameter.Batterie])
end
if isempty( dir(['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Zustand]))
    mkdir(['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Zustand])
end
if isempty( dir(['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Zustand '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad']))
    mkdir(['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Zustand '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad'])
end
save(['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Zustand '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad/' ...
    DRT_GUI.Testparameter.Batterie '_' DRT_GUI.Testparameter.Zustand '_' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad_' [strrep(num2str(DRT_GUI.Testparameter.SOC),'-','m')] 'SOC_DRT.mat'],'Data');
end