function fcn_menAlleOCVFittingsZuruecksetzen(app, event)
global DRT_GUI;
SOCs = get(app.SOCPopup ,'Items');
for i_SOC = 2:numel(SOCs)
    if isempty(SOCs{i_SOC}) || ~isempty(strfind(SOCs{i_SOC},'gray'))
        continue
    end
    set(app.SOCPopup,'Value',SOCs{i_SOC})
    fcn_SOCPopup(app,event);
    %%%%Dunno what to do here
    fcn_ZeitbereichsFittingButton(app,event);
    fcn_TDF_ResetOCVFitButton(app,event);
    fcn_SpeichernButton(app,event);
end
end