function app = fcn_MenAllTemperaturesPulsePlotMenuSelected(app, event)
%Plot the pulse data for all temperatures 
set(app.MainTabGroupRight,'SelectedTab', app.TDFTab)
fcn_TDF_OpeningFcn(app, event);
set(app.UsedTDDataButtonGroup_2,'SelectedObject', app.PulsesButton_2)
fcn_UsedTDDataButtonGroup_2SelectionChanged(app, event);
fcn_TDF_PlotOCVButton(app,event);
global DRT_GUI;
FarbenLaden
cmap=colormap(jet(100));
firstline = plot(1,1);
grid on;
new_ax = gca;
TempStrings = get(app.TemperaturPopup ,'Items');
Temps = cellfun(@str2num,strrep(strrep(TempStrings(2:end),'grad',''),'m','-'));
min_T = min(Temps);
max_T = max(Temps);
delete(firstline);
new_ax.XLabel.Interpreter='latex'; new_ax.YLabel.Interpreter='latex'; new_ax.XLabel.String='t / s'; new_ax.YLabel.String='U / V'; %title(Plot_Title,'Interpreter','none');
for i_Z = 2:(numel(Temps)+1)
    if isempty(TempStrings{i_Z}) || ~isempty(strfind(TempStrings{i_Z},'gray')) || strcmp(TempStrings{i_Z},' ')
        continue
    end
    x_label =  get(get(app.axes7,'xlabel'),'string');
    y_label =  get(get(app.axes7,'ylabel'),'string');
    set(app.TemperaturPopup,'Value',TempStrings{i_Z})
    fcn_TemperaturPopup(app,event);
    fcn_SOCPopup(app, event);
    xlimits = app.axes7.XLim;
    ylimits = app.axes7.YLim;
    Zustandstring = DRT_GUI.Testparameter.Zustand;
    Lines = get(app.axes7,'Children');
    for i_Children = 1:length(Lines)
        if strcmp(app.axes7.Children(i_Children).DisplayName,'Measurement')
            idx = i_Children;
            continue
        end
    end
    newlines = copyobj(Lines(idx),new_ax);
    FarbenIndex=round((DRT_GUI.Testparameter.Temperatur-min_T)*99/(max_T-min_T))+1;
    counter = 0;
    while FarbenIndex == -Inf && counter < 100000
        counter = counter +1;
        FarbenIndex=round((DRT_GUI.Testparameter.Temperatur-min_T)*99/(max_T-min_T))+1;
    end
    for i = 1:numel(newlines)
        set(newlines(i),'DisplayName',[get(newlines(i),'Displayname') ' ' sprintf('%0.2f%% SOC %0.2f°C ',DRT_GUI.Testparameter.SOC,DRT_GUI.Testparameter.Temperatur) Zustandstring ],'Color',cmap(FarbenIndex,:));
    end
    axes(new_ax)
    xlabel(x_label);ylabel(y_label);
    new_xlim = xlim;
    new_ylim = ylim;
    if new_xlim(1)< xlimits(1),xlimits(1) = new_xlim(1);end
    if new_xlim(2)> xlimits(2),xlimits(2) = new_xlim(2);end
    if new_ylim(1)< ylimits(1),ylimits(1) = new_ylim(1);end
    if new_ylim(2)> ylimits(2),ylimits(2) = new_ylim(2);end
    if ylimits(1)<0,ylimits(1)=0;end
end
grid on;
colorbar
caxis([min_T,max_T*1.001]);
axes(new_ax);
end

