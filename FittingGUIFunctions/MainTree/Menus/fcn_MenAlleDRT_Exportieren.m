function fcn_MenAlleDRT_Exportieren(app, event)
global DRT_GUI
choice = questdlg('Dieses Script exportiert die DRT-Berechnung alle SOCs und aller Temperaturen. Fortfahren?', ...
    'Script', ...
    'Abbrechen','Alle Exportieren','Abbrechen');
% Handle response
if isempty(choice), return,end
switch choice
    case 'Abbrechen'
        return
end
Zustaende = get(app.ZustandPopup ,'Items');
Testparameter = [];
Messdaten=[];
DRT=[];
Prozesse=[];
for i_Z = 1:numel(Zustaende)
    
    if isempty(Zustaende{i_Z})
        continue
    end
    set(app.ZustandPopup,'Value',app.ZustandPopup.Items{i_Z})
    
    fcn_ZustandPopup_Callback(app,event)
    TList = get(app.TemperaturPopup,'Items');
    for k = 2:numel(TList)
        set(app.TemperaturPopup,'Value',app.TemperaturPopup.Items{k})
        fcn_TemperaturPopup_Callback(app,event)
        SOCList = get(app.SOCPopup,'Items');
        for i = 2:numel(SOCList)
            set(app.SOCPopup,'Value',app.SOCPopup.Items{i})
            fcn_SOCPopup_Callback(app,event)
            pause(0.001)
            fcn_MenDRTExportieren_Callback(app,event)
            TempData = load(['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Zustand '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad/' ...
                DRT_GUI.Testparameter.Batterie '_' DRT_GUI.Testparameter.Zustand '_' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad_' [strrep(num2str(DRT_GUI.Testparameter.SOC),'-','m')] 'SOC_DRT.mat']);
            if isempty(Testparameter)
                Testparameter = TempData.Data.Testparameter;
                Messdaten = TempData.Data.Messdaten;
                DRT = TempData.Data.DRT;
                Prozesse = TempData.Data.Prozesse;
            else
                Testparameter(end+1) = Testparameter(end);
                felder = fieldnames(TempData.Data.Testparameter);
                for fi = 1:numel(felder)
                    Testparameter(end).(felder{fi}) = TempData.Data.Testparameter.(felder{fi});
                end
                
                Messdaten(end+1) = TempData.Data.Messdaten;
                DRT(end+1) = TempData.Data.DRT;
                Prozesse(end+1) = TempData.Data.Prozesse;
            end
        end
    end
    try
        SOC = cell2mat({Testparameter.SOC});
        [x xi] = sort( SOC);
        Testparameter = Testparameter(xi);
        Messdaten = Messdaten(xi);
        Prozesse = Prozesse(xi);
        DRT = DRT(xi);catch
    end
    
    save(['export' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie '_DRT.mat'],'Testparameter','Messdaten','DRT','Prozesse');
end
end