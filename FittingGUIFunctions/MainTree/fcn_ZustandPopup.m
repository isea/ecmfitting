function app = fcn_ZustandPopup(app, event)
global DRT_GUI
Batterien = app.BatterieNamePopup.Items;
Zustaende = fcn_killHTMLtags(app.ZustandPopup.Items);
Zustaende_2 = fcn_killHTMLtags(app.ZustandPopup_2.Items);
if app.CRateFlag 
    if strcmp(app.ZustandPopup.Value,app.ZustandPopup.Items{1}) && ~isempty(DRT_GUI) && sum(ismember(fieldnames(DRT_GUI),'Testparameter')) && ~isempty(DRT_GUI.Testparameter) && sum(ismember(fieldnames(DRT_GUI.Testparameter),'Zustand')) && ~isempty(DRT_GUI.Testparameter.Zustand)
        if ~isempty(find(ismember(Zustaende,DRT_GUI.Testparameter.Zustand)))
            app.ZustandPopup.Value=app.ZustandPopup.Items{find(ismember(Zustaende,DRT_GUI.Testparameter.Zustand))};
        end
    end
else
    if strcmp(app.ZustandPopup_2.Value,app.ZustandPopup_2.Items{1}) && ~isempty(DRT_GUI) && sum(ismember(fieldnames(DRT_GUI),'Testparameter')) && ~isempty(DRT_GUI.Testparameter) && sum(ismember(fieldnames(DRT_GUI.Testparameter),'Zustand')) && ~isempty(DRT_GUI.Testparameter.Zustand_2)
        if ~isempty(find(ismember(Zustaende_2,DRT_GUI.Testparameter.Zustand_2)))
            app.ZustandPopup_2.Value=app.ZustandPopup_2.Items{find(ismember(Zustaende_2,DRT_GUI.Testparameter.Zustand_2))};
        end
    end
end
Folders = {''};
if ~strcmp(app.ZustandPopup.Value,app.ZustandPopup.Items{1}) || ~strcmp(app.ZustandPopup_2.Value,app.ZustandPopup_2.Items{1})
    if app.CRateFlag && app.HysteresisFlag
        app.StateString = [app.ZustandPopup_2.Value '/' app.ZustandPopup.Value];
    elseif app.CRateFlag
        app.StateString = app.ZustandPopup.Value;
    else
        app.StateString = app.ZustandPopup_2.Value;
    end
    if app.GUI
        f = dir(['output/' app.BatterieNamePopup.Value '/'  app.StateString '/*']);
    else
        f = dir([app.OutputPath '/' app.BatterieNamePopup.Value '/'  app.StateString '/*']);
    end
    for i = 1:numel(f)
        if ~strcmp(f(i).name,'.') && ~strcmp(f(i).name,'..') && f(i).isdir
            Folders = [Folders;f(i).name];
        end
    end    
end
app.TemperaturPopup.Items=Folders;
app.TemperaturPopup.Value=app.TemperaturPopup.Items{1};
app.startingtemperatureDropDown.Items=Folders;
app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1};
app = fcn_TemperaturPopup(app,event);
end