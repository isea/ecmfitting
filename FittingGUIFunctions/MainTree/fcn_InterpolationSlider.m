function fcn_InterpolationSlider(app,event)
global DRT_Config
DRT_Config.InterpolationsFaktor = round(10^get(app.InterpolationSlider,'Value'));
set(app.InterpolationEdit,'Value',num2str(DRT_Config.InterpolationsFaktor))
fcn_DRTButton_Callback(app, event)
end