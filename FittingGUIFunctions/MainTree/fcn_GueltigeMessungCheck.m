function app = fcn_GueltigeMessungCheck(app, event)
global DRT_GUI
if isempty(DRT_GUI) || ~sum(ismember(fieldnames(DRT_GUI),'Messdaten'))
    if app.GUI
        set(app.GueltigeMessungCheck,'Value',0)
    else
        app.GueltigeMessungCheck.Value = 0;
    end
    return
end
if sum(ismember(fieldnames(DRT_GUI),'Fit'))    
    if app.GUI
        DRT_GUI.Fit.gueltig = get(app.GueltigeMessungCheck,'Value');
    else
        DRT_GUI.Fit.gueltig = app.GueltigeMessungCheck.Value;
    end
else
    if app.GUI
        set(app.GueltigeMessungCheck,'Value',0)
    else
        app.GueltigeMessungCheck.Value = 0;
    end
    return
end
end