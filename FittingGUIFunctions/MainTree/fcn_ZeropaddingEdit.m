function fcn_ZeropaddingEdit(app,event)
global DRT_Config
try
    Wert = str2num(get(app.ZeropaddingEdit,'Value'));
catch error_msg
    set(app.ZeropaddingEdit,'Value',num2str(DRT_Config.ZeroPadding))
    errordlg(error_msg)
end
if isempty(Wert)|| (Wert<10^app.ZeropaddingSlider.Limits(1) || Wert>10^app.ZeropaddingSlider.Limits(2))
    set(app.ZeropaddingEdit,'Value',num2str(DRT_Config.ZeroPadding))
    errordlg('Ungültiger Wert')
else
    DRT_Config.ZeroPadding = Wert;
    set(app.ZeropaddingSlider,'Value',log10(DRT_Config.ZeroPadding))
    fcn_DRTButton_Callback(app, event)
end
end