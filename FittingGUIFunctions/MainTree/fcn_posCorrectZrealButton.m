function fcn_posCorrectZrealButton(app,event)
global DRT_GUI

Zidx=DRT_GUI.Messdaten.Zimg>0;
EIS_ignore=1:length(Zidx);
EIS_ignore=EIS_ignore(Zidx);
EIS_ignore=EIS_ignore(1:end-1);
string = num2str(EIS_ignore);

app.PunkteWegnehmenTextBox.Value=string;
fcn_PunkteWegnehmeButton(app,event);
app.Korrigiert_Punkte_Weg_TextBox.Value=string;
fcn_Korrigiert_Punkte_Weg_Button(app,event);

end