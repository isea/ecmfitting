function fcn_FilterEdit(app, event)
global DRT_Config
try
    Wert = str2num(get(app.FilterEdit,'Value'));
catch error_msg
    set(app.FilterEdit,'Value',num2str(DRT_Config.FilterFaktor_ext))
    errordlg(error_msg)
end
if isempty(Wert)|| Wert<app.FilterFaktorSlider.Limits(1) || Wert>app.FilterFaktorSlider.Limits(2)
    set(app.FilterEdit,'Value',num2str(DRT_Config.FilterFaktor_ext))
    errordlg('Ungültiger Wert')
else
    DRT_Config.FilterFaktor_ext = Wert;
    set(app.FilterFaktorSlider,'Value',DRT_Config.FilterFaktor_ext)
    fcn_DRTButton_Callback(app, event)
end
end