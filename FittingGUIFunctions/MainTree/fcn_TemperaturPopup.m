function app = fcn_TemperaturPopup(app, event)
global DRT_GUI
global alleTemperaturen

Batterien = app.BatterieNamePopup.Items;
Zustaende = fcn_killHTMLtags(app.ZustandPopup.Items);
Temperaturen = app.TemperaturPopup.Items;
alleTemperaturen = app.TemperaturPopup.Items; %Saves Temperatures ain global variable
if strcmp(app.TemperaturPopup.Value,app.TemperaturPopup.Items{1}) && ~isempty(DRT_GUI) && sum(ismember(fieldnames(DRT_GUI),'Testparameter')) && ~isempty(DRT_GUI.Testparameter) && sum(ismember(fieldnames(DRT_GUI.Testparameter),'Temperatur')) && ~isempty(DRT_GUI.Testparameter.Temperatur)
    if ~isempty(find(ismember(Temperaturen,[strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad'])))
        app.TemperaturPopup.Value=app.TemperaturPopup.Items{ismember(Temperaturen,[strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad'])};
        app.startingtemperatureDropDown.Value=app.TemperaturPopup.Value;
    end
end
Folders = {''};
if ~strcmp(app.TemperaturPopup.Value,app.TemperaturPopup.Items{1})
    if app.GUI
        f = dir(['output/' app.BatterieNamePopup.Value '/'  app.StateString '/'  app.TemperaturPopup.Value '/*SOC.mat']);
    else
        f = dir([app.OutputPath '/' app.BatterieNamePopup.Value '/'  app.StateString '/'  app.TemperaturPopup.Value '/*SOC.mat']);
    end    
    for i = 1:numel(f)
        if ~strcmp(f(i).name,'.') && ~strcmp(f(i).name,'..') && ~f(i).isdir
            Folders = [Folders; strrep(strrep(strrep(strrep(strrep(strrep(f(i).name,app.BatterieNamePopup.Value,''),app.ZustandPopup.Value,''),app.ZustandPopup_2.Value,''),app.TemperaturPopup.Value,''),'_',''),'SOC.mat','')];
        end
    end   
end
%Sort temperatures in ascending order
matdata_vorzeichen_string = regexprep(Folders,'m','-');
matdata_vorzeichen_double = cellfun(@str2num,matdata_vorzeichen_string, 'UniformOutput',0);
double_matrix = cell2mat(matdata_vorzeichen_double);
double_matrix = sort(double_matrix);
double_cell = num2cell(double_matrix);
matdata_sortiert_string = cellfun(@num2str,double_cell, 'UniformOutput',0);
cellarray_sorted = [' '; matdata_sortiert_string];
cellarray_sorted = regexprep(cellarray_sorted,'-','m');

app.SOCPopup.Items=cellarray_sorted;
app.SOCPopup.Value=app.SOCPopup.Items{1};
app.startingSOCDropDown.Items=cellarray_sorted;
app.startingSOCDropDown.Value=app.SOCPopup.Items{1};
end