function app = fcn_Prozess_Taus_in_Tabelle(app)
global DRT_GUI;
global DRT_Config;
if ~sum(ismember(fieldnames(DRT_GUI),'DRT')) || isempty(DRT_GUI.DRT) || ...
        ~sum(ismember(fieldnames(DRT_GUI.DRT),'EI_DRT')) || ...
        isempty(DRT_GUI.DRT.EI_DRT)|| ...
        ~sum(ismember(fieldnames(DRT_GUI.DRT.EI_DRT),'ProzessFit')) ...
        || isempty(DRT_GUI.DRT.EI_DRT.ProzessFit)  ...
        || ( numel(DRT_GUI.DRT.EI_DRT.ProzessFit.tau)==0 && ( ...
        ~sum(ismember(fieldnames(DRT_GUI.DRT.EI_DRT.ProzessFit),'val')) ...
        || numel(DRT_GUI.DRT.EI_DRT.ProzessFit.val)==0 )...
        )
    %msgbox(['Bitte zuerst eine DRT erstellen'])
    if sum(ismember(fieldnames(app),'ProzesseTable'))
        %set(app.ProzesseTable,'Visible','off')
    end
    %set(app.ProzesseCaption,'Visible','off')
    return
end
if app.GUI
    set(app.ProzesseTable,'Visible','on')
else
    app.ProzesseTable.Visible = 'on';
end
if sum(ismember(fieldnames(DRT_GUI.DRT.EI_DRT.ProzessFit),'val')) && numel(DRT_GUI.DRT.EI_DRT.ProzessFit.val)>0
    if app.GUI
        ColFormat = get(app.ProzesseTable,'ColumnFormat');
    else
        ColFormat = app.ProzesseTable.ColumnFormat;
    end
    ColFormat{3} = DRT_GUI.DRT.EI_DRT.ProzessFit.used_parname';
    if app.GUI
        set(app.ProzesseTable,'ColumnFormat',ColFormat);
        ColNames = get(app.ProzesseTable,'ColumnName');
    else
        app.ProzesseTable.ColumnFormat = ColFormat;
        ColNames = app.ProzesseTable.ColumnName;
    end
    ColNames{1} = 'Val';
    ColNames{4} = '';
    ColNames{5} = '';
    if app.GUI
        set(app.ProzesseTable,'ColumnName',ColNames);
        TableCell = [num2cell(DRT_GUI.DRT.EI_DRT.ProzessFit.val) num2cell(logical(DRT_GUI.DRT.EI_DRT.ProzessFit.used)) DRT_GUI.DRT.EI_DRT.ProzessFit.used_parname  num2cell(zeros(numel(DRT_GUI.DRT.EI_DRT.ProzessFit.val),2))];
        set(app.ProzesseTable,'Data',TableCell)
    else
        app.ProzesseTable.ColumnName = ColNames;
        TableCell = [num2cell(DRT_GUI.DRT.EI_DRT.ProzessFit.val) num2cell(logical(DRT_GUI.DRT.EI_DRT.ProzessFit.used)) DRT_GUI.DRT.EI_DRT.ProzessFit.used_parname  num2cell(zeros(numel(DRT_GUI.DRT.EI_DRT.ProzessFit.val),2))];
        app.ProzesseTable.Data = TableCell;
    end    
else
    if app.GUI
        ColFormat = get(app.ProzesseTable,'ColumnFormat');
    else
        ColFormat = app.ProzesseTable.ColumnFormat;
    end
    Parameter_Taus = textscan(strrep(DRT_GUI.Fit.aktuell_Modell.ModellCell{1,3},' ',''),'%s','delimiter',',');
    Parameter_Taus = Parameter_Taus{1,1}(~cellfun(@isempty,strfind(Parameter_Taus{1,1}','Tau')));
    if isempty(Parameter_Taus) , Parameter_Taus = {' '};end
    ColFormat{3} = Parameter_Taus';
    if app.GUI
        set(app.ProzesseTable,'ColumnFormat',ColFormat);
        ColNames = get(app.ProzesseTable,'ColumnName');
    else
        app.ProzesseTable.ColumnFormat = ColFormat;
        ColNames = app.ProzesseTable.ColumnName;
    end        
    ColNames{1} = 'Tau';
    ColNames{4} = 'R';
    ColNames{5} = 'Phi';
    if app.GUI
        set(app.ProzesseTable,'ColumnName',ColNames);
        TauCell = get(app.ProzesseTable,'Data');
    else
        app.ProzesseTable.ColumnName= ColNames;
        TauCell = app.ProzesseTable.Data;
    end
    if size(TauCell,1)>numel(DRT_GUI.DRT.EI_DRT.ProzessFit.tau)
        TauCell = TauCell(1:numel(DRT_GUI.DRT.EI_DRT.ProzessFit.tau),:);
    end
    if ~isempty(TauCell)
        TauCell = cell2struct(TauCell',{'Tau','Used','Parameter','R','Phi'});
    end
    for i = 1:numel(DRT_GUI.DRT.EI_DRT.ProzessFit.tau)
        TauCell(i,1).Tau = DRT_GUI.DRT.EI_DRT.ProzessFit.tau(i);
        if sum(ismember(fieldnames(DRT_GUI.DRT.EI_DRT),'Rpol_EIS'))
            TauCell(i,1).R = DRT_GUI.DRT.EI_DRT.ProzessFit.r(i) .* DRT_GUI.DRT.EI_DRT.Rpol_EIS ./ DRT_GUI.DRT.EI_DRT.Rpol;
        else
            display('bitte DRT und Prozessfitting aktualisieren');
            TauCell(i,1).R = DRT_GUI.DRT.EI_DRT.ProzessFit.r(i) ;
        end
        TauCell(i,1).Phi = DRT_GUI.DRT.EI_DRT.ProzessFit.phi(i);
        if sum(ismember(fieldnames(DRT_GUI.DRT.EI_DRT.ProzessFit),'used')) && ...
                sum(ismember(fieldnames(DRT_GUI.DRT.EI_DRT.ProzessFit),'used_parname')) && ...
                DRT_GUI.DRT.EI_DRT.ProzessFit.used(i)
            TauCell(i,1).Used = true;
            TauCell(i,1).Parameter = DRT_GUI.DRT.EI_DRT.ProzessFit.used_parname{i};
        else
            TauCell(i,1).Used = false;
            TauCell(i,1).Parameter = '';
        end
    end
    TauCell = struct2cell(TauCell)';
    if app.GUI
        set(app.ProzesseTable,'Data',TauCell)
    else
        app.ProzesseTable.Data = TauCell;
    end
end
end