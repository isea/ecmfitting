function fcn_ProzesseTable_CellEdit(app, event)
global DRT_GUI
TauCell = get(app.ProzesseTable,'Data');
TauCell = cell2struct(TauCell',{'Tau','Used','Parameter','R','Phi'});
Parameter_Taus = textscan(strrep(DRT_GUI.Fit.aktuell_Modell.ModellCell{1,3},' ',''),'%s','delimiter',',');
Parameter_Taus = Parameter_Taus{1,1}(~cellfun(@isempty,strfind(Parameter_Taus{1,1}','Tau')));
if event.Indices(2) == 2 % used checkbox
    if event.NewData
        used_nr = 0;
        for i = 1:event.Indices(1)-1
            index = find( strcmp(TauCell(i).Parameter,Parameter_Taus),1,'first');
            if index > used_nr , used_nr = index; end
        end
        if used_nr == numel( Parameter_Taus)
            TauCell(event.Indices(1)).Parameter = '';
            TauCell(event.Indices(1)).Used = false;
        else
            TauCell(event.Indices(1)).Parameter = Parameter_Taus{used_nr+1};
        end
    else
        TauCell(event.Indices(1)).Parameter = '';
    end
end
if event.Indices(2) == 2 || event.Indices(2) == 3 % used checkbox or Parameter Dropdown-Box
    if TauCell(event.Indices(1)).Used
        used_nr = 0;
        
        for i = 1:numel(TauCell)
            index = find( strcmp(TauCell(i).Parameter,Parameter_Taus),1,'first');
            if ~isempty(index) && index <= used_nr
                if used_nr == numel( Parameter_Taus)
                    TauCell(i).Parameter = '';
                    TauCell(i).Used = false;
                else
                    used_nr = used_nr + 1;
                    TauCell(i).Parameter = Parameter_Taus{used_nr};
                end
            elseif ~isempty(index) && index > used_nr
                used_nr = index;
            end
        end
    else
        TauCell(event.Indices(1)).Parameter = '';
    end
end
TauCell = struct2cell(TauCell)';
set(app.ProzesseTable,'Data',TauCell)
DRT_GUI.DRT.EI_DRT.ProzessFit.used = cell2mat(TauCell(:,2));
DRT_GUI.DRT.EI_DRT.ProzessFit.used_parname = TauCell(:,3);
hold(app.axes3,'off');
Typ = 'DRT';
fcn_plot_Auswaehl(app,DRT_GUI.Korrigiert,DRT_GUI.DRT.E_DRT,DRT_GUI.DRT.EI_DRT,[],Typ)
end