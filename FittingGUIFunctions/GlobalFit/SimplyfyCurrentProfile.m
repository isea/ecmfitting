function diga = SimplyfyCurrentProfile(digab, index)
time = digab.daten.Programmdauer(index);
current = digab.daten.Strom(index);
span = digab.daten.Spannung(index);

% diga.real.Programmdauer = (time-time(1))/1000;
diga.real.Programmdauer = (time-time(1));
diga.real.Strom = current;
diga.real.Spannung = span;
% diga.real.Temperatur= digab.daten.Temperatur(index);
[C,ia,ic] = unique ( time );
time = C;
current = current(ia);
span = span(ia);
% time = (time -time(1))/ 1000;
time = (time -time(1));
index = find(abs(diff (current)) > 0.0001);
index = unique([index, index+1]);
diga.daten.Programmdauer = time(index);
diga.daten.Strom = current(index);
diga.daten.Strom( find( abs(diga.daten.Strom) < 0.0001)) = 0;
diga.daten.Spannung = span(index);
% figure; 
% plot(diga.daten.Programmdauer, diga.daten.Strom)
% hold all;
% plot(time -time(1), current)
