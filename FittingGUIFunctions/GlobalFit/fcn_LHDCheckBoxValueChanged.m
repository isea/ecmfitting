function app = fcn_LHDCheckBoxValueChanged(app, event); 

if app.LHDCheckBox.Value
    app.GF_FittetParametersButtonGroup.Enable = 'on';
    app.GF_ECEindicesEditFieldLabel.Enable = 'on';
    app.GF_ECEindicesEditField.Enable = 'on';
else
    app.GF_FittetParametersButtonGroup.Enable = 'off';
    app.GF_ECEindicesEditFieldLabel.Enable = 'off';
    app.GF_ECEindicesEditField.Enable = 'off';
end

end

