function app = fcn_GlobalFit_TDF(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
    sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array )
global DRT_GUI
%hyst down
for hyst = app.current_hyst_index:-1:1
    if app.HysteresisFlag
        index=hysteresis_state_index_rearrangement_array(hyst);
        app.ZustandPopup_2.Value=app.ZustandPopup_2.Items{1+index};
        app = fcn_ZustandPopup(app, event);
        app.current_hyst_index = hyst;
    end
    app.current_crate_index = Crate_index(hyst);
    %hyst down, crate down
    for crate = app.current_crate_index:-1:1
        if ~(~app.CRateFlag && sorted_crate_array{hyst}(crate) == 0)
            if app.CRateFlag
                index=crate_index_rearrangement_array{hyst}(crate);
                app.ZustandPopup.Value=app.ZustandPopup.Items{1+index};
                app = fcn_ZustandPopup(app, event);
                app.current_crate_index = crate;
            end
            app.current_temp_index = temp_index{hyst}(crate);
            %hyst down, crate down, temp down
            for temp=app.current_temp_index:-1:1
                %Lade richtige Daten bei Wechsel der Temperatur
                index1=temperature_index_rearrangement_array{hyst}{crate}(temp);
                if~strcmp(app.startingtemperatureDropDown.Value,app.startingtemperatureDropDown.Items{1+index1})
                    app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1+index1};
                    app.TemperaturPopup.Value=app.startingtemperatureDropDown.Value;
                    app = fcn_TemperaturPopup(app, event);
                end
                app.current_temp_index = temp;
                app.current_SOC_index= SOC_index{hyst}{crate}(temp);
                %hyst down, crate down, temp down, SOC down
                for SOC=app.current_SOC_index:-1:1
                    %load SOC
                    index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                    if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                        app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                        app.SOCPopup.Value=app.startingSOCDropDown.Value;
                        app = fcn_SOCPopup(app, event);
                        if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                            app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                            app = fcn_ModellAuswahlPopup(app);
                            app = fcn_BatterieNamePopup(app);
                        end
                    end
                    app.current_SOC_index=SOC;
                    if app.start_tupel_optimized_tdf
                        app = fcn_GlobalTDFFit(app,event,hyst,crate,temp,SOC,sorted_temperature_array,sorted_SOC_array,sorted_hysteresis_state_array,sorted_crate_array);
                    else
                        for initial_tdf_optimization_idx = 1:5
                            app = fcn_GlobalTDFFit(app,event,hyst,crate,temp,SOC,sorted_temperature_array,sorted_SOC_array,sorted_hysteresis_state_array,sorted_crate_array);
                        end
                    end
                    app.start_tupel_optimized_tdf = 1;
                    app.TDF_Optimization_done(hyst,crate,temp,SOC)=1;
                    fcn_CancelCheck(app);
                end
                app.current_SOC_index=SOC_index{hyst}{crate}(temp);
                %hyst down, crate down, temp down, SOC up
                for SOC=app.current_SOC_index+1:length(sorted_SOC_array{hyst}{crate}{temp})
                    %Lade richtige Daten bei Wechsel des SOC
                    index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                    if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                        app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                        app.SOCPopup.Value=app.startingSOCDropDown.Value;
                        app = fcn_SOCPopup(app, event);
                        if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                            app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                            app = fcn_ModellAuswahlPopup(app);
                            app = fcn_BatterieNamePopup(app);
                        end
                    end
                    app.current_SOC_index = SOC;
                    app = fcn_GlobalTDFFit(app,event,hyst,crate,temp,SOC,sorted_temperature_array,sorted_SOC_array,sorted_hysteresis_state_array,sorted_crate_array);
                    app.TDF_Optimization_done(hyst,crate,temp,SOC)=1;
                    fcn_CancelCheck(app);
                end
            end
            
            app.current_temp_index = temp_index{hyst}(crate);
            %hyst down, crate down, temp up
            for temp= app.current_temp_index+1:length(sorted_temperature_array{hyst}{crate})
                %Lade richtige Daten bei Wechsel der Temperatur
                index1=temperature_index_rearrangement_array{hyst}{crate}(temp);
                if~strcmp(app.startingtemperatureDropDown.Value,app.startingtemperatureDropDown.Items{1+index1})
                    app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1+index1};
                    app.TemperaturPopup.Value=app.startingtemperatureDropDown.Value;
                    app = fcn_TemperaturPopup(app, event);
                end
                app.current_temp_index=temp;
                app.current_SOC_index=SOC_index{hyst}{crate}(temp);
                %hyst down, crate down, temp down, SOC down
                for SOC=app.current_SOC_index:-1:1
                    %Lade richtige Daten bei Wechsel des SOC
                    index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                    if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                        app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                        app.SOCPopup.Value=app.startingSOCDropDown.Value;
                        app = fcn_SOCPopup(app, event);
                        if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                            app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                            app = fcn_ModellAuswahlPopup(app);
                            app = fcn_BatterieNamePopup(app);
                        end
                    end
                    app.current_SOC_index=SOC;
                    app = fcn_GlobalTDFFit(app,event,hyst,crate,temp,SOC,sorted_temperature_array,sorted_SOC_array,sorted_hysteresis_state_array,sorted_crate_array);
                    app.TDF_Optimization_done(hyst,crate,temp,SOC)=1;
                    fcn_CancelCheck(app);
                end
                app.current_SOC_index=SOC_index{hyst}{crate}(temp);
                %hyst down, crate down, temp down, SOC up
                for SOC=app.current_SOC_index+1:length(sorted_SOC_array{hyst}{crate}{temp})
                    %Lade richtige Daten bei Wechsel des SOC
                    index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                    if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                        app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                        app.SOCPopup.Value=app.startingSOCDropDown.Value;
                        app = fcn_SOCPopup(app, event);
                        if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                            app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                            app = fcn_ModellAuswahlPopup(app);
                            app = fcn_BatterieNamePopup(app);
                        end
                    end
                    app.current_SOC_index=SOC;
                    app = fcn_GlobalTDFFit(app,event,hyst,crate,temp,SOC,sorted_temperature_array,sorted_SOC_array,sorted_hysteresis_state_array,sorted_crate_array);
                    app.TDF_Optimization_done(hyst,crate,temp,SOC)=1;
                    fcn_CancelCheck(app);
                end
            end
        end
    end
    app.current_crate_index = Crate_index(hyst);
    %hyst down, crate up
    for crate = app.current_crate_index:length(sorted_crate_array{hyst})
        if ~(~app.CRateFlag && sorted_crate_array{hyst}(crate) == 0)
            if app.CRateFlag
                index=crate_index_rearrangement_array{hyst}(crate);
                app.ZustandPopup.Value=app.ZustandPopup.Items{1+index};
                app = fcn_ZustandPopup(app, event);
                app.current_crate_index = crate;
            end
            app.current_temp_index = temp_index{hyst}(crate);
            %hyst down, crate up, temp down
            for temp=app.current_temp_index:-1:1
                %Lade richtige Daten bei Wechsel der Temperatur
                index1=temperature_index_rearrangement_array{hyst}{crate}(temp);
                if~strcmp(app.startingtemperatureDropDown.Value,app.startingtemperatureDropDown.Items{1+index1})
                    app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1+index1};
                    app.TemperaturPopup.Value=app.startingtemperatureDropDown.Value;
                    app = fcn_TemperaturPopup(app, event);
                end
                app.current_temp_index = temp;
                app.current_SOC_index= SOC_index{hyst}{crate}(temp);
                %hyst down, crate up, temp down, SOC down
                for SOC=app.current_SOC_index:-1:1
                    %load SOC
                    index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                    if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                        app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                        app.SOCPopup.Value=app.startingSOCDropDown.Value;
                        app = fcn_SOCPopup(app, event);
                        if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                            app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                            app = fcn_ModellAuswahlPopup(app);
                            app = fcn_BatterieNamePopup(app);
                        end
                    end
                    app.current_SOC_index = SOC;
                    app = fcn_GlobalTDFFit(app,event,hyst,crate,temp,SOC,sorted_temperature_array,sorted_SOC_array,sorted_hysteresis_state_array,sorted_crate_array);
                    app.TDF_Optimization_done(hyst,crate,temp,SOC)=1;
                    fcn_CancelCheck(app);
                end
                app.current_SOC_index=SOC_index{hyst}{crate}(temp);
                %hyst down, crate up, temp down, SOC up
                for SOC=app.current_SOC_index+1:length(sorted_SOC_array{hyst}{crate}{temp})
                    %Lade richtige Daten bei Wechsel des SOC
                    index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                    if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                        app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                        app.SOCPopup.Value=app.startingSOCDropDown.Value;
                        app = fcn_SOCPopup(app, event);
                        if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                            app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                            app = fcn_ModellAuswahlPopup(app);
                            app = fcn_BatterieNamePopup(app);
                        end
                    end
                    app.current_SOC_index = SOC;
                    app = fcn_GlobalTDFFit(app,event,hyst,crate,temp,SOC,sorted_temperature_array,sorted_SOC_array,sorted_hysteresis_state_array,sorted_crate_array);
                    app.TDF_Optimization_done(hyst,crate,temp,SOC)=1;
                    fcn_CancelCheck(app);
                end
            end
            
            app.current_temp_index = temp_index{hyst}(crate);
            %hyst down, crate up, temp up
            for temp= app.current_temp_index+1:length(sorted_temperature_array{hyst}{crate})
                %Lade richtige Daten bei Wechsel der Temperatur
                index1=temperature_index_rearrangement_array{hyst}{crate}(temp);
                if~strcmp(app.startingtemperatureDropDown.Value,app.startingtemperatureDropDown.Items{1+index1})
                    app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1+index1};
                    app.TemperaturPopup.Value=app.startingtemperatureDropDown.Value;
                    app = fcn_TemperaturPopup(app, event);
                end
                app.current_temp_index=temp;
                app.current_SOC_index=SOC_index{hyst}{crate}(temp);
                %hyst down, crate up, temp up, SOC down
                for SOC=app.current_SOC_index:-1:1
                    %Lade richtige Daten bei Wechsel des SOC
                    index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                    if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                        app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                        app.SOCPopup.Value=app.startingSOCDropDown.Value;
                        app = fcn_SOCPopup(app, event);
                        if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                            app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                            app = fcn_ModellAuswahlPopup(app);
                            app = fcn_BatterieNamePopup(app);
                        end
                    end
                    app.current_SOC_index=SOC;
                    app = fcn_GlobalTDFFit(app,event,hyst,crate,temp,SOC,sorted_temperature_array,sorted_SOC_array,sorted_hysteresis_state_array,sorted_crate_array);
                    app.TDF_Optimization_done(hyst,crate,temp,SOC)=1;
                    fcn_CancelCheck(app);
                end
                app.current_SOC_index=SOC_index{hyst}{crate}(temp);
                %hyst down, crate up, temp up, SOC up
                for SOC=app.current_SOC_index+1:length(sorted_SOC_array{hyst}{crate}{temp})
                    %Lade richtige Daten bei Wechsel des SOC
                    index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                    if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                        app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                        app.SOCPopup.Value=app.startingSOCDropDown.Value;
                        app = fcn_SOCPopup(app, event);
                        if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                            app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                            app = fcn_ModellAuswahlPopup(app);
                            app = fcn_BatterieNamePopup(app);
                        end
                    end
                    app.current_SOC_index=SOC;
                    app = fcn_GlobalTDFFit(app,event,hyst,crate,temp,SOC,sorted_temperature_array,sorted_SOC_array,sorted_hysteresis_state_array,sorted_crate_array);
                    app.TDF_Optimization_done(hyst,crate,temp,SOC)=1;
                    fcn_CancelCheck(app);
                end
            end
        end
    end
end

%hyst up
app.current_hyst_index = original_Hysteresis;
for hyst = app.current_hyst_index+1:length(sorted_hysteresis_state_array)
    if app.HysteresisFlag
        index=hysteresis_state_index_rearrangement_array(hyst);
        app.ZustandPopup_2.Value=app.ZustandPopup_2.Items{1+index};
        app = fcn_ZustandPopup(app, event);
    end
    app.current_crate_index = Crate_index(hyst);
    %hyst up, crate down
    for crate = app.current_crate_index:-1:1
        if ~(~app.CRateFlag && sorted_crate_array{hyst}(crate) == 0)
            if app.CRateFlag
                index=crate_index_rearrangement_array{hyst}(crate);
                app.ZustandPopup.Value=app.ZustandPopup.Items{1+index};
                app = fcn_ZustandPopup(app, event);
                app.current_crate_index = crate;
            end
            app.current_temp_index = temp_index{hyst}(crate);
            %hyst up, crate down, temp down
            for temp=app.current_temp_index:-1:1
                %Lade richtige Daten bei Wechsel der Temperatur
                index1=temperature_index_rearrangement_array{hyst}{crate}(temp);
                if~strcmp(app.startingtemperatureDropDown.Value,app.startingtemperatureDropDown.Items{1+index1})
                    app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1+index1};
                    app.TemperaturPopup.Value=app.startingtemperatureDropDown.Value;
                    app = fcn_TemperaturPopup(app, event);
                end
                app.current_temp_index = temp;
                app.current_SOC_index= SOC_index{hyst}{crate}(temp);
                %hyst down, crate down, temp down, SOC down
                for SOC=app.current_SOC_index:-1:1
                    %load SOC
                    index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                    if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                        app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                        app.SOCPopup.Value=app.startingSOCDropDown.Value;
                        app = fcn_SOCPopup(app, event);
                        if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                            app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                            app = fcn_ModellAuswahlPopup(app);
                            app = fcn_BatterieNamePopup(app);
                        end
                    end
                    app.current_SOC_index = SOC;
                    app = fcn_GlobalTDFFit(app,event,hyst,crate,temp,SOC,sorted_temperature_array,sorted_SOC_array,sorted_hysteresis_state_array,sorted_crate_array);
                    app.TDF_Optimization_done(hyst,crate,temp,SOC)=1;
                    fcn_CancelCheck(app);
                end
                app.current_SOC_index=SOC_index{hyst}{crate}(temp);
                %hyst up, crate down, temp down, SOC up
                for SOC=app.current_SOC_index+1:length(sorted_SOC_array{hyst}{crate}{temp})
                    %Lade richtige Daten bei Wechsel des SOC
                    index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                    if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                        app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                        app.SOCPopup.Value=app.startingSOCDropDown.Value;
                        app = fcn_SOCPopup(app, event);
                        if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                            app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                            app = fcn_ModellAuswahlPopup(app);
                            app = fcn_BatterieNamePopup(app);
                        end
                    end
                    app.current_SOC_index = SOC;
                    app = fcn_GlobalTDFFit(app,event,hyst,crate,temp,SOC,sorted_temperature_array,sorted_SOC_array,sorted_hysteresis_state_array,sorted_crate_array);
                    app.TDF_Optimization_done(hyst,crate,temp,SOC)=1;
                    fcn_CancelCheck(app);
                end
            end
            
            app.current_temp_index = temp_index{hyst}(crate);
            %hyst up, crate down, temp up
            for temp= app.current_temp_index+1:length(sorted_temperature_array{hyst}{crate})
                %Lade richtige Daten bei Wechsel der Temperatur
                index1=temperature_index_rearrangement_array{hyst}{crate}(temp);
                if~strcmp(app.startingtemperatureDropDown.Value,app.startingtemperatureDropDown.Items{1+index1})
                    app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1+index1};
                    app.TemperaturPopup.Value=app.startingtemperatureDropDown.Value;
                    app = fcn_TemperaturPopup(app, event);
                end
                app.current_temp_index=temp;
                app.current_SOC_index=SOC_index{hyst}{crate}(temp);
                %hyst up, crate down, temp down, SOC down
                for SOC=app.current_SOC_index:-1:1
                    %Lade richtige Daten bei Wechsel des SOC
                    index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                    if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                        app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                        app.SOCPopup.Value=app.startingSOCDropDown.Value;
                        app = fcn_SOCPopup(app, event);
                        if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                            app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                            app = fcn_ModellAuswahlPopup(app);
                            app = fcn_BatterieNamePopup(app);
                        end
                    end
                    app.current_SOC_index=SOC;
                    app = fcn_GlobalTDFFit(app,event,hyst,crate,temp,SOC,sorted_temperature_array,sorted_SOC_array,sorted_hysteresis_state_array,sorted_crate_array);
                    app.TDF_Optimization_done(hyst,crate,temp,SOC)=1;
                    fcn_CancelCheck(app);
                end
                app.current_SOC_index=SOC_index{hyst}{crate}(temp);
                %hyst up, crate down, temp down, SOC up
                for SOC=app.current_SOC_index+1:length(sorted_SOC_array{hyst}{crate}{temp})
                    %Lade richtige Daten bei Wechsel des SOC
                    index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                    if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                        app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                        app.SOCPopup.Value=app.startingSOCDropDown.Value;
                        app = fcn_SOCPopup(app, event);
                        if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                            app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                            app = fcn_ModellAuswahlPopup(app);
                            app = fcn_BatterieNamePopup(app);
                        end
                    end
                    app.current_SOC_index=SOC;
                    app = fcn_GlobalTDFFit(app,event,hyst,crate,temp,SOC,sorted_temperature_array,sorted_SOC_array,sorted_hysteresis_state_array,sorted_crate_array);
                    app.TDF_Optimization_done(hyst,crate,temp,SOC)=1;
                    fcn_CancelCheck(app);
                end
            end
        end
    end
    app.current_crate_index = Crate_index(hyst);
    %hyst up, crate up
    for crate = app.current_crate_index:length(sorted_crate_array{hyst})
        if ~(~app.CRateFlag && sorted_crate_array{hyst}(crate) == 0)
            if app.CRateFlag
                index=crate_index_rearrangement_array{hyst}(crate);
                app.ZustandPopup.Value=app.ZustandPopup.Items{1+index};
                app = fcn_ZustandPopup(app, event);
                app.current_crate_index = crate;
            end
            app.current_temp_index = temp_index{hyst}(crate);
            %hyst up, crate up, temp down
            for temp=app.current_temp_index:-1:1
                %Lade richtige Daten bei Wechsel der Temperatur
                index1=temperature_index_rearrangement_array{hyst}{crate}(temp);
                if~strcmp(app.startingtemperatureDropDown.Value,app.startingtemperatureDropDown.Items{1+index1})
                    app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1+index1};
                    app.TemperaturPopup.Value=app.startingtemperatureDropDown.Value;
                    app = fcn_TemperaturPopup(app, event);
                end
                app.current_temp_index = temp;
                app.current_SOC_index= SOC_index{hyst}{crate}(temp);
                %hyst up, crate up, temp down, SOC down
                for SOC=app.current_SOC_index:-1:1
                    %load SOC
                    index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                    if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                        app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                        app.SOCPopup.Value=app.startingSOCDropDown.Value;
                        app = fcn_SOCPopup(app, event);
                        if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                            app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                            app = fcn_ModellAuswahlPopup(app);
                            app = fcn_BatterieNamePopup(app);
                        end
                    end
                    app.current_SOC_index = SOC;
                    app = fcn_GlobalTDFFit(app,event,hyst,crate,temp,SOC,sorted_temperature_array,sorted_SOC_array,sorted_hysteresis_state_array,sorted_crate_array);
                    app.TDF_Optimization_done(hyst,crate,temp,SOC)=1;
                    fcn_CancelCheck(app);
                end
                app.current_SOC_index=SOC_index{hyst}{crate}(temp);
                %hyst up, crate up, temp down, SOC up
                for SOC=app.current_SOC_index+1:length(sorted_SOC_array{hyst}{crate}{temp})
                    %Lade richtige Daten bei Wechsel des SOC
                    index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                    if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                        app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                        app.SOCPopup.Value=app.startingSOCDropDown.Value;
                        app = fcn_SOCPopup(app, event);
                        if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                            app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                            app = fcn_ModellAuswahlPopup(app);
                            app = fcn_BatterieNamePopup(app);
                        end
                    end
                    app.current_SOC_index = SOC;
                    app = fcn_GlobalTDFFit(app,event,hyst,crate,temp,SOC,sorted_temperature_array,sorted_SOC_array,sorted_hysteresis_state_array,sorted_crate_array);
                    app.TDF_Optimization_done(hyst,crate,temp,SOC)=1;
                    fcn_CancelCheck(app);
                end
            end
            
            app.current_temp_index = temp_index{hyst}(crate);
            %hyst up, crate up, temp up
            for temp= app.current_temp_index+1:length(sorted_temperature_array{hyst}{crate})
                %Lade richtige Daten bei Wechsel der Temperatur
                index1=temperature_index_rearrangement_array{hyst}{crate}(temp);
                if~strcmp(app.startingtemperatureDropDown.Value,app.startingtemperatureDropDown.Items{1+index1})
                    app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1+index1};
                    app.TemperaturPopup.Value=app.startingtemperatureDropDown.Value;
                    app = fcn_TemperaturPopup(app, event);
                end
                app.current_temp_index=temp;
                app.current_SOC_index=SOC_index{hyst}{crate}(temp);
                %hyst up, crate up, temp up, SOC down
                for SOC=app.current_SOC_index:-1:1
                    %Lade richtige Daten bei Wechsel des SOC
                    index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                    if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                        app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                        app.SOCPopup.Value=app.startingSOCDropDown.Value;
                        app = fcn_SOCPopup(app, event);
                        if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                            app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                            app = fcn_ModellAuswahlPopup(app);
                            app = fcn_BatterieNamePopup(app);
                        end
                    end
                    app.current_SOC_index=SOC;
                    app = fcn_GlobalTDFFit(app,event,hyst,crate,temp,SOC,sorted_temperature_array,sorted_SOC_array,sorted_hysteresis_state_array,sorted_crate_array);
                    app.TDF_Optimization_done(hyst,crate,temp,SOC)=1;
                    fcn_CancelCheck(app);
                end
                app.current_SOC_index=SOC_index{hyst}{crate}(temp);
                %hyst up, crate up, temp up, SOC up
                for SOC=app.current_SOC_index+1:length(sorted_SOC_array{hyst}{crate}{temp})
                    %Lade richtige Daten bei Wechsel des SOC
                    index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                    if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                        app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                        app.SOCPopup.Value=app.startingSOCDropDown.Value;
                        app = fcn_SOCPopup(app, event);
                        if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                            app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                            app = fcn_ModellAuswahlPopup(app);
                            app = fcn_BatterieNamePopup(app);
                        end
                    end
                    app.current_SOC_index=SOC;
                    app = fcn_GlobalTDFFit(app,event,hyst,crate,temp,SOC,sorted_temperature_array,sorted_SOC_array,sorted_hysteresis_state_array,sorted_crate_array);
                    app.TDF_Optimization_done(hyst,crate,temp,SOC)=1;
                    fcn_CancelCheck(app);
                end
            end
        end
    end
end
end
