function app = fcn_ZeitbereichsfittingCheckBoxValueChanged(app, event)

if app.ZeitbereichsfittingCheckBox.Value 
%     app.UsedTDDataButtonGroup.Visible = 'on';
%     app.ZeitbereichsfittingParameterAuswahlTable.Visible = 'on';
%     app.AktualisierenButton.Visible = 'on';
%     app.ZeitbereichsgrenzextrapolationCheckBox.Visible = 'on';
    app.ZeitbereichsfittingParameterAuswahlTable.Enable = 'on';
    app.AktualisierenButton.Enable = 'on';
    app.ZeitbereichsgrenzextrapolationCheckBox.Enable = 'on';
    app.RelaxButton.Enable = 'on';
    app.PulsesButton.Enable = 'on';
    app.BothButton.Enable = 'on';
else
%     app.UsedTDDataButtonGroup.Visible = 'off';
    app.ZeitbereichsfittingParameterAuswahlTable.Enable = 'off';
    app.AktualisierenButton.Enable = 'off';
    app.ZeitbereichsgrenzextrapolationCheckBox.Enable = 'off';
    app.RelaxButton.Enable = 'off';
    app.PulsesButton.Enable = 'off';
    app.BothButton.Enable = 'off';
end
end

