function app = fcn_GlobalFit_TC(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
                               sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array );
global DRT_GUI
CRate_var = strrep(DRT_GUI.Testparameter.Zustand,'m','-');
CRate_var = str2num(strrep(CRate_var(1:end-4),'_','.'));
if ~isfield(DRT_GUI.Messdaten,'RelaxEIS') || isempty(DRT_GUI.Messdaten.RelaxEIS) || CRate_var == 0
    return
end
set(app.LHD_FittetParametersButtonGroup,'SelectedObject',app.MFModelButton)
fcn_LHD_UpdateParameterTable(app,event);
fcn_LHD_CalculateDRTButtonPushed(app,event);
fcn_LHD_DRT_FitButtonPushed(app, event);

result_array = zeros(4,2*length(app.LHD_ParameterTable.Data(:,1)));
counter = 1;
%evaluation of the results
for n = 1:length(app.LHD_ParameterTable.Data(:,1))  
    for n1 = 1:length(app.LHD_ParameterTable.Data(:,1))
        if n ~= n1
            app.LHD_ParameterTable.Data{n1,2} = false; 
        else
            app.LHD_ParameterTable.Data{n1,2} = true; 
        end
    end
    
    %update LHD_ParameterTable_2
    TableData(1,1)= {'R'};
    TableData(1,2)= {[false]};
    TableData(2,1)= {'Tau'};
    TableData(2,2)= {[false]};
    if app.GUI
        set(app.LHD_ParameterTable_2,'Data',TableData);
        set(app.LHD_ParameterTable_2,'ColumnEditable',true);
    else
        app.LHD_ParameterTable_2.Data = TableData;
        app.LHD_ParameterTable_2.ColumnEditable = true;
    end
    for n1 = 1:length(app.LHD_ParameterTable_2.Data(:,1))
        for n2 = 1:length(app.LHD_ParameterTable_2.Data(:,1))
            if n2 ~= n1
                app.LHD_ParameterTable_2.Data{n2,2} = false; 
            else
                app.LHD_ParameterTable_2.Data{n2,2} = true; 
            end
        end
        [app, p_best] = fcn_LHD_FitTimeConstantButtonPushed(app,event);
        result_array(:,counter) = transpose(p_best);
        counter = counter + 1;
    end
end
p_best = mean(result_array,2);
app.LHD_Time_Constants{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index} = p_best;
DRT_GUI.Fit.LHD = p_best;
app = fcn_SpeichernButton(app, event);
end