function app = fcn_XMLCreationForVerification(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,considered_SOC_for_verification)
%Einlesen der FittingGUI Daten
autofit_path = cd;
app.sorted_hyst_states = [];
app.sorted_crate = [];
app.sorted_temp = [];
app.sorted_soc = [];
global DRT_GUI
if app.HysteresisFlag
    starting_Zustand_2 = DRT_GUI.Testparameter.Zustand_2;
end
if app.CRateFlag
    starting_Zustand = DRT_GUI.Testparameter.Zustand;
end
app.user = char(java.lang.System.getProperty('user.name'));
app.actBattery = Battery('');
app.actBattery.deleteAllLHD();
app.pulse = PulsData();
app = load_models(app);
cp = pwd;
app.default_path = cp;
if app.GUI
    path=fullfile(cp,'output',DRT_GUI.Testparameter.Batterie);
else
    path=fullfile(app.OutputPath,DRT_GUI.Testparameter.Batterie);
end

app.actBattery = Battery('');

app.SOC = [];
app.SOH = [];
app.Hyst = [];
app.Crate = [];
app.Temp = [];

app.actSOC = 1;
app.actTemp = 1;
app.actSOH = 1;
app.actHyst = 1;
app.actCrate = 1;
app.actParameter = 1;

ReFit_Whitelist = string(missing);
sSOH = '100';
tSOH = round(str2double(sSOH));
if isempty(app.SOH)
    app.SOH = tSOH;
    app.actSOH = 1;
else
    iSOH = find(app.SOH==tSOH);
    if iSOH
        app.actSOH = iSOH;
    else
        app.SOH = [app.SOH,tSOH];
        app.actSOH = length(app.SOH);
    end
end
app.actBattery = app.actBattery.setAuswertung('FittingGUI Daten',app.actSOH);

warning('off', 'MATLAB:dispatcher:UnresolvedFunctionHandle');
current_path = pwd;
cd(path);
list_hyst = dir('*H');
if isempty(list_hyst)
    list_hyst(1).name = [];
    length_list_hyst = 1;
else
    for n = 1:length(list_hyst)
        list_hyst(n).name = [list_hyst(n).name '\'];
    end
    length_list_hyst = length(list_hyst);
end
for hyst_index = 1:length_list_hyst
    cd(fullfile(path, list_hyst(hyst_index).name))
    if app.HysteresisFlag
        hyst = DRT_GUI.Testparameter.Zustand_2;
        if isempty(app.Hyst)
            app.Hyst = str2num(strrep(hyst(1:end-1),'m','-'));
            app.actHyst = 1;
            app.sorted_hyst_states = str2num(strrep(hyst(1:end-1),'m','-'));
        else
            iHyst = find(app.Hyst==str2num(strrep(hyst(1:end-1),'m','-')));
            if iHyst
                app.actHyst = iHyst;
            else
                app.Hyst = [app.Hyst,str2num(strrep(hyst(1:end-1),'m','-'))];
                app.actHyst = length(app.Hyst);
                app.sorted_hyst_states = [app.sorted_hyst_states, str2num(strrep(hyst(1:end-1),'m','-'))];
            end
        end
        app.sorted_crate{app.actHyst} = [];
        app.sorted_temp{app.actHyst} = [];
        app.sorted_soc{app.actHyst} = [];
    else
        app.actHyst = 1;
        app.Hyst = 0;
        app.sorted_hyst_states = 0;
    end
    list_crate = dir('*C');
    if isempty(list_crate)
        list_crate(1).name = [];
        length_list_crate = 1;
    else
        for n = 1:length(list_crate)
            list_crate(n).name = [list_crate(n).name '\'];
        end
        length_list_crate = length(list_crate);
    end
    if ~app.CrateCheckBox.Value
        list_crate = [];
        list_crate(1).name = '0C_DC\';
        length_list_crate = 1;
    end
    for crate_index = 1:length_list_crate
        cd(fullfile(path, [list_hyst(hyst_index).name,list_crate(crate_index).name]))
        if app.CRateFlag
            crate = list_crate(crate_index).name(1:end-1);
            if isempty(app.Crate)
                app.Crate = str2num(strrep(crate(1:end-4),'m','-'));
                app.actCrate = 1;
            else
                iCrate = find(app.Crate==str2num(strrep(strrep(crate(1:end-4),'m','-'),'_','.')));
                if iCrate
                    app.actCrate = iCrate;
                else
                    app.Crate = [app.Crate,str2num(strrep(strrep(crate(1:end-4),'m','-'),'_','.'))];
                    app.actCrate = length(app.Crate);
                end
            end
            app.sorted_temp{app.actHyst}{app.actCrate} = [];
            app.sorted_soc{app.actHyst}{app.actCrate} = [];
            if isempty(app.sorted_crate)
                app.sorted_crate{app.actHyst} = str2num(strrep(strrep(crate(1:end-4),'m','-'),'_','.'));
            else
                if isempty(app.sorted_crate{app.actHyst})
                    app.sorted_crate{app.actHyst} = str2num(strrep(strrep(crate(1:end-4),'m','-'),'_','.'));
                else
                    app.sorted_crate{app.actHyst} = [app.sorted_crate{app.actHyst}, str2num(strrep(strrep(crate(1:end-4),'m','-'),'_','.'))];
                end
            end
        else
            app.actCrate = 1;
        end
        
        list = dir('*grad*');
        for i=1:length(list)
            cd(fullfile(path,[list_hyst(hyst_index).name,list_crate(crate_index).name,list(i).name]))
            act_temp_list = dir('*SOC.mat');
            cd (current_path);
            for j=1:length(act_temp_list)
                load(fullfile(path,[list_hyst(hyst_index).name,list_crate(crate_index).name,list(i).name,'\',act_temp_list(j).name]));
                % Lade allgemeine Parameter beim ersten Aufruf
                if(i==1 && j==1 && hyst_index == 1 && crate_index == 1)
                    app.actBattery.name = DRT_GUI.Testparameter.Batterie;
                    app.SF_BatName.Text = DRT_GUI.Testparameter.Batterie;
                    try
                        iModell = find(strcmp(app.Modellliste,['ESB_',DRT_GUI.Fit.aktuell_Modell.Modellname]));
                    catch
                        warning('Kein SF_ESB vorhandem!')
                        return
                    end
                    app = change_act_model(app,iModell);
                    app.SF_ModellauswahlDropDown.Value = app.SF_ModellauswahlDropDown.Items(iModell);
                end
                if(j==1)
                    % Temperatur finden
                    temp = DRT_GUI.Testparameter.Temperatur;
                    if isempty(app.Temp)
                        app.Temp = temp;
                        app.actTemp = 1;
                    else
                        iTemp = find(app.Temp==temp);
                        if iTemp
                            app.actTemp = iTemp;
                        else
                            app.Temp = [app.Temp,temp];
                            app.actTemp = length(app.Temp);
                        end
                    end
                    if isempty(app.sorted_temp)
                        app.sorted_temp{app.actHyst}{app.actCrate} = temp;
                    else
                        if isempty(app.sorted_temp{app.actHyst}{app.actCrate})
                            app.sorted_temp{app.actHyst}{app.actCrate} = temp;
                        else
                            app.sorted_temp{app.actHyst}{app.actCrate} = [app.sorted_temp{app.actHyst}{app.actCrate}, temp];
                        end
                    end
                    app.sorted_soc{app.actHyst}{app.actCrate}{app.actTemp} = [];
                    % Kapazität speichern
                    if isfield(DRT_GUI.Testparameter,'Cap')
                        app.SF_NomCap.Text = [num2str(DRT_GUI.Testparameter.Cap) ' Ah'];
                        app.actBattery = app.actBattery.addCapacity(DRT_GUI.Testparameter.Cap,app.actTemp,app.actSOH,app.actHyst,app.actCrate);
                    else
                        sCap = inputdlg('Bitte Kapazität eingeben:','Kapazität');
                        app.SF_NomCap.Text = [sCap{1},' Ah'];
                        app.actBattery = app.actBattery.addCapacity(str2double(sCap),app.actTemp,app.actSOH,app,app.act,app.actHyst,app.actCrate);
                    end
                    qOCV = QOCV();
                    app.actBattery = app.actBattery.addqOCV_CH(qOCV,app.actTemp,app.actSOH,app.actHyst,app.actCrate);
                    app.actBattery = app.actBattery.addqOCV_DCH(qOCV,app.actTemp,app.actSOH,app.actHyst,app.actCrate);
                end
                % SOC finden
                SOC = DRT_GUI.Testparameter.SOC;
                if isempty(app.SOC)
                    app.SOC = SOC;
                    app.actSOC = 1;
                else
                    % Einsortieren weiterer Messungen in aktuelles SOC Array, falls neue SOCs
                    % auftreten, wird hinten angehangen
                    iSOC = find(app.SOC==SOC);
                    if iSOC
                        app.actSOC = iSOC;
                    else
                        app.SOC = [app.SOC,SOC];
                        app.actSOC = length(app.SOC);
                    end
                end
                if isempty(app.sorted_soc)
                    app.sorted_soc{app.actHyst}{app.actCrate}{app.actTemp} = SOC;
                else
                    if isempty(app.sorted_soc{app.actHyst}{app.actCrate}{app.actTemp})
                        app.sorted_soc{app.actHyst}{app.actCrate}{app.actTemp} = SOC;
                    else
                        app.sorted_soc{app.actHyst}{app.actCrate}{app.actTemp} = [app.sorted_soc{app.actHyst}{app.actCrate}{app.actTemp}, SOC];
                    end
                end
                % EIS Daten ablegen
                data = EISData();
                data.Zreal = DRT_GUI.Messdaten.Zreal;
                data.Zimg = DRT_GUI.Messdaten.Zimg;
                data.frequenz = DRT_GUI.Messdaten.frequenz;
                app.actBattery = app.actBattery.addEISdata(data,app.actSOC,app.actTemp,app.actSOH,app.actHyst,app.actCrate);
                % Fit Ablegen
                if ~isempty(iModell)
                    fit = Fit();
                    fit.Parameter = DRT_GUI.Fit.Parameter;
                    fit.Parameter_min = DRT_GUI.Fit.Parameter_min;
                    fit.Parameter_max = DRT_GUI.Fit.Parameter_max;
                    fit.ParFix = DRT_GUI.Fit.ParFix;
                    fit.Zreal = DRT_GUI.Fit.Zreal;
                    fit.Zimag = DRT_GUI.Fit.Zimg;
                    fit.Residuum = DRT_GUI.Fit.residuum;
                    app.actBattery = app.actBattery.addFit(fit,app.actSOC,app.actTemp,app.actSOH,app.actHyst,app.actCrate);
                    if app.LHDCheckBox_2.Value
                        lhd = LHD();
                        if isfield(DRT_GUI.Fit,'LHD') && ~isempty(DRT_GUI.Fit.LHD)
                            lhd.Parameter = DRT_GUI.Fit.LHD;
                        else
                            lhd.Parameter = zeros(1,4);
                        end
                        app.actBattery = app.actBattery.addLHD(lhd,app.actSOC,app.actTemp,app.actSOH,app.actHyst,app.actCrate);
                    end
                end
                if isfield(DRT_GUI.Messdaten,'relax')
                    if ~isempty(DRT_GUI.Messdaten.relax.zeit)
                        relax = RelaxData();
                        relax.time = DRT_GUI.Messdaten.relax.zeit;
                        relax.voltage = DRT_GUI.Messdaten.relax.spannung;
                        relax.current = DRT_GUI.Messdaten.relax.strom;
                        app.actBattery = app.actBattery.addRelaxdata(relax,app.actSOC,app.actTemp,app.actSOH,app.actHyst,app.actCrate);
                    end
                else
                    relax = RelaxData();
                    app.actBattery = app.actBattery.addRelaxdata(relax,app.actSOC,app.actTemp,app.actSOH,app.actHyst,app.actCrate);
                end
            end
        end
    end
end
% Aktualisieren der Index Vektoren
app.actBattery = app.actBattery.addSOCtable(app.SOC);
app.actBattery = app.actBattery.addTemptable(app.Temp);
app.actBattery = app.actBattery.addSOHtable(app.SOH);
app.actBattery = app.actBattery.addHysttable(app.Hyst);
app.actBattery = app.actBattery.addCratetable(app.Crate);

app.actParameter = 1;
app = sort_SOC(app);
app = sort_temp(app);
app = sort_crate(app);
app = sort_hyst(app);

for i1 = 1:length(app.sorted_hyst_states)
    [app.sorted_crate{i1}(:),rearr_crate] = sort(app.sorted_crate{i1});
    app.sorted_temp{i1} = app.sorted_temp{i1}(rearr_crate);
    for i2 = 1:length(app.sorted_crate{i1})
        [app.sorted_temp{i1}{i2}(:),rearr_temp] = sort(app.sorted_temp{i1}{i2});
        app.sorted_soc{i1}{i2} = app.sorted_soc{i1}{i2}(rearr_temp);
    end
end

if app.offButton_temperature_depencendy.Value
    reference_counter = 0;
    if app.referencetemperatureEditField.Value ~= -Inf
        if app.CRateFlag && app.HysteresisFlag
            state_string = [app.ZustandPopup_2.Value '/' app.ZustandPopup.Value];
        elseif app.CRateFlag
            state_string = app.ZustandPopup.Value;
        else
            state_string = app.ZustandPopup_2.Value;
        end
        f = dir(fullfile('output', app.BatterieNamePopup.Value, state_string, '*'));
        for reference_idx = 3:length(f)
            reference_temperature = f(reference_idx).name;
            reference_temperature = reference_temperature(1:end -4 );
            if contains(reference_temperature,'m')
                reference_temperature =strcat('-',reference_temperature(2:end));
            end
            if strcmp(num2str(app.referencetemperatureEditField.Value),reference_temperature)
                reference_counter = 1;
            end
        end
        if reference_counter == 0
            msgbox('Please select a valid reference temperature')
            return
        end
        
    else
        msgbox('Please select a valid reference temperature')
        return
    end
end

%% Auswahlboxen aktualisieren
app = update_listboxes(app);


%% ReFit Parameter
for hyst_index = 1:length_list_hyst
    cd(fullfile(path, list_hyst(hyst_index).name))
    if app.HysteresisFlag
        hyst = DRT_GUI.Testparameter.Zustand_2;
        if isempty(app.Hyst)
            app.Hyst = str2num(strrep(hyst(1:end-1),'m','-'));
            app.actHyst = 1;
        else
            iHyst = find(app.Hyst==str2num(strrep(hyst(1:end-1),'m','-')));
            if iHyst
                app.actHyst = iHyst;
            else
                app.Hyst = [app.Hyst,str2num(strrep(hyst(1:end-1),'m','-'))];
                app.actHyst = length(app.Hyst);
            end
        end
    else
        app.actHyst = 1;
    end
    list_crate = dir('*C');
    if isempty(list_crate)
        list_crate(1).name = [];
        length_list_crate = 1;
    else
        for n = 1:length(list_crate)
            list_crate(n).name = [list_crate(n).name '\'];
        end
        length_list_crate = length(list_crate);
    end
    if ~app.CrateCheckBox.Value
        list_crate = [];
        list_crate(1).name = '0C_DC\';
        length_list_crate = 1;
    end
    for crate_index = 1:length_list_crate
        cd(fullfile(path, [list_hyst(hyst_index).name,list_crate(crate_index).name]))
        if app.CRateFlag
            crate = list_crate(crate_index).name(1:end-1);
            if isempty(app.Crate)
                app.Crate = str2num(strrep(crate(1:end-4),'m','-'));
                app.actCrate = 1;
            else
                iCrate = find(app.Crate==str2num(strrep(strrep(crate(1:end-4),'m','-'),'_','.')));
                if iCrate
                    app.actCrate = iCrate;
                else
                    app.Crate = [app.Crate,str2num(strrep(strrep(crate(1:end-4),'m','-'),'_','.'))];
                    app.actCrate = length(app.Crate);
                end
            end
        else
            app.actCrate = 1;
        end
        list = dir('*grad*');
        for i=1:length(list)
            cd([path,'\',list_hyst(hyst_index).name,list_crate(crate_index).name,list(i).name])
            act_temp_list = dir('*SOC.mat');
            cd (current_path);
            for j=1:length(act_temp_list)
                warning('off', 'MATLAB:dispatcher:UnresolvedFunctionHandle');
                load(fullfile(path,[list_hyst(hyst_index).name,list_crate(crate_index).name,list(i).name,'\',act_temp_list(j).name]));

                if(j==1)
                    % Temperatur finden
                    temp = DRT_GUI.Testparameter.Temperatur;
                    iTemp = find(app.Temp==temp);
                    if iTemp
                        app.actTemp = iTemp;
                    else
                        app.Temp = [app.Temp,temp];
                        app.actTemp = length(app.Temp);
                    end
                end
                % SOC finden
                SOC = DRT_GUI.Testparameter.SOC;
                % Einsortieren weiterer Messungen in aktuelles SOC Array, falls neue SOCs
                % auftreten, wird hinten angehangen
                iSOC = find(app.SOC==SOC);
                if iSOC
                    app.actSOC = iSOC;
                else
                    app.SOC = [app.SOC,SOC];
                    app.actSOC = length(app.SOC);
                end

                if isfield(DRT_GUI.Fit.Implementierung,'OCV')
                    if ~isempty(DRT_GUI.Fit.aktuell_Modell.P_Name)&&~isempty(DRT_GUI.Fit.Implementierung.OCV)
                        for k = 1:length(DRT_GUI.Fit.aktuell_Modell.P_Name)
                            % are there responding ReFit parameters?
                            tdf_refit_idx = find(contains(DRT_GUI.Fit.Implementierung.OCV(:,1),['ReFit_',DRT_GUI.Fit.aktuell_Modell.P_Name{1,k}]));
                            if length(tdf_refit_idx) <= 1
                                %if there is only one, save it as no current dependency is parameterized
                                tdf_cd_flag = 0;
                                DRT_GUI.Fit.aktuell_Modell.P_Name{3,k}=find(transpose(strcmp(['ReFit_',DRT_GUI.Fit.aktuell_Modell.P_Name{1,k}],DRT_GUI.Fit.Implementierung.OCV(:,1))));
                            else
                                %current dependency is parameterized
                                tdf_cd_flag = 1;
                                tdf_cd_data = DRT_GUI.Fit.Implementierung.OCV(tdf_refit_idx,1:2);
                                underscores_goal = regexp(tdf_cd_data{1,1},'_');
                                for iter = 1:length(tdf_cd_data(:,1))
                                    underscores = regexp(tdf_cd_data{iter,1},'_');
                                    tdf_cd_data{iter,1} = str2num(strrep(tdf_cd_data{iter,1}(underscores(end)+1:end-1),'m','-'));                                       
                                end
                                tdf_cd_data = cell2mat(tdf_cd_data); 
                                if ~app.CrateCheckBox.Value
                                    %if C rate dependency is denied, interpolate to 0 C value                                   
                                    
                                    zero_C_value = interp1(transpose(tdf_cd_data(:,1)), transpose(tdf_cd_data(:,2)), 0);
                                    DRT_GUI.Fit.Implementierung.OCV{tdf_refit_idx(1),1} = DRT_GUI.Fit.Implementierung.OCV{tdf_refit_idx(1),1}(1:underscores_goal(end)-1);
                                    DRT_GUI.Fit.Implementierung.OCV{tdf_refit_idx(1),2} = zero_C_value;
                                    DRT_GUI.Fit.Implementierung.OCV(tdf_refit_idx(2:end),:) = [];
                                    tdf_refit_idx = tdf_refit_idx(1);
                                    tdf_cd_flag = 0;
                                end
                                DRT_GUI.Fit.aktuell_Modell.P_Name{3,k} = tdf_refit_idx;                                
                            end
                            
                           
                            if isempty(DRT_GUI.Fit.aktuell_Modell.P_Name{3,k}) %if no ReFit is found
                                DRT_GUI.Fit.aktuell_Modell.P_Name{4,k} = 0;
                            elseif sum(strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name{1,k},ReFit_Whitelist))>0 %if it is already on the Whitelist (Whitelist is true for all ReFits in automatic export)
                                DRT_GUI.Fit.aktuell_Modell.P_Name{4,k} = 1;
                            else
                                if app.useTDFparametersCheckBox.Value
                                    DRT_GUI.Fit.aktuell_Modell.P_Name{4,k} = 1;
                                    ReFit_Whitelist = [ReFit_Whitelist;DRT_GUI.Fit.aktuell_Modell.P_Name{1,k}];
                                else
                                    DRT_GUI.Fit.aktuell_Modell.P_Name{4,k} = 0;
                                end
                            end
                            %save C rates of the pulses
                            tdf_cd_crates = [];
                            if tdf_cd_flag && length(tdf_refit_idx) > 1
                                DRT_GUI.Fit.aktuell_Modell.P_Name{5,k} = transpose(tdf_cd_data(:,1));
                                tdf_cd_crates = transpose(tdf_cd_data(:,1));
                            end
                        end

                        app = change_several_fit_parameters(app,DRT_GUI.Fit.aktuell_Modell.P_Name,DRT_GUI.Fit.Implementierung.OCV,tdf_cd_flag,tdf_cd_crates);
                    else
                        disp(['No table with ReFit values found for ',temp,' grad, ',SOC,'% .'])
                    end
                end
            end
        end
    end
end
app.actParameter = 1;
app = sort_SOC(app);
app = sort_temp(app);
app = sort_crate(app);
app = sort_hyst(app);
app = sort_sorted_arrays(app);

app.SF_NominelleKapazittCheckBox.Value = 1;
if app.HysteresisFlag
    hyst_index = find(app.Hyst == str2num(strrep(starting_Zustand_2(1:end-1),'m','-')));
else
    hyst_index = 1;
end
if app.CRateFlag
    crate_index = find(app.Crate == str2num(strrep(starting_Zustand(1:end-4),'m','-')));
else
    crate_index = 1;
end
if ~app.onButton_temperature_depencendy.Value
    index = find(app.Temp==app.referencetemperatureEditField.Value);
    if ~isempty(index)
        app.actBattery.nom_cap = app.actBattery.capacity(hyst_index,crate_index,index,1);
    else
        msgbox('Reference temperature not found')
        return
    end
else
    if ismember(25,app.Temp)
        index = find(app.Temp == 25);
        app.actBattery.nom_cap = app.actBattery.capacity(hyst_index,crate_index,index,1);
    else
        index = find(app.Temp == str2num(app.TemperaturTextBox.Value));
    end
    app.actBattery.nom_cap = app.actBattery.capacity(hyst_index,crate_index,index,1);
end

cd(autofit_path)
app = fcn_Automatic_qOCV_Import(app,event);
addpath('FittingGUIFunctions/SimpleFit/SimpleFitFunctions/xml_functions');

app = update_listboxes(app);
app.SF_SchrittweiteEditField.Value = 5;
app.SF_OCVSchrittweiteEditField.Value = 1;
framework_struct = app.fitProperties.aktuell_Modell.Modell.generateXMLstruct(app,app.actBattery.fit,app.actBattery.SOC,...
    app.actBattery.temp,app.actSOH,app.actBattery.nom_cap,app.SF_SchrittweiteEditField.Value,0,app.actBattery.Hyst,app.actBattery.Crate);
if ~isempty(app.actBattery.qOCV_CH(1,1).SOC)&&~isempty(app.actBattery.qOCV_DCH(1,1).SOC)
    framework_struct = generate_OCV(framework_struct,app.fitProperties.aktuell_Modell.Modellname,[-10:app.SF_OCVSchrittweiteEditField.Value:100],...
        app.actBattery.temp,app.actBattery.qOCV_CH,app.actBattery.qOCV_DCH);
else
    if ~isempty(app.actBattery.qOCV_CH(1,1).SOC)
        framework_struct = generate_OCV(framework_struct,app.fitProperties.aktuell_Modell.Modellname,[-10:app.SF_OCVSchrittweiteEditField.Value:100],...
            app.actBattery.temp,app.actBattery.qOCV_CH);
    else
        if ~isempty(app.actBattery.qOCV_DCH(1,1).SOC)
            framework_struct = generate_OCV(framework_struct,app.fitProperties.aktuell_Modell.Modellname,[-10:app.SF_OCVSchrittweiteEditField.Value:100],...
                app.actBattery.temp,app.actBattery.qOCV_DCH);
        else
            warndlg('Keine OCV im Export enthalten');
        end
    end
end

% Add MetaData
framework_struct = generate_MetaData(framework_struct,app.actBattery,app.user);
cell_str = [framework_struct.MetaData.electrical.CellName.Text,'(',framework_struct.MetaData.electrical.ModelName.Text,')'];
[~,git_hash] = system('git rev-parse HEAD');
framework_struct.MetaData.electrical.ImportCXML_CommitHash.Text = git_hash(1:40);
app.electrical_models{1,1} = framework_struct;
app.electrical_models{1,2} = ['My' app.ModellAuswahlPopup.Value];

%Aufbau des XMLs
XML_struct.Configuration = struct('MetaData',[],'Options',[],'Observer',[],'ThermalMaterials',[],'CoolingBlocks',[], ...
    'CachedCoolings',[],'CustomDefinitions',[],'RootElement',[]);
XML_struct = generate_singleCell_XML_struct_FittingGUI(app,XML_struct,sorted_hysteresis_state_array,sorted_crate_array);
XML_struct = generate_options_observer_FittingGUI(app,XML_struct);
numCells = 1;
XML_struct = generate_root_element_FittingGUI(app,XML_struct,0,0);
XML_struct.Configuration.CustomDefinitions.(app.electrical_models{2}).ThermalBlock.Attributes.ref = 'MyPrismaticCell';
created_xml = XML_struct;
if app.GUI
    oldfolder = cd('Verification');
else
    oldfolder = cd(app.VerificationPath);
end
current_folder=pwd;
if ~isfolder(fullfile(current_folder,app.BatterieNamePopup.Value))
    mkdir(current_folder,app.BatterieNamePopup.Value);
end
current_folder_bat=fullfile(current_folder,app.BatterieNamePopup.Value);
currentTime=clock;
OutputFolderName=sprintf('%s_%s_%s_%s_%s',num2str(currentTime(1)),num2str(currentTime(2)),num2str(currentTime(3)), ...
    num2str(currentTime(4)),num2str(currentTime(5)));
mkdir(current_folder_bat,OutputFolderName);
current_folder_time = fullfile(current_folder_bat,OutputFolderName);
app.verification_time_paths = current_folder_time;
if app.HysteresisFlag
    [~,hyst_index] = min(abs(sorted_hysteresis_state_array));
else
    hyst_index = 1;
end
if app.CRateFlag
    [~,crate_index] = min(abs(sorted_crate_array{hyst_index}));
else
    crate_index = 1;
end
if app.ParallelComputingToolboxinstalledCheckBox.Value
    for t = 1:length(sorted_temperature_array{hyst_index}{crate_index})
        %         mkdir(current_folder_time,num2str(sorted_temperature_array(t)));
        %         current_folder_temp=strcat(current_folder_time,'\',num2str(sorted_temperature_array(t)));
        %         cd(current_folder_temp)
        cd(current_folder_time)
        pathname = [cd,'\'];
        app.currently_used_folder_paths{t} = pathname;
        for soc= 1:length(considered_SOC_for_verification{hyst_index}{crate_index}{t})
            %     [filename,pathname] = uiputfile('*.xml','Speichere XML','newSimulation.xml');
            filename = [strrep(num2str(app.BatterieNamePopup.Value),'.','_'), '_',num2str(round(sorted_temperature_array{hyst_index}{crate_index}(t))),'_',num2str(round(considered_SOC_for_verification{hyst_index}{crate_index}{t}(soc)))];
            XML_struct.Configuration.Options.AirTemperature.Text = num2str(sorted_temperature_array{hyst_index}{crate_index}(t));
            XML_struct.Configuration.CustomDefinitions.(app.electrical_models{2}).ThermalState.InitialTemperature.Text = num2str(sorted_temperature_array{hyst_index}{crate_index}(t));
            XML_struct.Configuration.CustomDefinitions.MyPrismaticCell.InitialTemperature.Text = num2str(sorted_temperature_array{hyst_index}{crate_index}(t));
            XML_struct.Configuration.CustomDefinitions.(app.electrical_models{2}).Soc.InitialSoc.Text = num2str(considered_SOC_for_verification{hyst_index}{crate_index}{t}(soc));
            XML_struct.Configuration.Options.SampleRate.Text = num2str(app.SampleTimeEditField.Value);
            XML_struct.Configuration.Options.StepTime.Text = num2str(0.1);
            XML_struct.Configuration.Observer.Electrical.Filter2.Filename.Text = ['SimuResult_',strrep(num2str(app.BatterieNamePopup.Value),'.','_'),'_',num2str(round(sorted_temperature_array{hyst_index}{crate_index}(t))),'_',num2str(round(considered_SOC_for_verification{hyst_index}{crate_index}{t}(soc))),'.mat'];
            app.verification_XML_structs{t,soc} = filename;
            save_xml_Struct(XML_struct,[pathname,filename]);
            app.verification_number_of_simulations = app.verification_number_of_simulations +1;
            app.verification_number_to_idx_conversion{app.verification_number_of_simulations} = [t,soc];
        end
        cd(current_folder_time)
    end
else
    for t = 1:length(sorted_temperature_array{hyst_index}{crate_index})
        cd(current_folder_time)
        %         mkdir(current_folder_time,num2str(sorted_temperature_array(t)));
        %         current_folder_temp=strcat(current_folder_time,'\',num2str(sorted_temperature_array(t)));
        %     [filename,pathname] = uiputfile('*.xml','Speichere XML','newSimulation.xml');
        filename = [strrep(num2str(app.BatterieNamePopup.Value),'.','_'), '_',num2str(round(sorted_temperature_array{hyst_index}{crate_index}(t)))];
        %         cd(current_folder_temp)
        pathname = [cd,'\'];
        app.currently_used_folder_paths{t} = pathname;
        XML_struct.Configuration.Options.AirTemperature.Text = num2str(sorted_temperature_array{hyst_index}{crate_index}(t));
        XML_struct.Configuration.CustomDefinitions.(app.electrical_models{2}).ThermalState.InitialTemperature.Text = num2str(sorted_temperature_array{hyst_index}{crate_index}(t));
        XML_struct.Configuration.CustomDefinitions.MyPrismaticCell.InitialTemperature.Text = num2str(sorted_temperature_array{hyst_index}{crate_index}(t));
        XML_struct.Configuration.Options.SampleRate.Text = num2str(app.SampleTimeEditField.Value);
        XML_struct.Configuration.Options.StepTime.Text = num2str(0.1);
        app.verification_XML_structs{t} = XML_struct;
        save_xml_Struct(XML_struct,[pathname,filename]);
        cd(current_folder_time)
        app.verification_number_of_simulations = app.verification_number_of_simulations + 1;
    end
end
cd(oldfolder)
end

