function app = fcn_ZeitFitConfigurationSelectionChanged(app, event)
%Initialize Table to configure custom ranges of values for fitting
%parameters in time domain
if app.UseCustomRangesCheckBox.Value
    global DRT_GUI
    if strcmp(app.ZeitFitConfiguration.SelectedTab.Title,'Permissble Range')
        ParameterSelectionTable = app.ZeitbereichsfittingParameterAuswahlTable.Data;
        RangeDefinitionTable = app.CustomRangesTable.Data;
        RangeDefinitionTable = [];
        [row,col] = size(ParameterSelectionTable);
        if ~isempty(RangeDefinitionTable)
            [row_range,~] = size(RangeDefinitionTable);
        else
            row_range = 0;
        end
        used_parameter_counter = row_range + 1;
        for n = 1:row
            if ParameterSelectionTable{n,3}
                for m = 1:length(DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{1,n}.inputs)
                    if ~isempty(RangeDefinitionTable)
                        if isempty(find(contains(RangeDefinitionTable(:,1),DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{1,n}.inputs{1,m}),1))
                            RangeDefinitionTable{used_parameter_counter,1} = DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{1,n}.inputs{1,m};
                            RangeDefinitionTable{used_parameter_counter,2} = 0;
                            RangeDefinitionTable{used_parameter_counter,3} = Inf;
                            used_parameter_counter = used_parameter_counter + 1;
                        end
                    else
                        RangeDefinitionTable{used_parameter_counter,1} = DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{1,n}.inputs{1,m};
                        RangeDefinitionTable{used_parameter_counter,2} = 0;
                        RangeDefinitionTable{used_parameter_counter,3} = Inf;
                        used_parameter_counter = used_parameter_counter + 1;
                    end
                end
            end
        end
        if app.GUI
            set(app.CustomRangesTable,'Data',RangeDefinitionTable);
            set(app.CustomRangesTable,'ColumnEditable',[false, true, true]);
        else
            app.CustomRangesTable.Data = RangeDefinitionTable;
            app.CustomRangesTable.ColumnEditable = [false, true, true];
        end
    end
end
end

