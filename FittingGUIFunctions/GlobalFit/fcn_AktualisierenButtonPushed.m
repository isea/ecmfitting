function app = fcn_AktualisierenButtonPushed(app, event)
global DRT_GUI
for n =1:length(DRT_GUI.Fit.aktuell_Modell.ModellCell{8})
    strcontent = [];
    if  ~strcmp( DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n}.Zfun_LF,'0')
        strcontent = strcat(strcontent ,{' '} , 'LF');
    end
    if ~strcmp( DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n}.Zfun_MF,'0')
        strcontent = strcat(strcontent , {' '} , 'MF');
    end
    if ~strcmp( DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n}.Zfun_HF,'0')
        strcontent = strcat(strcontent ,{' '}, 'HF');
        
    end
    TableData(n,1) = {DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n}.Funktionsname};
    TableData(n,2) = {[strcontent{1}]};
    TableData(n,3 )= {[false]};
end
if app.GUI
    set(app.ZeitbereichsfittingParameterAuswahlTable,'Data',TableData);
    set(app.ZeitbereichsfittingParameterAuswahlTable,'ColumnEditable',true);
else
    app.ZeitbereichsfittingParameterAuswahlTable.Data = TableData;
    app.ZeitbereichsfittingParameterAuswahlTable.ColumnEditable = true;
end
end
    


