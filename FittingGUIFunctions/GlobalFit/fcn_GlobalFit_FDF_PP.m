function [app,saving_struct] = fcn_GlobalFit_FDF_PP(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
    sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array )
global DRT_GUI
if ~app.FrequenzbereichsfittingCheckBox.Value
    %load the last of the saved savings structs in the GlobalData
    %folder
    GlobalData_folder=pwd;
    GlobalData_folder=strcat(GlobalData_folder,'\GlobalData');
    old_path = cd(GlobalData_folder);
    Savings_list = dir([app.BatterieNamePopup.Value,'*.mat']);
    cd(old_path)
    if isempty(Savings_list)
        msgbox('No data are avaiable in the GlobalData Folder for this battery')
        return
    end
    for n = 1:length(Savings_list)
        Date_numbers(n) = Savings_list(n).datenum;
    end
    [~,latest_data_set_idx] = max(Date_numbers);
    load([Savings_list(latest_data_set_idx).folder, '\' ,Savings_list(latest_data_set_idx).name])
    app.optimal_parameters = saving_struct.optimal_parameters;
    saving_struct.RMSE=app.RMSE;
    app.Boundaries = saving_struct.Boundaries;
    app.Boundaries_Default = saving_struct.Boundaries_Default;
    app.parameters = saving_struct.parameters;
    app.optimal_parameters = saving_struct.optimal_parameters;
    sorted_SOC_array = saving_struct.SOC_states;
    sorted_temperature_array = saving_struct.Temperature_states;
    sorted_crate_array = saving_struct.Crate_states;
    sorted_hysteresis_state_array = saving_struct.Hysteresis_states;
else
    saving_struct.SOC_states = sorted_SOC_array;
    saving_struct.Temperature_states = sorted_temperature_array;
    saving_struct.Crate_states = sorted_crate_array;
    saving_struct.Hysteresis_states = sorted_hysteresis_state_array;
    saving_struct.optimal_parameters=app.optimal_parameters;
end

max_soc_count = 0;
mad_variance_band = app.MADToleranceEditField.Value;
parameter_count= length(saving_struct.optimal_parameters{1}{1}{1}{1});
for hyst_index = 1:length(saving_struct.Hysteresis_states)
    Crate_count(hyst_index) =  length(saving_struct.Crate_states{hyst_index});
    for crate_index = 1:length(saving_struct.Crate_states{hyst_index})
        temp_count{hyst_index}(crate_index) = length(saving_struct.Temperature_states{hyst_index}{crate_index});
        for temp_index1 = 1:length(saving_struct.Temperature_states{hyst_index}{crate_index})
            SOC_count{hyst_index}{crate_index}(temp_index1) = length(saving_struct.SOC_states{hyst_index}{crate_index}{temp_index1});
            if SOC_count{hyst_index}{crate_index}(temp_index1) > max_soc_count
                %maximal amount of measured SOC states
                max_soc_count = SOC_count{hyst_index}{crate_index}(temp_index1);
            end
        end
    end
end

%initilaize data structures to save states that have to be re-fittet
for hyst_index = 1:length(saving_struct.Hysteresis_states)
    for crate_index = 1:length(saving_struct.Crate_states{hyst_index})
        for temp_index1=1:temp_count{hyst_index}(crate_index)
            change_boundaries{temp_index1}=[];
            final_counter{hyst_index}{crate_index}{temp_index1} = 1;
            final_change_points{hyst_index}{crate_index}{temp_index1}=[];
        end
    end
end
%determine order of parameters

for n = 1:length(DRT_GUI.Fit.aktuell_Modell.ModellCell{1, 8})
    esb_parameter_idx{n} = DRT_GUI.Fit.aktuell_Modell.ModellCell{1,8}{1,n}.ParameterIndexes;
end
parameter_fitted = zeros(1,parameter_count);
for esb_element_counter = 1:length(esb_parameter_idx)   
%     for hyst_index = 1:length(saving_struct.Hysteresis_states)
%         for crate_index = 1:length(saving_struct.Crate_states{hyst_index})
%             for temp_index1 = 1:temp_count{hyst_index}(crate_index)
%                 final_change_points{hyst_index}{crate_index}{temp_index1}=[];
%                 final_counter{hyst_index}{crate_index}{temp_index1} = 1;
%                 final_change_points{hyst_index}{crate_index}{temp_index1}=[];
%             end
%         end
%     end
    for p = esb_parameter_idx{esb_element_counter}
        for hyst_index = 1:length(saving_struct.Hysteresis_states)
            for crate_index = 1:length(saving_struct.Crate_states{hyst_index})
                if parameter_fitted(p) == 0
                    considered_values = NaN(temp_count{hyst_index}(crate_index),max_soc_count);
                    criterium_vector = NaN(temp_count{hyst_index}(crate_index),max_soc_count);
                    fcn_CancelCheck(app);
                    for t=1:temp_count{hyst_index}(crate_index)
                        %load data
                        change_boundaries{t}=[];
                        for s=1:length(saving_struct.SOC_states{hyst_index}{crate_index}{t})
                            considered_values(t,s) = saving_struct.optimal_parameters{hyst_index}{crate_index}{t}{s}(p);
                        end
                        start{t}=[];
                        len{t}=[];
                        val{t}=[];
                        rn{t}=[];
                        %calculate deviations
                        considered_values(t,1:length(saving_struct.SOC_states{hyst_index}{crate_index}{t}))=transpose(considered_values(t,~isnan(considered_values(t,:))));
                        criterium_vector(t,1:length(saving_struct.SOC_states{hyst_index}{crate_index}{t})) = abs(considered_values(t,1:length(saving_struct.SOC_states{hyst_index}{crate_index}{t})) - median(considered_values(t,1:length(saving_struct.SOC_states{hyst_index}{crate_index}{t})))) / mad(considered_values(t,1:length(saving_struct.SOC_states{hyst_index}{crate_index}{t})),1);
                        median_considered_values(t) = median(considered_values(t,1:length(saving_struct.SOC_states{hyst_index}{crate_index}{t})));
                        median_deviation_of_median(t) = mad(considered_values(t,1:length(saving_struct.SOC_states{hyst_index}{crate_index}{t})),1);
                        change_necessary_index{t} = find(criterium_vector(t,1:length(saving_struct.SOC_states{hyst_index}{crate_index}{t}))>mad_variance_band);
                        postprocessing_done{t}=zeros(size(change_necessary_index{t}));
                        index_diff_vec{t} = diff(change_necessary_index{t});
                    end
                    for t1=1:temp_count{hyst_index}(crate_index)
                        for s1=1:length(saving_struct.SOC_states{hyst_index}{crate_index}{t1})
                            app.Boundaries{hyst_index}{crate_index}{t1}{s1}{p,1} = app.optimal_parameters{hyst_index}{crate_index}{t1}{s1}(p)-median_deviation_of_median(t1);
                            app.Boundaries{hyst_index}{crate_index}{t1}{s1}{p,2} = app.optimal_parameters{hyst_index}{crate_index}{t1}{s1}(p)+median_deviation_of_median(t1);
                        end
                    end
                    %Maximal amount of discovered deviations for the considered temperatures
                    max_deviations_count = 0;
                    for t=1:temp_count{hyst_index}(crate_index)
                        max_deviations_count = max(max_deviations_count,length(change_necessary_index{t}));
                        if ~isempty(find(criterium_vector(t,1:length(saving_struct.SOC_states{hyst_index}{crate_index}{t}))>mad_variance_band))
                            %find arrays within the difference vector
                            if ~isempty(index_diff_vec{t})
                                [start{t}, val{t}, len{t}, rn{t}] = fcn_RunsCount(index_diff_vec{t});
                            else
                                if ~isempty(change_necessary_index{t})
                                    start{t} = 1;
                                    len{t} = 1;
                                    val{t} = 54;
                                end
                            end
                        end
                    end
                    % indentify chains of SOCs that should be re-fitted
                    for t = 1:temp_count{hyst_index}(crate_index)
                        index_deviation_chain{t} = [];
                        start_index_deviation_chain{t} = [];
                        start_isolated_deviation_start{t} = [];
                        isolated_len{t} = [];
                        chain_len{t} = [];
                        if ~isempty(find(criterium_vector(t,1:length(saving_struct.SOC_states{hyst_index}{crate_index}{t}))>mad_variance_band))
                            %index within start that marks all chains of directly successive deviations that should be re-fitted
                            index_deviation_chain{t} = find(val{t} == 1);
                            for n = 1:length(index_deviation_chain{t})
                                %index in index_diff_vec where the chain starts
                                start_index_deviation_chain{t}(n) = start{t}(index_deviation_chain{t}(n));
                                chain_len{t}(n) = len{t}(index_deviation_chain{t}(n));
                            end
                            isolated_count  = 1;
                            for n7 = 1:length(change_necessary_index{t})
                                n9 = 0;
                                if ~isempty(start_index_deviation_chain{t})
                                    for n8 = 1:length(start_index_deviation_chain{t})
                                        if ~(change_necessary_index{t}(n7) >=  change_necessary_index{t}(start_index_deviation_chain{t}(n8)) && change_necessary_index{t}(n7) <=  change_necessary_index{t}(start_index_deviation_chain{t}(n8) + chain_len{t}(n8)))
                                            n9 = n9 + 1;
                                        end
                                    end
                                    if  n9 == length(start_index_deviation_chain{t})
                                        start_isolated_deviation_start{t}(isolated_count) = n7;
                                        isolated_len{t}(isolated_count) = 1;
                                        isolated_count  = isolated_count + 1;
                                    end
                                else
                                    start_isolated_deviation_start{t}(isolated_count) = n7;
                                    isolated_len{t}(isolated_count) = 1;
                                    isolated_count  = isolated_count + 1;
                                end
                            end
                            sorted_out_chains{t} = [];
                            count{t} = 1;
                        end
                        %if changes are necessary at the edges of the considered
                        %SOC domain, ignore them
                        change_boundaries{t} = [];
                        if ~isempty(start_isolated_deviation_start{t})
                            if change_necessary_index{t}(start_isolated_deviation_start{t}(1)) == 1
                                start_isolated_deviation_start{t}(1) = [];
                            end
                            if ~isempty(start_isolated_deviation_start{t})
                                if change_necessary_index{t}(start_isolated_deviation_start{t}(end)) == length(sorted_SOC_array{hyst_index}{crate_index}{t})
                                    start_isolated_deviation_start{t}(end) = [];
                                end
                            end
                        end
                        %if a chain of deviations starts or ends at the edges,
                        %check whether they deviate in the same direction, if yes,
                        %ignore them
                        if ~isempty(start_index_deviation_chain{t})
                            if change_necessary_index{t}(start_index_deviation_chain{t}(1)) == 1
                                sign_determination_array = [];
                                for n = 0:chain_len{t}(1)
                                    sign_determination_array(n+1) = saving_struct.optimal_parameters{hyst_index}{crate_index}{t}{change_necessary_index{t}(start_index_deviation_chain{t}(1) + n)}(p)  - median_considered_values(t) ;
                                end
                                if ~any(diff(sign(sign_determination_array(sign_determination_array~=0))))
                                    sorted_out_chains{t}(length(sorted_out_chains{t})+1) = start_index_deviation_chain{t}(1);
                                end
                            end
                            if ~isempty(start_index_deviation_chain{t})
                                if change_necessary_index{t}(start_index_deviation_chain{t}(end) + chain_len{t}(end)) == length(sorted_SOC_array{hyst_index}{crate_index}{t})
                                    sign_determination_array = [];
                                    for n = 0:chain_len{t}(1)
                                        sign_determination_array(n+1) = saving_struct.optimal_parameters{hyst_index}{crate_index}{t}{change_necessary_index{t}(start_index_deviation_chain{t}(1) + n)}(p)  - median_considered_values(t) ;
                                    end
                                    if ~any(diff(sign(sign_determination_array(sign_determination_array~=0))))
                                        sorted_out_chains{t}(length(sorted_out_chains{t})+1) = start_index_deviation_chain{t}(end);
                                    end
                                end
                            end
                        end
                    end
                    % adjust boundaries for the considered SOCs and re-fit the considered parameter
                    n1 = 1;
                    offset = 0;
                    for t1 = 1:temp_count{hyst_index}(crate_index)
                        n1 = 0;
                        if ~isempty(start_isolated_deviation_start{t1})
                            %singled out deviations get new boundaries
                            for n1 = 1:length(start_isolated_deviation_start{t1})
                                if isolated_len{t1}(n1) == 1
                                    if start_isolated_deviation_start{t1}(n1) == 1
                                        change_boundaries{t1}(n1+offset) = change_necessary_index{t1}(start_isolated_deviation_start{t1}(n1));
                                    else
                                        change_boundaries{t1}(n1+offset) = change_necessary_index{t1}(start_isolated_deviation_start{t1}(n1));
                                    end
                                    %check if deviation is at lowest or highest SOC
                                    if change_boundaries{t1}(n1+offset) == 1
                                        app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,1} = app.optimal_parameters{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)+1}(p)-median_deviation_of_median(t1);
                                        app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,2} = app.optimal_parameters{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)+1}(p)+ median_deviation_of_median(t1);
                                    elseif change_boundaries{t1}(n1+offset) == length(sorted_SOC_array{hyst_index}{crate_index}{t1})
                                        app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,1} = app.optimal_parameters{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)-1}(p)-median_deviation_of_median(t1);
                                        app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,2} = app.optimal_parameters{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)-1}(p)+ median_deviation_of_median(t1);
                                    else
                                        app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,1} = min(app.optimal_parameters{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)-1}(p),app.optimal_parameters{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)+1}(p))-median_deviation_of_median(t1);
                                        app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,2} = max(app.optimal_parameters{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)-1}(p),app.optimal_parameters{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)+1}(p))+median_deviation_of_median(t1);
                                    end
                                    
                                    if app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,1}  < 0
                                        app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,1} = 0;
                                    end
                                    if app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,2}  < 0
                                        if app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,1}  > 0
                                            app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,2} = 2 * app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,1};
                                        else
                                            app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,2} = app.ParamTable.Data{p,5};
                                        end
                                    end
                                end
                            end
                        end
                        n1 = length(change_boundaries{t1}) + 1;
                        %non matched chains of deviations
                        for n2 = 1:length(start_index_deviation_chain{t1})
                            if isempty(find(change_necessary_index{t1}(start_index_deviation_chain{t1}(n2))==sorted_out_chains{t1}))
                                if ~isempty(start_index_deviation_chain{t1})
                                    for n3= 1:length(n2:n2+chain_len{t1}(n2))
                                        if start_index_deviation_chain{t1}(n2) == 1
                                            change_boundaries{t1}(n1+offset) = change_necessary_index{t1}(start_index_deviation_chain{t1}(n2)+(n3-1)) ;
                                        else
                                            change_boundaries{t1}(n1+offset) = change_necessary_index{t1}(start_index_deviation_chain{t1}(n2) +(n3-1)) ;
                                            %+ ~isempty(find(start_index_deviation_chain{t1}(n2)==length(index_diff_vec{t1})))
                                        end
                                        if change_necessary_index{t1}(start_index_deviation_chain{t1}(n2)) == 1
                                            app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,1} = median_considered_values(t1)-median_deviation_of_median(t1);
                                            app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,2} = median_considered_values(t1)+median_deviation_of_median(t1);
                                        elseif change_necessary_index{t1}(start_index_deviation_chain{t1}(n2)) + chain_len{t1}(n2) == length(sorted_SOC_array{hyst_index}{crate_index}{t1})
                                            app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,1} = median_considered_values(t1)-median_deviation_of_median(t1);
                                            app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,2} = median_considered_values(t1)+median_deviation_of_median(t1);
                                        else
                                            app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,1} = min(app.optimal_parameters{hyst_index}{crate_index}{t1}{change_necessary_index{t1}(start_index_deviation_chain{t1}(n2))-1}(p),app.optimal_parameters{hyst_index}{crate_index}{t1}{change_necessary_index{t1}(start_index_deviation_chain{t1}(n2))+chain_len{t1}(n2)+1}(p))-median_deviation_of_median(t1);
                                            app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,2} = max(app.optimal_parameters{hyst_index}{crate_index}{t1}{change_necessary_index{t1}(start_index_deviation_chain{t1}(n2))-1}(p),app.optimal_parameters{hyst_index}{crate_index}{t1}{change_necessary_index{t1}(start_index_deviation_chain{t1}(n2))+chain_len{t1}(n2)+1}(p))+median_deviation_of_median(t1);
                                        end
                                        if app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,1}  < 0
                                            app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,1} = 0;
                                        end
                                        if app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,2}  < 0
                                            if app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,1}  > 0
                                                app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,2} = 2 * app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,1};
                                            else
                                                app.Boundaries{hyst_index}{crate_index}{t1}{change_boundaries{t1}(n1+offset)}{p,2} = app.ParamTable.Data{p,5};
                                            end
                                        end
                                        n1 = n1 +1;
                                    end
                                end
                            end
                        end
                    end
                    %save all tupels of temperature and SOC that have to be refittet
                    for t1 = 1:temp_count{hyst_index}(crate_index)
                        if ~isempty(change_boundaries{t1})
                            for n6 = 1:length(change_boundaries{t1})
                                if isempty(find(change_boundaries{t1}(n6)==final_change_points{hyst_index}{crate_index}{t1}))
                                    final_change_points{hyst_index}{crate_index}{t1}(final_counter{hyst_index}{crate_index}{t1}) = change_boundaries{t1}(n6);
                                    final_counter{hyst_index}{crate_index}{t1} = final_counter{hyst_index}{crate_index}{t1} + 1;
                                end
                            end
                        end
                    end
                end
                fcn_CancelCheck(app);
            end
        end
        parameter_fitted(p) = 1;
    end
end    
    %re-fit parameters
    for hyst_index = 1:length(saving_struct.Hysteresis_states)
        if app.HysteresisFlag
            index=hysteresis_state_index_rearrangement_array(hyst_index);
            app.ZustandPopup_2.Value=app.ZustandPopup_2.Items{1+index};
            app = fcn_ZustandPopup(app, event);
            app.current_hyst_index = hyst_index;
        end
        app.current_crate_index = Crate_index(hyst_index);
        for crate_index = 1:length(saving_struct.Crate_states{hyst_index})
            if app.CRateFlag
                index=crate_index_rearrangement_array{hyst_index}(crate_index);
                app.ZustandPopup.Value=app.ZustandPopup.Items{1+index};
                app = fcn_ZustandPopup(app, event);
                app.current_crate_index = crate_index;
            end
            app.current_temp_index = temp_index{hyst_index}(crate_index);
            for t1 = 1:temp_count{hyst_index}(crate_index)
                index1=temperature_index_rearrangement_array{hyst_index}{crate_index}(t1);
                app.current_temp_index=t1;
                if ~strcmp(app.startingtemperatureDropDown.Value,app.startingtemperatureDropDown.Items{1+index1})
                    app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1+index1};
                    app.TemperaturPopup.Value=app.startingtemperatureDropDown.Value;
                    app = fcn_TemperaturPopup(app, event);
                end
                if ~isempty(final_change_points{hyst_index}{crate_index}{t1})
                    for n5 = 1:length(final_change_points{hyst_index}{crate_index}{t1})
                        index2=SOC_index_rearrangement_array{hyst_index}{crate_index}{t1}(final_change_points{hyst_index}{crate_index}{t1}(n5));
                        if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                            app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                            app.SOCPopup.Value=app.startingSOCDropDown.Value;
                            app = fcn_SOCPopup(app, event);
                            if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                                app.ModellAuswahlPopup.Value=Modell;
                                app = fcn_ModellAuswahlPopup(app);
                                app = fcn_BatterieNamePopup(app);
                            end
                        end
                        app.current_SOC_index = final_change_points{hyst_index}{crate_index}{t1}(n5);
                        app = fcn_PostprocessingFit(app,event);
                        fcn_CancelCheck(app);
                    end
                end
            end
        end
    end    

end

