function app = fcn_GlobalFit_Verification(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
    sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array,max_length_crate,max_length_temp)
global DRT_GUI
FarbenLaden()
autofit_path = cd;
% decide which tupels of temperature and SOC are used
for hyst_index = 1:length(sorted_hysteresis_state_array)
    for crate_index = 1:length(sorted_crate_array{hyst_index})
        for n = 1:length(sorted_temperature_array{hyst_index}{crate_index})
            [~,twenty_idx] = min(abs(sorted_SOC_array{hyst_index}{crate_index}{n}-20));
            [~,fifty_idx] = min(abs(sorted_SOC_array{hyst_index}{crate_index}{n}-50));
            [~,eighty_idx] = min(abs(sorted_SOC_array{hyst_index}{crate_index}{n}-80));
            considered_SOC_for_verification{hyst_index}{crate_index}{n} = sorted_SOC_array{hyst_index}{crate_index}{n}(unique([twenty_idx,fifty_idx,eighty_idx]));
            considered_SOC_for_verification{hyst_index}{crate_index}{n} = [considered_SOC_for_verification{hyst_index}{crate_index}{n} , 100];
        end
    end
end

veri_max_length=0;
for hyst_index = 1:length(sorted_hysteresis_state_array)
    for crate_index = 1:length(sorted_crate_array{hyst_index})
        for n=1:length(sorted_temperature_array{hyst_index}{crate_index})
            if length(considered_SOC_for_verification{hyst_index}{crate_index}{n})>veri_max_length
                veri_max_length=length(considered_SOC_for_verification{hyst_index}{crate_index}{n});
            end
        end
    end
end

app.verification_data_files = cell(length(sorted_hysteresis_state_array),max_length_crate,max_length_temp,veri_max_length);
app.verification_result_files = cell(length(sorted_hysteresis_state_array),max_length_crate,max_length_temp,veri_max_length);
app.verification_max_voltage_difference = cell(length(sorted_hysteresis_state_array),max_length_crate,max_length_temp,veri_max_length);
app.verification_figures = cell(length(sorted_hysteresis_state_array),max_length_crate,max_length_temp,veri_max_length);
app.verification_figures_omit_median = cell(length(sorted_hysteresis_state_array),max_length_crate,max_length_temp,veri_max_length);
app.verification_XML_names = cell(length(sorted_hysteresis_state_array),max_length_crate,max_length_temp,veri_max_length);
app.verification_number_of_simulations = 0;
if app.ParallelComputingToolboxinstalledCheckBox.Value
    app.verification_XML_structs = cell(length(sorted_hysteresis_state_array),max_length_crate,max_length_temp,veri_max_length);
end
Modell = app.GlobalFitUsedModelDropDown.Value;

% build xmls
app = fcn_XMLCreationForVerification(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,considered_SOC_for_verification);

% build current profile files
if app.HysteresisFlag
    [~,hyst_index] = min(abs(sorted_hysteresis_state_array));
else
    hyst_index = 1;
end
verification_hysteresis_idx = hyst_index;
if app.CRateFlag
    [~,crate_index] = min(abs(sorted_crate_array{hyst_index}));
else
    crate_index = 1;
end
verification_crate_idx = crate_index;

% for hyst_index = 1:length(sorted_hysteresis_state_array)
%     if app.HysteresisFlag
%         index=hysteresis_state_index_rearrangement_array(hyst_index);
%         app.ZustandPopup_2.Value=app.ZustandPopup_2.Items{1+index};
%         app = fcn_ZustandPopup(app, event);        
%     end
%     for crate_index = 1:length(sorted_crate_array{hyst_index})
%         if app.CRateFlag
%             index=crate_index_rearrangement_array{hyst_index}(crate_index);
%             app.ZustandPopup.Value=app.ZustandPopup.Items{1+index};
%             app = fcn_ZustandPopup(app, event);            
%         end
for verification_temperature_idx = 1:length(sorted_temperature_array{hyst_index}{crate_index})
    index1=temperature_index_rearrangement_array{hyst_index}{crate_index}(verification_temperature_idx);
    app.current_temp_index=verification_temperature_idx;
    if ~strcmp(app.startingtemperatureDropDown.Value,app.startingtemperatureDropDown.Items{1+index1})
        app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1+index1};
        app.TemperaturPopup.Value=app.startingtemperatureDropDown.Value;
        app = fcn_TemperaturPopup(app, event);
    end
    for verification_SOC_idx = 1:length(considered_SOC_for_verification{hyst_index}{crate_index}{verification_temperature_idx})-1
        index2=SOC_index_rearrangement_array{hyst_index}{crate_index}{verification_temperature_idx}(find(sorted_SOC_array{hyst_index}{crate_index}{verification_temperature_idx} == considered_SOC_for_verification{hyst_index}{crate_index}{verification_temperature_idx}(verification_SOC_idx)));
        if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
            app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
            app.SOCPopup.Value=app.startingSOCDropDown.Value;
            app = fcn_SOCPopup(app, event);
            if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                app.ModellAuswahlPopup.Value=Modell;
                app = fcn_ModellAuswahlPopup(app);
                app = fcn_BatterieNamePopup(app);
            end
        end
        fcn_CancelCheck(app);
        %read chosen data into array named digab and convert it
        %into the usual diga file
        digab.daten.Programmdauer = [];
        digab.daten.Strom = [];
        digab.daten.Spannung = [];
        %chosen data set is used
        if app.PulsesButtonForVerification.Value
            if isfield(DRT_GUI.Messdaten,'pulse')
                if ~isempty(DRT_GUI.Messdaten.pulse.spannung) && ~isempty(DRT_GUI.Messdaten.pulse.strom) && ~isempty(DRT_GUI.Messdaten.pulse.zeit)
                    digab.daten.Programmdauer = DRT_GUI.Messdaten.pulse.zeit;
                    digab.daten.Strom = DRT_GUI.Messdaten.pulse.strom;
                    digab.daten.Spannung = DRT_GUI.Messdaten.pulse.spannung;
                end
            elseif isfield(DRT_GUI.Messdaten,'nach_relax')
                if ~isempty(DRT_GUI.Messdaten.nach_relax.spannung) && ~isempty(DRT_GUI.Messdaten.nach_relax.strom) && ~isempty(DRT_GUI.Messdaten.nach_relax.zeit)
                    digab.daten.Programmdauer = DRT_GUI.Messdaten.nach_relax.zeit;
                    digab.daten.Strom = DRT_GUI.Messdaten.nach_relax.strom;
                    digab.daten.Spannung = DRT_GUI.Messdaten.nach_relax.spannung;
                end
            elseif isfield(DRT_GUI.Messdaten,'vor_relax')
                if ~isempty(DRT_GUI.Messdaten.vor_relax.spannung) && ~isempty(DRT_GUI.Messdaten.vor_relax.strom) && ~isempty(DRT_GUI.Messdaten.vor_relax.zeit)
                    digab.daten.Programmdauer = DRT_GUI.Messdaten.vor_relax.zeit;
                    digab.daten.Strom = DRT_GUI.Messdaten.vor_relax.strom;
                    digab.daten.Spannung = DRT_GUI.Messdaten.vor_relax.spannung;
                end
            else
                digab.daten.Programmdauer = [];
                digab.daten.Strom = [];
                digab.daten.Spannung = [];
            end
        else
            if isfield(DRT_GUI.Messdaten,'relax')
                digab.daten.Programmdauer = DRT_GUI.Messdaten.relax.zeit;
                digab.daten.Strom = DRT_GUI.Messdaten.relax.strom;
                digab.daten.Spannung = DRT_GUI.Messdaten.relax.spannung;
            else
                digab.daten.Programmdauer = [];
                digab.daten.Strom = [];
                digab.daten.Spannung = [];
            end
        end
        diga = [];
        %Filter data for wrong data parts
        [digab.daten.Programmdauer,unique_td_data_index_veri]= unique(digab.daten.Programmdauer);
        digab.daten.Strom = digab.daten.Strom(unique_td_data_index_veri);
        digab.daten.Spannung = digab.daten.Spannung(unique_td_data_index_veri);
        if ~isempty(digab.daten.Spannung) && ~isempty(digab.daten.Strom) && ~isempty(digab.daten.Programmdauer)
            if app.PulsesButtonForVerification.Value
                veri_data_change = 0;
                stromindex = find(abs(digab.daten.Strom)<0.001);
                diff_stromindex = diff(stromindex);
                strom_sprungindex = find(diff_stromindex > 1);
                counter = 1;
                strom_sprungindex_anfang = [];
                strom_sprungindex_ende = [];
                for n = 1:length(strom_sprungindex)
                    if stromindex(strom_sprungindex(n)) ~= length(digab.daten.Strom)
                        strom_sprungindex_anfang(counter) = stromindex(strom_sprungindex(n));
                        strom_sprungindex_ende(counter) = stromindex(strom_sprungindex(n) + 1);
                        counter = counter + 1;
                    end
                end
                start_index = [];
                for n = 1:length(strom_sprungindex_ende)
                    if abs(digab.daten.Programmdauer(strom_sprungindex_ende(n))-digab.daten.Programmdauer(strom_sprungindex_anfang(n)))> 5 &&  abs(digab.daten.Programmdauer(strom_sprungindex_ende(n))-digab.daten.Programmdauer(strom_sprungindex_anfang(n))) < 30
                        start_index = n;
                        break
                    end
                end
                start = [];
                if ~isempty(start_index)
                    if start_index == 1
                        if strom_sprungindex_anfang(1) ==  1
                            start = 1;
                        else
                            start = ceil(abs(strom_sprungindex_anfang(1)-stromindex(1))/2);
                        end
                    else
                        start = ceil(abs(strom_sprungindex_anfang(start_index)-strom_sprungindex_ende(start_index-1))*3/4) + strom_sprungindex_ende(start_index-1);
                    end
                end
                if ~isempty(start)
                    digab.daten.Programmdauer = digab.daten.Programmdauer(start:end) - digab.daten.Programmdauer(start);
                    digab.daten.Strom = digab.daten.Strom(start:end);
                    digab.daten.Spannung = digab.daten.Spannung(start:end);
                else
                    digab.daten.Programmdauer = DRT_GUI.Messdaten.relax.zeit;
                    digab.daten.Strom = DRT_GUI.Messdaten.relax.strom;
                    digab.daten.Spannung = DRT_GUI.Messdaten.relax.spannung;
                    veri_data_change = 1;
                end
                end_idx = find(digab.daten.Strom);
                end_idx = end_idx(end);
                end_idx_time = digab.daten.Programmdauer(end_idx);
                if end_idx_time < digab.daten.Programmdauer(end)*2/3
                    [~,premature_end] = min(abs(digab.daten.Programmdauer - (end_idx_time + (digab.daten.Programmdauer(end)-end_idx_time)/3)));
                    digab.daten.Programmdauer = digab.daten.Programmdauer(1:premature_end);
                    digab.daten.Strom = digab.daten.Strom(1:premature_end);
                    digab.daten.Spannung = digab.daten.Spannung(1:premature_end);
                end
                diga = digab;
            else
                diga = digab;
            end
        end
        %save data in mat file
        if ~isempty(diga)
            if app.HysteresisFlag && app.CRateFlag
                OutputFolderName=sprintf('%s_%s_%s_%s_%s.mat',strrep(num2str(app.BatterieNamePopup.Value),'.','_'),strrep(DRT_GUI.Testparameter.Zustand_2,'.','_'),strrep(DRT_GUI.Testparameter.Zustand,'.','_'),app.TemperaturPopup.Value,num2str(round(str2num(app.SOCPopup.Value))));
            elseif app.HysteresisFlag
                OutputFolderName=sprintf('%s_%s_%s_%s.mat',strrep(num2str(app.BatterieNamePopup.Value),'.','_'),strrep(DRT_GUI.Testparameter.Zustand_2,'.','_'),app.TemperaturPopup.Value,num2str(round(str2num(app.SOCPopup.Value))));
            elseif app.CRateFlag
                OutputFolderName=sprintf('%s_%s_%s_%s.mat',strrep(num2str(app.BatterieNamePopup.Value),'.','_'),strrep(DRT_GUI.Testparameter.Zustand,'.','_'),app.TemperaturPopup.Value,num2str(round(str2num(app.SOCPopup.Value))));
            end
            save(strcat(app.currently_used_folder_paths{verification_temperature_idx},OutputFolderName),'diga','-v7.3')
            app.verification_data_files{verification_hysteresis_idx,verification_crate_idx,verification_temperature_idx,verification_SOC_idx} = OutputFolderName;
        end
    end
    %Creation of 0.5C discharge profile
    load('MonotonyCheckBlankData.mat')
    diga.daten.Strom = diga.daten.Strom * app.actBattery.nom_cap/2;
    if app.HysteresisFlag && app.CRateFlag
        OutputFolderName=sprintf('%s_%s_%s_%s_%s.mat',strrep(num2str(app.BatterieNamePopup.Value),'.','_'),strrep(DRT_GUI.Testparameter.Zustand_2,'.','_'),strrep(DRT_GUI.Testparameter.Zustand,'.','_'),app.TemperaturPopup.Value,num2str(round(100)));
    elseif app.HysteresisFlag
        OutputFolderName=sprintf('%s_%s_%s_%s.mat',strrep(num2str(app.BatterieNamePopup.Value),'.','_'),strrep(DRT_GUI.Testparameter.Zustand_2,'.','_'),app.TemperaturPopup.Value,num2str(round(100)));
    elseif app.CRateFlag
        OutputFolderName=sprintf('%s_%s_%s_%s.mat',strrep(num2str(app.BatterieNamePopup.Value),'.','_'),strrep(DRT_GUI.Testparameter.Zustand,'.','_'),app.TemperaturPopup.Value,num2str(round(100)));
    end
    save(strcat(app.currently_used_folder_paths{verification_temperature_idx},OutputFolderName),'diga')
    app.verification_data_files{verification_hysteresis_idx,verification_crate_idx,verification_temperature_idx,verification_SOC_idx+1} = OutputFolderName;
end
%     end
% end
fcn_CancelCheck(app);
% start of simulations
if app.ParallelComputingToolboxinstalledCheckBox.Value
    
    parfor_verification_data_files = app.verification_data_files;
    parfor_BatterieName = strrep(num2str(app.BatterieNamePopup.Value),'.','_');
    parfor_verification_result_files = app.verification_result_files;
    parfor_electrical_models = app.electrical_models{2};
    parfor_xml_names = cell([1,app.verification_number_of_simulations]);
    parfor_verification_result_files = cell([1,app.verification_number_of_simulations]);
    HysteresisFlag = app.HysteresisFlag;
    CRateFlag = app.CRateFlag;
    myCluster = parcluster('local');
    %             for verification_temperature_idx = 1:length(sorted_temperature_array)
    fcn_CancelCheck(app);
    cd(app.currently_used_folder_paths{verification_temperature_idx})
    GUI = app.GUI;
    thermalelectricalSimulation = app.thermalelectricalSimulation;
    parfor_verification_number_to_idx_conversion = app.verification_number_to_idx_conversion;
    parfor (verification_simulation_idx = 1:(app.verification_number_of_simulations),myCluster.NumWorkers)
        verification_temperature_idx = parfor_verification_number_to_idx_conversion{verification_simulation_idx}(1);
        verification_SOC_idx = parfor_verification_number_to_idx_conversion{verification_simulation_idx}(2);
        current_profile_string = parfor_verification_data_files{verification_hysteresis_idx,verification_crate_idx,verification_temperature_idx,verification_SOC_idx};
        if ~isempty(current_profile_string)
            xml_string = [parfor_BatterieName, '_',num2str(round(sorted_temperature_array{verification_hysteresis_idx}{verification_crate_idx}(verification_temperature_idx))),'_',num2str(round(considered_SOC_for_verification{verification_hysteresis_idx}{verification_crate_idx}{verification_temperature_idx}(verification_SOC_idx))),'.xml'];
            parfor_verification_result_files{verification_simulation_idx} = ['SimuResult_',parfor_BatterieName,'_',num2str(round(sorted_temperature_array{verification_hysteresis_idx}{verification_crate_idx}(verification_temperature_idx))),'_',num2str(round(considered_SOC_for_verification{verification_hysteresis_idx}{verification_crate_idx}{verification_temperature_idx}(verification_SOC_idx))),'_0.mat'];
            if thermalelectricalSimulation
                if GUI
                    system_command_string= ['..\..\..\ISEAFrame\thermalElectricalSimulation.exe ', xml_string,' ', current_profile_string , ' -q'];
                else
                    system_command_string= [char(fullfile(autofit_path, 'ISEAFrame','thermalElectricalSimulation.exe')), ' ', xml_string,' ', current_profile_string , ' -q'];
                end
            else
                if GUI
                    system_command_string= ['..\..\..\ISEAFrame\electricalSimulation.exe ', xml_string,' ', current_profile_string , ' -q'];
                else
                    system_command_string= [char(fullfile(autofit_path, 'ISEAFrame','electricalSimulation.exe')), ' ', xml_string,' ', current_profile_string , ' -q'];
                end
            end
            system(system_command_string)
            parfor_xml_names{verification_simulation_idx} = xml_string;
        end
    end
    cd(autofit_path)    
    for verification_simulation_idx = 1:app.verification_number_of_simulations
        app.verification_result_files{hyst_index}{crate_index} ...
            {app.verification_number_to_idx_conversion{verification_simulation_idx}(1)}{app.verification_number_to_idx_conversion{verification_simulation_idx}(2)} = parfor_verification_result_files{verification_simulation_idx};
        app.verification_XML_names{hyst_index}{crate_index}...
            {app.verification_number_to_idx_conversion{verification_simulation_idx}(1)}{app.verification_number_to_idx_conversion{verification_simulation_idx}(2)} = parfor_xml_names{verification_simulation_idx};
    end
else
    for verification_temperature_idx = 1:length(sorted_temperature_array{hyst_index}{crate_index})
        fcn_CancelCheck(app);
        cd(app.currently_used_folder_paths{verification_temperature_idx})
        for verification_SOC_idx = 1:length(considered_SOC_for_verification{hyst_index}{crate_index}{verification_temperature_idx})
            fcn_CancelCheck(app);
            current_profile_string = app.verification_data_files{verification_hysteresis_idx,verification_crate_idx,verification_temperature_idx,verification_SOC_idx};
            if ~isempty(current_profile_string)
                xml_string = [app.BatterieNamePopup.Value, '_',num2str(round(sorted_temperature_array{hyst_index}{crate_index}(verification_temperature_idx))),'.xml'];
                XML_struct = app.verification_XML_structs{verification_temperature_idx};
                XML_struct.Configuration.CustomDefinitions.(app.electrical_models{2}).Soc.InitialSoc.Text = num2str(round(considered_SOC_for_verification{hyst_index}{crate_index}{verification_temperature_idx}(verification_SOC_idx)));
                XML_struct.Configuration.Observer.Electrical.Filter2.Filename.Text = ['SimuResult_',app.BatterieNamePopup.Value,'_',num2str(round(sorted_temperature_array{hyst_index}{crate_index}(verification_temperature_idx))),'_',num2str(round(considered_SOC_for_verification{hyst_index}{crate_index}{verification_temperature_idx}(verification_SOC_idx))),'.mat'];
                app.verification_result_files{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx} = ['SimuResult_',strrep(num2str(app.BatterieNamePopup.Value),'.','_'),'_',num2str(round(sorted_temperature_array{hyst_index}{crate_index}(verification_temperature_idx))),'_',num2str(round(considered_SOC_for_verification{hyst_index}{crate_index}{verification_temperature_idx}(verification_SOC_idx))),'_0.mat'];
                save_xml_Struct(XML_struct,xml_string);
                if app.thermalelectricalSimulation
                    if app.GUI
                        system_command_string= ['..\..\..\ISEAFrame\thermalElectricalSimulation.exe ', xml_string,' ', current_profile_string , ' -q'];
                    else
                        system_command_string= [char(fullfile(autofit_path, 'ISEAFrame','thermalElectricalSimulation.exe')), ' ', xml_string,' ', current_profile_string , ' -q'];
                    end
                else
                    if app.GUI
                        system_command_string= ['..\..\..\ISEAFrame\electricalSimulation.exe ', xml_string,' ', current_profile_string , ' -q'];
                    else
                        system_command_string= [char(fullfile(autofit_path, 'ISEAFrame','electricalSimulation.exe')), ' ', xml_string,' ', current_profile_string , ' -q'];
                    end
                end
                system(system_command_string)
                app.verification_XML_names{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx} = xml_string;
            end
        end
        cd(autofit_path)
    end     
end
% evaluation of simulations
median_corrected_figures_flag = 0;
max_max_difference = 0;
for verification_temperature_idx = 1:length(sorted_temperature_array{hyst_index}{crate_index})
    cd(app.currently_used_folder_paths{verification_temperature_idx})
    for verification_SOC_idx = 1:length(considered_SOC_for_verification{hyst_index}{crate_index}{verification_temperature_idx})-1
        fcn_CancelCheck(app);
        if ~isempty(app.verification_result_files{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx})
            fig_name = [];
            if exist([app.verification_result_files{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx}]) && exist(app.verification_data_files{verification_hysteresis_idx,verification_crate_idx,verification_temperature_idx,verification_SOC_idx})
                %load data
                sim_data = load([app.verification_result_files{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx}]);
                mes_data = load(app.verification_data_files{hyst_index,crate_index,verification_temperature_idx,verification_SOC_idx});
                if isfield(sim_data,'diga') && isfield(mes_data,'diga')
                    %make plots of simulations and measurements
                    [mes_data.diga.daten.Programmdauer, unique_mes_index]=unique(mes_data.diga.daten.Programmdauer);
                    [sim_data.diga.daten.Programmdauer, unique_sim_index]=unique(sim_data.diga.daten.Programmdauer);
                    Programmdauer=0:0.01:mes_data.diga.daten.Programmdauer(end);
                    MessSp=interp1(mes_data.diga.daten.Programmdauer, mes_data.diga.daten.Spannung(unique_mes_index), Programmdauer);
                    SimSp=interp1(sim_data.diga.daten.Programmdauer, sim_data.diga.daten.SpannungVec(unique_sim_index), Programmdauer);
                    nan_indexes_mes=isnan(MessSp);
                    nan_indexes_sim=isnan(SimSp);
                    voltage_difference=[];
                    counter_diff=1;
                    summed_voltage_error_area = 0;
                    for g=1:length(Programmdauer)
                        if(nan_indexes_mes(g)==0 && nan_indexes_sim(g)==0)
                            voltage_difference(counter_diff)=MessSp(g)-SimSp(g);
                            voltage_difference_time_stamps(counter_diff) = Programmdauer(g);
                            if g < length(Programmdauer)
                                summed_voltage_error_area = summed_voltage_error_area + abs(voltage_difference(counter_diff)) * (Programmdauer(g+1) - Programmdauer(g));
                            end
                            counter_diff=counter_diff+1;
                        end
                    end
                    %                     app.verification_max_voltage_difference{verification_temperature_idx,verification_SOC_idx} = max(abs(voltage_difference));
                    %                     if max(abs(voltage_difference)) > max_max_difference
                    %                         max_max_difference = max(abs(voltage_difference));
                    %                     end
                    app.verification_max_voltage_difference{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx} = summed_voltage_error_area;
                    if summed_voltage_error_area > max_max_difference
                        max_max_difference = summed_voltage_error_area;
                    end
                    f = figure('WindowState','minimized');
                    fig_name = [app.BatterieNamePopup.Value, '_',num2str(sorted_temperature_array{hyst_index}{crate_index}(verification_temperature_idx)),'_',num2str(round(considered_SOC_for_verification{hyst_index}{crate_index}{verification_temperature_idx}(verification_SOC_idx))),'.fig'];
                    plot(Programmdauer,MessSp,'color',RWTHBlau)
                    hold on
                    plot(Programmdauer,SimSp,'color',RWTHRot)
                    grid on
                    xlabel('t / s')
                    ylabel('U / V')
                    legend('measurement','simulation')
                    savefig(f,fig_name)
                    app.verification_figures{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx} = fig_name;
                    close(f)

%                     if abs(median(SimSp,'omitnan')-median(MessSp,'omitnan')) > 0.001
                        median_corrected_figures_flag = 1;
                        median_error = median(MessSp,'omitnan') - median(SimSp,'omitnan');
                        f = figure('WindowState','minimized');
                        fig_name = [app.BatterieNamePopup.Value, '_',num2str(sorted_temperature_array{hyst_index}{crate_index}(verification_temperature_idx)),'_',num2str(round(considered_SOC_for_verification{hyst_index}{crate_index}{verification_temperature_idx}(verification_SOC_idx))),'_median_filtered.fig'];
                        plot(Programmdauer,MessSp,'color',RWTHBlau)
                        hold on
                        plot(Programmdauer,SimSp + median_error,'color',RWTHRot)
                        grid on
                        xlabel('t / s')
                        ylabel('U / V')
                        legend('measurement','simulation with adjusted OCV')
                        savefig(f,fig_name)
                        app.verification_figures_omit_median{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx} = fig_name;
                        close(f)
%                     end
                end
            end
        end
    end
    %Plot monotony test profile
    if ~isempty(app.verification_result_files{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx+1})
        fig_name = [];
        if exist([app.verification_result_files{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx+1}]) && exist(app.verification_data_files{hyst_index,crate_index,verification_temperature_idx,verification_SOC_idx+1})
            %load data
            sim_data = load([app.verification_result_files{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx+1}]);
            if isfield(sim_data,'diga')
                %make plots of simulations and measurements
                f = figure('WindowState','minimized');
                fig_name = [app.BatterieNamePopup.Value, '_',num2str(sorted_temperature_array{hyst_index}{crate_index}(verification_temperature_idx)),'_0.5CDischarge','.fig'];
                plot(sim_data.diga.daten.Programmdauer,sim_data.diga.daten.SpannungVec)
                xlabel('t / s')
                ylabel('U / V')
                legend('simulation')
                savefig(f,fig_name)
                app.verification_figures{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx+1} = fig_name;
                diff_discharge_voltage = diff(sim_data.diga.daten.SpannungVec);
                idx_pos_voltage_slope = find(diff_discharge_voltage>1e-5);
                if isempty(idx_pos_voltage_slope)
                    app.verification_max_voltage_difference{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx+1} = 'monotonic';
                else
                    app.verification_max_voltage_difference{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx+1} = 'not monotonic';
                end
                close(f)
            end
        end
    end
end
cd(app.verification_time_paths)
so = app.verification_max_voltage_difference;
save('Maximal_Differences.mat','so')
app.EditField.Value = max_max_difference;
% Delete all data except for
if ~app.SaveDataCheckBox.Value
    for verification_temperature_idx = 1:length(sorted_temperature_array)
        cd(app.currently_used_folder_paths{verification_temperature_idx})
        delete *.mat
        delete *.xml
    end
end
%move files to proper structure
if app.SaveDataCheckBox.Value
    cd(app.verification_time_paths)
    mkdir(app.verification_time_paths,'Figures')
    mkdir(app.verification_time_paths,'ResultFiles')
    mkdir(app.verification_time_paths,'XMLs')
    mkdir(app.verification_time_paths,'CurrentProfile')
    if median_corrected_figures_flag
        mkdir(app.verification_time_paths,'MedianFreeFigures')
    end
%     for hyst_index = 1:length(sorted_hysteresis_state_array)    
%         for crate_index = 1:length(sorted_crate_array{hyst_index}) 
            for verification_temperature_idx = 1:length(sorted_temperature_array{hyst_index}{crate_index})
                mkdir([app.verification_time_paths,'\Figures'],num2str(sorted_temperature_array{hyst_index}{crate_index}(verification_temperature_idx)))
                mkdir([app.verification_time_paths,'\ResultFiles'],num2str(sorted_temperature_array{hyst_index}{crate_index}(verification_temperature_idx)))
                mkdir([app.verification_time_paths,'\XMLs'],num2str(sorted_temperature_array{hyst_index}{crate_index}(verification_temperature_idx)))
                mkdir([app.verification_time_paths,'\CurrentProfile'],num2str(sorted_temperature_array{hyst_index}{crate_index}(verification_temperature_idx)))
                if median_corrected_figures_flag
                    mkdir([app.verification_time_paths,'\MedianFreeFigures'],num2str(sorted_temperature_array{hyst_index}{crate_index}(verification_temperature_idx)))
                end
            end
%         end
%     end   
    for verification_temperature_idx = 1:length(sorted_temperature_array{hyst_index}{crate_index})
        for verification_SOC_idx = 1:length(considered_SOC_for_verification{hyst_index}{crate_index}{verification_temperature_idx})
            if ~isempty(app.verification_figures{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx}) && exist([app.verification_time_paths,'\',app.verification_figures{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx}])
                movefile([app.verification_time_paths,'\',app.verification_figures{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx}],...
                    [app.verification_time_paths,'\Figures','\',num2str(sorted_temperature_array{hyst_index}{crate_index}(verification_temperature_idx)),'\',app.verification_figures{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx}])
            end
            if ~isempty(app.verification_result_files{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx}) && exist([app.verification_time_paths,'\',app.verification_result_files{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx}])
                movefile([app.verification_time_paths,'\',app.verification_result_files{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx}],...
                    [app.verification_time_paths,'\ResultFiles','\',num2str(sorted_temperature_array{hyst_index}{crate_index}(verification_temperature_idx)),'\',app.verification_result_files{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx}])
            end
            if ~isempty(app.verification_XML_names{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx}) && exist([app.verification_time_paths,'\',app.verification_XML_names{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx}])
                movefile([app.verification_time_paths,'\',app.verification_XML_names{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx}],...
                    [app.verification_time_paths,'\XMLs','\',num2str(sorted_temperature_array{hyst_index}{crate_index}(verification_temperature_idx)),'\',app.verification_XML_names{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx}])
            end
            if ~isempty(app.verification_data_files{hyst_index,crate_index,verification_temperature_idx,verification_SOC_idx}) && exist([app.verification_time_paths,'\',app.verification_data_files{hyst_index,crate_index,verification_temperature_idx,verification_SOC_idx}])
                movefile([app.verification_time_paths,'\',app.verification_data_files{hyst_index,crate_index,verification_temperature_idx,verification_SOC_idx}],...
                    [app.verification_time_paths,'\CurrentProfile','\',num2str(sorted_temperature_array{hyst_index}{crate_index}(verification_temperature_idx)),'\',app.verification_data_files{hyst_index,crate_index,verification_temperature_idx,verification_SOC_idx}])
            end
            if median_corrected_figures_flag && verification_SOC_idx ~= length(considered_SOC_for_verification{hyst_index}{crate_index}{verification_temperature_idx})
                if ~isempty(app.verification_figures_omit_median{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx}) && exist([app.verification_time_paths,'\',app.verification_figures_omit_median{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx}])
                    movefile([app.verification_time_paths,'\',app.verification_figures_omit_median{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx}],...
                        [app.verification_time_paths,'\MedianFreeFigures','\',num2str(sorted_temperature_array{hyst_index}{crate_index}(verification_temperature_idx)),'\',app.verification_figures_omit_median{hyst_index}{crate_index}{verification_temperature_idx}{verification_SOC_idx}])
                end
            end
        end
    end    
else
    cd(app.verification_time_paths)
    mkdir(app.verification_time_paths,'Figures')
    for hyst_index = 1:length(sorted_hysteresis_state_array)    
        for crate_index = 1:length(sorted_crate_array{hyst_index}) 
            for verification_temperature_idx = 1:length(sorted_temperature_array{hyst_index}{crate_index})
                movefile([app.verification_time_paths],[app.verification_time_paths,'\','Figures'])
            end
        end
    end
end
end

