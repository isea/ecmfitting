function app = fcn_PostprocessingFit(app,event)
app.PunkteWegnehmenTextBox.Value = '';
app = fcn_PunkteWegnehmeButton(app, event);
global DRT_GUI;
app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
app = fcn_ModellAuswahlPopup(app);
app = fcn_BatterieNamePopup(app);
if isempty(DRT_GUI) || isempty(cell2mat(strfind(fieldnames(DRT_GUI),'Messdaten')))
    return
end
formula = DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell;
if  isempty(cell2mat(strfind(fieldnames(DRT_GUI.Messdaten),'relax_fft'))) || isempty(DRT_GUI.Messdaten.relax_fft)
    m_w = DRT_GUI.Messdaten.omega(DRT_GUI.Messdaten.aktiv==1);
    m_real = DRT_GUI.Messdaten.Zreal(DRT_GUI.Messdaten.aktiv==1);
    m_imag = DRT_GUI.Messdaten.Zimg(DRT_GUI.Messdaten.aktiv==1);
else
    m_w = [DRT_GUI.Messdaten.omega(DRT_GUI.Messdaten.aktiv==1) ; DRT_GUI.Messdaten.relax_fft.omega(DRT_GUI.Messdaten.relax_fft.aktiv==1)] ; %selbst
    m_real = [DRT_GUI.Messdaten.Zreal(DRT_GUI.Messdaten.aktiv==1); DRT_GUI.Messdaten.relax_fft.Zreal(DRT_GUI.Messdaten.relax_fft.aktiv==1)+DRT_GUI.Messdaten.relax_fft.Zreal_korrektur];%selbst
    m_imag = [DRT_GUI.Messdaten.Zimg(DRT_GUI.Messdaten.aktiv==1); DRT_GUI.Messdaten.relax_fft.Zimg(DRT_GUI.Messdaten.relax_fft.aktiv==1)];%selbst
end
if app.GUI
    TableCell = get(app.ParamTable,'Data');
else
   TableCell = app.ParamTable.Data; 
end
if ~isempty(TableCell)
    TableCell = cell2struct(TableCell',{'Name','Fix','Value','Min','Max','Lim'});
end

% die initialisierte Werte aus Feld bekommen
DRT_GUI.Fit.ParFix = zeros(size(DRT_GUI.Fit.Parameter));
for Par_init_i = 1:length(DRT_GUI.Fit.Parameter)
    if ~isempty(TableCell(Par_init_i).Value)
        DRT_GUI.Fit.Parameter(Par_init_i) = TableCell(Par_init_i).Value;
    else
        DRT_GUI.Fit.Parameter(Par_init_i) = 0;
        TableCell(Par_init_i).Value = 0;
    end
    if ~isempty(TableCell(Par_init_i).Min) && ~TableCell(Par_init_i).Fix
        p_min(Par_init_i)= TableCell(Par_init_i).Min;
    elseif TableCell(Par_init_i).Fix
        DRT_GUI.Fit.ParFix(Par_init_i) = 1 ;
        p_min(Par_init_i) = TableCell(Par_init_i).Value;
    else
        p_min(Par_init_i) = 0;
        TableCell(Par_init_i).Min = 0 ;
    end
    if ~isempty(TableCell(Par_init_i).Min)
        DRT_GUI.Fit.Parameter_min(Par_init_i) = TableCell(Par_init_i).Min;
    else
        DRT_GUI.Fit.Parameter_min(Par_init_i) = 0;
    end
    if ~isempty(TableCell(Par_init_i).Max) && ~TableCell(Par_init_i).Fix
        p_max(Par_init_i)= TableCell(Par_init_i).Max;
    elseif TableCell(Par_init_i).Fix
        p_max(Par_init_i) = TableCell(Par_init_i).Value;
    else
        p_max(Par_init_i) = inf;
        TableCell(Par_init_i).Max = Inf;
    end
    if ~isempty(TableCell(Par_init_i).Max)
        DRT_GUI.Fit.Parameter_max(Par_init_i) = TableCell(Par_init_i).Max;
    else
        DRT_GUI.Fit.Parameter_max(Par_init_i) = inf;
    end
end
p_init = DRT_GUI.Fit.Parameter;
options = optimset('MaxIter',2000000,'MaxFunEvals',2000000,'TolX',1e-10,'TolFun',1e-10);

switch app.GlobalUsedModell
    case 'R2RC'
        %Betrachte nur kapazitives Verhalten
        index=find(-m_imag>0);
        m_w=m_w(index);
        m_real=m_real(index);
        m_imag=m_imag(index);
        
        %Optimization within the calculated boundaries
        temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max=transpose(temporary_variable(:,2));
        p_min=transpose(temporary_variable(:,1));
        if length(p_init)~=length(p_min)
            if length(p_init)>length(p_min)
                p_init=p_init(1:length(p_min));
            elseif length(p_init)<length(p_min)
                p_init=[p_init zeros(1,length(p_min)-length(p_init))];
            end
        end
        for n = 1:length(p_min)
            if p_min(n)<0
                p_min(n) = 0;
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init)
                if p_max(n) - p_min(n) < 0.002
                    if p_init(n) > 0.001
                        p_max(n) = p_init(n) + 0.001;
                        p_min(n) = p_init(n) - 0.001;
                    else
                        p_max(n) = 0.002;
                        p_min(n) = 0;
                    end
                end
            end
        end
        [p_best,fval,exitflag_or,output_or]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min, p_max ,options, formula,app.used_optimization_algorithm);
        app.optimal_parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}=p_best;
    case 'LiIon4'
        index_cabling = find(-m_imag<0);
        index_esb = find(-m_imag>0);
        
        %Seperate the induktiv part resembling the cabling
        m_w_cabling = m_w(index_cabling);
        m_real_cabling = m_real(index_cabling);
        m_imag_cabling = m_imag(index_cabling);
        
        %Seperate the capacitiv part resembling the rest hte actual
        %battery
        m_w_esb = m_w(index_esb);
        m_real_esb = m_real(index_esb);
        m_imag_esb = m_imag(index_esb);
        
        %Seperate this part into hf,mf and lf
        [pks_u,locs_u] = findpeaks(m_imag_esb);
        if length(locs_u)> 1
            [minimum_value, step_index_index] = max(pks_u);
            step_index = locs_u(step_index_index) + 2;
            %                 step_index = locs_u(end) + 7;
        elseif length(locs_u) < 1
            step_index = findchangepts(diff(m_real_esb));
        else
            step_index = locs_u(end) + 2;
        end
        
        m_real_lf = m_real_esb(step_index-1:end);
        m_imag_lf = m_imag_esb(step_index-1:end);
        m_w_lf = m_w_esb(step_index-1:end);
        
        m_w_esb = m_w_esb(1:step_index-2);
        m_real_esb = m_real_esb(1:step_index-2);
        m_imag_esb = m_imag_esb(1:step_index-2);
        
        %Get fittet values from Gui table
        %             p_init_cabling = DRT_GUI.Fit.Parameter(2:4);
        p_init_cabling = p_init(1:4);
        p_init_esb = p_init(1:end-4);
        p_init_esb_compl = p_init;
        p_init_lf = p_init(14:17);
        
        %Fitte den Hochfrequenten Bereich
        formula_cabel='1.*(p(1))+1.*(p(2)./(1+p(2)./((1i.*w).^p(4).*p(3))))' ;
        temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max_cabel=transpose(temporary_variable(1:4,2));
        p_min_cabel=transpose(temporary_variable(1:4,1));
        for n = 1:length(p_min_cabel)
            if p_min_cabel(n)<0
                p_min_cabel(n) = 0;
            end
        end
        [p_best_cabel,fval_cabel,exitflag_cabel,output_cabel]=function_fit_easyfit2(app,m_w_cabling,[m_real_cabling, m_imag_cabling],p_init_cabling,@function_model_all_types2, p_min_cabel, p_max_cabel ,options, formula_cabel);
        
        %Fitte den Mittelfrequenten Bereich
        formula_mf_hf =  '1.*(p(1))+1.*(p(2)./(1+p(2)./((1i.*w).^p(4).*p(3))))+1.*(0)+1.*((p(8).*(p(5)./(1+1i.*w.*p(6)).^p(7))).^0.5.*coth((p(8)./(p(5)./(1+1i.*w.*p(6)).^p(7))).^0.5))+1.*(0)+1.*(0)+1.*((p(8).*(p(9)./(1+1i.*w.*p(10)).^p(11))).^0.5.*coth((p(8)./(p(9)./(1+1i.*w.*p(10)).^p(11))).^0.5))+1.*((p(8).*(Z_parallel((p(9)==0).*10^9+1./((p(10)./p(9)).*(1i.*w)), p(9) + 10* p(12) .* tanh((1i.*w.*p(13)*20).^0.5)./((1i.*w.*p(13)*20).^0.5-tanh((1i.*w.*p(13)*20).^0.5)))-p(9)./(1+1i.*w.*p(10))+p(9)./(1+1i.*w.*p(10)).^p(11))).^0.5.*coth((p(8)./(Z_parallel((p(9)==0).*10^9+1./((p(10)./p(9)).*(1i.*w)), p(9) + 10* p(12) .* tanh((1i.*w.*p(13)*20).^0.5)./((1i.*w.*p(13)*20).^0.5-tanh((1i.*w.*p(13)*20).^0.5)))-p(9)./(1+1i.*w.*p(10))+p(9)./(1+1i.*w.*p(10)).^p(11))).^0.5) -((p(8).*(p(9)./(1+1i.*w.*p(10)).^p(11))).^0.5.*coth((p(8)./(p(9)./(1+1i.*w.*p(10)).^p(11))).^0.5)))+1.*(0)+1.*(0)+1.*(-10* p(12) .* tanh((1i.*w.*p(13)*20).^0.5)./((1i.*w.*p(13)*20).^0.5-tanh((1i.*w.*p(13)*20).^0.5)))+1.*(0)+1.*(0)+1.*(10* p(12) .* tanh((1i.*w.*p(13)*20).^0.5)./((1i.*w.*p(13)*20).^0.5-tanh((1i.*w.*p(13)*20).^0.5)))';
        temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max_esb=transpose(temporary_variable(1:13,2));
        p_min_esb=transpose(temporary_variable(1:13,1));
        if length(p_init_esb)~=length(p_min_esb)
            if length(p_init_esb)>length(p_min_esb)
                p_init_esb=p_init_esb(1:length(p_min_esb));
            elseif length(p_init_esb)<length(p_min_esb)
                p_init_esb=[p_init_esb zeros(1,length(p_min_esb)-length(p_init_esb))];
            end
        end
        p_init_esb = [ p_best_cabel, p_init_esb(5:end)];
        p_min_esb=[ p_best_cabel ,p_min_esb(5:end)];
        p_max_esb=[ p_best_cabel ,p_max_esb(5:end)];
        for n = 1:length(p_min_esb)
            if p_min_esb(n)<0
                p_min_esb(n) = 0;
            end
        end
        [p_best,fval,exitflag_or,output_or]=function_fit_easyfit2(app,m_w_esb,[m_real_esb, m_imag_esb],p_init_esb,@function_model_all_types2, p_min_esb, p_max_esb ,options, formula_mf_hf);
        
        %Kompletter Fit
        temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max_esb_compl=transpose(temporary_variable(:,2));
        p_min_esb_compl=transpose(temporary_variable(:,1));
        if length(p_init_esb_compl)~=length(p_min_esb_compl)
            if length(p_init_esb_compl)>length(p_min_esb_compl)
                p_init_esb_compl=p_init_esb_compl(1:length(p_min_esb_compl));
            elseif length(p_init_esb_compl)<length(p_min_esb_compl)
                p_init_esb_compl=[p_init_esb_compl zeros(1,length(p_min_esb_compl)-length(p_init_esb_compl))];
            end
        end
        p_init_esb_compl = [p_best, p_init_esb_compl(14:end)];
        %     p_min_calc=[0.99*p_best(1:4),0.99*p_best(5:13) ,p_min_esb_compl(14:end)];
        %     p_max_calc=[1.01*p_best(1:4),1.01*p_best(5:13) ,p_max_esb_compl(14:end)];
        p_min_calc = p_min_esb_compl;
        p_max_calc = p_max_esb_compl;
        for N = 1:length(p_max_calc)
            if app.ParamTable.Data{N,2}
                p_min_calc(N) = app.ParamTable.Data{N,3};
                p_max_calc(N) = app.ParamTable.Data{N,3};
            end
        end
        for n = 1:length(p_min_calc)
            if p_min_calc(n)<0
                p_min_calc(n) = 0;
            end
        end
        [p_best_or_compl,fval_or_compl,exitflag_or_compl,output_or_compl]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init_esb_compl,@function_model_all_types2, p_min_calc, p_max_calc ,options, formula);
        app.optimal_parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}=p_best_or_compl;
        p_best = p_best_or_compl;
    otherwise
        index_cabling = find(-m_imag<0);
        index_esb = find(-m_imag>0);
%         if isempty(index_cabling)
%             index_cabling = find(DRT_GUI.Messdaten.frequenz>=1000);
%             index_esb  = find(DRT_GUI.Messdaten.frequenz<1000);
%         end
        
        %Seperate the induktiv part resembling the cabling
        m_w_hf = m_w(index_cabling);
        m_real_hf = m_real(index_cabling);
        m_imag_hf = m_imag(index_cabling);
        
        %Seperate the capacitiv part resembling the rest hte actual
        %battery
        m_w_esb = m_w(index_esb);
        m_real_esb = m_real(index_esb);
        m_imag_esb = m_imag(index_esb);
        
        %Seperate this part into hf,mf and lf
        [pks_u,locs_u] = findpeaks(m_imag_esb);
        if length(locs_u)> 1
            [minimum_value, step_index_index] = max(pks_u);
            step_index = locs_u(step_index_index) + 2;
            %                 step_index = locs_u(end) + 7;
        elseif length(locs_u) < 1
            step_index = findchangepts(diff(m_real_esb));
        else
            step_index = locs_u(end) + 2;
        end
        
        %Low frequency part of EIS-spectrum
        m_real_lf = m_real_esb(step_index-1:end);
        m_imag_lf = m_imag_esb(step_index-1:end);
        m_w_lf = m_w_esb(step_index-1:end);
        
        m_w_mf = m_w_esb(1:step_index-2);
        m_real_mf = m_real_esb(1:step_index-2);
        m_imag_mf = m_imag_esb(1:step_index-2);
        
        %extract formula for hf part
        relevant_hf_index = strfind(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_HF,'+1.*(');
        irrelevant_hf_index = strfind(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_HF,'+0.*(');
        first_hf_index = strcmp(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_HF(1),'1');
        
        combined_hf = [relevant_hf_index,irrelevant_hf_index];
        combined_values_hf = [ones(1,length(relevant_hf_index)),zeros(1,length(irrelevant_hf_index))];
        
        if first_hf_index
            combined_hf = [1 , combined_hf];
            combined_values_hf = [1 , combined_values_hf];
        end
        
        [sorted_hf , sorted_hf_index] = sort(combined_hf);
        sorted_combined_hf_values = combined_values_hf(sorted_hf_index);
        sorted_hf = [sorted_hf,length(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_HF)];
        
        formula_hf = [];
        for n = 1:length(sorted_combined_hf_values)
            if sorted_combined_hf_values(n) == 1
                if sorted_hf(n) == 1
                    formula_hf = [formula_hf, DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_HF(sorted_hf(n):sorted_hf(n+1))];
                else
                    formula_hf = [formula_hf, DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_HF(sorted_hf(n)+1:sorted_hf(n+1))];
                end
            end
        end
        
        %extract formula for mf part
        relevant_mf_index = strfind(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_MF,'+1.*(');
        irrelevant_mf_index = strfind(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_MF,'+0.*(');
        first_mf_index = strcmp(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_MF(1),'1');
        
        combined_mf = [relevant_mf_index,irrelevant_mf_index];
        combined_values_mf = [ones(1,length(relevant_mf_index)),zeros(1,length(irrelevant_mf_index))];
        
        
        [sorted_mf , sorted_mf_index] = sort(combined_mf);
        sorted_combined_mf_values = combined_values_mf(sorted_mf_index);
        sorted_mf = [sorted_mf,length(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_MF)];
        
        formula_mf = [];
        for n = 1:length(sorted_combined_mf_values)
            if sorted_combined_mf_values(n) == 1
                if sorted_mf(n) == 1
                    formula_mf = [formula_mf, DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_MF(sorted_mf(n):sorted_mf(n+1))];
                else
                    formula_mf = [formula_mf, DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_MF(sorted_mf(n)+1:sorted_mf(n+1))];
                end
            end
        end
        % Include Rser into MF part
        formula_mf = strcat('1.*(p(1))+',formula_mf);
        if strcmp(formula_mf(end),'+')
            formula_mf = formula_mf(1:end-1);
        end
        if isempty(formula_hf)
            formula_hf = '+';
        end
        if strcmp(formula_hf(end),'+')
            formula_hf = formula_hf(1:end-1);
        end
        
        %seperate HF part of the formula
        mf_param_index(1) = 1;
        hf_param_index = [];
        mf_param_index_counter = 2;
        hf_param_index_counter = 1;
        for n1 = 1:length(DRT_GUI.Fit.aktuell_Modell.ModellCell{8})
            %find all mf parameters
            if ~strcmp( DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.Zfun_MF,'0')
                for n2 = 1:length(DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.ParameterIndexes)
                    if ~isempty(strfind( DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.Zfun_MF,app.ParamTable.Data{DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.ParameterIndexes(n2),1}))
                        if isempty(find(mf_param_index == DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.ParameterIndexes(n2)))
                            mf_param_index(mf_param_index_counter) = DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.ParameterIndexes(n2);
                            mf_param_index_counter = mf_param_index_counter + 1;
                        end
                    end
                end
            end
            %find all hf parameters
            if ~strcmp( DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.Zfun_HF,'0')
                for n2 = 1:length(DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.ParameterIndexes)
                    if ~isempty(strfind( DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.Zfun_HF,app.ParamTable.Data{DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.ParameterIndexes(n2),1}))
                        if isempty(find(hf_param_index == DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.ParameterIndexes(n2)))
                            hf_param_index(hf_param_index_counter) = DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.ParameterIndexes(n2);
                            hf_param_index_counter = hf_param_index_counter + 1;
                        end
                    end
                end
            end
        end
        
        
        p_init_hf = p_init(1:max(hf_param_index));
        p_init_esb = p_init(1:mf_param_index(end));
        p_init_esb_compl = p_init;
        p_best_cable = [];
        %hf fit
        if ~isempty(formula_hf)
            temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
            p_max_cabel=transpose(temporary_variable(1:hf_param_index(end),2));
            p_min_cabel=transpose(temporary_variable(1:hf_param_index(end),1));
            for n = 1:length(p_min_cabel)
                if p_min_cabel(n)<0
                    p_min_cabel(n) = 0;
                end
            end
            [p_best_cable,fval_cabel,exitflag_cabel,output_cabel]=function_fit_easyfit2(app,m_w_hf,[m_real_hf, m_imag_hf],p_init_hf,@function_model_all_types2, p_min_cabel, p_max_cabel ,options, formula_hf);
        end
        
        %mf fit
        %Cell parameter fit with calculated boundaries
        p_best_or = [];
        p_best = [];
        hf_empty = 0;
        if isempty(p_best_cable)
            hf_empty = 1;
        end
        if ~strcmp(formula_mf,'1.*(p(1))')
            temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
            p_max_esb=transpose(temporary_variable(1:mf_param_index(end),2));
            p_min_esb=transpose(temporary_variable(1:mf_param_index(end),1));
            if length(p_init_esb)~=length(p_min_esb)
                if length(p_init_esb)>length(p_min_esb)
                    p_init_esb=p_init_esb(1:length(p_min_esb));
                elseif length(p_init_esb)<length(p_min_esb)
                    p_init_esb=[p_init_esb zeros(1,length(p_min_esb)-length(p_init_esb))];
                end
            end
            if hf_empty
                p_best_cable = p_init_esb(1:max(hf_param_index));
            end
            p_init_esb(hf_param_index) = p_best_cable(hf_param_index);
            p_min_esb(hf_param_index)=p_best_cable(hf_param_index);
            p_max_esb(hf_param_index)=p_best_cable(hf_param_index);
            for n = 1:length(p_min_esb)
                if p_min_esb(n)<0
                    p_min_esb(n) = 0;
                end
            end
            [p_best,fval,exitflag_or,output_or]=function_fit_easyfit2(app,m_w_esb,[m_real_esb, m_imag_esb],p_init_esb,@function_model_all_types2, p_min_esb, p_max_esb ,options, formula_mf);
        end
        
        %Fix fittet Parameters and fit low frequency parts onto the
        %slow semi-circle
        
        %for calculated boundaries
        temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_init_esb_compl = p_init;
        p_max_esb_compl=transpose(temporary_variable(:,2));
        p_min_esb_compl=transpose(temporary_variable(:,1));
        if length(p_init_esb_compl)~=length(p_min_esb_compl)
            if length(p_init_esb_compl)>length(p_min_esb_compl)
                p_init_esb_compl=p_init_esb_compl(1:length(p_min_esb_compl));
            elseif length(p_init_esb_compl)<length(p_min_esb_compl)
                p_init_esb_compl=[p_init_esb_compl zeros(1,length(p_min_esb_compl)-length(p_init_esb_compl))];
            end
        end
        if isempty(p_best)
            p_best = p_init_esb_compl(1:max([max(mf_param_index), max(hf_param_index)]));
        end
        p_init_esb_compl(mf_param_index) = p_best(mf_param_index);
        p_init_esb_compl(hf_param_index) = p_best(hf_param_index);
        p_min_calc = zeros(1,length(p_init_esb_compl));
        p_min_calc(hf_param_index) = 0.95*p_best(hf_param_index);
        p_min_calc(mf_param_index) = 0.9*p_best(mf_param_index);
        p_min_calc(mf_param_index(end)+1:end) = p_min_esb_compl(mf_param_index(end)+1:end);
        p_max_calc = zeros(1,length(p_init_esb_compl));
        p_max_calc(hf_param_index) = 1.05*p_best(hf_param_index);
        p_max_calc(mf_param_index) = 1.1*p_best(mf_param_index);
        p_max_calc(mf_param_index(end)+1:end) = p_max_esb_compl(mf_param_index(end)+1:end);
        for N = 1:length(p_max_calc)
            if app.ParamTable.Data{N,2}
                p_min_calc(N) = app.ParamTable.Data{N,3};
                p_max_calc(N) = app.ParamTable.Data{N,3};
                p_init_esb_compl(N) = app.ParamTable.Data{N,3};
            end
        end
        for n = 1:length(p_min_calc)
            if p_min_calc(n)<0
                p_min_calc(n) = 0;
            end
        end
        [p_best_or_compl,fval_or_compl,exitflag_or_compl,output_or_compl]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init_esb_compl,@function_model_all_types2, p_min_calc, p_max_calc ,options, formula);
        app.optimal_parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}=p_best_or_compl;
        fval = fval_or_compl;
        p_best = p_best_or_compl;
end
DRT_GUI.Fit.Parameter = p_best;
% die Fittwerte(p-best) in Feld zeigen
for P_i = 1:length(p_best)
    TableCell(P_i).Value = app.optimal_parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(P_i);
end
TableCell = struct2cell(TableCell)';
if app.GUI
    set(app.ParamTable,'Data',TableCell)
else
    app.ParamTable.Data = TableCell;
end
app = fcn_SpeichernButton(app, event);
message_string = ['PostProcessing: Fitting for ' num2str(DRT_GUI.Testparameter.Temperatur) ' �C and ' num2str(DRT_GUI.Testparameter.SOC) '% SOC finished'];
disp(message_string)
end

