function app = fcn_adapt_boundaries(app,hyst_index,crate_index,temp_index,SOC_index,sorted_hyst_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array)
%Calculate new boundaries for the parameters
app.PunkteWegnehmenTextBox.Value = '';
event.Source = [];
event.EventName = 'ButtonPushed';
app = fcn_PunkteWegnehmeButton(app, event);

global DRT_GUI
%load data
% app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
% fcn_ModellAuswahlPopup(app);
% fcn_BatterieNamePopup_Callback(app);
if  isempty(cell2mat(strfind(fieldnames(DRT_GUI.Messdaten),'relax_fft'))) || isempty(DRT_GUI.Messdaten.relax_fft)
    m_w = DRT_GUI.Messdaten.omega(DRT_GUI.Messdaten.aktiv==1);
    m_real = DRT_GUI.Messdaten.Zreal(DRT_GUI.Messdaten.aktiv==1);
    m_imag = DRT_GUI.Messdaten.Zimg(DRT_GUI.Messdaten.aktiv==1);
    m_f = DRT_GUI.Messdaten.frequenz(DRT_GUI.Messdaten.aktiv==1);
else
    m_w = [DRT_GUI.Messdaten.omega(DRT_GUI.Messdaten.aktiv==1) ; DRT_GUI.Messdaten.relax_fft.omega(DRT_GUI.Messdaten.relax_fft.aktiv==1)] ; %selbst
    m_real = [DRT_GUI.Messdaten.Zreal(DRT_GUI.Messdaten.aktiv==1); DRT_GUI.Messdaten.relax_fft.Zreal(DRT_GUI.Messdaten.relax_fft.aktiv==1)+DRT_GUI.Messdaten.relax_fft.Zreal_korrektur];%selbst
    m_imag = [DRT_GUI.Messdaten.Zimg(DRT_GUI.Messdaten.aktiv==1); DRT_GUI.Messdaten.relax_fft.Zimg(DRT_GUI.Messdaten.relax_fft.aktiv==1)];%selbst
    m_f = [DRT_GUI.Messdaten.frequenz(DRT_GUI.Messdaten.aktiv==1); DRT_GUI.Messdaten.relax_fft.frequenz(DRT_GUI.Messdaten.relax_fft.aktiv==1)];%selbst
end
esb_motivated_boundaries = zeros(1,length(app.ParamTable.Data));
if ~app.GUI
    m_real = DRT_GUI.Messdaten.Zreal(find(DRT_GUI.Messdaten.Zimg<=0));
    m_imag = DRT_GUI.Messdaten.Zimg(find(DRT_GUI.Messdaten.Zimg<=0));
    m_w = DRT_GUI.Messdaten.omega(find(DRT_GUI.Messdaten.Zimg<=0));
    m_f = DRT_GUI.Messdaten.frequenz(find(DRT_GUI.Messdaten.Zimg<=0));
    interpolated_real = min(m_real):(max(m_real)-min(m_real))/30:max(m_real);
    interpolated_imag = interp1(m_real,m_imag,min(m_real):(max(m_real)-min(m_real))/30:max(m_real),'spline');
    interpolated_w = interp1(m_real,m_w,min(m_real):(max(m_real)-min(m_real))/30:max(m_real),'spline');
    m_real = interpolated_real;
    m_imag = interpolated_imag;
    m_w = interpolated_w;
end
%%
%adapt boundaries using characteristic points
if strcmp(app.GlobalUsedModell, 'R2RC')
    if sum(sum(cellfun(@isempty,app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index})))==(length(app.ParamTable.Data(:,1))*2)
        try
            [pks_o,locs_o]=findpeaks(-m_imag);
            [pks_u,locs_u]=findpeaks(m_imag);
            if isempty(pks_u) && isempty(locs_u)
                locs_u=find(min(-m_imag(locs_o:end)));
                pks_u=-m_imag(locs_o+locs_u);
                index=find(-m_imag>0);
                index=find(min(-m_imag(index)))+index(1)-1;
                if index == 1
%                     Rser_Init= m_real(1);
                    Rser_Init = ((m_imag(2)-m_imag(1))/(m_real(2)-m_real(1)) * m_real(1) - m_imag(1)) * ((m_imag(2)-m_imag(1))/(m_real(2)-m_real(1)));
                    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(1,1)={Rser_Init*0.1};
                    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(1,2)={Rser_Init*10};
                else
                    Rser_Init= m_real(index-1)+(m_real(index)-m_real(index-1))*(m_imag(index-1))/(-m_imag(index)+m_imag(index-1));
                    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(1,1)={Rser_Init*0.95};
                    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(1,2)={Rser_Init*1.05};
                end
                R1_Init=abs(m_real(index)-m_real(locs_u(1)))  - Rser_Init;
                C1_Init=1/(R1_Init*2*pi*m_f(locs_o(1)));
                Tau1_Init=R1_Init*C1_Init;
                app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(2,1)={R1_Init*0.8};
                app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(2,2)=app.ParamTable.Data(2,5);
                esb_motivated_boundaries(2) = 1;
            else
                index=find(-m_imag>0);
                index=find(min(-m_imag(index)))+index(1)-1;
                if index == 1
                    Rser_Init= ((m_imag(2)-m_imag(1))/(m_real(2)-m_real(1)) * m_real(1) - m_imag(1)) * ((m_real(2)-m_real(1))/(m_imag(2)-m_imag(1)));
                    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(1,1)={Rser_Init*0.1};
                    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(1,2)={Rser_Init*10};
                else
                    Rser_Init=m_real(index-1)+(m_real(index)-m_real(index-1))*(m_imag(index-1))/(-m_imag(index)+m_imag(index-1));
                    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(1,1)={Rser_Init*0.95};
                    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(1,2)={Rser_Init*1.05};
                end
                
                
                R1_Init = ((m_imag(locs_u(1))-m_imag(locs_u(1)-1))/(m_real(locs_u(1))-m_real(locs_u(1)-1)) * m_real(locs_u(1)-1) - m_imag(locs_u(1)-1)) * ((m_real(locs_u(1))-m_real(locs_u(1)-1))/(m_imag(locs_u(1))-m_imag(locs_u(1)-1))) - Rser_Init;
                if ~isempty(find(locs_o < locs_u(1)))
                    w_interpolated_maximum = m_w(locs_o(1));
                else
                    [~,w_interpolated_maximum] = min(abs(m_real-((R1_Init-Rser_Init)/2+Rser_Init)));
                    w_interpolated_maximum = m_w(w_interpolated_maximum);
                end
                
                C1_Init=1/(R1_Init*w_interpolated_maximum);
                Tau1_Init=R1_Init*C1_Init;
                app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(2,1)={R1_Init*0.8};
                app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(2,2)={R1_Init*1.2};
                esb_motivated_boundaries(2) = 1;
            end
            
            
            
            %save initial boundaries
            app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(1,1)={Rser_Init*0.8};
            app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(1,2)={Rser_Init*1.2};
            esb_motivated_boundaries(1) = 1;
            if(strcmp(app.ParamTable.Data(3,1),'Tau1'))
                app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(3,1)={Tau1_Init*0.8};
                app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(3,2)={Tau1_Init*1.2};
                esb_motivated_boundaries(3) = 1;
            elseif(strcmp(app.ParamTable.Data(3,1),'C1'))
                app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(3,1)={C1_Init*0.8};
                app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(3,2)={C1_Init*1.2};
                esb_motivated_boundaries(3) = 1;
            end
            for n=4:length(app.ParamTable.Data(:,1))
                if(mod(length(app.ParamTable.Data(4:end,1)),2)~=0)
                    msgbox('Das Ersatzschaltbild ist keine RnRC-Schaltung!')
                    return
                end
                app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(n,1)=app.ParamTable.Data(n,4);
                app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(n,2)=app.ParamTable.Data(n,5);
            end         
        catch
            if SOC_index+1<=length(sorted_SOC_array{hyst_index}{crate_index}{temp_index}) && SOC_index-1>=1 && app.Optimization_done(hyst_index,crate_index,temp_index,SOC_index+1)==1 && app.Optimization_done(hyst_index,crate_index,temp_index,SOC_index-1)==1
                for n=1:length(app.ParamTable.Data(:,4))
                    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(n,1)={((app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index+1}{n,1}+app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index-1}{n,1})/2)*1.1};
                    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(n,2)={((app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index+1}{n,2}+app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index-1}{n,2})/2)*0.9};
                end
            elseif SOC_index+1<=length(sorted_SOC_array{hyst_index}{crate_index}{temp_index}) && app.Optimization_done(hyst_index,crate_index,temp_index,SOC_index+1)==1
                for n=1:length(app.ParamTable.Data(:,4))
                    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(n,1)={app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index+1}{n,1}/1.1};
                    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(n,2)={app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index+1}{n,2}/0.9};
                end
            elseif SOC_index-1>=1 && app.Optimization_done(hyst_index,crate_index,temp_index,SOC_index-1)==1
                for n=1:length(app.ParamTable.Data(:,4))
                    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(n,1)={app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index-1}{n,1}*1.1};
                    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(n,2)={app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index-1}{n,2}*0.9};
                end
            else
                app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(:,1)=app.ParamTable.Data(:,4);
                app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(:,2)=app.ParamTable.Data(:,5);
            end
        end
    end
elseif strcmp(app.GlobalUsedModell, 'LiIon4_sbi')
    %Set Phi to fixed value 1
    index=find(-m_imag>0);
    index=find(min(-m_imag(index)))+index(1)-1;
    if index == 1
        Rser_Init= ((m_imag(2)-m_imag(1))/(m_real(2)-m_real(1)) * m_real(1) - m_imag(1)) * ((m_imag(2)-m_imag(1))/(m_real(2)-m_real(1)));
    else
        Rser_Init=m_real(index-1)+(m_real(index)-m_real(index-1))*(m_imag(index-1))/(-m_imag(index)+m_imag(index-1));
    end
    
    DataMatrix = app.ParamTable.Data;
    DataMatrix(1,4)={[Rser_Init*1.2]};
    DataMatrix(1,5)={[Rser_Init*0.8]};
    esb_motivated_boundaries(1) = 1;
    if app.constantPhiCheckkBox.Value
        DataMatrix(4,2)= {true};
        DataMatrix(7,2)= {true};
        DataMatrix(11,2)= {true};
        DataMatrix(4,3)= {1};
        DataMatrix(7,3)= {1};
        DataMatrix(11,3)= {1};
        esb_motivated_boundaries(4) = 1;
        esb_motivated_boundaries(7) = 1;
        esb_motivated_boundaries(11) = 1;
    end
    DataMatrix(16,2)= {false};
    DataMatrix(17,2)= {false};
    if app.GUI
        set(app.ParamTable,'Data',DataMatrix);
    else
        app.ParamTable.Data = DataMatrix;
    end
    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(:,1)=app.ParamTable.Data(:,4);
    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(:,2)=app.ParamTable.Data(:,5);
    app = fcn_SpeichernButton(app, event);
elseif strcmp(app.GlobalUsedModell, 'LiIon4')
    DataMatrix = app.ParamTable.Data;
    index=find(-m_imag>0);
    index=find(min(-m_imag(index)))+index(1)-1;
    if index == 1
        Rser_Init= ((m_imag(2)-m_imag(1))/(m_real(2)-m_real(1)) * m_real(1) - m_imag(1)) * ((m_imag(2)-m_imag(1))/(m_real(2)-m_real(1)));
    else
        Rser_Init=m_real(index-1)+(m_real(index)-m_real(index-1))*(m_imag(index-1))/(-m_imag(index)+m_imag(index-1));
    end
    [pks_o,locs_o]=findpeaks(-m_imag);
    [pks_u,locs_u]=findpeaks(m_imag);
    if isempty(pks_u) && isempty(locs_u)  && ~isempty(pks_o) && ~isempty(locs_o)
        locs_u=find(min(-m_imag(locs_o:end)));
        pks_u=-m_imag(locs_o+locs_u);
        R1_Init=abs(Rser_Init-m_real(locs_u(1)));
        C1_Init=1/(R1_Init*2*pi*m_f(locs_o(1)));
        Tau1_Init=R1_Init*C1_Init;
        DataMatrix(5,3)= {[R1_Init]};
        DataMatrix(5,4)= {[0.5*R1_Init]};
        DataMatrix(5,5)= {[1.2*R1_Init]};
        DataMatrix(9,3)= {[R1_Init]};
        DataMatrix(9,4)= {[0.5*R1_Init]};
        DataMatrix(9,5)= {[1.2*R1_Init]};
        DataMatrix(6,3)= {[Tau1_Init]};
        DataMatrix(6,4)= {[0.5*Tau1_Init]};
        DataMatrix(6,5)= {[1.5*Tau1_Init]};
        DataMatrix(10,3)= {[Tau1_Init]};
        DataMatrix(10,4)= {[0.5*Tau1_Init]};
        DataMatrix(10,5)= {[1.5*Tau1_Init]};
        esb_motivated_boundaries(5) = 1;
        esb_motivated_boundaries(6) = 1;
        esb_motivated_boundaries(9) = 1;
        esb_motivated_boundaries(10) = 1;
    else
        m_w_mf = m_w(find(-m_imag>0));
        m_real_mf = m_real(find(-m_imag>0));
        m_imag_mf = m_imag(find(-m_imag>0));
        interpolated_real = min(m_real_mf):(max(m_real_mf)-min(m_real_mf))/30:max(m_real_mf);
        interpolated_imag = interp1(m_real_mf,m_imag_mf,min(m_real_mf):(max(m_real_mf)-min(m_real_mf))/30:max(m_real_mf),'spline');
        %             [~,turning_point_idx] = max(-diff(diff(interpolated_imag)));
        [~,turning_point_idx] = min(abs(diff(diff(interpolated_imag))-0));
        [~,step_index] = min(abs(m_real_mf - interpolated_real(turning_point_idx) ));
        if step_index < length(m_real_mf)-3
            step_index = step_index + 3;
        end
        R1_Init=abs(Rser_Init-m_real_mf(step_index));
        if ~isempty(locs_o)
            C1_Init=1/(R1_Init*m_w_mf(locs_o(1)));
        else
            Rser_Init=m_real(index-1)+(m_real(index)-m_real(index-1))*(m_imag(index-1))/(-m_imag(index)+m_imag(index-1));
        end
        
        DataMatrix = app.ParamTable.Data;
        DataMatrix(1,4)={[Rser_Init*1.2]};
        DataMatrix(1,5)={[Rser_Init*0.8]};
        esb_motivated_boundaries(1) = 1;
        if app.constantPhiCheckkBox.Value
            DataMatrix(4,2)= {true};
            DataMatrix(7,2)= {true};
            DataMatrix(11,2)= {true};
            DataMatrix(4,3)= {1};
            DataMatrix(7,3)= {1};
            DataMatrix(11,3)= {1};
            esb_motivated_boundaries(4) = 1;
            esb_motivated_boundaries(7) = 1;
            esb_motivated_boundaries(11) = 1;
        end
        DataMatrix(16,2)= {false};
        DataMatrix(17,2)= {false};
        if app.GUI
            set(app.ParamTable,'Data',DataMatrix);
        else
            app.ParamTable.Data = DataMatrix;
        end
        app.Boundaries{temp_index,SOC_index}(:,1)=app.ParamTable.Data(:,4);
        app.Boundaries{temp_index,SOC_index}(:,2)=app.ParamTable.Data(:,5);
        app = fcn_SpeichernButton(app, event);
    case'LiIon4'
        index=find(-m_imag>0);
        index=find(min(-m_imag(index)))+index(1)-1;
        if index == 1
            Rser_Init= m_real(1);
        else
            Rser_Init=m_real(index-1)+(m_real(index)-m_real(index-1))*(m_imag(index-1))/(-m_imag(index)+m_imag(index-1));
        end
        
        DataMatrix = app.ParamTable.Data;
        DataMatrix(1,4)={[Rser_Init*0.8]};
        DataMatrix(1,5)={[Rser_Init*1.05]};
        DataMatrix(1,3)={[Rser_Init]};
        esb_motivated_boundaries(1) = 1;
        %Fix Parameters
        if app.constantPhiCheckkBox.Value
            DataMatrix(4,2)= {true};
            DataMatrix(7,2)= {true};
            DataMatrix(11,2)= {true};
            DataMatrix(4,3)= {1};
            DataMatrix(7,3)= {1};
            DataMatrix(11,3)= {1};
            DRT_GUI.Fit.Parameter(4) = 1;
            DRT_GUI.Fit.Parameter(7) = 1;
            DRT_GUI.Fit.Parameter(11) = 1;
            DataMatrix(1,2)= {false};
            DataMatrix(2,2)= {false};
            DataMatrix(3,2)= {false};
            DataMatrix(5,2)= {false};
            DataMatrix(6,2)= {false};
            DataMatrix(8,2)= {false};
            DataMatrix(9,2)= {false};
            DataMatrix(10,2)= {false};
            DataMatrix(12,2)= {false};
            DataMatrix(13,2)= {false};
            DataMatrix(14,2)= {false};
            DataMatrix(15,2)= {false};
            DataMatrix(16,2)= {false};
            DataMatrix(17,2)= {false};
            esb_motivated_boundaries(4) = 1;
            esb_motivated_boundaries(7) = 1;
            esb_motivated_boundaries(11) = 1;
        else
            DataMatrix(4,2)= {false};
            DataMatrix(7,2)= {false};
            DataMatrix(11,2)= {false};
            DataMatrix(4,3)= {1};
            DataMatrix(7,3)= {1};
            DataMatrix(11,3)= {1};
            DataMatrix(1,2)= {false};
            DataMatrix(2,2)= {false};
            DataMatrix(3,2)= {false};
            DataMatrix(5,2)= {false};
            DataMatrix(6,2)= {false};
            DataMatrix(8,2)= {false};
            DataMatrix(9,2)= {false};
            DataMatrix(10,2)= {false};
            DataMatrix(12,2)= {false};
            DataMatrix(13,2)= {false};
            DataMatrix(14,2)= {false};
            DataMatrix(15,2)= {false};
            DataMatrix(16,2)= {false};
            DataMatrix(17,2)= {false};
            esb_motivated_boundaries(4) = 1;
            esb_motivated_boundaries(7) = 1;
            esb_motivated_boundaries(11) = 1;
        end
        DataMatrix(16,2)= {false};
        DataMatrix(17,2)= {false};
        esb_motivated_boundaries(4) = 1;
        esb_motivated_boundaries(7) = 1;
        esb_motivated_boundaries(11) = 1;
    end
    DataMatrix(16,2)= {false};
    DataMatrix(17,2)= {false};
    if app.GUI
        set(app.ParamTable,'Data',DataMatrix);
    else
        app.ParamTable.Data = DataMatrix;
    end
    Fixed_Param_Array = [];
    for n=1:length(DataMatrix(:,2))
        Fixed_Param_Array = [Fixed_Param_Array, DataMatrix{n,2}];
    end
    DRT_GUI.Fit.ParFix = Fixed_Param_Array;
    event.EventName = 'ButtonPushed';
    app  = fcn_SpeichernButton(app, event);
    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(:,1)=app.ParamTable.Data(:,4);
    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(:,2)=app.ParamTable.Data(:,5);
elseif strcmp(app.GlobalUsedModell, 'LiIon4_NoInd')
    DataMatrix = app.ParamTable.Data;
    index=find(-m_imag>0);
    index=find(min(-m_imag(index)))+index(1)-1;
    if index == 1
        Rser_Init= ((m_imag(2)-m_imag(1))/(m_real(2)-m_real(1)) * m_real(1) - m_imag(1)) * ((m_imag(2)-m_imag(1))/(m_real(2)-m_real(1)));
    else
        Rser_Init=m_real(index-1)+(m_real(index)-m_real(index-1))*(m_imag(index-1))/(-m_imag(index)+m_imag(index-1));
    end
    [pks_o,locs_o]=findpeaks(-m_imag);
    [pks_u,locs_u]=findpeaks(m_imag);
    if isempty(pks_u) && isempty(locs_u) && ~isempty(pks_o) && ~isempty(locs_o)
        locs_u=find(min(-m_imag(locs_o:end)));
        pks_u=-m_imag(locs_o+locs_u);
        R1_Init=abs(Rser_Init-m_real(locs_u(1)));
        C1_Init=1/(R1_Init*2*pi*m_f(locs_o(1)));
        Tau1_Init=R1_Init*C1_Init;
        DataMatrix(2,3)= {[R1_Init]};
        DataMatrix(2,4)= {[0.1*R1_Init]};
        DataMatrix(2,5)= {[1.2*R1_Init]};
        DataMatrix(6,3)= {[R1_Init]};
        DataMatrix(6,4)= {[0.1*R1_Init]};
        DataMatrix(6,5)= {[1.2*R1_Init]};
        DataMatrix(3,3)= {[Tau1_Init]};
        DataMatrix(3,4)= {[0.1*Tau1_Init]};
        DataMatrix(3,5)= {[1.5*Tau1_Init]};
        DataMatrix(7,3)= {[Tau1_Init]};
        DataMatrix(7,4)= {[0.1*Tau1_Init]};
        DataMatrix(7,5)= {[1.5*Tau1_Init]};
        esb_motivated_boundaries(5) = 1;
        esb_motivated_boundaries(6) = 1;
        esb_motivated_boundaries(9) = 1;
        esb_motivated_boundaries(10) = 1;
    else
        m_w_mf = m_w(find(-m_imag>0));
        m_real_mf = m_real(find(-m_imag>0));
        m_imag_mf = m_imag(find(-m_imag>0));
        interpolated_real = min(m_real_mf):(max(m_real_mf)-min(m_real_mf))/30:max(m_real_mf);
        interpolated_imag = interp1(m_real_mf,m_imag_mf,min(m_real_mf):(max(m_real_mf)-min(m_real_mf))/30:max(m_real_mf),'spline');
        %             [~,turning_point_idx] = max(-diff(diff(interpolated_imag)));
        [~,turning_point_idx] = min(abs(diff(diff(interpolated_imag))-0));
        [~,step_index] = min(abs(m_real_mf - interpolated_real(turning_point_idx) ));
        if step_index < length(m_real_mf)-3
            step_index = step_index + 3;
        end
        R1_Init=abs(Rser_Init-m_real_mf(step_index));
        if ~isempty(locs_o)
            C1_Init=1/(R1_Init*m_w_mf(locs_o(1)));
        else
            app.ParamTable.Data = DataMatrix;
        end
        Tau1_Init=R1_Init*C1_Init;
        DataMatrix(2,3)= {[R1_Init]};
        DataMatrix(2,4)= {[0.1*R1_Init]};
        DataMatrix(2,5)= {[2*R1_Init]};
        DataMatrix(6,3)= {[R1_Init]};
        DataMatrix(6,4)= {[0.1*R1_Init]};
        DataMatrix(6,5)= {[2*R1_Init]};
        DataMatrix(3,3)= {[Tau1_Init]};
        DataMatrix(3,4)= {[0.1*Tau1_Init]};
        DataMatrix(3,5)= {[10*Tau1_Init]};
        DataMatrix(7,3)= {[Tau1_Init]};
        DataMatrix(7,4)= {[0.1*Tau1_Init]};
        DataMatrix(7,5)= {[10*Tau1_Init]};
        esb_motivated_boundaries(5) = 1;
        esb_motivated_boundaries(6) = 1;
        esb_motivated_boundaries(9) = 1;
        esb_motivated_boundaries(10) = 1;
    end
    
    DataMatrix(1,4)={[Rser_Init*0.8]};
    DataMatrix(1,5)={[Rser_Init*1.01]};
    DataMatrix(1,3)={[Rser_Init]};
    esb_motivated_boundaries(1) = 1;
    %Fix Parameters
    if app.constantPhiCheckkBox.Value
        DataMatrix(4,2)= {true};
        DataMatrix(8,2)= {true};
        DataMatrix(4,3)= {1};
        DataMatrix(8,3)= {1};
        DRT_GUI.Fit.Parameter(4) = 1;
        DRT_GUI.Fit.Parameter(8) = 1;
        DataMatrix(1,2)= {false};
        DataMatrix(2,2)= {false};
        DataMatrix(3,2)= {false};
        DataMatrix(5,2)= {false};
        DataMatrix(6,2)= {false};
        DataMatrix(8,2)= {false};
        DataMatrix(9,2)= {false};
        DataMatrix(10,2)= {false};
        DataMatrix(12,2)= {false};
        DataMatrix(13,2)= {false};
        DataMatrix(14,2)= {false};
        DataMatrix(15,2)= {false};
        esb_motivated_boundaries(4) = 1;
        esb_motivated_boundaries(8) = 1;
    else
        DataMatrix(4,2)= {false};
        DataMatrix(8,2)= {false};
        DataMatrix(4,3)= {1};
        DataMatrix(8,3)= {1};
        DataMatrix(1,2)= {false};
        DataMatrix(2,2)= {false};
        DataMatrix(3,2)= {false};
        DataMatrix(5,2)= {false};
        DataMatrix(6,2)= {false};
        DataMatrix(8,2)= {false};
        DataMatrix(9,2)= {false};
        DataMatrix(10,2)= {false};
        DataMatrix(12,2)= {false};
        DataMatrix(13,2)= {false};
        DataMatrix(14,2)= {false};
        DataMatrix(15,2)= {false};
        esb_motivated_boundaries(4) = 1;
        esb_motivated_boundaries(8) = 1;
    end
    if app.GUI
        set(app.ParamTable,'Data',DataMatrix);
    else
        app.ParamTable.Data = DataMatrix;
    end
    Fixed_Param_Array = [];
    for n=1:length(DataMatrix(:,2))
        Fixed_Param_Array = [Fixed_Param_Array, DataMatrix{n,2}];
    end
    DRT_GUI.Fit.ParFix = Fixed_Param_Array;
    event.EventName = 'ButtonPushed';
    app  = fcn_SpeichernButton(app, event);
    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(:,1)=app.ParamTable.Data(:,4);
    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(:,2)=app.ParamTable.Data(:,5);
elseif strcmp(app.GlobalUsedModell, 'C')  || strcmp(app.GlobalUsedModell, '3RC') || strcmp(app.GlobalUsedModell,'Zarc')
    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index} = app.Boundaries_Default{hyst_index,crate_index,temp_index,SOC_index};
elseif strcmp(app.GlobalUsedModell, 'RC')
    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index} = app.Boundaries_Default{hyst_index,crate_index,temp_index,SOC_index};
    [pks_u,locs_u]=findpeaks(m_imag);
    index=find(-m_imag>0);
    index=find(min(-m_imag(index)))+index(1)-1;
    if index == 1
        Rser_Init = 0;
    else
        Rser_Init = m_real(index-1)+(m_real(index)-m_real(index-1))*(m_imag(index-1))/(-m_imag(index)+m_imag(index-1));
    end
    if ~isempty(pks_u)
        R_Init = m_real(loc_u) - Rser_Init;
    else
        R_Init = m_real(end) - Rser_Init;
    end
    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(1,1)={R_Init*0.8};
    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(1,2)={R_Init*1.2};
elseif strcmp(app.GlobalUsedModell, '2RC')
    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index} = app.Boundaries_Default{hyst_index,crate_index,temp_index,SOC_index};
    [pks_u,locs_u]=findpeaks(m_imag);
    index=find(-m_imag>0);
    index=find(min(-m_imag(index)))+index(1)-1;
    if index == 1
        Rser_Init = 0;
    else
        Rser_Init = m_real(index-1)+(m_real(index)-m_real(index-1))*(m_imag(index-1))/(-m_imag(index)+m_imag(index-1));
    end
    if ~isempty(pks_u)
        R_Init = m_real(loc_u) - Rser_Init;
    else
        R_Init = m_real(end) - Rser_Init;
    end
    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(1,1)={R_Init*0.8};
    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(1,2)={R_Init*1.2};
    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(3,1)={R_Init*0.8};
    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(3,2)={R_Init*1.2};
    
else
    index=find(-m_imag>0);
    index=find(min(-m_imag(index)))+index(1)-1;
    if index == 1
        Rser_Init= m_real(1);
    else
        Rser_Init=m_real(index-1)+(m_real(index)-m_real(index-1))*(m_imag(index-1))/(-m_imag(index)+m_imag(index-1));
    end
    
    DataMatrix = app.ParamTable.Data;
    DataMatrix(1,4)={[Rser_Init*0.8]};
    DataMatrix(1,5)={[Rser_Init*1.05]};
    DataMatrix(1,3)={[Rser_Init]};
    esb_motivated_boundaries(1) = 1;
    for n=1:length(DataMatrix(:,2))
        DataMatrix(n,2) = {false};
    end
    if app.GUI
        set(app.ParamTable,'Data',DataMatrix);
    else
        app.ParamTable.Data = DataMatrix;
    end
    Fixed_Param_Array = [];
    for n=1:length(DataMatrix(:,2))
        Fixed_Param_Array = [Fixed_Param_Array, DataMatrix{n,2}];
    end
    DRT_GUI.Fit.ParFix = Fixed_Param_Array;
    event.EventName = 'ButtonPushed';
    app = fcn_SpeichernButton(app, event);
    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(:,1)=app.ParamTable.Data(:,4);
    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(:,2)=app.ParamTable.Data(:,5);
end

%Use configuration from starting soc-temperature choice -considering the
%fixed parameter- values for all states
if app.Global_Fixed_Config_CheckBox.Value
    DataMatrix = app.ParamTable.Data;
    for n = 1:length(app.ParamTable.Data(:,1))
        if app.global_fixed_parameters(n) == 1
            DataMatrix(n,2) = {true};
        else
            DataMatrix(n,2) = {false};
        end
    end
    if app.GUI
        set(app.ParamTable,'Data',DataMatrix);
    else
        app.ParamTable.Data = DataMatrix;
    end
end
if app.UseDRTCheckBox.Value
%     app.Korrigiert_Punkte_Weg_TextBox.Value = 'end';
%     app = fcn_Korrigiert_Punkte_Weg_Button(app, event);
%     app.ZarcHNPopup.Value = 'MF-Model';
%     app = fcn_ZarcHNPopup(app,event);
%     if app.GUI
%         ParamData_OrigData = get(app.ParamTable,'Data');
%         ParamData = get(app.ParamTable,'Data');
%     else
%         ParamData_OrigData = app.ParamTable.Data;
%         ParamData = app.ParamTable.Data;
%     end
%     for n = 1:length(app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(:,1))
%         ParamData(n,4) = app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(n,1);
%         ParamData(n,5) = app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(n,2);
%     end
%     if app.GUI
%         set(app.ParamTable,'Data',ParamData);
%     else
%         app.ParamTable.Data = ParamData;
%     end
%     app = fcn_Prozesse_fitten_button(app,event);
%     if app.GUI
%         ProzessTableData = get(app.ProzesseTable,'Data');
%     else
%         ProzessTableData = app.ProzesseTable.Data;
%     end
%     for n =  1:length(app.DRT_indices)
%         DRT_GUI.Fit.Parameter(app.DRT_indices(n)) = ProzessTableData{n,1};
%         app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}{app.DRT_indices(n),1} = 0.1 * ProzessTableData{n,1};
%         app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}{app.DRT_indices(n),2} = 10 * ProzessTableData{n,1};
%         ParamData_OrigData{app.DRT_indices(n),3} = ProzessTableData{n,1};
%     end
%     if app.GUI
%         set(app.ParamTable,'Data',ParamData_OrigData);
%     else
%         app.ParamTable.Data = ParamData_OrigData;
%     end
    [~,process_parameter] = fcn_CalculateDRTCiucci(app,event);
end
%% Grenzextrapolation
%identify Taus and Rs
DataMatrix = app.ParamTable.Data;
tau_r_indices = [];
tau_r_indices_count = 1;
for n = 1:length(app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(:,1))
    if length(DataMatrix{n,1}) >= 3
        if strcmp(DataMatrix{n,1}(1:3),'Tau')
            tau_r_indices(tau_r_indices_count) = n;
            tau_r_indices_count = tau_r_indices_count + 1;
        elseif strcmp(DataMatrix{n,1}(1),'R')
            tau_r_indices(tau_r_indices_count) = n;
            tau_r_indices_count = tau_r_indices_count + 1;
        end
    elseif length(DataMatrix{n,1}) >= 1
        if strcmp(DataMatrix{n,1}(1),'R')
            tau_r_indices(tau_r_indices_count) = n;
            tau_r_indices_count = tau_r_indices_count + 1;
        end
    end
end

extrapolation_count=1;
potential_new_boundaries=[];
%Check if respective SOC/T Values have been optimized for current SOC/T
%values
%n�chst niedrigerer SOC
if SOC_index>1
    sl_index=SOC_index-1;
    if app.Optimization_done(hyst_index,crate_index,temp_index,sl_index)
        for n=1:length(app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(:,1))
            potential_new_boundaries{extrapolation_count}{n,1}=(1-((app.ContinousStep.Value/(100*10))*(sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)-sorted_SOC_array{hyst_index}{crate_index}{temp_index}(sl_index))))*app.optimal_parameters{hyst_index}{crate_index}{temp_index}{sl_index}(n);
            potential_new_boundaries{extrapolation_count}{n,2}=(1+((app.ContinousStep.Value/(100*10))*(sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)-sorted_SOC_array{hyst_index}{crate_index}{temp_index}(sl_index))))*app.optimal_parameters{hyst_index}{crate_index}{temp_index}{sl_index}(n);
        end
        extrapolation_count=extrapolation_count+1;
    end
end

%n�chst h�herer SOC
if SOC_index<length(sorted_SOC_array{hyst_index}{crate_index}{temp_index})
    su_index=SOC_index+1;
    if app.Optimization_done(hyst_index,crate_index,temp_index,su_index)
        for n=1:length(app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(:,1))
            potential_new_boundaries{extrapolation_count}{n,1}=(app.optimal_parameters{hyst_index}{crate_index}{temp_index}{su_index}(n))*(1+((app.ContinousStep.Value/(100*10))*(sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)-sorted_SOC_array{hyst_index}{crate_index}{temp_index}(su_index))));
            potential_new_boundaries{extrapolation_count}{n,2}=(app.optimal_parameters{hyst_index}{crate_index}{temp_index}{su_index}(n))*(1-((app.ContinousStep.Value/(100*10))*(sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)-sorted_SOC_array{hyst_index}{crate_index}{temp_index}(su_index))));
        end
        extrapolation_count=extrapolation_count+1;
    end
end



%n�chste niedrigere Temperatur
interpolated_value_lower_temp=[];
if temp_index>1
    %         tl_index=find(min(abs(sorted_SOC_array{temp_index}(SOC_index)-sorted_SOC_array{temp_index-1})));
    [~,tl_index]=min(abs(sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)-sorted_SOC_array{hyst_index}{crate_index}{temp_index-1}));
    if abs(sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)-sorted_SOC_array{hyst_index}{crate_index}{temp_index-1}(tl_index))<10
        if round(sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)/5)*5==round(sorted_SOC_array{hyst_index}{crate_index}{temp_index-1}(tl_index)/5)*5
            if app.Optimization_done(hyst_index,crate_index,temp_index-1,tl_index)
                interpolated_value_lower_temp=app.optimal_parameters{hyst_index}{crate_index}{temp_index-1}{tl_index};
            end
        elseif round(sorted_SOC_array{hyst_index}{crate_index}{temp_index-1}(tl_index)/5)*5<round(sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)/5)*5
            if tl_index+1<=length(sorted_SOC_array{hyst_index}{crate_index}{temp_index-1})
                temp_SOC_count=tl_index+1;
                if app.Optimization_done(hyst_index,crate_index,temp_index-1,tl_index) && app.Optimization_done(hyst_index,crate_index,temp_index-1,temp_SOC_count)
                    %Interpolation zwischen den SOC Punkten tl_index+1>tl_index
                    interpolated_value_lower_temp=zeros(1,length(app.Boundaries{hyst_index}{crate_index}{temp_index-1}{tl_index}(:,1)));
                    for n=1:length(app.Boundaries{hyst_index}{crate_index}{temp_index-1}{tl_index}(:,1))
                        interpolated_value_lower_temp(n)=(app.optimal_parameters{hyst_index}{crate_index}{temp_index-1}{temp_SOC_count}(n)-app.optimal_parameters{hyst_index}{crate_index}{temp_index-1}{tl_index}(n))*(((sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)-sorted_SOC_array{hyst_index}{crate_index}{temp_index-1}(tl_index))/(sorted_SOC_array{hyst_index}{crate_index}{temp_index-1}(temp_SOC_count)-sorted_SOC_array{hyst_index}{crate_index}{temp_index-1}(tl_index))))+app.optimal_parameters{hyst_index}{crate_index}{temp_index-1}{tl_index}(n);
                    end
                end
            end
        elseif round(sorted_SOC_array{hyst_index}{crate_index}{temp_index-1}(tl_index)/5)*5>round(sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)/5)*5
            if tl_index-1>=1
                temp_SOC_count=tl_index-1;
                if app.Optimization_done(hyst_index,crate_index,temp_index-1,tl_index) && app.Optimization_done(hyst_index,crate_index,temp_index-1,temp_SOC_count)
                    %Interpolation zwischen den SOC Punkten %tl_index>tl_index-1
                    interpolated_value_lower_temp=zeros(1,length(app.Boundaries{hyst_index}{crate_index}{temp_index-1}{tl_index}(:,1)));
                    for n=1:length(app.Boundaries{hyst_index}{crate_index}{temp_index-1}{tl_index}(:,1))
                        interpolated_value_lower_temp(n)=(app.optimal_parameters{hyst_index}{crate_index}{temp_index-1}{tl_index}(n)-app.optimal_parameters{hyst_index}{crate_index}{temp_index-1}{temp_SOC_count}(n))*(((sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)-sorted_SOC_array{hyst_index}{crate_index}{temp_index-1}(temp_SOC_count))/(sorted_SOC_array{hyst_index}{crate_index}{temp_index-1}(tl_index)-sorted_SOC_array{hyst_index}{crate_index}{temp_index-1}(temp_SOC_count))))+app.optimal_parameters{hyst_index}{crate_index}{temp_index-1}{temp_SOC_count}(n);
                    end
                end
            end
        end
        
        if ~isempty(interpolated_value_lower_temp)
            for n=1:length(app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(:,1))
                if ~isempty(find(tau_r_indices == n))
                    potential_new_boundaries{extrapolation_count}{n,2}=interpolated_value_lower_temp(n);
                else
                    potential_new_boundaries{extrapolation_count}{n,2}=interpolated_value_lower_temp(n)*(1+2*(app.ContinousStep.Value/100)*abs((sorted_temperature_array{hyst_index}{crate_index}(temp_index)-sorted_temperature_array{hyst_index}{crate_index}(temp_index-1))/10));
                end
                potential_new_boundaries{extrapolation_count}{n,1}=interpolated_value_lower_temp(n)*(1-2*(app.ContinousStep.Value/100)*abs((sorted_temperature_array{hyst_index}{crate_index}(temp_index)-sorted_temperature_array{hyst_index}{crate_index}(temp_index-1))/10));
            end
            extrapolation_count=extrapolation_count+1;
        end
    end
end

interpolated_value_upper_temp=[];
if temp_index<length(sorted_temperature_array{hyst_index}{crate_index})
    %         tu_index=find(min(abs(sorted_SOC_array{temp_index}(SOC_index)-sorted_SOC_array{temp_index+1})));
    [~,tu_index]=min(abs(sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)-sorted_SOC_array{hyst_index}{crate_index}{temp_index+1}));
    if abs(sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)-sorted_SOC_array{hyst_index}{crate_index}{temp_index+1}(tu_index))<10
        if round(sorted_SOC_array{hyst_index}{crate_index}{temp_index+1}(tu_index)/5)*5==round(sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)/5)*5
            if app.Optimization_done(hyst_index,crate_index,temp_index+1,tu_index)
                interpolated_value_upper_temp=app.optimal_parameters{hyst_index,crate_index,temp_index+1,tu_index};
            end
        elseif round(sorted_SOC_array{hyst_index}{crate_index}{temp_index+1}(tu_index)/5)*5<round(sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)/5)*5
            if tu_index+1<=length(sorted_SOC_array{hyst_index}{crate_index}{temp_index+1})
                temp_SOC_count=tu_index+1;
                if app.Optimization_done(hyst_index,crate_index,temp_index+1,tu_index) && app.Optimization_done(hyst_index,crate_index,temp_index+1,temp_SOC_count)
                    %Interpolation zwischen den SOC Punkten tl_index+1>tl_index
                    interpolated_value_upper_temp=zeros(1,length(app.Boundaries{hyst_index}{crate_index}{temp_index+1}{tu_index}(:,1)));
                    for n=1:length(app.Boundaries{hyst_index}{crate_index}{temp_index+1}{tu_index}(:,1))
                        interpolated_value_upper_temp(n)=(app.optimal_parameters{hyst_index}{crate_index}{temp_index+1}{temp_SOC_count}(n)-app.optimal_parameters{hyst_index}{crate_index}{temp_index+1}{tu_index}(n))*(((sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)-sorted_SOC_array{hyst_index}{crate_index}{temp_index+1}(tu_index))/(sorted_SOC_array{hyst_index}{crate_index}{temp_index+1}(temp_SOC_count)-sorted_SOC_array{hyst_index}{crate_index}{temp_index+1}(tu_index))))+app.optimal_parameters{hyst_index}{crate_index}{temp_index+1}{tu_index}(n);
                    end
                end
                
            end
        elseif round(sorted_SOC_array{hyst_index}{crate_index}{temp_index+1}(tu_index)/5)*5>round(sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)/5)*5
            if tu_index-1>=1
                temp_SOC_count=tu_index-1;
                if app.Optimization_done(hyst_index,crate_index,temp_index+1,tu_index) && app.Optimization_done(hyst_index,crate_index,temp_index+1,temp_SOC_count)
                    %Interpolation zwischen den SOC Punkten %tl_index>tl_index-1
                    interpolated_value_upper_temp=zeros(1,length(app.Boundaries{hyst_index}{crate_index}{temp_index+1}{tu_index}(:,1)));
                    for n=1:length(app.Boundaries{hyst_index}{crate_index}{temp_index+1}{tu_index}(:,1))
                        interpolated_value_upper_temp(n)=(app.optimal_parameters{hyst_index}{crate_index}{temp_index+1}{tu_index}(n)-app.optimal_parameters{hyst_index}{crate_index}{temp_index+1}{temp_SOC_count}(n))*(((sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)-sorted_SOC_array{hyst_index}{crate_index}{temp_index+1}(temp_SOC_count))/(sorted_SOC_array{hyst_index}{crate_index}{temp_index+1}(tu_index)-sorted_SOC_array{hyst_index}{crate_index}{temp_index+1}(temp_SOC_count))))+app.optimal_parameters{hyst_index}{crate_index}{temp_index+1}{temp_SOC_count}(n);
                    end
                end
                
            end
        end
        
        %Extrapolation
        if ~isempty(interpolated_value_upper_temp)
            for n=1:length(app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(:,1))
                if ~isempty(find(tau_r_indices == n))
                    potential_new_boundaries{extrapolation_count}{n,1}=interpolated_value_upper_temp(n);
                else
                    potential_new_boundaries{extrapolation_count}{n,1}=interpolated_value_upper_temp(n)*(1-2*(app.ContinousStep.Value/100)*abs((sorted_temperature_array{hyst_index}{crate_index}(temp_index)-sorted_temperature_array{hyst_index}{crate_index}(temp_index+1))/10));
                end
                potential_new_boundaries{extrapolation_count}{n,2}=interpolated_value_upper_temp(n)*(1+2*(app.ContinousStep.Value/100)*abs((sorted_temperature_array{hyst_index}{crate_index}(temp_index)-sorted_temperature_array{hyst_index}{crate_index}(temp_index+1))/10));
            end
            extrapolation_count=extrapolation_count+1;
        end
        
    end
end

%extrapolation over C rate
if app.CRateFlag > 3
    % next lower crate
    interpolated_value_lower_crate=[];
    if crate_index>1
        [~,tl_index]=min(abs(sorted_temperature_array{hyst_index}{crate_index}(temp_index)-sorted_temperature_array{hyst_index}{crate_index-1}));
        [~,sl_index]=min(abs(sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)-sorted_SOC_array{hyst_index}{crate_index}{tl_index}));
        
        if abs(sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)-sorted_SOC_array{hyst_index}{crate_index}{temp_index-1}(tl_index))<10
            if round(sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)/5)*5==round(sorted_SOC_array{hyst_index}{crate_index}{temp_index-1}(tl_index)/5)*5
                if app.Optimization_done(hyst_index,crate_index,temp_index-1,tl_index)
                    interpolated_value_lower_temp=app.optimal_parameters{hyst_index}{crate_index}{temp_index-1}{tl_index};
                end
            elseif round(sorted_SOC_array{hyst_index}{crate_index}{temp_index-1}(tl_index)/5)*5<round(sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)/5)*5
                if tl_index+1<=length(sorted_SOC_array{hyst_index}{crate_index}{temp_index-1})
                    temp_SOC_count=tl_index+1;
                    if app.Optimization_done(hyst_index,crate_index,temp_index-1,tl_index) && app.Optimization_done(hyst_index,crate_index,temp_index-1,temp_SOC_count)
                        %Interpolation zwischen den SOC Punkten tl_index+1>tl_index
                        interpolated_value_lower_temp=zeros(1,length(app.Boundaries{hyst_index}{crate_index}{temp_index-1}{tl_index}(:,1)));
                        for n=1:length(app.Boundaries{hyst_index}{crate_index}{temp_index-1}{tl_index}(:,1))
                            interpolated_value_lower_temp(n)=(app.optimal_parameters{hyst_index}{crate_index}{temp_index-1}{temp_SOC_count}(n)-app.optimal_parameters{hyst_index}{crate_index}{temp_index-1}{tl_index}(n))*(((sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)-sorted_SOC_array{hyst_index}{crate_index}{temp_index-1}(tl_index))/(sorted_SOC_array{hyst_index}{crate_index}{temp_index-1}(temp_SOC_count)-sorted_SOC_array{hyst_index}{crate_index}{temp_index-1}(tl_index))))+app.optimal_parameters{hyst_index}{crate_index}{temp_index-1}{tl_index}(n);
                        end
                    end
                end
            elseif round(sorted_SOC_array{hyst_index}{crate_index}{temp_index-1}(tl_index)/5)*5>round(sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)/5)*5
                if tl_index-1>=1
                    temp_SOC_count=tl_index-1;
                    if app.Optimization_done(hyst_index,crate_index,temp_index-1,tl_index) && app.Optimization_done(hyst_index,crate_index,temp_index-1,temp_SOC_count)
                        %Interpolation zwischen den SOC Punkten %tl_index>tl_index-1
                        interpolated_value_lower_temp=zeros(1,length(app.Boundaries{hyst_index}{crate_index}{temp_index-1}{tl_index}(:,1)));
                        for n=1:length(app.Boundaries{hyst_index}{crate_index}{temp_index-1}{tl_index}(:,1))
                            interpolated_value_lower_temp(n)=(app.optimal_parameters{hyst_index}{crate_index}{temp_index-1}{tl_index}(n)-app.optimal_parameters{hyst_index}{crate_index}{temp_index-1}{temp_SOC_count}(n))*(((sorted_SOC_array{hyst_index}{crate_index}{temp_index}(SOC_index)-sorted_SOC_array{hyst_index}{crate_index}{temp_index-1}(temp_SOC_count))/(sorted_SOC_array{hyst_index}{crate_index}{temp_index-1}(tl_index)-sorted_SOC_array{hyst_index}{crate_index}{temp_index-1}(temp_SOC_count))))+app.optimal_parameters{hyst_index}{crate_index}{temp_index-1}{temp_SOC_count}(n);
                        end
                    end
                end
            end
            
            if ~isempty(interpolated_value_lower_temp)
                for n=1:length(app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(:,1))
                    if ~isempty(find(tau_r_indices == n))
                        potential_new_boundaries{extrapolation_count}{n,2}=interpolated_value_lower_temp(n);
                    else
                        potential_new_boundaries{extrapolation_count}{n,2}=interpolated_value_lower_temp(n)*(1+2*(app.ContinousStep.Value/100)*abs((sorted_temperature_array{hyst_index}{crate_index}(temp_index)-sorted_temperature_array{hyst_index}{crate_index}(temp_index-1))/10));
                    end
                    potential_new_boundaries{extrapolation_count}{n,1}=interpolated_value_lower_temp(n)*(1-2*(app.ContinousStep.Value/100)*abs((sorted_temperature_array{hyst_index}{crate_index}(temp_index)-sorted_temperature_array{hyst_index}{crate_index}(temp_index-1))/10));
                end
                extrapolation_count=extrapolation_count+1;
            end
        end
    end
    %next higher crate
    interpolated_value_upper_crate=[];
    if crate_index < length(sorted_crate_array{hyst_index})
        
    end
end

%extrapolation over hysteresis
if app.HysteresisFlag
    
end

%% Ermittle die finalen Werte

if ~isempty(potential_new_boundaries)
    extrapolated_new_boundaries=cell2mat(potential_new_boundaries{1});
    %untere Grenze aus Extrapolation aus Minimum aller verf�gbaren
    %Werte, obere Grenze als Maximum der oberen Grenzen
    for m=2:length(potential_new_boundaries)
        for k=1:length(app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(:,1))
            if min(extrapolated_new_boundaries(k,1),potential_new_boundaries{m}{k,1})<max(extrapolated_new_boundaries(k,2),potential_new_boundaries{m}{k,2})
                extrapolated_new_boundaries(k,1)=min(extrapolated_new_boundaries(k,1),potential_new_boundaries{m}{k,1});
                extrapolated_new_boundaries(k,2)=max(extrapolated_new_boundaries(k,2),potential_new_boundaries{m}{k,2});
            end
        end
    end
    
    %Nutze Maximum der unteren Grenzen und Minimum der oberen Grenzen,
    %solange diese nicht aneinander vorbeilaufen
    for n=1:length(app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(:,1))
        if max(app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}{n,1},extrapolated_new_boundaries(n,1))<min(app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}{n,2},extrapolated_new_boundaries(n,2))
            if max(app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}{n,1},extrapolated_new_boundaries(n,1))<min(app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}{n,2},extrapolated_new_boundaries(n,2))
                if ~esb_motivated_boundaries(n)
                    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}{n,1}=max(app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}{n,1},extrapolated_new_boundaries(n,1));
                    app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}{n,2}=min(app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}{n,2},extrapolated_new_boundaries(n,2));
                end
            end
        end
    end
end
[Row_B,Col_B] = size(app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index});
for n= 1:Row_B
    if app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}{n,1} < 0
        app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}{n,1} = 0;
    elseif app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}{n,2} < 0
        app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(n,:) = app.Boundaries_Default{hyst_index}{crate_index}{temp_index}{SOC_index}(n,:);
    end
end

counter = 1;
if app.UseDRTCheckBox.Value
    for n = str2num(app.GF_ECEindicesEditField.Value)
        for n1 = DRT_GUI.Fit.aktuell_Modell.ModellCell{1, 8}{1, n}.ParameterIndexes        
            app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}{n1,1} = 0.95 * process_parameter(counter);             
            app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}{n1,2} = 1.05 * process_parameter(counter);  
            counter = counter + 1;
        end
    end
end

%Parameters marked as fix get fixed at initial value
for n=1:length(app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(:,1))
    if app.ParamTable.Data{n,2}
        app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}{n,1} = app.ParamTable.Data{n,3};
        app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}{n,2} = app.ParamTable.Data{n,3};
        app.Boundaries_Default{hyst_index,crate_index,temp_index,SOC_index}{n,1} = app.ParamTable.Data{n,3};
        app.Boundaries_Default{hyst_index,crate_index,temp_index,SOC_index}{n,2} = app.ParamTable.Data{n,3};
    end
end

for  n=1:length(app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}(:,1))
    if length(app.ParamTable.Data{n,1}) >= 3
        if strcmp(app.ParamTable.Data{n,1}(1:3),'Phi')
            if app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}{n,2} > 1
                app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}{n,2} = 1;
            end
            if app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}{n,1} > 1 || app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}{n,1} < 0
                app.Boundaries{hyst_index}{crate_index}{temp_index}{SOC_index}{n,1} = 0;
            end
        end
    end
end
end

