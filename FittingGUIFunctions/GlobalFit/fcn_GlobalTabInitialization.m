function app = fcn_GlobalTabInitialization(app,event)
global DRT_GUI 
app.GlobalFitUsedModelDropDown.Value = app.ModellAuswahlPopup.Value;

app = fcn_AktualisierenButtonPushed(app, event);
% fcn_AktualisierenButton2Pushed(app, event)
%fcn_AktualisierenButton3Pushed(app, event)

if strcmp(app.ModellAuswahlPopup.Value,'LiIon4')
    app.constantPhiCheckkBox.Visible = 'on';
else
    app.constantPhiCheckkBox.Visible = 'off';
end
end

