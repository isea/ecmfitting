function app = fcn_GetPulsesfromDRTstructButtonPushed(app, event)
    
   
    
    app.GettingPulsesLamp.Color = 'red';
    pause(3)
    try
        
        %Sortiere Temperatur und SOC-Arrays
      
        fcn_BatterieNamePopup_Callback(app);
        original_temp=app.startingtemperatureDropDown.Value;
        original_SOC=app.startingSOCDropDown.Value;

        temperature_array=zeros(1,length(app.startingtemperatureDropDown.Items)-1);
        for sort_var=1:length(app.startingtemperatureDropDown.Items)-1
            if contains(app.startingtemperatureDropDown.Items{sort_var+1},'m')
                temp_str=strcat('-',app.startingtemperatureDropDown.Items{sort_var+1}(2:end-4));
            else
                temp_str=app.startingtemperatureDropDown.Items{sort_var+1}(1:end-4);
            end
            temperature_array(sort_var)=str2num(temp_str);
        end
        [sorted_temperature_array,temperature_index_rearrangement_array]=sort(temperature_array);

        for b=1:length(sorted_temperature_array)
            index1=temperature_index_rearrangement_array(b);
            app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1+index1};
            app.TemperaturPopup.Value=app.startingtemperatureDropDown.Value;
            fcn_TemperaturPopup(app, event);
            SOC_array{b}=zeros(1,length(app.startingSOCDropDown.Items)-1);
            OCV_Data{b}=zeros(1,length(app.startingSOCDropDown.Items)-1);
            vor_or_nach_relax{b}=zeros(1,length(app.startingSOCDropDown.Items)-1);
            for sort_var=1:length(app.startingSOCDropDown.Items)-1
                if contains(app.startingSOCDropDown.Items{sort_var+1},'m')
                    SOC_str=strcat('-',app.startingSOCDropDown.Items{sort_var+1}(2:end));
                else
                    SOC_str=app.startingSOCDropDown.Items{sort_var+1}(1:end);
                end
                SOC_array{b}(sort_var)=str2num(SOC_str);
            end
            [sorted_SOC_array{b},SOC_index_rearrangement_array{b}]=sort(SOC_array{b});
        end
        clear index1
        
        [OCV_Data,vor_or_nach_relax,nach,vor]=fcn_GetRelaxOCVData(app,event,sorted_temperature_array,temperature_index_rearrangement_array,sorted_SOC_array,SOC_index_rearrangement_array);
        
        
        %find maximal possible number of SOC Variations
        max_length=0;
        for b=1:length(sorted_temperature_array)
            if length(sorted_SOC_array{b})>max_length
                max_length=length(sorted_SOC_array{b});
            end
        end
        if max_length==0
            msgbox('Something went wrong')
        end
        
        for t=1:length(sorted_temperature_array)
            
            %get Shift of values
            left_shifted=0;
            non_shifted=0;
            right_shifted=0;
            
%             if vor == 1
%                 for s=2:length(sorted_SOC_array{t})
%                     left_shifted = left_shifted + abs(OCV_Data{t}(s) - vor_or_nach_relax{t}(s-1));
%                     non_shifted  =  non_shifted + abs(OCV_Data{t}(s) - vor_or_nach_relax{t}(s));
%                 end
%             elseif nach == 1
%                 for s=1:length(sorted_SOC_array{t})-1
%                     non_shifted  =  non_shifted + abs(OCV_Data{t}(s) - vor_or_nach_relax{t}(s));
%                     right_shifted  =  right_shifted + abs(OCV_Data{t}(s) - vor_or_nach_relax{t}(s+1)); 
%                 end
%             end
            
           
            for s=2:length(sorted_SOC_array{t})-1
                left_shifted  =  left_shifted + abs(OCV_Data{t}(s) - vor_or_nach_relax{t}(s-1));
                non_shifted  =  non_shifted + abs(OCV_Data{t}(s) - vor_or_nach_relax{t}(s));
                right_shifted  =  right_shifted + abs(OCV_Data{t}(s) - vor_or_nach_relax{t}(s+1)); 
            end
            if left_shifted <= right_shifted && left_shifted <= non_shifted
                shift = -1;
            elseif right_shifted <= left_shifted && right_shifted <= non_shifted
                shift = +1;
            elseif non_shifted <= right_shifted && non_shifted <= left_shifted
                shift = 0;
            end
            
            current_folder=pwd;
            current_folder=strcat(current_folder,'\Pulsdaten');
            if ~isfolder(strcat(current_folder,'\',app.BatterieNamePopup.Value))
                mkdir(current_folder,app.BatterieNamePopup.Value);
            end
            current_folder=strcat(current_folder,'\',app.BatterieNamePopup.Value);
            mkdir(current_folder,num2str(sorted_temperature_array(t)));
            current_folder=strcat(current_folder,'\',num2str(sorted_temperature_array(t)));
            for s=2:length(sorted_SOC_array{t})-1
                index1=temperature_index_rearrangement_array(t);
                if~strcmp(app.startingtemperatureDropDown.Value,app.startingtemperatureDropDown.Items{1+index1})
                    app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1+index1};
                    app.TemperaturPopup.Value=app.startingtemperatureDropDown.Value;
                    app = fcn_TemperaturPopup(app, event);
                end
                
                index2=SOC_index_rearrangement_array{t}(s+shift);
                if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                    app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                    app.SOCPopup.Value=app.startingSOCDropDown.Value;
                    app = fcn_SOCPopup(app, event);
                    if ~strcmp(app.ModellAuswahlPopup.Value,'R2RC')
                        app.ModellAuswahlPopup.Value='R2RC';
                        app = fcn_ModellAuswahlPopup(app);
                        app = fcn_BatterieNamePopup_Callback(app);
                    end
                end
                
                global DRT_GUI 
                if isfield(DRT_GUI.Messdaten,'pulse')
                    diga.daten.Programmdauer = DRT_GUI.Messdaten.pulse.zeit;
                    diga.daten.Spannung = DRT_GUI.Messdaten.pulse.spannung;
                    diga.daten.Strom = DRT_GUI.Messdaten.pulse.strom;
                elseif isfield(DRT_GUI.Messdaten,'vor_relax')
                    diga.daten.Programmdauer = DRT_GUI.Messdaten.vor_relax.zeit;
                    diga.daten.Spannung = DRT_GUI.Messdaten.vor_relax.spannung;
                    diga.daten.Strom = DRT_GUI.Messdaten.vor_relax.strom;
                elseif isfield(DRT_GUI.Messdaten,'nach_relax')
                    diga.daten.Programmdauer = DRT_GUI.Messdaten.nach_relax.zeit;
                    diga.daten.Spannung = DRT_GUI.Messdaten.nach_relax.spannung;
                    diga.daten.Strom = DRT_GUI.Messdaten.nach_relax.strom;
                end
                
                if ~isempty(diga)
                    OutputFolderName=sprintf('%s_%s_%s.mat',app.BatterieNamePopup.Value,app.TemperaturPopup.Value,app.SOCPopup.Value);
                    save(strcat(current_folder,'\',OutputFolderName),'diga')
                end
            end
            clear diga
        end
        
        app.GettingPulsesLamp.Color = 'green';
    catch
        msgbox('Something went wrong');
    end
end

