function app = fcn_UsedTDDataButtonGroupSelectionChanged(app, event)
if app.UsedTDDataButtonGroup.SelectedObject == app.UsedTDDataButtonGroup.Children(1)
    app.UsedTDDataButtonGroup_2.SelectedObject = app.UsedTDDataButtonGroup_2.Children(1);
    set(app.TDF_stromloseMesspunkteCheckbox,'Value',true)
elseif app.UsedTDDataButtonGroup.SelectedObject == app.UsedTDDataButtonGroup.Children(2)
    app.UsedTDDataButtonGroup_2.SelectedObject = app.UsedTDDataButtonGroup_2.Children(2);
    set(app.TDF_stromloseMesspunkteCheckbox,'Value',false)
else
    app.UsedTDDataButtonGroup_2.SelectedObject = app.UsedTDDataButtonGroup_2.Children(3);
    set(app.TDF_stromloseMesspunkteCheckbox,'Value',true)
end
end

