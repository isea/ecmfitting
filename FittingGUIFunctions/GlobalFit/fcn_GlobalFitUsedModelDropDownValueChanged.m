function app = fcn_GlobalFitUsedModelDropDownValueChanged(app, event)
global DRT_GUI
%M�glichkeit f�r genannte Modelle alle Phi-Werte auf 1 festzulegen, um eine
%vereinfachte Transformation in den Zeitbereich zu erm�glichen
if strcmp(app.GlobalFitUsedModelDropDown.Value,'LiIon4') || strcmp(app.GlobalFitUsedModelDropDown.Value,'LiIon4_sbi')
    app.constantPhiCheckkBox.Enable = 'on'; 
else
    app.constantPhiCheckkBox.Enable = 'off';
end

%Lade ausgew�hltes Modell
if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalFitUsedModelDropDown.Value)
    app.ModellAuswahlPopup.Value=app.GlobalFitUsedModelDropDown.Value;
    app = fcn_ModellAuswahlPopup(app);
%     fcn_BatterieNamePopup_Callback(app);
end

%Lade Esb-Elemente zur Auswahl in Tabelle
app = fcn_AktualisierenButtonPushed(app, event);
% app = fcn_AktualisierenButton2Pushed(app, event);
end

