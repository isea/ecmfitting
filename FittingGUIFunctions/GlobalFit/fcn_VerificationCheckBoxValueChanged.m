function app = fcn_VerificationCheckBoxValueChanged(app, event)
    if app.VerificationCheckBox.Value
        app.UsedTDDataForVerificationButtonGroup.Enable = 'on';
        app.RelaxButtonForVerification.Enable = 'on';
        app.PulsesButtonForVerification.Enable = 'on';
    else
        app.UsedTDDataForVerificationButtonGroup.Enable = 'off';
        app.RelaxButtonForVerification.Enable = 'off';
        app.PulsesButtonForVerification.Enable = 'off';
    end
end

