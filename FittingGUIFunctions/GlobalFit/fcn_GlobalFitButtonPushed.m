function  app = fcn_GlobalFitButtonPushed(app,event)
% tic
app.tdf_postprocessing_started = 0;
app.start_tupel_optimized = 0;
app.start_tupel_optimized_tdf = 0;
app.start_tupel_optimized_idx = 1;
app.start_tupel_optimized_results = [];
app.GlobalFitInProgress = 1;
Modell = app.GlobalFitUsedModelDropDown.Value;

if app.LHDCheckBox.Value
    fcn_GF_FittetParametersButtonGroupSelectionChanged(app, event); 
end

global DRT_GUI;
warning('off','MATLAB:handle_graphics:Canvas:RenderingException')
app.tdf_esb_element = app.ZeitbereichsfittingParameterAuswahlTable.Data;
app.value_ranges = app.CustomRangesTable.Data;
app.BusyLabel.Visible = 'on';
app.GlobalFitLamp.Color = 'red';
if app.NelderMeadalgorithmDownhillSimplexButton.Value
    app.used_optimization_algorithm = [];
elseif app.SimulatedAnnealingButton.Value
    app.used_optimization_algorithm = 'SimAnn';
elseif app.ParticleSwarmButton.Value
    app.used_optimization_algorithm = 'PaSw';
elseif app.GeneticAlgorithmButton.Value
    app.used_optimization_algorithm = 'GenAlg';
elseif app.MultistartButton.Value
    app.used_optimization_algorithm = 'GloSea';
end
pause(3)
try
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                                      %
    %                                      %
    %           Data management            %
    %                                      %
    %                                      %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %get fixed parameters
    fixed_counter = 1;
    for n = 1:length(app.ParamTable.Data(:,1))
        if app.ParamTable.Data{n,2}
            app.global_fixed_parameters(fixed_counter) = 1;
            fixed_counter = fixed_counter + 1;
        else
            app.global_fixed_parameters(fixed_counter) = 0;
            fixed_counter = fixed_counter + 1;
        end
    end
    
    %Sort temperatures, SOCs, C rates and hysteresis states in ascending
    %order
    app.ModellAuswahlPopup.Value=app.GlobalFitUsedModelDropDown.Value;
    app = fcn_ModellAuswahlPopup(app);
    app = fcn_BatterieNamePopup(app);
    original_temp=app.startingtemperatureDropDown.Value;
    original_SOC=app.startingSOCDropDown.Value;
    if app.HysteresisFlag
        original_Hysteresis = app.ZustandPopup_2.Value;
    else
        original_Hysteresis = 1;
    end
    if app.CRateFlag
        original_crate = app.ZustandPopup.Value;
    else
        original_crate = [];
    end
    %hysteresis
    hysteresis_state_array = [];
    sorted_hysteresis_state_array = [];
    hysteresis_state_index_rearrangement_array = [];
    if app.HysteresisFlag
        for hyst_idx = 1:length(app.ZustandPopup_2.Items)-1
            if contains(app.ZustandPopup_2.Items{hyst_idx+1},'m')
                hyst_string=strcat('-',app.ZustandPopup_2.Items{hyst_idx+1}(2:end-1));
            else
                hyst_string=app.ZustandPopup_2.Items{hyst_idx+1}(1:end-1);
            end
            hysteresis_state_array(hyst_idx) = str2num(hyst_string);
        end
    else
        hysteresis_state_array(1) = 0;
    end
    [sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array]=sort(hysteresis_state_array);
    %C rate
    sorted_crate_array = cell(1,length(sorted_hysteresis_state_array));
    crate_array = cell(1,length(sorted_hysteresis_state_array));
    crate_index_rearrangement_array = cell(1,length(sorted_hysteresis_state_array));
    if app.CRateFlag
        for hyst_idx = 1:length(sorted_hysteresis_state_array)
            if app.HysteresisFlag
                index=hysteresis_state_index_rearrangement_array(hyst_idx);
                app.ZustandPopup_2.Value=app.ZustandPopup_2.Items{1+index};
                app = fcn_ZustandPopup(app, event);
            end
            crate_array{hyst_idx}=zeros(1,length(app.ZustandPopup.Items)-1);
            for crate_idx = 1:length(app.ZustandPopup.Items)-1
                if contains(app.ZustandPopup.Items{crate_idx+1},'m')
                    crate_string=strrep(strcat('-',app.ZustandPopup.Items{crate_idx+1}(2:end-4)),'_','.');
                else
                    crate_string=strrep(app.ZustandPopup.Items{crate_idx+1}(1:end-4),'_','.');
                end
                crate_array{hyst_idx}(crate_idx) = str2num(crate_string);
            end
            [sorted_crate_array{hyst_idx}, crate_index_rearrangement_array{hyst_idx}] = sort(crate_array{hyst_idx});
        end
    else
        for hyst_idx = 1:length(sorted_hysteresis_state_array)
            crate_array{hyst_idx}(1) = 0;
            [sorted_crate_array{hyst_idx}, crate_index_rearrangement_array{hyst_idx}] = sort(crate_array{hyst_idx});
        end
    end
    %temperature
    for hyst_idx = 1:length(sorted_hysteresis_state_array)
        if app.HysteresisFlag
            index=hysteresis_state_index_rearrangement_array(hyst_idx);
            app.ZustandPopup_2.Value=app.ZustandPopup_2.Items{1+index};
            app = fcn_ZustandPopup(app, event);
        end
        for crate_idx = 1:length(sorted_crate_array{hyst_idx})
            if app.CRateFlag
                index=crate_index_rearrangement_array{hyst_idx}(crate_idx);
                app.ZustandPopup.Value=app.ZustandPopup.Items{1+index};
                app = fcn_ZustandPopup(app, event);
            end
            temperature_array{hyst_idx}{crate_idx} = zeros(1,length(app.TemperaturPopup.Items)-1);
            sorted_temperature_array{hyst_idx}{crate_idx} = zeros(1,length(app.TemperaturPopup.Items)-1);
            temperature_index_rearrangement_array{hyst_idx}{crate_idx} = zeros(1,length(app.TemperaturPopup.Items)-1);
            for SOC_idx=1:length(app.TemperaturPopup.Items)-1
                if contains(app.TemperaturPopup.Items{SOC_idx+1},'m')
                    temp_string=strcat('-',app.TemperaturPopup.Items{SOC_idx+1}(2:end-4));
                else
                    temp_string=app.TemperaturPopup.Items{SOC_idx+1}(1:end-4);
                end
                temperature_array{hyst_idx}{crate_idx}(SOC_idx)=str2num(temp_string);
            end
            [sorted_temperature_array{hyst_idx}{crate_idx},temperature_index_rearrangement_array{hyst_idx}{crate_idx}]=sort(temperature_array{hyst_idx}{crate_idx});
        end
    end
    %SOC
    for hyst_idx = 1:length(sorted_hysteresis_state_array)
        if app.HysteresisFlag
            index=hysteresis_state_index_rearrangement_array(hyst_idx);
            app.ZustandPopup_2.Value=app.ZustandPopup_2.Items{1+index};
            app = fcn_ZustandPopup(app, event);
        end
        for crate_idx = 1:length(sorted_crate_array{hyst_idx})
            if app.CRateFlag
                index=crate_index_rearrangement_array{hyst_idx}(crate_idx);
                app.ZustandPopup.Value=app.ZustandPopup.Items{1+index};
                app = fcn_ZustandPopup(app, event);
            end
            for temp_idx=1:length(sorted_temperature_array{hyst_idx}{crate_idx})
                index=temperature_index_rearrangement_array{hyst_idx}{crate_idx}(temp_idx);
                app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1+index};
                app.TemperaturPopup.Value=app.startingtemperatureDropDown.Value;
                app = fcn_TemperaturPopup(app, event);
                SOC_array{hyst_idx}{crate_idx}{temp_idx}=zeros(1,length(app.startingSOCDropDown.Items)-1);
                sorted_SOC_array{hyst_idx}{crate_idx}{temp_idx}=zeros(1,length(app.startingSOCDropDown.Items)-1);
                SOC_index_rearrangement_array{hyst_idx}{crate_idx}{temp_idx}=zeros(1,length(app.startingSOCDropDown.Items)-1);
                for SOC_idx=1:length(app.startingSOCDropDown.Items)-1
                    if contains(app.startingSOCDropDown.Items{SOC_idx+1},'m')
                        SOC_string=strcat('-',app.startingSOCDropDown.Items{SOC_idx+1}(2:end));
                    else
                        SOC_string=app.startingSOCDropDown.Items{SOC_idx+1}(1:end);
                    end
                    SOC_array{hyst_idx}{crate_idx}{temp_idx}(SOC_idx)=str2num(SOC_string);
                end
                [sorted_SOC_array{hyst_idx}{crate_idx}{temp_idx},SOC_index_rearrangement_array{hyst_idx}{crate_idx}{temp_idx}]=sort(SOC_array{hyst_idx}{crate_idx}{temp_idx});
            end
        end
    end
    clear index
    
    %find maximal possible number of variations for each dimension
    min_length_crate = Inf;
    max_length_crate = 0;
    for hyst_idx = 1:length(sorted_hysteresis_state_array)
        if length(sorted_crate_array{hyst_idx})>max_length_crate
            max_length_crate = length(sorted_crate_array{hyst_idx});
        end
        if length(sorted_crate_array{hyst_idx})<min_length_crate
            min_length_crate = length(sorted_crate_array{hyst_idx});
        end
    end
    if max_length_crate==0
        msgbox('Error in line 177! No C rate found')
        return
    end
    
    min_length_temp = Inf;
    max_length_temp = 0;
    for hyst_idx = 1:length(sorted_hysteresis_state_array)
        for crate_idx = 1:length(sorted_crate_array{hyst_idx})
            if length(sorted_temperature_array{hyst_idx}{crate_idx})>max_length_temp
                max_length_temp = length(sorted_temperature_array{hyst_idx}{crate_idx});
            end
            if length(sorted_temperature_array{hyst_idx}{crate_idx})<min_length_temp
                min_length_temp = length(sorted_temperature_array{hyst_idx}{crate_idx});
            end
        end
    end
    if max_length_temp==0
        msgbox('Error in line 190! No temperature rate found')
        return
    end
    
    min_length_SOC = Inf;
    max_length_SOC = 0;
    for hyst_idx = 1:length(sorted_hysteresis_state_array)
        for crate_idx = 1:length(sorted_crate_array{hyst_idx})
            for temp_idx=1:length(sorted_temperature_array{hyst_idx}{crate_idx})
                if length(sorted_SOC_array{hyst_idx}{crate_idx}{temp_idx})>max_length_SOC
                    max_length_SOC=length(sorted_SOC_array{hyst_idx}{crate_idx}{temp_idx});
                end
                if length(sorted_SOC_array{hyst_idx}{crate_idx}{temp_idx})<min_length_SOC
                    min_length_SOC = length(sorted_SOC_array{hyst_idx}{crate_idx}{temp_idx});
                end
            end
        end
    end
    if max_length_SOC==0
        msgbox('Error in line 205! No SOC found')
        return
    end
    
    %initialization of
    app.Boundaries = cell(length(sorted_hysteresis_state_array),max_length_crate,max_length_temp,max_length_SOC);
    app.Boundaries_Default = cell(length(sorted_hysteresis_state_array),max_length_crate,max_length_temp,max_length_SOC);
    app.Optimization_done=zeros(length(sorted_hysteresis_state_array),max_length_crate,max_length_temp,max_length_SOC);
    app.TDF_Optimization_done = zeros(length(sorted_hysteresis_state_array),max_length_crate,max_length_temp,max_length_SOC);
    app.RMSE = cell(length(sorted_hysteresis_state_array),max_length_crate,max_length_temp,max_length_SOC);
    app.parameters = cell(length(sorted_hysteresis_state_array),max_length_crate,max_length_temp,max_length_SOC);
    app.optimal_parameters = cell(length(sorted_hysteresis_state_array),max_length_crate,max_length_temp,max_length_SOC);
    app.tdf_optimal_parameters = cell(length(sorted_hysteresis_state_array),max_length_crate,max_length_temp,max_length_SOC);
    app.TDF_Boundaries = cell(length(sorted_hysteresis_state_array),max_length_crate,max_length_temp,max_length_SOC);
    app.LHD_Time_Constants = cell(length(sorted_hysteresis_state_array),max_length_crate,max_length_temp,max_length_SOC);
    for hyst_idx = 1:length(sorted_hysteresis_state_array)
        for crate_idx = 1:length(sorted_crate_array{hyst_idx})
            for temp_idx=1:length(sorted_temperature_array{hyst_idx}{crate_idx})
                for m=1:max_length_SOC
                    app.Boundaries_Default{hyst_idx}{crate_idx}{temp_idx}{m}=app.ParamTable.Data(:,4:5);
                    app.Boundaries{hyst_idx}{crate_idx}{temp_idx}{m}=cell(size(app.ParamTable.Data(:,4:5)));
                    app.RMSE{hyst_idx}{crate_idx}{temp_idx}{m}=zeros(1,2);
                    app.parameters{hyst_idx}{crate_idx}{temp_idx}{m}=zeros(size(transpose(app.ParamTable.Data(:,1:2))));
                    app.optimal_parameters{hyst_idx}{crate_idx}{temp_idx}{m}=zeros(size(transpose(app.ParamTable.Data(:,1))));
                end
            end
        end
    end
    
    %identify index of starting states
    if app.HysteresisFlag
        app.ZustandPopup_2.Value=original_Hysteresis;
        app = fcn_ZustandPopup(app, event);
        Hysteresis_index=find(strcmp(app.ZustandPopup_2.Items,app.ZustandPopup_2.Value));
        Hysteresis_index=find(hysteresis_state_index_rearrangement_array==Hysteresis_index-1);
    else
        Hysteresis_index = 1;
    end
    
    if app.CRateFlag
        app.ZustandPopup.Value=original_crate;
        app = fcn_ZustandPopup(app, event);
        Crate_index1=find(strcmp(app.ZustandPopup.Items,app.ZustandPopup.Value));
        Crate_index1=find(crate_index_rearrangement_array{Hysteresis_index}==Crate_index1-1);
        
        %todo how does 0C interact with hysteresis dependency
        app.static_EIS_index = find(sorted_crate_array{1} == 0);
    else
        Crate_index1 = 1;
    end
    
    app.startingtemperatureDropDown.Value = original_temp;
    app.TemperaturPopup.Value = original_temp;
    app = fcn_TemperaturPopup(app, event);
    temp_index1 = find(strcmp(app.startingtemperatureDropDown.Items,app.startingtemperatureDropDown.Value));
    temp_index1 = find(temperature_index_rearrangement_array{Hysteresis_index}{Crate_index1}==temp_index1-1);
    
    app.startingSOCDropDown.Value=original_SOC;
    app.SOCPopup.Value=original_SOC;
    app = fcn_SOCPopup(app, event);
    SOC_index1=find(strcmp(app.startingSOCDropDown.Items,app.startingSOCDropDown.Value));
    SOC_index1=find(SOC_index_rearrangement_array{Hysteresis_index}{Crate_index1}{temp_index1}==SOC_index1-1);    
        
    
    %initial SOC index for hysteresis, C rate and temperature
    init_SOC = sorted_SOC_array{Hysteresis_index}{Crate_index1}{temp_index1}(SOC_index1);
    SOC_index{hyst_idx}{crate_idx}(temp_index1) = SOC_index1;
    for hyst_idx = 1:length(sorted_hysteresis_state_array)
        for crate_idx = 1:length(sorted_crate_array{hyst_idx})
            for temp_idx=1:length(sorted_temperature_array{hyst_idx}{crate_idx})
                [~,SOC_index{hyst_idx}{crate_idx}(temp_idx)] = min(abs(sorted_SOC_array{hyst_idx}{crate_idx}{temp_idx}-init_SOC));
            end
        end
    end
    
    init_temp = sorted_temperature_array{Hysteresis_index}{Crate_index1}(temp_index1);
    temp_index{hyst_idx}(Crate_index1) = temp_index1;
    for hyst_idx = 1:length(sorted_hysteresis_state_array)
        for crate_idx = 1:length(sorted_crate_array{hyst_idx})            
            [~,temp_index{hyst_idx}(crate_idx)] = min(abs(sorted_temperature_array{hyst_idx}{crate_idx}-init_temp));            
        end
    end
    
    init_crate = sorted_crate_array{Hysteresis_index}(Crate_index1);
    Crate_index(hyst_idx) = Crate_index1;
    for hyst_idx = 1:length(sorted_hysteresis_state_array)
        [~,Crate_index(hyst_idx)] = min(abs(sorted_crate_array{hyst_idx}-init_crate));            
    end
    
    %Final initialization
    waitbar_num = 0;
    for waitbar_index = 1:length(sorted_SOC_array)
        waitbar_num = waitbar_num + length(sorted_SOC_array{waitbar_index});
    end
    waitbar_counter = 1;
    app.current_temp_index=temp_index1;
    app.current_SOC_index=SOC_index1;
    app.current_hyst_index = Hysteresis_index;
    app.current_crate_index = Crate_index1;
    app.GlobalUsedModell = Modell;    
    if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
        app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
        app = fcn_ModellAuswahlPopup(app);
        app = fcn_BatterieNamePopup(app);
    end
    fcn_CancelCheck(app);
    
    if app.HysteresisFlag
        [~,app.default_hyst_index] = min(abs(sorted_hysteresis_state_array));
    else
        app.default_hyst_index = 1;
    end
    if app.CRateFlag
        [~,app.default_crate_index] = min(abs(sorted_crate_array{app.default_hyst_index}));
    else
        app.default_crate_index = 1;
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                                      %
    %                                      %
    %     Frequency Domain Fitting         %
    %                                      %
    %                                      %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if  app.FrequenzbereichsfittingCheckBox.Value
        disp('Start of fitting using EIS data')
%         waitbar_txt = ['Fitting anhand von EIS-Daten. Zustand: ',num2str(waitbar_counter),'/',num2str(waitbar_num)];
%         process_waitbar = waitbar(waitbar_counter/waitbar_num,waitbar_txt);
        fcn_GlobalFit_FDF(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
                               sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array );       
        disp('End of fitting using EIS data')
%         close(process_waitbar)
        saving_struct.RMSE=app.RMSE;
        saving_struct.Boundaries=app.Boundaries;
        saving_struct.Boundaries_Default=app.Boundaries_Default;
        saving_struct.parameters=app.parameters;
        saving_struct.optimal_parameters=app.optimal_parameters;
        saving_struct.SOC_states=sorted_SOC_array;
        saving_struct.Temperature_states=sorted_temperature_array;       
        saving_struct.Hysteresis_states=sorted_hysteresis_state_array;  
        saving_struct.Crate_states=sorted_crate_array;  
        currentTime=clock;
        current_folder=pwd;
        current_folder=strcat(current_folder,'\GlobalData');
        OutputFolderName=sprintf('%s_%s_%s_%s_%s_%s_%s',app.BatterieNamePopup.Value,num2str(currentTime(1)),num2str(currentTime(2)),num2str(currentTime(3)), ...
            num2str(currentTime(4)),num2str(currentTime(5)));
        save(strcat(current_folder,'\',OutputFolderName),'-regexp','^(?!(app|event)$).')
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                                      %
    %                                      %
    %           Post processing            %
    %                                      %
    %                                      %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if app.Nachbereitung.Value
        fcn_CancelCheck(app);
        disp('Start of PostProcessing')
        [app,saving_struct] = fcn_GlobalFit_FDF_PP(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
                                sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array );
        disp('End of PostProcessing')
    end
    %close(process_waitbar)
    saving_struct_1.RMSE=app.RMSE;
    saving_struct_1.Boundaries=app.Boundaries;
    saving_struct_1.Boundaries_Default=app.Boundaries_Default;
    saving_struct_1.parameters=app.parameters;
    saving_struct_1.optimal_parameters=app.optimal_parameters;
    saving_struct_1.SOC_states=sorted_SOC_array;
    saving_struct_1.Temperature_states=sorted_temperature_array;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                                      %
    %                                      %
    %         Time Domain Fitting          %
    %                                      %
    %                                      %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if  app.ZeitbereichsfittingCheckBox.Value
        disp('Start of fitting using time domain data')
        app.current_temp_index=temp_index1;
        app.current_SOC_index=SOC_index1;
        app.current_hyst_index = Hysteresis_index;
        app.current_crate_index = Crate_index1;
        fcn_GlobalFit_TDF(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
                               sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array);
        disp('End of fitting using time domain data')
        saving_struct.TDF_Boundaries=app.TDF_Boundaries;
        saving_struct.parameters=app.parameters;
        saving_struct.tdf_optimal_parameters=app.tdf_optimal_parameters;
        saving_struct.SOC_states=sorted_SOC_array;
        saving_struct.Temperature_states=sorted_temperature_array;
        saving_struct.Hysteresis_states=sorted_hysteresis_state_array;  
        saving_struct.Crate_states=sorted_crate_array;  
        saving_struct.ZeitbereichsfittingParameterAuswahlTable = app.ZeitbereichsfittingParameterAuswahlTable.Data;
        saving_struct.CustomRangesTable = app.CustomRangesTable.Data;
        saving_struct.ZeitbereichsgrenzextrapolationCheckBox = app.ZeitbereichsgrenzextrapolationCheckBox.Value;
        if app.BothButton.Value
            saving_struct.UsedTDData = 'Both';
        elseif  app.PulsesButton.Value
            saving_struct.UsedTDData = 'Pulse';
        else
            saving_struct.UsedTDData = 'Relax';
        end
        currentTime=clock;
        current_folder=pwd;
        current_folder=strcat(current_folder,'\GlobalData');
        OutputFolderName=sprintf('TDF_%s_%s_%s_%s_%s_%s_%s',app.BatterieNamePopup.Value,num2str(currentTime(1)),num2str(currentTime(2)),num2str(currentTime(3)), ...
            num2str(currentTime(4)),num2str(currentTime(5)));
        save(strcat(current_folder,'\',OutputFolderName),'-regexp','^(?!(app|event)$).')
    end
    %     a3 = toc;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                                      %
    %                                      %
    %           Post Processing            %
    %        Time Domain Fitting           %
    %                                      %
    %                                      %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    fcn_CancelCheck(app);    
    if app.Nachbereitung_TDF.Value
        disp('Start of TDF PostProcessing')
        [app,saving_struct] = fcn_GlobalFit_TDF_PP(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
                                sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array );
        disp('End of TDF PostProcessing')
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                                      %
    %                                      %
    %            Verification              %
    %                                      %
    %                                      %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    fcn_CancelCheck(app);
    if app.VerificationCheckBox.Value
        autofit_path = cd;
        disp('Start of verification using time domain data')
        app = fcn_GlobalFit_Verification(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
                    sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array,max_length_crate,max_length_temp);
        disp('End of verification using time domain data')
        cd(autofit_path)
    end    
    fcn_CancelCheck(app);
    app.BusyLabel.Visible = 'off';
    app.GlobalFitLamp.Color = 'green';
    app.GlobalFitInProgress = 0;
    warning('on','MATLAB:handle_graphics:Canvas:RenderingException')
    disp('GlobalFit finished!')
catch e
    if app.GUI
    warning('on','MATLAB:handle_graphics:Canvas:RenderingException')
    app.BusyLabel.Visible = 'off';
    app.GlobalFitLamp.Color = 'green';
    app.GlobalFitInProgress = 0;
    rethrow(e)    
end
% toc
end