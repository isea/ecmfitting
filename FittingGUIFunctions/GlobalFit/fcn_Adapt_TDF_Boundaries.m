function app = fcn_Adapt_TDF_Boundaries(app,TDF_index,hyst,crate,temp_index,SOC_index,sorted_temperature_array,sorted_SOC_array,sorted_hysteresis_state_array,sorted_crate_array)
    global DRT_GUI    
    DataMatrix_RelaxData = app.TDF_RelaxTable.Data;    
    for n = 5:length(app.TDF_RelaxTable.Data(:,1))
        tdf_min(n-4) = app.TDF_RelaxTable.Data{n,4};
        tdf_max(n-4) = app.TDF_RelaxTable.Data{n,5};
    end
    %TDF_index(n)
    extrapolation_count=1;
    potential_new_boundaries=[];
    %Check if respective SOC/T Values have been optimized for current SOC/T
    %values 
    if SOC_index>1
        sl_index=SOC_index-1;
        if app.TDF_Optimization_done(hyst,crate,temp_index,sl_index)
            for n=1:length(TDF_index)
                 potential_new_boundaries{extrapolation_count}{TDF_index(n),1}=(1-((app.ContinousStep.Value/(100*10))*(sorted_SOC_array{hyst}{crate}{temp_index}(SOC_index)-sorted_SOC_array{hyst}{crate}{temp_index}(sl_index))))*app.tdf_optimal_parameters{hyst}{crate}{temp_index}{sl_index}(TDF_index(n));
                 potential_new_boundaries{extrapolation_count}{TDF_index(n),2}=(1+((app.ContinousStep.Value/(100*10))*(sorted_SOC_array{hyst}{crate}{temp_index}(SOC_index)-sorted_SOC_array{hyst}{crate}{temp_index}(sl_index))))*app.tdf_optimal_parameters{hyst}{crate}{temp_index}{sl_index}(TDF_index(n));
            end
            extrapolation_count=extrapolation_count+1;
        end
    end

    %hier
    if SOC_index<length(sorted_SOC_array{temp_index})
        su_index=SOC_index+1;
        if app.TDF_Optimization_done(hyst,crate,temp_index,su_index)
            for n=1:length(TDF_index)
                %nicht von boundaries sondern von Optimalem Wert
    %              potential_new_boundaries{2}{n,1}=(app.Boundaries{temp_index,su_index}(n,1))/(1-((app.ContinousStep.Value/10)*abs(sorted_SOC_array{temp}(SOC_index)-sorted_SOC_array{temp}(su_index))));
    %              potential_new_boundaries{2}{n,2}=(app.Boundaries{temp_index,su_index}(n,2))/(1+((app.ContinousStep.Value/10)*abs(sorted_SOC_array{temp}(SOC_index)-sorted_SOC_array{temp}(su_index))));
                 potential_new_boundaries{extrapolation_count}{TDF_index(n),1}=(app.tdf_optimal_parameters{hyst}{crate}{temp_index}{su_index}(TDF_index(n)))*(1+((app.ContinousStep.Value/(100*10))*(sorted_SOC_array{hyst}{crate}{temp_index}(SOC_index)-sorted_SOC_array{hyst}{crate}{temp_index}(su_index))));
                 potential_new_boundaries{extrapolation_count}{TDF_index(n),2}=(app.tdf_optimal_parameters{hyst}{crate}{temp_index}{su_index}(TDF_index(n)))*(1-((app.ContinousStep.Value/(100*10))*(sorted_SOC_array{hyst}{crate}{temp_index}(SOC_index)-sorted_SOC_array{hyst}{crate}{temp_index}(su_index))));
            end
            extrapolation_count=extrapolation_count+1;
        end
    end

    %n�chste niedrigere Temperatur
    interpolated_value_lower_temp=[];
    if temp_index>1
        [~,tl_index]=min(abs(sorted_SOC_array{hyst}{crate}{temp_index}(SOC_index)-sorted_SOC_array{hyst}{crate}{temp_index-1}));
        if abs(sorted_SOC_array{hyst}{crate}{temp_index}(SOC_index)-sorted_SOC_array{hyst}{crate}{temp_index-1}(tl_index))<10
            if round(sorted_SOC_array{hyst}{crate}{temp_index}(SOC_index)/5)*5==round(sorted_SOC_array{hyst}{crate}{temp_index-1}(tl_index)/5)*5
                if app.TDF_Optimization_done(hyst,crate,temp_index-1,tl_index)
                    interpolated_value_lower_temp=app.tdf_optimal_parameters{hyst}{crate}{temp_index-1}{tl_index};
                end
            elseif round(sorted_SOC_array{hyst}{crate}{temp_index-1}(tl_index)/5)*5<round(sorted_SOC_array{hyst}{crate}{temp_index}(SOC_index)/5)*5
                if tl_index+1<=length(sorted_SOC_array{hyst}{crate}{temp_index-1})
                    temp_SOC_count=tl_index+1;
                    if app.TDF_Optimization_done(hyst,crate,temp_index-1,tl_index) && app.TDF_Optimization_done(hyst,crate,temp_index-1,temp_SOC_count) 
                        %Interpolation zwischen den SOC Punkten tl_index+1>tl_index
                        interpolated_value_lower_temp=zeros(1,length(TDF_index));
                        for n=1:length(TDF_index)
                            interpolated_value_lower_temp(TDF_index(n))=(app.tdf_optimal_parameters{hyst}{crate}{temp_index-1}{temp_SOC_count}(TDF_index(n))-app.tdf_optimal_parameters{hyst}{crate}{temp_index-1}{tl_index}(TDF_index(n)))*(((sorted_SOC_array{hyst}{crate}{temp_index}(SOC_index)-sorted_SOC_array{hyst}{crate}{temp_index-1}(tl_index))/(sorted_SOC_array{hyst}{crate}{temp_index-1}(temp_SOC_count)-sorted_SOC_array{hyst}{crate}{temp_index-1}(tl_index))))+app.tdf_optimal_parameters{hyst}{crate}{temp_index-1}{tl_index}(TDF_index(n));
                        end
                    end
                end
            elseif round(sorted_SOC_array{hyst}{crate}{temp_index-1}(tl_index)/5)*5>round(sorted_SOC_array{hyst}{crate}{temp_index}(SOC_index)/5)*5
                if tl_index-1>=1
                    temp_SOC_count=tl_index-1;
                    if app.TDF_Optimization_done(hyst,crate,temp_index-1,tl_index) && app.TDF_Optimization_done(hyst,crate,temp_index-1,temp_SOC_count)
                        %Interpolation zwischen den SOC Punkten %tl_index>tl_index-1
                        interpolated_value_lower_temp=zeros(1,length(TDF_index));
                        for n=1:length(TDF_index)
                            interpolated_value_lower_temp(TDF_index(n))=(app.tdf_optimal_parameters{hyst}{crate}{temp_index-1}{tl_index}(TDF_index(n))-app.tdf_optimal_parameters{hyst}{crate}{temp_index-1}{temp_SOC_count}(TDF_index(n)))*(((sorted_SOC_array{hyst}{crate}{temp_index}(SOC_index)-sorted_SOC_array{hyst}{crate}{temp_index-1}(temp_SOC_count))/(sorted_SOC_array{hyst}{crate}{temp_index-1}(tl_index)-sorted_SOC_array{hyst}{crate}{temp_index-1}(temp_SOC_count))))+app.tdf_optimal_parameters{hyst}{crate}{temp_index-1}{temp_SOC_count}(TDF_index(n));
                        end
                    end
                end
            end

            %hier bin ich
            %Extrapolation auf n�chsten Punkt
    %         b2(n)=exp((log(b2(n-1))+1/t(n)-1/t(n-1))/(1+0.3));
    %         c2(n)=exp((log(c2(n-1))+1/t(n)-1/t(n-1))/(1-0.3));
              if ~isempty(interpolated_value_lower_temp)
                 for n=1:length(TDF_index)
                    potential_new_boundaries{extrapolation_count}{TDF_index(n),1}=interpolated_value_lower_temp(TDF_index(n))*(1-2*(app.ContinousStep.Value/100)*abs((sorted_temperature_array{hyst}{crate}(temp_index)-sorted_temperature_array{hyst}{crate}(temp_index-1))/10));
                    potential_new_boundaries{extrapolation_count}{TDF_index(n),2}=interpolated_value_lower_temp(TDF_index(n))*(1+2*(app.ContinousStep.Value/100)*abs((sorted_temperature_array{hyst}{crate}(temp_index)-sorted_temperature_array{hyst}{crate}(temp_index-1))/10));
                 end
                 extrapolation_count=extrapolation_count+1;
              end
        end
    end

    %hier
    interpolated_value_upper_temp=[];
    if temp_index<length(sorted_temperature_array{hyst}{crate})
        [~,tu_index]=min(abs(sorted_SOC_array{hyst}{crate}{temp_index}(SOC_index)-sorted_SOC_array{hyst}{crate}{temp_index+1}));
        if abs(sorted_SOC_array{hyst}{crate}{temp_index}(SOC_index)-sorted_SOC_array{hyst}{crate}{temp_index+1}(tu_index))<10
            if round(sorted_SOC_array{hyst}{crate}{temp_index+1}(tu_index)/5)*5==round(sorted_SOC_array{hyst}{crate}{temp_index}(SOC_index)/5)*5
                if app.TDF_Optimization_done(hyst,crate,temp_index+1,tu_index)
                    interpolated_value_upper_temp=app.tdf_optimal_parameters{hyst}{crate}{temp_index+1}{tu_index};
                end
            elseif round(sorted_SOC_array{hyst}{crate}{temp_index+1}(tu_index)/5)*5<round(sorted_SOC_array{hyst}{crate}{temp_index}(SOC_index)/5)*5
                if tu_index+1<=length(sorted_SOC_array{hyst}{crate}{temp_index+1})
                    temp_SOC_count=tu_index+1;
                    if app.TDF_Optimization_done(hyst,crate,temp_index+1,tu_index) && app.TDF_Optimization_done(hyst,crate,temp_index+1,temp_SOC_count)
                        %Interpolation zwischen den SOC Punkten tl_index+1>tl_index
                        interpolated_value_upper_temp=zeros(1,length(TDF_index));
                        for n=1:length(TDF_index)
                            interpolated_value_upper_temp(TDF_index(n))=(app.tdf_optimal_parameters{hyst}{crate}{temp_index+1}{temp_SOC_count}(TDF_index(n))-app.tdf_optimal_parameters{hyst}{crate}{temp_index+1}{tu_index}(TDF_index(n)))*(((sorted_SOC_array{hyst}{crate}{temp_index}(SOC_index)-sorted_SOC_array{hyst}{crate}{temp_index+1}(tu_index))/(sorted_SOC_array{hyst}{crate}{temp_index+1}(temp_SOC_count)-sorted_SOC_array{hyst}{crate}{temp_index+1}(tu_index))))+app.tdf_optimal_parameters{hyst}{crate}{temp_index+1}{tu_index}(TDF_index(n));
                        end
                    end

                end
            elseif round(sorted_SOC_array{hyst}{crate}{temp_index+1}(tu_index)/5)*5>round(sorted_SOC_array{hyst}{crate}{temp_index}(SOC_index)/5)*5
                if tu_index-1>=1
                    temp_SOC_count=tu_index-1;
                    if app.TDF_Optimization_done(hyst,crate,temp_index+1,tu_index) && app.TDF_Optimization_done(hyst,crate,temp_index+1,temp_SOC_count)
                        %Interpolation zwischen den SOC Punkten %tl_index>tl_index-1
                        interpolated_value_upper_temp=zeros(1,length(TDF_index));
                        for n=1:length(TDF_index)
                            interpolated_value_upper_temp(TDF_index(n))=(app.tdf_optimal_parameters{hyst}{crate}{temp_index+1}{tu_index}(TDF_index(n))-app.tdf_optimal_parameters{hyst}{crate}{temp_index+1}{temp_SOC_count}(TDF_index(n)))*(((sorted_SOC_array{hyst}{crate}{temp_index}(SOC_index)-sorted_SOC_array{hyst}{crate}{temp_index+1}(temp_SOC_count))/(sorted_SOC_array{hyst}{crate}{temp_index+1}(tu_index)-sorted_SOC_array{hyst}{crate}{temp_index+1}(temp_SOC_count))))+app.tdf_optimal_parameters{hyst}{crate}{temp_index+1}{temp_SOC_count}(TDF_index(n));
                        end
                    end

                end
            end

            %Extrapolation
            if ~isempty(interpolated_value_upper_temp)
                for n=1:length(TDF_index)
                    potential_new_boundaries{extrapolation_count}{TDF_index(n),1}=interpolated_value_upper_temp(TDF_index(n))*(1-2*(app.ContinousStep.Value/100)*abs((sorted_temperature_array{hyst}{crate}(temp_index)-sorted_temperature_array{hyst}{crate}(temp_index+1))/10));
                    potential_new_boundaries{extrapolation_count}{TDF_index(n),2}=interpolated_value_upper_temp(TDF_index(n))*(1+2*(app.ContinousStep.Value/100)*abs((sorted_temperature_array{hyst}{crate}(temp_index)-sorted_temperature_array{hyst}{crate}(temp_index+1))/10));
                end
                extrapolation_count=extrapolation_count+1;
            end


        end
    end


    if ~isempty(potential_new_boundaries)
        extrapolated_new_boundaries=cell2mat(potential_new_boundaries{1});
        for m=2:length(potential_new_boundaries)
            for k=1:length(TDF_index)
                if min(extrapolated_new_boundaries(k,1),potential_new_boundaries{m}{k,1})<max(extrapolated_new_boundaries(k,2),potential_new_boundaries{m}{k,2})
                    extrapolated_new_boundaries(k,1)=min(extrapolated_new_boundaries(k,1),potential_new_boundaries{m}{k,1});
                    extrapolated_new_boundaries(k,2)=max(extrapolated_new_boundaries(k,2),potential_new_boundaries{m}{k,2});
                end
            end
        end

        for n=1:length(TDF_index)
            if max(tdf_min(n),extrapolated_new_boundaries(TDF_index(n),1))<min(tdf_max(n),extrapolated_new_boundaries(TDF_index(n),2))
                if max(tdf_min(n),extrapolated_new_boundaries(TDF_index(n),1))<min(tdf_max(n),extrapolated_new_boundaries(TDF_index(n),2))
                    tdf_min(n)=max(tdf_min(n),extrapolated_new_boundaries(TDF_index(n),1));
                    tdf_max(n)=min(tdf_max(n),extrapolated_new_boundaries(TDF_index(n),2));
                    app.TDF_Boundaries{hyst}{crate}{temp_index}{SOC_index}{n,1} = tdf_min(n);
                    app.TDF_Boundaries{hyst}{crate}{temp_index}{SOC_index}{n,2} = tdf_max(n);
                end
            end
        end
        
        for n=1:length(TDF_index)
            DataMatrix_RelaxData(n+4,4) = {tdf_min(n)};
            DataMatrix_RelaxData(n+4,5) = {tdf_max(n)};
            if DataMatrix_RelaxData{n+4,2} > DataMatrix_RelaxData{n+4,5}|| DataMatrix_RelaxData{n+4,2} < DataMatrix_RelaxData{n+4,4}
                DataMatrix_RelaxData{n+4,2} = DataMatrix_RelaxData{n+4,5};
            end
        end
        if app.GUI
            set(app.TDF_RelaxTable,'Data',DataMatrix_RelaxData) 
        else
            app.TDF_RelaxTable.Data = DataMatrix_RelaxData;
        end
    end
end

