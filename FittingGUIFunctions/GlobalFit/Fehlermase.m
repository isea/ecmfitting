%% Skript zum Vergleich von Messungen und Simulationen anhand von verschiedenen Fehlermaßen
%Daten laden
[file,~]=uigetfile;
digaSim=load(file);

[file,~]=uigetfile;
digaMes=load(file);

Programmdauer=0:0.01:2000;
[digaMes.diga.real.Programmdauer, index]=unique(digaMes.diga.real.Programmdauer);

MessSt=interp1(digaMes.diga.real.Programmdauer, digaMes.diga.real.Strom(index), Programmdauer);
MessSp=interp1(digaMes.diga.real.Programmdauer, digaMes.diga.real.Spannung(index), Programmdauer);
SimSp=interp1(digaSim.diga.daten.Programmdauer, digaSim.diga.daten.SpannungVec, Programmdauer);
SimSt=interp1(digaSim.diga.daten.Programmdauer, digaSim.diga.daten.StromVec, Programmdauer);
SimSOC = interp1(digaSim.diga.daten.Programmdauer, digaSim.diga.daten.SOCVec, Programmdauer);

Spannungsverlaeufe = 0;
Energiebilanz = 1;
relativer_Fehler = 0;
absoluter_Fehler = 0;
SOC_bereinigt = 0;

if SOC_bereinigt
    [file,~] = uigetfile;
    qOCV = load(file);
    SOC_Schritte_min = min(qOCV.OCV_File{1,1}.SOC(1),qOCV.OCV_File{2,1}.SOC(1));
    SOC_Schritte = SOC_Schritte_min:0.01:100;
    [qOCV_SOC_CHA,index_CHA] = unique(qOCV.OCV_File{1,1}.SOC);
    [qOCV_SOC_DCH,index_DCH] = unique(qOCV.OCV_File{2,1}.SOC);
    qOCV_SOC_CHA = qOCV_SOC_CHA * 100;
    qOCV_SOC_DCH = qOCV_SOC_DCH * 100;
    qOCV_Spannung_CHA = interp1(qOCV_SOC_CHA,qOCV.OCV_File{1,1}.voltage(index_CHA),SOC_Schritte);
    qOCV_Spannung_DCH = interp1(qOCV_SOC_DCH,qOCV.OCV_File{2,1}.voltage(index_DCH),SOC_Schritte);
    qOCV_Spannung_Mittel = (qOCV_Spannung_CHA + qOCV_Spannung_DCH)./2;
%     qOCV_Spannung_CHA = ;
%     qOCV_Spannung_DCH = ;    
end

if Spannungsverlaeufe 
    figure(1)
    plot(Programmdauer, MessSp,'b')
    hold on
    plot(Programmdauer, SimSp , 'r')
    hold off
    grid on
    xlabel('Zeit')
    ylabel('Spannung')
    legend('Messung','Simulation')
end

%Energiebilanz
if Energiebilanz
    for n=1:length(Programmdauer)
        if(MessSt(n)<0 && MessSp(n)<0)
            p_mes(n)=-(MessSt(n)*MessSp(n));
        elseif (MessSt(n)>0 && MessSp(n)>0)
            p_mes(n)=MessSt(n)*MessSp(n);
        else
            p_mes(n)=MessSt(n)*MessSp(n);
        end
    end
    for n=1:length(Programmdauer)

        if(SimSt(n)<0 && SimSp(n)<0)
            p_sim(n)=-(SimSt(n)*SimSp(n));
        elseif (SimSt(n)>0 && SimSp(n)>0)
            p_sim(n)=SimSt(n)*SimSp(n);
        else
            p_sim(n)=SimSt(n)*SimSp(n);
        end
    end


    for n=1:length(Programmdauer)
        p_diff(n)=p_sim(n)-p_mes(n);
    end

    delta_energie(1)=p_diff(1)*0.001;
    for n=2:length(p_diff)
        delta_energie(n)=delta_energie(n-1)+p_diff(n)*0.01;
    end

    figure(4)
    plot(Programmdauer,delta_energie)
    xlabel('Zeit in Sekunden')
    ylabel('Energiedifferenz zw. Messung und Simulatuion in Wattsekunden')
    title('Energiedifferenz')
end

%relative Spannungsdifferenz
if relativer_Fehler 
    if ~SOC_bereinigt
        DiffSp = SimSp - MessSp;
        strom_idx = 1:length(MessSp);
        isnan_idx = find(isnan(MessSp)==0);
        MessSp_korrigiert = MessSp - median(MessSp);
         Maximaler_Peak = max(abs(MessSp_korrigiert));
        for n = 1:length(MessSp)
            if MessSp_korrigiert > Maximaler_Peak * 0.1
                rel_error(n) = DiffSp(n)./MessSp_korrigiert(n);
            else 
                rel_error(n) = DiffSp(n)./(Maximaler_Peak * 0.3);
            end
        end
        figure(2)
        plot(Programmdauer(strom_idx),rel_error)
        xlabel('Zeit in Sekunden')
        ylabel('relative Differenz')
        title('relativer Fehler und maximaler relativer Fehler')
        hold on
        grid on
        [max_rel_error,idx] = max(abs(rel_error));
        if rel_error(idx) > 0
            plot(Programmdauer(strom_idx(idx)),max_rel_error,'rX')
            plot([0 Programmdauer(end)],[max_rel_error max_rel_error],'r')
        else
            plot(Programmdauer(strom_idx(idx)),-max_rel_error,'rX')
            plot([0 Programmdauer(end)],[-max_rel_error -max_rel_error],'r')
        end
    %Bereinigung um qOCV mit exaktem SOC-Wert aus Simulation
    else
        DiffSp = SimSp - MessSp;
        strom_idx = 1:length(MessSp);
        for n = 1:length(MessSp)
            [~,ocv_idx] = min(abs(SOC_Schritte - SimSOC(n)));
            Ruhespannung(n) = qOCV_Spannung_Mittel(ocv_idx);
        end        
        MessSp_korrigiert = MessSp - Ruhespannung;
        Maximaler_Peak = max(abs(MessSp_korrigiert));
        for n = 1:length(MessSp)
            if MessSp_korrigiert > Maximaler_Peak * 0.5
                rel_error(n) = DiffSp(n)./MessSp_korrigiert(n);
            else 
                rel_error(n) = DiffSp(n)./(Maximaler_Peak * 0.5);
            end
        end
%         rel_error = DiffSp(strom_idx)./MessSp_korrigiert(strom_idx);
        figure(2)
        plot(Programmdauer(strom_idx),rel_error)
        xlabel('Zeit in Sekunden')
        ylabel('relative Differenz')
        title('relativer Fehler und maximaler relativer Fehler')
        hold on
        grid on
        [max_rel_error,idx] = max(abs(rel_error));
        if rel_error(idx) > 0
            plot(Programmdauer(strom_idx(idx)),max_rel_error,'rX')
            plot([0 Programmdauer(end)],[max_rel_error max_rel_error],'r')
        else
            plot(Programmdauer(strom_idx(idx)),-max_rel_error,'rX')
            plot([0 Programmdauer(end)],[-max_rel_error -max_rel_error],'r')
        end
    end
end

%absolute Spannungsdifferenz
if relativer_Fehler     
    DiffSp = SimSp - MessSp;
    figure(3)
    plot(Programmdauer,DiffSp)
    xlabel('Zeit in Sekunden')
    ylabel('absoluter Differenz')
    title('absoluter Fehler')
    hold on
    grid on
end