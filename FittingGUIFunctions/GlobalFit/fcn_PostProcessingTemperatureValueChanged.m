function app = fcn_PostProcessingTemperatureValueChanged(app,event)           
%Load Data into Table
global DRT_GUI
if app.PostProcessingTemperature.Value
    TableData = app.PostProcessingTemperatureTable.Data;
    TableData = cell(length(app.ParamTable.Data),2);
    TableData(:,1) = app.ParamTable.Data(:,1);
    for n = 1:length(TableData(:,1))
        TableData(n,2) = {[false]};
    end
    if app.GUI
        set(app.PostProcessingTemperatureTable,'Data',TableData)
        set(app.PostProcessingTemperatureTable,'ColumnEditable',true)
    else
        app.PostProcessingTemperatureTable.Data = TableData;
        app.PostProcessingTemperatureTable.ColumnEditable = true;
    end
end

end

