function app = fcn_GlobalSingleFit(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array)
%     app.PunkteWegnehmenTextBox.Value = '';
%     fcn_PunkteWegnehmeButton(app, event)

%load data
global DRT_GUI;
% app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
% fcn_ModellAuswahlPopup(app);
% fcn_BatterieNamePopup(app);
if isempty(DRT_GUI) || isempty(cell2mat(strfind(fieldnames(DRT_GUI),'Messdaten')))
    return
end
formula = DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell;
if  isempty(cell2mat(strfind(fieldnames(DRT_GUI.Messdaten),'relax_fft'))) || isempty(DRT_GUI.Messdaten.relax_fft)
    m_w = DRT_GUI.Messdaten.omega(DRT_GUI.Messdaten.aktiv==1);
    m_real = DRT_GUI.Messdaten.Zreal(DRT_GUI.Messdaten.aktiv==1);
    m_imag = DRT_GUI.Messdaten.Zimg(DRT_GUI.Messdaten.aktiv==1);
else
    m_w = [DRT_GUI.Messdaten.omega(DRT_GUI.Messdaten.aktiv==1) ; DRT_GUI.Messdaten.relax_fft.omega(DRT_GUI.Messdaten.relax_fft.aktiv==1)] ; %selbst
    m_real = [DRT_GUI.Messdaten.Zreal(DRT_GUI.Messdaten.aktiv==1); DRT_GUI.Messdaten.relax_fft.Zreal(DRT_GUI.Messdaten.relax_fft.aktiv==1)+DRT_GUI.Messdaten.relax_fft.Zreal_korrektur];%selbst
    m_imag = [DRT_GUI.Messdaten.Zimg(DRT_GUI.Messdaten.aktiv==1); DRT_GUI.Messdaten.relax_fft.Zimg(DRT_GUI.Messdaten.relax_fft.aktiv==1)];%selbst
end
if app.GUI
    TableCell = get(app.ParamTable,'Data');
else
    TableCell = app.ParamTable.Data;
end
if ~isempty(TableCell)
    TableCell = cell2struct(TableCell',{'Name','Fix','Value','Min','Max','Lim'});
end

% die initialisierte Werte aus Feld bekommen
DRT_GUI.Fit.ParFix = zeros(size(DRT_GUI.Fit.Parameter));
for Par_init_i = 1:length(DRT_GUI.Fit.Parameter)
    if ~isempty(TableCell(Par_init_i).Value)
        DRT_GUI.Fit.Parameter(Par_init_i) = TableCell(Par_init_i).Value;
    else
        DRT_GUI.Fit.Parameter(Par_init_i) = 0;
        TableCell(Par_init_i).Value = 0;
    end
    if ~isempty(TableCell(Par_init_i).Min) && ~TableCell(Par_init_i).Fix
        p_min(Par_init_i)= TableCell(Par_init_i).Min;
    elseif TableCell(Par_init_i).Fix
        DRT_GUI.Fit.ParFix(Par_init_i) = 1 ;
        p_min(Par_init_i) = TableCell(Par_init_i).Value;
    else
        p_min(Par_init_i) = 0;
        TableCell(Par_init_i).Min = 0 ;
    end
    if ~isempty(TableCell(Par_init_i).Min)
        DRT_GUI.Fit.Parameter_min(Par_init_i) = TableCell(Par_init_i).Min;
    else
        DRT_GUI.Fit.Parameter_min(Par_init_i) = 0;
    end
    if ~isempty(TableCell(Par_init_i).Max) && ~TableCell(Par_init_i).Fix
        p_max(Par_init_i)= TableCell(Par_init_i).Max;
    elseif TableCell(Par_init_i).Fix
        p_max(Par_init_i) = TableCell(Par_init_i).Value;
    else
        p_max(Par_init_i) = inf;
        TableCell(Par_init_i).Max = Inf;
    end
    if ~isempty(TableCell(Par_init_i).Max)
        DRT_GUI.Fit.Parameter_max(Par_init_i) = TableCell(Par_init_i).Max;
    else
        DRT_GUI.Fit.Parameter_max(Par_init_i) = inf;
    end
end
p_init = DRT_GUI.Fit.Parameter;
% options = optimset('MaxIter',2000000,'MaxFunEvals',2000000,'TolX',1e-10,'TolFun',1e-10,'display','final');
options = optimset('MaxIter',2000000,'MaxFunEvals',2000000,'TolX',1e-10,'TolFun',1e-10);
%get initail values from Table
for n_input_change=1:length(p_init)
    if p_init(n_input_change)> TableCell(n_input_change).Max || p_init(n_input_change)< TableCell(n_input_change).Min
        p_init(n_input_change)=TableCell(n_input_change).Value ;
    end
end

%use specified Fitting algorithm for chosen battery model or deviate to
%default option if no model specific algorithm is defined
switch app.GlobalUsedModell
    case 'RRC'
        %Betrachte nur kapazitives Verhalten
        index=find(-m_imag>0);
        m_w=m_w(index);
        m_real=m_real(index);
        m_imag=m_imag(index);
        
        %Optimation within the default boundaries
        p_init = DRT_GUI.Fit.Parameter;
        temporary_variable=cell2mat(app.Boundaries_Default{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max_or=transpose(temporary_variable(:,2));
        p_min_or=transpose(temporary_variable(:,1));
        if length(p_init)~=length(p_min_or)
            if length(p_init)>length(p_min_or)
                p_init=p_init(1:length(p_min_or));
            elseif length(p_init)<length(p_min_or)
                p_init=[p_init zeros(1,length(p_min_or)-length(p_init))];
            end
        end
        for n = 1:length(p_min_or)
            if p_min_or(n)<0
                p_min_or(n) = 0;
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init)
                if p_max_or(n) - p_min_or(n) < 0.002
                    if p_init(n) > 0.001
                        p_max_or(n) = p_init(n) + 0.001;
                        p_min_or(n) = p_init(n) - 0.001;
                    else
                        p_max_or(n) = 0.002;
                        p_min_or(n) = 0;
                    end
                end
            end
        end
        [p_best_or,fval_or,exitflag_or,output_or]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min_or, p_max_or ,options, formula,app.used_optimization_algorithm);
        app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1)=fval_or;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1,:)=p_best_or;
        
        %Optimization within the calculated boundaries
        temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max=transpose(temporary_variable(:,2));
        p_min=transpose(temporary_variable(:,1));
        if length(p_init)~=length(p_min)
            if length(p_init)>length(p_min)
                p_init=p_init(1:length(p_min));
            elseif length(p_init)<length(p_min)
                p_init=[p_init zeros(1,length(p_min)-length(p_init))];
            end
        end
        for n = 1:length(p_min)
            if p_min(n)<0
                p_min(n) = 0;
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init)
                if p_max(n) - p_min(n) < 0.002
                    if p_init(n) > 0.001
                        p_max(n) = p_init(n) + 0.001;
                        p_min(n) = p_init(n) - 0.001;
                    else
                        p_max(n) = 0.002;
                        p_min(n) = 0;
                    end
                end
            end
        end
        [p_best,fval,exitflag_or,output_or]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min, p_max ,options, formula,app.used_optimization_algorithm);
        app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2)=fval;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2,:)=p_best;
    case 'R2RC'
        if app.current_crate_index == app.static_EIS_index
            %Betrachte nur kapazitives Verhalten
            index=find(-m_imag>0);
            m_w=m_w(index);
            m_real=m_real(index);
            m_imag=m_imag(index);
            
            %Seperate this part into hf,mf and lf
            [pks_u,locs_u] = findpeaks(m_imag);
            if length(locs_u)> 1
                [minimum_value, step_index_index] = max(pks_u);
                step_index = locs_u(step_index_index) + 2;
                %                 step_index = locs_u(end) + 7;
            elseif length(locs_u) < 1
                step_index = findchangepts(diff(m_real));
            else
                step_index = locs_u(end) + 2;
            end
            
            m_real_lf = m_real(step_index-1:end);
            m_imag_lf = m_imag(step_index-1:end);
            m_w_lf = m_w(step_index-1:end);
            
            m_w_mf = m_w(1:step_index-2);
            m_real_mf = m_real(1:step_index-2);
            m_imag_mf = m_imag(1:step_index-2);
            
            formula_mf = '1.*(p(1))+1.*(p(2)./(1+1i.*w.*p(3)))';
            
            %%%%%%%%%%%%%%%%%%%%% MF
            p_init_mf = DRT_GUI.Fit.Parameter(1:3);
            temporary_variable=cell2mat(app.Boundaries_Default{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
            p_max_or_mf=transpose(temporary_variable(1:3,2));
            p_min_or_mf=transpose(temporary_variable(1:3,1));
            if length(p_init_mf )~=length(p_min_or_mf )
                if length(p_init_mf)>length(p_min_or_mf)
                    p_init_mf=p_init_mf(1:length(p_min_or_mf));
                elseif length(p_init_mf)<length(p_min_or_mf)
                    p_init_mf=[p_init_mf zeros(1,length(p_min_or_mf)-length(p_init_mf))];
                end
            end
            for n = 1:length(p_min_or_mf)
                if p_min_or_mf(n)<0
                    p_min_or_mf(n) = 0;
                end
            end
            if strcmp(app.used_optimization_algorithm,'SimAnn')
                for n = 1:length(p_init_mf)
                    if p_max_or_mf(n) - p_min_or_mf(n) < 0.002
                        if p_init_mf(n) > 0.001
                            p_max_or_mf(n) = p_init_mf(n) + 0.001;
                            p_min_or_mf(n) = p_init_mf(n) - 0.001;
                        else
                            p_max_or_mf(n) = 0.002;
                            p_min_or_mf(n) = 0;
                        end
                    end
                end
            end
            [p_best_or_mf,fval_or_mf,exitflag_or,output_or]=function_fit_easyfit2(app,m_w_mf,[m_real_mf, m_imag_mf],p_init_mf,@function_model_all_types2, p_min_or_mf, p_max_or_mf ,options, formula_mf ,app.used_optimization_algorithm);
            app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1)=fval_or_mf;
            app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1,1:3)=p_best_or_mf;
            
            %Optimization within the calculated boundaries
            temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
            p_max_mf=transpose(temporary_variable(1:3,2));
            p_min_mf=transpose(temporary_variable(1:3,1));
            if length(p_init_mf)~=length(p_min_mf)
                if length(p_init_mf)>length(p_min_mf)
                    p_init_mf=p_init_mf(1:length(p_min_mf));
                elseif length(p_init_mf)<length(p_min_mf)
                    p_init_mf=[p_init_mf zeros(1,length(p_min_mf)-length(p_init_mf))];
                end
            end
            for n = 1:length(p_min_mf)
                if p_min_mf(n)<0
                    p_min_mf(n) = 0;
                end
            end
            if strcmp(app.used_optimization_algorithm,'SimAnn')
                for n = 1:length(p_init_mf)
                    if p_max_mf(n) - p_min_mf(n) < 0.002
                        if p_init_mf(n) > 0.001
                            p_max_mf(n) = p_init_mf(n) + 0.001;
                            p_min_mf(n) = p_init_mf(n) - 0.001;
                        else
                            p_max_mf(n) = 0.002;
                            p_min_mf(n) = 0;
                        end
                    end
                end
            end
            [p_best_mf,fval_mf,exitflag_or,output_or]=function_fit_easyfit2(app,m_w_mf,[m_real_mf, m_imag_mf],p_init_mf,@function_model_all_types2, p_min_mf, p_max_mf ,options, formula_mf,app.used_optimization_algorithm);
            app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2)=fval_mf;
            app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2,1:3)=p_best_mf;
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Complete
            %Optimation within the default boundaries
            temporary_variable=cell2mat(app.Boundaries_Default{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
            p_max_or=transpose(temporary_variable(:,2));
            p_min_or=transpose(temporary_variable(:,1));
            p_init_or = [ p_best_or_mf, p_init(4:end)];
            p_min_or=[ 0.98*p_best_or_mf(1:3) ,p_min_or(4:end)];
            p_max_or=[ 1.02*p_best_or_mf(1:3) ,p_max_or(4:end)];
            for n = 1:length(p_min_or)
                if p_min_or(n)<0
                    p_min_or(n) = 0;
                end
            end
            if strcmp(app.used_optimization_algorithm,'SimAnn')
                for n = 1:length(p_init_or)
                    if p_max_or(n) - p_min_or(n) < 0.002
                        if p_init_or(n) > 0.001
                            p_max_or(n) = p_init_or(n) + 0.001;
                            p_min_or(n) = p_init_or(n) - 0.001;
                        else
                            p_max_or(n) = 0.002;
                            p_min_or(n) = 0;
                        end
                    end
                end
            end
            [p_best_or,fval_or,exitflag_or,output_or]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init_or,@function_model_all_types2, p_min_or, p_max_or ,options, formula,app.used_optimization_algorithm);
            app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1)=fval_or;
            app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1,:)=p_best_or;
            %     index_min = find((p_best_or<=p_min_default*1.001 & p_min_default~=p_max_default & p_min_default~=p_min_model & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Rser') & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'C')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Cser')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Kskin')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Lser')));
            %     index_max = find((p_best_or>=p_max_default*0.999 & p_min_default~=p_max_default & p_max_default~=p_max_model & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Rser') & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'C')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Cser')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Kskin')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Lser')));
            
            %Optimization within the calculated boundaries
            temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
            p_max=transpose(temporary_variable(:,2));
            p_min=transpose(temporary_variable(:,1));
            p_init = [ p_best_mf, p_init(4:end)];
            p_min=[ 0.98*p_best_mf ,p_min(4:end)];
            p_max=[ 1.02*p_best_mf ,p_max(4:end)];
            for n = 1:length(p_min)
                if p_min(n)<0
                    p_min(n) = 0;
                end
            end
            if strcmp(app.used_optimization_algorithm,'SimAnn')
                for n = 1:length(p_init)
                    if p_max(n) - p_min(n) < 0.002
                        if p_init(n) > 0.001
                            p_max(n) = p_init(n) + 0.001;
                            p_min(n) = p_init(n) - 0.001;
                        else
                            p_max(n) = 0.002;
                            p_min(n) = 0;
                        end
                    end
                end
            end
            [p_best,fval,exitflag_or,output_or]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min, p_max ,options, formula,app.used_optimization_algorithm);
            app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2)=fval;
            app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2,:)=p_best;
            %     index_min = find((p_best<=p_min_default*1.001 & p_min_default~=p_max_default & p_min_default~=p_min_model & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Rser') & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'C')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Cser')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Kskin')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Lser')));
            %     index_max = find((p_best>=p_max_default*0.999 & p_min_default~=p_max_default & p_max_default~=p_max_model & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Rser') & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'C')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Cser')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Kskin')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Lser')));
        else
            formula = '1.*(p(1))+1.*(p(2)./(1+1i.*w.*p(3)))';
            
            index=find(-m_imag>0);
            m_w=m_w(index);
            m_real=m_real(index);
            m_imag=m_imag(index);
            
            
            %Optimation within the default boundaries
            p_init = DRT_GUI.Fit.Parameter;
            p_init_mf = p_init(1:3);
            
            %Optimation within the default boundaries
            p_init = DRT_GUI.Fit.Parameter(1:3);
            temporary_variable=cell2mat(app.Boundaries_Default{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
            p_max_or=transpose(temporary_variable(1:3,2));
            p_min_or=transpose(temporary_variable(1:3,1));
            if length(p_init)~=length(p_min_or)
                if length(p_init)>length(p_min_or)
                    p_init=p_init(1:length(p_min_or));
                elseif length(p_init)<length(p_min_or)
                    p_init=[p_init zeros(1,length(p_min_or)-length(p_init))];
                end
            end
            for n = 1:length(p_min_or)
                if p_min_or(n)<0
                    p_min_or(n) = 0;
                end
            end
            if strcmp(app.used_optimization_algorithm,'SimAnn')
                for n = 1:length(p_init)
                    if p_max_or(n) - p_min_or(n) < 0.002
                        if p_init(n) > 0.001
                            p_max_or(n) = p_init(n) + 0.001;
                            p_min_or(n) = p_init(n) - 0.001;
                        else
                            p_max_or(n) = 0.002;
                            p_min_or(n) = 0;
                        end
                    end
                end
            end
            [p_best_or,fval_or,exitflag_or,output_or]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min_or, p_max_or ,options, formula,app.used_optimization_algorithm);
            app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1)=fval_or;
            app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1,1:3)=p_best_or;
            
            %Optimization within the calculated boundaries
            temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
            p_max=transpose(temporary_variable(1:3,2));
            p_min=transpose(temporary_variable(1:3,1));
            if length(p_init)~=length(p_min)
                if length(p_init)>length(p_min)
                    p_init=p_init(1:length(p_min));
                elseif length(p_init)<length(p_min)
                    p_init=[p_init zeros(1,length(p_min)-length(p_init))];
                end
            end
            for n = 1:length(p_min)
                if p_min(n)<0
                    p_min(n) = 0;
                end
            end
            if strcmp(app.used_optimization_algorithm,'SimAnn')
                for n = 1:length(p_init)
                    if p_max(n) - p_min(n) < 0.002
                        if p_init(n) > 0.001
                            p_max(n) = p_init(n) + 0.001;
                            p_min(n) = p_init(n) - 0.001;
                        else
                            p_max(n) = 0.002;
                            p_min(n) = 0;
                        end
                    end
                end
            end
            [p_best,fval,exitflag_or,output_or]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min, p_max ,options, formula,app.used_optimization_algorithm);
            app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2)=fval;
            app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2,1:3)=p_best;
            
            [~,soc_index] = min(abs(sorted_SOC_array{app.current_hyst_index}{app.static_EIS_index}{app.current_temp_index}-sorted_SOC_array{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}(app.current_SOC_index)));
            app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1,4:5) = app.optimal_parameters{app.current_hyst_index}{app.static_EIS_index}{app.current_temp_index}{soc_index}(4:5);
            app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2,4:5) = app.optimal_parameters{app.current_hyst_index}{app.static_EIS_index}{app.current_temp_index}{soc_index}(4:5);
            
            p_best = app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2,:);
            p_best_or = app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1,:);
        end
    case 'R3RC'
        
        %Betrachte nur kapazitives Verhalten
        index=find(-m_imag>0);
        m_w=m_w(index);
        m_real=m_real(index);
        m_imag=m_imag(index);
        
        
        %Seperate this part into hf,mf and lf
        [pks_u,locs_u] = findpeaks(m_imag);
        if length(locs_u)> 1
            [minimum_value, step_index_index] = max(pks_u);
            step_index = locs_u(step_index_index) + 2;
            %                 step_index = locs_u(end) + 7;
        elseif length(locs_u) < 1
            step_index = findchangepts(diff(m_real));
        else
            step_index = locs_u(end) + 2;
        end
        
        m_real_lf = m_real(step_index-1:end);
        m_imag_lf = m_imag(step_index-1:end);
        m_w_lf = m_w(step_index-1:end);
        
        m_w_mf = m_w(1:step_index-2);
        m_real_mf = m_real(1:step_index-2);
        m_imag_mf = m_imag(1:step_index-2);
        
        formula_mf = '1.*(p(1))+1.*(p(2)./(1+1i.*w.*p(3)))+1.*(p(4)./(1+1i.*w.*p(5)))';
        
        %Optimation within the default boundaries
        p_init = DRT_GUI.Fit.Parameter;
        p_init_mf = p_init(1:5);
        
        
        temporary_variable=cell2mat(app.Boundaries_Default{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max_or=transpose(temporary_variable(:,2));
        p_min_or=transpose(temporary_variable(:,1));
        p_max_or_mf = p_max_or(1:5);
        p_min_or_mf = p_min_or(1:5);
        if length(p_init_mf)~=length(p_min_or_mf)
            if length(p_init_mf)>length(p_min_or_mf)
                p_init_mf=p_init_mf(1:length(p_min_or_mf));
            elseif length(p_init_mf)<length(p_min_or_mf)
                p_init_mf=[p_init_mf zeros(1,length(p_min_or_mf)-length(p_init_mf))];
            end
        end
        for n = 1:length(p_min_or_mf)
            if p_min_or_mf(n)<0
                p_min_or_mf(n) = 0;
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init_mf)
                if p_max_or_mf(n) - p_min_or_mf(n) < 0.002
                    if p_init_mf(n) > 0.001
                        p_max_or_mf(n) = p_init_mf(n) + 0.001;
                        p_min_or_mf(n) = p_init_mf(n) - 0.001;
                    else
                        p_max_or_mf(n) = 0.002;
                        p_min_or_mf(n) = 0;
                    end
                end
            end
        end
        [p_best_or,fval_or,exitflag_or,output_or]=function_fit_easyfit2(app,m_w_mf,[m_real_mf, m_imag_mf],p_init_mf,@function_model_all_types2, p_min_or_mf, p_max_or_mf ,options, formula_mf,app.used_optimization_algorithm);
        
        %     index_min = find((p_best_or<=p_min_default*1.001 & p_min_default~=p_max_default & p_min_default~=p_min_model & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Rser') & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'C')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Cser')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Kskin')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Lser')));
        %     index_max = find((p_best_or>=p_max_default*0.999 & p_min_default~=p_max_default & p_max_default~=p_max_model & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Rser') & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'C')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Cser')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Kskin')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Lser')));
        
        %Optimization within the calculated boundaries
        temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max=transpose(temporary_variable(:,2));
        p_min=transpose(temporary_variable(:,1));
        p_max_mf = p_max(1:5);
        p_min_mf = p_min(1:5);
        if length(p_init_mf)~=length(p_min_mf)
            if length(p_init_mf)>length(p_min_mf)
                p_init_mf=p_init_mf(1:length(p_min_mf));
            elseif length(p_init_mf)<length(p_min_mf)
                p_init_mf=[p_init_mf zeros(1,length(p_min_mf)-length(p_init_mf))];
            end
        end
        for n = 1:length(p_min_mf)
            if p_min_mf(n)<0
                p_min_mf(n) = 0;
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init_mf)
                if p_max_mf(n) - p_min_mf(n) < 0.002
                    if p_init_mf(n) > 0.001
                        p_max_mf(n) = p_init_mf(n) + 0.001;
                        p_min_mf(n) = p_init_mf(n) - 0.001;
                    else
                        p_max_mf(n) = 0.002;
                        p_min_mf(n) = 0;
                    end
                end
            end
        end
        [p_best,fval,exitflag_or,output_or]=function_fit_easyfit2(app,m_w_mf,[m_real_mf, m_imag_mf],p_init_mf,@function_model_all_types2, p_min_mf, p_max_mf ,options, formula_mf,app.used_optimization_algorithm);
        
        
        
        p_init_or = [ p_best_or, p_init(6:end)];
        p_min_or=[ p_best_or(1) 0.9*p_best_or(2:5) ,p_min_or(6:end)];
        p_max_or=[ p_best_or(1) 1.1*p_best_or(2:5) ,p_max_or(6:end)];
        for n = 1:length(p_min_or)
            if p_min_or(n)<0
                p_min_or(n) = 0;
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init)
                if p_max_or(n) - p_min_or(n) < 0.002
                    if p_init(n) > 0.001
                        p_max_or(n) = p_init(n) + 0.001;
                        p_min_or(n) = p_init(n) - 0.001;
                    else
                        p_max_or(n) = 0.002;
                        p_min_or(n) = 0;
                    end
                end
            end
        end
        [p_best_or,fval_or,exitflag_or,output_or]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min_or, p_max_or ,options, formula,app.used_optimization_algorithm);
        app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1)=fval_or;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1,:)=p_best_or;
        
        p_init_ges = [ p_best, p_init(6:end)];
        p_min=[ p_best(1) 0.9*p_best(2:5) ,p_min(6:end)];
        p_max=[ p_best(1) 1.1*p_best(2:5) ,p_max(6:end)];
        for n = 1:length(p_min)
            if p_min(n)<0
                p_min(n) = 0;
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init)
                if p_max(n) - p_min(n) < 0.002
                    if p_init(n) > 0.001
                        p_max(n) = p_init(n) + 0.001;
                        p_min(n) = p_init(n) - 0.001;
                    else
                        p_max(n) = 0.002;
                        p_min(n) = 0;
                    end
                end
            end
        end
        [p_best,fval,exitflag_or,output_or]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min, p_max ,options, formula,app.used_optimization_algorithm);
        app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2)=fval;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2,:)=p_best;
        
    case 'R4RC'
        %Betrachte nur kapazitives Verhalten
        index=find(-m_imag>0);
        m_w=m_w(index);
        m_real=m_real(index);
        m_imag=m_imag(index);
        
        
        %Seperate this part into hf,mf and lf
        [pks_u,locs_u] = findpeaks(m_imag);
        if length(locs_u)> 1
            [minimum_value, step_index_index] = max(pks_u);
            step_index = locs_u(step_index_index) + 2;
            %                 step_index = locs_u(end) + 7;
        elseif length(locs_u) < 1
            step_index = findchangepts(diff(m_real));
        else
            step_index = locs_u(end) + 2;
        end
        
        m_real_lf = m_real(step_index-1:end);
        m_imag_lf = m_imag(step_index-1:end);
        m_w_lf = m_w(step_index-1:end);
        
        m_w_mf = m_w(1:step_index-2);
        m_real_mf = m_real(1:step_index-2);
        m_imag_mf = m_imag(1:step_index-2);
        
        formula_mf = '1.*(p(1))+1.*(p(2)./(1+1i.*w.*p(3)))+1.*(p(4)./(1+1i.*w.*p(5)))';
        
        %Optimation within the default boundaries
        p_init = DRT_GUI.Fit.Parameter;
        p_init_mf = p_init(1:5);
        
        
        temporary_variable=cell2mat(app.Boundaries_Default{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max_or=transpose(temporary_variable(:,2));
        p_min_or=transpose(temporary_variable(:,1));
        p_max_or_mf = p_max_or(1:5);
        p_min_or_mf = p_min_or(1:5);
        if length(p_init_mf)~=length(p_min_or_mf)
            if length(p_init_mf)>length(p_min_or_mf)
                p_init_mf=p_init_mf(1:length(p_min_or_mf));
            elseif length(p_init_mf)<length(p_min_or_mf)
                p_init_mf=[p_init_mf zeros(1,length(p_min_or_mf)-length(p_init_mf))];
            end
        end
        for n = 1:length(p_min_or_mf)
            if p_min_or_mf(n)<0
                p_min_or_mf(n) = 0;
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init_mf)
                if p_max_or_mf(n) - p_min_or_mf(n) < 0.002
                    if p_init_mf(n) > 0.001
                        p_max_or_mf(n) = p_init_mf(n) + 0.001;
                        p_min_or_mf(n) = p_init_mf(n) - 0.001;
                    else
                        p_max_or_mf(n) = 0.002;
                        p_min_or_mf(n) = 0;
                    end
                end
            end
        end
        [p_best_or,fval_or,exitflag_or,output_or]=function_fit_easyfit2(app,m_w_mf,[m_real_mf, m_imag_mf],p_init_mf,@function_model_all_types2, p_min_or_mf, p_max_or_mf ,options, formula_mf,app.used_optimization_algorithm);
        
        %     index_min = find((p_best_or<=p_min_default*1.001 & p_min_default~=p_max_default & p_min_default~=p_min_model & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Rser') & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'C')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Cser')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Kskin')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Lser')));
        %     index_max = find((p_best_or>=p_max_default*0.999 & p_min_default~=p_max_default & p_max_default~=p_max_model & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Rser') & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'C')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Cser')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Kskin')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Lser')));
        
        %Optimization within the calculated boundaries
        temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max=transpose(temporary_variable(:,2));
        p_min=transpose(temporary_variable(:,1));
        p_max_mf = p_max(1:5);
        p_min_mf = p_min(1:5);
        if length(p_init_mf)~=length(p_min_mf)
            if length(p_init_mf)>length(p_min_mf)
                p_init_mf=p_init_mf(1:length(p_min_mf));
            elseif length(p_init_mf)<length(p_min_mf)
                p_init_mf=[p_init_mf zeros(1,length(p_min_mf)-length(p_init_mf))];
            end
        end
        for n = 1:length(p_min_mf)
            if p_min_mf(n)<0
                p_min_mf(n) = 0;
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init_mf)
                if p_max_mf(n) - p_min_mf(n) < 0.002
                    if p_init_mf(n) > 0.001
                        p_max_mf(n) = p_init_mf(n) + 0.001;
                        p_min_mf(n) = p_init_mf(n) - 0.001;
                    else
                        p_max_mf(n) = 0.002;
                        p_min_mf(n) = 0;
                    end
                end
            end
        end
        [p_best,fval,exitflag_or,output_or]=function_fit_easyfit2(app,m_w_mf,[m_real_mf, m_imag_mf],p_init_mf,@function_model_all_types2, p_min_mf, p_max_mf ,options, formula_mf,app.used_optimization_algorithm);
        
        
        p_init_or = [ p_best_or, p_init(6:end)];
        p_min_or=[ p_best_or(1) 0.9*p_best_or(2:5) ,p_min_or(6:end)];
        p_max_or=[ p_best_or(1) 1.1*p_best_or(2:5) ,p_max_or(6:end)];
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init)
                if p_max_or(n) - p_min_or(n) < 0.002
                    if p_init(n) > 0.001
                        p_max_or(n) = p_init(n) + 0.001;
                        p_min_or(n) = p_init(n) - 0.001;
                    else
                        p_max_or(n) = 0.002;
                        p_min_or(n) = 0;
                    end
                end
            end
        end
        [p_best_or,fval_or,exitflag_or,output_or]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min_or, p_max_or ,options, formula,app.used_optimization_algorithm);
        app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1)=fval_or;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1,:)=p_best_or;
        
        p_init_ges = [ p_best, p_init(6:end)];
        p_min=[ p_best(1) 0.9*p_best(2:5) ,p_min(6:end)];
        p_max=[ p_best(1) 1.1*p_best(2:5) ,p_max(6:end)];
        for n = 1:length(p_min)
            if p_min(n)<0
                p_min(n) = 0;
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init)
                if p_max(n) - p_min(n) < 0.002
                    if p_init(n) > 0.001
                        p_max(n) = p_init(n) + 0.001;
                        p_min(n) = p_init(n) - 0.001;
                    else
                        p_max(n) = 0.002;
                        p_min(n) = 0;
                    end
                end
            end
        end
        [p_best,fval,exitflag_or,output_or]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min, p_max ,options, formula,app.used_optimization_algorithm);
        app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2)=fval;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2,:)=p_best;
    case'LiIon4_sbi'
        index_cabling = find(-m_imag<0);
        index_esb = find(-m_imag>0);
        if isempty(index_cabling)
            index_cabling = find(DRT_GUI.Messdaten.frequenz>=1000);
            index_esb  = find(DRT_GUI.Messdaten.frequenz<1000);
        end
        
        %Seperate the induktiv part resembling the cabling
        m_w_hf = m_w(index_cabling);
        m_real_hf = m_real(index_cabling);
        m_imag_hf = m_imag(index_cabling);
        
        %Seperate the capacitiv part resembling the rest hte actual
        %battery
        m_w_mf = m_w(index_esb);
        m_real_mf = m_real(index_esb);
        m_imag_mf = m_imag(index_esb);
        
        
        m_w_mf = [m_w_hf , m_w_mf];
        m_real_mf = [m_real_hf , m_real_mf];
        m_imag_mf = [m_imag_hf , m_imag_mf];
        
        %Get fittet values from Gui table
        p_init_cabling = DRT_GUI.Fit.Parameter(2:4);
        p_init_esb = DRT_GUI.Fit.Parameter;
        
        %Cabel parameter fit
        formula_cabel='1.*(p(1)./(1+p(1)./((1i.*w).^p(3).*p(2))))' ;
        temporary_variable=cell2mat(app.Boundaries_Default{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max_cabel=transpose(temporary_variable(2:4,2));
        p_min_cabel=transpose(temporary_variable(2:4,1));
        for n = 1:length(p_min_cabel)
            if p_min_cabel(n)<0
                p_min_cabel(n) = 0;
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init_cabling)
                if p_max_cabel(n) - p_min_cabel(n) < 0.002
                    if p_init_cabling(n) > 0.001
                        p_max_cabel(n) = p_init_cabling(n) + 0.001;
                        p_min_cabel(n) = p_init_cabling(n) - 0.001;
                    else
                        p_max_or(n) = 0.002;
                        p_min_cabel(n) = 0;
                    end
                end
            end
        end
        [p_best_cable,fval_cabel,exitflag_cabel,output_cabel]=function_fit_easyfit2(app,m_w_hf,[m_real_hf, m_imag_hf],p_init_cabling,@function_model_all_types2, p_min_cabel, p_max_cabel ,options, formula_cabel,app.used_optimization_algorithm);
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1,2:4)=p_best_cable;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2,2:4)=p_best_cable;
        
        %Cell parameter fit with esb boundaries
        temporary_variable=cell2mat(app.Boundaries_Default{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max_or=transpose(temporary_variable(:,2));
        p_min_or=transpose(temporary_variable(:,1));
        if length(p_init_esb)~=length(p_min_or)
            if length(p_init_esb)>length(p_min_or)
                p_init_esb=p_init_esb(1:length(p_min_or));
            elseif length(p_init)<length(p_min_or)
                p_init_esb=[p_init_esb zeros(1,length(p_min_or)-length(p_init_esb))];
            end
        end
        p_init_esb = [p_init_esb(1), p_best_cable, p_init_esb(5:end)];
        p_min_or=[p_min_or(1), p_best_cable ,p_min_or(5:end)];
        p_max_or=[p_max_or(1), p_best_cable ,p_max_or(5:end)];
        for n = 1:length(p_min_or)
            if p_min_or(n)<0
                p_min_or(n) = 0;
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init_esb)
                if p_max_or(n) - p_min_or(n) < 0.002
                    if p_init_esb_compl(n) > 0.001
                        p_max_or(n) = p_init_esb(n) + 0.001;
                        p_min_or(n) = p_init_esb(n) - 0.001;
                    else
                        p_max_or(n) = 0.002;
                        p_min_or(n) = 0;
                    end
                end
            end
        end
        [p_best_or,fval_or,exitflag_or,output_or]=function_fit_easyfit2(app,m_w_mf,[m_real_mf, m_imag_mf],p_init_esb,@function_model_all_types2, p_min_or, p_max_or ,options, formula,app.used_optimization_algorithm);
        app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1)=fval_or;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1,:)=p_best_or;
        
        %Cell parameter fit with calculated boundaries
        temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max=transpose(temporary_variable(:,2));
        p_min=transpose(temporary_variable(:,1));
        if length(p_init_esb)~=length(p_min)
            if length(p_init_esb)>length(p_min)
                p_init_esb=p_init_esb(1:length(p_min));
            elseif length(p_init_esb)<length(p_min)
                p_init_esb=[p_init_esb zeros(1,length(p_min)-length(p_init_esb))];
            end
        end
        p_init_esb = [p_init_esb(1), p_best_cable, p_init_esb(5:end)];
        p_min=[p_min(1), p_best_cable ,p_min(5:end)];
        p_max=[p_max(1), p_best_cable ,p_max(5:end)];
        for n = 1:length(p_min)
            if p_min(n)<0
                p_min(n) = 0;
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init_esb)
                if p_max(n) - p_min(n) < 0.002
                    if p_init_esb(n) > 0.001
                        p_max(n) = p_init_esb(n) + 0.001;
                        p_min(n) = p_init_esb(n) - 0.001;
                    else
                        p_max(n) = 0.002;
                        p_min(n) = 0;
                    end
                end
            end
        end
        [p_best,fval,exitflag_or,output_or]=function_fit_easyfit2(app,m_w_mf,[m_real_mf, m_imag_mf],p_init_esb,@function_model_all_types2, p_min, p_max ,options, formula,app.used_optimization_algorithm);
        app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2)=fval;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2,:)=p_best;
    case 'LiIon4'
        index_cabling = find(-m_imag<0);
        index_esb = find(-m_imag>0);
        if isempty(index_cabling)
            index_cabling = find(DRT_GUI.Messdaten.frequenz>=1000);
            index_esb  = find(DRT_GUI.Messdaten.frequenz<1000);
        end
        
        
        
        %Seperate the induktiv part resembling the cabling
        m_w_hf = m_w(index_cabling);
        m_real_hf = m_real(index_cabling);
        m_imag_hf = m_imag(index_cabling);
        
        %Seperate the capacitiv part resembling the rest hte actual
        %battery
        m_w_mf = m_w(index_esb);
        m_real_mf = m_real(index_esb);
        m_imag_mf = m_imag(index_esb);
        
        %Seperate this part into hf,mf and lf
        [pks_u,locs_u] = findpeaks(m_imag_mf);
        if length(locs_u)> 1
            [minimum_value, step_index_index] = max(pks_u);
            step_index = locs_u(step_index_index) + 2;
            %                 step_index = locs_u(end) + 7;
        elseif length(locs_u) < 1
            step_index = findchangepts(diff(m_real_mf));
        else
            step_index = locs_u(end) + 2;
        end
        
        m_real_lf = m_real_mf(step_index-1:end);
        m_imag_lf = m_imag_mf(step_index-1:end);
        m_w_lf = m_w_mf(step_index-1:end);
        
        m_w_mf = m_w_mf(1:step_index-2);
        m_real_mf = m_real_mf(1:step_index-2);
        m_imag_mf = m_imag_mf(1:step_index-2);
        
        m_w_mf = [m_w_hf ; m_w_mf];
        m_real_mf = [m_real_hf ; m_real_mf];
        m_imag_mf = [m_imag_hf ; m_imag_mf];
        
        %Get fittet values from Gui table
        %             p_init_cabling = DRT_GUI.Fit.Parameter(2:4);
        p_init_cabling = p_init(1:4);
        p_init_esb = p_init(1:end-4);
        p_init_esb_compl = p_init;
        p_init_lf = p_init(14:17);
        
        %Cabel parameter fit
        %             formula_cabel='1.*(p(1)./(1+p(1)./((1i.*w).^p(3).*p(2))))' ;
        %             temporary_variable=cell2mat(app.Boundaries_Default{app.current_temp_index,app.current_SOC_index});
        %             p_max_cabel=transpose(temporary_variable(2:4,2));
        %             p_min_cabel=transpose(temporary_variable(2:4,1));
        formula_cabel='1.*(p(1))+1.*(p(2)./(1+p(2)./((1i.*w).^p(4).*p(3))))' ;
        temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max_cabel=transpose(temporary_variable(1:4,2));
        p_min_cabel=transpose(temporary_variable(1:4,1));
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init_cabling)
                if p_max_cabel(n) - p_min_cabel(n) < 0.002
                    if p_init_cabling(n) > 0.001
                        p_max_cabel(n) = p_init_cabling(n) + 0.001;
                        p_min_cabel(n) = p_init_cabling(n) - 0.001;
                    else
                        p_max_cabel(n) = 0.002;
                        p_min_cabel(n) = 0;
                    end
                end
            end
        end
        for n = 1:length(p_min_cabel)
            if p_min_cabel(n)<0
                p_min_cabel(n) = 0;
            end
        end
        [p_best_cable,fval_cabel,exitflag_cabel,output_cabel]=function_fit_easyfit2(app,m_w_hf,[m_real_hf, m_imag_hf],p_init_cabling,@function_model_all_types2, p_min_cabel, p_max_cabel ,options, formula_cabel,[]);
        %             app.parameters{app.current_temp_index,app.current_SOC_index}(1,2:4)=p_best_cabel;
        %             app.parameters{app.current_temp_index,app.current_SOC_index}(2,2:4)=p_best_cabel;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1,1:4)=p_best_cable;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2,1:4)=p_best_cable;
        
        %Cell parameter fit with esb boundaries
        formula_mf_hf =  '1.*(p(1))+1.*(p(2)./(1+p(2)./((1i.*w).^p(4).*p(3))))+1.*(0)+1.*((p(8).*(p(5)./(1+1i.*w.*p(6)).^p(7))).^0.5.*coth((p(8)./(p(5)./(1+1i.*w.*p(6)).^p(7))).^0.5))+1.*(0)+1.*(0)+1.*((p(8).*(p(9)./(1+1i.*w.*p(10)).^p(11))).^0.5.*coth((p(8)./(p(9)./(1+1i.*w.*p(10)).^p(11))).^0.5))+1.*((p(8).*(Z_parallel((p(9)==0).*10^9+1./((p(10)./p(9)).*(1i.*w)), p(9) + 10* p(12) .* tanh((1i.*w.*p(13)*20).^0.5)./((1i.*w.*p(13)*20).^0.5-tanh((1i.*w.*p(13)*20).^0.5)))-p(9)./(1+1i.*w.*p(10))+p(9)./(1+1i.*w.*p(10)).^p(11))).^0.5.*coth((p(8)./(Z_parallel((p(9)==0).*10^9+1./((p(10)./p(9)).*(1i.*w)), p(9) + 10* p(12) .* tanh((1i.*w.*p(13)*20).^0.5)./((1i.*w.*p(13)*20).^0.5-tanh((1i.*w.*p(13)*20).^0.5)))-p(9)./(1+1i.*w.*p(10))+p(9)./(1+1i.*w.*p(10)).^p(11))).^0.5) -((p(8).*(p(9)./(1+1i.*w.*p(10)).^p(11))).^0.5.*coth((p(8)./(p(9)./(1+1i.*w.*p(10)).^p(11))).^0.5)))+1.*(0)+1.*(0)+1.*(-10* p(12) .* tanh((1i.*w.*p(13)*20).^0.5)./((1i.*w.*p(13)*20).^0.5-tanh((1i.*w.*p(13)*20).^0.5)))+1.*(0)+1.*(0)+1.*(10* p(12) .* tanh((1i.*w.*p(13)*20).^0.5)./((1i.*w.*p(13)*20).^0.5-tanh((1i.*w.*p(13)*20).^0.5)))';
        temporary_variable=cell2mat(app.Boundaries_Default{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max_default=transpose(temporary_variable(1:13,2));
        p_min_default=transpose(temporary_variable(1:13,1));
        if length(p_init_esb)~=length(p_min_default)
            if length(p_init_esb)>length(p_min_default)
                p_init_esb=p_init_esb(1:length(p_min_default));
            elseif length(p_init)<length(p_min_default)
                p_init_esb=[p_init_esb zeros(1,length(p_min_default)-length(p_init_esb))];
            end
        end
        %             p_init_esb = [p_init_esb(1), p_best_cabel, p_init_esb(5:end)];
        %             p_min_default=[p_min_default(1), p_best_cabel ,p_min_default(5:end)];
        %             p_max_default=[p_max_default(1), p_best_cabel ,p_max_default(5:end)];
        p_init_esb = [ p_best_cable, p_init_esb(5:end)];
        p_min_default=[0.95* p_best_cable ,p_min_default(5:end)];
        p_max_default=[1.05* p_best_cable ,p_max_default(5:end)];
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init_esb)
                if p_max_default(n) - p_min_default(n) < 0.002
                    if p_init_esb(n) > 0.001
                        p_max_default(n) = p_init_esb(n) + 0.001;
                        p_min_default(n) = p_init_esb(n) - 0.001;
                    else
                        p_max_default(n) = 0.002;
                        p_min_default(n) = 0;
                    end
                end
            end
        end
        for n = 1:length(p_min_default)
            if p_min_default(n)<0
                p_min_default(n) = 0;
            end
        end
        [p_best_or,fval_or,exitflag_or,output_or]=function_fit_easyfit2(app,m_w_mf,[m_real_mf, m_imag_mf],p_init_esb,@function_model_all_types2, p_min_default, p_max_default ,options, formula_mf_hf,[]);
        % %             app.RMSE{app.current_temp_index,app.current_SOC_index}(1)=fval_or;
        % %             app.parameters{app.current_temp_index,app.current_SOC_index}(1,:)=p_best_or;
        
        %Cell parameter fit with calculated boundaries
        temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max_esb=transpose(temporary_variable(1:13,2));
        p_min_esb=transpose(temporary_variable(1:13,1));
        if length(p_init_esb)~=length(p_min_esb)
            if length(p_init_esb)>length(p_min_esb)
                p_init_esb=p_init_esb(1:length(p_min_esb));
            elseif length(p_init_esb)<length(p_min_esb)
                p_init_esb=[p_init_esb zeros(1,length(p_min_esb)-length(p_init_esb))];
            end
        end
        %             p_init_esb = [p_init_esb(1), p_best_cabel, p_init_esb(5:end)];
        %             p_min_esb=[p_min_esb(1), p_best_cabel ,p_min_esb(5:end)];
        %             p_max_esb=[p_max_esb(1), p_best_cabel ,p_max_esb(5:end)];
        p_init_esb = [ p_best_cable, p_init_esb(5:end)];
        p_min_esb=[0.95* p_best_cable ,p_min_esb(5:end)];
        p_max_esb=[1.05* p_best_cable ,p_max_esb(5:end)];
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init_esb)
                if p_max_esb(n) - p_min_esb(n) < 0.002
                    if p_init_esb(n) > 0.001
                        p_max_esb(n) = p_init_esb(n) + 0.001;
                        p_min_esb(n) = p_init_esb(n) - 0.001;
                    else
                        p_max_esb(n) = 0.002;
                        p_min_esb(n) = 0;
                    end
                end
            end
        end
        for n = 1:length(p_min_esb)
            if p_min_esb(n)<0
                p_min_esb(n) = 0;
            end
        end
        [p_best,fval,exitflag_or,output_or]=function_fit_easyfit2(app,m_w_mf,[m_real_mf, m_imag_mf],p_init_esb,@function_model_all_types2, p_min_esb, p_max_esb ,options, formula_mf_hf,[]);
        % %             app.RMSE{app.current_temp_index,app.current_SOC_index}(2)=fval;
        % %             app.parameters{app.current_temp_index,app.current_SOC_index}(2,:)=p_best;
        
        %Fix fittet Parameters and fit last two RC-Elements on slow
        %semi-circle
        
        %for default boundaries
        temporary_variable=cell2mat(app.Boundaries_Default{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max_esb_compl_default=transpose(temporary_variable(:,2));
        p_min_esb_compl_default=transpose(temporary_variable(:,1));
        if length(p_init_esb_compl)~=length(p_min_esb_compl_default)
            if length(p_init_esb_compl)>length(p_min_esb_compl_default)
                p_init_esb_compl=p_init_esb_compl(1:length(p_min_esb_compl_default));
            elseif length(p_init_esb_compl)<length(p_min_esb_compl_default)
                p_init_esb_compl=[p_init_esb_compl zeros(1,length(p_min_esb_compl_default)-length(p_init_esb_compl))];
            end
        end
        p_init_esb_compl = [p_best_or, p_init_esb_compl(14:end)];
        p_min_or=[0.95*p_best_or(1:4),0.9*p_best_or(5:13), p_min_esb_compl_default(14:end)];
        p_max_or=[1.05*p_best_or(1:4),1.1*p_best_or(5:13), p_max_esb_compl_default(14:end)];
        for N = 1:length(p_max_or)
            if app.ParamTable.Data{N,2}
                p_min_or(N) = app.ParamTable.Data{N,3};
                p_max_or(N) = app.ParamTable.Data{N,3};
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init_esb_compl)
                if p_max_or(n) - p_min_or(n) < 0.002
                    if p_init_esb_compl(n) > 0.001
                        p_max_or(n) = p_init_esb_compl(n) + 0.001;
                        p_min_or(n) = p_init_esb_compl(n) - 0.001;
                    else
                        p_max_or(n) = 0.002;
                        p_min_or(n) = 0;
                    end
                end
            end
        end
        for n = 1:length(p_min_or)
            if p_min_or(n)<0
                p_min_or(n) = 0;
            end
        end
        [p_best_or_compl_default,fval_or_compl_default,exitflag_or_compl_default,output_or_compl_default]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init_esb_compl,@function_model_all_types2, p_min_or, p_max_or ,options, formula,app.used_optimization_algorithm);
        app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1)=fval_or_compl_default;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1,:)=p_best_or_compl_default;
        fval_or = fval_or_compl_default;
        p_best_or = p_best_or_compl_default;
        
        %for calculated boundaries
        temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max_esb_compl=transpose(temporary_variable(:,2));
        p_min_esb_compl=transpose(temporary_variable(:,1));
        if length(p_init_esb_compl)~=length(p_min_esb_compl)
            if length(p_init_esb_compl)>length(p_min_esb_compl)
                p_init_esb_compl=p_init_esb_compl(1:length(p_min_esb_compl));
            elseif length(p_init_esb_compl)<length(p_min_esb_compl)
                p_init_esb_compl=[p_init_esb_compl zeros(1,length(p_min_esb_compl)-length(p_init_esb_compl))];
            end
        end
        p_init_esb_compl = [p_best, p_init_esb_compl(14:end)];
        p_min=[0.95*p_best(1:4),0.9*p_best(5:13) ,p_min_esb_compl(14:end)];
        p_max=[1.05*p_best(1:4),1.1*p_best(5:13) ,p_max_esb_compl(14:end)];
        for N = 1:length(p_max)
            if app.ParamTable.Data{N,2}
                p_min(N) = app.ParamTable.Data{N,3};
                p_max(N) = app.ParamTable.Data{N,3};
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init_esb_compl)
                if p_max(n) - p_min(n) < 0.002
                    if p_init_esb_compl(n) > 0.001
                        p_max(n) = p_init_esb_compl(n) + 0.001;
                        p_min(n) = p_init_esb_compl(n) - 0.001;
                    else
                        p_max(n) = 0.002;
                        p_min(n) = 0;
                    end
                end
            end
        end
        for n = 1:length(p_min)
            if p_min(n)<0
                p_min(n) = 0;
            end
        end
        [p_best_or_compl,fval_or_compl,exitflag_or_compl,output_or_compl]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init_esb_compl,@function_model_all_types2, p_min, p_max ,options, formula,app.used_optimization_algorithm);
        app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2)=fval_or_compl;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2,:)=p_best_or_compl;
        fval = fval_or_compl;
        p_best = p_best_or_compl;
    case 'LiIon4_NoInd'
        if app.current_crate_index == app.static_EIS_index
            index_cabling = find(-m_imag<0);
            index_esb = find(-m_imag>0);
            
            %Seperate the induktiv part resembling the cabling
            m_w_hf = m_w(index_cabling);
            m_real_hf = m_real(index_cabling);
            m_imag_hf = m_imag(index_cabling);
            
            %Seperate the capacitiv part resembling the rest the actual
            %battery
            m_w_mf = m_w(index_esb);
            m_real_mf = m_real(index_esb);
            m_imag_mf = m_imag(index_esb);
            
            %Seperate this part into hf,mf and lf
            if ~app.GUI
                %             interpolated_real = min(m_real_mf):(max(m_real_mf)-min(m_real_mf))/30:max(m_real_mf);
                %             interpolated_imag = interp1(m_real_mf,m_imag_mf,min(m_real_mf):(max(m_real_mf)-min(m_real_mf))/30:max(m_real_mf),'spline');
                %             interpolated_w = interp1(m_real_mf,m_w_mf,min(m_real_mf):(max(m_real_mf)-min(m_real_mf))/30:max(m_real_mf),'spline');
                interpolated_real =m_real_mf;
                interpolated_imag = m_imag_mf;
                interpolated_w = m_w_mf;
                [pks_u,locs_u] = findpeaks(interpolated_imag);
                if length(locs_u)> 1
                    [minimum_value, step_index_index] = max(pks_u);
                    step_index = locs_u(step_index_index) + 2;
                elseif length(locs_u) < 1
                    [~,turning_point_idx] = min(abs(diff(diff(interpolated_imag))-0));
                    step_index = turning_point_idx + 3;
                else
                    step_index = locs_u(end) + 2;
                end
                m_w_mf = transpose(interpolated_w);
                m_real_mf = transpose(interpolated_real);
                m_imag_mf = transpose(interpolated_imag);
                m_w = transpose(interpolated_w);
                m_real = transpose(interpolated_real);
                m_imag = transpose(interpolated_imag);
            else
                [pks_u,locs_u] = findpeaks(m_imag_mf);
                if length(locs_u)> 1
                    [minimum_value, step_index_index] = max(pks_u);
                    step_index = locs_u(step_index_index) + 2;
                    %                 step_index = locs_u(end) + 7;
                elseif length(locs_u) < 1
                    interpolated_real = min(m_real_mf):(max(m_real_mf)-min(m_real_mf))/30:max(m_real_mf);
                    interpolated_imag = interp1(m_real_mf,m_imag_mf,min(m_real_mf):(max(m_real_mf)-min(m_real_mf))/30:max(m_real_mf),'spline');
                    %             [~,turning_point_idx] = max(-diff(diff(interpolated_imag)));
                    [~,turning_point_idx] = min(abs(diff(diff(interpolated_imag))-0));
                    [~,step_index] = min(abs(m_real_mf - interpolated_real(turning_point_idx) ));
                    if step_index < length(m_real_mf)-3
                        step_index = step_index + 3;
                    end
                    %             step_index = findchangepts(diff(m_real_mf));
                else
                    step_index = locs_u(end) + 2;
                end
            end
            %       old version without interpolation
            %         if length(locs_u)> 1
            %             [minimum_value, step_index_index] = max(pks_u);
            %             step_index = locs_u(step_index_index) + 2;
            %             %                 step_index = locs_u(end) + 7;
            %         elseif length(locs_u) < 1
            %             interpolated_real = min(m_real_mf):(max(m_real_mf)-min(m_real_mf))/100:max(m_real_mf);
            %             interpolated_imag = interp1(m_real_mf,m_imag_mf,min(m_real_mf):(max(m_real_mf)-min(m_real_mf))/100:max(m_real_mf),'spline');
            %             %             [~,turning_point_idx] = max(-diff(diff(interpolated_imag)));
            %             [~,turning_point_idx] = min(abs(diff(diff(interpolated_imag))-0));
            %             [~,step_index] = min(abs(m_real_mf - interpolated_real(turning_point_idx) ));
            %             if step_index < length(m_real_mf)-3
            %                 step_index = step_index + 3;
            %             end
            %             %             step_index = findchangepts(diff(m_real_mf));
            %         else
            %             step_index = locs_u(end) + 2;
            %         end
            
            m_real_lf = m_real_mf(step_index-1:end);
            m_imag_lf = m_imag_mf(step_index-1:end);
            m_w_lf = m_w_mf(step_index-1:end);
            
            m_w_mf = m_w_mf(1:step_index-2);
            m_real_mf = m_real_mf(1:step_index-2);
            m_imag_mf = m_imag_mf(1:step_index-2);
            
            
            %Get fittet values from Gui table
            p_init_esb = p_init(1:end-4);
            p_init_esb_compl = p_init;
            p_init_lf = p_init(11:14);
            
            %Cell parameter fit with esb boundaries
            formula_mf_hf =  '1.*(p(1))+1.*((p(5).*(p(2)./(1+1i.*w.*p(3)).^p(4))).^0.5.*coth((p(5)./(p(2)./(1+1i.*w.*p(3)).^p(4))).^0.5))+1.*((p(5).*(Z_parallel((p(6)==0).*10^9+1./((p(7)./p(6)).*(1i.*w)), p(6) + 10* p(9) .* tanh((1i.*w.*p(10)*20).^0.5)./((1i.*w.*p(10)*20).^0.5-tanh((1i.*w.*p(10)*20).^0.5)))-p(6)./(1+1i.*w.*p(7))+p(6)./(1+1i.*w.*p(7)).^p(8))).^0.5.*coth((p(5)./(Z_parallel((p(6)==0).*10^9+1./((p(7)./p(6)).*(1i.*w)), p(6) + 10* p(9) .* tanh((1i.*w.*p(10)*20).^0.5)./((1i.*w.*p(10)*20).^0.5-tanh((1i.*w.*p(10)*20).^0.5)))-p(6)./(1+1i.*w.*p(7))+p(6)./(1+1i.*w.*p(7)).^p(8))).^0.5) )+1.*(-10* p(9) .* tanh((1i.*w.*p(10)*20).^0.5)./((1i.*w.*p(10)*20).^0.5-tanh((1i.*w.*p(10)*20).^0.5)))+1.*(10* p(9) .* tanh((1i.*w.*p(10)*20).^0.5)./((1i.*w.*p(10)*20).^0.5-tanh((1i.*w.*p(10)*20).^0.5)))';
            temporary_variable=cell2mat(app.Boundaries_Default{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
            p_max_default=transpose(temporary_variable(1:10,2));
            p_min_default=transpose(temporary_variable(1:10,1));
            if length(p_init_esb)~=length(p_min_default)
                if length(p_init_esb)>length(p_min_default)
                    p_init_esb=p_init_esb(1:length(p_min_default));
                elseif length(p_init)<length(p_min_default)
                    p_init_esb=[p_init_esb zeros(1,length(p_min_default)-length(p_init_esb))];
                end
            end
            if strcmp(app.used_optimization_algorithm,'SimAnn')
                for n = 1:length(p_init_esb)
                    if p_max_default(n) - p_min_default(n) < 0.002
                        if p_init_esb(n) > 0.001
                            p_max_default(n) = p_init_esb(n) + 0.001;
                            p_min_default(n) = p_init_esb(n) - 0.001;
                        else
                            p_max_default(n) = 0.002;
                            p_min_default(n) = 0;
                        end
                    end
                end
            end
            for n = 1:length(p_min_default)
                if p_min_default(n)<0
                    p_min_default(n) = 0;
                end
            end
            [p_best_or,fval_or,exitflag_or,output_or]=function_fit_easyfit2(app,m_w_mf,[m_real_mf, m_imag_mf],p_init_esb,@function_model_all_types2, p_min_default, p_max_default ,options, formula_mf_hf,[]);
            
            %Cell parameter fit with calculated boundaries
            temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
            p_max_esb=transpose(temporary_variable(1:10,2));
            p_min_esb=transpose(temporary_variable(1:10,1));
            if length(p_init_esb)~=length(p_min_esb)
                if length(p_init_esb)>length(p_min_esb)
                    p_init_esb=p_init_esb(1:length(p_min_esb));
                elseif length(p_init_esb)<length(p_min_esb)
                    p_init_esb=[p_init_esb zeros(1,length(p_min_esb)-length(p_init_esb))];
                end
            end
            if strcmp(app.used_optimization_algorithm,'SimAnn')
                for n = 1:length(p_init_esb)
                    if p_max_esb(n) - p_min_esb(n) < 0.002
                        if p_init_esb(n) > 0.001
                            p_max_esb(n) = p_init_esb(n) + 0.001;
                            p_min_esb(n) = p_init_esb(n) - 0.001;
                        else
                            p_max_esb(n) = 0.002;
                            p_min_esb(n) = 0;
                        end
                    end
                end
            end
            for n = 1:length(p_min_esb)
                if p_min_esb(n)<0
                    p_min_esb(n) = 0;
                end
            end
            [p_best,fval,exitflag_or,output_or]=function_fit_easyfit2(app,m_w_mf,[m_real_mf, m_imag_mf],p_init_esb,@function_model_all_types2, p_min_esb, p_max_esb ,options, formula_mf_hf,[]);
            %Fix fittet Parameters and fit last two RC-Elements on slow
            %semi-circle
            
            %for default boundaries
            temporary_variable=cell2mat(app.Boundaries_Default{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
            p_max_esb_compl_default=transpose(temporary_variable(:,2));
            p_min_esb_compl_default=transpose(temporary_variable(:,1));
            if length(p_init_esb_compl)~=length(p_min_esb_compl_default)
                if length(p_init_esb_compl)>length(p_min_esb_compl_default)
                    p_init_esb_compl=p_init_esb_compl(1:length(p_min_esb_compl_default));
                elseif length(p_init_esb_compl)<length(p_min_esb_compl_default)
                    p_init_esb_compl=[p_init_esb_compl zeros(1,length(p_min_esb_compl_default)-length(p_init_esb_compl))];
                end
            end
            p_init_esb_compl = [p_best_or, p_init_esb_compl(11:end)];
            p_min_or=[0.95*p_best_or(1),0.9*p_best_or(2:10), p_min_esb_compl_default(11:end)];
            p_max_or=[1.05*p_best_or(1),1.1*p_best_or(2:10), p_max_esb_compl_default(11:end)];
            for N = 1:length(p_max_or)
                if app.ParamTable.Data{N,2}
                    p_min_or(N) = app.ParamTable.Data{N,3};
                    p_max_or(N) = app.ParamTable.Data{N,3};
                end
            end
            if strcmp(app.used_optimization_algorithm,'SimAnn')
                for n = 1:length(p_init_esb_compl)
                    if p_max_or(n) - p_min_or(n) < 0.002
                        if p_init_esb_compl(n) > 0.001
                            p_max_or(n) = p_init_esb_compl(n) + 0.001;
                            p_min_or(n) = p_init_esb_compl(n) - 0.001;
                        else
                            p_max_or(n) = 0.002;
                            p_min_or(n) = 0;
                        end
                    end
                end
            end
            for n = 1:length(p_min_or)
                if p_min_or(n)<0
                    p_min_or(n) = 0;
                end
            end
            [p_best_or_compl_default,fval_or_compl_default,exitflag_or_compl_default,output_or_compl_default]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init_esb_compl,@function_model_all_types2, p_min_or, p_max_or ,options, formula,app.used_optimization_algorithm);
            app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1)=fval_or_compl_default;
            app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1,:)=p_best_or_compl_default;
            fval_or = fval_or_compl_default;
            p_best_or = p_best_or_compl_default;
            
            %for calculated boundaries
            temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
            p_max_esb_compl=transpose(temporary_variable(:,2));
            p_min_esb_compl=transpose(temporary_variable(:,1));
            if length(p_init_esb_compl)~=length(p_min_esb_compl)
                if length(p_init_esb_compl)>length(p_min_esb_compl)
                    p_init_esb_compl=p_init_esb_compl(1:length(p_min_esb_compl));
                elseif length(p_init_esb_compl)<length(p_min_esb_compl)
                    p_init_esb_compl=[p_init_esb_compl zeros(1,length(p_min_esb_compl)-length(p_init_esb_compl))];
                end
            end
            p_init_esb_compl = [p_best, p_init_esb_compl(11:end)];
            p_min=[0.95*p_best(1),0.9*p_best(2:10) ,p_min_esb_compl(11:end)];
            p_max=[1.05*p_best(1),1.1*p_best(2:10) ,p_max_esb_compl(11:end)];
            for N = 1:length(p_max)
                if app.ParamTable.Data{N,2}
                    p_min(N) = app.ParamTable.Data{N,3};
                    p_max(N) = app.ParamTable.Data{N,3};
                end
            end
            if strcmp(app.used_optimization_algorithm,'SimAnn')
                for n = 1:length(p_init_esb_compl)
                    if p_max(n) - p_min(n) < 0.002
                        if p_init_esb_compl(n) > 0.001
                            p_max(n) = p_init_esb_compl(n) + 0.001;
                            p_min(n) = p_init_esb_compl(n) - 0.001;
                        else
                            p_max(n) = 0.002;
                            p_min(n) = 0;
                        end
                    end
                end
            end
            for n = 1:length(p_min)
                if p_min(n)<0
                    p_min(n) = 0;
                end
            end
            [p_best_or_compl,fval_or_compl,exitflag_or_compl,output_or_compl]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init_esb_compl,@function_model_all_types2, p_min, p_max ,options, formula,app.used_optimization_algorithm);
            app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2)=fval_or_compl;
            app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2,:)=p_best_or_compl;
            fval = fval_or_compl;
            p_best = p_best_or_compl;
        else
            index_cabling = find(-m_imag<0);
            index_esb = find(-m_imag>0);
            
            %Seperate the induktiv part resembling the cabling
            m_w_hf = m_w(index_cabling);
            m_real_hf = m_real(index_cabling);
            m_imag_hf = m_imag(index_cabling);
            
            %Seperate the capacitiv part resembling the rest the actual
            %battery
            m_w_mf = m_w(index_esb);
            m_real_mf = m_real(index_esb);
            m_imag_mf = m_imag(index_esb);
            
            %Seperate this part into hf,mf and lf
            if ~app.GUI                
                interpolated_real =m_real_mf;
                interpolated_imag = m_imag_mf;
                interpolated_w = m_w_mf;
                [pks_u,locs_u] = findpeaks(interpolated_imag);
                if length(locs_u)> 1
                    [minimum_value, step_index_index] = max(pks_u);
                    step_index = locs_u(step_index_index) + 2;
                elseif length(locs_u) < 1
                    [~,turning_point_idx] = min(abs(diff(diff(interpolated_imag))-0));
                    step_index = turning_point_idx + 3;
                else
                    step_index = locs_u(end) + 2;
                end
                m_w_mf = transpose(interpolated_w);
                m_real_mf = transpose(interpolated_real);
                m_imag_mf = transpose(interpolated_imag);
                m_w = transpose(interpolated_w);
                m_real = transpose(interpolated_real);
                m_imag = transpose(interpolated_imag);
            else
                [pks_u,locs_u] = findpeaks(m_imag_mf);
                if length(locs_u)> 1
                    [minimum_value, step_index_index] = max(pks_u);
                    step_index = locs_u(step_index_index) + 2;
                    %                 step_index = locs_u(end) + 7;
                elseif length(locs_u) < 1
                    interpolated_real = min(m_real_mf):(max(m_real_mf)-min(m_real_mf))/30:max(m_real_mf);
                    interpolated_imag = interp1(m_real_mf,m_imag_mf,min(m_real_mf):(max(m_real_mf)-min(m_real_mf))/30:max(m_real_mf),'spline');
                    %             [~,turning_point_idx] = max(-diff(diff(interpolated_imag)));
                    [~,turning_point_idx] = min(abs(diff(diff(interpolated_imag))-0));
                    [~,step_index] = min(abs(m_real_mf - interpolated_real(turning_point_idx) ));
                    if step_index < length(m_real_mf)-3
                        step_index = step_index + 3;
                    end
                    %             step_index = findchangepts(diff(m_real_mf));
                else
                    step_index = locs_u(end) + 2;
                end
            end
                       
            m_real_lf = m_real_mf(step_index-1:end);
            m_imag_lf = m_imag_mf(step_index-1:end);
            m_w_lf = m_w_mf(step_index-1:end);
            
            m_w_mf = m_w_mf(1:step_index-2);
            m_real_mf = m_real_mf(1:step_index-2);
            m_imag_mf = m_imag_mf(1:step_index-2);
            
            
            %Get fittet values from Gui table
            p_init_esb = p_init(1:end-4);
            p_init_esb_compl = p_init;
            p_init_lf = p_init(11:14);
            
            %Cell parameter fit with esb boundaries
            formula_mf_hf =  '1.*(p(1))+1.*((p(5).*(p(2)./(1+1i.*w.*p(3)).^p(4))).^0.5.*coth((p(5)./(p(2)./(1+1i.*w.*p(3)).^p(4))).^0.5))+1.*((p(5).*(Z_parallel((p(6)==0).*10^9+1./((p(7)./p(6)).*(1i.*w)), p(6) + 10* p(9) .* tanh((1i.*w.*p(10)*20).^0.5)./((1i.*w.*p(10)*20).^0.5-tanh((1i.*w.*p(10)*20).^0.5)))-p(6)./(1+1i.*w.*p(7))+p(6)./(1+1i.*w.*p(7)).^p(8))).^0.5.*coth((p(5)./(Z_parallel((p(6)==0).*10^9+1./((p(7)./p(6)).*(1i.*w)), p(6) + 10* p(9) .* tanh((1i.*w.*p(10)*20).^0.5)./((1i.*w.*p(10)*20).^0.5-tanh((1i.*w.*p(10)*20).^0.5)))-p(6)./(1+1i.*w.*p(7))+p(6)./(1+1i.*w.*p(7)).^p(8))).^0.5) )+1.*(-10* p(9) .* tanh((1i.*w.*p(10)*20).^0.5)./((1i.*w.*p(10)*20).^0.5-tanh((1i.*w.*p(10)*20).^0.5)))+1.*(10* p(9) .* tanh((1i.*w.*p(10)*20).^0.5)./((1i.*w.*p(10)*20).^0.5-tanh((1i.*w.*p(10)*20).^0.5)))';
            temporary_variable=cell2mat(app.Boundaries_Default{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
            p_max_default=transpose(temporary_variable(1:10,2));
            p_min_default=transpose(temporary_variable(1:10,1));
            if length(p_init_esb)~=length(p_min_default)
                if length(p_init_esb)>length(p_min_default)
                    p_init_esb=p_init_esb(1:length(p_min_default));
                elseif length(p_init)<length(p_min_default)
                    p_init_esb=[p_init_esb zeros(1,length(p_min_default)-length(p_init_esb))];
                end
            end
            if strcmp(app.used_optimization_algorithm,'SimAnn')
                for n = 1:length(p_init_esb)
                    if p_max_default(n) - p_min_default(n) < 0.002
                        if p_init_esb(n) > 0.001
                            p_max_default(n) = p_init_esb(n) + 0.001;
                            p_min_default(n) = p_init_esb(n) - 0.001;
                        else
                            p_max_default(n) = 0.002;
                            p_min_default(n) = 0;
                        end
                    end
                end
            end
            for n = 1:length(p_min_default)
                if p_min_default(n)<0
                    p_min_default(n) = 0;
                end
            end
            [p_best_or,fval_or,exitflag_or,output_or]=function_fit_easyfit2(app,m_w_mf,[m_real_mf, m_imag_mf],p_init_esb,@function_model_all_types2, p_min_default, p_max_default ,options, formula_mf_hf,[]);
            
            %Cell parameter fit with calculated boundaries
            temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
            p_max_esb=transpose(temporary_variable(1:10,2));
            p_min_esb=transpose(temporary_variable(1:10,1));
            if length(p_init_esb)~=length(p_min_esb)
                if length(p_init_esb)>length(p_min_esb)
                    p_init_esb=p_init_esb(1:length(p_min_esb));
                elseif length(p_init_esb)<length(p_min_esb)
                    p_init_esb=[p_init_esb zeros(1,length(p_min_esb)-length(p_init_esb))];
                end
            end
            if strcmp(app.used_optimization_algorithm,'SimAnn')
                for n = 1:length(p_init_esb)
                    if p_max_esb(n) - p_min_esb(n) < 0.002
                        if p_init_esb(n) > 0.001
                            p_max_esb(n) = p_init_esb(n) + 0.001;
                            p_min_esb(n) = p_init_esb(n) - 0.001;
                        else
                            p_max_esb(n) = 0.002;
                            p_min_esb(n) = 0;
                        end
                    end
                end
            end
            for n = 1:length(p_min_esb)
                if p_min_esb(n)<0
                    p_min_esb(n) = 0;
                end
            end
            [p_best,fval,exitflag_or,output_or]=function_fit_easyfit2(app,m_w_mf,[m_real_mf, m_imag_mf],p_init_esb,@function_model_all_types2, p_min_esb, p_max_esb ,options, formula_mf_hf,[]);
                        
           [~,soc_index] = min(abs(sorted_SOC_array{app.current_hyst_index}{app.static_EIS_index}{app.current_temp_index}-sorted_SOC_array{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}(app.current_SOC_index)));
            app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1,11:15) = app.optimal_parameters{app.current_hyst_index}{app.static_EIS_index}{app.current_temp_index}{soc_index}(11:15);
            app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2,11:15) = app.optimal_parameters{app.current_hyst_index}{app.static_EIS_index}{app.current_temp_index}{soc_index}(11:15);
            
            p_best = app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2,:);
            p_best_or = app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1,:);
            
            %extrapolated
            temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
            p_max_esb_compl=transpose(temporary_variable(:,2));
            p_min_esb_compl=transpose(temporary_variable(:,1));
            if length(p_init_esb_compl)~=length(p_min_esb_compl)
                if length(p_init_esb_compl)>length(p_min_esb_compl)
                    p_init_esb_compl=p_init_esb_compl(1:length(p_min_esb_compl));
                elseif length(p_init_esb_compl)<length(p_min_esb_compl)
                    p_init_esb_compl=[p_init_esb_compl zeros(1,length(p_min_esb_compl)-length(p_init_esb_compl))];
                end
            end
            p_init_esb_compl = [p_best, p_init_esb_compl(11:end)];
            p_min=[0.95*p_best(1),0.9*p_best(2:10) ,p_min_esb_compl(11:end)];
            p_max=[1.05*p_best(1),1.1*p_best(2:10) ,p_max_esb_compl(11:end)];
            for N = 1:length(p_max)
                if app.ParamTable.Data{N,2}
                    p_min(N) = app.ParamTable.Data{N,3};
                    p_max(N) = app.ParamTable.Data{N,3};
                end
            end
            if strcmp(app.used_optimization_algorithm,'SimAnn')
                for n = 1:length(p_init_esb_compl)
                    if p_max(n) - p_min(n) < 0.002
                        if p_init_esb_compl(n) > 0.001
                            p_max(n) = p_init_esb_compl(n) + 0.001;
                            p_min(n) = p_init_esb_compl(n) - 0.001;
                        else
                            p_max(n) = 0.002;
                            p_min(n) = 0;
                        end
                    end
                end
            end
            for n = 1:length(p_min)
                if p_min(n)<0
                    p_min(n) = 0;
                end
            end
            
            %default
            temporary_variable=cell2mat(app.Boundaries_Default{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
            p_max_esb_compl_default=transpose(temporary_variable(:,2));
            p_min_esb_compl_default=transpose(temporary_variable(:,1));
            if length(p_init_esb_compl)~=length(p_min_esb_compl_default)
                if length(p_init_esb_compl)>length(p_min_esb_compl_default)
                    p_init_esb_compl=p_init_esb_compl(1:length(p_min_esb_compl_default));
                elseif length(p_init_esb_compl)<length(p_min_esb_compl_default)
                    p_init_esb_compl=[p_init_esb_compl zeros(1,length(p_min_esb_compl_default)-length(p_init_esb_compl))];
                end
            end
            p_init_esb_compl = [p_best_or, p_init_esb_compl(11:end)];
            p_min_or=[0.95*p_best_or(1),0.9*p_best_or(2:10), p_min_esb_compl_default(11:end)];
            p_max_or=[1.05*p_best_or(1),1.1*p_best_or(2:10), p_max_esb_compl_default(11:end)];
            for N = 1:length(p_max_or)
                if app.ParamTable.Data{N,2}
                    p_min_or(N) = app.ParamTable.Data{N,3};
                    p_max_or(N) = app.ParamTable.Data{N,3};
                end
            end
            if strcmp(app.used_optimization_algorithm,'SimAnn')
                for n = 1:length(p_init_esb_compl)
                    if p_max_or(n) - p_min_or(n) < 0.002
                        if p_init_esb_compl(n) > 0.001
                            p_max_or(n) = p_init_esb_compl(n) + 0.001;
                            p_min_or(n) = p_init_esb_compl(n) - 0.001;
                        else
                            p_max_or(n) = 0.002;
                            p_min_or(n) = 0;
                        end
                    end
                end
            end
            for n = 1:length(p_min_or)
                if p_min_or(n)<0
                    p_min_or(n) = 0;
                end
            end
        end
    case 'C'
        %Betrachte nur kapazitives Verhalten
        index=find(-m_imag>0);
        m_w=m_w(index);
        m_real=m_real(index);
        m_imag=m_imag(index);
        
        %Optimation within the default boundaries
        p_init = DRT_GUI.Fit.Parameter;
        temporary_variable=cell2mat(app.Boundaries_Default{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max_or=transpose(temporary_variable(:,2));
        p_min_or=transpose(temporary_variable(:,1));
        if length(p_init)~=length(p_min_or)
            if length(p_init)>length(p_min_or)
                p_init=p_init(1:length(p_min_or));
            elseif length(p_init)<length(p_min_or)
                p_init=[p_init zeros(1,length(p_min_or)-length(p_init))];
            end
        end
        for n = 1:length(p_min_or)
            if p_min_or(n)<0
                p_min_or(n) = 0;
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init)
                if p_max_or(n) - p_min_or(n) < 0.002
                    if p_init(n) > 0.001
                        p_max_or(n) = p_init(n) + 0.001;
                        p_min_or(n) = p_init(n) - 0.001;
                    else
                        p_max_or(n) = 0.002;
                        p_min_or(n) = 0;
                    end
                end
            end
        end
        [p_best_or,fval_or,exitflag_or,output_or]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min_or, p_max_or ,options, formula,app.used_optimization_algorithm);
        app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1)=fval_or;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1,:)=p_best_or;
        %     index_min = find((p_best_or<=p_min_default*1.001 & p_min_default~=p_max_default & p_min_default~=p_min_model & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Rser') & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'C')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Cser')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Kskin')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Lser')));
        %     index_max = find((p_best_or>=p_max_default*0.999 & p_min_default~=p_max_default & p_max_default~=p_max_model & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Rser') & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'C')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Cser')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Kskin')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Lser')));
        
        %Optimization within the calculated boundaries
        temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max=transpose(temporary_variable(:,2));
        p_min=transpose(temporary_variable(:,1));
        if length(p_init)~=length(p_min)
            if length(p_init)>length(p_min)
                p_init=p_init(1:length(p_min));
            elseif length(p_init)<length(p_min)
                p_init=[p_init zeros(1,length(p_min)-length(p_init))];
            end
        end
        for n = 1:length(p_min)
            if p_min(n)<0
                p_min(n) = 0;
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init)
                if p_max(n) - p_min(n) < 0.002
                    if p_init(n) > 0.001
                        p_max(n) = p_init(n) + 0.001;
                        p_min(n) = p_init(n) - 0.001;
                    else
                        p_max(n) = 0.002;
                        p_min(n) = 0;
                    end
                end
            end
        end
        [p_best,fval,exitflag_or,output_or]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min, p_max ,options, formula,app.used_optimization_algorithm);
        app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2)=fval;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2,:)=p_best;
        %     index_min = find((p_best<=p_min_default*1.001 & p_min_default~=p_max_default & p_min_default~=p_min_model & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Rser') & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'C')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Cser')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Kskin')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Lser')));
        %     index_max = find((p_best>=p_max_default*0.999 & p_min_default~=p_max_default & p_max_default~=p_max_model & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Rser') & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'C')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Cser')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Kskin')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Lser')));
    case 'RC'
        %Betrachte nur kapazitives Verhalten
        index=find(-m_imag>0);
        m_w=m_w(index);
        m_real=m_real(index);
        m_imag=m_imag(index);
        
        %Optimation within the default boundaries
        p_init = DRT_GUI.Fit.Parameter;
        temporary_variable=cell2mat(app.Boundaries_Default{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max_or=transpose(temporary_variable(:,2));
        p_min_or=transpose(temporary_variable(:,1));
        if length(p_init)~=length(p_min_or)
            if length(p_init)>length(p_min_or)
                p_init=p_init(1:length(p_min_or));
            elseif length(p_init)<length(p_min_or)
                p_init=[p_init zeros(1,length(p_min_or)-length(p_init))];
            end
        end
        for n = 1:length(p_min_or)
            if p_min_or(n)<0
                p_min_or(n) = 0;
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init)
                if p_max_or(n) - p_min_or(n) < 0.002
                    if p_init(n) > 0.001
                        p_max_or(n) = p_init(n) + 0.001;
                        p_min_or(n) = p_init(n) - 0.001;
                    else
                        p_max_or(n) = 0.002;
                        p_min_or(n) = 0;
                    end
                end
            end
        end
        [p_best_or,fval_or,exitflag_or,output_or]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min_or, p_max_or ,options, formula,app.used_optimization_algorithm);
        app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1)=fval_or;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1,:)=p_best_or;
        %     index_min = find((p_best_or<=p_min_default*1.001 & p_min_default~=p_max_default & p_min_default~=p_min_model & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Rser') & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'C')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Cser')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Kskin')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Lser')));
        %     index_max = find((p_best_or>=p_max_default*0.999 & p_min_default~=p_max_default & p_max_default~=p_max_model & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Rser') & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'C')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Cser')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Kskin')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Lser')));
        
        %Optimization within the calculated boundaries
        temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max=transpose(temporary_variable(:,2));
        p_min=transpose(temporary_variable(:,1));
        if length(p_init)~=length(p_min)
            if length(p_init)>length(p_min)
                p_init=p_init(1:length(p_min));
            elseif length(p_init)<length(p_min)
                p_init=[p_init zeros(1,length(p_min)-length(p_init))];
            end
        end
        for n = 1:length(p_min)
            if p_min(n)<0
                p_min(n) = 0;
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init)
                if p_max(n) - p_min(n) < 0.002
                    if p_init(n) > 0.001
                        p_max(n) = p_init(n) + 0.001;
                        p_min(n) = p_init(n) - 0.001;
                    else
                        p_max(n) = 0.002;
                        p_min(n) = 0;
                    end
                end
            end
        end
        [p_best,fval,exitflag_or,output_or]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min, p_max ,options, formula,app.used_optimization_algorithm);
        app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2)=fval;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2,:)=p_best;
        %     index_min = find((p_best<=p_min_default*1.001 & p_min_default~=p_max_default & p_min_default~=p_min_model & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Rser') & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'C')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Cser')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Kskin')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Lser')));
        %     index_max = find((p_best>=p_max_default*0.999 & p_min_default~=p_max_default & p_max_default~=p_max_model & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Rser') & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'C')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Cser')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Kskin')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Lser')));
    case '2RC'
        %Betrachte nur kapazitives Verhalten
        index=find(-m_imag>0);
        m_w=m_w(index);
        m_real=m_real(index);
        m_imag=m_imag(index);
        
        %Optimation within the default boundaries
        p_init = DRT_GUI.Fit.Parameter;
        temporary_variable=cell2mat(app.Boundaries_Default{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max_or=transpose(temporary_variable(:,2));
        p_min_or=transpose(temporary_variable(:,1));
        if length(p_init)~=length(p_min_or)
            if length(p_init)>length(p_min_or)
                p_init=p_init(1:length(p_min_or));
            elseif length(p_init)<length(p_min_or)
                p_init=[p_init zeros(1,length(p_min_or)-length(p_init))];
            end
        end
        for n = 1:length(p_min_or)
            if p_min_or(n)<0
                p_min_or(n) = 0;
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init)
                if p_max_or(n) - p_min_or(n) < 0.002
                    if p_init(n) > 0.001
                        p_max_or(n) = p_init(n) + 0.001;
                        p_min_or(n) = p_init(n) - 0.001;
                    else
                        p_max_or(n) = 0.002;
                        p_min_or(n) = 0;
                    end
                end
            end
        end
        [p_best_or,fval_or,exitflag_or,output_or]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min_or, p_max_or ,options, formula,app.used_optimization_algorithm);
        app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1)=fval_or;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1,:)=p_best_or;
        
        %Optimization within the calculated boundaries
        temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max=transpose(temporary_variable(:,2));
        p_min=transpose(temporary_variable(:,1));
        if length(p_init)~=length(p_min)
            if length(p_init)>length(p_min)
                p_init=p_init(1:length(p_min));
            elseif length(p_init)<length(p_min)
                p_init=[p_init zeros(1,length(p_min)-length(p_init))];
            end
        end
        for n = 1:length(p_min)
            if p_min(n)<0
                p_min(n) = 0;
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init)
                if p_max(n) - p_min(n) < 0.002
                    if p_init(n) > 0.001
                        p_max(n) = p_init(n) + 0.001;
                        p_min(n) = p_init(n) - 0.001;
                    else
                        p_max(n) = 0.002;
                        p_min(n) = 0;
                    end
                end
            end
        end
        [p_best,fval,exitflag_or,output_or]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min, p_max ,options, formula,app.used_optimization_algorithm);
        app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2)=fval;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2,:)=p_best;
    case '3RC'
        %Betrachte nur kapazitives Verhalten
        index=find(-m_imag>0);
        m_w=m_w(index);
        m_real=m_real(index);
        m_imag=m_imag(index);
        
        %Optimation within the default boundaries
        p_init = DRT_GUI.Fit.Parameter;
        temporary_variable=cell2mat(app.Boundaries_Default{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max_or=transpose(temporary_variable(:,2));
        p_min_or=transpose(temporary_variable(:,1));
        if length(p_init)~=length(p_min_or)
            if length(p_init)>length(p_min_or)
                p_init=p_init(1:length(p_min_or));
            elseif length(p_init)<length(p_min_or)
                p_init=[p_init zeros(1,length(p_min_or)-length(p_init))];
            end
        end
        for n = 1:length(p_min_or)
            if p_min_or(n)<0
                p_min_or(n) = 0;
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init)
                if p_max_or(n) - p_min_or(n) < 0.002
                    if p_init(n) > 0.001
                        p_max_or(n) = p_init(n) + 0.001;
                        p_min_or(n) = p_init(n) - 0.001;
                    else
                        p_max_or(n) = 0.002;
                        p_min_or(n) = 0;
                    end
                end
            end
        end
        [p_best_or,fval_or,exitflag_or,output_or]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min_or, p_max_or ,options, formula,app.used_optimization_algorithm);
        app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1)=fval_or;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1,:)=p_best_or;
        
        %Optimization within the calculated boundaries
        temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max=transpose(temporary_variable(:,2));
        p_min=transpose(temporary_variable(:,1));
        if length(p_init)~=length(p_min)
            if length(p_init)>length(p_min)
                p_init=p_init(1:length(p_min));
            elseif length(p_init)<length(p_min)
                p_init=[p_init zeros(1,length(p_min)-length(p_init))];
            end
        end
        for n = 1:length(p_min)
            if p_min(n)<0
                p_min(n) = 0;
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init)
                if p_max(n) - p_min(n) < 0.002
                    if p_init(n) > 0.001
                        p_max(n) = p_init(n) + 0.001;
                        p_min(n) = p_init(n) - 0.001;
                    else
                        p_max(n) = 0.002;
                        p_min(n) = 0;
                    end
                end
            end
        end
        [p_best,fval,exitflag_or,output_or]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min, p_max ,options, formula,app.used_optimization_algorithm);
        app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2)=fval;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2,:)=p_best;
    case 'Zarc'
        %Betrachte nur kapazitives Verhalten
        index=find(-m_imag>0);
        m_w=m_w(index);
        m_real=m_real(index);
        m_imag=m_imag(index);
        
        %Optimation within the default boundaries
        p_init = DRT_GUI.Fit.Parameter;
        temporary_variable=cell2mat(app.Boundaries_Default{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max_or=transpose(temporary_variable(:,2));
        p_min_or=transpose(temporary_variable(:,1));
        if length(p_init)~=length(p_min_or)
            if length(p_init)>length(p_min_or)
                p_init=p_init(1:length(p_min_or));
            elseif length(p_init)<length(p_min_or)
                p_init=[p_init zeros(1,length(p_min_or)-length(p_init))];
            end
        end
        for n = 1:length(p_min_or)
            if p_min_or(n)<0
                p_min_or(n) = 0;
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init)
                if p_max_or(n) - p_min_or(n) < 0.002
                    if p_init(n) > 0.001
                        p_max_or(n) = p_init(n) + 0.001;
                        p_min_or(n) = p_init(n) - 0.001;
                    else
                        p_max_or(n) = 0.002;
                        p_min_or(n) = 0;
                    end
                end
            end
        end
        [p_best_or,fval_or,exitflag_or,output_or]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min_or, p_max_or ,options, formula,app.used_optimization_algorithm);
        app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1)=fval_or;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1,:)=p_best_or;
        %     index_min = find((p_best_or<=p_min_default*1.001 & p_min_default~=p_max_default & p_min_default~=p_min_model & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Rser') & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'C')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Cser')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Kskin')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Lser')));
        %     index_max = find((p_best_or>=p_max_default*0.999 & p_min_default~=p_max_default & p_max_default~=p_max_model & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Rser') & ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'C')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Cser')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Kskin')& ~strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),'Lser')));
        
        %Optimization within the calculated boundaries
        temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max=transpose(temporary_variable(:,2));
        p_min=transpose(temporary_variable(:,1));
        if length(p_init)~=length(p_min)
            if length(p_init)>length(p_min)
                p_init=p_init(1:length(p_min));
            elseif length(p_init)<length(p_min)
                p_init=[p_init zeros(1,length(p_min)-length(p_init))];
            end
        end
        for n = 1:length(p_min)
            if p_min(n)<0
                p_min(n) = 0;
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init)
                if p_max(n) - p_min(n) < 0.002
                    if p_init(n) > 0.001
                        p_max(n) = p_init(n) + 0.001;
                        p_min(n) = p_init(n) - 0.001;
                    else
                        p_max(n) = 0.002;
                        p_min(n) = 0;
                    end
                end
            end
        end
        [p_best,fval,exitflag_or,output_or]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min, p_max ,options, formula,app.used_optimization_algorithm);
        app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2)=fval;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2,:)=p_best;
        
    otherwise
        index_cabling = find(-m_imag<0);
        index_esb = find(-m_imag>0);
%         if isempty(index_cabling)
%             index_cabling = find(DRT_GUI.Messdaten.frequenz>=1000);
%             index_esb  = find(DRT_GUI.Messdaten.frequenz<1000);
%         end
        
        m_w_hf = [];
        m_w_mf = [];
        m_w_lf = [];
        
        %Seperate the induktiv part resembling the cabling
        m_w_hf = m_w(index_cabling);
        m_real_hf = m_real(index_cabling);
        m_imag_hf = m_imag(index_cabling);
        
        %Seperate the capacitiv part resembling the rest hte actual
        %battery
        m_w_esb = m_w(index_esb);
        m_real_esb = m_real(index_esb);
        m_imag_esb = m_imag(index_esb);
        
        %Seperate this part into hf,mf and lf
        [pks_u,locs_u] = findpeaks(m_imag_esb);
        if length(locs_u)> 1
            [minimum_value, step_index_index] = max(pks_u);
            step_index = locs_u(step_index_index) + 2;
            %                 step_index = locs_u(end) + 7;
        elseif length(locs_u) < 1
            step_index = findchangepts(diff(m_real_esb));
        else
            step_index = locs_u(end) + 2;
        end
        
        %Low frequency part of EIS-spectrum
        m_real_lf = m_real_esb(step_index-1:end);
        m_imag_lf = m_imag_esb(step_index-1:end);
        m_w_lf = m_w_esb(step_index-1:end);
        
        m_w_mf = m_w_esb(1:step_index-2);
        m_real_mf = m_real_esb(1:step_index-2);
        m_imag_mf = m_imag_esb(1:step_index-2);
        
        m_w_mf = [m_w_hf , m_w_mf];
        m_real_mf = [m_real_hf , m_real_mf];
        m_imag_mf = [m_imag_hf , m_imag_mf];
        
        %extract formula for hf part
        relevant_hf_index = strfind(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_HF,'+1.*(');
        irrelevant_hf_index = strfind(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_HF,'+0.*(');
        first_hf_index = strcmp(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_HF(1),'1');
        
        combined_hf = [relevant_hf_index,irrelevant_hf_index];
        combined_values_hf = [ones(1,length(relevant_hf_index)),zeros(1,length(irrelevant_hf_index))];
        
        if first_hf_index
            combined_hf = [1 , combined_hf];
            combined_values_hf = [1 , combined_values_hf];
        end
        
        [sorted_hf , sorted_hf_index] = sort(combined_hf);
        sorted_combined_hf_values = combined_values_hf(sorted_hf_index);
        sorted_hf = [sorted_hf,length(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_HF)];
        
        formula_hf = [];
        for n = 1:length(sorted_combined_hf_values)
            if sorted_combined_hf_values(n) == 1
                if sorted_hf(n) == 1
                    formula_hf = [formula_hf, DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_HF(sorted_hf(n):sorted_hf(n+1))];
                else
                    formula_hf = [formula_hf, DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_HF(sorted_hf(n)+1:sorted_hf(n+1))];
                end
            end
        end
        
        %extract formula for mf part
        relevant_mf_index = strfind(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_MF,'+1.*(');
        irrelevant_mf_index = strfind(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_MF,'+0.*(');
        first_mf_index = strcmp(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_MF(1),'1');
        
        combined_mf = [relevant_mf_index,irrelevant_mf_index];
        combined_values_mf = [ones(1,length(relevant_mf_index)),zeros(1,length(irrelevant_mf_index))];
        
        
        [sorted_mf , sorted_mf_index] = sort(combined_mf);
        sorted_combined_mf_values = combined_values_mf(sorted_mf_index);
        sorted_mf = [sorted_mf,length(DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_MF)];
        
        formula_mf = [];
        for n = 1:length(sorted_combined_mf_values)
            if sorted_combined_mf_values(n) == 1
                if sorted_mf(n) == 1
                    formula_mf = [formula_mf, DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_MF(sorted_mf(n):sorted_mf(n+1))];
                else
                    formula_mf = [formula_mf, DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell_MF(sorted_mf(n)+1:sorted_mf(n+1))];
                end
            end
        end
        % Include Rser into MF part
        formula_mf = strcat('1.*(p(1))+',formula_mf);
        if strcmp(formula_mf(end),'+')
            formula_mf = formula_mf(1:end-1);
        end
        if isempty(formula_hf)
            formula_hf = '+';
        end
        if strcmp(formula_hf(end),'+')
            formula_hf = formula_hf(1:end-1);
        end
        
        %seperate HF part of the formula
        mf_param_index(1) = 1;
        hf_param_index = [];
        mf_param_index_counter = 2;
        hf_param_index_counter = 1;
        for n1 = 1:length(DRT_GUI.Fit.aktuell_Modell.ModellCell{8})
            %find all mf parameters
            if ~strcmp( DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.Zfun_MF,'0')
                for n2 = 1:length(DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.ParameterIndexes)
                    if ~isempty(strfind( DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.Zfun_MF,app.ParamTable.Data{DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.ParameterIndexes(n2),1}))
                        if isempty(find(mf_param_index == DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.ParameterIndexes(n2)))
                            mf_param_index(mf_param_index_counter) = DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.ParameterIndexes(n2);
                            mf_param_index_counter = mf_param_index_counter + 1;
                        end
                    end
                end
            end
            %find all hf parameters
            if ~strcmp( DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.Zfun_HF,'0')
                for n2 = 1:length(DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.ParameterIndexes)
                    if ~isempty(strfind( DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.Zfun_HF,app.ParamTable.Data{DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.ParameterIndexes(n2),1}))
                        if isempty(find(hf_param_index == DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.ParameterIndexes(n2)))
                            hf_param_index(hf_param_index_counter) = DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n1}.ParameterIndexes(n2);
                            hf_param_index_counter = hf_param_index_counter + 1;
                        end
                    end
                end
            end
        end
        
        
        p_init_hf = p_init(1:max(hf_param_index));
        p_init_esb = p_init(1:mf_param_index(end));
        p_init_esb_compl = p_init;
        p_best_cable = [];
        if ~isempty(formula_hf)
            temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
            p_max_cabel=transpose(temporary_variable(1:max(hf_param_index),2));
            p_min_cabel=transpose(temporary_variable(1:max(hf_param_index),1));
            if strcmp(app.used_optimization_algorithm,'SimAnn')
                for n = 1:length(p_init_hf)
                    if p_max_cabel(n) - p_min_cabel(n) < 0.002
                        if p_init_hf(n) > 0.001
                            p_max_cabel(n) = p_init_hf(n) + 0.001;
                            p_min_cabel(n) = p_init_hf(n) - 0.001;
                        else
                            p_max_cabel(n) = 0.002;
                            p_min_cabel(n) = 0;
                        end
                    end
                end
            end
            for n = 1:length(p_min_cabel)
                if p_min_cabel(n)<0
                    p_min_cabel(n) = 0;
                end
            end
            [p_best_cable,fval_cabel,exitflag_cabel,output_cabel]=function_fit_easyfit2(app,m_w_hf,[m_real_hf, m_imag_hf],p_init_hf,@function_model_all_types2, p_min_cabel, p_max_cabel ,options, formula_hf,app.used_optimization_algorithm);
            app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1,hf_param_index)=p_best_cable;
            app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2,hf_param_index)=p_best_cable;
        end
        
        %Cell parameter fit with mf boundaries
        p_best_or = [];
        p_best = [];
        hf_empty = 0;
        if isempty(p_best_cable)
            hf_empty = 1;
        end
        if ~strcmp(formula_mf,'1.*(p(1))')
            temporary_variable=cell2mat(app.Boundaries_Default{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
            p_max_default=transpose(temporary_variable(1:mf_param_index(end),2));
            p_min_default=transpose(temporary_variable(1:mf_param_index(end),1));
            if length(p_init_esb)~=length(p_min_default)
                if length(p_init_esb)>length(p_min_default)
                    p_init_esb=p_init_esb(1:length(p_min_default));
                elseif length(p_init)<length(p_min_default)
                    p_init_esb=[p_init_esb zeros(1,length(p_min_default)-length(p_init_esb))];
                end
            end
            if hf_empty
                p_best_cable = p_init_esb(1:max(hf_param_index));
            end
            p_init_esb(hf_param_index) = p_best_cable(hf_param_index);
            p_min_default(hf_param_index)=0.95*p_best_cable(hf_param_index);
            p_max_default(hf_param_index)=1.05*p_best_cable(hf_param_index);
            if strcmp(app.used_optimization_algorithm,'SimAnn')
                for n = 1:length(p_init_esb)
                    if p_max_default(n) - p_min_default(n) < 0.002
                        if p_init_esb(n) > 0.001
                            p_max_default(n) = p_init_esb(n) + 0.001;
                            p_min_default(n) = p_init_esb(n) - 0.001;
                        else
                            p_max_default(n) = 0.002;
                            p_min_default(n) = 0;
                        end
                    end
                end
            end
            for n = 1:length(p_min_default)
                if p_min_default(n)<0
                    p_min_default(n) = 0;
                end
            end
            [p_best_or,fval_or,exitflag_or,output_or]=function_fit_easyfit2(app,m_w_mf,[m_real_mf, m_imag_mf],p_init_esb,@function_model_all_types2, p_min_default, p_max_default ,options, formula_mf,app.used_optimization_algorithm);
            app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1)=fval_or;
            app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1,1:mf_param_index(end))=p_best_or;
            
            %Cell parameter fit with calculated boundaries
            temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
            p_max_esb=transpose(temporary_variable(1:mf_param_index(end),2));
            p_min_esb=transpose(temporary_variable(1:mf_param_index(end),1));
            if length(p_init_esb)~=length(p_min_esb)
                if length(p_init_esb)>length(p_min_esb)
                    p_init_esb=p_init_esb(1:length(p_min_esb));
                elseif length(p_init_esb)<length(p_min_esb)
                    p_init_esb=[p_init_esb zeros(1,length(p_min_esb)-length(p_init_esb))];
                end
            end
            if hf_empty
                p_best_cable = p_init_esb(1:max(hf_param_index));
            end
            p_init_esb(hf_param_index) = p_best_cable(hf_param_index);
            p_min_esb(hf_param_index)=0.95* p_best_cable(hf_param_index);
            p_max_esb(hf_param_index)=1.05* p_best_cable(hf_param_index);
            if strcmp(app.used_optimization_algorithm,'SimAnn')
                for n = 1:length(p_init_esb)
                    if p_max_esb(n) - p_min_esb(n) < 0.002
                        if p_init_esb(n) > 0.001
                            p_max_esb(n) = p_init_esb(n) + 0.001;
                            p_min_esb(n) = p_init_esb(n) - 0.001;
                        else
                            p_max_esb(n) = 0.002;
                            p_min_esb(n) = 0;
                        end
                    end
                end
            end
            for n = 1:length(p_min_esb)
                if p_min_esb(n)<0
                    p_min_esb(n) = 0;
                end
            end
            [p_best,fval,exitflag_or,output_or]=function_fit_easyfit2(app,m_w_mf,[m_real_mf, m_imag_mf],p_init_esb,@function_model_all_types2, p_min_esb, p_max_esb ,options, formula_mf,app.used_optimization_algorithm);
            app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2)=fval;
            app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2,1:mf_param_index(end))=p_best;
        end
        
        %Fix fittet Parameters and fit last two RC-Elements on slow
        %semi-circle
        
        %for default boundaries
        temporary_variable=cell2mat(app.Boundaries_Default{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_max_esb_compl_default=transpose(temporary_variable(:,2));
        p_min_esb_compl_default=transpose(temporary_variable(:,1));
        if length(p_init_esb_compl)~=length(p_min_esb_compl_default)
            if length(p_init_esb_compl)>length(p_min_esb_compl_default)
                p_init_esb_compl=p_init_esb_compl(1:length(p_min_esb_compl_default));
            elseif length(p_init_esb_compl)<length(p_min_esb_compl_default)
                p_init_esb_compl=[p_init_esb_compl zeros(1,length(p_min_esb_compl_default)-length(p_init_esb_compl))];
            end
        end
        if isempty(p_best_or)
            p_best_or = p_init_esb_compl(1:max([max(mf_param_index), max(hf_param_index)]));
        end
        p_init_esb_compl(mf_param_index) = p_best_or(mf_param_index);
        p_init_esb_compl(hf_param_index) = p_best_or(hf_param_index);
        p_min_or = zeros(1,length(p_init_esb_compl));
        p_min_or(hf_param_index) = 0.95*p_best_or(hf_param_index);
        p_min_or(mf_param_index) = 0.9*p_best_or(mf_param_index);
        p_min_or(mf_param_index(end)+1:end) = p_min_esb_compl_default(mf_param_index(end)+1:end);
        p_max_or = zeros(1,length(p_init_esb_compl));
        p_max_or(hf_param_index) = 1.05*p_best_or(hf_param_index);
        p_max_or(mf_param_index) = 1.1*p_best_or(mf_param_index);
        p_max_or(mf_param_index(end)+1:end) = p_max_esb_compl_default(mf_param_index(end)+1:end);
        for N = 1:length(p_max_or)
            if app.ParamTable.Data{N,2}
                p_min_or(N) = app.ParamTable.Data{N,3};
                p_max_or(N) = app.ParamTable.Data{N,3};
                p_init_esb_compl(N) = app.ParamTable.Data{N,3};
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init_esb_compl)
                if p_max_or(n) - p_min_or(n) < 0.002
                    if p_init_esb_compl(n) > 0.001
                        p_max_or(n) = p_init_esb_compl(n) + 0.001;
                        p_min_or(n) = p_init_esb_compl(n) - 0.001;
                    else
                        p_max_or(n) = 0.002;
                        p_min_or(n) = 0;
                    end
                end
            end
        end
        for n = 1:length(p_min_or)
            if p_min_or(n)<0
                p_min_or(n) = 0;
            end
        end
        [p_best_or_compl_default,fval_or_compl_default,exitflag_or_compl_default,output_or_compl_default]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init_esb_compl,@function_model_all_types2, p_min_or, p_max_or ,options, formula,app.used_optimization_algorithm);
        app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1)=fval_or_compl_default;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(1,:)=p_best_or_compl_default;
        fval_or = fval_or_compl_default;
        p_best_or = p_best_or_compl_default;
        
        %for calculated boundaries
        temporary_variable=cell2mat(app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index});
        p_init_esb_compl = p_init;
        p_max_esb_compl=transpose(temporary_variable(:,2));
        p_min_esb_compl=transpose(temporary_variable(:,1));
        if length(p_init_esb_compl)~=length(p_min_esb_compl)
            if length(p_init_esb_compl)>length(p_min_esb_compl)
                p_init_esb_compl=p_init_esb_compl(1:length(p_min_esb_compl));
            elseif length(p_init_esb_compl)<length(p_min_esb_compl)
                p_init_esb_compl=[p_init_esb_compl zeros(1,length(p_min_esb_compl)-length(p_init_esb_compl))];
            end
        end
        if isempty(p_best)
            p_best = p_init_esb_compl(1:max([max(mf_param_index), max(hf_param_index)]));
        end
        p_init_esb_compl(mf_param_index) = p_best(mf_param_index);
        p_init_esb_compl(hf_param_index) = p_best(hf_param_index);
        p_min = zeros(1,length(p_init_esb_compl));
        p_min(hf_param_index) = 0.95*p_best(hf_param_index);
        p_min(mf_param_index) = 0.9*p_best(mf_param_index);
        p_min(mf_param_index(end)+1:end) = p_min_esb_compl(mf_param_index(end)+1:end);
        p_max = zeros(1,length(p_init_esb_compl));
        p_max(hf_param_index) = 1.05*p_best(hf_param_index);
        p_max(mf_param_index) = 1.1*p_best(mf_param_index);
        p_max(mf_param_index(end)+1:end) = p_max_esb_compl(mf_param_index(end)+1:end);
        for N = 1:length(p_max)
            if app.ParamTable.Data{N,2}
                p_min(N) = app.ParamTable.Data{N,3};
                p_max(N) = app.ParamTable.Data{N,3};
                p_init_esb_compl(N) = app.ParamTable.Data{N,3};
            end
        end
        if strcmp(app.used_optimization_algorithm,'SimAnn')
            for n = 1:length(p_init_esb_compl)
                if p_max(n) - p_min(n) < 0.002
                    if p_init_esb_compl(n) > 0.001
                        p_max(n) = p_init_esb_compl(n) + 0.001;
                        p_min(n) = p_init_esb_compl(n) - 0.001;
                    else
                        p_max(n) = 0.002;
                        p_min(n) = 0;
                    end
                end
            end
        end
        for n = 1:length(p_min)
            if p_min(n)<0
                p_min(n) = 0;
            end
        end
        [p_best_or_compl,fval_or_compl,exitflag_or_compl,output_or_compl]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init_esb_compl,@function_model_all_types2, p_min, p_max ,options, formula,app.used_optimization_algorithm);
        app.RMSE{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2)=fval_or_compl;
        app.parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(2,:)=p_best_or_compl;
        fval = fval_or_compl;
        p_best = p_best_or_compl;
end

%adjust the considered RMSE for default boundaries to make up for
%potential incontinuity
if ~isempty(p_min) || ~isempty(p_max) || ~isempty(p_min_or) || ~isempty(p_max_or)
    adjusted_RMSE_default = fval_or * app.PenaltyEditField.Value;
    %compare values to decide which Optimum fits better
    if adjusted_RMSE_default < fval
        app.optimal_parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}=p_best_or;
        drt_boundaries_min = p_min_or;
        drt_boundaries_max = p_max_or;
    elseif adjusted_RMSE_default >= fval
        app.optimal_parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}=p_best;
        drt_boundaries_min = p_min;
        drt_boundaries_max = p_max;
    end
elseif isempty(p_min) || isempty(p_max) || ~isempty(p_min_or) || ~isempty(p_max_or)
    app.optimal_parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}=p_best_or;
elseif ~isempty(p_min) || ~isempty(p_max) || isempty(p_min_or) || isempty(p_max_or)
    app.optimal_parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}=p_best;
end

if app.start_tupel_optimized
    TableCell1 = TableCell;
    DRT_GUI.Fit.Parameter = app.optimal_parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index};
    % die Fittwerte(p-best) in Feld zeigen
    for P_i = 1:length(p_best)
        TableCell(P_i).Value = app.optimal_parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(P_i);
    end
    TableCell = struct2cell(TableCell)';
    if app.GUI
        set(app.ParamTable,'Data',TableCell)
    else
        app.ParamTable.Data = TableCell;
    end
    
    app.GueltigeMessungCheck.Value = 1;
    app = fcn_GueltigeMessungCheck(app, event);
    %fcn_PlotFittedParametersButton(app, event)
    DRT_GUI.Fit.Parameter_min = cell2mat(DRT_GUI.Fit.aktuell_Modell.ModellCell{5});
    DRT_GUI.Fit.Parameter_max = cell2mat(DRT_GUI.Fit.aktuell_Modell.ModellCell{6});
    app = fcn_SpeichernButton(app, event);
else
    if adjusted_RMSE_default < fval
        app.start_tupel_optimized_results{app.start_tupel_optimized_idx}{1,1} = adjusted_RMSE_default;
    elseif adjusted_RMSE_default >= fval
        app.start_tupel_optimized_results{app.start_tupel_optimized_idx}{1,1} = fval;
    end
    app.start_tupel_optimized_results{app.start_tupel_optimized_idx}{1,2} = app.optimal_parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index};
    if adjusted_RMSE_default < fval
        app.start_tupel_optimized_results{app.start_tupel_optimized_idx}{1,3} = app.Boundaries_Default{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index};
    elseif adjusted_RMSE_default >= fval
        app.start_tupel_optimized_results{app.start_tupel_optimized_idx}{1,3} = app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index};
    end
    app.start_tupel_optimized_idx = app.start_tupel_optimized_idx + 1;
end
message_string = ['EIS: Fitting for ' DRT_GUI.Testparameter.Zustand ', ' num2str(DRT_GUI.Testparameter.Temperatur) ' �C and ' num2str(DRT_GUI.Testparameter.SOC) '% SOC finished'];
disp(message_string)
end

