function app = fcn_CancelCheck(app)
if app.GUI
    pause(1)
    get(app.CancelButton,'Value');
    if app.CancelButton.Value
        app.CancelButton.Value = 0;
        app.BusyLabel.Visible = 'off';
        app.GlobalFitLamp.Color = 'green';
        return
    end
end
end

