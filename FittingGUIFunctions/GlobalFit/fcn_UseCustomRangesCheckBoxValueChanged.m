function app = fcn_UseCustomRangesCheckBoxValueChanged(app, event)            
if app.UseCustomRangesCheckBox.Value
    app.CustomRangesTable.Enable = 'on';
    app = fcn_ZeitFitConfigurationSelectionChanged(app, event);
else
    app.CustomRangesTable.Enable = 'off';
end
end

