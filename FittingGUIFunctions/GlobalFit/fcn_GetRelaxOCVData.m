function [OCV_Data,vor_or_nach_relax,nach,vor]=fcn_GetRelaxOCVData(app,event,sorted_temperature_array,temperature_index_rearrangement_array,sorted_SOC_array,SOC_index_rearrangement_array)
    %get OCV-Data 
    vor = 0;
    nach = 0;
    try
        for t=1:length(sorted_temperature_array)
            for s=1:length(sorted_SOC_array{t})
                index1=temperature_index_rearrangement_array(t);
                if~strcmp(app.startingtemperatureDropDown.Value,app.startingtemperatureDropDown.Items{1+index1})
                    app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1+index1};
                    app.TemperaturPopup.Value=app.startingtemperatureDropDown.Value;
                    fcn_TemperaturPopup(app, event);
                end

                index2=SOC_index_rearrangement_array{t}(s);
                if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                    app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                    app.SOCPopup.Value=app.startingSOCDropDown.Value;
                    fcn_SOCPopup(app, event);
                end

                global DRT_GUI 
                try
                    if isfield(DRT_GUI.Messdaten,'pulse')
                        vor_or_nach_relax{t}(s)=DRT_GUI.Messdaten.pulse.spannung(end);
                        OCV_Data{t}(s)=DRT_GUI.Messdaten.relax.spannung(1);                        
                        vor = 1;
                    elseif isfield(DRT_GUI.Messdaten,'vor_relax')
                        vor_or_nach_relax{t}(s)=DRT_GUI.Messdaten.vor_relax.spannung(end);
                        OCV_Data{t}(s)=DRT_GUI.Messdaten.relax.spannung(1);                        
                        vor = 1;
                    elseif isfield(DRT_GUI.Messdaten,'nach_relax')
                        vor_or_nach_relax{t}(s)=DRT_GUI.Messdaten.nach_relax.spannung(1);
                        OCV_Data{t}(s)=DRT_GUI.Messdaten.relax.spannung(end);
                        nach = 1;
                    end
                catch
                    OCV_Data{t}(s)=0;
                    vor_or_nach_relax{t}(s)=0;
                end
            end
        end
    catch
        sorted_temperature_array(t)
        sorted_SOC_array{t}(s)
        msgbox('Something went wrong')
    end
end

