function app = fcn_GlobalTDFFit(app,event,hyst,crate,temp,SOC,sorted_temperature_array,sorted_SOC_array,sorted_hysteresis_state_array,sorted_crate_array)
global DRT_GUI;
if app.GUI
    set(app.ZeitbereichsfittingParameterAuswahlTable,'Data',app.tdf_esb_element)
else
    app.ZeitbereichsfittingParameterAuswahlTable.Data = app.tdf_esb_element;
end
app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
app = fcn_ModellAuswahlPopup(app);
app = fcn_BatterieNamePopup(app);
if isempty(DRT_GUI) || isempty(cell2mat(strfind(fieldnames(DRT_GUI),'Messdaten')))
   return
end
formula = DRT_GUI.Fit.aktuell_Modell.Rechnen_Modell;
if  isempty(cell2mat(strfind(fieldnames(DRT_GUI.Messdaten),'relax_fft'))) || isempty(DRT_GUI.Messdaten.relax_fft)
    m_w = DRT_GUI.Messdaten.omega(DRT_GUI.Messdaten.aktiv==1);
    m_real = DRT_GUI.Messdaten.Zreal(DRT_GUI.Messdaten.aktiv==1);
    m_imag = DRT_GUI.Messdaten.Zimg(DRT_GUI.Messdaten.aktiv==1);
else
    m_w = [DRT_GUI.Messdaten.omega(DRT_GUI.Messdaten.aktiv==1) ; DRT_GUI.Messdaten.relax_fft.omega(DRT_GUI.Messdaten.relax_fft.aktiv==1)] ; %selbst
    m_real = [DRT_GUI.Messdaten.Zreal(DRT_GUI.Messdaten.aktiv==1); DRT_GUI.Messdaten.relax_fft.Zreal(DRT_GUI.Messdaten.relax_fft.aktiv==1)+DRT_GUI.Messdaten.relax_fft.Zreal_korrektur];%selbst
    m_imag = [DRT_GUI.Messdaten.Zimg(DRT_GUI.Messdaten.aktiv==1); DRT_GUI.Messdaten.relax_fft.Zimg(DRT_GUI.Messdaten.relax_fft.aktiv==1)];%selbst
end
if app.GUI
    TableCell = get(app.ParamTable,'Data');
else
    TableCell = app.ParamTable.Data;
end
if ~isempty(TableCell)
    TableCell = cell2struct(TableCell',{'Name','Fix','Value','Min','Max','Lim'});
end

% die initialisierte Werte aus Feld bekommen
DRT_GUI.Fit.ParFix = zeros(size(DRT_GUI.Fit.Parameter));
for Par_init_i = 1:length(DRT_GUI.Fit.Parameter)
    if ~isempty(TableCell(Par_init_i).Value)
        DRT_GUI.Fit.Parameter(Par_init_i) = TableCell(Par_init_i).Value;
    else
        DRT_GUI.Fit.Parameter(Par_init_i) = 0;
        TableCell(Par_init_i).Value = 0;
    end
    if ~isempty(TableCell(Par_init_i).Min) && ~TableCell(Par_init_i).Fix
        p_min(Par_init_i)= TableCell(Par_init_i).Min;
    elseif TableCell(Par_init_i).Fix
        DRT_GUI.Fit.ParFix(Par_init_i) = 1 ;
        p_min(Par_init_i) = TableCell(Par_init_i).Value;
    else
        p_min(Par_init_i) = 0;
        TableCell(Par_init_i).Min = 0 ;
    end
    if ~isempty(TableCell(Par_init_i).Min)
        DRT_GUI.Fit.Parameter_min(Par_init_i) = TableCell(Par_init_i).Min;
    else
        DRT_GUI.Fit.Parameter_min(Par_init_i) = 0;
    end
    if ~isempty(TableCell(Par_init_i).Max) && ~TableCell(Par_init_i).Fix
        p_max(Par_init_i)= TableCell(Par_init_i).Max;
    elseif TableCell(Par_init_i).Fix
        p_max(Par_init_i) = TableCell(Par_init_i).Value;
    else
        p_max(Par_init_i) = inf;
        TableCell(Par_init_i).Max = Inf;
    end
    if ~isempty(TableCell(Par_init_i).Max)
        DRT_GUI.Fit.Parameter_max(Par_init_i) = TableCell(Par_init_i).Max;
    else
        DRT_GUI.Fit.Parameter_max(Par_init_i) = inf;
    end
end

p_best = app.optimal_parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index};
%anschlie�ender Zeitbereichsfit f�r die niederfrequenten
%Anteile dargestellt durch eine Warburg-Impedanz
%ReFit f�r R_DP,Tau_DP, R_slow, Tau_slow

%Lade Daten in Zeitbereichsfitting-Tabelle  
app = fcn_ZeitbereichsFittingButton(app, event);
%Einstellungen in der Tabelle
DataMatrix = app.TDF_ImplementierungsTable.Data;
for n = 1:length(app.ZeitbereichsfittingParameterAuswahlTable.Data(:,2))
    if app.ZeitbereichsfittingParameterAuswahlTable.Data{n,3} ==  true 
        if ~strcmp(DataMatrix{n,2},'Re-Fit')
            DataMatrix(n,2) = {'Re-Fit'};
            event1.Indices = [n 2];
            event1.DisplayIndices = [n 2];
            event1.PreviousData = DataMatrix{n,2};
            event1.EditData = 'Re-Fit';
            event1.NewData = 'Re-Fit';
            event1.Error = [];
            event1.Source = app.TDF_ImplementierungsTable;
            event1.EventName = 'CellEdit';
            event2 = event1;
            event2.EventName = 'CellSelection';
            if app.GUI
                set(app.TDF_ImplementierungsTable,'Data',DataMatrix); 
            else
               app.TDF_ImplementierungsTable.Data = DataMatrix; 
            end
            app = fcn_TDF_ImplementierungsTable_CellSelection(app,event1);
            app = fcn_TDF_ImplementierungsTable_CellEdit(app,event1);
        end
    else
        %DataMatrix(n,2) = {{}};
        if ~isempty(DataMatrix{n,2})
            DataMatrix(n,2) = {''};
            event1.Indices = [n 2];
            event1.DisplayIndices = [n 2];
            event1.PreviousData = DataMatrix{n,2};
            event1.EditData = ' ';
            event1.NewData = ' ';
            event1.Error = [];
            event1.Source = app.TDF_ImplementierungsTable;
            event1.EventName = 'CellEdit';
            event2 = event1;
            event2.EventName = 'CellSelection';
            if app.GUI
               set(app.TDF_ImplementierungsTable,'Data',DataMatrix); 
            else
               app.TDF_ImplementierungsTable.Data = DataMatrix; 
            end
            app = fcn_TDF_ImplementierungsTable_CellSelection(app,event1);
            app = fcn_TDF_ImplementierungsTable_CellEdit(app,event1) ;           
        end
    end
end            
% app.TDF_Relaxation_complete_CheckBox.Value = true;
% app.TDF_stromloseMesspunkteCheckbox.Value = true;
%Grenzen einstellen
DataMatrix_RelaxData = app.TDF_RelaxTable.Data;
if app.UseCustomRangesCheckBox.Value
	for n1 = 5:length(app.TDF_RelaxTable.Data(:,1))
        for n2 = 1:length(app.CustomRangesTable.Data(:,1))
            if strcmp(app.TDF_RelaxTable.Data{n1,1}(7:end),app.CustomRangesTable.Data{n2,1})
                %minimum
                DataMatrix_RelaxData(n1,4) = app.CustomRangesTable.Data(n2,2);
                %maximum
                DataMatrix_RelaxData(n1,5) = app.CustomRangesTable.Data(n2,3);  
                %Fixed
                DataMatrix_RelaxData(n1,3) = {[false]};
                continue
            end
        end
    end
else
    for n1 = 5:length(app.TDF_RelaxTable.Data(:,1))
        %minimum
        DataMatrix_RelaxData(n1,4) = {0};
        %maximum
        DataMatrix_RelaxData(n1,5) = {Inf};  
        %Fixed
        DataMatrix_RelaxData(n1,3) = {[false]};
    end
end

for n1 = 5:length(app.TDF_RelaxTable.Data(:,1))
    TDF_index(n1- 4) = n1 - 4;    
end
if app.GUI
   set(app.TDF_RelaxTable,'Data',DataMatrix_RelaxData)
else
   app.TDF_RelaxTable.Data = DataMatrix_RelaxData; 
end
app = fcn_TDF_RelaxTable_CellEdit(app,event);
if app.ZeitbereichsgrenzextrapolationCheckBox.Value && ~isempty(TDF_index)
    app = fcn_Adapt_TDF_Boundaries(app,TDF_index,hyst,crate,temp,SOC,sorted_temperature_array,sorted_SOC_array,sorted_hysteresis_state_array,sorted_crate_array);
    app = fcn_TDF_RelaxTable_CellEdit(app,event);
end
if app.tdf_postprocessing_started
    for n = 1:length(app.TDF_Boundaries{1,1})
        DataMatrix_RelaxData(n+4,4) = app.TDF_Boundaries{app.current_hyst_index}{app.current_crate_index}{temp}{SOC}(n,1);
        DataMatrix_RelaxData(n+4,5) = app.TDF_Boundaries{app.current_hyst_index}{app.current_crate_index}{temp}{SOC}(n,2);
        if DataMatrix_RelaxData{n+4,2} > DataMatrix_RelaxData{n+4,5}|| DataMatrix_RelaxData{n+4,2} < DataMatrix_RelaxData{n+4,4}
            DataMatrix_RelaxData{n+4,2} = 0.5*(DataMatrix_RelaxData{n+4,5} + DataMatrix_RelaxData{n+4,4});
        end
    end
    if app.GUI
       set(app.TDF_RelaxTable,'Data',DataMatrix_RelaxData)
    else
       app.TDF_RelaxTable.Data = DataMatrix_RelaxData; 
    end      
end
% if strcmp(app.GlobalUsedModell,'LiIon4')
%     fcn_TDF_OCVFitButton_LiIon4(app,event)                
% else
app = fcn_TDF_OCVFitButton(app,event);
% end
app = fcn_TDF_SpeichernButton(app,event);

for n1 = 5:length(app.TDF_RelaxTable.Data(:,1))
    app.tdf_optimal_parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(n1-4) = app.TDF_RelaxTable.Data{n1,2};
end
end