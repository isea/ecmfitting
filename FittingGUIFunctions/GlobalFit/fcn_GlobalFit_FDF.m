function app = fcn_GlobalFit_FDF(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
                               sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array )
global DRT_GUI
%hyst down
for hyst = app.current_hyst_index:-1:1
    if app.HysteresisFlag
        index=hysteresis_state_index_rearrangement_array(hyst);
        app.ZustandPopup_2.Value=app.ZustandPopup_2.Items{1+index};
        app = fcn_ZustandPopup(app, event);
        app.current_hyst_index = hyst;
    end
    app.current_crate_index = Crate_index(hyst);
    %hyst down, crate down
    for crate = app.current_crate_index:-1:1
        if app.CRateFlag
            index=crate_index_rearrangement_array{hyst}(crate);
            app.ZustandPopup.Value=app.ZustandPopup.Items{1+index};
            app = fcn_ZustandPopup(app, event);
            app.current_crate_index = crate;
        end
        app.current_temp_index = temp_index{hyst}(crate);
        %hyst down, crate down, temp down
        for temp=app.current_temp_index:-1:1
            %Lade richtige Daten bei Wechsel der Temperatur
            index1=temperature_index_rearrangement_array{hyst}{crate}(temp);
            if~strcmp(app.startingtemperatureDropDown.Value,app.startingtemperatureDropDown.Items{1+index1})
                app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1+index1};
                app.TemperaturPopup.Value=app.startingtemperatureDropDown.Value;
                app = fcn_TemperaturPopup(app, event);
            end
            app.current_temp_index = temp;
            app.current_SOC_index= SOC_index{hyst}{crate}(temp);
            %hyst down, crate down, temp down, SOC down
            for SOC=app.current_SOC_index:-1:1
                %load SOC
                index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                    app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                    app.SOCPopup.Value=app.startingSOCDropDown.Value;
                    app = fcn_SOCPopup(app, event);
                    if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                        app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                        app = fcn_ModellAuswahlPopup(app);
                        app = fcn_BatterieNamePopup(app);
                    end
                end
                app.current_SOC_index=SOC;
                app = fcn_adapt_boundaries(app,hyst,crate,temp,SOC,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                if app.start_tupel_optimized
                    app = fcn_GlobalSingleFit(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);                    
                else
                    short_holder = app.used_optimization_algorithm;
                    app.used_optimization_algorithm = [];
                    app = fcn_GlobalSingleFit(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                    app.used_optimization_algorithm = 'SimAnn';
                    app = fcn_GlobalSingleFit(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                    app.used_optimization_algorithm = 'PaSw';
                    app = fcn_GlobalSingleFit(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                    app.used_optimization_algorithm = 'GenAlg';
                    app = fcn_GlobalSingleFit(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                    app.used_optimization_algorithm = 'GloSea';
                    app = fcn_GlobalSingleFit(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                    app.used_optimization_algorithm = short_holder;
                    [~,min_idx] = min([app.start_tupel_optimized_results{1}{1},...
                        app.start_tupel_optimized_results{2}{1},...
                        app.start_tupel_optimized_results{3}{1},...
                        app.start_tupel_optimized_results{4}{1},...
                        app.start_tupel_optimized_results{5}{1}]);
                    app.optimal_parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index} = app.start_tupel_optimized_results{min_idx}{2};
                    app.Boundaries{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index} = app.start_tupel_optimized_results{min_idx}{3};
                    if app.GUI
                        TableCell = get(app.ParamTable,'Data');
                    else
                        TableCell = app.ParamTable.Data;
                    end
                    DRT_GUI.Fit.Parameter = app.optimal_parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index};
                    % die Fittwerte(p-best) in Feld zeigen
                    for P_i = 1:length(app.optimal_parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index})
                        TableCell{P_i,3} = app.optimal_parameters{app.current_hyst_index}{app.current_crate_index}{app.current_temp_index}{app.current_SOC_index}(P_i);
                    end
                    if app.GUI
                        set(app.ParamTable,'Data',TableCell)
                    else
                        app.ParamTable.Data = TableCell;
                    end
                    
                    app.GueltigeMessungCheck.Value = 1;
                    app = fcn_GueltigeMessungCheck(app, event);
                    %fcn_PlotFittedParametersButton(app, event)
                    DRT_GUI.Fit.Parameter_min = cell2mat(DRT_GUI.Fit.aktuell_Modell.ModellCell{5});
                    DRT_GUI.Fit.Parameter_max = cell2mat(DRT_GUI.Fit.aktuell_Modell.ModellCell{6});
                    app = fcn_SpeichernButton(app, event);
                end
                if app.LHDCheckBox.Value
                    fcn_GlobalFit_TC(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
                                       sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array );
                end
                app.start_tupel_optimized = 1;
                app.Optimization_done(hyst,crate,temp,SOC)=1;                 
                fcn_CancelCheck(app);
            end
            app.current_SOC_index=SOC_index{hyst}{crate}(temp);
            %hyst down, crate down, temp down, SOC up
            for SOC=app.current_SOC_index+1:length(sorted_SOC_array{hyst}{crate}{temp})
                %Lade richtige Daten bei Wechsel des SOC
                index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                    app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                    app.SOCPopup.Value=app.startingSOCDropDown.Value;
                    app = fcn_SOCPopup(app, event);
                    if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                        app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                        app = fcn_ModellAuswahlPopup(app);
                        app = fcn_BatterieNamePopup(app);
                    end
                end
                app.current_SOC_index = SOC;
                app = fcn_adapt_boundaries(app,hyst,crate,temp,SOC,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                app = fcn_GlobalSingleFit(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                if app.LHDCheckBox.Value
                    fcn_GlobalFit_TC(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
                                       sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array );
                end
                app.Optimization_done(hyst,crate,temp,SOC)=1;
                fcn_CancelCheck(app);
            end
        end
        
        app.current_temp_index = temp_index{hyst}(crate);
         %hyst down, crate down, temp up
        for temp= app.current_temp_index+1:length(sorted_temperature_array{hyst}{crate})
            %Lade richtige Daten bei Wechsel der Temperatur
            index1=temperature_index_rearrangement_array{hyst}{crate}(temp);
            if~strcmp(app.startingtemperatureDropDown.Value,app.startingtemperatureDropDown.Items{1+index1})
                app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1+index1};
                app.TemperaturPopup.Value=app.startingtemperatureDropDown.Value;
                app = fcn_TemperaturPopup(app, event);
            end
            app.current_temp_index=temp;
            app.current_SOC_index=SOC_index{hyst}{crate}(temp);
            %hyst down, crate down, temp down, SOC down
            for SOC=app.current_SOC_index:-1:1
                %Lade richtige Daten bei Wechsel des SOC
                index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                    app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                    app.SOCPopup.Value=app.startingSOCDropDown.Value;
                    app = fcn_SOCPopup(app, event);
                    if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                        app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                        app = fcn_ModellAuswahlPopup(app);
                        app = fcn_BatterieNamePopup(app);
                    end
                end
                app.current_SOC_index=SOC;
                app = fcn_adapt_boundaries(app,hyst,crate,temp,SOC,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                app = fcn_GlobalSingleFit(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                if app.LHDCheckBox.Value
                    fcn_GlobalFit_TC(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
                                       sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array );
                end
                app.Optimization_done(hyst,crate,temp,SOC)=1;
                fcn_CancelCheck(app);
            end
            app.current_SOC_index=SOC_index{hyst}{crate}(temp);
            %hyst down, crate down, temp down, SOC up
            for SOC=app.current_SOC_index+1:length(sorted_SOC_array{hyst}{crate}{temp})
                %Lade richtige Daten bei Wechsel des SOC
                index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                    app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                    app.SOCPopup.Value=app.startingSOCDropDown.Value;
                    app = fcn_SOCPopup(app, event);
                    if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                        app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                        app = fcn_ModellAuswahlPopup(app);
                        app = fcn_BatterieNamePopup(app);
                    end
                end
                app.current_SOC_index=SOC;
                %                         waitbar_txt = ['Fitting anhand von EIS-Daten. Zustand: ',num2str(waitbar_counter),'/',num2str(waitbar_num)];
                %                         waitbar(waitbar_counter/waitbar_num,process_waitbar,waitbar_txt);
                app = fcn_adapt_boundaries(app,hyst,crate,temp,SOC,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                app = fcn_GlobalSingleFit(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                if app.LHDCheckBox.Value
                    fcn_GlobalFit_TC(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
                                       sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array );
                end
                app.Optimization_done(hyst,crate,temp,SOC)=1;                 
                fcn_CancelCheck(app);
            end
        end
    end
    app.current_crate_index = Crate_index(hyst);
    %hyst down, crate up
    for crate = app.current_crate_index+1:length(sorted_crate_array{hyst})
        if app.CRateFlag
            index=crate_index_rearrangement_array{hyst}(crate);
            app.ZustandPopup.Value=app.ZustandPopup.Items{1+index};
            app = fcn_ZustandPopup(app, event);
            app.current_crate_index = crate;
        end
        app.current_temp_index = temp_index{hyst}(crate);
        %hyst down, crate up, temp down
        for temp=app.current_temp_index:-1:1
            %Lade richtige Daten bei Wechsel der Temperatur
            index1=temperature_index_rearrangement_array{hyst}{crate}(temp);
            if~strcmp(app.startingtemperatureDropDown.Value,app.startingtemperatureDropDown.Items{1+index1})
                app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1+index1};
                app.TemperaturPopup.Value=app.startingtemperatureDropDown.Value;
                app = fcn_TemperaturPopup(app, event);
            end
            app.current_temp_index = temp;
            app.current_SOC_index= SOC_index{hyst}{crate}(temp);
            %hyst down, crate up, temp down, SOC down
            for SOC=app.current_SOC_index:-1:1
                %load SOC
                index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                    app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                    app.SOCPopup.Value=app.startingSOCDropDown.Value;
                    app = fcn_SOCPopup(app, event);
                    if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                        app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                        app = fcn_ModellAuswahlPopup(app);
                        app = fcn_BatterieNamePopup(app);
                    end
                end
                app.current_SOC_index = SOC;
                app = fcn_adapt_boundaries(app,hyst,crate,temp,SOC,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                app = fcn_GlobalSingleFit(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                if app.LHDCheckBox.Value
                    fcn_GlobalFit_TC(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
                                       sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array );
                end
                app.Optimization_done(hyst,crate,temp,SOC)=1;
                fcn_CancelCheck(app);      
            end
            app.current_SOC_index=SOC_index{hyst}{crate}(temp);
            %hyst down, crate up, temp down, SOC up
            for SOC=app.current_SOC_index+1:length(sorted_SOC_array{hyst}{crate}{temp})
                %Lade richtige Daten bei Wechsel des SOC
                index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                    app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                    app.SOCPopup.Value=app.startingSOCDropDown.Value;
                    app = fcn_SOCPopup(app, event);
                    if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                        app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                        app = fcn_ModellAuswahlPopup(app);
                        app = fcn_BatterieNamePopup(app);
                    end
                end
                app.current_SOC_index = SOC;
                app = fcn_adapt_boundaries(app,hyst,crate,temp,SOC,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                app = fcn_GlobalSingleFit(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                if app.LHDCheckBox.Value
                    fcn_GlobalFit_TC(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
                                       sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array );
                end
                app.Optimization_done(hyst,crate,temp,SOC)=1;                 
                fcn_CancelCheck(app);
            end
        end
        
        app.current_temp_index = temp_index{hyst}(crate);
        %hyst down, crate up, temp up
        for temp= app.current_temp_index+1:length(sorted_temperature_array{hyst}{crate})
            %Lade richtige Daten bei Wechsel der Temperatur
            index1=temperature_index_rearrangement_array{hyst}{crate}(temp);
            if~strcmp(app.startingtemperatureDropDown.Value,app.startingtemperatureDropDown.Items{1+index1})
                app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1+index1};
                app.TemperaturPopup.Value=app.startingtemperatureDropDown.Value;
                app = fcn_TemperaturPopup(app, event);
            end
            app.current_temp_index=temp;
            app.current_SOC_index=SOC_index{hyst}{crate}(temp);
            %hyst down, crate up, temp up, SOC down
            for SOC=app.current_SOC_index:-1:1
                %Lade richtige Daten bei Wechsel des SOC
                index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                    app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                    app.SOCPopup.Value=app.startingSOCDropDown.Value;
                    app = fcn_SOCPopup(app, event);
                    if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                        app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                        app = fcn_ModellAuswahlPopup(app);
                        app = fcn_BatterieNamePopup(app);
                    end
                end
                app.current_SOC_index=SOC;
                app = fcn_adapt_boundaries(app,hyst,crate,temp,SOC,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                app = fcn_GlobalSingleFit(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                if app.LHDCheckBox.Value
                    fcn_GlobalFit_TC(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
                                       sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array );
                end
                app.Optimization_done(hyst,crate,temp,SOC)=1;
                fcn_CancelCheck(app);
            end
            app.current_SOC_index=SOC_index{hyst}{crate}(temp);;
            %hyst down, crate up, temp up, SOC up
            for SOC=app.current_SOC_index+1:length(sorted_SOC_array{hyst}{crate}{temp})
                %Lade richtige Daten bei Wechsel des SOC
                index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                    app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                    app.SOCPopup.Value=app.startingSOCDropDown.Value;
                    app = fcn_SOCPopup(app, event);
                    if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                        app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                        app = fcn_ModellAuswahlPopup(app);
                        app = fcn_BatterieNamePopup(app);
                    end
                end
                app.current_SOC_index=SOC;
                app = fcn_adapt_boundaries(app,hyst,crate,temp,SOC,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                app = fcn_GlobalSingleFit(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                if app.LHDCheckBox.Value
                    fcn_GlobalFit_TC(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
                                       sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array );
                end
                app.Optimization_done(hyst,crate,temp,SOC)=1;
                fcn_CancelCheck(app);
            end
        end
    end
end

%hyst up
app.current_hyst_index = original_Hysteresis;
for hyst = app.current_hyst_index+1:length(sorted_hysteresis_state_array)
    if app.HysteresisFlag
        index=hysteresis_state_index_rearrangement_array(hyst);
        app.ZustandPopup_2.Value=app.ZustandPopup_2.Items{1+index};
        app = fcn_ZustandPopup(app, event);
    end
    app.current_crate_index = Crate_index(hyst);
    %hyst up, crate down
    for crate = app.current_crate_index:-1:1
        if app.CRateFlag
            index=crate_index_rearrangement_array{hyst}(crate);
            app.ZustandPopup.Value=app.ZustandPopup.Items{1+index};
            app = fcn_ZustandPopup(app, event);
            app.current_crate_index = crate;
        end
        app.current_temp_index = temp_index{hyst}(crate);
        %hyst up, crate down, temp down
        for temp=app.current_temp_index:-1:1
            %Lade richtige Daten bei Wechsel der Temperatur
            index1=temperature_index_rearrangement_array{hyst}{crate}(temp);
            if~strcmp(app.startingtemperatureDropDown.Value,app.startingtemperatureDropDown.Items{1+index1})
                app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1+index1};
                app.TemperaturPopup.Value=app.startingtemperatureDropDown.Value;
                app = fcn_TemperaturPopup(app, event);
            end
            app.current_temp_index = temp;
            app.current_SOC_index= SOC_index{hyst}{crate}(temp);
            %hyst down, crate down, temp down, SOC down
            for SOC=app.current_SOC_index:-1:1
                %load SOC
                index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                    app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                    app.SOCPopup.Value=app.startingSOCDropDown.Value;
                    app = fcn_SOCPopup(app, event);
                    if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                        app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                        app = fcn_ModellAuswahlPopup(app);
                        app = fcn_BatterieNamePopup(app);
                    end
                end
                app.current_SOC_index = SOC;
                app = fcn_adapt_boundaries(app,hyst,crate,temp,SOC,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                app = fcn_GlobalSingleFit(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                if app.LHDCheckBox.Value
                    fcn_GlobalFit_TC(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
                                       sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array );
                end
                app.Optimization_done(hyst,crate,temp,SOC)=1;
                fcn_CancelCheck(app);
            end
            app.current_SOC_index=SOC_index{hyst}{crate}(temp);
            %hyst up, crate down, temp down, SOC up
            for SOC=app.current_SOC_index+1:length(sorted_SOC_array{hyst}{crate}{temp})
                %Lade richtige Daten bei Wechsel des SOC
                index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                    app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                    app.SOCPopup.Value=app.startingSOCDropDown.Value;
                    app = fcn_SOCPopup(app, event);
                    if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                        app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                        app = fcn_ModellAuswahlPopup(app);
                        app = fcn_BatterieNamePopup(app);
                    end
                end
                app.current_SOC_index = SOC;
                app = fcn_adapt_boundaries(app,hyst,crate,temp,SOC,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                app = fcn_GlobalSingleFit(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                if app.LHDCheckBox.Value
                    fcn_GlobalFit_TC(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
                                       sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array );
                end
                app.Optimization_done(hyst,crate,temp,SOC)=1;
                fcn_CancelCheck(app);
            end
        end
        
        app.current_temp_index = temp_index{hyst}(crate);
         %hyst up, crate down, temp up
        for temp= app.current_temp_index+1:length(sorted_temperature_array{hyst}{crate})
            %Lade richtige Daten bei Wechsel der Temperatur
            index1=temperature_index_rearrangement_array{hyst}{crate}(temp);
            if~strcmp(app.startingtemperatureDropDown.Value,app.startingtemperatureDropDown.Items{1+index1})
                app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1+index1};
                app.TemperaturPopup.Value=app.startingtemperatureDropDown.Value;
                app = fcn_TemperaturPopup(app, event);
            end
            app.current_temp_index=temp;
            app.current_SOC_index=SOC_index{hyst}{crate}(temp);
            %hyst up, crate down, temp down, SOC down
            for SOC=app.current_SOC_index:-1:1
                %Lade richtige Daten bei Wechsel des SOC
                index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                    app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                    app.SOCPopup.Value=app.startingSOCDropDown.Value;
                    app = fcn_SOCPopup(app, event);
                    if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                        app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                        app = fcn_ModellAuswahlPopup(app);
                        app = fcn_BatterieNamePopup(app);
                    end
                end
                app.current_SOC_index=SOC;
                %                         waitbar_txt = ['Fitting anhand von EIS-Daten. Zustand: ',num2str(waitbar_counter),'/',num2str(waitbar_num)];
                %                         waitbar(waitbar_counter/waitbar_num,process_waitbar,waitbar_txt);
                app = fcn_adapt_boundaries(app,hyst,crate,temp,SOC,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                app = fcn_GlobalSingleFit(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                if app.LHDCheckBox.Value
                    fcn_GlobalFit_TC(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
                                       sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array );
                end
                app.Optimization_done(hyst,crate,temp,SOC)=1;
                fcn_CancelCheck(app);
            end
            app.current_SOC_index=SOC_index{hyst}{crate}(temp);;
            %hyst up, crate down, temp down, SOC up
            for SOC=app.current_SOC_index+1:length(sorted_SOC_array{hyst}{crate}{temp})
                %Lade richtige Daten bei Wechsel des SOC
                index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                    app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                    app.SOCPopup.Value=app.startingSOCDropDown.Value;
                    app = fcn_SOCPopup(app, event);
                    if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                        app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                        app = fcn_ModellAuswahlPopup(app);
                        app = fcn_BatterieNamePopup(app);
                    end
                end
                app.current_SOC_index=SOC;
                %                         waitbar_txt = ['Fitting anhand von EIS-Daten. Zustand: ',num2str(waitbar_counter),'/',num2str(waitbar_num)];
                %                         waitbar(waitbar_counter/waitbar_num,process_waitbar,waitbar_txt);
                app = fcn_adapt_boundaries(app,hyst,crate,temp,SOC,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                app = fcn_GlobalSingleFit(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                if app.LHDCheckBox.Value
                    fcn_GlobalFit_TC(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
                                       sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array );
                end
                app.Optimization_done(hyst,crate,temp,SOC)=1;
                fcn_CancelCheck(app);
            end
        end
    end
    app.current_crate_index = Crate_index(hyst);
    %hyst up, crate up
    for crate = app.current_crate_index+1:length(sorted_crate_array{hyst})
        if app.CRateFlag
            index=crate_index_rearrangement_array{hyst}(crate);
            app.ZustandPopup.Value=app.ZustandPopup.Items{1+index};
            app = fcn_ZustandPopup(app, event);
            app.current_crate_index = crate;
        end
        app.current_temp_index = temp_index{hyst}(crate);
        %hyst up, crate up, temp down
        for temp=app.current_temp_index:-1:1
            %Lade richtige Daten bei Wechsel der Temperatur
            index1=temperature_index_rearrangement_array{hyst}{crate}(temp);
            if~strcmp(app.startingtemperatureDropDown.Value,app.startingtemperatureDropDown.Items{1+index1})
                app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1+index1};
                app.TemperaturPopup.Value=app.startingtemperatureDropDown.Value;
                app = fcn_TemperaturPopup(app, event);
            end
            app.current_temp_index = temp;
            app.current_SOC_index= SOC_index{hyst}{crate}(temp);
            %hyst up, crate up, temp down, SOC down
            for SOC=app.current_SOC_index:-1:1
                %load SOC
                index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                    app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                    app.SOCPopup.Value=app.startingSOCDropDown.Value;
                    app = fcn_SOCPopup(app, event);
                    if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                        app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                        app = fcn_ModellAuswahlPopup(app);
                        app = fcn_BatterieNamePopup(app);
                    end
                end
                app.current_SOC_index = SOC;
                app = fcn_adapt_boundaries(app,hyst,crate,temp,SOC,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                app = fcn_GlobalSingleFit(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                if app.LHDCheckBox.Value
                    fcn_GlobalFit_TC(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
                                       sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array );
                end
                app.Optimization_done(hyst,crate,temp,SOC)=1;
                fcn_CancelCheck(app);
            end
            app.current_SOC_index=SOC_index{hyst}{crate}(temp);
            %hyst up, crate up, temp down, SOC up
            for SOC=app.current_SOC_index+1:length(sorted_SOC_array{hyst}{crate}{temp})
                %Lade richtige Daten bei Wechsel des SOC
                index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                    app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                    app.SOCPopup.Value=app.startingSOCDropDown.Value;
                    app = fcn_SOCPopup(app, event);
                    if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                        app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                        app = fcn_ModellAuswahlPopup(app);
                        app = fcn_BatterieNamePopup(app);
                    end
                end
                app.current_SOC_index = SOC;
                app = fcn_adapt_boundaries(app,hyst,crate,temp,SOC,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                app = fcn_GlobalSingleFit(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                if app.LHDCheckBox.Value
                    fcn_GlobalFit_TC(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
                                       sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array );
                end
                app.Optimization_done(hyst,crate,temp,SOC)=1;
                fcn_CancelCheck(app);
            end
        end
        
        app.current_temp_index = temp_index{hyst}(crate);
        %hyst up, crate up, temp up
        for temp= app.current_temp_index+1:length(sorted_temperature_array{hyst}{crate})
            %Lade richtige Daten bei Wechsel der Temperatur
            index1=temperature_index_rearrangement_array{hyst}{crate}(temp);
            if~strcmp(app.startingtemperatureDropDown.Value,app.startingtemperatureDropDown.Items{1+index1})
                app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1+index1};
                app.TemperaturPopup.Value=app.startingtemperatureDropDown.Value;
                app = fcn_TemperaturPopup(app, event);
            end
            app.current_temp_index=temp;
            app.current_SOC_index=SOC_index{hyst}{crate}(temp);
            %hyst up, crate up, temp up, SOC down
            for SOC=app.current_SOC_index:-1:1
                %Lade richtige Daten bei Wechsel des SOC
                index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                    app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                    app.SOCPopup.Value=app.startingSOCDropDown.Value;
                    app = fcn_SOCPopup(app, event);
                    if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                        app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                        app = fcn_ModellAuswahlPopup(app);
                        app = fcn_BatterieNamePopup(app);
                    end
                end
                app.current_SOC_index=SOC;
                app = fcn_adapt_boundaries(app,hyst,crate,temp,SOC,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                app = fcn_GlobalSingleFit(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                if app.LHDCheckBox.Value
                    fcn_GlobalFit_TC(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
                                       sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array );
                end
                app.Optimization_done(hyst,crate,temp,SOC)=1;
                fcn_CancelCheck(app);
            end
            app.current_SOC_index=SOC_index{hyst}{crate}(temp);
            %hyst up, crate up, temp up, SOC up
            for SOC=app.current_SOC_index+1:length(sorted_SOC_array{hyst}{crate}{temp})
                %Lade richtige Daten bei Wechsel des SOC
                index2=SOC_index_rearrangement_array{hyst}{crate}{temp}(SOC);
                if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+index2})
                    app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+index2};
                    app.SOCPopup.Value=app.startingSOCDropDown.Value;
                    app = fcn_SOCPopup(app, event);
                    if ~strcmp(app.ModellAuswahlPopup.Value,app.GlobalUsedModell)
                        app.ModellAuswahlPopup.Value=app.GlobalUsedModell;
                        app = fcn_ModellAuswahlPopup(app);
                        app = fcn_BatterieNamePopup(app);
                    end
                end
                app.current_SOC_index=SOC;
                app = fcn_adapt_boundaries(app,hyst,crate,temp,SOC,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                app = fcn_GlobalSingleFit(app,event,sorted_hysteresis_state_array,sorted_crate_array,sorted_temperature_array,sorted_SOC_array);
                if app.LHDCheckBox.Value
                    fcn_GlobalFit_TC(app,event,SOC_index,temp_index,Crate_index,original_Hysteresis,sorted_hysteresis_state_array,hysteresis_state_index_rearrangement_array,sorted_crate_array, crate_index_rearrangement_array, ...
                                       sorted_temperature_array,temperature_index_rearrangement_array, sorted_SOC_array, SOC_index_rearrangement_array );
                end
                app.Optimization_done(hyst,crate,temp,SOC)=1;
                fcn_CancelCheck(app);
            end
        end
    end
end
end

