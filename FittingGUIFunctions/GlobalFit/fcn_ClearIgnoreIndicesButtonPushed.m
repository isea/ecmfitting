function app = fcn_ClearIgnoreIndicesButtonPushed(app,event)

for sort_var_temp=1:length(app.startingtemperatureDropDown.Items)-1
    if~strcmp(app.startingtemperatureDropDown.Value,app.startingtemperatureDropDown.Items{1+sort_var_temp})
        app.startingtemperatureDropDown.Value=app.startingtemperatureDropDown.Items{1+sort_var_temp};
        app.TemperaturPopup.Value=app.startingtemperatureDropDown.Value;
        app = fcn_TemperaturPopup(app, event);
    end
    for sort_var_soc = 1:length(app.startingSOCDropDown.Items)-1
        if~strcmp(app.startingSOCDropDown.Value,app.startingSOCDropDown.Items{1+sort_var_soc})
            app.startingSOCDropDown.Value=app.startingSOCDropDown.Items{1+sort_var_soc};
            app.SOCPopup.Value=app.startingSOCDropDown.Value;
            app = fcn_SOCPopup(app, event);
        end
        app.PunkteWegnehmenTextBox.Value = '';
        app = fcn_PunkteWegnehmeButton(app, event);
    end
end

end

