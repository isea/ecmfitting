function fcn_FrequenzbereichsfittingCheckBoxValueChanged(app, event)

if app.FrequenzbereichsfittingCheckBox.Value
    app.constantPhiCheckkBox.Enable = 'on';
    app.ContinousStep.Enable = 'on';
    app.PenaltyEditField.Enable = 'on';
    app.Global_Fixed_Config_CheckBox.Enable = 'on';
    app.NelderMeadalgorithmDownhillSimplexButton.Enable = 'on';
else
    app.constantPhiCheckkBox.Enable = 'off';
    app.constantPhiCheckkBox.Value = false;
    app.ContinousStep.Enable = 'off';
    app.PenaltyEditField.Enable = 'off';
    app.Global_Fixed_Config_CheckBox.Enable = 'off';
    app.Global_Fixed_Config_CheckBox.Value = false;
    app.NelderMeadalgorithmDownhillSimplexButton.Enable = 'off';
end
end

