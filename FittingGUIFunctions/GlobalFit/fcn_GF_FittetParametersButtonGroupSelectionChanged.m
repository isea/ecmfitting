function app = fcn_GF_FittetParametersButtonGroupSelectionChanged(app, event);      
global DRT_GUI
if strcmp(app.GF_FittetParametersButtonGroup.SelectedObject.Text,'MF - Model')
    app.DRT_Model = 'MF';
    if app.GUI
        set(app.GF_ECEindicesEditField,'Editable','off')
    else
        app.GF_ECEindicesEditField.Editable = 'off';
    end
    process_indices = [];
    process_counter = 0;
    for n =1:length(DRT_GUI.Fit.aktuell_Modell.ModellCell{8})
        if ~strcmp( DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n}.Zfun_MF,'0')
            process_indices = [process_indices, num2str(n)];
            process_counter = process_counter + 1;
            process_indices = [process_indices, ', '];
        end        
    end
    process_indices(end-1:end) = [];
    set(app.GF_ECEindicesEditField,'Value',process_indices)
elseif strcmp(app.GF_FittetParametersButtonGroup.SelectedObject.Text,'Complete')
    app.DRT_Model = 'Complete';
    if app.GUI
        set(app.GF_ECEindicesEditField,'Editable','off')
    else
        app.GF_ECEindicesEditField.Editable = 'off';
    end
    process_indices = [];
    process_counter = 0;
    for n =1:length(DRT_GUI.Fit.aktuell_Modell.ModellCell{8})        
        process_indices = [process_indices, num2str(n)];
        process_counter = process_counter + 1;
        process_indices = [process_indices, ', '];
    end
    process_indices(end-1:end) = [];
    set(app.GF_ECEindicesEditField,'Value',process_indices)
else 
    app.DRT_Model = 'Custom';
    if app.GUI
        set(app.GF_ECEindicesEditField,'Editable','on')
    else
        app.GF_ECEindicesEditField.Editable = 'on';
    end
end
end

