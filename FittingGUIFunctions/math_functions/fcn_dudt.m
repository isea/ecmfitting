function u_dot = fcn_dudt(t, u, I, OCV, ROCV, COCV, R_Ladder_OCV, C_Ladder_OCV)
% Achtung, COCV ist kein reales Element im Netzwerk. Es dient nur zur
% Berechnung der Spannung auf der OCV-Spannungsquelle.
u_dot = zeros(size(u));
U_COCV = u(1);
U_RC = u(2:1+numel(OCV.R_RC));
U_C = u(2+numel(OCV.R_RC):1+numel(OCV.R_RC)+numel(OCV.C_ser));

% I_COCV ist der Strom, der parallel zu den Warburgimpedanzen in die
% OCV-Spannungsquelle flie�t
if isempty(R_Ladder_OCV)
    I_COCV = (sum(U_RC)+sum(U_C)-U_COCV)/(ROCV+OCV.R_ser)+I*(OCV.R_ser/(ROCV+OCV.R_ser));
    u_dot(1) = I/COCV;
    if numel(OCV.R_RC)>0
        u_dot(2:1+numel(OCV.R_RC))   = ((I-I_COCV)-U_RC./OCV.R_RC)./OCV.C_RC;
    end
    if numel(OCV.C_ser)>0 && abs(OCV.C_ser)<1e16
        u_dot(2+numel(OCV.R_RC):1+numel(OCV.R_RC)+numel(OCV.C_ser)) =  (I-I_COCV)./OCV.C_ser;
    end
else
    % Berechnung des Netzwerks mittels Helmholtz Superposition
    U_CL = u(2+numel(OCV.R_RC)+numel(OCV.C_ser):1+numel(OCV.R_RC)+numel(OCV.C_ser)+numel(R_Ladder_OCV));
    GL1 = 1/R_Ladder_OCV(1);
    if numel(R_Ladder_OCV) > 1; GL2 = 1/R_Ladder_OCV(2); else GL2 = 0; end
    GRser = 1/OCV.R_ser;
    GOCV = 1/ROCV;
    
    % Stromteiler f�r Stromquelle I
    I_COCV = I*(GOCV/(GRser+GL1+GOCV)); %
    % Strom aus OCV-Spannungsquelle
    I_COCV = I_COCV - U_COCV / (ROCV + 1/(GRser+GL1));
    % Strom aus Voigt-Netzwerk + Cser
    I_aus_Voigt = (sum(U_RC)+sum(U_C)) / (OCV.R_ser + 1/(GOCV+GL1));
    I_COCV = I_COCV + I_aus_Voigt * GOCV/(GOCV+GL1);
    % Strom aus Ladder-Netzwerk
    I_aus_UCL1_durch_RL1 = U_CL(1) / (R_Ladder_OCV(1) + 1/(GRser+GOCV));
    I_COCV = I_COCV + I_aus_UCL1_durch_RL1 * GOCV/(GOCV+GRser);
    
    if OCV.R_ser == 0
        % Stromteiler f�r Stromquelle I
        I_Voigt = I; %
        % Strom aus OCV-Spannungsquelle
        I_aus_OCV = U_COCV / (ROCV + 1/(GRser+GL1));
        I_Voigt = I_Voigt + I_aus_OCV ;
        
    else
        % Stromteiler f�r Stromquelle I
        I_Voigt = I*(GRser/(GRser+GL1+GOCV)); %
        % Strom aus OCV-Spannungsquelle
        I_aus_OCV = U_COCV / (ROCV + 1/(GRser+GL1));
        I_Voigt = I_Voigt + I_aus_OCV * GRser/(GRser+GL1) ;
    end
    % Strom aus Voigt-Netzwerk + Cser
    I_Voigt = I_Voigt - (sum(U_RC)+sum(U_C)) / (OCV.R_ser + 1/(GOCV+GL1));
    % Strom aus Ladder-Netzwerk
    I_aus_UCL1_durch_RL1 = U_CL(1) / (R_Ladder_OCV(1) + 1/(GRser+GOCV));
    if OCV.R_ser == 0
        I_Voigt = I_Voigt + I_aus_UCL1_durch_RL1;
    else
        I_Voigt = I_Voigt + I_aus_UCL1_durch_RL1 * GRser/(GOCV+GRser);
    end
    I_Ladder = I - I_COCV-I_Voigt;
    
    
    u_dot(1) = I/COCV;
    if numel(OCV.R_RC)>0
        u_dot(2:1+numel(OCV.R_RC))   = (I_Voigt-U_RC./OCV.R_RC)./OCV.C_RC;
    end
    if numel(OCV.C_ser)>0 && abs(OCV.C_ser)<1e16
        u_dot(2+numel(OCV.R_RC):1+numel(OCV.R_RC)+numel(OCV.C_ser)) = I_Voigt./OCV.C_ser;
    end
    
    if numel(R_Ladder_OCV)>1
        u_dot(1+numel(OCV.R_RC)+numel(OCV.C_ser)+numel(R_Ladder_OCV)) = (U_CL(end-1)-U_CL(end)) / R_Ladder_OCV(end) / C_Ladder_OCV(end);
        for i = (numel(R_Ladder_OCV)-1):-1:2
            u_dot(1+numel(OCV.R_RC)+numel(OCV.C_ser)+i) = ((U_CL(i-1)-U_CL(i)) / R_Ladder_OCV(i) - (U_CL(i)-U_CL(i+1)) / R_Ladder_OCV(i+1))/C_Ladder_OCV(i);
        end
        u_dot(1+numel(OCV.R_RC)+numel(OCV.C_ser)+1) = (I_Ladder - (U_CL(1)-U_CL(2)) / R_Ladder_OCV(2)) / C_Ladder_OCV(1);
    elseif numel(R_Ladder_OCV)>0
        u_dot(1+numel(OCV.R_RC)+numel(OCV.C_ser)+numel(R_Ladder_OCV)) = I_Ladder / C_Ladder_OCV;
    end
end
end