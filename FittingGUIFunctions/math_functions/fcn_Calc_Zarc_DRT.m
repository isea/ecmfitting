function DRT_out = fcn_Calc_Zarc_DRT(r0, tau0, phi0, tau)
DRT_out = r0./(2.*pi).*(sin((1-phi0).*pi))./(cosh(phi0.*log(tau./tau0))-cos((1-phi0).*pi));
end