function data = ReduceData( data)
resolution = 0.0001;

lengthBefore = length(data.x);

deleteList = [];
lastUndeleted = 1;
for i = 2:length(data.x)-1;
    y = fcn_linFunction(data.x(lastUndeleted), data.y(lastUndeleted), data.x(i+1), data.y(i+1), data.x(lastUndeleted+1:i));
    if(max(abs(y - data.y(lastUndeleted+1:i))) < resolution)
        deleteList = [deleteList; i];
    else
        lastUndeleted = i;
    end;
end;

data.x(deleteList) = [];
data.y(deleteList) = [];

lengthAfter = length(data.x);

if (lengthAfter < lengthBefore)
    disp([num2str(lengthBefore) ' Datenpunkte vorher, ' num2str(lengthAfter) ' Datenpunkte nachher. Reduktion um ' num2str(100-100*lengthAfter/lengthBefore,3) ' %']);
end
end