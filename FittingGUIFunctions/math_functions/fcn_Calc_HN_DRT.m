function DRT_out = fcn_Calc_HN_DRT(r0, tau0, phi0, Tau_out)
global DRT_GUI;
global DRT_Config;
Schwingfaktor = DRT_Config.Schwingfaktor; % ist hoffentlich immer 1
InterpolationsFaktor = 3;
FilterFaktor_ext = DRT_Config.FilterFaktor_ext;
FilterFaktor_int = FilterFaktor_ext / InterpolationsFaktor * Schwingfaktor;
%   w_DRT = 10.^(-15:0.1:15)/tau0;
w_DRT = 10.^(log10(min(DRT_GUI.DRT.EI_DRT.omega)):0.1:log10(max(DRT_GUI.DRT.EI_DRT.omega)));
[freq_int,HN_imag_int]=interpolate_signal(InterpolationsFaktor,imag(r0./((1+1i*w_DRT'*tau0).^phi0)),w_DRT'/2/pi);
[x_int,DRT_HN]=makeDRT(HN_imag_int',freq_int',true,FilterFaktor_int);
%     [x_int,DRT_HN]=makeDRT(imag(r0./((1+1i*FittingGUI.DRT.EI_DRT.omega'*tau0).^phi0)),FittingGUI.DRT.EI_DRT.frequenz',true,FilterFaktor_int);
DRT_HN=real(DRT_HN)/Schwingfaktor;


% [C,ia,ib]=intersect(FittingGUI.DRT.EI_DRT.tau,Tau_out);
% DRT_HN=DRT_HN(ia);
% DRT_HN=DRT_HN(FittingGUI.DRT.EI_DRT.aktiv');
DRT_out=interp1(log(freq_int),DRT_HN,log(1./Tau_out/(2*pi)));
end