function DRT_out = fcn_Calc_PorEl_DRT(r0, tau0, rmp0_durch_r0, Tau_out)
global DRT_GUI;
global DRT_Config;
Schwingfaktor = DRT_Config.Schwingfaktor; % ist hoffentlich immer 1
InterpolationsFaktor = 3;
FilterFaktor_ext = DRT_Config.FilterFaktor_ext;
FilterFaktor_int = FilterFaktor_ext / InterpolationsFaktor * Schwingfaktor;
rmp0 = rmp0_durch_r0 *r0;
w_DRT = 10.^(log10(min(DRT_GUI.DRT.EI_DRT.omega)):0.1:log10(max(DRT_GUI.DRT.EI_DRT.omega)));
[freq_int,HN_imag_int]=interpolate_signal(InterpolationsFaktor,imag((rmp0.*(r0./(1+1i.*w_DRT.*tau0))).^0.5...
    .*coth((rmp0./(r0./(1+1i.*w_DRT.*tau0))).^0.5)),w_DRT'/2/pi);
[~,DRT_PorEl]=makeDRT(HN_imag_int',freq_int',true,FilterFaktor_int);
DRT_PorEl=real(DRT_PorEl)/Schwingfaktor;
DRT_out=interp1(log(freq_int),DRT_PorEl,log(1./Tau_out/(2*pi)));
end