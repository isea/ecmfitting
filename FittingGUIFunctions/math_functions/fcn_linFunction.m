function y = fcn_linFunction(x1, y1, x2, y2, x)
m = (y2-y1)/(x2-x1);
n = y1 - m*x1;
y = m*x + n;
end

