
function mapResult = tempToMap(Temp, Tmin, Tmax, Tpoints)
    mapResult = round(round((Temp-Tmin)/(Tmax-Tmin)*(Tpoints-1) ) +1);
end