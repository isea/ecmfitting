function z_real = function_model_all_types2_real(app,p, w, formula)
z_comp = eval(formula);
z_real=real(z_comp);
end