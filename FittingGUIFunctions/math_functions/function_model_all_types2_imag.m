function z_imag = function_model_all_types2_imag(app,p, w, formula)
z_comp = eval(formula);
z_imag=imag(z_comp);
end