function DRT_out = fcn_Calc_Model_DRT(z_model, p, Tau_out)
global DRT_GUI;
global DRT_Config;
Schwingfaktor = DRT_Config.Schwingfaktor; % ist hoffentlich immer 1
InterpolationsFaktor = 3;
FilterFaktor_ext = DRT_Config.FilterFaktor_ext;
FilterFaktor_int = FilterFaktor_ext / InterpolationsFaktor * Schwingfaktor;
w_DRT = 10.^(log10(min(DRT_GUI.DRT.EI_DRT.omega)):0.1:log10(max(DRT_GUI.DRT.EI_DRT.omega)));
w = w_DRT;
[freq_int,model_imag_int]=interpolate_signal(InterpolationsFaktor,imag(eval(z_model)),w_DRT'/2/pi);
[~,DRT_Model]=makeDRT(model_imag_int',freq_int',true,FilterFaktor_int);
DRT_Model=real(DRT_Model)/Schwingfaktor;
DRT_out=interp1(log(freq_int),DRT_Model,log(1./Tau_out/(2*pi)));
end