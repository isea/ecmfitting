function app = fcn_ExportelectricalModellasXMLButtonPushed(app, ~)
%Aufbau des SimpleFit structs
addpath(fullfile("FittingGUIFunctions", "SimpleFit", "SimpleFitFunctions", "xml_functions"));

if app.HysteresisFlag
    app = sort_hyst(app);
end
if app.CRateFlag
    app = sort_crate(app);
end
app = sort_temp(app);
app = sort_SOC(app);

app = update_listboxes(app);
% app.SF_SchrittweiteEditField.Value = 5;
% app.SF_OCVSchrittweiteEditField.Value = 1;
framework_struct = app.fitProperties.aktuell_Modell.Modell.generateXMLstruct(app,app.actBattery.fit,app.actBattery.SOC,...
    app.actBattery.temp,app.actSOH,app.actBattery.nom_cap,app.SF_SchrittweiteEditField.Value,0,app.actBattery.Hyst,app.actBattery.Crate);
if ~isempty(app.actBattery.qOCV_CH(1,1).SOC)&&~isempty(app.actBattery.qOCV_DCH(1,1).SOC)
    framework_struct = generate_OCV(framework_struct,app.fitProperties.aktuell_Modell.Modellname,[-10:app.SF_OCVSchrittweiteEditField.Value:100],...
        app.actBattery.temp,app.actBattery.qOCV_CH,app.actBattery.qOCV_DCH);
else
    if ~isempty(app.actBattery.qOCV_CH(1, 1).SOC)
        framework_struct = generate_OCV(framework_struct, app.fitProperties.aktuell_Modell.Modellname, [-10:app.SF_OCVSchrittweiteEditField.Value:100], ...
            app.actBattery.temp, app.actBattery.qOCV_CH);
    else
        if ~isempty(app.actBattery.qOCV_DCH(1, 1).SOC)
            framework_struct = generate_OCV(framework_struct, app.fitProperties.aktuell_Modell.Modellname, [-10:app.SF_OCVSchrittweiteEditField.Value:100], ...
                app.actBattery.temp, app.actBattery.qOCV_DCH);
        else
            warndlg('Keine OCV im Export enthalten');
        end
    end
end

% Add MetaData
framework_struct = generate_MetaData(framework_struct,app.actBattery,app.user);
cell_str = [framework_struct.MetaData.electrical.CellName.Text,'(',framework_struct.MetaData.electrical.ModelName.Text,')'];
[~,git_hash] = system('git rev-parse HEAD');
framework_struct.MetaData.electrical.ImportCXML_CommitHash.Text = git_hash(1:40);
app.electrical_models{1, 1} = framework_struct;
app.electrical_models{1, 2} = ['My', app.ModellAuswahlPopup.Value];

%Aufbau des XMLs
%if isfield(framework_struct,'LHD')
%     XML_struct.Configuration = struct('MetaData',[],'Options',[],'Observer',[],'ThermalMaterials',[],'CoolingBlocks',[], ...
%         'CachedCoolings',[],'CustomDefinitions',[],'LHD',[],'RootElement',[]);
% else
    XML_struct.Configuration = struct('MetaData',[],'Options',[],'Observer',[],'ThermalMaterials',[],'CoolingBlocks',[], ...
        'CachedCoolings',[],'CustomDefinitions',[],'RootElement',[]);
% end
XML_struct = generate_singleCell_XML_struct_FittingGUI(app,XML_struct,app.actBattery.Hyst,app.actBattery.Crate);
XML_struct = generate_options_observer_FittingGUI(app,XML_struct);
numCells = 1;
XML_struct = generate_root_element_FittingGUI(app, XML_struct, 0, 0);
XML_struct.Configuration.CustomDefinitions.(app.electrical_models{2}).ThermalBlock.Attributes.ref = app.thermal_models{1, 2};
created_xml = XML_struct;



oldfolder = cd('SF_output');
[filename,pathname] = uiputfile('*.xml','Speichere XML',[XML_struct.Configuration.MetaData.electrical.CellName.Text '.xml']);
save_xml_Struct(XML_struct,[pathname,filename]);
cd(oldfolder)
end
