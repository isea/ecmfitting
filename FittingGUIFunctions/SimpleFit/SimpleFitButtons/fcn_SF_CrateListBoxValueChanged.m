function fcn_SF_CrateListBoxValueChanged(app, event)
value = str2double(app.SF_CrateListBox.Value);
app.actCrate = find(app.Crate==str2double(app.SF_TemperaturListBox.Value));
app.actTemp = find(app.Temp==str2double(app.SF_TemperaturListBox.Value));
if app.HysteresisFlag
    app.actHyst = find(app.Hyst==str2double(app.SF_HystListBox.Value));
else
    app.actHyst = 1;
end
tempFit = app.actBattery.getFit(app.actSOC,app.actTemp,app.actSOH,app.actHyst,app.actCrate);
if isa(tempFit,'Fit')
    if ~isempty(tempFit.Parameter)
        app.fit = tempFit;
    end
end
update_listboxes(app);
change_state(app);
app.SF_NomCap.Text = [num2str(app.actBattery.capacity(app.actHyst,app.actCrate,app.actTemp,app.actSOH)) ' Ah'];
% plot_nyquist(app);
% plot_relax(app);
% plot_qOCV(app);
% plot_datapoints(app);
update_parameter_table(app);
end

