function fcn_SF_FitAll1endButtonPushed(app, event)
for index = 1:length(app.SF_SOCListBox.Items)
    app.actSOC = find(app.SOC==str2double(app.SF_SOCListBox.Items{index}));
    app.SF_SOCListBox.Value = app.SF_SOCListBox.Items{index};
    fit_act_data(app);
    plot_nyquist(app);
    plot_relax(app);
    plot_datapoints(app);
end
end