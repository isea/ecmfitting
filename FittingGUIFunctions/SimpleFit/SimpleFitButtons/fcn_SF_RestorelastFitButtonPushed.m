function fcn_SF_RestorelastFitButtonPushed(app, event)
if ~isempty(app.last_fit)
    app.actBattery = app.actBattery.addFit(app.last_fit,app.actSOC,app.actTemp,app.actSOH);
    app.fit = app.last_fit;
end
plot_nyquist(app);
update_parameter_table(app);
end