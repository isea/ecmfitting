function fcn_SF_SOHListBoxValueChanged(app, event)
value = str2double(app.SF_SOHListBox.Value);
app.actSOH = find(app.SOH==value);
tempFit = app.actBattery.getFit(app.actSOC,app.actTemp,app.actSOH);
if isa(tempFit,'Fit')
    if ~isempty(tempFit.Parameter)
        app.fit = tempFit;
    end
end
update_listboxes(app);
change_state(app);
app.SF_NomCap.Text = [num2str(app.actBattery.capacity(app.actTemp,app.actSOH)) ' Ah'];
app.SF_TestName.Text = app.actBattery.auswertung_name{app.actSOH};
% plot_nyquist(app);
% plot_relax(app);
% plot_qOCV(app);
% plot_datapoints(app);
update_parameter_table(app);
end