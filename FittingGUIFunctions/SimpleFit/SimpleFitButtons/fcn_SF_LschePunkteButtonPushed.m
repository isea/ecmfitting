function fcn_SF_LschePunkteButtonPushed(app, event)
EISdata = app.actBattery.EISdata(app.actSOC,app.actTemp,app.actSOH);
EISdata_clean = choosePointsGui(EISdata);
[C,ia,ib] = intersect(EISdata.Zreal, EISdata_clean.Zreal);
EISdata_clean.frequenz = EISdata.frequenz(ia);
app.actBattery = app.actBattery.addEISdata_clean(EISdata_clean,app.actSOC,app.actTemp,app.actSOH);
plot_nyquist(app);
end