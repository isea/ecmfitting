function fcn_SF_ImportiereOCVMenuSelected(app, event)
oldfolder = cd(app.default_path);
uiopen('load');
cd(oldfolder);
[row,col] = size(OCV_array);
for n = 1:row
    if strcmp(OCV_array{n,2},'CHA')
        OCV_array{n,2} = 'CH';
    end
    if max(OCV_array{n,1}.SOC) < 10
        OCV_array{n,1}.SOC = OCV_array{n,1}.SOC * 100;
    end
end
import_OCV(app,OCV_array);
end