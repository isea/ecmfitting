function fcn_ExportelectricalModellasMATButtonPushed(app, event)
addpath('FittingGUIFunctions/SimpleFit/SimpleFitFunctions/xml_functions');

sort_temp(app);
sort_SOC(app);
update_listboxes(app);
app.SF_SchrittweiteEditField.Value = 5;
app.SF_OCVSchrittweiteEditField.Value = 1;
framework_struct = app.fitProperties.aktuell_Modell.Modell.generateXMLstruct(app.actBattery.fit,app.actBattery.SOC,...
    app.actBattery.temp,app.actSOH,app.actBattery.nom_cap,app.SF_SchrittweiteEditField.Value,0);
if ~isempty(app.actBattery.qOCV_CH(1,1).SOC)&&~isempty(app.actBattery.qOCV_DCH(1,1).SOC)
    framework_struct = generate_OCV(framework_struct,app.fitProperties.aktuell_Modell.Modellname,[-10:app.SF_OCVSchrittweiteEditField.Value:100],...
        app.actBattery.temp,app.actBattery.qOCV_CH,app.actBattery.qOCV_DCH);
else
    if ~isempty(app.actBattery.qOCV_CH(1,1).SOC)
        framework_struct = generate_OCV(framework_struct,app.fitProperties.aktuell_Modell.Modellname,[-10:app.SF_OCVSchrittweiteEditField.Value:100],...
            app.actBattery.temp,app.actBattery.qOCV_CH);
    else
        if ~isempty(app.actBattery.qOCV_DCH(1,1).SOC)
            framework_struct = generate_OCV(framework_struct,app.fitProperties.aktuell_Modell.Modellname,[-10:app.SF_OCVSchrittweiteEditField.Value:100],...
                app.actBattery.temp,app.actBattery.qOCV_DCH);
        else
            warndlg('Keine OCV im Export enthalten');
        end
    end
end

% Add MetaData
framework_struct = generate_MetaData(framework_struct,app.actBattery,app.user);
%



cell_str = [framework_struct.MetaData.electrical.CellName.Text,'(',framework_struct.MetaData.electrical.ModelName.Text,')'];
[~,git_hash] = system('git rev-parse HEAD');
framework_struct.MetaData.electrical.ImportCXML_CommitHash.Text = git_hash(1:40);
app.electrical_models{1,1} = framework_struct;
app.electrical_models{1,2} = ['My' app.ModellAuswahlPopup.Value];

eleDatabaseModel = app.electrical_models(1,:);
[filename,path] = uiputfile('*.mat','Speichere Mat-File','newSimulation.mat');
old_path = cd;
cd(path)
save(filename,'eleDatabaseModel');
cd(old_path)
end