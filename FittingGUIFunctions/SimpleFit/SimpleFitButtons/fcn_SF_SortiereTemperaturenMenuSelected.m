function fcn_SF_SortiereTemperaturenMenuSelected(app, event)
sort_temp(app);
update_listboxes(app);
app.SF_NomCap.Text = [num2str(app.actBattery.capacity(app.actTemp,app.actSOH)) ' Ah'];
plot_nyquist(app);
plot_relax(app);
plot_qOCV(app);
end