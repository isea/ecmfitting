function fcn_SF_LadeBatterieMenuSelected(app, event)
load_models(app)
app.default_path=cd;
if ~isempty(app.default_path)
    oldfolder = cd(app.default_path);
else
    oldfolder=cd;
end
uiopen('load');
cd(oldfolder);
try
    app.actBattery = battery;
catch
    msgbox('Datei enth�lt keine g�ltige Batterieklasse');
end
app.actSOC = 1;
app.actTemp = 1;
app.actSOH = 1;
app.actParameter = 1;

% Arbeitsvektoren laden
app.SOC = app.actBattery.SOC;
app.Temp = app.actBattery.temp;
app.SOH = app.actBattery.SOH;

% Auswahlboxen aktualisieren
update_listboxes(app);

% Zelldaten aktualiseren
app.SF_NomCap.Text = [num2str(app.actBattery.capacity(app.actTemp,app.actSOH)) ' Ah'];
app.SF_BatName.Text = app.actBattery.name;
if app.actBattery.auswertung_name{:}~=[]
    app.SF_TestName.Text = app.actBattery.auswertung_name;
end

% Modell ausw�hlen
app.SF_ModellauswahlDropDown.Value = app.actBattery.model;
index = find(strcmp(app.actBattery.model,app.SF_ModellauswahlDropDown.Items));
change_act_model(app,index)

plot_nyquist(app);
plot_relax(app);
plot_qOCV(app);
plot_datapoints(app);
update_parameter_table(app);
end