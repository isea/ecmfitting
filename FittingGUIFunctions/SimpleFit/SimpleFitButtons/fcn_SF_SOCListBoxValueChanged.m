function fcn_SF_SOCListBoxValueChanged(app, event)
value = str2double(app.SF_SOCListBox.Value);
app.actSOC = find(app.SOC==value);
if app.HysteresisFlag
    app.actHyst = find(app.Hyst==str2double(app.SF_HystListBox.Value));
else
    app.actHyst = 1;
end
if app.CRateFlag
    app.actCrate = find(app.Crate==str2double(app.SF_CrateListBox.Value));
else
    app.actCrate = 1;
end
tempFit = app.actBattery.getFit(app.actSOC,app.actTemp,app.actSOH,app.actHyst,app.actCrate);
if isa(tempFit,'Fit')
    if ~isempty(tempFit.Parameter)
        app.fit = tempFit;
    end
end
change_state(app);
% plot_nyquist(app);
% plot_relax(app);
% plot_datapoints(app);
update_parameter_table(app);
end