function fcn_SF_NeueBatterieMenuSelected(app, event)
name = inputdlg('Bitte Namen der Zelle eingeben:','Neue Zelle');
if isempty(name{1,1})
    msgbox('Der Name darf nicht Leer sein.','','error');
else
    app.actBattery = Battery(name{1,1});
    app.SF_BatName.Text = name{1,1};
end
end