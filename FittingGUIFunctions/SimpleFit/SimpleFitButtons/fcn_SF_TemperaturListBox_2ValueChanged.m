function fcn_SF_TemperaturListBox_2ValueChanged(app, event)
value = app.SF_TemperaturListBox_2.Value;
index = find(strcmp(value,app.SF_TemperaturListBox_2.Items));
app.actTemp = index;

update_listboxes(app);
change_state(app);
plot_parameter(app);
end