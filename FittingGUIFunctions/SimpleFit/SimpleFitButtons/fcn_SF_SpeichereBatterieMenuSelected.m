function fcn_SF_SpeichereBatterieMenuSelected(app, event)
app.actBattery.metadata.creator = app.user;
app.actBattery.metadata.date = datetime('now');
[~,git_hash] = system('git rev-parse HEAD');
app.actBattery.metadata.SimpleFit_CommitHash = git_hash(1:40);

battery = app.actBattery;
oldfolder = cd(app.default_path);
uisave('battery',battery.name);
% app.SimpleFitUIFigure.Visible = 'off';
% app.SimpleFitUIFigure.Visible = 'on';
cd(oldfolder);
end