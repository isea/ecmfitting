function fcn_SF_SOHListBox_2ValueChanged(app, event)
value = app.SF_SOHListBox_2.Value;
index = find(strcmp(value,app.SF_SOHListBox_2.Items));
app.actSOH = index;

update_listboxes(app);
change_state(app);
plot_parameter(app);
end