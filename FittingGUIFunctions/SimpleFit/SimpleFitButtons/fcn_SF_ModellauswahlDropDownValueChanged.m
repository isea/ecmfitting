function fcn_SF_ModellauswahlDropDownValueChanged(app, event)
value = app.SF_ModellauswahlDropDown.Value;
index = find(strcmp(value,app.ModellauswahlDropDown.Items));
choice = questdlg({'Bei der �nderung des Modelles werden alle Fits gel�scht!','Modell wirklich �ndern?'},...
    '','Ja','Nein','Nein');
switch choice
    case 'Ja'
        change_act_model(app,index);
        app.actBattery = app.actBattery.deleteAllFits();
        plot_nyquist(app);
        plot_datapoints(app);
    case 'Nein'
        app.SF_ModellauswahlDropDown.Value = event.PreviousValue;
end
end