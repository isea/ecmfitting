function fcn_SimpleFit(app,event)
global DRT_GUI
set(app.SF_FileMenu,'Visible','on')
set(app.SF_OptimierungMenu,'Visible','on')
set(app.SF_OCVMenu,'Visible','on')
set(app.MenDatei,'Visible','off')
set(app.MenExtras,'Visible','off')
% set(app.MenRelaxFFT,'Visible','off')
app.user = char(java.lang.System.getProperty('user.name'));
app.actBattery = Battery('');
app.pulse = PulsData();
load_models(app);
% initialize_plots(app);
end