function fcn_SF_ExportFrameworkButtonPushed(app, event)
addpath('FittingGUIFunctions/SimpleFit/SimpleFitFunctions/xml_functions');

sort_temp(app);
sort_SOC(app);
update_listboxes(app);

framework_struct = app.fitProperties.aktuell_Modell.Modell.generateXMLstruct(app.actBattery.fit,app.actBattery.SOC,...
    app.actBattery.temp,app.actSOH,app.actBattery.nom_cap,app.SF_SchrittweiteEditField.Value,0);
if ~isempty(app.actBattery.qOCV_CH(1,1).SOC)&&~isempty(app.actBattery.qOCV_DCH(1,1).SOC)
    framework_struct = generate_OCV(framework_struct,app.fitProperties.aktuell_Modell.Modellname,[-10:app.SF_OCVSchrittweiteEditField.Value:100],...
        app.actBattery.temp,app.actBattery.qOCV_CH,app.actBattery.qOCV_DCH);
else
    if ~isempty(app.actBattery.qOCV_CH(1,1).SOC)
        framework_struct = generate_OCV(framework_struct,app.fitProperties.aktuell_Modell.Modellname,[-10:app.SF_OCVSchrittweiteEditField.Value:100],...
            app.actBattery.temp,app.actBattery.qOCV_CH);
    else
        if ~isempty(app.actBattery.qOCV_DCH(1,1).SOC)
            framework_struct = generate_OCV(framework_struct,app.fitProperties.aktuell_Modell.Modellname,[-10:app.SF_OCVSchrittweiteEditField.Value:100],...
                app.actBattery.temp,app.actBattery.qOCV_DCH);
        else
            warndlg('Keine OCV im Export enthalten');
        end
    end
end

% Add MetaData
framework_struct = generate_MetaData(framework_struct,app.actBattery,app.user);
oldfolder = cd(app.default_path);
uisave('framework_struct',app.actBattery.name);
cd(oldfolder);

app.SF_NomCap.Text = [num2str(app.actBattery.capacity(app.actTemp,app.actSOH)) ' Ah'];
% plot_nyquist(app);
% plot_relax(app);
% plot_qOCV(app);

end