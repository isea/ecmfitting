classdef SF_ESB_R3RC < ESB
    %ESB_1RC1SPHERICALDIFFUSION Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function obj = SF_ESB_R3RC()
            %ESB_1RC1SPHERICALDIFFUSION Construct an instance of this class
            %   Detailed explanation goes here
            addpath('FittingGUIFunctions/SimpleFit/SimpleFitFunctions/ESBelements');
            obj.model={'','','',{},{},{},{},{}};
            
            obj.Name = 'R3RC';
            
            %Rser
            i=1;
            esb_element{i} = SF_ESBe_Rser();
            Appendix{i} = 'ser';
            HFMFLF = 'HF' ; % entweder 'Auto' oder 'HF' oder 'MF' oder 'LF'
            Startwerte = {  1e-3    };
            Minimum = {     1e-7    };
            Maximum = {     100     };
            Fix = {         0       };
            obj.model = add_ESBe(obj.model,esb_element{i},Appendix{i},HFMFLF,Startwerte,Minimum,Maximum,Fix);
            
            % RC1
            i=i+1;
            esb_element{i} = SF_ESBe_RC_Tau();
            Appendix{i} = '_1';
            HFMFLF = 'MF' ; % entweder 'Auto' oder 'HF' oder 'MF' oder 'LF'
            %               R       Tau
            Startwerte = {  1e-3    1e-4  };
            Minimum =    {  1e-7    1e-5   };
            Maximum =    {  100     1e-2 };
            Fix =        {  0       0   };
            obj.model = add_ESBe(obj.model,esb_element{i},Appendix{i},HFMFLF,Startwerte,Minimum,Maximum,Fix);
            
            % RC2
            i=i+1;
            esb_element{i} = SF_ESBe_RC_Tau();
            Appendix{i} = '_2';
            HFMFLF = 'MF' ; % entweder 'Auto' oder 'HF' oder 'MF' oder 'LF'
            %               R       Tau
            Startwerte = {  1e-3    1e-2  };
            Minimum =    {  1e-7    1e-3   };
            Maximum =    {  100     1e-1 };
            Fix =        {  0       0   };
            obj.model = add_ESBe(obj.model,esb_element{i},Appendix{i},HFMFLF,Startwerte,Minimum,Maximum,Fix);
            
            %RC3
            i=i+1;
            esb_element{i} = SF_ESBe_RC_Tau();
            Appendix{i} = '_3';
            HFMFLF = 'LF' ; % entweder 'Auto' oder 'HF' oder 'MF' oder 'LF'
            %               R       Tau
            Startwerte = {  1e-3    1e1  };
            Minimum =    {  1e-7    1e1   };
            Maximum =    {  100     1e3 };
            Fix =        {  0       0   };
            obj.model = add_ESBe(obj.model,esb_element{i},Appendix{i},HFMFLF,Startwerte,Minimum,Maximum,Fix);
            
            obj.elemente = esb_element;
            obj.Appendix = Appendix;
            obj = obj.makeFormula();
        end
    end
end
