classdef SF_ESB_LiIon4  < ESB
    %LIION4 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function obj = SF_ESB_LiIon4()
            %LIION4 Construct an instance of this class
            %   Detailed explanation goes here
            addpath('FittingGUIFunctions/SimpleFit/SimpleFitFunctions/SF_ESBelements');
            obj.model={'','','',{},{},{},{},{}};
            
            obj.Name = 'LiIon4';
            
            %Rser
            i=1;
            esb_element{i} = SF_ESBe_Rser();
            Appendix{i} = 'ser';
            HFMFLF = 'HF' ; % entweder 'Auto' oder 'HF' oder 'MF' oder 'LF'
            Startwerte = {  1e-3    };
            Minimum = {     1e-7    };
            Maximum = {     100     };
            Fix = {         0       };
            obj.model = add_ESBe(obj.model,esb_element{i},Appendix{i},HFMFLF,Startwerte,Minimum,Maximum,Fix);
            
            
            %RL-Glied mit CPE statt L 
            i=i+1;
            esb_element{i} = SF_ESBe_RL_CPE();
            Appendix{i} = 'ind';
            HFMFLF = 'HF' ; % entweder 'Auto' oder 'HF' oder 'MF' oder 'LF'
            %               R       L       Phi
            Startwerte = {  1e-3    1e-8    1   };
            Minimum = {     0       0       0   };
            Maximum = {     100     inf     1   };
            Fix = {         0       0       0   };
            obj.model = add_ESBe(obj.model,esb_element{i},Appendix{i},HFMFLF,Startwerte,Minimum,Maximum,Fix);
            
            % Reaktion Anode
            i=i+1;
            esb_element{i} = SF_ESBe_RMP_HN1RC();
            Appendix{i} = '_A';
            HFMFLF = 'Auto' ; % entweder 'Auto' oder 'HF' oder 'MF' oder 'LF'
            %               R_ct    Tau_dl  Phi_HN  RMP
            Startwerte = {  1e-3    1e-4    1      1e-3 };
            Minimum = {     1e-7    1e-5    0.5    1e-7 };
            Maximum = {     100     1e-2    1      100};
            Fix = {         0       0       1      0 };
            obj.model = add_ESBe(obj.model,esb_element{i},Appendix{i},HFMFLF,Startwerte,Minimum,Maximum,Fix);
            
            % Positive Elektrode
            i=i+1;
            esb_element{i} = SF_ESBe_RMP_HN1RC_DiffKugel();
            Appendix{i} = '_B';
            HFMFLF = 'Auto' ; % entweder 'Auto' oder 'HF' oder 'MF' oder 'LF'
            %               (R_ct, Tau_dl,Phi_HN,  R_D,    Tau_D     RMP)
            Startwerte = {  1e-3    1e-2    1      1e-3      100     1e-3};
            Minimum = {     1e-7    1e-5    0.5    1e-7      10      1e-7 };
            Maximum = {     100     1e-1    1      100       1500    100};
            Fix = {         0       0       1      0         0        0};
            obj.model = add_ESBe(obj.model,esb_element{i},Appendix{i},HFMFLF,Startwerte,Minimum,Maximum,Fix);
            
            % Por�se Elektrode -> Elektrolytdiffusion
            i=i+1;
            esb_element{i} = SF_ESBe_RC_Tau();
            Appendix{i} = '_DP';
            HFMFLF = 'LF' ; % entweder 'Auto' oder 'HF' oder 'MF' oder 'LF'
            %               R       Tau
            Startwerte = {  1e-3    40  };
            Minimum =    {  1e-7    1   };
            Maximum =    {  100     150 };
            Fix =        {  0       0   };
            obj.model = add_ESBe(obj.model,esb_element{i},Appendix{i},HFMFLF,Startwerte,Minimum,Maximum,Fix);
            
            % Ausgleichsprozesse
            i=i+1;
            esb_element{i} = SF_ESBe_RC_Tau();
            Appendix{i} = '_slow';
            HFMFLF = 'LF' ; % entweder 'Auto' oder 'HF' oder 'MF' oder 'LF'
            %               R       Tau
            Startwerte = {  0    2000  };
            Minimum =    {  1e-7    500   };
            Maximum =    {  100     5000 };
            Fix =        {  1      1   };
            obj.model = add_ESBe(obj.model,esb_element{i},Appendix{i},HFMFLF,Startwerte,Minimum,Maximum,Fix);
            
            % Kombiniere die beiden  R_MP Parameter von positiver und
            % negativer Elektrode
            obj = obj.combineParameters('R_MP','R_MP_A','R_MP_B');
            
            obj.elemente = esb_element;
            obj.Appendix = Appendix;
            obj = obj.makeFormula();
        end
    end
end

