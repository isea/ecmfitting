%% General config for Simple Fit

%% Input  filename format (copy from my preference in AHJO)

app.FileFormat.string = '%proj%=%spec%=%conn%=%tset%=%date%=%ptst%=%test%=%equi%';
app.FileFormat.delimiter = '=';

app.FileFormat.BatName = '%spec%';
app.FileFormat.EISName = '%test%';
app.FileFormat.ProcedureName = '%ptst%';

%% Colors for Plots

colors_RWTH;
app.Colors = colorsRWTH;