function [] = update_parameter_table(app)
%UPDATE_PARAMETER_TABLE Summary of this function goes here
%   Detailed explanation goes here
TableData = app.SF_UITable.Data;

Fit = app.fit;

if ~isempty(Fit)
    for i=1:length(Fit.Parameter)
        TableData{i,2} = logical(Fit.ParFix(i));
        TableData{i,3} = Fit.Parameter(i);
        TableData{i,4} = Fit.Parameter_min(i);
        TableData{i,5} = Fit.Parameter_max(i);
        if Fit.Parameter(i) <= Fit.Parameter_min(i)*1.001 || Fit.Parameter(i) >= Fit.Parameter_max(i)*0.999
            TableData{i,6} = true;
        else
            TableData{i,6} = false;
        end
    end
    
    app.SF_UITable.Data = TableData;
end
end

