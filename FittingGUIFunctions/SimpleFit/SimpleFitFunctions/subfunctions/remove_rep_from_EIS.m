function [data] = remove_rep_from_EIS(input)
%REMOVE_ Summary of this function goes here
%   Detailed explanation goes here
data = EISData();
[data.frequenz,indices] = unique(input.frequenz,'stable');
data.Zreal = zeros(1,length(data.frequenz));
data.Zimg = zeros(1,length(data.frequenz));
for i=1:length(indices)
   data.Zreal(i) = input.Zreal(indices(i));
   data.Zimg(i) = input.Zimg(indices(i));
end
end

