function [] = plot_parameter(app)
%PLOT_PARAMETER Summary of this function goes here
%   Detailed explanation goes here
ax = app.SF_Parameter;

ax.YLabel.Interpreter = 'latex';
ax.YLabel.String = '';
ax.YLimMode = 'auto';
ax.Title.String = app.SF_ParameterListBox.Value;

SOC = app.actBattery.SOC;
j=1;

for i=1:length(SOC)
    try
        parameter(j) = app.actBattery.fit(i,app.actTemp,app.actSOH).Parameter(app.actParameter);
        SOCclean(j) = SOC(i);
        j=j+1;
    catch
    end
end

plot(ax,SOCclean,parameter,'-o','LineWidth',2,'Color',app.Colors.Blau);

switch ax.Title.String(1)
    case 'R'
       ax.YLabel.String = ['$' app.SF_ParameterListBox.Value ' / \Omega$']; 
    case 'L'
       ax.YLabel.String = ['$' app.SF_ParameterListBox.Value ' / H$'];  
    case 'C'    
       ax.YLabel.String = ['$' app.SF_ParameterListBox.Value ' / F$']; 
    case 'T'
       ax.YLabel.String = ['$' app.SF_ParameterListBox.Value ' / sec$']; 
    case 'P'
       ax.YLabel.String = ['$' app.SF_ParameterListBox.Value ' / \circ$'];
end



end

