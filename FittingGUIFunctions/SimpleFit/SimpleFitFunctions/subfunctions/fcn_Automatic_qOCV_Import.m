function app = fcn_Automatic_qOCV_Import(app,event)
global DRT_GUI
old_path = cd;
if app.GUI
    path=fullfile(cd,'output',DRT_GUI.Testparameter.Batterie,DRT_GUI.Testparameter.Zustand);
else
    path=fullfile(app.OutputPath, DRT_GUI.Testparameter.Batterie,DRT_GUI.Testparameter.Zustand);
end
cd(path)
list = dir('*grad*');
qOCV_counter = 0;
for i=1:length(list)
    cd(fullfile(path,list(i).name))
    qOCV_List = dir('*qOCV-File*');
    if ~isempty(qOCV_List)
       qOCV_counter = qOCV_counter + 1;    
    end
end
[row,col] = size(app.actBattery.qOCV_CH);
for l = 1:row
    for k = 1:col        
        app.actBattery.qOCV_CH(l,k).SOC = [];
        app.actBattery.qOCV_CH(l,k).voltage = [];
        app.actBattery.qOCV_DCH(l,k).SOC = [];
        app.actBattery.qOCV_DCH(l,k).voltage = [];
    end
end

if app.onButton_temperature_depencendy.Value
    if qOCV_counter == length(list)
        for i=1:length(list)
            cd(fullfile(path,list(i).name))
            qOCV_List = dir('*qOCV-File*');
            ocv_file = load(qOCV_List.name);
            ocv_file_fieldnames = fieldnames(ocv_file);
            OCV_array = ocv_file.(ocv_file_fieldnames{1});
            [row,col] = size(OCV_array);
            for n = 1:row
                if strcmp(OCV_array{n,3},'CHA')
                    OCV_array{n,3} = 'CH';
                end
                if max(OCV_array{n,1}.SOC) < 10
                    OCV_array{n,1}.SOC = OCV_array{n,1}.SOC * 100;
                end
            end
            sSOH = '100';
            tSOH = round(str2double(sSOH));
            iSOH = find(app.SOH==tSOH);

            for i1=1:size(OCV_array,1)
                index = find(app.Temp==OCV_array{i1,2});
                if isempty(index)
                    warndlg(sprintf('F�r %i�C wurden keine EIS Daten gefunden, diese OCV wird ignoriert.',OCV_array{i1,2}));
                else
                    if strcmp(OCV_array{i1,3},'CH')
%                         app.actBattery = app.actBattery.addqOCV_CH(OCV_array{i1,1},index,iSOH);
                        app.actBattery.qOCV_CH(index,iSOH) = OCV_array{i1,1};
                    else
                        if strcmp(OCV_array{i1,3},'DCH')
%                             app.actBattery = app.actBattery.addqOCV_DCH(OCV_array{i1,1},index,iSOH);
                            app.actBattery.qOCV_DCH(index,iSOH) = OCV_array{i1,1};
                        else
                            warndlg(sprintf('F�r %i�C wurde kein g�ltiger Identifier gefunden \n G�ltige Identifier sind CH und DCH',OCV_array{i1,2}));
                        end
                    end
                end       
            end
        end
    else
        cd(old_path)
        app = fcn_SF_GeneriereOCVausRelaxDatenMenuSelected(app, event);
    end
else
    OCV_read = 0;
    for reference_idx = 1:length(list)
        if OCV_read
           continue 
        end
        reference_temperature = list(reference_idx).name;
        reference_temperature = reference_temperature(1:end -4 );
        if contains(reference_temperature,'m')
            reference_temperature =strcat('-',reference_temperature(2:end));
        end
        if strcmp(num2str(app.referencetemperatureEditField.Value),reference_temperature)       
            cd(fullfile(path,list(reference_idx).name))
            qOCV_List = dir('*qOCV-File*');
            if ~isempty(qOCV_List)
                ocv_file = load(qOCV_List.name);
                ocv_file_fieldnames = fieldnames(ocv_file);
                OCV_array = ocv_file.(ocv_file_fieldnames{1});
                [row,col] = size(OCV_array);
                for n = 1:row
                    if strcmp(OCV_array{n,3},'CHA')
                        OCV_array{n,3} = 'CH';
                    end
                    if max(OCV_array{n,1}.SOC) < 10
                        OCV_array{n,1}.SOC = OCV_array{n,1}.SOC * 100;
                    end
                end
                sSOH = '100';
                tSOH = round(str2double(sSOH));
                iSOH = find(app.SOH==tSOH);
                for i1=1:size(OCV_array,1)
                    if OCV_array{i1,2} == app.referencetemperatureEditField.Value
                        for i=1:length(list) 
                            if strcmp(OCV_array{i1,3},'CH')
    %                                     app.actBattery = app.actBattery.addqOCV_CH(OCV_array{i1,1},index,iSOH);
                                app.actBattery.qOCV_CH(i,iSOH) = OCV_array{i1,1};
                            else
                                if strcmp(OCV_array{i1,3},'DCH')
    %                                         app.actBattery = app.actBattery.addqOCV_DCH(OCV_array{i1,1},index,iSOH);
                                    app.actBattery.qOCV_DCH(i,iSOH) = OCV_array{i1,1};
                                else
                                    warndlg(sprintf('F�r %i�C wurde kein g�ltiger Identifier gefunden \n G�ltige Identifier sind CH und DCH',OCV_array{i1,2}));
                                end
                            end
                        end
                        OCV_read = 1;
                    end                    
                end
            else
                cd(old_path)
                app = fcn_SF_GeneriereOCVausRelaxDatenMenuSelected(app, event);
            end
        end
    end
end
% else
%     cd(old_path)
%     fcn_SF_GeneriereOCVausRelaxDatenMenuSelected(app, event)
% end
cd(old_path)
end

