function [ ] = initialize_plots( app )
%INITIALIZE_PLOTS Summary of this function goes here
%   Detailed explanation goes here
%   Initialisiert alle Plots der GUI

% Nyquist Plot
ax = app.SF_nyquist;

ax.XLabel.Interpreter = 'latex';
ax.XLabel.String = '$\Re\{\underline Z\}$ in $\Omega$';
ax.YLabel.Interpreter = 'latex';
ax.YLabel.String = '$\Im\{\underline Z\}$ in $\Omega$';

%Relax Plot
ax = app.SF_relax;

ax.XLabel.Interpreter = 'latex';
ax.XLabel.String = 'Zeit / sec';
ax.YLabel.Interpreter = 'latex';
ax.YLabel.String = 'Spannung / V';

%qOCV Plot
ax = app.SF_qOCV;

ax.XLabel.Interpreter = 'latex';
ax.XLabel.String = 'SOC / \%';
ax.YLabel.Interpreter = 'latex';
ax.YLabel.String = 'qOCV / V';

%Parameter Plot
ax = app.SF_Parameter;

ax.XLabel.Interpreter = 'latex';
ax.XLabel.String = 'SOC / \%';
ax.YLabel.Interpreter = 'latex';
ax.YLabel.String = '';
ax.Title.String = app.SF_ParameterListBox.Value;

end

