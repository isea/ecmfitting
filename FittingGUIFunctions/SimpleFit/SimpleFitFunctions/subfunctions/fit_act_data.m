function [ ] = fit_act_data( app )
%FIT_ACT_DATA Summary of this function goes here
%   Detailed explanation goes here

%% Lade Formel und EIS Daten
formula = app.fitProperties.aktuell_Modell.Rechnen_Modell;
m_w = app.actBattery.EISdata(app.actSOC,app.actTemp,app.actSOH).frequenz;
m_real = app.actBattery.EISdata(app.actSOC,app.actTemp,app.actSOH).Zreal;
m_imag = app.actBattery.EISdata(app.actSOC,app.actTemp,app.actSOH).Zimg;

try
    Zreal_clean = app.actBattery.EISdata_clean(app.actSOC,app.actTemp,app.actSOH).Zreal;
    Zimg_clean = app.actBattery.EISdata_clean(app.actSOC,app.actTemp,app.actSOH).Zimg;
    w_clean = app.actBattery.EISdata_clean(app.actSOC,app.actTemp,app.actSOH).frequenz;
    if ~isempty(Zreal_clean)
        m_real = Zreal_clean;
        m_imag = Zimg_clean;
        m_w = w_clean;
    end
catch 
end

%% Lade Parameter
app.last_fit = app.fit;
p_init = app.fit.Parameter;
p_min = app.fit.Parameter_min;
p_max = app.fit.Parameter_max;
p_fix = app.fit.ParFix;

%% Setze gefixte Parameter
for i=1:length(p_fix)
   if p_fix(i) == 1
       p_min(i) = p_init(i);
       p_max(i) = p_init(i);
   end
end

%% fit
options = optimset('MaxIter',20000,'MaxFunEvals',20000,'TolX',1e-8,'TolFun',1e-8,'display','final');
[p_best,fval,exitflag,output]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min, p_max ,options, formula);

app.fit.Parameter = p_best;
%% Ausgabe in Tabelle
TableData = app.SF_UITable.Data;

for i=1:length(p_best)
    TableData{i,3} = p_best(i);
    if p_best(i) <= app.fit.Parameter_min(i)*1.001 || p_best(i) >= app.fit.Parameter_max(i)*0.999
        TableData{i,6} = true;
    else
        TableData{i,6} = false;
    end
end

app.SF_UITable.Data = TableData;
p = p_best;
w = m_w;
%% Berechnung der gefitteten Kurve
f_real = real(eval(formula));
f_imag = imag(eval(formula));

%% Residuum
distances=zeros(1,length(m_real));
for m=1:length(m_real)
    distances(m)=sqrt((m_real(m)-f_real(m))^2+(m_imag(m)-f_imag(m))^2);
end
resid=sum(distances)/length(distances);
app.SF_Residuum.Text = num2str(resid);

app.fit.Residuum = resid;
app.fit.Zreal = f_real;
app.fit.Zimag = f_imag;

fit = Fit();
%% Speichern in Battery Klasse
fit.Parameter = app.fit.Parameter;
fit.Parameter_min = app.fit.Parameter_min;
fit.Parameter_max = app.fit.Parameter_max;
fit.ParFix = app.fit.ParFix;
fit.Zreal = app.fit.Zreal;
fit.Zimag = app.fit.Zimag;
fit.Residuum = app.fit.Residuum;



app.actBattery = app.actBattery.addFit(fit,app.actSOC,app.actTemp,app.actSOH);

end

