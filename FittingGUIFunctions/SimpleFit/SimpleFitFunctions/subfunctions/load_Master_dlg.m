function [] = load_Master_dlg(app)
%LOAD_MASTER_DLG Summary of this function goes here
%   Detailed explanation goes here
current_path = pwd;

[filename{1}, pathname] = uigetfile({'*.mat' 'Digatron Daten';}, 'W�hle die Mastermessung aus:', 'MultiSelect','off');
% app.SimpleFitUIFigure.Visible = 'off';
% app.SimpleFitUIFigure.Visible = 'on';

if ~isequal(filename{1},0) %Ist �berhaupt eine Datei ausgew�hlt?
    ProcedureName = find_ProcedureName(filename,app.FileFormat);%Existsiert eine Auswerteklasse?
    try
        auswertung = eval([ProcedureName{1} '()']);
    catch
        errordlg('Keine passende Auswerteklasse vorhanden');
    end
    app.SF_TestName.Text = ProcedureName{1};
    %% Wurde schon eine Batterie angelegt?
    if ~isa(app.actBattery,'Battery')
        app.actBattery = Battery('');
    end
    
    BatNames = find_BatName(filename,app.FileFormat);
    % Pr�fen ob Batteriename in Datei der aktuellen entspricht
    if ~strcmp(BatNames{1},app.actBattery.name)
        choice = questdlg({'Der Batteriename und der im File gefundene stimmen nicht �berein',['Filename: ',BatNames{1}],['aktueller Name: ',app.actBattery.name]},...
            '','Aus Datei �bernehmen','Nicht �bernehmen','Messung nicht nutzen','Aus Datei �bernehmen');
        switch choice
            case 'Aus Datei �bernehmen'
                app.actBattery.name = BatNames{1};
                app.BatName.Text = BatNames{1};
            case 'Messung nicht nutzen'
                return;
            case 'Nicht �bernehmen'
        end
    end
    
    %% SOH einlesen
    sSOH = inputdlg('Bitte SOH eingeben:','Neuer SOH');
    if isempty(sSOH{1,1})
        msgbox('Der SOH darf nicht Leer sein.','','error');
        return;
    else
        tSOH = round(str2double(sSOH));
        if isempty(app.SOH)
            app.SOH = tSOH;
            app.actSOH = 1;
        else
            iSOH = find(app.SOH==tSOH);
            if iSOH
                app.actSOH = iSOH;
            else
                app.SOH = [app.SOH,tSOH];
                app.actSOH = length(app.SOH);
            end
        end
    end
    
    %% Datei laden
    load(strcat(pathname,filename{1}));
    diga.daten.Schritt=typecast(diga.daten.Schritt,'int64');
    diga.daten.Prozedurebene=typecast(diga.daten.Prozedurebene,'int64');
    %% Temperatur aus Master File generieren
    temp = auswertung.getTemp(diga);
    if isempty(app.Temp)
        app.Temp = temp;
        app.actTemp = 1;
    else
        iTemp = find(app.Temp==temp);
        if iTemp
            app.actTemp = iTemp;
        else
            app.Temp = [app.Temp,temp];
            app.actTemp = length(app.Temp);
        end
    end
    
    %% SOCs aus Master File einlesen
    SOCanswer=questdlg('Sollen SOC-Daten generiert werden?','SOC_Daten','Ja','Nein','Abbrechen','Abbrechen');
    oldSOC=app.SOC;
    SOC = auswertung.getSOCs(diga);
    i_newData = zeros(1,length(SOC));
    if isempty(app.SOC)
        app.SOC = SOC;
        app.actSOC = 1;
        i_newData = [1:length(SOC)];
    else
        % Einsortieren weiterer Messungen in aktuelles SOC Array, falls neue SOCs
        % auftreten, wird hinten angehangen
        for i=1:length(SOC)
            iSOC = find(app.SOC==SOC(i));
            if ~isempty(iSOC)
                %SOC ist schon vorhanden
                i_newData(i) = iSOC;
            else
                %SOC ist noch nicht vorhanden, hinten anh�ngen
                app.SOC(end+1) = SOC(i);
                i_newData(i) = length(app.SOC);
            end
        end
    end
    
    %% Auswertungsname speichern
    app.actBattery = app.actBattery.setAuswertung(auswertung.name,app.actSOH);
    
    %% Kapazit�t auslesen
    capacity = auswertung.getCapacity(diga);
    app.SF_NomCap.Text = [num2str(capacity) ' Ah'];
    app.actBattery = app.actBattery.addCapacity(capacity,app.actTemp,app.actSOH);
    
    %% Aktualisieren der Index Vektoren
    app.actBattery = app.actBattery.addSOCtable(oldSOC);
    app.actBattery = app.actBattery.addTemptable(app.Temp);
    app.actBattery = app.actBattery.addSOHtable(app.SOH);
    
    %% Relax auslesen
    for i=1:length(SOC)
        relax = auswertung.getRelax(diga,i);
        app.actBattery = app.actBattery.addRelaxdata(relax,i_newData(i),app.actTemp,app.actSOH);
    end
    
    %% qOCVs auslesen
    [qOCV_CH, qOCV_DCH] = auswertung.getqOCV(diga);
    app.actBattery = app.actBattery.addqOCV_CH(qOCV_CH,app.actTemp,app.actSOH);
    app.actBattery = app.actBattery.addqOCV_DCH(qOCV_DCH,app.actTemp,app.actSOH);
    %% Pulse auslesen
    for i=1:length(SOC)
        puls = auswertung.getPuls(diga,i);
        if ~isempty(puls)
            app.pulse(i_newData(i),app.actTemp,app.actSOH) = puls;
        end
    end
    
    %% EIS Daten einlesen
    %Option EisDaten  einzulesen
    EISanswer=questdlg('Sollen EIS-Daten aus dem Ordner geladen werden?','EIS_Daten','Ja','Nein','Abbrechen','Abbrechen');
    if strcmp(EISanswer,'Ja')
        cd (pathname);
        list = dir('*EIS0*.mat'); %% Finde alle EIS Files im Ordner
        cd (current_path);
        if length(SOC) > length(list)
            error('Die Anzahl der EIS Dateien stimmt nicht mit der Masterdatei �berein');
        else
            for i=1:length(SOC)
                load(strcat(pathname,list(i).name));
                try % Check ob auch wirklich EIS Daten in der Datei sind
                    if sum(diga.daten.ActFreq)~=0
                        if diga.daten.ActFreq(1) == 0 %Abfragen, ob zu Beginn der Daten ung�ltige Eintr�ge sind
                            start_measurement = find(diff(diga.daten.ActFreq)>0)+1; % Beginn der Messung suchen
                        else
                            start_measurement = 1;
                        end
                        
                        temp_data = EISData();
                        temp_data.frequenz = diga.daten.ActFreq(start_measurement:end);
                        temp_data.Zreal = diga.daten.Zreal1(start_measurement:end)/1000;
                        temp_data.Zimg = diga.daten.Zimg1(start_measurement:end)/1000;
                        
                        data = remove_rep_from_EIS(temp_data);
                        app.actBattery = app.actBattery.addEISdata(data,i_newData(i),app.actTemp,app.actSOH);
                    else
                        msgbox('Datei enth�lt keine EIS Messung');
                    end
                catch
                    msgbox('Datei enth�lt keine EIS Messung');
                end
            end
        end
    end
    if strcmp(SOCanswer,'Nein')
        app.SOC=oldSOC;
    end
    app.actSOC = 1;
    %% Auswahlboxen aktualisieren
    update_listboxes(app);
    
    %app.actBattery = app.actBattery.addEISdata_allSOC(data,app.actTemp,app.actSOH);
    
    %% Aktualisieren der Plots
    try
        plot_nyquist(app);
        plot_datapoints(app);
    catch
    end
    plot_relax(app);
    plot_qOCV(app);
end
end

