function [app ] = change_act_model( app, index )
%CHANGE_ACT_MODEL Summary of this function goes here
%   Detailed explanation goes here

clear app.fit;
clear app.last_fit;
clear app.fitProperties;
addpath('FittingGUIFunctions/SimpleFit/SimpleFitFunctions/SF_ESB');

aktModellName = app.Modellliste{index};

aktModell = eval(['SF_',aktModellName,'();']);
%insert parameters of the one-state model for load history dependency
if app.LHDCheckBox_2.Value
    aktModell.elemente{end+1} = SF_ESBe_TimeConstant();
    aktModell.ParameterNamen{end+1} = 'P1_LHD';
    aktModell.ParameterNamen{end+1} = 'Tau1_LHD';
    aktModell.ParameterNamen{end+1} = 'P2_LHD';
    aktModell.ParameterNamen{end+1} = 'Tau2_LHD';
    aktModell.Appendix{end+1} = '_LHD';
    aktModell.ParameterMin(end+1:end+4) = -Inf *ones(1,4);
    aktModell.ParameterMax(end+1:end+4) = Inf *ones(1,4);
    aktModell.ParameterFix(end+1:end+4) = zeros(1,4);
    aktModell.ParameterInit(end+1:end+4) = zeros(1,4);
    aktModell.model{1, 4}{end+1} = 'P1_LHD';
    aktModell.model{1, 4}{end+1} = 'Tau1_LHD';
    aktModell.model{1, 4}{end+1} = 'P2_LHD';
    aktModell.model{1, 4}{end+1} = 'Tau2_LHD';
    aktModell.model{1, 5}(end+1:end+4) = zeros(1,4);
    aktModell.model{1, 6}(end+1:end+4) = -Inf *ones(1,4);
    aktModell.model{1, 7}(end+1:end+4) = Inf *ones(1,4);
    aktModell.model{1, 8}(end+1:end+4) = zeros(1,4);
end

app.fitProperties.aktuell_Modell.Modellname = aktModell.getModelName();  % name
app.fitProperties.aktuell_Modell.Rechnen_Modell = aktModell.getRechenFormula;  % Formel
app.fitProperties.aktuell_Modell.Modell = aktModell;
parNames = aktModell.getParameterNames(); %Parameter Namen
[app.fit.Parameter, app.fit.Parameter_min, app.fit.Parameter_max, app.fit.ParFix] = aktModell.getParameters(); %Parameter Grenzen

TableCell(:,1) = transpose(parNames);
TableCell(:,3) = num2cell(transpose(app.fit.Parameter));
TableCell(:,4) = num2cell(transpose(app.fit.Parameter_min));
TableCell(:,5) = num2cell(transpose(app.fit.Parameter_max));

for i=1:length(parNames)
   TableCell{i,2} = logical(app.fit.ParFix(i));
   TableCell{i,6} = false;
end

app.SF_UITable.Data = TableCell;
% app.SF_ParameterListBox.Items = parNames;

app.fitProperties.aktuell_Modell.Rechnen_Modell = strrep(app.fitProperties.aktuell_Modell.Rechnen_Modell,'HF','1');
app.fitProperties.aktuell_Modell.Rechnen_Modell = strrep(app.fitProperties.aktuell_Modell.Rechnen_Modell,'LF','1');
app.fitProperties.aktuell_Modell.Rechnen_Modell = strrep(app.fitProperties.aktuell_Modell.Rechnen_Modell,'MF','1');

app.actBattery = app.actBattery.setModel(app.fitProperties.aktuell_Modell.Modellname);
end

