function [] = save_plots(app)
%PLOT_PARAMETER Summary of this function goes here
%   Detailed explanation goes here

savefolder = uigetdir([],'Path to save figures');
list = {'fig','m','jpg','png','eps','pdf','bmp','emf','pbm','pcx','pgm','ppm','tif'};
[indx,tf] = listdlg('PromptString','Select figure format:','ListString',list);


ParameterNamen = app.fitProperties.aktuell_Modell.Modell.ParameterNamen;
ParameterNamenLatex =  ParameterNamen;
for l =1:length(ParameterNamenLatex)
    ind_temp = [];
    ind_temp = strfind(ParameterNamenLatex{l},'_');
    if ~isempty(ind_temp)
        ParameterNamenLatex{l}(ind_temp(2:end)) = ',';
        ParameterNamenLatex{l} = insertAfter(ParameterNamenLatex{l},ind_temp(1),'{');
        ParameterNamenLatex{l} = [ParameterNamenLatex{l},'}'];
    end
end

temp = app.actBattery.temp;
SOC = app.actBattery.SOC;
for l =1:length(ParameterNamenLatex)
    figure('Name',ParameterNamenLatex{l},'visible', 'off')
    ax=axes();
    ax.YLimMode = 'auto';

    for k = 1:length(temp)
        clear parameter
        clear SOCclean
        j=1;
        for i=1:length(SOC)
            try
                parameter(j) = app.actBattery.fit(i,k,app.actSOH).Parameter(l);
                SOCclean(j) = SOC(i);
                j=j+1;
            catch
            end
        end
        plot(ax,SOCclean,parameter,'-o','LineWidth',2,'DisplayName',['$',num2str(temp(k)),'^\circ C$']);
        hold on
    end

    ax.XLabel.Interpreter = 'latex';
    ax.XLabel.String = '$ SOC/\% $';
    ax.YLabel.Interpreter = 'latex';
    ax.YLabel.String = '';
    ax.Title.String = ParameterNamenLatex{l};
    switch ax.Title.String(1)
        case 'R'
           ax.YLabel.String = ['$' ParameterNamenLatex{l} ' / \Omega$']; 
        case 'L'
           ax.YLabel.String = ['$' ParameterNamenLatex{l} ' / H$'];  
        case 'C'    
           ax.YLabel.String = ['$' ParameterNamenLatex{l} ' / F$']; 
        case 'T'
           ax.YLabel.String = ['$' ParameterNamenLatex{l} ' / sec$']; 
        case 'P'
           ax.YLabel.String = ['$' ParameterNamenLatex{l} ' / ^{\circ}$'];
    end
    leg =legend;
    set(leg,'Interpreter','latex');

for i = 1:length(indx)
    saveas(gcf,[savefolder,'/',ParameterNamen{l}],list(indx(i)))
end

end

