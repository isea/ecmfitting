function [app] = sort_crate(app)
%SORT_TEMP Summary of this function goes here
%   Detailed explanation goes here
[app.actBattery.Crate,order] = sort(app.actBattery.Crate);
try
    app.actBattery.fit = app.actBattery.fit(:,order,:,:,:);     
    app.actBattery.lhd = app.actBattery.lhd(:,order,:,:,:);     
catch
end

try
    app.actBattery.relaxdata = app.actBattery.relaxdata(:,order,:,:,:); 
catch
end

app.actBattery.EISdata = app.actBattery.EISdata(:,order,:,:,:);
app.actBattery.capacity = app.actBattery.capacity(:,order,:,:);
app.Crate = app.actBattery.Crate;
end

