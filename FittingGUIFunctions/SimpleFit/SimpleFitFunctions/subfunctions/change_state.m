function [app] = change_state(app)
%CHANGE_STATE Summary of this function goes here
%   Detailed explanation goes here

iSOC = find(str2double(app.SF_SOCListBox.Items)==app.SOC(app.actSOC));
iTemp = find(str2double(app.SF_TemperaturListBox.Items)==app.Temp(app.actTemp));
if app.HysteresisFlag
    iHyst = find(str2double(app.SF_HystListBox.Items)==app.Hyst(app.actHyst));
end
if app.CRateFlag
    iCrate = find(str2double(app.SF_CrateListBox.Items)==app.Crate(app.actCrate));
end

if isempty(iSOC)
    iSOC = 1;
    app.actSOC = find(app.SOC ==str2double(app.SF_SOCListBox.Items(iSOC)));
end
if isempty(iTemp)
    iTemp = 1;
    app.actTemp = find(app.SOC ==str2double(app.SF_TemperaturListBox.Items(iTemp)));
end
if app.CRateFlag
    if isempty(iCrate)
        iCrate = 1;
        app.actCrate = find(app.Crate ==str2double(app.SF_CrateListBox.Items(iCrate)));
    end
else
    app.actCrate = 1;
end
if app.HysteresisFlag
    if isempty(iHyst)
        iHyst = 1;
        app.actHyst = find(app.Hyst ==str2double(app.SF_HystListBox.Items(iHyst)));
    end
else
    app.actHyst = 1;
end

app.SF_SOCListBox.Value = app.SF_SOCListBox.Items{iSOC};
app.SF_TemperaturListBox.Value = app.SF_TemperaturListBox.Items{iTemp};
app.SF_SOHListBox.Value = app.SF_SOHListBox.Items{app.actSOH};
if app.HysteresisFlag
    app.SF_HystListBox.Value = app.SF_HystListBox.Items{app.actHyst};
end
if app.CRateFlag
    app.SF_CrateListBox.Value = app.SF_CrateListBox.Items{app.actCrate};
end

if ~isempty(app.actBattery.capacity)
    if app.actBattery.nom_cap == app.actBattery.capacity(app.actHyst,app.actCrate,app.actTemp,app.actSOH)
        app.SF_NominelleKapazittCheckBox.Value = 1;
    else
        app.SF_NominelleKapazittCheckBox.Value = 0;
    end
end
end

