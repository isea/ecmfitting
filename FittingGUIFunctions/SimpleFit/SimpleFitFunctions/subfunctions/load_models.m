function [app ] = load_models( app )
%LOAD_MODELS Summary of this function goes here
%   Detailed explanation goes here
global Modellliste
warning ('off','all');
addpath('FittingGUIFunctions/SimpleFit/SimpleFitFunctions/SF_ESB')
addpath('FittingGUIFunctions/SimpleFit/SimpleFitFunctions/SF_ESBelements');

standard_modell = Modellliste.standard_modell;

app.Modellliste = {};

files = dir('FittingGUIFunctions/SimpleFit/SimpleFitFunctions/SF_ESB/SF_ESB*.m');
warning ('on','all');
for i = 1:numel(files)
    app.Modellliste{i} = files(i).name(4:end-2);
    Modelliste_Listbox{i} = files(i).name(8:end-2);
end

app.SF_ModellauswahlDropDown.Items = Modelliste_Listbox;
app.SF_ModellauswahlDropDown.Value = Modelliste_Listbox{standard_modell};
app = change_act_model(app,standard_modell);

end

