function XML_struct = generate_root_element(app,XML_struct,isModul,isSpread)
%GENERATE_ROOT_ELEMENT Summary of this function goes here
%   Detailed explanation goes here
%   isModul: Definiere ob Modul oder einzelnde Zelle simuliert werden soll
%   isSpread:   Definiert ob Streuung berücksichtigt wird

if ~isModul
    %create single Cell root element
    %Root element
    XML_struct.Configuration.RootElement.Attributes.class = 'SerialTwoPort';
    XML_struct.Configuration.RootElement.Children.Attributes.count = '1';
    XML_struct.Configuration.RootElement.Children.Pack.Text ='';
    XML_struct.Configuration.RootElement.Children.Pack.Attributes.ref = app.electrical_models{app.actEleModelindex,2};
else

    if ~isSpread
        XML_struct.Configuration.RootElement.Attributes.class = 'SerialTwoPort';
        XML_struct.Configuration.RootElement.Children.Attributes.count = num2str(app.SerielleZellenEditField.Value);
        XML_struct.Configuration.RootElement.Children.Pack.Text ='';
        XML_struct.Configuration.RootElement.Children.Pack.Attributes.ref = 'MyParallelString';
        if app.WiderstandSerienverschaltungEditField.Value ~= 0
            XML_struct.Configuration.RootElement.Children.ResistanceWiring.Text ='';
            XML_struct.Configuration.RootElement.Children.ResistanceWiring.Attributes.ref = 'MyResistanceWiring';
        end
    else
        XML_struct.Configuration.RootElement.Attributes.class = 'SerialTwoPort';
        XML_struct.Configuration.RootElement.Children.Attributes.count = '1';
        for i=1:app.SerielleZellenEditField.Value
            string_name = ['MyString' num2str(i)];
            ref_name = ['MyParallelString' num2str(i)];
            XML_struct.Configuration.RootElement.Children.(string_name).Text ='';
            XML_struct.Configuration.RootElement.Children.(string_name).Attributes.ref = ref_name;
        end
        if app.WiderstandSerienverschaltungEditField.Value ~= 0
            XML_struct.Configuration.RootElement.Children.ResistanceWiring.Text ='';
            XML_struct.Configuration.RootElement.Children.ResistanceWiring.Attributes.ref = 'MyResistanceWiring';
        end
    end
end

end

