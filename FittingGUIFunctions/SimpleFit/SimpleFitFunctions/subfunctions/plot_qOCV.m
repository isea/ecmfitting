function [] = plot_qOCV(app)
%PLOT_RELAX Summary of this function goes here
%   Detailed explanation goes here

qOCV_CH.voltage = app.actBattery.qOCV_CH(app.actTemp,app.actSOH).voltage;
qOCV_CH.SOC = app.actBattery.qOCV_CH(app.actTemp,app.actSOH).SOC;
qOCV_DCH.voltage = app.actBattery.qOCV_DCH(app.actTemp,app.actSOH).voltage;
qOCV_DCH.SOC = app.actBattery.qOCV_DCH(app.actTemp,app.actSOH).SOC;

if ~isempty(qOCV_CH.voltage)
   hold(app.SF_qOCV, 'off');
   plot(app.SF_qOCV,qOCV_CH.SOC,qOCV_CH.voltage,'DisplayName','Charge','LineWidth',2,'Color',app.Colors.Blau);
   hold(app.SF_qOCV, 'on');
   h1 = legend(app.SF_qOCV,'show');
   set(h1,'Interpreter','none','Location','NorthWest');
end


if ~isempty(qOCV_DCH.voltage) 
   plot(app.SF_qOCV,qOCV_DCH.SOC,qOCV_DCH.voltage,'DisplayName','Discharge','LineWidth',2,'Color',app.Colors.Rot);
   h1 = legend(app.SF_qOCV,'show');
   set(h1,'Interpreter','none','Location','NorthWest');
   hold(app.SF_qOCV, 'off');
end
end

