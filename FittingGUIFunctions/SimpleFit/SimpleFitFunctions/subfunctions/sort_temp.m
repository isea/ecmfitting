function [app] = sort_temp(app)
%SORT_TEMP Summary of this function goes here
%   Detailed explanation goes here
[app.actBattery.temp,order] = sort(app.actBattery.temp);
try
    app.actBattery.fit = app.actBattery.fit(:,:,:,order,:);  
    app.actBattery.lhd = app.actBattery.lhd(:,:,:,order,:);  
catch
end

try
    app.actBattery.relaxdata = app.actBattery.relaxdata(:,:,:,order,:); 
catch
end

try
    app.actBattery.qOCV_CH = app.actBattery.qOCV_CH(order,:);
catch
end

try
    app.actBattery.qOCV_DCH = app.actBattery.qOCV_DCH(order,:);
catch
end

app.actBattery.EISdata = app.actBattery.EISdata(:,:,:,order,:);
app.actBattery.capacity = app.actBattery.capacity(:,:,order,:);
app.Temp = app.actBattery.temp;
end

