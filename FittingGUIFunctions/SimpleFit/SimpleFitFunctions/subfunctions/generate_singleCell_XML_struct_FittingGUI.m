function XML_struct = generate_singleCell_XML_struct_FittingGUI(app, XML_struct,Hyst,Crate)
%GENERATEXML Summary of this function goes here
%   Detailed explanation goes here
old_path = cd;
path = [old_path '/utils/'];
cd(path)
if ~app.ImportThermalModelCheckBox.Value
    if app.CylindricalButton.Value
        load('thermal_model_cylindrical.mat');
    elseif app.PrismaticButton.Value
        load('thermal_model_prismatic.mat');
    else
        load('thermal_model_pouch.mat');
    end
else
    if app.GUI
        [thermal_file,thermal_path] = uigetfile('*.mat','Choose a thermal model');
        load([thermal_path,thermal_file]);
    else
        if app.thermalelectricalSimulation == 1
            load(app.thermal_model_path);
        else
            load('thermal_model_prismatic.mat');
        end
    end
end
cd(old_path)

cell_str = [thermalDatabaseModel{1,1}.MetaData.thermal.CellName.Text,'(',thermalDatabaseModel{1,1}.MetaData.thermal.ModelName.Text,')'];
app.thermal_models = thermalDatabaseModel;

%MetaData
XML_struct.Configuration.MetaData.electrical = app.electrical_models{1,1}.MetaData.electrical;
XML_struct.Configuration.MetaData.thermal = app.thermal_models{1,1}.MetaData.thermal;

%Thermal Properies
XML_struct.Configuration.ThermalMaterials = app.thermal_models{1,1}.ThermalMaterials;
if isfield(app.thermal_models{1,1},'CoolingBlocks')
    XML_struct.Configuration.CoolingBlocks = app.thermal_models{1,1}.CoolingBlocks;
    XML_struct.Configuration.CoolingBlocks.Text = strip(XML_struct.Configuration.CoolingBlocks.Text);
else
    XML_struct.Configuration.CoolingBlocks.Text = '';
end
if isfield(app.thermal_models{1,1},'CachedCoolings')
    XML_struct.Configuration.CachedCoolings = app.thermal_models{1,1}.CachedCoolings;
    XML_struct.Configuration.CachedCoolings.Text = strip(XML_struct.Configuration.CachedCoolings.Text);
else
    XML_struct.Configuration.CachedCoolings.Text = '';
end

%Electric Elements
elefieldnames = fieldnames(app.electrical_models{1,1}.CustomDefinitions);
eleelements = setxor(elefieldnames,app.electrical_models{1,2},'stable');

for i=1:length(eleelements)
%     if ~strcmp(eleelements{i},'MyTimeConstantLHD_LHD')
        XML_struct.Configuration.CustomDefinitions.(eleelements{i}) = app.electrical_models{1,1}.CustomDefinitions.(eleelements{i});
%     end
end

%Thermal Elements
thermalfieldnames = fieldnames(app.thermal_models{1,1}.CustomDefinitions);

for i=1:length(thermalfieldnames)
    XML_struct.Configuration.CustomDefinitions.(thermalfieldnames{i}) = app.thermal_models{1,1}.CustomDefinitions.(thermalfieldnames{i});
end

%Add Cell Element
XML_struct.Configuration.CustomDefinitions.(app.electrical_models{1,2}) = app.electrical_models{1,1}.CustomDefinitions.(app.electrical_models{1,2});
if isfield(XML_struct.Configuration.CustomDefinitions.(app.electrical_models{1,2}).Children,'TimeConstantLHD')
    XML_struct.Configuration.CustomDefinitions.(app.electrical_models{1,2}).Children = rmfield(XML_struct.Configuration.CustomDefinitions.(app.electrical_models{1,2}).Children,'TimeConstantLHD');
end

%Add intial SOC to electric model
XML_struct.Configuration.CustomDefinitions.(app.electrical_models{1,2}).Soc.InitialSoc.Text = '100';
%Add capacity to electric model
XML_struct.Configuration.CustomDefinitions.(app.electrical_models{1,2}).Soc.InitialCapacity.Text = num2str(app.actBattery.nom_cap);

%Add initial Temperature to electric model
XML_struct.Configuration.CustomDefinitions.(app.electrical_models{1,2}).ThermalState.InitialTemperature.Text = '25';
XML_struct.Configuration.CustomDefinitions.(app.electrical_models{1,2}).ThermalState.Attributes.cache = 'True';
XML_struct.Configuration.CustomDefinitions.(app.electrical_models{1,2}).ThermalState.Attributes.class = 'ThermalState';

%Add C Rate or Hyst States
if length(Crate) == 1 && length(Hyst) > 1 
    XML_struct.Configuration.CustomDefinitions.(app.electrical_models{1,2}).HystState.InitialHystState.Text = '0';
    XML_struct.Configuration.CustomDefinitions.(app.electrical_models{1,2}).HystState.Attributes.cache = 'True';
    XML_struct.Configuration.CustomDefinitions.(app.electrical_models{1,2}).HystState.Attributes.class = 'HystState';
elseif length(Crate) > 1 && length(Hyst) == 1 
    XML_struct.Configuration.CustomDefinitions.(app.electrical_models{1,2}).CRateState.Attributes.cache = 'True';
    XML_struct.Configuration.CustomDefinitions.(app.electrical_models{1,2}).CRateState.Attributes.class = 'CRateState';
end

if app.LHDCheckBox_2.Value
    XML_struct.Configuration.CustomDefinitions.(app.electrical_models{1,2}).LHDState.Attributes.cache = 'True';
    XML_struct.Configuration.CustomDefinitions.(app.electrical_models{1,2}).LHDState.Attributes.class = 'LHDState';
    XML_struct.Configuration.CustomDefinitions.(app.electrical_models{1,2}).LHDState.TimeConstantLHD.text = '';
    XML_struct.Configuration.CustomDefinitions.(app.electrical_models{1,2}).LHDState.TimeConstantLHD.Attributes.ref = 'MyTimeConstantLHD_LHD';
    XML_struct.Configuration.CustomDefinitions.(app.electrical_models{1,2}).LHDState.SampleRate.Text = '1000';
end

%Add thermal Block
XML_struct.Configuration.CustomDefinitions.(app.electrical_models{1,2}).ThermalBlock.Text = '';
XML_struct.Configuration.CustomDefinitions.(app.electrical_models{1,2}).ThermalBlock.Attributes.ref = [];

end

