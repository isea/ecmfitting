
function [BatNames] = find_BatName(filename,FileFormat)
%FIND_BATNAME Summary of this function goes here
%   Detailed explanation goes here
[names,values] = parse_filename(filename,FileFormat);

index = find(strcmp(names,FileFormat.BatName));
BatNames = values(:,index);

end

