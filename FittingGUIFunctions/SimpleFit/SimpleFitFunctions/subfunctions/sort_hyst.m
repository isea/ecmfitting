function [app] = sort_hyst(app)
%SORT_TEMP Summary of this function goes here
%   Detailed explanation goes here
[app.actBattery.Hyst,order] = sort(app.actBattery.Hyst);
try
    app.actBattery.fit = app.actBattery.fit(order,:,:,:,:);    
    app.actBattery.lhd = app.actBattery.lhd(order,:,:,:,:); 
catch
end

try
    app.actBattery.relaxdata = app.actBattery.relaxdata(order,:,:,:,:); 
catch
end

app.actBattery.EISdata = app.actBattery.EISdata(order,:,:,:,:);
app.actBattery.capacity = app.actBattery.capacity(order,:,:,:);
app.Hyst = app.actBattery.Hyst;
% app.sorted_hyst_states = app.sorted_hyst_states(order);
end

