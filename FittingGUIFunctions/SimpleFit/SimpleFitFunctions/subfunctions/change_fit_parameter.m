function [ ] = change_fit_parameter( app, event )
%CHANGE_FIT_PARAMETER Summary of this function goes here
%   Detailed explanation goes here

switch event.Indices(2)
    
    case 2
        app.fit.ParFix(event.Indices(1)) = event.NewData;
    case 3
        app.fit.Parameter(event.Indices(1)) = event.NewData;
        formula = app.fitProperties.aktuell_Modell.Rechnen_Modell;
        w = app.actBattery.EISdata(app.actSOC,app.actTemp,app.actSOH).frequenz;
        m_real = app.actBattery.EISdata(app.actSOC,app.actTemp,app.actSOH).Zreal;
        m_imag = app.actBattery.EISdata(app.actSOC,app.actTemp,app.actSOH).Zimg; 
        %% Lade Parameter
        app.last_fit = app.fit;
        p = app.fit.Parameter;
        %% Berechnung der gefitteten Kurve
        f_real = real(eval(formula));
        f_imag = imag(eval(formula));
        %% Residuum
        distances=zeros(1,length(m_real));
        for m=1:length(m_real)
            distances(m)=sqrt((m_real(m)-f_real(m))^2+(m_imag(m)-f_imag(m))^2);
        end
        resid=sum(distances)/length(distances);
        app.SF_Residuum.Text = num2str(resid);
        
        app.fit.Residuum = resid;
        app.fit.Zreal = f_real;
        app.fit.Zimag = f_imag;
        
        fit = Fit();
        %% Speichern in Battery Klasse
        fit.Parameter = app.fit.Parameter;
        fit.Parameter_min = app.fit.Parameter_min;
        fit.Parameter_max = app.fit.Parameter_max;
        fit.ParFix = app.fit.ParFix;
        fit.Zreal = app.fit.Zreal;
        fit.Zimag = app.fit.Zimag;
        fit.Residuum = app.fit.Residuum;
        
        app.actBattery = app.actBattery.addFit(fit,app.actSOC,app.actTemp,app.actSOH);
        plot_nyquist(app);
    case 4
        app.fit.Parameter_min(event.Indices(1)) = event.NewData;
    case 5
        app.fit.Parameter_max(event.Indices(1)) = event.NewData;
end
end

