function [app] = sort_SOC(app)
%SORT_SOC Summary of this function goes here
%   Detailed explanation goes here
[app.actBattery.SOC,order] = sort(app.actBattery.SOC);
try
    app.actBattery.fit = app.actBattery.fit(:,:,order,:,:);
    app.actBattery.lhd = app.actBattery.lhd(:,:,order,:,:);
catch
end
try
    app.actBattery.relaxdata = app.actBattery.relaxdata(:,:,order,:,:);
catch
end

app.actBattery.EISdata = app.actBattery.EISdata(:,:,order,:,:);

app.SOC = app.actBattery.SOC;
end

