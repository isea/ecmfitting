function [app] = update_listboxes(app)
%UPDATE_LISTBOXES Summary of this function goes here
%   Detailed explanation goes here
if app.HysteresisFlag
    j=1;
    Hyst = [];
    app.actBattery.Hyst = app.Hyst;
    for k=1:length(app.actBattery.SOC)
        for i=1:length(app.actBattery.temp)
            for n = 1:length(app.actBattery.Crate)
                for m= 1:length(app.actBattery.Hyst)
                    if ~isempty(app.actBattery.EISdata(m,n,k,i,app.actSOH).Zreal)
                        if isempty(find(Hyst==app.actBattery.Hyst(m)))
                            Hyst(j) = app.actBattery.Hyst(m);
                            j =j+1;
                        end
                    end
                end            
            end
        end
    end
    Hyst = sort(Hyst);
    if isempty(find(Hyst==app.Hyst(app.actHyst)))
       app.actHyst = find(app.Hyst==Hyst(1)); 
    end
else
    app.actHyst = 1;
end

if app.CRateFlag
    j=1;
    Crate = [];
    app.actBattery.Crate = app.Crate;
    for k=1:length(app.actBattery.SOC)
        for i=1:length(app.actBattery.temp)
            for n = 1:length(app.actBattery.Crate)
                if ~isempty(app.actBattery.EISdata(app.actHyst,n,k,i,app.actSOH).Zreal)
                    if isempty(find(Crate==app.actBattery.Crate(n)))
                        Crate(j) = app.actBattery.Crate(n);
                        j =j+1;
                    end
                end                       
            end
        end
    end
    Crate = sort(Crate);
    if isempty(find(Crate==app.Crate(app.actCrate)))
       app.actCrate = find(app.Crate==Crate(1)); 
    end
else
    app.actCrate = 1;
end

j=1;
Temp = [];
app.actBattery.temp = app.Temp;
for k=1:length(app.actBattery.SOC)
    for i=1:length(app.actBattery.temp)
        if ~isempty(app.actBattery.EISdata(app.actHyst,app.actCrate,k,i,app.actSOH).Zreal)
            if isempty(find(Temp==app.actBattery.temp(i)))
                Temp(j) = app.actBattery.temp(i);
                j =j+1;
            end
            
        end
    end
end
Temp = sort(Temp);
if isempty(find(Temp==app.Temp(app.actTemp)))
   app.actTemp = find(app.Temp==Temp(1)); 
end

j=1;
for i=1:length(app.actBattery.SOC)
   if ~isempty(app.actBattery.EISdata(app.actHyst,app.actCrate,i,app.actTemp,app.actSOH).Zreal)
     SOC(j) = app.actBattery.SOC(i);
     j =j+1;
   end
end



%%�bersicht
app.SF_SOCListBox.Items = cellstr(num2str(transpose(SOC)));
app.SF_TemperaturListBox.Items = cellstr(num2str(transpose(Temp)));
app.SF_SOHListBox.Items = cellstr(num2str(transpose(app.actBattery.SOH)));
if app.CRateFlag
    app.SF_CrateListBox.Items = cellstr(num2str(transpose(app.actBattery.Crate)));
    app.SF_CrateListBox.Enable = 'on';
else
    app.SF_CrateListBox.Enable = 'off';
end
if app.HysteresisFlag
    app.SF_HystListBox.Items = cellstr(num2str(transpose(app.actBattery.Hyst)));
    app.SF_HystListBox.Enable = 'on';
else
    app.SF_HystListBox.Enable = 'off';
end

%%Parameter
% app.SF_TemperaturListBox_2.Items = cellstr(num2str(transpose(Temp)));
% app.SF_SOHListBox_2.Items = cellstr(num2str(transpose(app.actBattery.SOH)));
% TableData = cell(length(SOC),2);
% for i=1:length(SOC)
%     TableData{i,1} = num2str(SOC(i));
%     TableData{i,2} = i;
% end
% app.SF_IgnoreTable.Data = TableData;

%%Validierung
% app.SF_SOCListBox_2.Items = cellstr(num2str(transpose(SOC)));
% app.SF_TemperaturListBox_3.Items = cellstr(num2str(transpose(Temp)));
% app.SF_SOHListBox_3.Items = cellstr(num2str(transpose(app.actBattery.SOH)));

app= change_state(app);

if ~isempty(app.actBattery.capacity)
    if app.actBattery.nom_cap == app.actBattery.capacity(app.actHyst,app.actCrate,app.actTemp,app.actSOH)
        app.SF_NominelleKapazittCheckBox.Value = 1;
    else
        app.SF_NominelleKapazittCheckBox.Value = 0;
    end
end
end

