function [] = zoom_nyquist(app)
%ZOOM_NYQUIST Summary of this function goes here
%   Detailed explanation goes here
Zreal = app.actBattery.EISdata(app.actSOC,app.actTemp,app.actSOH).Zreal;
Zimg = app.actBattery.EISdata(app.actSOC,app.actTemp,app.actSOH).Zimg;

try
    Zreal_clean = app.actBattery.EISdata_clean(app.actSOC,app.actTemp,app.actSOH).Zreal;
    Zimg_clean = app.actBattery.EISdata_clean(app.actSOC,app.actTemp,app.actSOH).Zimg;
    if ~isempty(Zreal_clean)
        [Zreal_rest,ia] = setdiff(Zreal,Zreal_clean);
        Zimg_rest = Zimg(ia);
        Zreal = Zreal_clean;
        Zimg = Zimg_clean;
    end
catch 
end

figure;

if ~isempty(Zreal)
    plot(Zreal,Zimg,'o','DisplayName','Messung','LineWidth',2,'Color',app.Colors.Blau);
    set(gca,'YDir','reverse');
    if exist('Zreal_rest','var')==1
        hold 'on';
        plot(Zreal_rest,Zimg_rest,'o','DisplayName','nicht berücksichtigt',...
            'LineWidth',2,'Color',app.Colors.Grau);
    end
end
    try
        hold on;
        Zreal = app.actBattery.fit(app.actSOC,app.actTemp,app.actSOH).Zreal;
        Zimag = app.actBattery.fit(app.actSOC,app.actTemp,app.actSOH).Zimag;
        plot(Zreal,Zimag,'DisplayName','Fit','LineWidth',2,'Color',app.Colors.Rot);
    catch
    end
h1 = legend(gca,'show');

set(h1,'Interpreter','none','Location','NorthWest');
hold off;

end

