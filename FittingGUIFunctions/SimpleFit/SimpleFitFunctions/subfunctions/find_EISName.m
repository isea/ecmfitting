
function [EISNames] = find_EISName(filename,FileFormat)
%FIND_BATNAME Summary of this function goes here
%   Detailed explanation goes here
[names,values] = parse_filename(filename,FileFormat);

index = find(strcmp(names,FileFormat.EISName));
EISNames = values(:,index);

end

