function [] = load_EISmeasurement_dlg(app)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here


[filename, pathname] = uigetfile({'*.mat' 'EIS Daten';}, 'W�hle die EIS Daten aus:', 'MultiSelect','on');
app.SimpleFitUIFigure.Visible = 'off';
app.SimpleFitUIFigure.Visible = 'on';

if ~isequal(filename,0) %Ist �berhaupt eine Datei ausgew�hlt?
    
    
    %% Abfrage von Temperatur und SOH f�r alle einzuladenden SOCs
    stemp = inputdlg('Bitte Testtemperatur eingeben:','Neue Temperatur');
    if isempty(stemp{1,1})
        msgbox('Die Temperatur darf nicht Leer sein.','','error');
        return;
    else
        temp = round(str2double(stemp));
        if isempty(app.Temp)
            app.Temp = temp;
            app.actTemp = 1;
        else
            iTemp = find(app.Temp==temp);
            if iTemp
                app.actTemp = iTemp;
            else
                app.Temp = [app.Temp,temp];
                app.actTemp = length(app.Temp);
            end
        end
    end
    sSOH = inputdlg('Bitte SOH eingeben:','Neuer SOH');
    if isempty(sSOH{1,1})
        msgbox('Der SOH darf nicht Leer sein.','','error');
        return;
    else
        tSOH = round(str2double(sSOH));
        if isempty(app.SOH)
            app.SOH = tSOH;
            app.actSOH = 1;
        else
            iSOH = find(app.SOH==tSOH);
            if iSOH
                app.actSOH = iSOH;
            else
                app.SOH = [app.SOH,tSOH];
                app.actSOH = length(app.SOH);
            end
        end
    end
    
    %% Erzeugen den Liste der einzulesenden Daten
    
    
    if ~iscell(filename) %Sind es mehrere Dateien oder nur eine?
        temp = filename; %Wenn nur eine Datei Konvertierung in Cell Array
        clear filename;
        filename{1} = temp;
        clear temp;
    end
    
    BatNames = find_BatName(filename,app.FileFormat);
    EISNames = find_EISName(filename,app.FileFormat);
    
    %% Wurde schon eine Batterie angelegt?
    if ~isa(app.actBattery,'Battery');
        app.actBattery = Battery('');
    end
    
    %% Einlesen der Daten
    
    dataIndex = 1; %Wird ben�tigt um unabh�ngig von der Laufvariable zu sein
    %(Falls Batterienamen nicht stimmen und Daten verworfen werden)
    for index=1:length(filename)
        % Pr�fen ob Batteriename in Datei der aktuellen entspricht
        if ~strcmp(BatNames{index},app.actBattery.name)
            choice = questdlg({'Der Batteriename und der im File gefundene stimmen nicht �berein',['Filename: ',BatNames{index}],['aktueller Name: ',app.actBattery.name]},...
                '','Aus Datei �bernehmen','Nicht �bernehmen','Messung nicht nutzen','Aus Datei �bernehmen');
            switch choice
                case 'Aus Datei �bernehmen'
                    app.actBattery.name = BatNames{index};
                    app.SF_BatName.Text = BatNames{index};
                case 'Messung nicht nutzen'
                    continue;
                case 'Nicht �bernehmen'
            end
        end
        % Abfrage, welcher SOC zu den aktuellen Daten geh�rt
        sSOC = inputdlg(['Bitte SOC f�r ',EISNames{index},' eingeben:'],'Neuer SOC');
        if isempty(sSOC{1,1})
            msgbox('Der SOC darf nicht Leer sein.','','error');
            continue;
        else
            SOC = round(str2double(sSOC));
            if isempty(app.SOC)
                app.SOC = SOC;
                app.actSOC = 1;
            else
                iSOC = find(app.SOC==SOC);
                if iSOC
                    app.actSOC = iSOC;
                else
                    app.SOC = [app.SOC,SOC];
                    app.actSOC = length(app.SOC);
                end
            end
        end
        
        % Laden der Datei
        load(strcat(pathname,filename{index}));
        
        try % Check ob auch wirklich EIS Daten in der Datei sind
            if sum(diga.daten.ActFreq)~=0
                if diga.daten.ActFreq(1) == 0 %Abfragen, ob zu Beginn der Daten ung�ltige Eintr�ge sind
                    start_measurement = find(diff(diga.daten.ActFreq)>0)+1; % Beginn der Messung suchen
                else
                    start_measurement = 1;
                end
                temp_data = EISData();
                temp_data.frequenz = diga.daten.ActFreq(start_measurement:end);
                temp_data.Zreal = diga.daten.Zreal1(start_measurement:end)/1000;
                temp_data.Zimg = diga.daten.Zimg1(start_measurement:end)/1000;
                
                data(dataIndex) = remove_rep_from_EIS(temp_data);
                dataIndex = dataIndex+1;
            else
                msgbox('Datei enth�lt keine EIS Messung');
            end
        catch
            msgbox('Datei enth�lt keine EIS Messung');
        end
    % end for loop
    end


    app.actSOC = 1;
    
    
    %% Aktualisieren der Index Vektoren
    app.actBattery = app.actBattery.addSOCtable(app.SOC);
    app.actBattery = app.actBattery.addTemptable(app.Temp);
    app.actBattery = app.actBattery.addSOHtable(app.SOH);
    
    %% Sichern der Daten in der Batterieklasse
    app.actBattery = app.actBattery.addEISdata_allSOC(data,app.actTemp,app.actSOH);
    
    %% Auswahlboxen aktualisieren
    update_listboxes(app);
    
    %% Aktualisieren der Plots
    try
        plot_nyquist(app);
        plot_datapoints(app);
    catch
    end
end


end

