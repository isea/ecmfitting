function [app ] = change_several_fit_parameters( app, P_Name, ReFit, tdf_cd_flag,tdf_cd_crates)
%if no C rate dependency was parameterized, overwrite FDF parameters
if ~tdf_cd_flag
    app.fit =app.actBattery.fit(app.actHyst,app.actCrate,app.actSOC,app.actTemp,app.actSOH);

    to_replace = find(cell2mat(P_Name(4,:))); %Indizes in P_Name
    replace_by = cell2mat(P_Name(3,:));       %Indizes in ReFit

    if isempty(to_replace)
%             disp(['Keine passenden Refitwerte gefunden f�r ',temp,' grad and ',SOC,'%.'])
        disp(['No suitable Refit values found at ' num2str(app.actBattery.temp(app.actTemp)) '�C, ' num2str(app.actBattery.SOC(app.actSOC)) ' %'])
        return
    end        

    app.fit.Parameter(to_replace) = [ReFit{replace_by,2}];
    app.fit.ParFix(to_replace) = [ReFit{replace_by,3}];
    app.fit.Parameter_min(to_replace) = [ReFit{replace_by,4}];
    app.fit.Parameter_max(to_replace) = [ReFit{replace_by,5}];

    p = app.fit.Parameter;
    formula = app.fitProperties.aktuell_Modell.Rechnen_Modell;
    if strcmp(formula(end),'+')
        formula = formula(1:end-1);
    end
    w = app.actBattery.EISdata(app.actHyst,app.actCrate,app.actSOC,app.actTemp,app.actSOH).frequenz;
    m_real = app.actBattery.EISdata(app.actHyst,app.actCrate,app.actSOC,app.actTemp,app.actSOH).Zreal;
    m_imag = app.actBattery.EISdata(app.actHyst,app.actCrate,app.actSOC,app.actTemp,app.actSOH).Zimg; 

    %% Berechnung der gefitteten Kurve
    f_real = real(eval(formula));
    f_imag = imag(eval(formula));
    %% Residuum
    distances=zeros(1,length(m_real));
    for m=1:length(m_real)
        distances(m)=sqrt((m_real(m)-f_real(m))^2+(m_imag(m)-f_imag(m))^2);
    end
    resid=sum(distances)/length(distances);
    app.SF_Residuum.Text = num2str(resid);

    app.fit.Residuum = resid;
    app.fit.Zreal = f_real;
    app.fit.Zimag = f_imag;

    fit = Fit();
    %% Speichern in Battery Klasse
    fit.Parameter = app.fit.Parameter;
    fit.Parameter_min = app.fit.Parameter_min;
    fit.Parameter_max = app.fit.Parameter_max;
    fit.ParFix = app.fit.ParFix;
    fit.Zreal = app.fit.Zreal;
    fit.Zimag = app.fit.Zimag;
    fit.Residuum = app.fit.Residuum;

    app.actBattery = app.actBattery.addFit(fit,app.actSOC,app.actTemp,app.actSOH,app.actHyst,app.actCrate);
else
    %%create new slices for parameterized C rates
    actCrate = app.actCrate;
    %create data structures
    app.fit =app.actBattery.fit(app.actHyst,app.actCrate,app.actSOC,app.actTemp,app.actSOH);
    to_replace = cell2mat(P_Name(4,:)); %Indizes in P_Name    
    replacement_parameter = zeros(length(tdf_cd_crates),length(P_Name(4,:)));   
    replacement_fix = zeros(length(tdf_cd_crates),length(P_Name(4,:)));   
    replacement_parameter_min = zeros(length(tdf_cd_crates),length(P_Name(4,:)));   
    replacement_parameter_max = zeros(length(tdf_cd_crates),length(P_Name(4,:)));   
    
    if isempty(to_replace)
        disp(['No suitable Refit values found at ' num2str(app.actBattery.temp(app.actTemp)) '�C, ' num2str(app.actBattery.SOC(app.actSOC)) ' %'])
        return
    end       
    
    %construct matrix of values for each C rate
    for n = 1:length(P_Name(4,:))
        if to_replace(n)
            replace_by = P_Name{3,n};  
            if length(replace_by) > 1
                for m = 1:length(replace_by)
                    replacement_parameter(m,n) =     ReFit{replace_by(m),2};
                    replacement_fix(m,n) =           ReFit{replace_by(m),3};
                    replacement_parameter_min(m,n) = ReFit{replace_by(m),4};
                    replacement_parameter_max(m,n) = ReFit{replace_by(m),5};
                end
            else
                replacement_parameter(:,n) =     ReFit{replace_by,2} * ones(size(replacement_parameter(:,n)));
                replacement_fix(:,n) =           ReFit{replace_by,3} * ones(size(replacement_parameter(:,n)));
                replacement_parameter_min(:,n) = ReFit{replace_by,4} * ones(size(replacement_parameter(:,n)));
                replacement_parameter_max(:,n) = ReFit{replace_by,5} * ones(size(replacement_parameter(:,n)));
            end
        else
            replacement_parameter(:,n) =        app.fit.Parameter(n)     * ones(size(replacement_parameter(:,n)));
            replacement_fix(:,n) =              app.fit.ParFix(n)        * ones(size(replacement_parameter(:,n)));
            replacement_parameter_min(:,n) =    app.fit.Parameter_min(n) * ones(size(replacement_parameter(:,n)));
            replacement_parameter_max(:,n) =    app.fit.Parameter_max(n) * ones(size(replacement_parameter(:,n)));
        end 
    end
    
    %add 0C entry 
    if isempty(find(tdf_cd_crates == 0,1))
        next_highest_Crate = find(tdf_cd_crates > 0, 1, 'first');
        %move data a row to add 0C data
        replacement_parameter(next_highest_Crate+1:end+1,:) = replacement_parameter(next_highest_Crate:end,:);
        %interpolate to 0C
        for n = 1:length(replacement_parameter(next_highest_Crate,:))
            replacement_parameter(next_highest_Crate,n) = interp1(tdf_cd_crates,replacement_parameter([1:next_highest_Crate-1 next_highest_Crate+1:end],n),0);
        end
    end
    
    w = app.actBattery.EISdata(app.actHyst,app.actCrate,app.actSOC,app.actTemp,app.actSOH).frequenz;
    m_real = app.actBattery.EISdata(app.actHyst,app.actCrate,app.actSOC,app.actTemp,app.actSOH).Zreal;
    m_imag = app.actBattery.EISdata(app.actHyst,app.actCrate,app.actSOC,app.actTemp,app.actSOH).Zimg; 
    formula = app.fitProperties.aktuell_Modell.Rechnen_Modell;   
    if strcmp(formula(end),'+')
        formula = formula(1:end-1);
    end
    for n = 1:length(tdf_cd_crates)
        %update data structures
        crate_idx = find(app.actBattery.Crate == tdf_cd_crates(n));
        if isempty(crate_idx)
            app.actBattery.Crate = [app.actBattery.Crate,tdf_cd_crates(n)];
            app.actCrate = length(app.actBattery.Crate);
            app.sorted_crate{app.actHyst} = [app.sorted_crate{app.actHyst} tdf_cd_crates(n)];
            if length(app.sorted_temp{app.actHyst}) < app.actCrate
                app.sorted_temp{app.actHyst}{app.actCrate} = app.actBattery.temp(app.actTemp);
            else
                app.sorted_temp{app.actHyst}{app.actCrate} = [app.sorted_temp{app.actHyst}{actCrate} app.actBattery.temp(app.actTemp)];
            end
            if length(app.sorted_soc{app.actHyst}) < app.actCrate
                app.sorted_soc{app.actHyst}{app.actCrate}{app.actTemp} = app.actBattery.SOC(app.actSOC);
            else
                app.sorted_soc{app.actHyst}{app.actCrate}{app.actTemp} = [app.sorted_soc{app.actHyst}{app.actCrate}{app.actTemp} app.actBattery.SOC(app.actSOC)];
            end
        else
            app.actCrate = crate_idx;
        end
        app.fit.Parameter =         [replacement_parameter(n,:)];
        app.fit.ParFix =            [replacement_fix(n,:)];
        app.fit.Parameter_min =     [replacement_parameter_min(n,:)];
        app.fit.Parameter_max =     [replacement_parameter_max(n,:)];

        p = app.fit.Parameter;        
        
        % Berechnung der gefitteten Kurve
        f_real = real(eval(formula));
        f_imag = imag(eval(formula));
        % Residuum
        distances=zeros(1,length(m_real));
        for m=1:length(m_real)
            distances(m)=sqrt((m_real(m)-f_real(m))^2+(m_imag(m)-f_imag(m))^2);
        end
        resid=sum(distances)/length(distances);
        app.SF_Residuum.Text = num2str(resid);

        app.fit.Residuum = resid;
        app.fit.Zreal = f_real;
        app.fit.Zimag = f_imag;

        fit = Fit();
        % Speichern in Battery Klasse
        fit.Parameter = app.fit.Parameter;
        fit.Parameter_min = app.fit.Parameter_min;
        fit.Parameter_max = app.fit.Parameter_max;
        fit.ParFix = app.fit.ParFix;
        fit.Zreal = app.fit.Zreal;
        fit.Zimag = app.fit.Zimag;
        fit.Residuum = app.fit.Residuum;

        app.actBattery = app.actBattery.addFit(fit,app.actSOC,app.actTemp,app.actSOH,app.actHyst,app.actCrate);
        app.actBattery = app.actBattery.addEISdata(app.actBattery.EISdata(app.actHyst,actCrate,app.actSOC,app.actTemp,app.actSOH),app.actSOC,app.actTemp,app.actSOH,app.actHyst,app.actCrate);
        app.actBattery = app.actBattery.addRelaxdata(app.actBattery.relaxdata(app.actHyst,actCrate,app.actSOC,app.actTemp,app.actSOH),app.actSOC,app.actTemp,app.actSOH,app.actHyst,app.actCrate);
        app.actBattery = app.actBattery.addCapacity(app.actBattery.capacity(app.actHyst,actCrate,app.actTemp,app.actSOH),app.actTemp,app.actSOH,app.actHyst,app.actCrate);        
    end
    app.actCrate = actCrate;
end
end

