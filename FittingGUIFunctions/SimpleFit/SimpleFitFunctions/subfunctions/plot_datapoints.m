function [] = plot_datapoints(app)
%PLOT_DATAPOINTS Summary of this function goes here
%   Detailed explanation goes here

k=1;
X=[];
Y=[];
for i=1:length(app.actBattery.SOC)
   for j=1:length(app.actBattery.temp)
       try
       if ~isempty(app.actBattery.EISdata(i,j,app.actSOH).Zreal)
           X(k) = app.actBattery.SOC(i);
           Y(k) = app.actBattery.temp(j);
           k=k+1;
       end
       catch
       end
   end
end

plot(app.SF_dataPoints,X,Y,'o','DisplayName','Messungen','LineWidth',2,'Color',app.Colors.Blau);
k=1;
X=[];
Y=[];
for i=1:length(app.actBattery.SOC)
   for j=1:length(app.actBattery.temp)
       try
       if ~isempty(app.actBattery.fit(i,j,app.actSOH).Zreal)
           X(k) = app.actBattery.SOC(i);
           Y(k) = app.actBattery.temp(j);
           k=k+1;
       end   
       catch
       end
   end
end
hold(app.SF_dataPoints, 'on');
plot(app.SF_dataPoints,X,Y,'*','DisplayName','Gefitted','LineWidth',2,'Color',app.Colors.Rot);
plot(app.SF_dataPoints,app.actBattery.SOC(app.actSOC),app.actBattery.temp(app.actTemp),'xk',...
    'DisplayName','aktuelle Messung','LineWidth',2,'Color',app.Colors.Schwarz);
h1 = legend(app.SF_dataPoints,'show');
set(h1,'Interpreter','none','Location','NorthWest');
hold(app.SF_dataPoints, 'off');
end

