
function [BatNames] = find_BatName(filename,FileFormat)
%FIND_BATNAME Summary of this function goes here
%   Detailed explanation goes here
[names,values] = parse_filename(filename,FileFormat);

index = find(strcmp(names,FileFormat.ProcedureName));
BatNames = values(:,index);


    for i=1:length(BatNames)
        newformat=strfind(BatNames{i},'.mat');
        if ~isempty(newformat)
            part1=BatNames{i}(1:9);
            part2=BatNames{i}(end-9:end-4);
            BatNames{i}=[part1,part2];
        end
    end
    
    
end

