function XML_struct = generate_options_observer_FittingGUI(app, XML_struct)
%GENERATE_OPTIONS_OBSERVER Summary of this function goes here
%   Detailed explanation goes here

%MetaData
XML_struct.Configuration.MetaData.UUID.Text = char(java.util.UUID.randomUUID);
XML_struct.Configuration.MetaData.CXML.Date.Text = datestr(datetime);
XML_struct.Configuration.MetaData.CXML.Creator.Text = char(java.lang.System.getProperty('user.name'));

[~,git_hash] = system('git rev-parse HEAD');
XML_struct.Configuration.MetaData.CXML.CommitHash.Text = git_hash(1:40);

%% Options
%General Options
XML_struct.Configuration.Options.SampleRate.Text = '1000';
XML_struct.Configuration.Options.AirTemperature.Text = '25';
XML_struct.Configuration.Options.AirTemperature.Attributes.ReadFromFile = 'false';
XML_struct.Configuration.Options.StepTime.Text = num2str(0.1);


%Simulink Options
XML_struct.Configuration.Options.MaxStateSize.Text = '2000';
XML_struct.Configuration.Options.MaxOutputSize.Text = '500';
XML_struct.Configuration.Options.MaxNumberOfCells.Text = '100';


%% Observer
%Observer electrical
XML_struct.Configuration.Observer.Electrical.Filter1 = [];
XML_struct.Configuration.Observer.Thermal.Filter1 = [];

XML_struct.Configuration.Observer.Electrical.Filter2.Filename.Text = 'SampleOutput.mat';
XML_struct.Configuration.Observer.Electrical.Filter2.Attributes.class = 'MatlabFilter';
XML_struct.Configuration.Observer.Electrical.Filter2.MaxSampleSize.Text = '20000000';
XML_struct.Configuration.Observer.Electrical.Filter2.Attributes.class = 'MatlabFilter';
if app.SampleTimeEditField.Value <= 100
    XML_struct.Configuration.Observer.Electrical.Filter1.TimeDelay.Text = '0.0009999';
else
    XML_struct.Configuration.Observer.Electrical.Filter1.TimeDelay.Text = num2str((1/10) * 1/app.SampleTimeEditField.Value);
end
XML_struct.Configuration.Observer.Electrical.Filter1.Attributes.class = 'DecimateFilter';
end

