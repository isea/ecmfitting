function [] = plot_puls(app)
%PLOT_PULS Summary of this function goes here
%   Detailed explanation goes here
ax_voltage = app.SF_puls_voltage;
ax_current = app.SF_puls_current;

ax_voltage.XLimMode = 'auto';
ax_voltage.YLimMode = 'auto';
ax_current.XLimMode = 'auto';
ax_current.YLimMode = 'auto';

time = app.pulse(app.actSOC,app.actTemp,app.actSOH).time;
voltage = app.pulse(app.actSOC,app.actTemp,app.actSOH).voltage;
current = app.pulse(app.actSOC,app.actTemp,app.actSOH).current;

plot(ax_voltage,time,voltage,'Color',app.Color.Blau);
plot(ax_current,time,current,'Color',app.Color.Blau);
end

