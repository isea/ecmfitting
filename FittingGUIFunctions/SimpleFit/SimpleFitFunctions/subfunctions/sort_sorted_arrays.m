function [app] = sort_sorted_arrays(app)
%sort sorted_SOC sorted_temp, sorted crate etc.
for n = 1:length(app.actBattery.Hyst)
    [app.sorted_crate{n},order] = sort(app.sorted_crate{n});
    app.sorted_temp{n} = app.sorted_temp{n}(order);
    app.sorted_soc{n} = app.sorted_soc{n}(order);
    
    for n1 = 1:length(app.sorted_crate{n})
        [app.sorted_temp{n}{n1},order] = sort(app.sorted_temp{n}{n1});
         app.sorted_soc{n}{n1} = app.sorted_soc{n}{n1}(order);
         
         for n2 = 1:length(app.sorted_temp{n}{n1})
             [app.sorted_soc{n}{n1}{n2},order] = sort(app.sorted_soc{n}{n1}{n2});
         end
    end
end
end

