function [] = load_FittingGUI_data(app,path)
%LOAD_FITTINGGUI_DATA Summary of this function goes here
oldpath = path;
%   Detailed explanation goes here
global DRT_GUI
app.actBattery = Battery('');
if app.GUI
    path=[pwd,'/','output/',DRT_GUI.Testparameter.Batterie,'/'];
else
    path=[app.OutputPath '/',DRT_GUI.Testparameter.Batterie,'/'];
end
app.SOC = [];
app.SOH = [];
app.Hyst = [];
app.Crate = [];
app.Temp = [];

app.actSOC = 1;
app.actTemp = 1;
app.actSOH = 1;
app.actHyst = 1;
app.actCrate = 1;
app.actParameter = 1;

ReFit_Whitelist =string(missing);
% SOH einlesen

sSOH = inputdlg('Please enter the SOH of the selected cell:','New SOH');
if isempty(sSOH{1,1})
    msgbox('The SOH may not be empty.','','error');
    return;
else
    tSOH = round(str2double(sSOH));
    if isempty(app.SOH)
        app.SOH = tSOH;
        app.actSOH = 1;
    else
        iSOH = find(app.SOH==tSOH);
        if iSOH
            app.actSOH = iSOH;
        else
            app.SOH = [app.SOH,tSOH];
            app.actSOH = length(app.SOH);
        end
    end
end

% Auswertungsname speichern
app.actBattery = app.actBattery.setAuswertung('FittingGUI Data',app.actSOH);

current_path = pwd;
cd(path);
list_hyst = dir('*H');
if isempty(list_hyst)
    list_hyst(1).name = [];
    length_list_hyst = 1;
else
    for n = 1:length(list_hyst)
        list_hyst(n).name = [list_hyst(n).name '\'];
    end
    length_list_hyst = length(list_hyst);
end
for hyst_index = 1:length_list_hyst
    cd([path, '\', list_hyst(hyst_index).name])
    if app.HysteresisFlag
        hyst = DRT_GUI.Testparameter.Zustand_2;
        if isempty(app.Hyst)
            app.Hyst = str2num(strrep(hyst(1:end-1),'m','-'));
            app.actHyst = 1;
        else
            iHyst = find(app.Hyst==str2num(strrep(hyst(1:end-1),'m','-')));
            if iHyst
                app.actHyst = iHyst;
            else
                app.Hyst = [app.Hyst,str2num(strrep(hyst(1:end-1),'m','-'))];
                app.actHyst = length(app.Hyst);
            end
        end
    else
        app.actHyst = 1;
        app.Hyst = 0;
    end
    list_crate = dir('*C');
    if isempty(list_crate)
        list_crate(1).name = [];
        length_list_crate = 1;
    else
        for n = 1:length(list_crate)
            list_crate(n).name = [list_crate(n).name '\'];
        end
        length_list_crate = length(list_crate);
    end
    for crate_index = 1:length_list_crate
        cd([path, '\', list_hyst(hyst_index).name,list_crate(crate_index).name])
        if app.CRateFlag
            crate = DRT_GUI.Testparameter.Zustand;
            if isempty(app.Crate)
                app.Crate = str2num(strrep(crate(1:end-4),'m','-'));
                app.actCrate = 1;
            else
                iCrate = find(app.Crate==str2num(strrep(crate(1:end-4),'m','-')));
                if iCrate
                    app.actCrate = iCrate;
                else
                    app.Crate = [app.Crate,str2num(strrep(crate(1:end-4),'m','-'))];
                    app.actCrate = length(app.Crate);
                end
            end
        else
            app.actCrate = 1;
        end
        list = dir('*grad*');
        for i=1:length(list)
            cd([path,'\',list_hyst(hyst_index).name,list_crate(crate_index).name,list(i).name])
            act_temp_list = dir('*SOC.mat');
            cd (current_path);
            for j=1:length(act_temp_list)
                warning('off', 'MATLAB:dispatcher:UnresolvedFunctionHandle');
                load([path,'\',list_hyst(hyst_index).name,list_crate(crate_index).name,list(i).name,'\',act_temp_list(j).name],'DRT_GUI');
                % Lade allgemeine Parameter beim ersten Aufruf
                if(i==1 && j==1 && hyst_index == 1 && crate_index == 1)
                    if ~strcmp(DRT_GUI.Testparameter.Batterie,app.actBattery.name)
                        choice = questdlg({'The batteries name and the name in the file are not equal',['Filename: ',DRT_GUI.Testparameter.Batterie],['currently used name: ',app.actBattery.name]},...
                            '','Copy from file','Do not copy','Do not use measurement','Copy from file');
                        switch choice
                            case 'Copy from file'
                                app.actBattery.name = DRT_GUI.Testparameter.Batterie;
                                app.SF_BatName.Text = DRT_GUI.Testparameter.Batterie;
                            case 'Do not use measurement'
                                return;
                            case 'Do not copy'
                        end
                    end
                    iModell = find(strcmp(app.Modellliste,['ESB_',DRT_GUI.Fit.aktuell_Modell.Modellname]));
                    if isempty(iModell)
                        h = warndlg(['Model ',DRT_GUI.Fit.aktuell_Modell.Modellname,' is unknown']);
                        uiwait(h);
                        choice = questdlg({'Only import measurement data?'},...
                            '','Yes','No','Yes');
                        switch choice
                            case 'Yes'
                            case 'No'
                                return;
                        end
                    else
                        change_act_model(app,iModell);
                        app.SF_ModellauswahlDropDown.Value = app.SF_ModellauswahlDropDown.Items(iModell);
                    end
                end
                if(j==1)
                    % Temperatur finden
                    temp = DRT_GUI.Testparameter.Temperatur;
                    if isempty(app.Temp)
                        app.Temp = temp;
                        app.actTemp = 1;
                    else
                        iTemp = find(app.Temp==temp);
                        if iTemp
                            app.actTemp = iTemp;
                        else
                            app.Temp = [app.Temp,temp];
                            app.actTemp = length(app.Temp);
                        end
                    end
                    
                    % Kapazitšt speichern
                    if isfield(DRT_GUI.Testparameter,'Cap')
                        app.SF_NomCap.Text = [num2str(DRT_GUI.Testparameter.Cap) ' Ah'];
                        app.actBattery = app.actBattery.addCapacity(DRT_GUI.Testparameter.Cap,app.actTemp,app.actSOH,app.actHyst,app.actCrate);
                    else
                        sCap = inputdlg('Please enter the capacity:','Capacity');
                        app.SF_NomCap.Text = [sCap{1},' Ah'];
                        app.actBattery = app.actBattery.addCapacity(str2double(sCap),app.actTemp,app.actSOH,app.actHyst,app.actCrate);
                    end
                    qOCV = QOCV();
                    app.actBattery = app.actBattery.addqOCV_CH(qOCV,app.actTemp,app.actSOH,app.actHyst,app.actCrate);
                    app.actBattery = app.actBattery.addqOCV_DCH(qOCV,app.actTemp,app.actSOH,app.actHyst,app.actCrate);
                end
                % SOC finden
                SOC = DRT_GUI.Testparameter.SOC;
                if isempty(app.SOC)
                    app.SOC = SOC;
                    app.actSOC = 1;
                else
                    % Einsortieren weiterer Messungen in aktuelles SOC Array, falls neue SOCs
                    % auftreten, wird hinten angehangen
                    iSOC = find(app.SOC==SOC);
                    if iSOC
                        app.actSOC = iSOC;
                    else
                        app.SOC = [app.SOC,SOC];
                        app.actSOC = length(app.SOC);
                    end
                end
                % EIS Daten ablegen
                data = EISData();
                data.Zreal = DRT_GUI.Messdaten.Zreal;
                data.Zimg = DRT_GUI.Messdaten.Zimg;
                data.frequenz = DRT_GUI.Messdaten.frequenz;
                app.actBattery = app.actBattery.addEISdata(data,app.actSOC,app.actTemp,app.actSOH,app.actHyst,app.actCrate);
                % Fit Ablegen
                if ~isempty(iModell)
                    
                    fit = Fit();
                    fit.Parameter = DRT_GUI.Fit.Parameter;
                    fit.Parameter_min = DRT_GUI.Fit.Parameter_min;
                    fit.Parameter_max = DRT_GUI.Fit.Parameter_max;
                    fit.ParFix = DRT_GUI.Fit.ParFix;
                    fit.Zreal = DRT_GUI.Fit.Zreal;
                    fit.Zimag = DRT_GUI.Fit.Zimg;
                    fit.Residuum = DRT_GUI.Fit.residuum;
                    app.actBattery = app.actBattery.addFit(fit,app.actSOC,app.actTemp,app.actSOH,app.actHyst,app.actCrate);
                end
                if isfield(DRT_GUI.Messdaten,'relax')
                    if ~isempty(DRT_GUI.Messdaten.relax.zeit)
                        relax = RelaxData();
                        relax.time = DRT_GUI.Messdaten.relax.zeit;
                        relax.voltage = DRT_GUI.Messdaten.relax.spannung;
                        relax.current = DRT_GUI.Messdaten.relax.strom;
                        app.actBattery = app.actBattery.addRelaxdata(relax,app.actSOC,app.actTemp,app.actSOH,app.actHyst,app.actCrate);
                    end
                else
                    relax = RelaxData();
                    app.actBattery = app.actBattery.addRelaxdata(relax,app.actSOC,app.actTemp,app.actSOH,app.actHyst,app.actCrate);
                end
            end
        end
    end
end
% Aktualisieren der Index Vektoren
app.actBattery = app.actBattery.addSOCtable(app.SOC);
app.actBattery = app.actBattery.addTemptable(app.Temp);
app.actBattery = app.actBattery.addSOHtable(app.SOH);
app.actBattery = app.actBattery.addHysttable(app.Hyst);
app.actBattery = app.actBattery.addCratetable(app.Crate);

app.actParameter = 1;
app = sort_SOC(app);
app = sort_temp(app);
app = sort_crate(app);
app = sort_hyst(app);

%% Auswahlboxen aktualisieren
update_listboxes(app);

%% ReFit Parameter
choice = questdlg({['Search for ReFit parameters from time domain fitting?']},...
    '','Yes','No','No');
switch choice
    case 'No'
        return
    case 'Yes'
        for hyst_index = 1:length_list_hyst
            cd([path, '\', list_hyst(hyst_index).name])
            if app.HysteresisFlag
                hyst = DRT_GUI.Testparameter.Zustand_2;
                if isempty(app.Hyst)
                    app.Hyst = str2num(strrep(hyst(1:end-1),'m','-'));
                    app.actHyst = 1;
                else
                    iHyst = find(app.Hyst==str2num(strrep(hyst(1:end-1),'m','-')));
                    if iHyst
                        app.actHyst = iHyst;
                    else
                        app.Hyst = [app.Hyst,str2num(strrep(hyst(1:end-1),'m','-'))];
                        app.actHyst = length(app.Hyst);
                    end
                end
            else
                app.actHyst = 1;
            end
            list_crate = dir('*C');
            if isempty(list_crate)
                list_crate(1).name = [];
                length_list_crate = 1;
            else
                for n = 1:length(list_crate)
                    list_crate(n).name = [list_crate(n).name '\'];
                end
                length_list_crate = length(list_crate);
            end
            for crate_index = 1:length_list_crate
                cd([path, '\', list_hyst(hyst_index).name,list_crate(crate_index).name])
                if app.CRateFlag
                    crate = DRT_GUI.Testparameter.Zustand;
                    if isempty(app.Crate)
                        app.Crate = str2num(strrep(crate(1:end-4),'m','-'));
                        app.actCrate = 1;
                    else
                        iCrate = find(app.Crate==str2num(strrep(crate(1:end-4),'m','-')));
                        if iCrate
                            app.actCrate = iCrate;
                        else
                            app.Crate = [app.Crate,str2num(strrep(crate(1:end-4),'m','-'))];
                            app.actCrate = length(app.Crate);
                        end
                    end
                else
                    app.actCrate = 1;
                end
                list = dir('*grad*');
                for i=1:length(list)
                    cd([path,'\',list_hyst(hyst_index).name,list_crate(crate_index).name,list(i).name])
                    act_temp_list = dir('*SOC.mat');
                    cd (current_path);
                    for j=1:length(act_temp_list)
                        warning('off', 'MATLAB:dispatcher:UnresolvedFunctionHandle');
                        load([path,'\',list_hyst(hyst_index).name,list_crate(crate_index).name,list(i).name,'\',act_temp_list(j).name],'DRT_GUI');
                        
                        if(j==1)
                            % Temperatur finden
                            temp = DRT_GUI.Testparameter.Temperatur;
                            iTemp = find(app.Temp==temp);
                            if iTemp
                                app.actTemp = iTemp;
                            else
                                app.Temp = [app.Temp,temp];
                                app.actTemp = length(app.Temp);
                            end
                        end
                        % SOC finden
                        SOC = DRT_GUI.Testparameter.SOC;
                        % Einsortieren weiterer Messungen in aktuelles SOC Array, falls neue SOCs
                        % auftreten, wird hinten angehangen
                        iSOC = find(app.SOC==SOC);
                        if iSOC
                            app.actSOC = iSOC;
                        else
                            app.SOC = [app.SOC,SOC];
                            app.actSOC = length(app.SOC);
                        end
                        
                        if ~isempty(DRT_GUI.Fit.aktuell_Modell.P_Name)&&~isempty(DRT_GUI.Fit.Implementierung.OCV)
                            for k = 1:length(DRT_GUI.Fit.aktuell_Modell.P_Name)
                                % Gibt es passenden ReFit Parameter?
                                DRT_GUI.Fit.aktuell_Modell.P_Name{3,k}=find(transpose(strcmp(['ReFit_',DRT_GUI.Fit.aktuell_Modell.P_Name{1,k}],DRT_GUI.Fit.Implementierung.OCV(:,1))));
                                % Soll Parameter verwendet werden?
                                if isempty(DRT_GUI.Fit.aktuell_Modell.P_Name{3,k}) %falls kein ReFit gefunden
                                    DRT_GUI.Fit.aktuell_Modell.P_Name{4,k}=0;
                                elseif sum(strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name{1,k},ReFit_Whitelist))>0
                                    DRT_GUI.Fit.aktuell_Modell.P_Name{4,k}=1;
                                else
                                    choice = questdlg({['ReFit parameter found for: ', DRT_GUI.Fit.aktuell_Modell.P_Name{1,k}],['SOC = ',num2str(SOC), ' %'],['T   = ',num2str(temp), ' degrees'],['Use ReFit Parameters?']},...
                                        '','Yes','Yes, for subsequent fits too(if possible)','No','No');
                                    switch choice
                                        case 'Yes'
                                            DRT_GUI.Fit.aktuell_Modell.P_Name{4,k}=1;
                                        case 'Yes, for subsequent fits too(if possible)'
                                            DRT_GUI.Fit.aktuell_Modell.P_Name{4,k}=1;
                                            ReFit_Whitelist = [ReFit_Whitelist;DRT_GUI.Fit.aktuell_Modell.P_Name{1,k}];
                                        case 'No'
                                            DRT_GUI.Fit.aktuell_Modell.P_Name{4,k}=0;
                                    end
                                end
                            end
                            
                            change_several_fit_parameters(app,DRT_GUI.Fit.aktuell_Modell.P_Name,DRT_GUI.Fit.Implementierung.OCV);
                        else
                            disp(['No table for Refit values was found for ',temp,' grad and ',SOC,'%.'])
                        end
                    end
                end
            end
        end           
end
path = oldpath;
end

