function app = generate_qOCV(app)
%GENERATE_OCV Summary of this function goes here
%   Detailed explanation goes here

for k=1:length(app.SOH)
    if app.onButton_temperature_depencendy.Value
        for i=1:length(app.Temp)
            l=1;
            for j=1:length(app.SOC)
                if ~isempty(app.actBattery.relaxdata(app.default_hyst_index,app.default_crate_index,j,i,k).time)
                    app.actBattery.qOCV_CH(i,k).SOC(l) = app.SOC(j);
                    app.actBattery.qOCV_CH(i,k).voltage(l) = app.actBattery.relaxdata(app.default_hyst_index,app.default_crate_index,j,i,k).voltage(end);
                    l=l+1;
                end
            end
        end
    elseif app.offButton_temperature_depencendy.Value
        if app.GUI
            f = dir(['output/' app.BatterieNamePopup.Value '/'  app.ZustandPopup.Value '/*']);
        else
            f = dir([app.OutputPath '/' app.BatterieNamePopup.Value '/'  app.ZustandPopup.Value '/*']);            
        end
        for reference_idx = 3:length(f)
            reference_temperature = f(reference_idx).name;
            reference_temperature = reference_temperature(1:end -4 );
            if contains(reference_temperature,'m')
                reference_temperature =strcat('-',reference_temperature(2:end));
            end
            if strcmp(num2str(app.referencetemperatureEditField.Value),reference_temperature)                
                temp_idx = find(app.Temp == str2num(convertCharsToStrings(reference_temperature)));                
                i = temp_idx;
                for i1 = 1:length(app.Temp)
                    l=1;
                    for j=1:length(app.SOC)
                        if ~isempty(app.actBattery.relaxdata(app.default_hyst_index,app.default_crate_index,j,i,k).time)
                            app.actBattery.qOCV_CH(i1,k).SOC(l) = app.SOC(j);
                            app.actBattery.qOCV_CH(i1,k).voltage(l) = app.actBattery.relaxdata(app.default_hyst_index,app.default_crate_index,j,i,k).voltage(end);
                            l=l+1;
                        end
                    end
                end
            end
        end
    end
end

end

