function XML_struct = generate_root_element_FittingGUI(app,XML_struct,isModul,isSpread)
%GENERATE_ROOT_ELEMENT Summary of this function goes here
%   Detailed explanation goes here
%   isModul: Definiere ob Modul oder einzelnde Zelle simuliert werden soll
%   isSpread:   Definiert ob Streuung berücksichtigt wird


%create single Cell root element
%Root element
XML_struct.Configuration.RootElement.Attributes.class = 'SerialTwoPort';
XML_struct.Configuration.RootElement.Children.Attributes.count = '1';
XML_struct.Configuration.RootElement.Children.Pack.Text ='';
XML_struct.Configuration.RootElement.Children.Pack.Attributes.ref = app.electrical_models{1,2};


end