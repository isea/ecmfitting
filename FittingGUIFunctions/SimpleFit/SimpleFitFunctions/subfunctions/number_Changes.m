function [index] = number_Changes(Schritte,Prozedurebene,Schrittwechsel,Prozedurwechsel)

    if nargin < 4
       error('Nicht genug Eingangsparameter'); 
    end
    if nargin > 4
       error('Zu viele Eingangsparameter'); 
    end
    
    if(diff(Schrittwechsel)~=0)
        index_stepchanges = find(diff(Schritte)==diff(Schrittwechsel));
        stepchanges = zeros(1,length(index_stepchanges));
        for i=1:length(index_stepchanges)
            stepchanges(i) = Schritte(index_stepchanges(i));
        end
        right_start = find(stepchanges==Schrittwechsel(1));
        
        index_w_right_start = zeros(1,length(right_start));
        for i=1:length(right_start)
            index_w_right_start(i) = index_stepchanges(right_start(i));
        end
    else
        index_w_right_start = find(Schritte==Schrittwechsel(1));
    end
    
    if(diff(Prozedurwechsel)~=0)
        index_prochanges = find(diff(Prozedurebene)==diff(Prozedurwechsel));
        prochanges = zeros(1,length(index_prochanges));
        for i=1:length(index_prochanges)
            prochanges(i) = Prozedurebene(index_prochanges(i));
        end
        right_start_pro = find(prochanges==Prozedurwechsel(1));
        
        index_w_right_start_pro = zeros(1,length(right_start_pro));
        for i=1:length(right_start_pro)
            index_w_right_start_pro(i) = index_prochanges(right_start_pro(i));
        end
    else
        index_w_right_start_pro = find(Prozedurebene==Prozedurwechsel(1));
    end
    
    index = intersect(index_w_right_start,index_w_right_start_pro);
    
end

