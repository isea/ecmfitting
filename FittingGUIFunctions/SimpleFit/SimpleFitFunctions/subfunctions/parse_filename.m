function [names,values] = parse_filename(filename,FileFormat)
%PARSE_FILENAME Summary of this function goes here
%   Detailed explanation goes here
names = split(FileFormat.string,FileFormat.delimiter);
values = cell(length(filename),length(names));
for i=1:length(filename)
    values(i,:) = transpose(split(filename(i),FileFormat.delimiter));
end
end

