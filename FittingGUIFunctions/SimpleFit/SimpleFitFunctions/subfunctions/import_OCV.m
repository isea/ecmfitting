function import_OCV(app,OCV_array)
%IMPORT_OCV Summary of this function goes here
%   Detailed explanation goes here
% SOH einlesen
sSOH = inputdlg('Bitte SOH eingeben:','Neuer SOH');
if isempty(sSOH{1,1})
    msgbox('Der SOH darf nicht Leer sein.','','error');
    return;
else
    tSOH = round(str2double(sSOH));
    iSOH = find(app.SOH==tSOH);
    if isempty(iSOH)
        errordlg('Kein g�ltiger SOH gew�hlt, SOH muss vorher schon in der Liste enthalten sein.');
        return;
    end
end
for i=1:size(OCV_array,1)
    index = find(app.Temp==OCV_array{i,2});
    if isempty(index)
        warndlg(sprintf('F�r %i�C wurden keine EIS Daten gefunden, diese OCV wird ignoriert.',OCV_array{i,2}));
    else
        if strcmp(OCV_array{i,3},'CH')
            app.actBattery = app.actBattery.addqOCV_CH(OCV_array{i,1},index,iSOH);
        else
            if strcmp(OCV_array{i,3},'DCH')
                app.actBattery = app.actBattery.addqOCV_DCH(OCV_array{i,1},index,iSOH);
            else
                warndlg(sprintf('F�r %i�C wurde kein g�ltiger Identifier gefunden \n G�ltige Identifier sind CH und DCH',OCV_array{i,2}));
            end
        end
    end       
end
% plot_qOCV(app);
end

