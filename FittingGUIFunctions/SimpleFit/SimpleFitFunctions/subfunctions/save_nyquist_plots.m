function [] = save_plots(app)
%PLOT_PARAMETER Summary of this function goes here
%   Detailed explanation goes here

savefolder = uigetdir([],'Path to save figures');
list = {'fig','m','jpg','png','eps','pdf','bmp','emf','pbm','pcx','pgm','ppm','tif'};
[indx,tf] = listdlg('PromptString','Select figure format:','ListString',list);

temp = app.actBattery.temp;
SOC = app.actBattery.SOC;
for k = 1:length(temp)
    clear Zreal;
    clear Zimag;
    clear SOCclean;
    j=1
    for i=1:length(SOC)
        try
            Zreal(j,:) = app.actBattery.EISdata(i,k).Zreal;
            Zimag(j,:) = app.actBattery.EISdata(i,k).Zimg;
            SOCclean(j) = SOC(i);
            j=j+1;
        catch
        end
    end
    fig(k) = figure('Name',['$',num2str(temp(k)),'^\circ C$'],'visible', 'off')
    ax(k) = axes();
    newDefaultColors = colormap(jet(j));
    set(fig(k),'DefaultAxesColorOrder',jet(j));
    plot(transpose(Zreal),-transpose(Zimag),'LineWidth',2);
    hc = colorbar
    hc.Ticks = linspace(0, 1, 11);
    hc.TickLabels = num2cell(0:10:100);
    %set(hc,'YTick',[0:10:100])
    %cb = linspace(1,j,j);
    %set(hc, 'YTick',cb, 'YTickLabel',cb)
    ax(k).XLabel.Interpreter = 'latex';
    ax(k).XLabel.String = '$ Real(Z) / \Omega$';
    ax(k).YLabel.Interpreter = 'latex';
    ax(k).YLabel.String = '$ -Imag(Z) / \Omega$';
    ax(k).Title.Interpreter = 'latex';
    ax(k).Title.String = ['$',num2str(temp(k)),'^\circ C$'];
    grid on
    axis tight   % set tight range
    storexlim(k,:) = xlim(ax(k));
    storeylim(k,:) = ylim(ax(k));
end
    
maxxlim = [min(storexlim(:,1)),max(storexlim(:,2))];
maxylim = [min(storeylim(:,1)),max(storeylim(:,2))];
maxxlim(2) = 1.5e-3;
maxylim(2) = 1e-3;
    for i=1:size(fig,2)
        xlim(ax(i),maxxlim);
        ylim(ax(i),maxylim);
        for w = 1:length(indx)
        saveas(fig(i),[savefolder,'/',num2str(temp(i))],list(indx(w)));
        end
        
    end
end

