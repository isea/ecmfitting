function [] = plot_relax(app)
%PLOT_RELAX Summary of this function goes here
%   Detailed explanation goes here
app.SF_relax.XLimMode = 'auto';
app.SF_relax.YLimMode = 'auto';

try
voltage = app.actBattery.relaxdata(app.actSOC,app.actTemp,app.actSOH).voltage;
time = app.actBattery.relaxdata(app.actSOC,app.actTemp,app.actSOH).time;
catch
   voltage = [];
   time =[];
end
if ~isempty(voltage)
   plot(app.SF_relax,time,voltage,'LineWidth',2,'Color',app.Colors.Blau); 
end
end

