function [ ] = plot_nyquist(app)
%PLOT_NYQUIST Summary of this function goes here
%   Detailed explanation goes here


app.SF_nyquist.XLimMode = 'auto';
app.SF_nyquist.YLimMode = 'auto';


Zreal = app.actBattery.EISdata(app.actSOC,app.actTemp,app.actSOH).Zreal;
Zimg = app.actBattery.EISdata(app.actSOC,app.actTemp,app.actSOH).Zimg;

try
    Zreal_clean = app.actBattery.EISdata_clean(app.actSOC,app.actTemp,app.actSOH).Zreal;
    Zimg_clean = app.actBattery.EISdata_clean(app.actSOC,app.actTemp,app.actSOH).Zimg;
    if ~isempty(Zreal_clean)
        [Zreal_rest,ia] = setdiff(Zreal,Zreal_clean);
        Zimg_rest = Zimg(ia);
        Zreal = Zreal_clean;
        Zimg = Zimg_clean;
    end
catch 
end

if ~isempty(Zreal)
    plot(app.SF_nyquist,Zreal,Zimg,'o','DisplayName','Messung','LineWidth',2,'Color',app.Colors.Blau);
    if exist('Zreal_rest','var')==1
        hold(app.SF_nyquist, 'on');
        plot(app.SF_nyquist,Zreal_rest,Zimg_rest,'o','DisplayName','nicht berücksichtigt',...
            'LineWidth',2,'Color',app.Colors.Grau);
    end
else
    if ~isempty(app.SF_nyquist.Children)
        for i=length(app.SF_nyquist.Children):-1:1
            delete(app.SF_nyquist.Children(i));
        end
    end
end
app.SF_Residuum.Text = '';
    try
        hold(app.SF_nyquist, 'on');
        Zreal = app.actBattery.fit(app.actSOC,app.actTemp,app.actSOH).Zreal;
        Zimag = app.actBattery.fit(app.actSOC,app.actTemp,app.actSOH).Zimag;
        plot(app.SF_nyquist,Zreal,Zimag,'DisplayName','Fit','LineWidth',2,'Color',app.Colors.Rot);
        app.SF_Residuum.Text = num2str(app.actBattery.fit(app.actSOC,app.actTemp,app.actSOH).Residuum);
    catch
    end
h1 = legend(app.SF_nyquist,'show');
set(h1,'Interpreter','none','Location','NorthWest');
hold(app.SF_nyquist, 'off');

end

