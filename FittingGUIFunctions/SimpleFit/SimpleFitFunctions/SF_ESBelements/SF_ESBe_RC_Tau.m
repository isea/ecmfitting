classdef SF_ESBe_RC_Tau < ESBelement
    %ESBE_RC_TAU Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function obj = SF_ESBe_RC_Tau()
            obj.Funktionsname = 'RC_Tau';
            obj.inputs = {'R','Tau'};
            obj.Zfun = 'R./(1+1i.*w.*Tau)';
            obj.Zfun_HF = '0';
            obj.Zfun_MF = 'R./(1+1i.*w.*Tau)';
            obj.Zfun_LF = '0';
            obj.Frameworkname{1} = 'ParallelRC';
        end
        
        function xml_struct = generateXMLstruct(obj,SOC,temp,Crate,Hyst,data,LHD)
            xml_struct.Attributes.class = obj.Frameworkname{1};
            xml_struct.LookupOhmicResistance = generate_Lookup2d(data{1},SOC,temp,Crate,Hyst,LHD);
            xml_struct.LookupTau = generate_Lookup2d(data{2},SOC,temp,Crate,Hyst,LHD);
        end
    end
end

