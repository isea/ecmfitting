classdef SF_ESBe_RL_CPE < ESBelement
    %ESBE_RL_CPE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function obj = SF_ESBe_RL_CPE()
            obj.Funktionsname = 'RL_CPE';
            obj.inputs = {'R','L','Phi'};
            obj.Zfun = 'R./(1+R./((1i.*w).^Phi.*L))';
            obj.Zfun_HF = 'R./(1+R./((1i.*w).^Phi.*L))';
            obj.Zfun_MF = '0';
            obj.Zfun_LF = '0';
            obj.Frameworkname = [];
        end
    end
end

