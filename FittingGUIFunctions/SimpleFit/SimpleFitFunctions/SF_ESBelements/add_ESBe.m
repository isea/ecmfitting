function [model] = add_ESBe(model,esb_element,Appendix,HFMFLF,Startwerte,Minimum,Maximum,Fix)
%ADD_ESBE Summary of this function goes here
%   Detailed explanation goes here
    inputs = esb_element.getInputs();
    [Zfun, Zfun_HF, Zfun_MF, Zfun_LF] = esb_element.getZfun();
    for i=1:numel(inputs)
        if isempty(model{4})
            model{4} = {[inputs{i},Appendix]};
            model{5} = Startwerte{i};
            model{6} = Minimum{i};
            model{7} = Maximum{i};
            model{8} = Fix{i};
        else
            model{4} = [model{4},{[inputs{i},Appendix]}];
            model{5} = [model{5},Startwerte{i}];
            model{6} = [model{6},Minimum{i}];
            model{7} = [model{7},Maximum{i}];
            model{8} = [model{8},Fix{i}];
        end
        Zfun = strrep(Zfun,inputs{i},[inputs{i},Appendix]);
        Zfun_HF = strrep(Zfun_HF,inputs{i},[inputs{i},Appendix]);
        Zfun_MF = strrep(Zfun_MF,inputs{i},[inputs{i},Appendix]);
        Zfun_LF = strrep(Zfun_LF,inputs{i},[inputs{i},Appendix]);
    end
    switch HFMFLF
        case 'HF'
            model{1} = [model{1},'+HF.*(',Zfun,')'];
        case 'MF'
            model{2} = [model{2},'+MF.*(',Zfun,')'];
        case 'LF'
            model{3} = [model{3},'+LF.*(',Zfun,')'];
        otherwise
            if ~strcmp(Zfun_HF,'0')
                model{1} = [model{1},'+HF.*(',Zfun_HF,')'];
            end
            if ~strcmp(Zfun_MF,'0')
                model{2} = [model{2},'+MF.*(',Zfun_MF,')'];
            end
            if ~strcmp(Zfun_LF,'0')
                model{3} = [model{3},'+LF.*(',Zfun_LF,')'];
            end
    end
    for i=1:3
        if ~isempty(model{i})
            if model{i}(1) == '+'
                model{i} = model{i}(2:end);
            end
        end
    end

end
