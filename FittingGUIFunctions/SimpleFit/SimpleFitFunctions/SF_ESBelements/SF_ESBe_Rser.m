classdef SF_ESBe_Rser < ESBelement
    %ESBE_RSER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function obj = SF_ESBe_Rser()
            obj.Funktionsname = 'R';
            obj.inputs = {'R'};
            obj.Zfun = 'R';
            obj.Zfun_HF = 'R';
            obj.Zfun_MF = '0';
            obj.Zfun_LF = '0';
            obj.Frameworkname{1} = 'OhmicResistance';
        end
        function xml_struct = generateXMLstruct(obj,SOC,temp,Crate,Hyst,data,LHD)
            xml_struct.Attributes.class = obj.Frameworkname{1};
            xml_struct.Object = generate_Lookup2d(data{1},SOC,temp,Crate,Hyst,LHD);
        end
    end
end

