classdef SF_ESBe_RMP_HN1RC < ESBelement
    %ESBE_RL_CPE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function obj = SF_ESBe_RMP_HN1RC()
            obj.Funktionsname = 'RMP_HN1RC';
            obj.inputs = {'R_ct','Tau_dl','Phi_HN','R_MP'};
            obj.Zfun = '(R_MP.*(R_ct./(1+1i.*w.*Tau_dl).^Phi_HN)).^0.5.*coth((R_MP./(R_ct./(1+1i.*w.*Tau_dl).^Phi_HN)).^0.5)';
            
            obj.Zfun_HF = '0';
            obj.Zfun_MF = '(R_MP.*(R_ct./(1+1i.*w.*Tau_dl).^Phi_HN)).^0.5.*coth((R_MP./(R_ct./(1+1i.*w.*Tau_dl).^Phi_HN)).^0.5)';
            obj.Zfun_LF = '0';
            obj.Frameworkname{1} = 'Rmphn';
        end
        
        function xml_struct = generateXMLstruct(obj,SOC,temp,Crate,Hyst,data,LHD)
            xml_struct.Attributes.class = obj.Frameworkname{1};
            
            xml_struct.LookupOhmicResistance = generate_Lookup2d(data{1},SOC,temp,Crate,Hyst,LHD);
            xml_struct.LookupTau = generate_Lookup2d(data{2},SOC,temp,Crate,Hyst,LHD);
            xml_struct.OhmicResistance_RMP = generate_Lookup2d(data{4},SOC,temp,Crate,Hyst,LHD);
        end
    end
end

