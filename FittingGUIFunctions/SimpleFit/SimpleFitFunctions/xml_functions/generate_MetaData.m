function [framework_struct] = generate_MetaData(framework_struct,battery,creator)
%GENERATE_METADATA Summary of this function goes here
%   Detailed explanation goes here
framework_struct.MetaData.electrical.Date.Text = datestr(datetime('now'));

framework_struct.MetaData.electrical.Creator.Text = creator;

framework_struct.MetaData.electrical.CellName.Text = battery.name;
framework_struct.MetaData.electrical.ModelName.Text = battery.model;

[~,git_hash] = system('git rev-parse HEAD');
if isfield(battery.metadata,'SimpleFit_CommitHash')
    git_hash_fit = battery.metadata.SimpleFit_CommitHash;
else
    git_hash_fit = git_hash(1:40);
end

if strcmp(git_hash_fit, git_hash(1:40))
    framework_struct.MetaData.electrical.SimpleFit_CommitHash.Text = git_hash(1:40);
else
    framework_struct.MetaData.electrical.ExportSimpleFit_CommitHash.Text = git_hash(1:40);
    framework_struct.MetaData.electrical.FitSimpleFit_CommitHash.Text = git_hash_fit;
end

end

