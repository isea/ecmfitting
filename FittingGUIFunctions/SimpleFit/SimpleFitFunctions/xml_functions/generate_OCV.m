function [xml_struct] = generate_OCV(xml_struct,model_name,SOC,temp,varargin)
%GENERATE_OCV Summary of this function goes here
%   Detailed explanation goes here
switch numel(varargin)
    case 0
        return
    case 1
        if ~isa(varargin{1},'QOCV')
           return; 
        end
        data = zeros(length(temp),length(SOC));
        for i=1:length(temp)
            [inputSOC,indices] = unique(varargin{1}(i).SOC,'stable');
            voltage = varargin{1}(i).voltage(indices);
            data(i,:) = interp1(inputSOC,voltage,SOC,'spline');
        end
    case 2
        if ~isa(varargin{1},'QOCV')&&~isa(varargin{2},'QOCV')
           return; 
        end
        
        dataA = zeros(length(temp),length(SOC));
        for i=1:length(temp)
            [inputSOC,indices] = unique(varargin{1}(i).SOC,'stable');
            voltage = varargin{1}(i).voltage(indices);
            dataA(i,:) = interp1(inputSOC,voltage,SOC,'spline');
        end
        
        dataB = zeros(length(temp),length(SOC));
        for i=1:length(temp)
            [inputSOC,indices] = unique(varargin{2}(i).SOC,'stable');
            voltage = varargin{2}(i).voltage(indices);
            dataB(i,:) = interp1(inputSOC,voltage,SOC,'spline');
        end
        
        data = (dataA+dataB)./2;
end
data(data<0.5) = 0.5;

xml_struct.CustomDefinitions.MyOCV.Attributes.class = 'VoltageSource';
xml_struct.CustomDefinitions.MyOCV.Object = generate_Lookup2d(data,SOC,temp,[],[],0);

xml_struct.CustomDefinitions.(['My',model_name]).Children.VoltageSource.text = '';
xml_struct.CustomDefinitions.(['My',model_name]).Children.VoltageSource.Attributes.ref = 'MyOCV';

end

