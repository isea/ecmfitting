function [Data_lookup,newSOC,newTemp,newCrate,newHyst] = convert_Fit_into_DataArray(app,fit,SOC,temp,SOH,stepsize,check,Hyst,Crate)
%generiert aus der Fit Struktur ein Array folgender Stuktur:
% 1Dim = Hysteresis
% 2Dim = C rate
% 3Dim = Temp
% 4Dim = SOC
% 5Dim = Parameter
% WICHTIG!: Vor dieser Funktion Fit Struktur in allen Dimensionen
% aufsteigend sortieren

%Daten werden in 5% Schritten interpoliert, um ein gleichm��iges Array zu
%erhalten

% new Start Step
newTemp = [];
newCrate = [];
newHyst = [];

if app.HysteresisFlag && app.CRateFlag
    
    %%potentiell werden hier in Schleifen Daten �berschrieben und Reihenfolge der Interpolationen entlang der Dimensionen falsch
    param_length = 0;
    for m = 1:length(Hyst)
        for n = 1:length(app.sorted_crate{m})
            for n1 = 1:length(app.sorted_temp{m}{n})
                for n2 = 1:length(app.sorted_soc{m}{n}{n1})
                    if ~isempty(fit(m,n,n2,n1,SOH).Parameter)
                        param_length = length(fit(m,n,n2,n1,SOH).Parameter);
                        break;
                    end
                end
            end
        end
    end
    
    newHyst = Hyst;
    %which temperatures and SOCs are interpolated
    newSOC = [-10:stepsize:100];
    if check
        temp1=[newSOC,SOC];
        newSOC=unique(temp1);
    end
    iSOC = [];
    
    newTemp = [floor(min(temp)/10)*10:10:ceil(max(temp)/10)*10];
    iTemp = [];
    tTemp = [];
    
    newCrate = [floor(min(Crate)) :ceil(max(Crate))];
    tCrate = [];
    iCrate = [];
    
    Data_lookup = zeros(length(Hyst),length(newCrate),length(newTemp),length(newSOC),param_length);
    
    %iterate over all Crates
    for n = 1:length(Hyst)
        for i=1:length(app.sorted_crate{n})
            tCrate(i) = app.sorted_crate{n}(i);
            iCrate(i) = find(Crate == app.sorted_crate{n}(i));
        end
        Data_lookup1{n} = [];
        for n1 = iCrate
            for i=1:length(app.sorted_temp{n}{n1})
                tTemp(i) = app.sorted_temp{n}{n1}(i);
                iTemp(i) = find(temp == app.sorted_temp{n}{n1}(i));
            end
            Data_lookup1{n} = zeros(1,length(Crate),length(temp),length(newSOC),param_length);
            
            % Gehe durch alle Temperaturen
            for itemp = iTemp
                % Entferne leere Eintr�ge
                j=1;
                tSOC = [];
                iSOC = [];
                for i=1:length(SOC)
                    if ~isempty(fit(n,n1,i,itemp,SOH).Parameter)
                        tSOC(j) = SOC(i);
                        iSOC(j) = i;
                        j =j+1;
                    end
                end
                
                %find parameter length
                for i = iSOC
                    if ~isempty(fit(n,n1,i,itemp,SOH).Parameter)
                        param_length = length(fit(n,n1,i,itemp,SOH).Parameter);
                        break;
                    end
                end
                
                %Alle vorhandenen Daten sammeln
                %Durch alle Parameter iterieren
                for k=1:param_length
                    param = zeros(1,length(tSOC));
                    for i=1:length(tSOC)
                        param(i) = fit(n,n1,iSOC(i),itemp,SOH).Parameter(k);
                    end
                    if length(tSOC) ==1
                        % use constant parameter over SOC range, if only one SOC was
                        % tested
                        new_param = interp1([min(newSOC);tSOC],[param;param],newSOC,'linear','extrap');
                    else
                        new_param = interp1(tSOC,param,newSOC,'linear','extrap');
                    end
                    for n2 = 1:length(new_param)
                        if new_param(n2) < 0
                            new_param(n2) = 1e-9;
                        end
                    end
                    Data_lookup(n,n1,itemp,:,k) = new_param;
                end
            end
            
            for i1 = 1:length(newSOC)
                %Alle vorhandenen Daten sammeln
                %Durch alle Parameter iterieren
                for k=1:param_length
                    param = zeros(1,length(tTemp));
                    for i=1:length(tTemp)
                        param(i) = Data_lookup1(n,n1,i,i1,k);
                    end
                    if length(tTemp) ==1
                        % use constant parameter over SOC range, if only one SOC was
                        % tested
                        new_param = interp1([min(newTemp);tTemp],[param;param],newTemp,'linear','extrap');
                    else
                        new_param = interp1(tTemp,param,newTemp,'linear','extrap');
                    end
                    for n2 = 1:length(new_param)
                        if new_param(n2) < 0
                            new_param(n2) = 1e-9;
                        end
                    end
                    Data_lookup(n,n1,:,i1,k) = new_param;
                end
            end
        end
        
        for i2 = 1:length(newTemp)
            for i1 = 1:length(newSOC)
                %Alle vorhandenen Daten sammeln
                %Durch alle Parameter iterieren
                for k=1:param_length
                    param = zeros(1,length(tCrate));
                    for i=1:length(tCrate)
                        param(i) = Data_lookup1(n,i,i2,i1,k);
                    end
                    if length(tCrate) ==1
                        % use constant parameter over SOC range, if only one SOC was
                        % tested
                        new_param = interp1([min(newCrate);tCrate],[param;param],newCrate,'linear','extrap');
                    else
                        new_param = interp1(tCrate,param,newCrate,'linear','extrap');
                    end
                    for n2 = 1:length(new_param)
                        if new_param(n2) < 0
                            new_param(n2) = 1e-9;
                        end
                    end
                    Data_lookup(n,:,i2,i1,k) = new_param;
                end
            end
        end
    end
elseif app.HysteresisFlag && length(Hyst) > 1
    %reihenfolge falsch
    newCrate = 0;
    newHyst = Hyst;
    %which temperatures and SOCs are interpolated
    newSOC = [-10:stepsize:100];
    if check
        temp1=[newSOC,SOC];
        newSOC=unique(temp1);
    end
    iSOC = [];
    
    newTemp = [floor(min(temp)/10)*10:10:ceil(max(temp)/10)*10];
    iTemp = [];
    tTemp = [];
    
    param_length = 0;
    for n = 1:length(Crate)
        for n1 = 1:length(app.sorted_temp{1}{n})
            for n2 = 1:length(app.sorted_soc{1}{n}{n1})
                if ~isempty(fit(1,n,n2,n1,SOH).Parameter)
                    param_length = length(fit(1,n,n2,n1,SOH).Parameter);
                    break;
                end
            end
        end
    end
    Data_lookup = zeros(length(Hyst),1,length(newTemp),length(newSOC),param_length);
    Data_lookup1 = zeros(length(Hyst),1,length(temp),length(newSOC),param_length);
    %iterate over all Crates
    for n = 1:length(Hyst)
        for i=1:length(app.sorted_temp{n}{1})
            tTemp(i) = app.sorted_temp{n}{1}(i);
            iTemp(i) = find(temp == app.sorted_temp{n}{1}(i));
        end    
        % Gehe durch alle Temperaturen
        for itemp = iTemp
            % Entferne leere Eintr�ge
            j=1;
            tSOC = [];
            iSOC = [];
            for i=1:length(SOC)
                if ~isempty(fit(n,1,i,itemp,SOH).Parameter)
                    tSOC(j) = SOC(i);
                    iSOC(j) = i;
                    j =j+1;
                end
            end
            
            %find parameter length
            for i = iSOC
                if ~isempty(fit(n,1,i,itemp,SOH).Parameter)
                    param_length = length(fit(n,1,i,itemp,SOH).Parameter);
                    break;
                end
            end
            
            %Alle vorhandenen Daten sammeln
            %Durch alle Parameter iterieren
            for k=1:param_length
                param = zeros(1,length(tSOC));
                for i=1:length(tSOC)
                    param(i) = fit(n,1,iSOC(i),itemp,SOH).Parameter(k);
                end
                if length(tSOC) ==1
                    % use constant parameter over SOC range, if only one SOC was
                    % tested
                    new_param = interp1([min(newSOC);tSOC],[param;param],newSOC,'linear','extrap');
                else
                    new_param = interp1(tSOC,param,newSOC,'linear','extrap');
                end
                for n1 = 1:length(new_param)
                    if new_param(n1) < 0
                        new_param(n1) = 1e-9;
                    end
                end
                Data_lookup1(n,1,itemp,:,k) = new_param;
            end
        end
        
        for i1 = 1:length(newSOC)
            %Alle vorhandenen Daten sammeln
            %Durch alle Parameter iterieren
            for k=1:param_length
                param = zeros(1,length(tTemp));
                for i=1:length(tTemp)
                    param(i) = Data_lookup1(n,1,i,i1,k);
                end
                if length(tTemp) ==1
                    % use constant parameter over SOC range, if only one SOC was
                    % tested
                    new_param = interp1([min(newTemp);tTemp],[param;param],newTemp,'linear','extrap');
                else
                    new_param = interp1(tTemp,param,newTemp,'linear','extrap');
                end
                for n1 = 1:length(new_param)
                    if new_param(n1) < 0
                        new_param(n1) = 1e-9;
                    end
                end
                Data_lookup(n,1,:,i1,k) = new_param;
            end
        end
    end
elseif app.CRateFlag && length(Crate) > 1
    newCrate = Crate;
    newHyst = 0;
    %which temperatures and SOCs are interpolated
    newSOC = [-10:stepsize:100];
    if check
        temp1=[newSOC,SOC];
        newSOC=unique(temp1);
    end
    iSOC = [];
    
    if length(temp) > 1
        newTemp = [floor(min(temp)/10)*10:10:ceil(max(temp)/10)*10];
    else
        newTemp = temp;
    end
    iTemp = [];
    tTemp = [];
    
    param_length = 0;
    for n = 1:length(Crate)
        for n1 = 1:length(app.sorted_temp{1}{n})
            for n2 = 1:length(app.sorted_soc{1}{n}{n1})
                if ~isempty(fit(1,n,n2,n1,SOH).Parameter)
                    param_length = length(fit(1,n,n2,n1,SOH).Parameter);
                    break;
                end
            end
        end
    end
    Data_lookup = zeros(1,length(newCrate),length(newTemp),length(newSOC),param_length);
    Data_lookup1 = zeros(1,length(newCrate),length(temp),length(newSOC),param_length);
    fit1 = fit;
    
    %iterate over all temperatures
    for i = 1:length(temp)
        %%Interpolation over C rates
        %iterate over all SOCs
        for i1 = 1:length(SOC)
            %iterate over all parameters
            for k = 1:param_length
                %interpolate along C Rate dimension
                counter = 1;
                param = [];
                tCrate = [];
                iCrate = [];
                for i2 = 1:length(Crate)
                    if ~isempty(fit(1,i2,i1,i,SOH).Parameter)
                        param(counter) = fit(1,i2,i1,i,SOH).Parameter(k);
                        tCrate(counter) = Crate(i2);
                        iCrate(counter) = i2;
                        counter  = counter + 1;
                    end
                end
                if isempty(param)
                   continue 
                end
                
                if length(tCrate) <= 1
                    % use constant parameter over SOC range, if only one SOC was
                    % tested
                    new_param = param * ones(size(newCrate));
                else
                    new_param = interp1(tCrate,param,newCrate,'linear','extrap');
                end
                for n1 = 1:length(new_param)
                    if new_param(n1) < 0
                        new_param(n1) = 1e-9;
                    end
                end                

                for n2 = 1:length(newCrate)
                    if ~isempty(fit1(1,n2,i1,i,SOH).Parameter)
                        fit1(1,n2,i1,i,SOH).Parameter(k) = new_param(n2);
                    else
                        new_fit = Fit();
                        %save new entry of class Fit
                        new_fit.Parameter = zeros(1,param_length);
                        new_fit.Parameter(k) = new_param(n2);
                        %dummy data
                        new_fit.Parameter_min = zeros(1,param_length);
                        new_fit.Parameter_max = Inf * ones(1,param_length);
                        new_fit.ParFix = zeros(1,param_length);
                        new_fit.Zreal = zeros(1,param_length);
                        new_fit.Zimag = zeros(1,param_length);
                        new_fit.Residuum = 0;
                        fit1(1,n2,i1,i,SOH) = new_fit;
                        new_fit = [];
                    end
                end
            end
        end
        
        %%Interpolation over SOCs
        %iterate over all C rates
        for i2 = 1:length(newCrate)
            %iterate over all parameters
            for k = 1:param_length
                %interpolate along SOC dimension
                counter = 1;
                param = [];
                tSOC = [];
                iSOC = [];
                for i1 = 1:length(SOC)
                    if ~isempty(fit1(1,i2,i1,i,SOH).Parameter)
                        param(counter) = fit1(1,i2,i1,i,SOH).Parameter(k);
                        tSOC(counter) = SOC(i1);
                        iSOC(counter) = i1;
                        counter  = counter + 1;
                    end
                end
                
                if isempty(param)
                   continue 
                end
                
                if length(tSOC) <=1
                    % use constant parameter over SOC range, if only one SOC was
                    % tested
                    new_param = param * ones(size(newSOC));
                else
                    new_param = interp1(tSOC,param,newSOC,'linear','extrap');
                end
                
                for n1 = 1:length(new_param)
                    if new_param(n1) < 0
                        new_param(n1) = 1e-9;
                    end
                end                
                Data_lookup1(1,i2,i,:,k) = new_param;        
            end
        end
    end
    %% Interpolation over all temperatures
    %iterate over all SOCs
    for i1 = 1:length(SOC)
        %iterate over all C rates
        for i2 = 1:length(newCrate)
            %iterate over all parameters
            for k = 1:param_length
                %interpolate along temperature dimension
                counter = 1;
                param = [];
                tTemp = [];
                iTemp = [];
                for i = 1:length(temp)
                    if find(Data_lookup1(1,i2,i,i1,k))
                        param(counter) = Data_lookup1(1,i2,i,i1,k);                        
                        tTemp(counter) = temp(i);
                        iTemp(counter) = i;
                        counter  = counter + 1;
                    end
                end
                
                if length(tTemp) ==1
                    % use constant parameter over SOC range, if only one SOC was
                    % tested
                    new_param = param * ones(size(newTemp));
                else
                    try
                    new_param = interp1(tTemp,param,newTemp,'linear','extrap');
                    catch
                    end
                end
                
                for n1 = 1:length(new_param)
                    if new_param(n1) < 0
                        new_param(n1) = 1e-9;
                    end
                end                          
                Data_lookup(1,i2,:,i1,k) = new_param; 
            end
        end        
    end 
else
    newCrate = 0;
    newHyst = 0;
    %considered SOCs
    newSOC = [-10:stepsize:100];
    if check
        temp1=[newSOC,SOC];
        newSOC=unique(temp1);
    end
    iSOC = [];
    %note that all different SOCs are present so if one temp has 3.5345
    % as their first SOC, probably no other temp has a value for
    % exactly this SOC
    for i=1:length(temp)
        if ~isempty(fit(1,1,1,i,SOH).Parameter)
            param_length = length(fit(1,1,1,i,SOH).Parameter);
            break;
        end
    end
    Data_lookup = zeros(1,1,length(temp),length(newSOC),param_length);
    
    % Gehe durch alle Temperaturen
    for itemp=1:length(temp)
        % Entferne leere Eintr�ge
        j=1;
        tSOC = [];
        for i=1:length(SOC)
            if ~isempty(fit(1,1,i,itemp,SOH).Parameter)
                tSOC(j) = SOC(i);
                iSOC(j) = i;
                j =j+1;
            end
        end
        %Alle vorhandenen Daten sammeln
        %Durch alle Parameter iterieren
        for k=1:param_length
            param = zeros(1,length(tSOC));
            for i=1:length(tSOC)
                param(i) = fit(1,1,iSOC(i),itemp,SOH).Parameter(k);
            end
            if length(tSOC) ==1
                % use constant parameter over SOC range, if only one SOC was
                % tested
                new_param = interp1([min(newSOC);tSOC],[param;param],newSOC,'linear','extrap');
            else
                new_param = interp1(tSOC,param,newSOC,'linear','extrap');
            end
            for n = 1:length(new_param)
                if new_param(n) < 0
                    new_param(n) = 1e-9;
                end
            end
            Data_lookup(1,1,itemp,:,k) = new_param;
        end
    end
    newTemp = temp;
end
end

