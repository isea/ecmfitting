function [Lookup] = generate_Lookup2d(data,SOC,temp,Crate,Hyst,LHD)
%GENERATE_LOOKUP2D Summary of this function goes here
%   Detailed explanation goes here
%%
Lookup.RowState.Text = '';
Lookup.RowState.Attributes.cacheref = 'Soc';
Lookup.ColState.Text = '';
if (length(Crate) == 1 && length(Hyst) == 1 ) || (isempty(Crate) && isempty(Hyst))
    Lookup.ColState.Attributes.cacheref = 'ThermalState';
elseif length(Crate) == 1 && length(Hyst) > 1
    Lookup.ColState.Attributes.cacheref = 'ThermalState';
    Lookup.SliState.Attributes.cacheref = 'HystState';
elseif length(Crate) > 1 && length(Hyst) == 1
    Lookup.SliState.Attributes.cacheref = 'CRateState';
    Lookup.ColState.Attributes.cacheref = 'ThermalState';
else
    return
    Lookup.ColState.Attributes.cacheref = 'CrateHystThermalState';
end

if LHD
    Lookup.VirtualState.Attributes.cacheref = 'LHDState';
end

sData = mat2str(data);
sData = strrep(sData(2:end-1), ' ' , ', ');
sData = strrep(sData, ';' , [';' newline] );
sData = [newline sData newline];

Lookup.LookupData.Text = sData;

sSoc = mat2str(SOC);
if length(SOC) ~= 1
    sSoc = strrep(sSoc(2:end-1), ' ' , ', ');
end
sSoc = [newline sSoc newline];

Lookup.MeasurementPointsRow.Text = sSoc;
Lookup.MeasurementPointsRow.Attributes.desc = 'StateOfCharge';

if (length(Crate) == 1 && length(Hyst) == 1) || (isempty(Crate) && isempty(Hyst))
    
    sTemp = mat2str(temp);
    if length(temp) ~= 1
        sTemp = strrep(sTemp(2:end-1), ' ' , ', ');
    end
    sTemp = [newline sTemp newline];
    
    Lookup.MeasurementPointsColumn.Text = sTemp;
    Lookup.MeasurementPointsColumn.Attributes.desc = 'ThermalState';
    Lookup.Attributes.class = 'LookupObj2dWithState';
elseif length(Crate) == 1 && length(Hyst) > 1
    %         sState = '(';
    sState = [];
    for n1 = 1:length(temp)
        sState = [sState, num2str(temp(n1)), ','];
    end
    sState(end) = [];
    sState = [newline sState newline];
    
    Lookup.MeasurementPointsColumn.Text = sState;
    Lookup.MeasurementPointsColumn.Attributes.desc = 'ThermalState';
    
    sState = [];
    for n1 = 1:length(Hyst)
        sState = [sState, num2str(Hyst(n1)), ','];
    end
    sState(end) = [];
    sState = [newline sState newline];
    
    Lookup.MeasurementPointsSlice.Text = sState;
    Lookup.MeasurementPointsSlice.Attributes.desc = 'HystState';
    Lookup.Attributes.class = 'LookupObj3dWithState';
elseif length(Crate) > 1 && length(Hyst) == 1
    sState = [];
    for n1 = 1:length(temp)
        sState = [sState, num2str(temp(n1)), ','];
    end
    sState(end) = [];
    sState = [newline sState newline];
    
    Lookup.MeasurementPointsColumn.Text = sState;
    Lookup.MeasurementPointsColumn.Attributes.desc = 'ThermalState';
    
    sState = [];
    for n1 = 1:length(Crate)
        sState = [sState, num2str(Crate(n1)), ','];
    end
    sState(end) = [];
    sState = [newline sState newline];
    
    Lookup.MeasurementPointsSlice.Text = sState;
    Lookup.MeasurementPointsSlice.Attributes.desc = 'CRateState';
    Lookup.Attributes.class = 'LookupObj3dWithState';
else
    %         sState = '(';
    sState = [];
    for n = 1:length(Hyst)
        for n1 = 1:length(Crate)
            for n2 = 1:length(temp)
                %                     sState = [sState, num2str(Hyst(n)), ',', num2str(Crate(n)), ',', num2str(temp(n1)), '),(' ];
                sState = [sState, num2str(Hyst(n)), ',',  num2str(Crate(n)), ',', num2str(temp(n1)), ';'];
            end
        end
    end
    sState(end-1:end) = [];
    sState = [newline sState newline];
    
    Lookup.MeasurementPointsColumn.Text = sState;
    Lookup.MeasurementPointsColumn.Attributes.desc = 'HystCrateThermalState';
    Lookup.Attributes.class = 'LookupObj4dWithState';
end
if LHD
    
end
end

