function varargout = choosePointsGui(varargin)
% CHOOSEPOINTSGUI MATLAB code for choosePointsGui.fig
%      CHOOSEPOINTSGUI, by itself, creates a new CHOOSEPOINTSGUI or raises the existing
%      singleton*.
%
%      H = CHOOSEPOINTSGUI returns the handle to a new CHOOSEPOINTSGUI or the handle to
%      the existing singleton*.
%
%      CHOOSEPOINTSGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CHOOSEPOINTSGUI.M with the given input arguments.
%
%      CHOOSEPOINTSGUI('Property','Value',...) creates a new CHOOSEPOINTSGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before choosePointsGui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to choosePointsGui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help choosePointsGui

% Last Modified by GUIDE v2.5 22-Dec-2017 17:26:39

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @choosePointsGui_OpeningFcn, ...
                   'gui_OutputFcn',  @choosePointsGui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
end
function txt = getPoint(~,event_obj,handles)
pos = event_obj.Position;
  disp(['You clicked X:',num2str(pos(1)),', Y:',num2str(pos(2))]);
  txt = {''};
  EISdata = handles.axes1.UserData.EISdata;
  EISdata_removed = handles.axes1.UserData.EISdata_removed;
  
  index = find(EISdata.Zreal==pos(1));
  EISdata_removed.Zreal(index) = EISdata.Zreal(index);
  EISdata_removed.Zimg(index) = EISdata.Zimg(index);
  EISdata_removed.frequenz(index) = EISdata.frequenz(index); 
  EISdata.Zreal(index) = [];
  EISdata.Zimg(index) = [];
  EISdata.frequenz(index) = [];
  
  event_obj.Target.XData(event_obj.Target.XData==pos(1)) = nan;
  event_obj.Target.YData(event_obj.Target.YData==pos(2)) = nan;
%   
%   hold(handles.axes1,'on');
%   
%   if length(handles.axes1.Children)==1
%       plot(handles.axes1,EISdata_removed.Zreal,EISdata_removed.Zimg,'o','LineWidth',2,'Color',[20 20 20]);
%   else
%       handles.axes1.Children(2).XData = EISdata_removed.Zreal;
%       handles.axes1.Children(2).YData = EISdata_removed.Zimg;
%   end
%   handles.axes1.UserData.EISdata = EISdata;
%   handles.axes1.UserData.EISdata_removed = EISdata_removed;
  
  
end

% --- Executes just before choosePointsGui is made visible.
function choosePointsGui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to choosePointsGui (see VARARGIN)

% Choose default command line output for choosePointsGui
handles.output = hObject;


EISdata = varargin{1};
plot(EISdata.Zreal,EISdata.Zimg,'o','DisplayName','Messung','LineWidth',2);


grid on;

ax = gca;
ax.XLabel.Interpreter = 'latex';
ax.XLabel.String = '$\Re\{\underline Z\}$ in $\Omega$';
ax.YLabel.Interpreter = 'latex';
ax.YLabel.String = '$\Im\{\underline Z\}$ in $\Omega$';
ax.YDir = 'reverse';
ax.DataAspectRatioMode = 'manual';
ax.DataAspectRatio = [1 1 1];
ax.UserData.EISdata = EISdata;
ax.UserData.EISdata_removed = EISData();


h1 = legend('show');
set(h1,'Interpreter','none','Location','NorthWest');

%datacursormode on;
%dcm_obj = datacursormode(gcf);
%dcm_obj.DisplayStyle = 'datatip';
%set(dcm_obj,'UpdateFcn',{@getPoint,handles});

brush on;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes choosePointsGui wait for user response (see UIRESUME)
uiwait(handles.figure1);
end




% --- Outputs from this function are returned to the command line.
function varargout = choosePointsGui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
%varargout{1} = handles.output;
EISdata = EISData();
EISdata.Zreal = handles.axes1.Children(1).XData;
EISdata.Zimg = handles.axes1.Children(1).YData;
varargout{1} = EISdata;
delete(hObject);
end

% --- Executes on button press in exit_button.
function exit_button_Callback(hObject, eventdata, handles)
% hObject    handle to exit_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
figure1_CloseRequestFcn(handles.figure1, eventdata, handles);
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
uiresume(hObject);
end
