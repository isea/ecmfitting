classdef PulsData
    %PULSDATA Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        voltage
        current
        temperature
        time
    end
    
    methods
        function obj = PulsData()
        end
        
        function outputArg = method1(obj,inputArg)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            outputArg = obj.Property1 + inputArg;
        end
    end
end

