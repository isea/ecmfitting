classdef Auswertung
    %Hauptklasse f�r alle Auswerteklassen, f�r jede Testroutine soll eine
    %eignene Auswerteklasse erstellt werden, die von dieser Klasse
    %abgeleitet ist
    
    properties
        name;
    end
    
    methods
        function obj = Auswertung()
            %Constructor
        end
        
        function capacity = getCapacity(obj,diga)
            capacity = [];
        end
        function [qOCV_CH,qOCV_DCH] = getqOCV(obj,diga)
            qOCV_CH = QOCV();
            qOCV_DCH = QOCV();
        end
        function temp = getTemp(obj,diga)
            temp = [];
        end
        function SOC = getSOCs(obj,diga)
            SOC = [];
        end
        function relax = getRelax(obj,diga, iSOC)
            relax = RelaxData();
        end
        function puls = getPuls(obj,diga,index)
            puls = [];
        end
    end
end

