classdef Battery
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        name
        capacity
        nom_cap
        model
        auswertung_name
        temp
        SOC
        SOH
        Hyst
        Crate
        fit = Fit();
        lhd = LHD();
        EISdata = EISData();
        EISdata_clean = EISData();
        relaxdata = RelaxData();
        qOCV_CH = QOCV();
        qOCV_DCH = QOCV();
        metadata;
    end
    
    methods
        function obj = Battery(name)
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
            obj.name = name;
        end
        function obj = setAuswertung(obj,name,actSOH)
           obj.auswertung_name{actSOH} = name; 
        end
        function obj = addCapacity(obj,capacity,actTemp,actSOH,actHyst,actCrate)
           obj.capacity(actHyst,actCrate,actTemp,actSOH) = capacity; 
        end
        function obj = setNomCapacity(obj,capacity)
           obj.nom_cap = capacity; 
        end
        
        function obj = addFit(obj,Fit,actSOC,actTemp,actSOH,actHyst,actCrate)
            %Ablegen eines Fittes
            obj.fit(actHyst,actCrate,actSOC,actTemp,actSOH) = Fit;
        end
        function Fit = getFit(obj,actSOC,actTemp,actSOH,actHyst,actCrate)
            %Auslesen eines Fittes
            try
                Fit = obj.fit(actHyst,actCrate,actSOC,actTemp,actSOH);
            catch
               Fit = []; 
            end
        end
        function obj = addEISdata(obj,EISdata,actSOC,actTemp,actSOH,actHyst,actCrate)
            %Ablegen eines Fittes
            obj.EISdata(actHyst,actCrate,actSOC,actTemp,actSOH) = EISdata;
        end
        
        function obj = addEISdata_clean(obj,EISdata,actSOC,actTemp,actSOH,actHyst,actCrate)
            %Ablegen eines Fittes
            obj.EISdata_clean(actHyst,actCrate,actSOC,actTemp,actSOH) = EISdata;
        end
        
        function obj = addEISdata_allSOC(obj,EISdata,actTemp,actSOH,actHyst,actCrate)
            %Ablegen eines Fittes
            if length(obj.EISdata) == 1
                obj.EISdata(length(EISdata),1,1) = EISData();
            end
            obj.EISdata(actHyst,actCrate,:,actTemp,actSOH) = EISdata;
        end
        
        function obj = addRelaxdata(obj,relaxdata,actSOC,actTemp,actSOH,actHyst,actCrate)
            %Ablegen eines Fittes
            obj.relaxdata(actHyst,actCrate,actSOC,actTemp,actSOH) = relaxdata;
        end
        function obj = addqOCV_CH(obj,qOCV,actTemp,actSOH,actHyst,actCrate)
            %Ablegen eines Fittes
            obj.qOCV_CH(actTemp,actSOH) = qOCV;
        end
        function obj = addqOCV_DCH(obj,qOCV,actTemp,actSOH,actHyst,actCrate)
            %Ablegen eines Fittes
            obj.qOCV_DCH(actTemp,actSOH) = qOCV;
        end
        
        function obj = addSOCtable(obj,SOC)
            %Ablegen der SOC Tabelle im Batterieobjekt
            %Automatische Erweiterung der Speicherarrays
           obj.SOC = SOC;
%            obj.fit(length(SOC),1,1) = Fit();
%            obj.EISdata(length(SOC),1,1) = EISData();
%            obj.relaxdata(length(SOC),1,1) = RelaxData();
        end
        function obj = addTemptable(obj,temp)
            %Ablegen der SOC Tabelle im Batterieobjekt
            %Automatische Erweiterung der Speicherarrays
           obj.temp = temp;
%            obj.fit(1,length(temp),1) = Fit();
%            obj.EISdata(1,length(temp),1) = EISData();
%            obj.relaxdata(1,length(temp),1) = RelaxData();
%            obj.qOCV_CH(length(temp),1) = QOCV();
%            obj.qOCV_DCH(length(temp),1) = QOCV();
        end
        function obj = addSOHtable(obj,SOH)
            %Ablegen der SOC Tabelle im Batterieobjekt
            %Automatische Erweiterung der Speicherarrays
           obj.SOH = SOH;
%            obj.fit(1,1,length(SOH)) = Fit();
%            obj.EISdata(1,1,length(SOH)) = EISData();
%            obj.relaxdata(1,1,length(SOH)) = RelaxData();
%            obj.qOCV_CH(1,length(SOH)) = QOCV();
%            obj.qOCV_DCH(1,length(SOH)) = QOCV();
        end
        function obj = addHysttable(obj,Hyst)
            obj.Hyst = Hyst;
        end
        function obj = addCratetable(obj,Crate)
            obj.Crate = Crate;
        end
        function obj = setModel(obj, model)
           obj.model = model; 
        end
        function obj = deleteAllFits(obj)
           clear obj.fit;
           obj.fit = Fit();
        end
        function obj = deleteAllLHD(obj)
            clear obj.LHD;
            obj.lhd = LHD();
        end
        function obj = addLHD(obj,LHD,actSOC,actTemp,actSOH,actHyst,actCrate)
%             if isempty(obj.lhd)
%                obj.lhd(actHyst,actCrate,actSOC,actTemp,actSOH) = [];
%             end
            obj.lhd(actHyst,actCrate,actSOC,actTemp,actSOH) = LHD;
        end
    end
end

