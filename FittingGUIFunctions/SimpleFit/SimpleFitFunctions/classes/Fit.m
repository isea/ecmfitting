classdef Fit
    %FIT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Parameter
        Parameter_min
        Parameter_max
        ParFix
        Zreal
        Zimag
        Residuum
    end
    
    methods
        function obj = Fit()
            %FIT Construct an instance of this class
            %   Detailed explanation goes here
        end
        
        function outputArg = method1(obj,inputArg)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            outputArg = obj.Property1 + inputArg;
        end
    end
end

