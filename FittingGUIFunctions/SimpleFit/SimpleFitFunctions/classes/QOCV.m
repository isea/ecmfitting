classdef QOCV
    %Quasi open circuit voltage
    %%%!!!Keep class definition equal to QOCV class definiton in
    %%%ICPD!!!%%%
    
    properties
        SOC double
        voltage double
        Type char
        Temperature double
    end
    
    methods
        function obj = QOCV(SOC,voltage,Type,Temperature)
            %constructor
            if exist('SOC','var')
                obj.SOC = SOC;
            else
                obj.SOC = [];
            end
            
            if exist('voltage','var')
                obj.voltage = voltage;
            else
                obj.voltage = [];
            end
            
            if exist('Type','var')
                obj.Type = Type;
            else
                obj.SOC = '';
            end
            
            if exist('Temperature','var')
                obj.Temperature = Temperature;
            else
                obj.Temperature = [];
            end
            
        end
        
    end
    
     methods (Access = public)
        function output = GetProperty(obj, property)
            if isprop(obj,property)
                output = obj.(property);
            else
               warning([property,' is not a Property of class QOCV return 0']);
               output = 0;
            end
        end
        
        function obj = SetProperty(obj,property,value)
            if isprop(obj,property)
                obj.(property) = value; 
            else
               error([property,' is not a Property of class QOCV']); 
            end
        end
    end
end

