classdef ESBelement
    %Hauptklasse f�r alle ESB Elemente
    
    properties (Access = protected)
        Funktionsname
        Frameworkname
        inputs
        Zfun
        Zfun_HF
        Zfun_MF
        Zfun_LF
    end
    
    methods
        function obj = ESBelement()
        end
        
        function name = getName(obj)
            name = obj.Funktionsname;
        end
        function name = getFrameName(obj)
            name = obj.Frameworkname;
        end
        
        
        function inputs = getInputs(obj)
            inputs = obj.inputs;
        end
        function [Zfun, Zfun_HF, Zfun_MF, Zfun_LF] = getZfun(obj)
            Zfun = obj.Zfun;
            Zfun_HF = obj.Zfun_HF;
            Zfun_MF = obj.Zfun_MF;
            Zfun_LF = obj.Zfun_LF;
        end
        

    end
end

