classdef ESB
    %ESB Hauptklasse f�r alle Ersatzschaltbilder (Modelle)
    
    properties (Access = public)
        Name
        model
        elemente
        LHD_elemente
        Appendix
        Formel
        combinedParam
        ParameterNamen
        ParameterInit
        ParameterMin
        ParameterMax
        ParameterFix
    end
    
    methods
        function obj = ESB()

        end
        
        function formula = getRechenFormula(obj)
            formel_p = obj.Formel;
            for i=1:numel(obj.ParameterNamen)
                formel_p = strrep(formel_p,obj.ParameterNamen{i},['p(',num2str(i),')']);
            end
            formula = formel_p;
        end
        
        function name = getModelName(obj)
           name = obj.Name; 
        end
        
        function parNames = getParameterNames(obj)
           parNames = obj.ParameterNamen; 
        end
        
        function [p_init,p_min,p_max,p_fix] = getParameters(obj)
            p_init = obj.ParameterInit;
            p_min = obj.ParameterMin;
            p_max = obj.ParameterMax;
            p_fix = obj.ParameterFix;
        end
        
        function xml_struct = generateXMLstruct(obj,app,fit,SOC,temp,iSOH,capacity,stepsize,check,Hyst,Crate)
            addpath('FittingGUIFunctions/SimpleFit/SimpleFitFunctions/xml_functions');
            [data,newSOC,newTemp,newCrate,newHyst] = convert_Fit_into_DataArray(app,fit,SOC,temp,iSOH,stepsize,check,Hyst,Crate);
            if app.LHDCheckBox_2.Value
                [data_LHD] = convert_Fit_into_DataArray_LHD(app,app.actBattery.lhd,SOC,temp,iSOH,stepsize,check,Hyst,Crate);
            end
            %obj.elemente wird in change_act_model gesetzt
            number_of_model_parameters = 0;
            for i=1:length(obj.elemente)
                if ~isa(obj.elemente{i},'SF_ESBe_TimeConstant')
                    elem_parameter = obj.elemente{i}.getInputs();
                    for j=1:numel(elem_parameter)
                       act_parameter = [elem_parameter{j} obj.Appendix{i}];
                       [a,~] = find(strcmp(obj.combinedParam,act_parameter));%Find combined Parameters
                       if ~isempty(a)
                           act_parameter = obj.combinedParam{a,1};
                       end
                       i_act_parameter = find(strcmp(act_parameter,obj.ParameterNamen));
                       temp_data = [];
                       elem_data{j} = [];
                       for n = 1:length(data(:,1,1,1,i_act_parameter))
                           for n1 = 1:length(data(1,:,1,1,i_act_parameter))
                               for n2 = 1:length(data(1,1,:,1,i_act_parameter))
                                   for n3 = 1:length(data(1,1,1,:,i_act_parameter))
                                       temp_data(n2,n3) = data(n,n1,n2,n3,i_act_parameter);
                                   end
                               end
                               elem_data{j} = [elem_data{j}; temp_data];
                           end
                       end   
                       number_of_model_parameters = number_of_model_parameters + 1;
                    end
                    frame_name = obj.elemente{i}.getFrameName();
                    if ~isempty(frame_name)%Element has no represenation in Framework, skip this element
                        for k=1:numel(frame_name)
                            %Add Custom Definition for each element
                            frame_lookups = obj.elemente{i}.generateXMLstruct(newSOC,newTemp,newCrate,newHyst,elem_data,app.LHDCheckBox_2.Value);
                            xml_struct.CustomDefinitions.(['My', frame_name{k}, obj.Appendix{i}]) = frame_lookups(k);
                        end
                    end                    
                else
                    %hier
                    elem_parameter = obj.elemente{i}.getInputs();
                    for j=1:numel(elem_parameter)
                       act_parameter = [elem_parameter{j} obj.Appendix{i}];
                       [a,~] = find(strcmp(obj.combinedParam,act_parameter));%Find combined Parameters
                       if ~isempty(a)
                           act_parameter = obj.combinedParam{a,1};
                       end
                       i_act_parameter = find(strcmp(act_parameter,obj.ParameterNamen)) - number_of_model_parameters;
                       temp_data = [];
                       elem_data{j} = [];
                       for n = 1:length(data_LHD(:,1,1,1,i_act_parameter))
                           for n1 = 1:length(data_LHD(1,:,1,1,i_act_parameter))
                               for n2 = 1:length(data_LHD(1,1,:,1,i_act_parameter))
                                   for n3 = 1:length(data_LHD(1,1,1,:,i_act_parameter))
                                       temp_data(n2,n3) = data_LHD(n,n1,n2,n3,i_act_parameter);
                                   end
                               end
                               elem_data{j} = [elem_data{j}; temp_data];
                           end
                       end                   
                    end
                    frame_name = obj.elemente{i}.getFrameName();
                    if ~isempty(frame_name)%Element has no represenation in Framework, skip this element
                        for k=1:numel(frame_name)
                            %Add Custom Definition for each element
                            %newCrate(newCrate== 0) = [];
                            frame_lookups = obj.elemente{i}.generateXMLstruct(newSOC,newTemp,newCrate,newHyst,elem_data,0);
                            xml_struct.CustomDefinitions.(['My', frame_name{k}, obj.Appendix{i}]) = frame_lookups(k);
%                             xml_struct.LHD = frame_lookups(k);                            
                        end
                    end    
                end
            end
            %Generate cell structure
            %Electric configuration of cell
            for i=1:length(obj.elemente)
                frame_name = obj.elemente{i}.getFrameName();
                for k=1:numel(frame_name)%%Cell representation if more than one element of same type
                    if isfield(xml_struct.CustomDefinitions,['My', obj.Name])
                        if isfield(xml_struct.CustomDefinitions.(['My', obj.Name]).Children,(frame_name{k}))
                            if iscell(xml_struct.CustomDefinitions.(['My', obj.Name]).Children.(frame_name{k}))
                                xml_struct.CustomDefinitions.(['My', obj.Name]).Children.(frame_name{k}){end+1}.text = '';
                                xml_struct.CustomDefinitions.(['My', obj.Name]).Children.(frame_name{k}){end}.Attributes.ref = ['My', frame_name{k}, obj.Appendix{i}];
                            else
                                temp_ref = xml_struct.CustomDefinitions.(['My', obj.Name]).Children.(frame_name{k}).Attributes.ref;
                                xml_struct.CustomDefinitions.(['My', obj.Name]).Children.(frame_name{k})=[];
                                xml_struct.CustomDefinitions.(['My', obj.Name]).Children.(frame_name{k}){1}.text = '';
                                xml_struct.CustomDefinitions.(['My', obj.Name]).Children.(frame_name{k}){1}.Attributes.ref = temp_ref;
                                xml_struct.CustomDefinitions.(['My', obj.Name]).Children.(frame_name{k}){2}.text = '';
                                xml_struct.CustomDefinitions.(['My', obj.Name]).Children.(frame_name{k}){2}.Attributes.ref = ['My', frame_name{k}, obj.Appendix{i}];
                                
                            end
                        else
                            xml_struct.CustomDefinitions.(['My', obj.Name]).Children.(frame_name{k}).text = '';
                            xml_struct.CustomDefinitions.(['My', obj.Name]).Children.(frame_name{k}).Attributes.ref = ['My', frame_name{k}, obj.Appendix{i}];
                        end
                    else
                        xml_struct.CustomDefinitions.(['My', obj.Name]).Children.(frame_name{k}).text = '';
                        xml_struct.CustomDefinitions.(['My', obj.Name]).Children.(frame_name{k}).Attributes.ref = ['My', frame_name{k}, obj.Appendix{i}];
                    end
                end
            end
            if isfield(xml_struct.CustomDefinitions,'MyTimeConstantLHD_LHD')
                xml_struct.CustomDefinitions.MyTimeConstantLHD_LHD = rmfield(xml_struct.CustomDefinitions.MyTimeConstantLHD_LHD,'Attributes');
            end
            %SOC state
            xml_struct.CustomDefinitions.(['My', obj.Name]).Soc.InitialCapacity.Text = num2str(capacity);
            xml_struct.CustomDefinitions.(['My', obj.Name]).Soc.InitialSoc.Text = '100';
            %Generate SOC string
            sSoc = mat2str(newSOC);
            if length(newSOC) ~= 1
                sSoc = strrep(sSoc(2:end-1), ' ' , ', ');
            end
            sSoc = [newline sSoc newline];
            xml_struct.CustomDefinitions.(['My', obj.Name]).Soc.MeasurementPoints.Text = sSoc;
            xml_struct.CustomDefinitions.(['My', obj.Name]).Soc.Attributes.cache = 'True';
            xml_struct.CustomDefinitions.(['My', obj.Name]).Soc.Attributes.class = 'Soc';
            %Attributes of cell
            xml_struct.CustomDefinitions.(['My', obj.Name]).Attributes.class = 'CellElement';
            xml_struct.CustomDefinitions.(['My', obj.Name]).Attributes.observable = 'True';
        end
        
    end
    methods (Access = protected)
        
        function obj = combineParameters(obj,new_name,varargin)
            index = [];
            if nargin < 4
                error('At least 2 parameters are needed');
            else
                obj.combinedParam{end+1,1} = new_name;
                for i=1:numel(varargin)
                    for j=1:numel(obj.model{4})
                        if strcmp(obj.model{4}{j},varargin{i})
                           index = [index,j];
                        end
                        obj.combinedParam{end,i+1} = varargin{i};
                    end
                end
                obj.model{4}{index(1)} = new_name;
                obj.model{4}(index(2:end)) = [];
                obj.model{5}(index(2:end)) = [];
                obj.model{6}(index(2:end)) = [];
                obj.model{7}(index(2:end)) = [];
                obj.model{8}(index(2:end)) = [];
                for i=1:numel(varargin)
                    for j=1:3
                        obj.model{j} = strrep(obj.model{j},varargin{i},new_name);
                    end
                end
            end
        end
        
        function obj = makeFormula(obj)
            obj.ParameterNamen = obj.model{4};
            obj.ParameterInit = obj.model{5};
            obj.ParameterMin = obj.model{6};
            obj.ParameterMax = obj.model{7};
            obj.ParameterFix = obj.model{8};
            obj.Formel = [obj.model{1},'+',obj.model{2},'+',obj.model{3}];
        end
    end
end

