function app = fcn_TDF_ImplementierungsTable_CellSelection(app,event)
global Modellliste
global DRT_GUI
if isempty(event) || ~isstruct(event) || ~sum(strcmp(fieldnames(event),'Indices')) || ~isfield(event,'Indices'), return,end
ImplCell = struct2cell(Modellliste.Implementierung);
if app.GUI
    TableCell = get(app.TDF_ImplementierungsTable,'Data');
    ImpNr = 1;
    ColNames = get(app.TDF_ImplementierungsTable,'ColumnName');
    ColNames=reshape(ColNames,1,numel(ColNames));
    ColFormat = get(app.TDF_ImplementierungsTable,'ColumnFormat');
else
    TableCell = app.TDF_ImplementierungsTable.Data;
    ImpNr = 1;
    ColNames = app.TDF_ImplementierungsTable.ColumnName;
    ColNames=reshape(ColNames,1,numel(ColNames));
    ColFormat = app.TDF_ImplementierungsTable.ColumnFormat;
end
if strcmp(TableCell{event.Indices(1),1},' ') || isempty(TableCell{event.Indices(1),1})
    ColAnz = 0;
else
    ImpNr = find(strcmp(fieldnames(Modellliste.Implementierung),TableCell{event.Indices(1),1}));
    ColAnz = numel(ImplCell{ImpNr}.inputs);
    ColFormat(1,5:ColAnz+4) = {DRT_GUI.Fit.aktuell_Modell.P_Name(1,:)};
    if ~isempty(ImplCell{ImpNr}.inputs)
        ColNames(1,5:ColAnz+4) = ImplCell{ImpNr}.inputs;
    end
end
ColFormat(1) = {''};
ColFormat(5+ColAnz:end)={''};
if app.GUI
    set(app.TDF_ImplementierungsTable,'ColumnFormat',ColFormat);
    ColNames(1,5+ColAnz:end) = {''};
    set(app.TDF_ImplementierungsTable,'ColumnName',ColNames');
    set(app.TDF_ImplementierungsTable,'ColumnEditable',[true(1,2) false(1,2) true(1,ColAnz)  false(1,numel(ColNames)-ColAnz-2)]);
    if sum(strcmp(fieldnames(app),'TDF_Implementierung')) && ~isempty(app.TDF_Implementierung) ...
            && sum(strcmp(fieldnames(app.TDF_Implementierung),'Table'))...
            && size(app.TDF_Implementierung.Table,2)< numel(ColNames)
        app.TDF_Implementierung.Table(:,end+(numel(ColNames)-size(app.TDF_Implementierung.Table,2))) = {''};
        set(app.TDF_ImplementierungsTable,'Data',app.TDF_Implementierung.Table);
    end
else
    app.TDF_ImplementierungsTable.ColumnFormat = ColFormat;
    ColNames(1,5+ColAnz:end) = {''};
    app.TDF_ImplementierungsTable.ColumnName = ColNames;
    app.TDF_ImplementierungsTable.ColumnEditable = [true(1,2) false(1,2) true(1,ColAnz)  false(1,numel(ColNames)-ColAnz-2)];
    if sum(strcmp(fieldnames(app),'TDF_Implementierung')) && ~isempty(app.TDF_Implementierung) ...
            && sum(strcmp(fieldnames(app.TDF_Implementierung),'Table'))...
            && size(app.TDF_Implementierung.Table,2)< numel(ColNames)
        app.TDF_Implementierung.Table(:,end+(numel(ColNames)-size(app.TDF_Implementierung.Table,2))) = {''};
        app.TDF_ImplementierungsTable.Data = app.TDF_Implementierung.Table;
    end
end
end