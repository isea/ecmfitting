function app = fcn_TDF_ImplementierungsTable_CellEdit(app,event)
global Modellliste
global DRT_GUI
if app.GUI
    app.TDF_Implementierung.Table = get(app.TDF_ImplementierungsTable,'Data');
else
    app.TDF_Implementierung.Table = app.TDF_ImplementierungsTable.Data;
end
app = fcn_TDF_ImplementierungsTable_CellSelection(app,event);
if ~isempty(event) && isstruct(event) && sum(strcmp(fieldnames(event),'Indices'))
    if  ~isempty(event.Indices) && event.Indices(2)==1
        app.TDF_Implementierung.Table(event.Indices(1),2:end) = {''};
    end
end
app.TDF_Implementierung.Table(find(strcmp(app.TDF_Implementierung.Table(:,1),'')),:) = [];
app.TDF_Implementierung.Table(find(strcmp(app.TDF_Implementierung.Table(:,1),' ')),:) = [];
app.TDF_Implementierung.Table(end+1,:)={''};
app.TDF_Implementierung.Info = cell(size(app.TDF_Implementierung.Table,1),1);
if app.GUI
    set(app.TDF_ImplementierungsTable,'Data',app.TDF_Implementierung.Table)
else
    app.TDF_ImplementierungsTable.Data = app.TDF_Implementierung.Table;
end
%Speichere Ersatzschaltbild-Elemente in Info
for i = 1:size(app.TDF_Implementierung.Table,1)
    if sum(find(strcmp(app.TDF_Implementierung.Table{i,1},fieldnames(Modellliste.Implementierung))))
        app.TDF_Implementierung.Info{i} = Modellliste.Implementierung.(app.TDF_Implementierung.Table{i,1});
    end
end
%Iteriere �ber ESB-Elemente und berechne Voigt-Netzwerk Elemente
cd_counter = 0;
for i = 1:numel(app.TDF_Implementierung.Info)-1
    if isempty(app.TDF_Implementierung.Info{i}),continue;end
    args = cell(1,numel(app.TDF_Implementierung.Info{i}.inputs));
    argliste = '';
    if size(app.TDF_Implementierung.Table,2)<(4+numel(app.TDF_Implementierung.Info{i}.inputs)) || sum(find(strcmp('',app.TDF_Implementierung.Table(i,4+(1:numel(app.TDF_Implementierung.Info{i}.inputs)))))) || ...
            sum(find(strcmp(' ',app.TDF_Implementierung.Table(i,4+(1:numel(app.TDF_Implementierung.Info{i}.inputs))))))
        continue
    end
    %Speichere alle Parameter in Liste
    for k = 1:numel(app.TDF_Implementierung.Info{i}.inputs)
        ParNr = find(strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),app.TDF_Implementierung.Table{i,k+4}));
        if isempty(ParNr)
            warning('Konnte Parameter aus Implementierung nicht finden. Implementierungsinfos werden reseted!')
            if app.GUI
                set(app.TDF_ImplementierungsTable,'Data',app.TDF_Implementierung.Table)
            else
                app.TDF_ImplementierungsTable.Data = app.TDF_Implementierung.Table;
            end
            return
        end
        args{k} = DRT_GUI.Fit.Parameter(ParNr);
        argliste = [argliste 'args{' num2str(k) '},' ];
    end
    argliste = argliste(1:end-1);
    %Rufe Funktion zur Berechnung der Zeitbereichsparameter auf
    if strcmp(app.TDF_Implementierung.Table{i,1},'OCV_source')
        DeltaU = DRT_GUI.Messdaten.relax.spannung(end)-DRT_GUI.Messdaten.relax.spannung(1);
        Ladung = [0, cumsum(DRT_GUI.Messdaten.relax.strom(1:end-1) .* diff(DRT_GUI.Messdaten.relax.zeit))]; %selbst
        C_OCV = Ladung(end) / DeltaU;
        if ismember('Funktionsname',fieldnames(app.TDF_Implementierung.Info{i}))
            Z = CalculateESBeImpedance(app.TDF_Implementierung.Info{i},DRT_GUI.Fit.omega,C_OCV);
            [R_RC, C_RC, C_ser, R_ser, R_Ladder, C_Ladder] = RunLocalESBeFunction(app.TDF_Implementierung.Info{i}.Funktionsname,C_OCV);
        elseif ismember('f',fieldnames(app.TDF_Implementierung.Info{i}))
            Z = app.TDF_Implementierung.Info{i}.Z(C_OCV,DRT_GUI.Fit.omega);
            [R_RC, C_RC, C_ser, R_ser, R_Ladder, C_Ladder] = app.TDF_Implementierung.Info{i}.f(C_OCV);
        end
    elseif isempty(argliste)
        if ismember('Funktionsname',fieldnames(app.TDF_Implementierung.Info{i}))
            Z = CalculateESBeImpedance(app.TDF_Implementierung.Info{i},DRT_GUI.Fit.omega);
            [R_RC, C_RC, C_ser, R_ser, R_Ladder, C_Ladder] = RunLocalESBeFunction(app.TDF_Implementierung.Info{i}.Funktionsname);
        elseif ismember('f',fieldnames(app.TDF_Implementierung.Info{i}))
            Z =app.TDF_Implementierung.Info{i}.Z(DRT_GUI.Fit.omega);
            [R_RC, C_RC, C_ser, R_ser, R_Ladder, C_Ladder] = app.TDF_Implementierung.Info{i}.f;
        end
    else
        if ismember('Funktionsname',fieldnames(app.TDF_Implementierung.Info{i}))
            Z = CalculateESBeImpedance(app.TDF_Implementierung.Info{i},DRT_GUI.Fit.omega,args{:});
            [R_RC, C_RC, C_ser, R_ser, R_Ladder, C_Ladder] = RunLocalESBeFunction(app.TDF_Implementierung.Info{i}.Funktionsname,args{:});
        elseif ismember('f',fieldnames(app.TDF_Implementierung.Info{i}))
            eval(['Z=app.TDF_Implementierung.Info{i}.Z(' argliste ',DRT_GUI.Fit.omega);'])
            eval(['[R_RC, C_RC, C_ser, R_ser, R_Ladder, C_Ladder] = app.TDF_Implementierung.Info{i}.f(' argliste ');'])
        end
    end
    cd_counter = cd_counter + ~isempty(regexp(app.TDF_Implementierung.Table{i,2},'CD Re-Fit'));
    %2 * Clim1^2 * Sigma.^2 / (n*pi).^2
    %     tau =
    tau = C_RC(find(R_RC==max(R_RC),1))*max(R_RC);
    app.TDF_Implementierung.Table{i,3}=sum(R_RC)+sum(R_ser);
    app.TDF_Implementierung.Table{i,4} = tau;
end
if app.GUI
    set(app.TDF_ImplementierungsTable,'Data',app.TDF_Implementierung.Table)
else
    app.TDF_ImplementierungsTable.Data = app.TDF_Implementierung.Table;
end
app = fcn_TDF_UpdateTable(app, event, cd_counter);
end