function U = fcn_TDF_OCV_Spannung(app, ROCV, COCV, R_Ladder_OCV, C_Ladder_OCV, t, OCV, strom, stromindex)
% Achtung, COCV ist kein reales Element im Netzwerk. Es dient nur zur
% Berechnung der Spannung auf der OCV-Spannungsquelle.
  %chosen data set
U = zeros(size(t));

fastind = find((OCV.R_RC.*OCV.C_RC)<0.3);
OCV.R_ser = OCV.R_ser + sum(OCV.R_RC(fastind));
OCV.R_RC(fastind)=[];
OCV.C_RC(fastind)=[];

U_0 = zeros(1+numel(OCV.R_RC)+numel(OCV.C_ser)+numel(C_Ladder_OCV),1);
options = odeset('RelTol',1e-4,'AbsTol',1e-5*ones(size(U_0)),'MaxStep',1);
for i = 1:numel(stromindex)-1
    [odetime , odespannung] = ode45(@fcn_dudt,[t(stromindex(i):stromindex(i+1))],...
        [U_0],options,strom(i),OCV,ROCV,COCV,R_Ladder_OCV,C_Ladder_OCV);
    if stromindex(i+1)-stromindex(i)==1 ,
        odespannung = odespannung([1 end],:);
        odetime = odetime([1 end],:);
    end
    U_0 = odespannung(end,:);
    U_COCV = odespannung(:,1);
    U_RC = odespannung(:,2:1+numel(OCV.R_RC));
    U_C = odespannung(:,(2+numel(OCV.R_RC)):(1+numel(OCV.R_RC)+numel(OCV.C_ser)));
    U_Ladder = odespannung(:,(2+numel(OCV.R_RC)+numel(OCV.C_ser)):(1+numel(OCV.R_RC)+numel(OCV.C_ser)+numel(R_Ladder_OCV)));
    if isempty(U_C), U_C=0;end
    if isempty(U_RC), U_RC=0;end
    if isempty(U_Ladder), U_Ladder=0;end
    % Masche �ber Parallelgeschaltete OCV-Quelle und Stromteiler
    % des Eingangsstroms
    
    
    if isempty(R_Ladder_OCV)
        I_COCV = (sum(U_RC,2)+U_C-U_COCV)./(ROCV+OCV.R_ser)+...
            strom(i).*(OCV.R_ser./(ROCV+OCV.R_ser));
        I_Voigt = strom(i)-I_COCV;
        
    else
        U_CL = odespannung(:,2+numel(OCV.R_RC)+numel(OCV.C_ser):1+numel(OCV.R_RC)+numel(OCV.C_ser)+numel(R_Ladder_OCV));
        GL1 = 1/R_Ladder_OCV(1);
        GRser = 1/OCV.R_ser;
        GOCV = 1/ROCV;
        
        if OCV.R_ser == 0
            % Stromteiler f�r Stromquelle I
            I_Voigt = strom(i); %
            % Strom aus OCV-Spannungsquelle
            I_aus_OCV = U_COCV / (ROCV + 1/(GRser+GL1));
            I_Voigt = I_Voigt + I_aus_OCV ;
            
        else
            % Stromteiler f�r Stromquelle I
            I_Voigt = strom(i)*(GRser/(GRser+GL1+GOCV)); %
            % Strom aus OCV-Spannungsquelle
            I_aus_OCV = U_COCV / (ROCV + 1/(GRser+GL1));
            I_Voigt = I_Voigt + I_aus_OCV * GRser/(GRser+GL1) ;
        end
        % Strom aus Voigt-Netzwerk + Cser
        I_Voigt = I_Voigt - (sum(U_RC,2)+sum(U_C,2)) / (OCV.R_ser + 1/(GOCV+GL1));
        % Strom aus Ladder-Netzwerk
        I_aus_UCL1_durch_RL1 = U_CL(:,1) / (R_Ladder_OCV(1) + 1/(GRser+GOCV));
        if OCV.R_ser >0
            I_Voigt = I_Voigt + I_aus_UCL1_durch_RL1 * GRser/(GOCV+GRser);
        else
            I_Voigt = I_Voigt + I_aus_UCL1_durch_RL1;
        end
    end
    U(stromindex(i):stromindex(i+1)-1)=U(stromindex(i):stromindex(i+1)-1)+...
        sum(odespannung(1:end-1,2:1+numel(OCV.R_RC)+numel(OCV.C_ser)),2)+I_Voigt(1:end-1).*OCV.R_ser;
end
U(end)=sum(odespannung(end,2:1+numel(OCV.R_RC)+numel(OCV.C_ser)),2)+I_Voigt(end).*OCV.R_ser;
end