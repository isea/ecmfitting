function app = fcn_TDF_SpeichernButton(app,event)
global DRT_GUI
if sum(strcmp(fieldnames(app),'TDF_Implementierung')) && ~isempty(app.TDF_Implementierung)
    DRT_GUI.Fit.Implementierung = app.TDF_Implementierung;
end
% Update handles structure
app= fcn_SpeichernButton(app,event);
%update table in bottom right
if app.GUI
    set(app.RelaxTable,'Data',app.TDF_Implementierung.OCV)
else
    app.RelaxTable.Data = app.TDF_Implementierung.OCV;
end
end