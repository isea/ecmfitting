function fcn_TDF_ResetOCVFitButton(app,event)
if ~isempty(event) && ismember('Indices',fieldnames(event))
    event.Indices = [1 2 ];
end
for n = 1:length(app.TDF_Implementierung.Table(:,2))
     app.TDF_Implementierung.Table{n,2} = '';
end
set(app.TDF_ImplementierungsTable,'Data',app.TDF_Implementierung.Table);
fcn_TDF_ImplementierungsTable_CellEdit(app,event);
fcn_TDF_PlotOCVButton(app,event);
fcn_TDF_SpeichernButton(app,event);
end