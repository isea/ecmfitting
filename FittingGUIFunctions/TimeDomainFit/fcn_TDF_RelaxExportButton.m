function fcn_TDF_RelaxExportButton(app,event)
global DRT_GUI
if app.CRateFlag && app.HysteresisFlag
    StateString = [DRT_GUI.Testparameter.Zustand_2 '_' DRT_GUI.Testparameter.Zustand];
elseif app.CRateFlag
    StateString = DRT_GUI.Testparameter.Zustand;
else
    StateString = DRT_GUI.Testparameter.Zustand_2;
end
AltePosition = get(app.axes7,'Position');
AltePosition(1:2)=5;
AltePosition2=[AltePosition(1:2),AltePosition(3:4)+10];
newfig=figure('Units','pixels','Position',AltePosition2); %'Position',AltePosition+10,
plot(1,1);
neueAchse = gca;
cNames=fieldnames(app.axes7);
cValues=get(app.axes7,cNames);
% neueAchse=axes('Parent',newfig);
% for ix1=1:length(cNames)
%     try 
%         set(neueAchse,cNames{ix1},cValues{ix1})
%     catch
%     end
% end
%set(neueAchse,'OuterPosition',AltePosition)
plot(-rand(length(app.axes7.Children))*1e-3,'Parent',neueAchse);
for ix2=1:length(app.axes7.Children)
    cNames2=fieldnames(app.axes7.Children(ix2));
    cValues2=get(app.axes7.Children(ix2),cNames2);
    for ix3=1:length(cNames2)
        if strcmp(cNames2{ix3},'Parent'), continue; end
        try
            set(neueAchse.Children(ix2),cNames2{ix3},cValues2{ix3})
        catch
        end
    end
end

h1=legend();
cNames3=fieldnames(app.axes7.Legend);
cValues3=get(app.axes7.Legend,cNames3);
for ix4=1:length(cNames3)
    try 
        set(h1,cNames3{ix4},cValues3{ix4})
    catch
    end
end
set(neueAchse.Legend,'Location','SouthEast')
app.axes7.XLabel.String='Zeit in s';app.axes7.XLabel.Interpreter='latex';
app.axes7.YLabel.String='Spannung in V';app.axes7.YLabel.Interpreter='latex';

if isempty(dir('export'))
    mkdir('export')
end
if isempty( dir(['export' '/' DRT_GUI.Testparameter.Batterie]))
    mkdir(['export' '/' DRT_GUI.Testparameter.Batterie])
end
savefig(['export/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie '_TDMFit_' ...
    StateString '_' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad_'...
    strrep(num2str(DRT_GUI.Testparameter.SOC),'-','m') 'SOC'...
    '.fig'])
end