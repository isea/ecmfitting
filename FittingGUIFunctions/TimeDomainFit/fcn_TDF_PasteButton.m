function fcn_TDF_PasteButton(app,event)
global Implementierung_Copy
if strcmp(app.copy_model,app.ModellAuswahlPopup.Value)
    if ~isempty(Implementierung_Copy)
        app.TDF_Implementierung.Config = Implementierung_Copy.Config;
        app.TDF_Implementierung.Table = Implementierung_Copy.Table;
        set(app.TDF_ImplementierungsTable,'Data',app.TDF_Implementierung.Table)
        if ~isempty(event) && ismember('Indices',fieldnames(event))
            event.Indices = [1 2 ];
        end
        fcn_TDF_ImplementierungsTable_CellEditCallback(app,event)
        fcn_TDF_PlotOCVButton_Callback(app,event)
        if ~isempty(event) && ismember('info',fieldnames(event)) 
            if ischar(event.info) && strcmp(event.info,'save')
            fcn_SpeichernButton_Callback(app,event)
            end
        end
        app.TDF_Implementierung.OCV(5:end,:) = Implementierung_Copy.OCV(5:end,:);
        set(app.TDF_RelaxTable,'Data',app.TDF_Implementierung.OCV)
        set(app.RelaxTable,'Data',app.TDF_Implementierung.OCV)
    end
else
    errordlg('Used model is not the same as in the copied case!')
    return
end
fcn_TDF_PlotOCVButton(app,event);
end