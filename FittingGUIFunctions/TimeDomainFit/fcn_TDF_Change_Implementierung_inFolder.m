function fcn_TDF_Change_Implementierung_inFolder(app, Folder, ZellName, ModellName, Implementierung)
if Folder(end) == '/' || Folder(end) == '\'
    Folder = Folder(1:end-1);
end
FileList = dir([Folder '/*' ZellName '*.mat']);
for i = 1:numel(FileList)
    if ~strcmp(FileList(i).name(end-10:end),'_Modell.mat') && ~strcmp(FileList(i).name(end-15:end),'_modelexport.mat')
        display(['Modifiziere ' FileList(i).name]);
        
        load([Folder '/' FileList(i).name])
        if ~(exist('DRT_GUI','var')==1)
            warning('Die Datei %s konnte nicht geladen werden',[Folder '/' FileList(i).name])
            continue
        end
        if strcmp(DRT_GUI.Fit.aktuell_Modell.Modellname,ModellName)
            DRT_GUI.Fit.Implementierung.Table = Implementierung.Table;
            DRT_GUI.Fit.Implementierung.Info = Implementierung.Info;
            
            save([Folder '/' FileList(i).name],'DRT_GUI')
        end
        clear DRT_GUI
    end
end
FolderList = dir([Folder '/*']);
for i = 1:numel(FolderList)
    if FolderList(i).isdir && ~strcmp(FolderList(i).name,'.') && ~strcmp(FolderList(i).name,'..')
        fcn_TDF_Change_Implementierung_inFolder([Folder '/' FolderList(i).name],ZellName,ModellName,Implementierung)
    end
end
end