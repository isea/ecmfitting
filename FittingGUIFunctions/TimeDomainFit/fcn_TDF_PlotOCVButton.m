function fcn_TDF_PlotOCVButton(app,event)
global DRT_GUI
if ~sum(strcmp(fieldnames(app),'TDF_Implementierung')) || isempty(app.TDF_Implementierung) ||...
        ~sum(strcmp(fieldnames(app.TDF_Implementierung),'OCV')) || isempty(app.TDF_Implementierung.OCV)
    return
end

%Determine which field contains the pulse data
pulse_data_fieldname.pulse = isfield(DRT_GUI.Messdaten,'pulse');
pulse_data_fieldname.vor_relax = isfield(DRT_GUI.Messdaten,'vor_relax');
pulse_data_fieldname.nach_relax = isfield(DRT_GUI.Messdaten,'nach_relax');
pulse_data_existent_flag = 0;
if pulse_data_fieldname.pulse
    used_pulse_data_name = 'pulse';
    pulse_data_existent_flag = 1;
elseif pulse_data_fieldname.nach_relax
    used_pulse_data_name = 'nach_relax';
    pulse_data_existent_flag = 1;
else
    used_pulse_data_name = 'vor_relax';
    pulse_data_existent_flag = 1;
end
%determine which data can be used
used_tdf_data = 'Relax';
if app.BothButton.Value
    if pulse_data_existent_flag && isfield(DRT_GUI.Messdaten,'relax')
        if ~isempty(DRT_GUI.Messdaten.(used_pulse_data_name).spannung) && ~isempty(DRT_GUI.Messdaten.(used_pulse_data_name).strom) && ~isempty(DRT_GUI.Messdaten.(used_pulse_data_name).zeit)
            used_tdf_data = 'Both';
        end
    end   
elseif app.PulsesButton.Value && strcmp(used_tdf_data,'Relax')
    if pulse_data_existent_flag
        if ~isempty(DRT_GUI.Messdaten.(used_pulse_data_name).spannung) && ~isempty(DRT_GUI.Messdaten.(used_pulse_data_name).strom) && ~isempty(DRT_GUI.Messdaten.(used_pulse_data_name).zeit)
            used_tdf_data = 'Pulses';
        end
    end
end

%initialize data arrays depending on the chosen data set
tdf_data.spannung = [];
tdf_data.strom = [];
tdf_data.zeit = [];
if strcmp(used_tdf_data,'Relax')
    if isempty(DRT_GUI.Messdaten.relax.zeit) || isempty(DRT_GUI.Messdaten.relax.strom) || isempty(DRT_GUI.Messdaten.relax.spannung)
       warning('No Relax Data found')
       return
    else
        tdf_data.spannung = DRT_GUI.Messdaten.relax.spannung;
        tdf_data.strom = DRT_GUI.Messdaten.relax.strom;
        tdf_data.zeit = DRT_GUI.Messdaten.relax.zeit;
        %if there current at the beginning of the profile, add a initial
        %data point
        if abs(tdf_data.strom(1)) > 0.001
            tdf_data.zeit = [0, tdf_data.zeit + 0.001];
            tdf_data.strom = [0, tdf_data.strom];
            tdf_data.spannung = [tdf_data.spannung(1) , tdf_data.spannung];
        end
    end   
elseif strcmp(used_tdf_data,'Pulses')
    tdf_data.spannung = DRT_GUI.Messdaten.(used_pulse_data_name).spannung;
    tdf_data.strom = DRT_GUI.Messdaten.(used_pulse_data_name).strom;
    tdf_data.zeit = DRT_GUI.Messdaten.(used_pulse_data_name).zeit;
else
    tdf_data.spannung = [DRT_GUI.Messdaten.relax.spannung-(DRT_GUI.Messdaten.relax.spannung(end)-DRT_GUI.Messdaten.(used_pulse_data_name).spannung(1)) DRT_GUI.Messdaten.(used_pulse_data_name).spannung];
    tdf_data.strom = [DRT_GUI.Messdaten.relax.strom DRT_GUI.Messdaten.(used_pulse_data_name).strom];
    tdf_data.zeit = [DRT_GUI.Messdaten.relax.zeit (DRT_GUI.Messdaten.(used_pulse_data_name).zeit + DRT_GUI.Messdaten.relax.zeit(end))];
    end_of_relax_idx = length(DRT_GUI.Messdaten.relax.zeit);
    %if there current at the beginning of the profile, add a initial
        %data point
    if abs(tdf_data.strom(1)) > 0.001
        tdf_data.zeit = [0, tdf_data.zeit + 0.001];
        tdf_data.strom = [0, tdf_data.strom];
        tdf_data.spannung = [tdf_data.spannung(1) , tdf_data.spannung];
        end_of_relax_idx = end_of_relax_idx + 1;
    end
end

[tdf_data.zeit,unique_td_data_index]= unique(tdf_data.zeit);
tdf_data.strom = tdf_data.strom(unique_td_data_index);
tdf_data.spannung = tdf_data.spannung(unique_td_data_index);

%upsampling for homogenous weighting of voltage errors
tempzeit = 0:0.1:tdf_data.zeit(end);
tdf_data.spannung = interp1(tdf_data.zeit,tdf_data.spannung,tempzeit);
tdf_data.strom = interp1(tdf_data.zeit,tdf_data.strom,tempzeit);
tdf_data.zeit = tempzeit;

%detect steps within current profile for the final profile
stromindex = find(abs([tdf_data.strom 0])<0.001);
diff_stromindex = diff(stromindex);
strom_sprungindex = find(diff_stromindex > 1);
counter = 1;
counter_ampere_idx = 1;
strom_sprungindex_anfang = [];
strom_sprungindex_ende = [];
strom_sprungindex_ampere_idx = [];
strom_sprungindex_ampere = [];
filtered_current = medfilt1(tdf_data.strom);
for n = 1:length(strom_sprungindex)
    if stromindex(strom_sprungindex(n) + 1) <= length(tdf_data.strom)
        end_of_pulse = stromindex(strom_sprungindex(n) + 1);
    else
        end_of_pulse = length(tdf_data.strom);
    end
    if (abs(tdf_data.zeit(end_of_pulse)-tdf_data.zeit(stromindex(strom_sprungindex(n)))) > 1) && (max(abs(tdf_data.strom(stromindex(strom_sprungindex(n)):end_of_pulse))) > 1/20 * DRT_GUI.Testparameter.Cap)
        strom_sprungindex_anfang(counter) = stromindex(strom_sprungindex(n));
        strom_sprungindex_ende(counter) = end_of_pulse;
        %check whetther there are acutally 2 pulses in inverse
        %direction and divide it into two pulses
        if ~isempty(find(filtered_current(strom_sprungindex_anfang(counter):strom_sprungindex_ende(counter)) > 1)) && ~isempty(find(filtered_current(strom_sprungindex_anfang(counter):strom_sprungindex_ende(counter)) < -1))
            if find(filtered_current(strom_sprungindex_anfang(counter):strom_sprungindex_ende(counter)) > 0,1,'last') > find(filtered_current(strom_sprungindex_anfang(counter):strom_sprungindex_ende(counter)) < 0,1,'last')
                strom_sprungindex_ende(counter) =  strom_sprungindex_anfang(counter) + find(filtered_current(strom_sprungindex_anfang(counter):strom_sprungindex_ende(counter)) < 0,1,'last');
            else
                strom_sprungindex_ende(counter) =  strom_sprungindex_anfang(counter) + find(filtered_current(strom_sprungindex_anfang(counter):strom_sprungindex_ende(counter)) > 0,1,'last');
            end
            strom_sprungindex_ende(counter + 1) = end_of_pulse;
            strom_sprungindex_anfang(counter + 1) = strom_sprungindex_ende(counter) + 1;
            strom_sprungindex_ampere(counter_ampere_idx) = tdf_data.strom(round(strom_sprungindex_anfang(counter)+(strom_sprungindex_ende(counter)-strom_sprungindex_anfang(counter))/2));
            strom_sprungindex_ampere(counter_ampere_idx + 1) = tdf_data.strom(round(strom_sprungindex_anfang(counter + 1)+(strom_sprungindex_ende(counter + 1)-strom_sprungindex_anfang(counter + 1))/2));
            strom_sprungindex_ampere_idx(counter_ampere_idx) = strom_sprungindex_ampere_idx(counter_ampere_idx - 1) + 1;
            strom_sprungindex_ampere_idx(counter_ampere_idx + 1) = strom_sprungindex_ampere_idx(counter_ampere_idx - 1) + 2;
            counter_ampere_idx = counter_ampere_idx + 2;
            counter = counter + 2;
        else
            strom_sprungindex_ampere(counter_ampere_idx) = tdf_data.strom(round(stromindex(strom_sprungindex(n))+(end_of_pulse-stromindex(strom_sprungindex(n)))/2));
            if ~isempty(strom_sprungindex_ampere_idx)
                strom_sprungindex_ampere_idx(counter_ampere_idx) = strom_sprungindex_ampere_idx(counter_ampere_idx - 1) + 1;
            else
                strom_sprungindex_ampere_idx(counter_ampere_idx) = n;
            end
            counter_ampere_idx = counter_ampere_idx + 1;
            counter = counter + 1;
        end
    else
        tdf_data.strom(stromindex(strom_sprungindex(n)):end_of_pulse) = 0;
        if stromindex(strom_sprungindex(n))
            tdf_data.spannung(stromindex(strom_sprungindex(n)):end_of_pulse) = ones(size(tdf_data.spannung(stromindex(strom_sprungindex(n)):end_of_pulse))) * tdf_data.spannung(1);
        else
            tdf_data.spannung(stromindex(strom_sprungindex(n)):end_of_pulse) = tdf_data.spannung(stromindex(strom_sprungindex(n))-1) + ((tdf_data.spannung(stromindex(strom_sprungindex(n) + 2))-tdf_data.spannung(stromindex(strom_sprungindex(n))-1))/(tdf_data.zeit(stromindex(strom_sprungindex(n) + 2))-tdf_data.zeit(stromindex(strom_sprungindex(n))-1))) .*(tdf_data.zeit(stromindex(strom_sprungindex(n)):end_of_pulse)-tdf_data.zeit(stromindex(strom_sprungindex(n))-1)) ;
        end
    end    
end
strom_sprungindex_ampere_idx = 1:length(strom_sprungindex_ampere_idx);

IndexWithoutEvaluation = [];
stromindex = 1;
stromindex_counter = 2;
for n = 1:length(strom_sprungindex_ende)
    if abs(tdf_data.zeit(strom_sprungindex_ende(n))-tdf_data.zeit(strom_sprungindex_anfang(n))) > 1
        IndexWithoutEvaluation = [IndexWithoutEvaluation (strom_sprungindex_anfang(n)+1):(strom_sprungindex_ende(n)-1)];
        stromindex(stromindex_counter) = strom_sprungindex_anfang(n);
        stromindex_counter = stromindex_counter + 1;
        stromindex(stromindex_counter) = strom_sprungindex_ende(n);
        stromindex_counter = stromindex_counter + 1;
    end
end
stromindex = sort(stromindex);
if stromindex(end) < numel(tdf_data.strom)
    stromindex(end+1) = numel(tdf_data.strom);
end

%if any parameters are fitted as C rate dependent -> Identify intervals in
%which the C rate is correct and denote the corresponding C rate and
%parameter idx
cd_time_interval = []; %segmentation of the profile contianing the parts for distinct C rates
cd_crate = []; % arry containing the corresponding C rates to cd_time_interval
cd_parameter_idx = []; %inidices mapping the intervals to a parameter idx;
cd_parameter_idx_inverted = [];
stromindex_to_CD_idx = [];
%save intervals 
for n = 1:length(strom_sprungindex_ampere)
    if n == 1
        cd_time_interval{n} = [1:stromindex(4)-1];         
        stromindex_to_CD_idx = ones(1,3);
    elseif n == length(strom_sprungindex_ampere)
        if ~mod(length(stromindex),2)
            cd_time_interval{n} = [stromindex(2*n):stromindex(2*n+2)];   
            stromindex_to_CD_idx = [stromindex_to_CD_idx length(cd_time_interval)*ones(1,3)];
        else
            cd_time_interval{n} = [stromindex(2*n):stromindex(2*n+1)];   
            stromindex_to_CD_idx = [stromindex_to_CD_idx length(cd_time_interval)*ones(1,2)];
        end
    else
        cd_time_interval{n} = [stromindex(2*n):stromindex(2*n+2)-1];     
        stromindex_to_CD_idx = [stromindex_to_CD_idx length(cd_time_interval)*ones(1,2)];
    end          
    cd_crate{n} = strom_sprungindex_ampere(n)/DRT_GUI.Testparameter.Cap;        
end   

%identify pulses of approx. equal C rate
rounded_crate = round(strom_sprungindex_ampere/DRT_GUI.Testparameter.Cap,1); 
crate_counter = 1;
for n = 1:length(cd_crate)
    if isempty(find(rounded_crate(n) == cd_parameter_idx_inverted))
        cd_parameter_idx(n) = crate_counter;
        cd_parameter_idx_inverted(crate_counter) = rounded_crate(n);
        crate_counter = crate_counter + 1;
    else
        cd_parameter_idx(n) = find(rounded_crate(n) == cd_parameter_idx_inverted);
    end
end
crate_counter = crate_counter - 1;

strom = zeros(1,numel(stromindex)-1);
timediff = [ diff(tdf_data.zeit), 0];
for i = 1:numel(stromindex)-1
    if numel(stromindex(i):stromindex(i+1)-1) == 1
        strom(i) = tdf_data.strom(stromindex(i));
    else
        strom(i) = sum(tdf_data.strom(stromindex(i):stromindex(i+1)-1).*timediff(stromindex(i):stromindex(i+1)-1)./sum(timediff(stromindex(i):stromindex(i+1)-1)));
    end
end

%plot measurement data
FarbenLaden
hold(app.axes7,'off')
plot(app.axes7,tdf_data.zeit,tdf_data.spannung ,'color',RWTHBlau,'DisplayName','Measurement','LineWidth',2)
grid(app.axes7,'on')
hold(app.axes7,'on')

Voigt.R_RC = [];
Voigt.C_RC = [];
Voigt.R_ser = 0;
Voigt.C_ser = [];
Voigt.CD = [];

% Iteration �ber alle ESB-Elemente und aufstellen des Voigt-Netzwerks)
% Re-Fit aus TDF_RelaxTable Rest aus Frequenzbereich
RC_counter = 1;
cd_counter = 0;
for i = 1:numel(app.TDF_Implementierung.Info)-1
    args = cell(1,numel(app.TDF_Implementierung.Info{i}.inputs));
    argliste = '';
    for k = 1:numel(app.TDF_Implementierung.Info{i}.inputs)
        ParNr = find(strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),app.TDF_Implementierung.Table{i,k+4}));
        if isempty(ParNr)
            warning('Konnte Parameter aus Implementierung nicht finden. Implementierungsinfos werden reseted!')
            DRT_GUI.Fit=rmfield(DRT_GUI.Fit,'Implementierung');
            return
        end
        args{k} = DRT_GUI.Fit.Parameter(ParNr);
        if strcmp(app.TDF_Implementierung.Table{i,2},'CD Re-Fit')
            args{k} = cell(1,crate_counter);
            for n = 1:crate_counter
                args{k}{n} = app.TDF_Implementierung.OCV{find(strcmp(app.TDF_Implementierung.OCV(:,1),['ReFit_' app.TDF_Implementierung.Table{i,k+4} '_' strrep(num2str(round(cd_crate{n},1)),'-','m') 'C']),1),2};
            end
        elseif strcmp(app.TDF_Implementierung.Table{i,2},'Re-Fit')
            args{k} = app.TDF_Implementierung.OCV{find(strcmp(app.TDF_Implementierung.OCV(:,1),['ReFit_' app.TDF_Implementierung.Table{i,k+4}]),1),2};            
        end
        argliste = [argliste 'args{' num2str(k) '},' ];
    end
    argliste = argliste(1:end-1);
    for n = 1:crate_counter
        temp_args = cell(1,numel(app.TDF_Implementierung.Info{i}.inputs));
        if strcmp(app.TDF_Implementierung.Table{i,2},'CD Re-Fit')
            for k = 1:numel(app.TDF_Implementierung.Info{i}.inputs)
                temp_args{k} = args{k}{n};
            end
        else
            temp_args = args;
        end
        if strcmp(app.TDF_Implementierung.Table{i,1},'OCV_source')
            DeltaU = tdf_data.spannung(end)-tdf_data.spannung(1);
            Ladung = [0, cumsum(tdf_data.strom(1:end-1) .* diff(tdf_data.zeit))];
            C_OCV = Ladung(end) / DeltaU;

            if ismember('Funktionsname',fieldnames(app.TDF_Implementierung.Info{i}))
                Z = CalculateESBeImpedance(app.TDF_Implementierung.Info{i},DRT_GUI.Fit.omega,C_OCV);
                [R_RC, C_RC, C_ser, R_ser, R_Ladder, C_Ladder] = RunLocalESBeFunction(app.TDF_Implementierung.Info{i}.Funktionsname,C_OCV);
            elseif ismember('f',fieldnames(app.TDF_Implementierung.Info{i}))
                Z = app.TDF_Implementierung.Info{i}.Z(C_OCV,DRT_GUI.Fit.omega);
                [R_RC, C_RC, C_ser, R_ser, R_Ladder, C_Ladder] = app.TDF_Implementierung.Info{i}.f(C_OCV);
            end
        elseif isempty(argliste)
            if ismember('Funktionsname',fieldnames(app.TDF_Implementierung.Info{i}))
                Z = CalculateESBeImpedance(app.TDF_Implementierung.Info{i},DRT_GUI.Fit.omega);
                [R_RC, C_RC, C_ser, R_ser, R_Ladder, C_Ladder] = RunLocalESBeFunction(app.TDF_Implementierung.Info{i}.Funktionsname);
            elseif ismember('f',fieldnames(app.TDF_Implementierung.Info{i}))
                Z =app.TDF_Implementierung.Info{i}.Z(DRT_GUI.Fit.omega);
                [R_RC, C_RC, C_ser, R_ser, R_Ladder, C_Ladder] = app.TDF_Implementierung.Info{i}.f;
            end
        else
            if ismember('Funktionsname',fieldnames(app.TDF_Implementierung.Info{i}))
                Z = CalculateESBeImpedance(app.TDF_Implementierung.Info{i},DRT_GUI.Fit.omega,temp_args{:});
                [R_RC, C_RC, C_ser, R_ser, R_Ladder, C_Ladder] = RunLocalESBeFunction(app.TDF_Implementierung.Info{i}.Funktionsname,temp_args{:});
            elseif ismember('f',fieldnames(app.TDF_Implementierung.Info{i}))
                eval(['Z=app.TDF_Implementierung.Info{i}.Z(' argliste ',DRT_GUI.Fit.omega);'])
                eval(['[R_RC, C_RC, C_ser, R_ser, R_Ladder, C_Ladder] = app.TDF_Implementierung.Info{i}.f(' argliste ');'])
            end
        end            
        Voigt.R_RC = [Voigt.R_RC ;R_RC'];
        Voigt.C_RC = [Voigt.C_RC ;C_RC'];
        if ~isempty(R_ser) , Voigt.R_ser = Voigt.R_ser + R_ser; end
        if ~isempty(C_ser) && abs(C_ser) < 1e16
            if isempty(Voigt.C_ser) || abs(Voigt.C_ser) > 1e16
                Voigt.C_ser = C_ser;
            else
                Voigt.C_ser = (Voigt.C_ser .* C_ser)./(Voigt.C_ser + C_ser);
            end
        end           
        if ~strcmp(app.TDF_Implementierung.Table{i,2},'CD Re-Fit')
            Voigt.CD = [Voigt.CD; 0*ones(length(R_RC),1)];  
            break
        else
            Voigt.CD = [Voigt.CD; n*ones(length(R_RC),1)];  
        end
    end
end
% Voigt Netzwerk created

% calculate dynamic response of the Voigt circuit
zeit = tdf_data.zeit(stromindex);
ModellSpannung = tdf_data.strom .* Voigt.R_ser;
if ~(isempty(Voigt.C_ser) || abs(Voigt.C_ser) > 1e16)
    ModellSpannung =ModellSpannung +...
        [0, cumsum(diff(tdf_data.zeit).*tdf_data.strom(1:end-1)./Voigt.C_ser)]; %selbst
end
%display(sprintf('Cserspannung: %f,   Cser: %f      As: %f', sum(diff(DRT_GUI.Messdaten.relax.zeit).*DRT_GUI.Messdaten.relax.strom(1:end-1)./Voigt.C_ser), Voigt.C_ser,sum(diff(DRT_GUI.Messdaten.relax.zeit).*DRT_GUI.Messdaten.relax.strom(1:end-1))));
U_RC = zeros(numel(tdf_data.strom),numel(Voigt.R_RC));
for i = 1:numel(stromindex)-1
    if stromindex(i)==1
        U0 = zeros(size(Voigt.R_RC));
    else
        U0 = U_RC(stromindex(i),:);
    end
    Umax = strom(i).*Voigt.R_RC;
    t = tdf_data.zeit(stromindex(i)+1:stromindex(i+1))-tdf_data.zeit(stromindex(i));
    for k = 1:numel(Voigt.R_RC)        
        if ~Voigt.CD(k)
            if abs(U0(k)-abs(Umax(k)))<=0.0001
                U_RC(stromindex(i)+1:stromindex(i+1),k) = Umax(k);
            else
                U_RC(stromindex(i)+1:stromindex(i+1),k) = U0(k)+(Umax(k)-U0(k)).*(1-exp(-t./(Voigt.R_RC(k).*Voigt.C_RC(k))));
            end
        elseif cd_parameter_idx(stromindex_to_CD_idx(i)) == Voigt.CD(k)
            if abs(U0(k)-Umax(k))<=0.0001
                U_RC(stromindex(i)+1:stromindex(i+1),k) = Umax(k);
            else
                U_RC(stromindex(i)+1:stromindex(i+1),k) = U0(k)+(Umax(k)-U0(k)).*(1-exp(-t./(Voigt.R_RC(k).*Voigt.C_RC(k))));
            end
        end
    end
end
ModellSpannung = ModellSpannung + sum(U_RC,2)';

qOCV_voltage= [];
if app.UseOCVCheckBox.Value
    if app.GUI
        qOCV_List= dir(['output' '/' DRT_GUI.Testparameter.Batterie '/' app.StateString '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad/' '*qOCV-File*']);
    else
        qOCV_List= dir([app.OutputPath '/' DRT_GUI.Testparameter.Batterie '/' app.StateString '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad/' '*qOCV-File*']); 
    end
    if ~isempty(qOCV_List)
        ocv_file = load(qOCV_List.name);
        ocv_file_fieldnames = fieldnames(ocv_file);
        OCV_array = ocv_file.(ocv_file_fieldnames{1});       
        
        %Calculate the start SOC corresponding to the voltage value at the
        %first index        
        [inputSOC,indices] = unique(OCV_array{1, 1}.SOC,'stable');
        qOCV_voltage_A = zeros(1,length(inputSOC));
        voltage = OCV_array{1, 1}.voltage(indices);
        qOCV_voltage_A(1,:) = interp1(inputSOC,voltage,inputSOC,'spline');
        qOCV_voltage_B = zeros(1,length(inputSOC));
        [inputSOCB,indicesB] = unique(OCV_array{2, 1}.SOC  ,'stable');
        voltage = OCV_array{2, 1}.voltage(indicesB);
        qOCV_voltage_B(1,:) = interp1(inputSOCB,voltage,inputSOC,'spline');
        qOCV_voltage = (qOCV_voltage_A+qOCV_voltage_B)./2;
        
        start_idx = find(qOCV_voltage <= tdf_data.spannung(1),1,'last');
        if start_idx == length(qOCV_voltage)
            Start_SOC = inputSOC(start_idx);
        elseif tdf_data.spannung(1) == qOCV_voltage(start_idx)
            Start_SOC = inputSOC(start_idx);
        elseif tdf_data.spannung(1) == qOCV_voltage(start_idx+1)
            Start_SOC = inputSOC(start_idx+1);
        else
            Start_SOC = inputSOC(start_idx) + ((inputSOC(start_idx + 1) - inputSOC(start_idx))/(qOCV_voltage(start_idx + 1) - qOCV_voltage(start_idx))) * (tdf_data.spannung(1) - qOCV_voltage(start_idx));
        end
        Start_SOC = Start_SOC/100;
        %Calculate OCV for all time steps
        if  strcmp(used_tdf_data, 'Relax')
%             relax_ladung = sum(tdf_data.strom(1:end-1) .* diff(tdf_data.zeit(1:end)));      
%             Start_SOC = DRT_GUI.Testparameter.SOC/100 - (relax_ladung/(3600*DRT_GUI.Testparameter.Cap));
            AhCounter = cumsum((tdf_data.strom(1:end-1) .* diff(tdf_data.zeit))/(3600*DRT_GUI.Testparameter.Cap)) + Start_SOC;
            AhCounter = AhCounter * 100;   
        elseif strcmp(used_tdf_data, 'Both')
%             relax_ladung = sum(tdf_data.strom(1:end_of_relax_idx-1) .* diff(tdf_data.zeit(1:end_of_relax_idx)));            
%             Start_SOC = DRT_GUI.Testparameter.SOC/100 - (relax_ladung/(3600*DRT_GUI.Testparameter.Cap));
            AhCounter = cumsum((tdf_data.strom(1:end-1) .* diff(tdf_data.zeit))/(3600*DRT_GUI.Testparameter.Cap)) + Start_SOC;
            AhCounter = AhCounter * 100;
        else
%             Start_SOC = DRT_GUI.Testparameter.SOC/100;
            AhCounter = cumsum((tdf_data.strom(1:end-1) .* diff(tdf_data.zeit))/(3600*DRT_GUI.Testparameter.Cap)) + Start_SOC;
            AhCounter = AhCounter * 100;
        end
        [OCV_row,OCV_col] =size(OCV_array);
        if OCV_row == 2
            qOCV_voltage_A = zeros(1,length(AhCounter));                
            [inputSOC,indices] = unique(OCV_array{1, 1}.SOC,'stable');
            voltage = OCV_array{1, 1}.voltage(indices);
            qOCV_voltage_A(1,:) = interp1(inputSOC,voltage,AhCounter,'spline');


            qOCV_voltage_B = zeros(1,length(AhCounter));                
            [inputSOC,indices] = unique(OCV_array{2, 1}.SOC  ,'stable');
            voltage = OCV_array{2, 1}.voltage(indices);
            qOCV_voltage_B(1,:) = interp1(inputSOC,voltage,AhCounter,'spline');

            qOCV_voltage = (qOCV_voltage_A+qOCV_voltage_B)./2;
        else
            qOCV_voltage = zeros(1,length(AhCounter));                
            [inputSOC,indices] = unique(OCV_array{1, 1}.SOC,'stable');
            voltage = OCV_array{1, 1}.voltage(indices);
            qOCV_voltage(1,:) = interp1(inputSOC,voltage,AhCounter,'spline');
        end
        qOCV_voltage(end + 1) = qOCV_voltage(end);
    end    
end

% Ende Berechnung Modellspannungn des Voigt-Netzwerks
% verwendet den ersten Index als Spannungsreferenz (sollte relaxiert sein)
% data.y = ModellSpannung+OCV_Korrigiert_Spannung+tdf_data.spannung(1);
% data.x = tdf_data.zeit;
%todo Pr�fen ob ReducedData genutzt wird im weiteren Verlauf, sonst diesen
%Schritt ignorieren
% data = ReduceData(data);
% app.TDF_Implementierung.Sim.spannung = data.y;
% app.TDF_Implementierung.Sim.zeit = data.x;

% data.x=tdf_data.zeit;
% data.y=tdf_data.spannung;
%todo Pr�fen ob ReducedData genutzt wird im weiteren Verlauf, sonst diesen
%Schritt ignorieren
% data = ReduceData(data);
% app.TDF_Implementierung.Sim.spannung_orig = data.y;
% app.TDF_Implementierung.Sim.zeit_orig = data.x;
if ~app.UseOCVCheckBox.Value
    plot(app.axes7,tdf_data.zeit,ModellSpannung+tdf_data.spannung(1),'--','color',RWTHRot,'DisplayName','Simulation','LineWidth',2);
else
    if ~isempty(qOCV_voltage)
        plot(app.axes7,tdf_data.zeit,ModellSpannung+qOCV_voltage,'--','color',RWTHRot,'DisplayName','Simulation','LineWidth',2);
    else
        plot(app.axes7,tdf_data.zeit,ModellSpannung+tdf_data.spannung(1),'--','color',RWTHRot,'DisplayName','Simulation','LineWidth',2);
    end
end
app.axes7.XLabel.String='Zeit in s';app.axes7.XLabel.Interpreter='latex';
app.axes7.YLabel.String='Spannung in V';app.axes7.YLabel.Interpreter='latex';
app.axes7.YLim = [floor(min(min(ModellSpannung+tdf_data.spannung(1)),min(tdf_data.spannung))*100)/100 ceil(max(max(ModellSpannung+tdf_data.spannung(1)),max(tdf_data.spannung))*100)/100 ];
app.axes7.XLim = [tdf_data.zeit(1) tdf_data.zeit(end)];

legend(app.axes7,'off')
h1=legend(app.axes7,'show');
set(h1,'Location','SouthEast','Interpreter','latex')
message_string = ['TDF: Plotting for ' num2str(DRT_GUI.Testparameter.Temperatur) ' �C and ' num2str(DRT_GUI.Testparameter.SOC) '% SOC finished'];
disp(message_string)
end