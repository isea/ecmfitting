function app = fcn_TDF_UpdateTable(app, event, cd_counter)
global DRT_GUI
if ~isfield(app.TDF_Implementierung,'OCV') || isempty(app.TDF_Implementierung.OCV) || (isempty(cell2mat(regexp(app.TDF_Implementierung.Table(:,2),'OCV\d', 'once'))) && isempty(cell2mat(regexp(app.TDF_Implementierung.Table(:,2),'Re-Fit', 'once'))))
    if app.GUI
        if ~isempty(DRT_GUI.Messdaten.relax.zeit)
            DeltaU = DRT_GUI.Messdaten.relax.spannung(end)-DRT_GUI.Messdaten.relax.spannung(1);
            Ladung = [0, cumsum(DRT_GUI.Messdaten.relax.strom(1:end-1) .* diff(DRT_GUI.Messdaten.relax.zeit))];
            C_OCV = Ladung(end) / DeltaU;
            app.TDF_Implementierung.OCV = {'ROCV',inf,false,-inf,inf;'COCV',C_OCV,false,-inf,inf;'UStart',DRT_GUI.Messdaten.relax.spannung(1),false,-inf,inf;'UEnde',DRT_GUI.Messdaten.relax.spannung(end),false,-inf,inf};
        else
            DeltaU = DRT_GUI.Messdaten.pulse.spannung(end)-DRT_GUI.Messdaten.pulse.spannung(1);
            Ladung = [0, cumsum(DRT_GUI.Messdaten.pulse.strom(1:end-1) .* diff(DRT_GUI.Messdaten.pulse.zeit))];
            C_OCV = Ladung(end) / DeltaU;
            app.TDF_Implementierung.OCV = {'ROCV',inf,false,-inf,inf;'COCV',C_OCV,false,-inf,inf;'UStart',DRT_GUI.Messdaten.pulse.spannung(1),false,-inf,inf;'UEnde',DRT_GUI.Messdaten.pulse.spannung(end),false,-inf,inf};
        end
    else
        qOCV_List= dir([app.OutputPath '/' DRT_GUI.Testparameter.Batterie '/' app.StateString '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad/' '*qOCV-File*']);
        ocv_file = load(qOCV_List.name);
        ocv_file_fieldnames = fieldnames(ocv_file);
        OCV_array = ocv_file.(ocv_file_fieldnames{1});
        [inputSOC_a,indices] = unique(OCV_array{1, 1}.SOC,'stable');
        voltage_a = OCV_array{1, 1}.voltage(indices);
        [inputSOC_b,indices] = unique(OCV_array{2, 1}.SOC  ,'stable');
        voltage_b = OCV_array{2, 1}.voltage(indices);
        if app.current_SOC_index == length(app.SOCs)
            necessary_SOC = [app.SOCs(end) 100];
            qOCV_voltage_A(1,:) = interp1(inputSOC_a,voltage_a,necessary_SOC,'spline');
            qOCV_voltage_B(1,:) = interp1(inputSOC_b,voltage_b,necessary_SOC,'spline');
            qOCV_voltage = (qOCV_voltage_A+qOCV_voltage_B)./2;
        else
            necessary_SOC = [app.SOCs(app.current_SOC_index) app.SOCs(app.current_SOC_index+1)];
            qOCV_voltage_A(1,:) = interp1(inputSOC_a,voltage_a,necessary_SOC,'spline');
            qOCV_voltage_B(1,:) = interp1(inputSOC_b,voltage_b,necessary_SOC,'spline');
            qOCV_voltage = (qOCV_voltage_A+qOCV_voltage_B)./2;
        end
        Ladung = (necessary_SOC(2)-necessary_SOC(1))/100*DRT_GUI.Testparameter.Cap*3600;
        DeltaU = qOCV_voltage(2)-qOCV_voltage(1);
        C_OCV = Ladung(end) / DeltaU;
        app.TDF_Implementierung.OCV = {'ROCV',inf,false,-inf,inf;'COCV',C_OCV,false,-inf,inf;'UStart',qOCV_voltage(1),false,-inf,inf;'UEnde',qOCV_voltage(2),false,-inf,inf};
    end
    if app.GUI
%         set(app.RelaxTable,'Data',app.TDF_Implementierung.OCV)
        set(app.TDF_RelaxTable,'Data',app.TDF_Implementierung.OCV)
    else
%         app.RelaxTable.Data = app.TDF_Implementierung.OCV;
        app.TDF_RelaxTable.Data = app.TDF_Implementierung.OCV;
    end
end
Crates = fcn_TDF_GetCRates(app, cd_counter);
%reset Table
if app.GUI
    previous_refit_data = get(app.TDF_RelaxTable,'Data');
else
    previous_refit_data = app.TDF_RelaxTable.Data;
end
app.TDF_Implementierung.OCV = app.TDF_Implementierung.OCV(1:4,:);

%set default boundaries and fixed parameters
if size(app.TDF_Implementierung.OCV,2) == 2
    app.TDF_Implementierung.OCV(:,3) = {false};
    app.TDF_Implementierung.OCV(:,4) = {-inf};
    app.TDF_Implementierung.OCV(:,5) = {inf};
end

%Startwerte f�r Re-Fit eintragen
n_p=5;
NumberOfParameters = 0;
for i = find(~cellfun(@isempty,regexp(app.TDF_Implementierung.Table(:,2),'Re-Fit')))'
    if strcmp(app.TDF_Implementierung.Table(i,2),'Re-Fit')
        NumberOfParameters = NumberOfParameters + numel(app.TDF_Implementierung.Info{i}.inputs);
    else
        NumberOfParameters = NumberOfParameters + numel(app.TDF_Implementierung.Info{i}.inputs) * length(Crates);
    end
end
OCV_row = 0;
OCV_row_set = 0;
if isfield(DRT_GUI.Fit.Implementierung,'OCV')
    [OCV_row,~] = size(DRT_GUI.Fit.Implementierung.OCV);
    OCV_row_set = 1;
end
% if number of elements is identical with the number in the saved table you
% that, otherwise get parameter values from the FDF
if OCV_row == NumberOfParameters + 4 && OCV_row_set
    if NumberOfParameters > 0
        NumberOfIdenticalParameters = 0;
        [row,col] = size( app.TDF_Implementierung.Table);
        for i = 1:row-1
            if strcmp(app.TDF_Implementierung.Table{i,2},'Re-Fit')
                for j = 5:col
                    if ~isempty(app.TDF_Implementierung.Table{i,j})
                        temporary_table_parameter_name = ['ReFit_' app.TDF_Implementierung.Table{i,j}];
                        for k = 1:length(DRT_GUI.Fit.Implementierung.OCV(5:end,1))
                            if strcmp(DRT_GUI.Fit.Implementierung.OCV(4+k,1),temporary_table_parameter_name)
                                NumberOfIdenticalParameters =  NumberOfIdenticalParameters + 1;
                            end
                        end
                    end
                end
            elseif strcmp(app.TDF_Implementierung.Table{i,2},'CD Re-Fit')
                for j = 5:col
                    if ~isempty(app.TDF_Implementierung.Table{i,j})
                        for n = Crates
                            temporary_table_parameter_name = [strrep(['ReFit_' app.TDF_Implementierung.Table{i,j} '_' num2str(n)],'-','m') 'C'];
                            for k = 1:length(DRT_GUI.Fit.Implementierung.OCV(5:end,1))
                                if strcmp(DRT_GUI.Fit.Implementierung.OCV(4+k,1),temporary_table_parameter_name)
                                    NumberOfIdenticalParameters =  NumberOfIdenticalParameters + 1;
                                end
                            end
                        end
                    end
                end
            end
        end
        if NumberOfIdenticalParameters == NumberOfParameters
            app.TDF_Implementierung.OCV = DRT_GUI.Fit.Implementierung.OCV;
        else
            for i = find(~cellfun(@isempty,regexp(app.TDF_Implementierung.Table(:,2),'Re-Fit')))'
                CDFlag = 0;
                if strcmp(app.TDF_Implementierung.Table(i,2),'CD Re-Fit'), CDFlag = 1; end
                for n = Crates
                    for k = 1:numel(app.TDF_Implementierung.Info{i}.inputs)
                        ParNr = find(strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),app.TDF_Implementierung.Table{i,k+4}));
                        if app.GUI
                            RelaxTableData = get(app.RelaxTable,'Data');
                        else
                            RelaxTableData = app.RelaxTable.Data;
                        end
                        
                        %construct display name with/without Current Dependency
                        if CDFlag
                            DisplayName = [strrep(['ReFit_' app.TDF_Implementierung.Table{i,k+4} '_' num2str(n)],'-','m') 'C'];
                        else
                            DisplayName = ['ReFit_' app.TDF_Implementierung.Table{i,k+4}];
                        end
                        
                        if DRT_GUI.Fit.Parameter(ParNr)==0
                            app.TDF_Implementierung.OCV(n_p,:) = {DisplayName,...
                                DRT_GUI.Fit.Parameter(ParNr),false,0,inf};
                        elseif size(RelaxTableData,1)==4+2*size(find(~cellfun(@isempty,regexp(app.TDF_Implementierung.Table(:,2),'Re-Fit')))',2)
                            app.TDF_Implementierung.OCV(n_p,:)=DRT_GUI.Fit.Implementierung.OCV(n_p,:);
                        else
%                             app.TDF_Implementierung.OCV(n_p,:) = {DisplayName,...
%                                 DRT_GUI.Fit.Parameter(ParNr),false,DRT_GUI.Fit.Parameter(ParNr)*0.25,DRT_GUI.Fit.Parameter(ParNr)*4};
                              app.TDF_Implementierung.OCV(n_p,:) = {DisplayName,...
                                DRT_GUI.Fit.Parameter(ParNr),false,0,inf};
                        end
                        n_p = n_p+1;
                    end
                    
                    %if no current dependent ECE was selected stop after 1
                    %iteration
                    if ~CDFlag
                        break
                    end
                end
            end
        end
    else
        app.TDF_Implementierung.OCV = DRT_GUI.Fit.Implementierung.OCV;
    end
elseif isfield(event,'PreviousData') &&  strcmp(event.PreviousData,'Re-Fit') && strcmp(event.NewData,'CD Re-Fit')
    %% if the edited cell was changed from ReFit to CD ReFit, adop values from ReFit data files
    for i = find(~cellfun(@isempty,regexp(app.TDF_Implementierung.Table(:,2),'Re-Fit')))'
        CDFlag = 0;
        adopt_prev_refit_values_flag = 0;
        if strcmp(app.TDF_Implementierung.Table(i,2),'CD Re-Fit'), CDFlag = 1; end
        if (i == event.Indices(1)), adopt_prev_refit_values_flag = 1; end
        for n = Crates
            for k = 1:numel(app.TDF_Implementierung.Info{i}.inputs)
                %find particle index
                ParNr = find(strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),app.TDF_Implementierung.Table{i,k+4}));
                
                %construct display name with/without Current Dependency
                if CDFlag
                    DisplayName = [strrep(['ReFit_' app.TDF_Implementierung.Table{i,k+4} '_' num2str(n)],'-','m') 'C'];
                else
                    DisplayName = ['ReFit_' app.TDF_Implementierung.Table{i,k+4}];
                end
                
                %save parameter names, limits and initial values
                if ~adopt_prev_refit_values_flag
                    if DRT_GUI.Fit.Parameter(ParNr)==0
                        app.TDF_Implementierung.OCV(n_p,:) = {DisplayName,...
                            DRT_GUI.Fit.Parameter(ParNr),false,0,inf};                    
                    else                     
                        app.TDF_Implementierung.OCV(n_p,:) = {DisplayName,...
                            DRT_GUI.Fit.Parameter(ParNr),false,0,inf};
                    end
                else
                    if DRT_GUI.Fit.Parameter(ParNr)==0
                        app.TDF_Implementierung.OCV(n_p,:) = {DisplayName,...
                            previous_refit_data(find(contains(previous_refit_data(:,1),['ReFit_' app.TDF_Implementierung.Table{i,k+4}])),2),false,0,inf};                    
                    else                     
                        app.TDF_Implementierung.OCV(n_p,:) = {DisplayName,...
                            previous_refit_data(find(contains(previous_refit_data(:,1),['ReFit_' app.TDF_Implementierung.Table{i,k+4}])),2),false,0,inf};
                    end
                end               
                
                n_p = n_p+1;
            end
            
            %if no current dependent ECE was selected stop after 1
            %iteration
            if ~CDFlag
                break
            end
        end
    end
else
    for i = find(~cellfun(@isempty,regexp(app.TDF_Implementierung.Table(:,2),'Re-Fit')))'
        CDFlag = 0;
        if strcmp(app.TDF_Implementierung.Table(i,2),'CD Re-Fit'), CDFlag = 1; end
        for n = Crates
            for k = 1:numel(app.TDF_Implementierung.Info{i}.inputs)
                %find particle index
                ParNr = find(strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),app.TDF_Implementierung.Table{i,k+4}));
                
                %construct display name with/without Current Dependency
                if CDFlag
                    DisplayName = [strrep(['ReFit_' app.TDF_Implementierung.Table{i,k+4} '_' num2str(n)],'-','m') 'C'];
                else
                    DisplayName = ['ReFit_' app.TDF_Implementierung.Table{i,k+4}];
                end
                
                %save parameter names, limits and initial values
                if DRT_GUI.Fit.Parameter(ParNr)==0
                    app.TDF_Implementierung.OCV(n_p,:) = {DisplayName,...
                        DRT_GUI.Fit.Parameter(ParNr),false,0,inf};
                    %                 elseif size(RelaxTableData,1)==4+2*size(find(~cellfun(@isempty,regexp(app.TDF_Implementierung.Table(:,2),'Re-Fit')))',2)
                    %                     app.TDF_Implementierung.OCV(n_p,:)=DRT_GUI.Fit.Implementierung.OCV(n_p,:);
                else
%                     app.TDF_Implementierung.OCV(n_p,:) = {DisplayName,...
%                         DRT_GUI.Fit.Parameter(ParNr),false,DRT_GUI.Fit.Parameter(ParNr)*0.25,DRT_GUI.Fit.Parameter(ParNr)*4};
                    app.TDF_Implementierung.OCV(n_p,:) = {DisplayName,...
                            DRT_GUI.Fit.Parameter(ParNr),false,0,inf};
                end
                n_p = n_p+1;
            end
            
            %if no current dependent ECE was selected stop after 1
            %iteration
            if ~CDFlag
                break
            end
        end
    end
end
if app.GUI
    set(app.TDF_RelaxTable,'Data',app.TDF_Implementierung.OCV)
else
    app.TDF_RelaxTable.Data = app.TDF_Implementierung.OCV;
end
end