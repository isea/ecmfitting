 function U = fcn_TDF_Fit_OCV_Spannung(app, p, t, args)
% Achtung, COCV ist kein reales Element im Netzwerk. Es dient nur zur
% Berechnung der Spannung auf der OCV-Spannungsquelle.
OCV = args{1};
ReFit = args{2};
strom = args{3};
stromindex = args{4};
ModellSpannung = args{5};
U_Mess = args{6};
%%%IndexOhneWertung ist in den Inputdaten das 7te Element nicht das 8te o.O
if numel(args)>6
    IndexOhneWertung=args{7};
    qOCV = args{8};
    cd_time_interval = args{9};
    cd_crate = args{10};
    cd_parameter_idx = args{11};
    crate_counter = length(unique(cd_parameter_idx));
    stromindex_to_CD_idx = args{12};
end

%% source of errors if the first voltage sample is not in a relaxed state !!!
if ~isempty(qOCV)
    U = ModellSpannung + transpose(qOCV) ;
else
    U = ModellSpannung+U_Mess(1) ;
end

ReFitSpannung = zeros(size(ModellSpannung));
if ~isempty(ReFit)
    n_p = 1;
    Voigt.R_RC = [];
    Voigt.C_RC = [];
    Voigt.R_ser = 0;
    Voigt.C_ser = [];
    Voigt.CD = [];
    Voigt.RCIdx = [];
    for i = 1:numel(ReFit) 
        % Erzeuge f�r alle Implementierungselemente die entsprechenden
        % Voigt-Netzwerke, sowie Rser und Cser
        argliste = '';        
        for k = 1:numel(ReFit(i).args)                   
            ReFit(i).args{k} = p(n_p);
            n_p = n_p+1;
            argliste = [argliste 'ReFit(i).args{' num2str(k) '},' ];            
        end
        argliste = argliste(1:end-1);
        if ismember('Funktionsname',fieldnames(ReFit(i).Info))
            eval(['[R_RC, C_RC, C_ser, R_ser, R_Ladder, C_Ladder] = RunLocalESBeFunction(ReFit(i).Info.Funktionsname,' argliste ');'])
        elseif ismember('f',fieldnames(ReFit(i).Info))
            eval(['[R_RC, C_RC, C_ser, R_ser, R_Ladder, C_Ladder] = ReFit(i).Info.f(' argliste ');'])
        end

        % Falls nicht, liegt es als reines Voigtnetzwerk mit Rser und Cser vor
        Voigt.R_RC = [Voigt.R_RC ;R_RC'];
        Voigt.C_RC = [Voigt.C_RC ;C_RC'];
        Voigt.CD = [Voigt.CD; ReFit(i).CD*ones(length(R_RC),1)];
        Voigt.RCIdx = [Voigt.RCIdx; ReFit(i).RC_counter*ones(length(R_RC),1)];
        if ~isempty(R_ser) && ~ReFit(i).CD 
            Voigt.R_ser = Voigt.R_ser + R_ser; 
        end
        if ~isempty(C_ser) && abs(C_ser) < 1e16
            if isempty(Voigt.C_ser) || abs(Voigt.C_ser)>1e16
                Voigt.C_ser = C_ser;
            else
                Voigt.C_ser = (Voigt.C_ser .* C_ser)./(Voigt.C_ser + C_ser);
            end
        end
    end
    
    %evaluate Voigt circuit -> gain current state of voltage response to
    %the given current profile as predicted by the model
    U_RC = zeros(numel(ReFitSpannung),numel(Voigt.R_RC));
    AlleStrom = zeros(numel(ReFitSpannung),1);
    for i = 1:numel(stromindex)-1
        if stromindex(i)==1
            U0 = zeros(size(Voigt.R_RC));
        else
            U0 = U_RC(stromindex(i),:);
        end
        Umax = strom(i).*Voigt.R_RC;
        zeit = t(stromindex(i)+1:stromindex(i+1))-t(stromindex(i));
        AlleStrom(stromindex(i):stromindex(i+1)-1) =  strom(i);
        for k = 1:numel(Voigt.R_RC)
            if ~Voigt.CD(k)
                if abs(U0(k)-Umax(k))<=0.0001
                    U_RC(stromindex(i)+1:stromindex(i+1),k) = Umax(k);
                else
                    U_RC(stromindex(i)+1:stromindex(i+1),k) = U0(k)+(Umax(k)-U0(k)).*(1-exp(-zeit./(Voigt.R_RC(k).*Voigt.C_RC(k))));
                end
            elseif cd_parameter_idx(stromindex_to_CD_idx(i)) == Voigt.CD(k)
                if abs(U0(k)-Umax(k))<=0.0001
                    U_RC(stromindex(i)+1:stromindex(i+1),k) = Umax(k);
                else
                    U_RC(stromindex(i)+1:stromindex(i+1),k) = U0(k)+(Umax(k)-U0(k)).*(1-exp(-zeit./(Voigt.R_RC(k).*Voigt.C_RC(k))));
                end
            end
        end
        ReFitSpannung(stromindex(i)+1:stromindex(i+1))=ReFitSpannung(stromindex(i)+1:stromindex(i+1))+ ...
            strom(i) .* Voigt.R_ser;
    end
    if ~(isempty(Voigt.C_ser) ||  abs(Voigt.C_ser)>1e16)
        CserSpannung = cumsum([ diff(t(stromindex(1):stromindex(end))) ; 0 ].*AlleStrom)./Voigt.C_ser;
    else
        CserSpannung = zeros(numel(ReFitSpannung),1);
    end
    ReFitSpannung = ReFitSpannung + sum(U_RC,2) + CserSpannung;
    U = U+ReFitSpannung;
end
U = [U zeros(size(U))];
%display(sprintf('Cserspannung: %f,   Cser: %f      As: %f', CserSpannung(end), Voigt.C_ser,sum([ diff(t(stromindex(1):stromindex(end))) ; 0 ].*AlleStrom)));
n_p = 1;
for i = 1:numel(ReFit)
    % Results aus ReFit in OCV-Tabelle eintragen
    for k = 1:numel(ReFit(i).args)
        ReFit(i).args{k} = p(n_p);
        n_p = n_p+1;
        if ~ReFit(i).CD
            app.TDF_Implementierung.OCV{find(strcmp(app.TDF_Implementierung.OCV(:,1),['ReFit_' ReFit(i).ParNames{k}]),1),2} = ReFit(i).args{k};
        else
            app.TDF_Implementierung.OCV{find(strcmp(app.TDF_Implementierung.OCV(:,1),['ReFit_' ReFit(i).ParNames{k}  '_' strrep(num2str(round(cd_crate{ReFit(i).CD},1)),'-','m') 'C']),1),2} = ReFit(i).args{k};
        end
    end
end

if numel(args)>6
    U(IndexOhneWertung,1) = U_Mess(IndexOhneWertung);
end
end