function fcn_TDF_PlotNyquistButton(app,event)
global DRT_GUI
if app.CRateFlag && app.HysteresisFlag
    StateString = [DRT_GUI.Testparameter.Zustand_2 '_' DRT_GUI.Testparameter.Zustand];
elseif app.CRateFlag
    StateString = DRT_GUI.Testparameter.Zustand;
else
    StateString = DRT_GUI.Testparameter.Zustand_2;
end
Z = 0;
Z_RC = 0;
for i = 1:numel(app.TDF_Implementierung.Info)-1
    args = cell(1,numel(app.TDF_Implementierung.Info{i}.inputs));
    argliste = '';
    for k = 1:numel(app.TDF_Implementierung.Info{i}.inputs)
        ParNr = find(strcmp(DRT_GUI.Fit.aktuell_Modell.P_Name(1,:),app.TDF_Implementierung.Table{i,k+4}));
        if isempty(ParNr)
            warning('Konnte Parameter aus Implementierung nicht finden. Implementierungsinfos werden reseted!')
            DRT_GUI.Fit=rmfield(DRT_GUI.Fit,'Implementierung');
            return
        end
        args{k} = DRT_GUI.Fit.Parameter(ParNr);
        if strcmp(app.TDF_Implementierung.Table{i,2},'Re-Fit')
            args{k} = app.TDF_Implementierung.OCV{find(strcmp(app.TDF_Implementierung.OCV(:,1),['ReFit_' app.TDF_Implementierung.Table{i,k+4}]),1),2};
        end
        argliste = [argliste 'args{' num2str(k) '},' ];
    end
    argliste = argliste(1:end-1);
    %     Z_neu = eval(['app.TDF_Implementierung.Info{i}.Z(' argliste ',DRT_GUI.Fit.omega)']);
    %     figure
    %     plot(DRT_GUI.Fit.Zreal,DRT_GUI.Fit.Zimg,'ob');hold on;
    %     plot(real(Z_neu),imag(Z_neu),'xr');
    %     grid on; axis square; axis equal; set(gca,'ydir', 'reverse');
    %     xlabel('Re');ylabel('Im'); %title(Plot_Title,'Interpreter','none');
    %
    %     h1 = legend('Fitting','Implementierung','Location','NorthWest');
    %     set(h1,'Interpreter','none');
    %     axis equal
    %
    if strcmp(app.TDF_Implementierung.Table{i,1},'OCV_source')
        
        DeltaU = DRT_GUI.Messdaten.relax.spannung(end)-DRT_GUI.Messdaten.relax.spannung(1);
        Ladung = [0 cumsum(DRT_GUI.Messdaten.relax.strom(1:end-1) .* diff(DRT_GUI.Messdaten.relax.zeit))];
        C_OCV = Ladung(end) / DeltaU;
        
        if ismember('Funktionsname',fieldnames(app.TDF_Implementierung.Info{i}))
            Z = Z+CalculateESBeImpedance(app.TDF_Implementierung.Info{i},DRT_GUI.Fit.omega,C_OCV);
            [R_RC, C_RC, C_ser, R_ser, R_Ladder, C_Ladder] = RunLocalESBeFunction(app.TDF_Implementierung.Info{i}.Funktionsname,C_OCV);
        elseif ismember('f',fieldnames(app.TDF_Implementierung.Info{i}))
            Z = Z+app.TDF_Implementierung.Info{i}.Z(C_OCV,DRT_GUI.Fit.omega);
            [R_RC, C_RC, C_ser, R_ser, R_Ladder, C_Ladder] = app.TDF_Implementierung.Info{i}.f(C_OCV);
        end
    elseif isempty(argliste)
        if ismember('Funktionsname',fieldnames(app.TDF_Implementierung.Info{i}))
            Z = Z+CalculateESBeImpedance(app.TDF_Implementierung.Info{i},DRT_GUI.Fit.omega);
            [R_RC, C_RC, C_ser, R_ser, R_Ladder, C_Ladder] = RunLocalESBeFunction(app.TDF_Implementierung.Info{i}.Funktionsname);
        elseif ismember('f',fieldnames(app.TDF_Implementierung.Info{i}))
            Z =Z+app.TDF_Implementierung.Info{i}.Z(DRT_GUI.Fit.omega);
            [R_RC, C_RC, C_ser, R_ser, R_Ladder, C_Ladder] = app.TDF_Implementierung.Info{i}.f;
        end
    else
        if ismember('Funktionsname',fieldnames(app.TDF_Implementierung.Info{i}))
            Z = Z+ CalculateESBeImpedance(app.TDF_Implementierung.Info{i},DRT_GUI.Fit.omega,args{:});
            [R_RC, C_RC, C_ser, R_ser, R_Ladder, C_Ladder] = RunLocalESBeFunction(app.TDF_Implementierung.Info{i}.Funktionsname,args{:});
        elseif ismember('f',fieldnames(app.TDF_Implementierung.Info{i}))
            eval(['Z=Z+app.TDF_Implementierung.Info{i}.Z(' argliste ',DRT_GUI.Fit.omega);'])
            eval(['[R_RC, C_RC, C_ser, R_ser, R_Ladder, C_Ladder] = app.TDF_Implementierung.Info{i}.f(' argliste ');'])
        end
    end
    if ~isempty(R_ser), Z_RC = Z_RC + R_ser;end
    if ~isempty(C_ser), Z_RC = Z_RC + 1./(1i.*DRT_GUI.Fit.omega.*C_ser);end
    for k = 1:numel(R_RC)
        Z_RC = Z_RC + R_RC(k)./(1+1i.*DRT_GUI.Fit.omega.*R_RC(k).*C_RC(k));
    end
end


FarbenLaden
if isempty(dir('export'))
    mkdir('export')
end
if isempty( dir(['export' '/' DRT_GUI.Testparameter.Batterie]))
    mkdir(['export' '/' DRT_GUI.Testparameter.Batterie])
end


figure('Name','Vgl. EIS-Relax','NumberTitle','off','UnitS','centimeters','Position',[0, 0, 25, 10])
subplot(2,5,[1:2 6:7])
plot(DRT_GUI.Fit.Zreal,DRT_GUI.Fit.Zimg,'o','color',RWTHBlau,'DisplayName','EIS-Modell','LineWidth',1,'MarkerSize',7);hold on;
plot(real(Z),imag(Z),'o','color',RWTHRot,'DisplayName','Zeitbereichsmodell','LineWidth',1,'MarkerSize',7);
% plot(real(Z_RC),imag(Z_RC),'x','color',RWTHOrange,'DisplayName','Implementierung','LineWidth',1,'MarkerSize',7);
plot(DRT_GUI.Fit.Zreal-real(Z),DRT_GUI.Fit.Zimg-imag(Z),':','color',RWTHSchwarz,'DisplayName','Differenz(EIS-Zeitbereich)','LineWidth',2,'MarkerSize',7)
grid on; axis square; axis equal; set(gca,'ydir', 'reverse');
xlabel('$\Re\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
ylabel('$\Im\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
h1 = legend('show','Location','NorthWest');
set(h1,'Interpreter','latex');
set(gca,'TickLabelInterpreter','latex')
axis equal
subplot(2,5,3:5)
semilogx(DRT_GUI.Fit.frequenz, DRT_GUI.Fit.Zreal,'-','color',RWTHBlau,'DisplayName','EIS-Modell','LineWidth',2,'MarkerSize',7);hold on;
semilogx(DRT_GUI.Fit.frequenz, real(Z),'--','color',RWTHRot,'DisplayName','Zeitbereichsmodell','LineWidth',2,'MarkerSize',7);
% semilogx(DRT_GUI.Fit.frequenz, real(Z_RC),'--','color',RWTHOrange,'DisplayName','Implementierung','LineWidth',2,'MarkerSize',7);
semilogx(DRT_GUI.Fit.frequenz, DRT_GUI.Fit.Zreal-real(Z),'--','color',RWTHSchwarz,'DisplayName','Differenz(EIS-Zeitbereich)','LineWidth',2,'MarkerSize',7)
set(gca,'xdir','reverse'),grid on, hold on
set(gca,'TickLabelInterpreter','latex')
h1 = legend('show','Location','NorthWest');
set(h1,'Interpreter','latex');
ylabel('$\Re\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
xlabel('$f$ in Hz','Interpreter','latex');
subplot(2,5,8:10)
semilogx(DRT_GUI.Fit.frequenz, DRT_GUI.Fit.Zimg,'-','color',RWTHBlau,'DisplayName','EIS-Modell','LineWidth',2,'MarkerSize',7);hold on;
semilogx(DRT_GUI.Fit.frequenz, imag(Z),'--','color',RWTHRot,'DisplayName','Zeitbereichsmodell','LineWidth',2,'MarkerSize',7);
% semilogx(DRT_GUI.Fit.frequenz, imag(Z_RC),'--','color',RWTHOrange,'DisplayName','Implementierung','LineWidth',2,'MarkerSize',7);
semilogx(DRT_GUI.Fit.frequenz, DRT_GUI.Fit.Zimg-imag(Z),'--','color',RWTHSchwarz,'DisplayName','Differenz(EIS-Zeitbereich)','LineWidth',2,'MarkerSize',7)
set(gca,'xdir','reverse'),grid on, hold on
set(gca,'ydir','reverse')
set(gca,'TickLabelInterpreter','latex')
ylabel('$\Im\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
xlabel('$f$ in Hz','Interpreter','latex');
savefig(['export/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie '_vgl_EIS_TDM_' ...
    StateString '_' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad_'...
    strrep(num2str(DRT_GUI.Testparameter.SOC),'-','m') 'SOC'...
    '.fig'])


figure('Name','Diff. EIS-Relax','NumberTitle','off','UnitS','centimeters','Position',[0, 15, 25, 10])
subplot(2,5,[1:2 6:7])
% plot(DRT_GUI.Fit.Zreal,DRT_GUI.Fit.Zimg,'o','color',RWTHBlau,'DisplayName','EIS-Modell','LineWidth',1,'MarkerSize',7);hold on;
% plot(real(Z),imag(Z),'o','color',RWTHRot,'DisplayName','Zeitbereichsmodell','LineWidth',1,'MarkerSize',7);
% plot(real(Z_RC),imag(Z_RC),'x','color',RWTHOrange,'DisplayName','Implementierung','LineWidth',1,'MarkerSize',7);
plot(DRT_GUI.Messdaten.Zreal(DRT_GUI.Messdaten.aktiv==1)-real(Z),DRT_GUI.Messdaten.Zimg(DRT_GUI.Messdaten.aktiv==1)-imag(Z),'-','color',RWTHBlau,'DisplayName','Differenz(EIS-TDMFit)','LineWidth',2,'MarkerSize',7)
hold on
plot(DRT_GUI.Fit.Zreal-real(Z),DRT_GUI.Fit.Zimg-imag(Z),'--','color',RWTHDunkelgrau,'DisplayName','Differenz(EISFit-TDMFit)','LineWidth',2,'MarkerSize',7)
grid on; axis square; axis equal; set(gca,'ydir', 'reverse');
xlabel('$\Re\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
ylabel('$\Im\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
h1 = legend('show','Location','NorthWest');
set(h1,'Interpreter','latex');
set(gca,'TickLabelInterpreter','latex')
axis equal
subplot(2,5,3:5)
% semilogx(DRT_GUI.Fit.frequenz, DRT_GUI.Fit.Zreal,'-','color',RWTHBlau,'DisplayName','EIS-Modell','LineWidth',2,'MarkerSize',7);hold on;
% semilogx(DRT_GUI.Fit.frequenz, real(Z),'--','color',RWTHRot,'DisplayName','Zeitbereichsmodell','LineWidth',2,'MarkerSize',7);
% semilogx(DRT_GUI.Fit.frequenz, real(Z_RC),'--','color',RWTHOrange,'DisplayName','Implementierung','LineWidth',2,'MarkerSize',7);
semilogx(DRT_GUI.Fit.frequenz, DRT_GUI.Messdaten.Zreal(DRT_GUI.Messdaten.aktiv==1)-real(Z),'-','color',RWTHBlau,'DisplayName','Differenz(EIS-TDMFit)','LineWidth',2,'MarkerSize',7)
hold on
semilogx(DRT_GUI.Fit.frequenz, DRT_GUI.Fit.Zreal-real(Z),'--','color',RWTHDunkelgrau,'DisplayName','Differenz(EISFit-TDMFit)','LineWidth',2,'MarkerSize',7)
set(gca,'xdir','reverse'),grid on, hold on
set(gca,'TickLabelInterpreter','latex')
h1 = legend('show','Location','NorthWest');
set(h1,'Interpreter','latex');
ylabel('$\Re\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
xlabel('$f$ in Hz','Interpreter','latex');
subplot(2,5,8:10)
% semilogx(DRT_GUI.Fit.frequenz, DRT_GUI.Fit.Zimg,'-','color',RWTHBlau,'DisplayName','EIS-Modell','LineWidth',2,'MarkerSize',7);hold on;
% semilogx(DRT_GUI.Fit.frequenz, imag(Z),'--','color',RWTHRot,'DisplayName','Zeitbereichsmodell','LineWidth',2,'MarkerSize',7);
% semilogx(DRT_GUI.Fit.frequenz, imag(Z_RC),'--','color',RWTHOrange,'DisplayName','Implementierung','LineWidth',2,'MarkerSize',7);
semilogx(DRT_GUI.Fit.frequenz, DRT_GUI.Messdaten.Zimg(DRT_GUI.Messdaten.aktiv==1)-imag(Z),'-','color',RWTHBlau,'DisplayName','Differenz(EIS-TDMFit)','LineWidth',2,'MarkerSize',7)
hold on
semilogx(DRT_GUI.Fit.frequenz, DRT_GUI.Fit.Zimg-imag(Z),'--','color',RWTHDunkelgrau,'DisplayName','Differenz(EISFit-TDMFit)','LineWidth',2,'MarkerSize',7)

set(gca,'xdir','reverse'),grid on, hold on
set(gca,'ydir','reverse')
set(gca,'TickLabelInterpreter','latex')
ylabel('$\Im\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
xlabel('$f$ in Hz','Interpreter','latex');
savefig(['export/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie '_diff_EIS_TDM_' ...
    StateString '_' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad_'...
    strrep(num2str(DRT_GUI.Testparameter.SOC),'-','m') 'SOC'...
    '.fig'])




figure('Name','Vgl. Fit-Implementierung','NumberTitle','off','UnitS','centimeters','Position',[25, 0, 25, 10])
subplot(2,5,[1:2 6:7])
plot(DRT_GUI.Fit.Zreal,DRT_GUI.Fit.Zimg,'o','color',RWTHBlau,'DisplayName','EIS-Modell','LineWidth',1,'MarkerSize',7);hold on;
plot(real(Z),imag(Z),'o','color',RWTHRot,'DisplayName','Zeitbereichsmodell','LineWidth',1,'MarkerSize',7);
plot(real(Z_RC),imag(Z_RC),'x','color',RWTHSchwarz,'DisplayName','Implementierung','LineWidth',1,'MarkerSize',7);
grid on; axis square; axis equal; set(gca,'ydir', 'reverse');
xlabel('$\Re\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
ylabel('$\Im\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
h1 = legend('show','Location','NorthWest');
set(h1,'Interpreter','latex');
set(gca,'TickLabelInterpreter','latex')
axis equal,
subplot(2,5,3:5)
semilogx(DRT_GUI.Fit.frequenz, DRT_GUI.Fit.Zreal,'-','color',RWTHBlau,'DisplayName','EIS-Modell','LineWidth',2,'MarkerSize',7);hold on;
semilogx(DRT_GUI.Fit.frequenz, real(Z),'--','color',RWTHRot,'DisplayName','Zeitbereichsmodell','LineWidth',2,'MarkerSize',7);
semilogx(DRT_GUI.Fit.frequenz, real(Z_RC),'--','color',RWTHSchwarz,'DisplayName','Implementierung','LineWidth',2,'MarkerSize',7);
% semilogx(DRT_GUI.Fit.frequenz, DRT_GUI.Messdaten.Zreal-real(Z),':','color',RWTHSchwarz,'DisplayName','Differenz(EIS-Zeitbereich)','LineWidth',2,'MarkerSize',7)
set(gca,'xdir','reverse'),grid on, hold on
set(gca,'TickLabelInterpreter','latex')
h1 = legend('show','Location','NorthWest');
set(h1,'Interpreter','latex');
ylabel('$\Re\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
xlabel('$f$ in Hz','Interpreter','latex');
subplot(2,5,8:10)
semilogx(DRT_GUI.Fit.frequenz, DRT_GUI.Fit.Zimg,'-','color',RWTHBlau,'DisplayName','EIS-Modell','LineWidth',2,'MarkerSize',7);hold on;
semilogx(DRT_GUI.Fit.frequenz, imag(Z),'--','color',RWTHRot,'DisplayName','Zeitbereichsmodell','LineWidth',2,'MarkerSize',7);
semilogx(DRT_GUI.Fit.frequenz, imag(Z_RC),'--','color',RWTHSchwarz,'DisplayName','Implementierung','LineWidth',2,'MarkerSize',7);
% semilogx(DRT_GUI.Fit.frequenz, DRT_GUI.Messdaten.Zimg-imag(Z),':','color',RWTHSchwarz,'DisplayName','Differenz(EIS-Zeitbereich)','LineWidth',2,'MarkerSize',7)
set(gca,'xdir','reverse'),grid on, hold on
set(gca,'ydir','reverse')
set(gca,'TickLabelInterpreter','latex')
ylabel('$\Im\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
xlabel('$f$ in Hz','Interpreter','latex');
savefig(['export/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie '_vgl_Fit_Implementierung_' ...
    StateString '_' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad_'...
    strrep(num2str(DRT_GUI.Testparameter.SOC),'-','m') 'SOC'...
    '.fig'])
end