function app = fcn_TDF_RelaxTable_CellEdit(app,event)
if app.GUI
    app.TDF_Implementierung.OCV = get(app.TDF_RelaxTable,'Data');
else
    app.TDF_Implementierung.OCV = app.TDF_RelaxTable.Data;
end
end