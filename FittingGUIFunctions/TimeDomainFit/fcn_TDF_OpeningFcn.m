function app = fcn_TDF_OpeningFcn(app, event)
global Modellliste
global DRT_GUI
if isempty(DRT_GUI) || isempty(cell2mat(strfind(fieldnames(DRT_GUI),'Fit')))
    return
end
%Schreibe Parametertabelle in GUI
app.TDF_ImplementierungsTable.ColumnFormat{2}=[{' '}     {'Re-Fit'}    {'CD Re-Fit'}];
if app.GUI
    ColFormat = get(app.TDF_ImplementierungsTable,'ColumnFormat');
else
    ColFormat = app.TDF_ImplementierungsTable.ColumnFormat;
end
ColFormat(1,1) = {[  ' '  ;fieldnames(Modellliste.Implementierung)]'};
ColFormat(1,5:end) = {''};
if app.GUI
    set(app.TDF_ImplementierungsTable,'ColumnFormat',ColFormat);
else
    app.TDF_ImplementierungsTable.ColumnFormat = ColFormat;
end
if sum(strcmp(fieldnames(DRT_GUI.Fit),'Implementierung')) && ~isempty(DRT_GUI.Fit.Implementierung)
    app.TDF_Implementierung = DRT_GUI.Fit.Implementierung;
    if app.GUI
        set(app.TDF_ImplementierungsTable,'Data',app.TDF_Implementierung.Table);
    else
        app.TDF_ImplementierungsTable.Data = app.TDF_Implementierung.Table;
    end
    app = fcn_TDF_ImplementierungsTable_CellEdit(app,event);
end

if ~sum(strcmp(fieldnames(DRT_GUI.Fit),'Implementierung')) || isempty(DRT_GUI.Fit.Implementierung)...
        || ~sum(strcmp(fieldnames(DRT_GUI.Fit.Implementierung),'OCV')) || isempty(app.TDF_Implementierung.OCV)
    RelaxCell = {};
    if strcmp(app.ModeSwitch.Value,'manually')
        if app.GUI
            set(app.RelaxTable,'Data',RelaxCell)
        else
            app.RelaxTable.Data  = RelaxCell;
        end
    end
    if app.GUI
        set(app.TDF_RelaxTable,'Data',RelaxCell)
    else
        app.TDF_RelaxTable.Data = RelaxCell;
    end
else
    if size(app.TDF_Implementierung.OCV,2) == 2 % altes Format
        app.TDF_Implementierung.OCV(:,3) = {false};
        app.TDF_Implementierung.OCV(:,4) = {-inf};
        app.TDF_Implementierung.OCV(:,5) = {inf};
    end
    %Tabellen (links) f�llen
    if strcmp(app.ModeSwitch.Value,'manually')
        if app.GUI
            set(app.RelaxTable,'Data', app.TDF_Implementierung.OCV)
        else
            app.RelaxTable.Data = app.TDF_Implementierung.OCV;
        end
    end
    if app.GUI
        set(app.TDF_RelaxTable,'Data', app.TDF_Implementierung.OCV);        
    else
        app.TDF_RelaxTable.Data = app.TDF_Implementierung.OCV;        
    end
end
if ~sum(strcmp(fieldnames(app),'TDF_Implementierung')) || ~sum(strcmp(fieldnames(app.TDF_Implementierung),'Config')) || isempty(app.TDF_Implementierung.Config)
    app.TDF_Implementierung.Config.PauseDchRatio = 100;
end
if app.GUI && (~isempty(DRT_GUI.Messdaten.relax.zeit) && ~isempty(DRT_GUI.Messdaten.relax.spannung) && ~isempty(DRT_GUI.Messdaten.relax.strom))
    distinct_index = [1, 1+find(abs(diff(DRT_GUI.Messdaten.relax.zeit))>0)];
    DRT_GUI.Messdaten.relax.zeit = DRT_GUI.Messdaten.relax.zeit(distinct_index);
    DRT_GUI.Messdaten.relax.spannung = DRT_GUI.Messdaten.relax.spannung(distinct_index);
    DRT_GUI.Messdaten.relax.strom = DRT_GUI.Messdaten.relax.strom(distinct_index);
end
eventdata1.Indices=[1 1 ];
app = fcn_TDF_ImplementierungsTable_CellSelection(app, eventdata1);
if app.GUI
    set(app.RelaxTable,'Data',app.TDF_Implementierung.OCV)
else
    app.RelaxTable.Data = app.TDF_Implementierung.OCV;
end
end