function [Crates] = fcn_TDF_GetCRates(app, cd_counter)
%get all relevant C rates from the pulse profile as it is done in
%fcn_TDF_OCVFitButton.m
Crates = [];
global DRT_GUI;
pulse_data_fieldname.pulse = isfield(DRT_GUI.Messdaten,'pulse');
pulse_data_fieldname.vor_relax = isfield(DRT_GUI.Messdaten,'vor_relax');
pulse_data_fieldname.nach_relax = isfield(DRT_GUI.Messdaten,'nach_relax');
pulse_data_existent_flag = 0;
if pulse_data_fieldname.pulse
    used_pulse_data_name = 'pulse';
    pulse_data_existent_flag = 1;
elseif pulse_data_fieldname.nach_relax
    used_pulse_data_name = 'nach_relax';
    pulse_data_existent_flag = 1;
else
    used_pulse_data_name = 'vor_relax';
    pulse_data_existent_flag = 1;
end
%determine which data can be used
used_tdf_data = 'Relax';
if app.BothButton.Value
    if pulse_data_existent_flag && isfield(DRT_GUI.Messdaten,'relax')
        if ~isempty(DRT_GUI.Messdaten.(used_pulse_data_name).spannung) && ~isempty(DRT_GUI.Messdaten.(used_pulse_data_name).strom) && ~isempty(DRT_GUI.Messdaten.(used_pulse_data_name).zeit)
            used_tdf_data = 'Both';
        end
    end
elseif app.PulsesButton.Value && strcmp(used_tdf_data,'Relax')
    if pulse_data_existent_flag
        if ~isempty(DRT_GUI.Messdaten.(used_pulse_data_name).spannung) && ~isempty(DRT_GUI.Messdaten.(used_pulse_data_name).strom) && ~isempty(DRT_GUI.Messdaten.(used_pulse_data_name).zeit)
            used_tdf_data = 'Pulses';
        end
    end
end

%initialize data arrays depending on the chosen data set
tdf_data.spannung = [];
tdf_data.strom = [];
tdf_data.zeit = [];
if strcmp(used_tdf_data,'Relax')
    if isempty(DRT_GUI.Messdaten.relax.zeit) || isempty(DRT_GUI.Messdaten.relax.strom) || isempty(DRT_GUI.Messdaten.relax.spannung)
        warning('No Relax Data found')
        return
    else
        tdf_data.spannung = DRT_GUI.Messdaten.relax.spannung;
        tdf_data.strom = DRT_GUI.Messdaten.relax.strom;
        tdf_data.zeit = DRT_GUI.Messdaten.relax.zeit;
        %if there current at the beginning of the profile, add a initial
        %data point
        if abs(tdf_data.strom(1)) > 0.001
            tdf_data.zeit = [0, tdf_data.zeit + 0.001];
            tdf_data.strom = [0, tdf_data.strom];
            tdf_data.spannung = [tdf_data.spannung(1) , tdf_data.spannung];
        end
    end
elseif strcmp(used_tdf_data,'Pulses')
    tdf_data.spannung = DRT_GUI.Messdaten.(used_pulse_data_name).spannung;
    tdf_data.strom = DRT_GUI.Messdaten.(used_pulse_data_name).strom;
    tdf_data.zeit = DRT_GUI.Messdaten.(used_pulse_data_name).zeit;
else
    tdf_data.spannung = [DRT_GUI.Messdaten.relax.spannung-(DRT_GUI.Messdaten.relax.spannung(end)-DRT_GUI.Messdaten.(used_pulse_data_name).spannung(1)) DRT_GUI.Messdaten.(used_pulse_data_name).spannung];
    tdf_data.strom = [DRT_GUI.Messdaten.relax.strom DRT_GUI.Messdaten.(used_pulse_data_name).strom];
    tdf_data.zeit = [DRT_GUI.Messdaten.relax.zeit (DRT_GUI.Messdaten.(used_pulse_data_name).zeit + DRT_GUI.Messdaten.relax.zeit(end))];
    end_of_relax_idx = length(DRT_GUI.Messdaten.relax.zeit);
    %if there current at the beginning of the profile, add a initial
    %data point
    if abs(tdf_data.strom(1)) > 0.001
        tdf_data.zeit = [0, tdf_data.zeit + 0.001];
        tdf_data.strom = [0, tdf_data.strom];
        tdf_data.spannung = [tdf_data.spannung(1) , tdf_data.spannung];
        end_of_relax_idx = end_of_relax_idx + 1;
    end
end

%check for points in time with more than one data point
[tdf_data.zeit,unique_td_data_index]= unique(tdf_data.zeit);
tdf_data.strom = tdf_data.strom(unique_td_data_index);
tdf_data.spannung = tdf_data.spannung(unique_td_data_index);

%upsampling for homogenous weighting of voltage errors
tempzeit = 0:0.1:tdf_data.zeit(end);
tdf_data.spannung = interp1(tdf_data.zeit,tdf_data.spannung,tempzeit);
tdf_data.strom = interp1(tdf_data.zeit,tdf_data.strom,tempzeit);
tdf_data.zeit = tempzeit;

%detect steps within current profile for the final profile
stromindex = find(abs(tdf_data.strom)<0.001);
diff_stromindex = diff(stromindex);
strom_sprungindex = find(diff_stromindex > 1);
counter = 1;
counter_ampere_idx = 1;
strom_sprungindex_anfang = [];
strom_sprungindex_ende = [];
strom_sprungindex_ampere_idx = [];
strom_sprungindex_ampere = [];
filtered_current = medfilt1(tdf_data.strom);
for n = 1:length(strom_sprungindex)
    if stromindex(strom_sprungindex(n) + 1) <= length(tdf_data.strom)
        end_of_pulse = stromindex(strom_sprungindex(n) + 1);
    else
        end_of_pulse = length(tdf_data.strom);
    end
    if (abs(tdf_data.zeit(end_of_pulse)-tdf_data.zeit(stromindex(strom_sprungindex(n)))) > 1) && (max(abs(tdf_data.strom(stromindex(strom_sprungindex(n)):end_of_pulse))) > 1/20 * DRT_GUI.Testparameter.Cap)
        strom_sprungindex_anfang(counter) = stromindex(strom_sprungindex(n));
        strom_sprungindex_ende(counter) = end_of_pulse;
        %check whetther there are acutally 2 pulses in inverse
        %direction and divide it into two pulses
        if ~isempty(find(filtered_current(strom_sprungindex_anfang(counter):strom_sprungindex_ende(counter)) > 1)) && ~isempty(find(filtered_current(strom_sprungindex_anfang(counter):strom_sprungindex_ende(counter)) < -1))
            if find(filtered_current(strom_sprungindex_anfang(counter):strom_sprungindex_ende(counter)) > 0,1,'last') > find(filtered_current(strom_sprungindex_anfang(counter):strom_sprungindex_ende(counter)) < 0,1,'last')
                strom_sprungindex_ende(counter) =  strom_sprungindex_anfang(counter) + find(filtered_current(strom_sprungindex_anfang(counter):strom_sprungindex_ende(counter)) < 0,1,'last');
            else
                strom_sprungindex_ende(counter) =  strom_sprungindex_anfang(counter) + find(filtered_current(strom_sprungindex_anfang(counter):strom_sprungindex_ende(counter)) > 0,1,'last');
            end
            strom_sprungindex_ende(counter + 1) = end_of_pulse;
            strom_sprungindex_anfang(counter + 1) = strom_sprungindex_ende(counter) + 1;
            strom_sprungindex_ampere(counter_ampere_idx) = tdf_data.strom(round(strom_sprungindex_anfang(counter)+(strom_sprungindex_ende(counter)-strom_sprungindex_anfang(counter))/2));
            strom_sprungindex_ampere(counter_ampere_idx + 1) = tdf_data.strom(round(strom_sprungindex_anfang(counter + 1)+(strom_sprungindex_ende(counter + 1)-strom_sprungindex_anfang(counter + 1))/2));
            strom_sprungindex_ampere_idx(counter_ampere_idx) = strom_sprungindex_ampere_idx(counter_ampere_idx - 1) + 1;
            strom_sprungindex_ampere_idx(counter_ampere_idx + 1) = strom_sprungindex_ampere_idx(counter_ampere_idx - 1) + 2;
            counter_ampere_idx = counter_ampere_idx + 2;
            counter = counter + 2;
        else
            strom_sprungindex_ampere(counter_ampere_idx) = tdf_data.strom(round(stromindex(strom_sprungindex(n))+(end_of_pulse-stromindex(strom_sprungindex(n)))/2));
            if ~isempty(strom_sprungindex_ampere_idx)
                strom_sprungindex_ampere_idx(counter_ampere_idx) = strom_sprungindex_ampere_idx(counter_ampere_idx - 1) + 1;
            else
                strom_sprungindex_ampere_idx(counter_ampere_idx) = n;
            end
            counter_ampere_idx = counter_ampere_idx + 1;
            counter = counter + 1;
        end
    else
        tdf_data.strom(stromindex(strom_sprungindex(n)):end_of_pulse) = 0;
        if stromindex(strom_sprungindex(n))
            tdf_data.spannung(stromindex(strom_sprungindex(n)):end_of_pulse) = ones(size(tdf_data.spannung(stromindex(strom_sprungindex(n)):end_of_pulse))) * tdf_data.spannung(1);
        else
            tdf_data.spannung(stromindex(strom_sprungindex(n)):end_of_pulse) = tdf_data.spannung(stromindex(strom_sprungindex(n))-1) + ((tdf_data.spannung(stromindex(strom_sprungindex(n) + 2))-tdf_data.spannung(stromindex(strom_sprungindex(n))-1))/(tdf_data.zeit(stromindex(strom_sprungindex(n) + 2))-tdf_data.zeit(stromindex(strom_sprungindex(n))-1))) .*(tdf_data.zeit(stromindex(strom_sprungindex(n)):end_of_pulse)-tdf_data.zeit(stromindex(strom_sprungindex(n))-1)) ;
        end
    end    
end
strom_sprungindex_ampere_idx = 1:length(strom_sprungindex_ampere_idx);

IndexWithoutEvaluation = [];
stromindex = 1;
stromindex_counter = 2;
for n = 1:length(strom_sprungindex_ende)
    if abs(tdf_data.zeit(strom_sprungindex_ende(n))-tdf_data.zeit(strom_sprungindex_anfang(n))) > 1
        IndexWithoutEvaluation = [IndexWithoutEvaluation (strom_sprungindex_anfang(n)+1):(strom_sprungindex_ende(n)-1)];
        stromindex(stromindex_counter) = strom_sprungindex_anfang(n);
        stromindex_counter = stromindex_counter + 1;
        stromindex(stromindex_counter) = strom_sprungindex_ende(n);
        stromindex_counter = stromindex_counter + 1;
    end
end
stromindex = sort(stromindex);
if stromindex(end) < numel(tdf_data.strom)
    stromindex(end+1) = numel(tdf_data.strom);
end

%if any parameters are fitted as C rate dependent -> Identify intervals in
%which the C rate is correct and denote the corresponding C rate and
%parameter idx
cd_time_interval = []; %segmentation of the profile contianing the parts for distinct C rates
cd_crate = []; % arry containing the corresponding C rates to cd_time_interval
cd_parameter_idx = []; %inidices mapping the intervals to a parameter idx;
cd_parameter_idx_inverted = [];

%save intervals 
for n = 1:length(strom_sprungindex_ampere)
    if n == 1
        cd_time_interval{n} = [1:stromindex(4)-1];            
    elseif n == length(strom_sprungindex_ampere)
        cd_time_interval{n} = [stromindex(2*n):stromindex(2*n+2)];           
    else
        cd_time_interval{n} = [stromindex(2*n):stromindex(2*n+2)-1];            
    end          
    cd_crate{n} = strom_sprungindex_ampere(n);        
end   

%identify pulses of approx. equal C rate
rounded_crate = round(strom_sprungindex_ampere/DRT_GUI.Testparameter.Cap,1); 
crate_counter = 1;
for n = 1:length(cd_crate)
    if isempty(find(rounded_crate(n) == cd_parameter_idx_inverted))
        cd_parameter_idx(n) = crate_counter;
        cd_parameter_idx_inverted(crate_counter) = rounded_crate(n);
        crate_counter = crate_counter + 1;
    else
        cd_parameter_idx(n) = find(rounded_crate(n) == cd_parameter_idx_inverted);
    end
end
crate_counter = crate_counter - 1;
Crates = sort(cd_parameter_idx_inverted);

end

