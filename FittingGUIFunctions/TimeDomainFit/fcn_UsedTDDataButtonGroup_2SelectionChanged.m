function fcn_UsedTDDataButtonGroup_2SelectionChanged(app, event)
if app.UsedTDDataButtonGroup_2.SelectedObject == app.UsedTDDataButtonGroup_2.Children(1)
    app.UsedTDDataButtonGroup.SelectedObject = app.UsedTDDataButtonGroup.Children(1);  
    set(app.TDF_stromloseMesspunkteCheckbox,'Value',true)
elseif app.UsedTDDataButtonGroup_2.SelectedObject == app.UsedTDDataButtonGroup_2.Children(2)
    app.UsedTDDataButtonGroup.SelectedObject = app.UsedTDDataButtonGroup.Children(2);
    set(app.TDF_stromloseMesspunkteCheckbox,'Value',false)
else
    app.UsedTDDataButtonGroup.SelectedObject = app.UsedTDDataButtonGroup.Children(3);
    set(app.TDF_stromloseMesspunkteCheckbox,'Value',true)
end
if ~isempty(find(contains(app.TDF_ImplementierungsTable.Data(:,2),'CD Re-Fit'),1))
    fcn_TDF_ImplementierungsTable_CellEdit(app,event);
end
fcn_TDF_PlotOCVButton(app,event)
end

