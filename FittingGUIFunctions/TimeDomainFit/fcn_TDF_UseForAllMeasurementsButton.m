function fcn_TDF_UseForAllMeasurementsButton(app,event)
global DRT_GUI
DRT_GUI_orig = DRT_GUI;
try
    fcn_TDF_Change_Implementierung_inFolder(['output/' DRT_GUI.Testparameter.Batterie],DRT_GUI.Testparameter.Batterie,DRT_GUI.Fit.aktuell_Modell.Modellname,app.TDF_Implementierung)
    display('Alle Messungen angepasst!')
catch
    warning('Fehler beim ‹bernehmen der Werte! Datenfile defekt?')
end
DRT_GUI = DRT_GUI_orig;
end