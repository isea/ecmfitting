function fcn_FakeFittingGUI_2(scenario_config, path_management, variations)
%Initialisation of data structures
global DRT_GUI
app = [];
event = [];
app.BatterieNamePopup.Items = [];
app.ZustandPopup.Items = [];
app.TemperaturPopup.Items = [];
app.SOCPopup.Items = [];
app.ModellAuswahlPopup.Items = [];
app.GlobalFitUsedModelDropDown.Items = [];
app.ParamTable.Data = [];
app.Residuum.Text = [];
app.Korrigiert_Punkte_Weg_TextBox.Value = [];
app.PlotElementsCheckbox.Value = 0;
app.InterpolationSlider.Value = [];
app.InterpolationEdit.Value = [];
app.FilterFaktorSlider.Value = [];
app.FilterEdit.Value = [];
app.ZeropaddingSlider.Value = [];
app.ZeropaddingEdit.Value = [];
app.PeakfinderSlider.Value = [];
app.PeakfinderEdit.Value = [];
app.ProzesseSlider.Value = [];
app.ProzesseEdit.Value = [];
app.TauTable.Data = [];
app.TauTable.ColumnName = [{'Relaxation'}, {'time'}, {'Used'}, {'Parameter'}];
app.TDFTab = [];
app.ZarcHNPopup.Value = 'MF-Model';
app.ZarcHNPopup.Items = [{'Zarc'}, {'HN'}, {'RC'}, {'Poröse Elektrode'}, {'MF-Model'}];
app.ProzesseTable.ColumnFormat = [];
app.ProzesseTable.ColumnName = [{'Tau'}, {'Used'}, {'Parameter'}, {'R'}, {'Phi'}];
app.TDF_ImplementierungsTable.ColumnFormat = [];
app.TDF_ImplementierungsTable.ColumnName = [{'name'}, {'OCV'}, {'R_total'}, {'Tau_avg'}, {[]}, {[]}];
app.FDFTab = [];
app.GUI = 0;
app.verification_number_of_simulations = 0;
app.ModeSwitch.Value = 'automatically';

%General Algorithm Parameterization
Models = [{scenario_config.AutoFit.FDF.Model}];
Start_SOCs = num2str(scenario_config.AutoFit.FDF.Start_SOC);
if scenario_config.AutoFit.FDF.Start_temp >= 0
    Start_Temperaturen = [num2str(scenario_config.AutoFit.FDF.Start_temp), 'grad'];
else
    Start_Temperaturen = ['m', num2str(scenario_config.AutoFit.FDF.Start_temp), 'grad'];
end
app.thermalelectricalSimulation = 0;


%GlobalFit parameterization
app.FrequenzbereichsfittingCheckBox.Value = scenario_config.AutoFit.FDF.FDF;
app.ZeitbereichsfittingCheckBox.Value = scenario_config.AutoFit.TDF.TDF;
app.Nachbereitung.Value = scenario_config.AutoFit.FDF.PostProcessing;
app.UseDRTCheckBox.Value = scenario_config.AutoFit.FDF.DRT;
app.PostProcessingTemperature.Value = 0;

app.MADToleranceEditField.Value = scenario_config.AutoFit.FDF.MADTolerance;
app.TauTable.ColumnFormat = [];
app.ProzesseSlider.Limits = [0, 10];
app.Global_Fixed_Config_CheckBox.Value = 0;
app.ContinousStep.Value = 20;
app.PenaltyEditField.Value = 1;
app.constantPhiCheckkBox.Value = 0;
app.NelderMeadalgorithmDownhillSimplexButton.Value = 1;
app.SimulatedAnnealingButton.Value = 0;
app.ParticleSwarmButton.Value = 0;
app.MultistartButton.Value = 0;
app.GeneticAlgorithmButton.Value = 0;
app.CancelButton.Value = 0;

%TDF parameterization
app.ZeitbereichsgrenzextrapolationCheckBox.Value = 0;
app.TDF_stromloseMesspunkteCheckbox.Value = 0;
app.Nachbereitung_TDF.Value = 0;
if strcmp(scenario_config.AutoFit.TDF.Data, 'Relax')
    app.PulsesButton.Value = 0;
    app.RelaxButton.Value = 1;
    app.BothButton.Value = 0;
elseif strcmp(scenario_config.AutoFit.TDF.Data, 'Both')
    app.PulsesButton.Value = 0;
    app.RelaxButton.Value = 0;
    app.BothButton.Value = 1;
else
    app.PulsesButton.Value = 1;
    app.RelaxButton.Value = 0;
    app.BothButton.Value = 0;
end
app.UseOCVCheckBox.Value = 1;
app.UseCustomRangesCheckBox.Value = 1;
app.ZeitFitConfiguration.SelectedTab.Title = 'Permissble Range';
app.CustomRangesTable.Data = [];
app.TDF_KettenleiterGliederEdit.Value = '0';
app.TDF_ParallelOCVcheckbox.Value = 0;
app.TDF_Relaxation_complete_CheckBox.Value = 1;
app.TDF_PauseDchRatioTextbox.Value = '100';
CustomRanges = str2num(scenario_config.AutoFit.TDF.Boundaries);
TDF_Elements = str2num(scenario_config.AutoFit.TDF.Elements);

%Verification parameterization
app.VerificationCheckBox.Value = scenario_config.AutoFit.Verification.Verification;
app.ParallelComputingToolboxinstalledCheckBox.Value = 1;
app.SaveDataCheckBox.Value = 1;
if strcmp(scenario_config.AutoFit.TDF.Data, 'Relax')
    app.PulsesButtonForVerification.Value = 0;
    app.RelaxButtonForVerification.Value = 1;
else
    app.PulsesButtonForVerification.Value = 1;
    app.RelaxButtonForVerification.Value = 0;
end
app.SampleTimeEditField.Value = scenario_config.AutoFit.Verification.SampleRate;

%Export parameterization
app.onButton_temperature_depencendy.Value = 1;
app.offButton_temperature_depencendy.Value = 0;
app.referencetemperatureEditField.Value = -Inf;
app.ImportThermalModelCheckBox.Value = 0;
app.useTDFparametersCheckBox.Value = 1;
[row, ~] = size(variations);

Battery_Name = scenario_config.CellName;
cell_name = scenario_config.CellName;


for m = 1:row
    if scenario_config.AutoFit.ExportToFramework
        app.FrameworkSavePath = path_management.(cell_name).(['ID', variations.VariationID{m}]).('Framework');
        app.PrismaticButton.Value = false;
        app.CylindricalButton.Value = false;
        if strcmp(scenario_config.AutoFit.CellGeometry, "Cylindrical")
            app.CylindricalButton.Value = true;
        end
        if strcmp(scenario_config.AutoFit.CellGeometry, "Prismatic")
            app.PrismaticButton.Value = true;
        end
    end

    app.thermal_model_path = fullfile(path_management.autofit_path, 'utils', 'thermal_model_prismatic.mat');
    app.OutputPath = path_management.(cell_name).(['ID', variations.VariationID{m}]).('FittingGUI').Folder;
    app.VerificationPath = path_management.(cell_name).(['ID', variations.VariationID{m}]).('FittingGUI').Verification;
    app.Temperatures = str2num(scenario_config.Temperature);
    app.SOCs = str2num(scenario_config.SOC);
    for n = 1:length(Models)
        %Specification of starting state

        addpath(genpath(cd))
        config();
        app = fcn_ModellAktualisierenButton(app);
        app = fcn_ModellAuswahlPopup(app);
        app = fcn_BatterieNamePopup(app);
        app.BatterieNamePopup.Value = Battery_Name;
        app = fcn_BatterieNamePopup(app, event);
        app.ZustandPopup.Value = '0C_DC';
        app = fcn_ZustandPopup(app, event);
        app.TemperaturPopup.Value = Start_Temperaturen;
        app = fcn_TemperaturPopup(app, event);
        app.SOCPopup.Value = Start_SOCs;
        app = fcn_SOCPopup(app, event);
        app.ModellAuswahlPopup.Value = Models{n};
        app = fcn_ModellAuswahlPopup(app);


        app = fcn_AktualisierenButtonPushed(app, event);
        app.startingSOCDropDown.Value = Start_SOCs;
        app.startingtemperatureDropDown.Value = Start_Temperaturen;

        app.GlobalFitUsedModelDropDown.Value = Models{1};
        [row2, ~] = size(app.ZeitbereichsfittingParameterAuswahlTable.Data);
        for n2 = 1:length(TDF_Elements)
            if TDF_Elements(n2) <= row2
                app.ZeitbereichsfittingParameterAuswahlTable.Data{TDF_Elements(n2), 3} = 1;
            end
        end
        app = fcn_UseCustomRangesCheckBoxValueChanged(app, event);
        [row2, ~] = size(app.CustomRangesTable.Data);
        [row3, ~] = size(CustomRanges);
        for n2 = 1:row3
            if n2 <= row2
                app.CustomRangesTable.Data{n2, 2} = CustomRanges(n2, 1);
                app.CustomRangesTable.Data{n2, 3} = CustomRanges(n2, 2);
            end
        end
        %Start of GlobalFit
        app = fcn_GlobalFitButtonPushed(app, event);
    end

    if scenario_config.AutoFit.ExportToFramework
        fcn_QuickXMLExportButtonPushed(app, 0);
    end
end
end