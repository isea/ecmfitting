function fcn_LoadIdentifiedDataButtonPushed(app, event)
disp('Start of data processing')
global DRT_GUI;
global Pattern;
files = app.FileReadFiles;
masterpath=app.FileReadMasterFile;
path_idx=strfind(masterpath,'\');
folderpath=masterpath(1:(path_idx(end)));
master = load(masterpath);
if ~isfield(master.diga.daten,'Spannung') && isfield(master.diga.daten,'Span')
    master.diga.daten.Spannung=master.diga.daten.Span;
end
if ~isfield(master.diga.daten,'Strom') && isfield(master.diga.daten,'Current')
    master.diga.daten.Strom=master.diga.daten.Current;
end
if ~isfield(master.diga.daten,'AhAkku') && isfield(master.diga.daten,'AhAkku_0')
    master.diga.daten.AhAkku=master.diga.daten.AhAkku_0;
elseif ~isfield(master.diga.daten,'AhAkku') && isfield(master.diga.daten,'AhStep')
    master.diga.daten.AhAkku=master.diga.daten.AhStep;
end
if ~isfield(master.diga.daten,'Temperatur') && isfield(master.diga.daten,'Temperature')
    master.diga.daten.Temperatur=master.diga.daten.Temperature;
end
%% Masterfile indizieren
idx_InitialCharge_temp=cellfun(@(x) strcmp(x,'InitialCharge'),app.FileReadTable.Data,'UniformOutput',false);
idx_InitialCharge_temp=idx_InitialCharge_temp(:,2);
idx_InitialCharge_temp=find(not(cellfun(@(x) x==0,idx_InitialCharge_temp)));
idx_CapTest_temp=cellfun(@(x) strcmp(x,'Cap-Test'),app.FileReadTable.Data,'UniformOutput',false);
idx_CapTest_temp=idx_CapTest_temp(:,2);
idx_CapTest_temp=find(not(cellfun(@(x) x==0,idx_CapTest_temp)));
idx_EIS_temp=cellfun(@(x) strcmp(x,'EIS'),app.FileReadTable.Data,'UniformOutput',false);
idx_EIS_temp=idx_EIS_temp(:,2);
idx_EIS_temp=find(not(cellfun(@(x) x==0,idx_EIS_temp)));
idx_Pulse_temp=cellfun(@(x) strcmp(x,'Pulse'),app.FileReadTable.Data,'UniformOutput',false);
idx_Pulse_temp=idx_Pulse_temp(:,2);
idx_Pulse_temp=find(not(cellfun(@(x) x==0,idx_Pulse_temp)));
idx_QOCV_temp=cellfun(@(x) strcmp(x,'QOCV'),app.FileReadTable.Data,'UniformOutput',false);
idx_QOCV_temp=idx_QOCV_temp(:,2);
idx_QOCV_temp=find(not(cellfun(@(x) x==0,idx_QOCV_temp)));

if isempty(idx_InitialCharge_temp) && isempty(idx_CapTest_temp) && isempty(idx_EIS_temp) && isempty(idx_Pulse_temp) && isempty(idx_QOCV_temp)
    msgbox('Keine Prozedur zugewiesen!')
    return
end

idx_InitialCharge=[];
if ~isempty(idx_InitialCharge_temp)
    idx_InitialCharge=strcmp(master.diga.daten.Prozedur,app.FileReadTable.Data{idx_InitialCharge_temp,1});
end
idx_CapTest = [];
if ~isempty(idx_CapTest_temp)
    idx_CapTest=strcmp(master.diga.daten.Prozedur,app.FileReadTable.Data{idx_CapTest_temp,1});
else
    msgbox('Keine Kapazittstest-Prozedur gewhlt!')
    return
end
idx_EIS = [];
if ~isempty(idx_EIS_temp)
    idx_EIS=strcmp(master.diga.daten.Prozedur,app.FileReadTable.Data{idx_EIS_temp,1});
else
    msgbox('Keine EIS-Prozedur gewhlt!')
    return
end
idx_Pulse = [];
if ~isempty(idx_Pulse_temp)
    if length(idx_Pulse_temp) == 1
        idx_Pulse=strcmp(master.diga.daten.Prozedur,app.FileReadTable.Data{idx_Pulse_temp,1});
    else
       for n = 1:length(idx_Pulse_temp)
          idx_Pulse{n} =  strcmp(master.diga.daten.Prozedur,app.FileReadTable.Data{idx_Pulse_temp(n),1});
       end
    end
end
idx_QOCV = [];
if ~isempty(idx_QOCV_temp)
    idx_QOCV=strcmp(master.diga.daten.Prozedur,app.FileReadTable.Data{idx_QOCV_temp,1});
end

first_EIS_index = find(idx_EIS);
first_EIS_index = first_EIS_index(1);
idx_CapTest_corrected = find(idx_CapTest);
Last_Indices_Of_Connected_Parts = find(diff(idx_CapTest_corrected)>1);
if ~isempty(Last_Indices_Of_Connected_Parts)
    for last_captest_index = length(Last_Indices_Of_Connected_Parts):-1:1
        if last_captest_index == length(Last_Indices_Of_Connected_Parts)
            if length(idx_CapTest_corrected(Last_Indices_Of_Connected_Parts(last_captest_index)+1:end)) > 1000
                if idx_CapTest_corrected(end) < first_EIS_index
                    idx_CapTest = zeros(1,length(idx_CapTest));
                    idx_CapTest(idx_CapTest_corrected(Last_Indices_Of_Connected_Parts(last_captest_index)+1:end)) = ones(1,length(idx_CapTest(idx_CapTest_corrected(Last_Indices_Of_Connected_Parts(last_captest_index)+1:end))));
                    continue
                end
            end
        else
            if length(idx_CapTest_corrected(Last_Indices_Of_Connected_Parts(last_captest_index)+1:idx_CapTest_corrected(Last_Indices_Of_Connected_Parts(last_captest_index+1)))) > 1000
                if idx_CapTest(idx_CapTest_corrected(Last_Indices_Of_Connected_Parts(last_captest_index+1))) < first_EIS_index
                    idx_CapTest(idx_CapTest_corrected(Last_Indices_Of_Connected_Parts(last_captest_index)+1:idx_CapTest_corrected(Last_Indices_Of_Connected_Parts(last_captest_index+1)))) = ones(1,length((idx_CapTest_corrected(Last_Indices_Of_Connected_Parts(last_captest_index)+1:idx_CapTest_corrected(Last_Indices_Of_Connected_Parts(last_captest_index+1))))));
                    continue
                end
            end
        end
    end
end
%% Plot Indices within voltage curve
% figure;
% g = gca;
% plot(master.diga.daten.Programmdauer,master.diga.daten.Spannung)
% hold on
% legend_string = [];
% legend_string = [legend_string,"'Voltage'"];
% if ~isempty(idx_InitialCharge)
%     plot(master.diga.daten.Programmdauer(idx_InitialCharge),master.diga.daten.Spannung(idx_InitialCharge),'rX')
%     legend_string = [legend_string,"'InitialCharge'"];
% end
% if ~isempty(idx_CapTest)
%     plot(master.diga.daten.Programmdauer(idx_CapTest),master.diga.daten.Spannung(idx_CapTest),'kX')
%     legend_string = [legend_string,"'CapTest'"];
% end
% if ~isempty(idx_EIS)
%     plot(master.diga.daten.Programmdauer(idx_EIS),master.diga.daten.Spannung(idx_EIS),'gX')
%     legend_string = [legend_string,"'EIS'"];
% end
% if ~isempty(idx_Pulse)
%     plot(master.diga.daten.Programmdauer(idx_Pulse),master.diga.daten.Spannung(idx_Pulse),'bX')
%     legend_string = [legend_string,"'Pulse'"];
% end
% if ~isempty(idx_QOCV)
%     plot(master.diga.daten.Programmdauer(idx_QOCV),master.diga.daten.Spannung(idx_QOCV),'yX')
%     legend_string = [legend_string,"'qOCV'"];
% end
% legend(g, legend_string)
% xlabel('t / s')
% ylabel('U / V')
%% Status Indexing
if ~isempty(idx_EIS)
    EIS_Measurement_start_idx=diff(idx_EIS)>0;
    EIS_Measurement_end_idx=diff(idx_EIS)<0;
    EIS_Measurement_start_idx=find(EIS_Measurement_start_idx);
    EIS_Measurement_end_idx=find(EIS_Measurement_end_idx);
end
if ~isempty(idx_Pulse)
    if length(idx_Pulse_temp) == 1
        Pulse_Measurement_start_idx=diff(idx_Pulse)>0;
        Pulse_Measurement_end_idx=diff(idx_Pulse)<0;
        Pulse_Measurement_start_idx=find(Pulse_Measurement_start_idx);
        Pulse_Measurement_end_idx=find(Pulse_Measurement_end_idx);
    else
        for n= 1:length(idx_Pulse_temp)
            Pulse_Measurement_start_idx{n}=diff(idx_Pulse{n})>0;
            Pulse_Measurement_end_idx{n}=diff(idx_Pulse{n})<0;
            Pulse_Measurement_start_idx{n}=find(Pulse_Measurement_start_idx{n});
            Pulse_Measurement_end_idx{n}=find(Pulse_Measurement_end_idx{n});
        end
        count = 0;
        for n = 2:length(idx_Pulse_temp)
           if (Pulse_Measurement_start_idx{n}(1) - Pulse_Measurement_end_idx{n-1}(1) + 1) < 100
               count = count + 1;
           end
        end
        if count == length(idx_Pulse_temp) - 1
           for n = 1:length(Pulse_Measurement_start_idx{1})
               Pulse_Measurement_start_idx_temp(n) = Pulse_Measurement_start_idx{1}(n);
               Pulse_Measurement_end_idx_temp(n) = Pulse_Measurement_end_idx{length(idx_Pulse_temp)}(n);
           end
           Pulse_Measurement_start_idx = Pulse_Measurement_start_idx_temp;
           Pulse_Measurement_end_idx = Pulse_Measurement_end_idx_temp;
        else
            answer = inputdlg('Welche Pulse sollen fr das Fitting genutzt werden? z.B.: 1 oder 2');
            Pulse_Measurement_start_idx = Pulse_Measurement_start_idx{answer{1}};
            Pulse_Measurement_end_idx = Pulse_Measurement_end_idx{answer{1}};
        end        
    end
end
DCH_idx=strcmp(master.diga.daten.Zustand,'DCH');
CHA_idx=strcmp(master.diga.daten.Zustand,'CHA');
PAU_idx=strcmp(master.diga.daten.Zustand,'PAU');
if ~isempty(idx_EIS)
    if ((master.diga.daten.Programmdauer(EIS_Measurement_end_idx(1))-master.diga.daten.Programmdauer(EIS_Measurement_start_idx(1))) > 1e5) || max(master.diga.daten.Programmdauer)>3600*24*200
        master.diga.daten.Programmdauer = master.diga.daten.Programmdauer/1000;
    end
else
    if max(master.diga.daten.Programmdauer)>3600*24*200
        master.diga.daten.Programmdauer = master.diga.daten.Programmdauer/1000;
    end
end
%% Ampere hour counting correction
StepIndex_InitialCharge = 1;
StepIndex = [ 1 1+find(abs(diff(master.diga.daten.Schritt(StepIndex_InitialCharge(1):end))) > 0) numel(master.diga.daten.Schritt(StepIndex_InitialCharge(1):end))+1];
StepIndex(2:end) = StepIndex(2:end) + StepIndex_InitialCharge(1) - 2;
if ~isempty(idx_CapTest)
    CapTest_start_end_idx=find(diff(idx_CapTest>0));
    if length(CapTest_start_end_idx) == 1
        CapTest_start_end_idx(2) = CapTest_start_end_idx;
        CapTest_start_end_idx(1) = 1;
    end
%     AhPrev = master.diga.daten.AhAkku(CapTest_start_end_idx(2));
    AhPrev = 0;
%     figure(2)
%     hold on
    if ~isempty(idx_QOCV)
        QOCV_Start_Index = find(idx_QOCV);
        QOCV_Start_Index = QOCV_Start_Index(1);
        for i_step = 2:(numel(StepIndex)-1)
%             Zero_Idx = find(master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1)) == 0);    
            Zero_Idx = [];     
            if isempty(Zero_Idx)
                %hier ist noch hartgecoded 
                maximum_sensible_charge_transfer = max(abs(master.diga.daten.Strom(StepIndex(i_step):(StepIndex(i_step+1)-1))))*max(diff(master.diga.daten.Programmdauer(StepIndex(i_step):(StepIndex(i_step+1)-1))));
                Zero_Idx = find(abs(diff(master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1))))>3*maximum_sensible_charge_transfer,1,'first')+1;
            end
%             if ~isempty(Zero_Idx) && ( StepIndex(i_step) > CapTest_start_end_idx(2) && (StepIndex(i_step+1)-1)< QOCV_Start_Index) 
            if ~isempty(Zero_Idx) %&& (StepIndex(i_step+1)-1)< QOCV_Start_Index
                if length(Zero_Idx) ~= length(master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1)))
                    if Zero_Idx(1) ~= 1
                        incorrect_AhAkku = AhPrev + master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1))-master.diga.daten.AhAkku(StepIndex(i_step)); 
                        master.diga.daten.AhAkku(StepIndex(i_step):StepIndex(i_step)+Zero_Idx(1)-2) = incorrect_AhAkku(1:Zero_Idx(1)-1);
                        master.diga.daten.AhAkku(StepIndex(i_step)+Zero_Idx(1)-1:(StepIndex(i_step+1)-1)) = incorrect_AhAkku(Zero_Idx(1):end) + (incorrect_AhAkku(Zero_Idx(1)-1) - incorrect_AhAkku(Zero_Idx(1)));
                        AhPrev=master.diga.daten.AhAkku(StepIndex(i_step+1)-1);
%                         plot(master.diga.daten.Programmdauer(1:StepIndex(i_step)+Zero_Idx(1)-2),master.diga.daten.AhAkku(1:StepIndex(i_step)+Zero_Idx(1)-2),'r')
                    else
                        master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1)) = ...
                        AhPrev + master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1))-master.diga.daten.AhAkku(StepIndex(i_step));
                        AhPrev=master.diga.daten.AhAkku(StepIndex(i_step+1)-1);
%                         plot(master.diga.daten.Programmdauer(1:(StepIndex(i_step+1)-1)),master.diga.daten.AhAkku(1:(StepIndex(i_step+1)-1)),'r')
                    end
                else
                     master.diga.daten.AhAkku(StepIndex(i_step)+Zero_Idx(1)-1:(StepIndex(i_step+1)-1)) = AhPrev * ones(1,length(master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1))));
%                      plot(master.diga.daten.Programmdauer(1:(StepIndex(i_step+1)-1)),master.diga.daten.AhAkku(1:(StepIndex(i_step+1)-1)),'r')
                end
            else
                master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1)) = ...
                    AhPrev + master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1))-master.diga.daten.AhAkku(StepIndex(i_step));
                AhPrev=master.diga.daten.AhAkku(StepIndex(i_step+1)-1);
%                 plot(master.diga.daten.Programmdauer(1:(StepIndex(i_step+1)-1)),master.diga.daten.AhAkku(1:(StepIndex(i_step+1)-1)),'r')
            end
        end
    elseif ~isempty(idx_Pulse)
        if length(idx_Pulse_temp)>1
            QOCV_Start_Index = find(idx_Pulse{end},1,'last');
        else
            array = find(idx_Pulse);
            QOCV_Start_Index = array(end);
        end
        for i_step = 2:(numel(StepIndex)-1)
            Zero_Idx = find(master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1))  == 0);           
            if isempty(Zero_Idx)
                maximum_sensible_charge_transfer = max(abs(master.diga.daten.Strom(StepIndex(i_step):(StepIndex(i_step+1)-1))))*max(diff(master.diga.daten.Programmdauer(StepIndex(i_step):(StepIndex(i_step+1)-1))));
                Zero_Idx = find(abs(diff(master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1))))>3*maximum_sensible_charge_transfer,1,'first')+1;
                %Zero_Idx = find(abs(diff(master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1))))>1,1,'first')+1;
            end
            if ~isempty(Zero_Idx)% && (StepIndex(i_step+1)-1)< QOCV_Start_Index
                if length(Zero_Idx) ~= length(master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1)))
                    if Zero_Idx(1) ~= 1
                        incorrect_AhAkku = AhPrev + master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1))-master.diga.daten.AhAkku(StepIndex(i_step)); 
                        master.diga.daten.AhAkku(StepIndex(i_step):StepIndex(i_step)+Zero_Idx(1)-2) = incorrect_AhAkku(1:Zero_Idx(1)-1);
                        master.diga.daten.AhAkku(StepIndex(i_step)+Zero_Idx(1)-1:(StepIndex(i_step+1)-1)) = incorrect_AhAkku(Zero_Idx(1):end) + (incorrect_AhAkku(Zero_Idx(1)-1) - incorrect_AhAkku(Zero_Idx(1)));
                        AhPrev=master.diga.daten.AhAkku(StepIndex(i_step+1)-1);
%                         plot(master.diga.daten.Programmdauer(1:StepIndex(i_step)+Zero_Idx(1)-2),master.diga.daten.AhAkku(1:StepIndex(i_step)+Zero_Idx(1)-2),'r')
                    else
                        master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1)) = ...
                        AhPrev + master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1))-master.diga.daten.AhAkku(StepIndex(i_step));
                        AhPrev=master.diga.daten.AhAkku(StepIndex(i_step+1)-1);
%                         plot(master.diga.daten.Programmdauer(1:(StepIndex(i_step+1)-1)),master.diga.daten.AhAkku(1:(StepIndex(i_step+1)-1)),'r')                        
                    end
                else
                     master.diga.daten.AhAkku(StepIndex(i_step)+Zero_Idx(1)-1:(StepIndex(i_step+1)-1)) = AhPrev * ones(1,length(master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1))));
%                      plot(master.diga.daten.Programmdauer(1:(StepIndex(i_step+1)-1)),master.diga.daten.AhAkku(1:(StepIndex(i_step+1)-1)),'r')
                end
            else
                master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1)) = ...
                    AhPrev + master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1))-master.diga.daten.AhAkku(StepIndex(i_step));
                AhPrev=master.diga.daten.AhAkku(StepIndex(i_step+1)-1);
%                 plot(master.diga.daten.Programmdauer(1:(StepIndex(i_step+1)-1)),master.diga.daten.AhAkku(1:(StepIndex(i_step+1)-1)),'r')
            end
        end
    else
        for i_step = 2:(numel(StepIndex)-1)
            Zero_Idx = find(master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1)) == 0); 
            if isempty(Zero_Idx)
                maximum_sensible_charge_transfer = max(abs(master.diga.daten.Strom(StepIndex(i_step):(StepIndex(i_step+1)-1))))*max(diff(master.diga.daten.Programmdauer(StepIndex(i_step):(StepIndex(i_step+1)-1))));
                Zero_Idx = find(abs(diff(master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1))))>3*maximum_sensible_charge_transfer,1,'first')+1;
               % Zero_Idx = find(abs(diff(master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1))))>1,1,'first')+1;
            end
            if ~isempty(Zero_Idx)
                if length(Zero_Idx) ~= length(master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1)))
                    if Zero_Idx(1) ~= 1
                        incorrect_AhAkku = AhPrev + master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1))-master.diga.daten.AhAkku(StepIndex(i_step)); 
                        master.diga.daten.AhAkku(StepIndex(i_step):StepIndex(i_step)+Zero_Idx(1)-2) = incorrect_AhAkku(1:Zero_Idx(1)-1);
                        master.diga.daten.AhAkku(StepIndex(i_step)+Zero_Idx(1)-1:(StepIndex(i_step+1)-1)) = incorrect_AhAkku(Zero_Idx(1):end) + (incorrect_AhAkku(Zero_Idx(1)-1) - incorrect_AhAkku(Zero_Idx(1)));
                        AhPrev=master.diga.daten.AhAkku(StepIndex(i_step+1)-1);
                    else
                        master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1)) = ...
                        AhPrev + master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1))-master.diga.daten.AhAkku(StepIndex(i_step));
                        AhPrev=master.diga.daten.AhAkku(StepIndex(i_step+1)-1);
        %                         plot(master.diga.daten.Programmdauer(1:(StepIndex(i_step+1)-1)),master.diga.daten.AhAkku(1:(StepIndex(i_step+1)-1)),'r')                        
                    end
                else
                     master.diga.daten.AhAkku(StepIndex(i_step)+Zero_Idx(1)-1:(StepIndex(i_step+1)-1)) = AhPrev * ones(1,length(master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1))));
    %                      plot(master.diga.daten.Programmdauer(1:(StepIndex(i_step+1)-1)),master.diga.daten.AhAkku(1:(StepIndex(i_step+1)-1)),'r')
                end
            else
                 master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1)) = ...
                    AhPrev + master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1))-master.diga.daten.AhAkku(StepIndex(i_step));
                AhPrev=master.diga.daten.AhAkku(StepIndex(i_step+1)-1);
%                      plot(master.diga.daten.Programmdauer(1:(StepIndex(i_step+1)-1)),master.diga.daten.AhAkku(1:(StepIndex(i_step+1)-1)),'r')
            end
        end
    end
    %%%%%%%%%%%%%%%%%%% Debugging Start
    %plot(master.diga.daten.Programmdauer,master.diga.daten.AhAkku)
    %[~,a] = min(abs(master.diga.daten.Programmdauer -
    %TimeInstanceOfJumpingAhAkku))
    %master.diga.daten.AhAkku(a-10,a+10) -> find index of jump
    %master.diga.daten.AhAkku(Idx:end) = master.diga.daten.AhAkku(Idx:end)- master.diga.daten.AhAkku(Idx) + master.diga.daten.AhAkku(Idx-1);
    %%%%%%%%%%%%%%%%%%% Debugging End
    Cap_DCH_idx=find(diff(DCH_idx>0));
    Cap_DCH_start_idx=Cap_DCH_idx(find(Cap_DCH_idx>=CapTest_start_end_idx(1),1,'first'));
    Cap_DCH_end_idx=Cap_DCH_idx(find(Cap_DCH_idx<=CapTest_start_end_idx(2),1,'last'));
    master.diga.daten.AhAkku=master.diga.daten.AhAkku - min(master.diga.daten.AhAkku(Cap_DCH_start_idx:Cap_DCH_end_idx));
    Kapazitaet = max(master.diga.daten.AhAkku(Cap_DCH_start_idx:Cap_DCH_end_idx))-min(master.diga.daten.AhAkku(Cap_DCH_start_idx:Cap_DCH_end_idx));
%     master.diga.daten.AhAkku=master.diga.daten.AhAkku+Kapazitaet-master.diga.daten.AhAkku(CapTest_start_end_idx(2));    
    set(app.KapTextBox,'Value',num2str(Kapazitaet));
    fcn_KapTextBox(app, event);
end
disp('End of data processing')
%% Working with each EIS-File
disp('Start of DRT_GUI creation')
SOC_array = [];
Voltage_array = [];
for i = 1:numel(files)
    filename = files{i};
    DRT_GUI = [];
    DRT_GUI.Testparameter.fileName = strcat(folderpath,filename);
    set(app.PunkteWegnehmenTextBox,'Value','');
    set(app.ModellFormelTextBox,'Value','');
    messung=load(strcat(folderpath,filename));
    % Datum
    datefound = 0;
    [DString DStringOrig] = FindDateInStr(filename);
    if ~isempty(DString)
        datefound = 1;
        set(app.DatumTextBox,'Value',datestr(datenum(DString)))
        fcn_DatumTextBox(app,event);
    end
    if ~datefound
        while true
            DString = cell2mat(inputdlg('Bitte Testdatum angeben! (yyyy-mm-dd)','Datum',[1],{datestr(now,'yyyy-mm-dd')}));
            if  isempty(DString) , return,end
            if isdate(DString)
                set(app.DatumTextBox,'Value',datestr(datenum(DString,'yymmdd')))
                datefound = 1;
                break,
            end
        end
    end
    % Batteriename
    Batteriefound = 0;
    for i1 = 1:numel(Pattern.Batterie)
        if ~isempty(Pattern.Batterie{i1})
            [k,l] = regexp(filename,Pattern.Batterie{i1});
            if isempty(k) && Pattern.Batterie{i1}(1)=='_'
                [k,l] = regexp(filename(1:numel(Pattern.Batterie{i1})),Pattern.Batterie{i1}(2:end));
            end
            if k > 0
                set(app.BatterieTextBox,'Value',filename(k:l));
                fcn_BatterieTextBox(app, event);
                Batteriefound = 1;
                break
            end
        end
    end
    if Batteriefound==0;
        while true
            Batteriename= inputdlg('Wie heit die Batterie?','Batteriename',[1],{'dummy'});
            if  isempty(Batteriename) ||    strcmp(Batteriename,'') ,
                return
            else
                Batteriename=Batteriename{1};
                break
            end
        end
        set(app.BatterieTextBox,'Value',Batteriename);
        fcn_BatterieTextBox(app, event);
        Batteriefound = 1;
    end
    Zustandfound = 0;
    Zustand = [];
    DRT_GUI.Testparameter.Zustand=Zustand;
    % Temperatur
    if ~isempty(idx_EIS)
        Temperatur=master.diga.daten.Temperatur(EIS_Measurement_end_idx(1):EIS_Measurement_end_idx(end));
        Temperatur=median(Temperatur);
        Temperatur=round(Temperatur*2)/2;
        set(app.TemperaturTextBox,'Value',num2str(Temperatur))
        fcn_TemperaturTextBox(app, event);
        Temperaturfound = 1;
    end
    % EIS number + EIS data
    if sum(ismember(fieldnames(messung),'diga'))
        if sum(~diff(messung.diga.daten.ActFreq)~=0)
            index = find(messung.diga.daten.ActFreq>0 & [1, diff(messung.diga.daten.ActFreq)~=0]);
        else
            index = 1:numel(messung.diga.daten.ActFreq);
        end
        [k,l] = regexp(filename,'EIS\d\d\d\d\d');
        if ~isempty(k)
            eisnumber = str2double(filename(k+3:l));
        else
            eisnumber=0;
        end
        ind2 = find(diff(messung.diga.daten.ActFreq(index))>0);
%         if ~isempty(ind2)
%             ind2 = [1 ind2+1 numel(index)+1];
%             if eisnumber>0 && eisnumber < numel(ind2)
%                 index = index(ind2(eisnumber):(ind2(eisnumber+1)-1));
%             else
%                 eisnumber = inputdlg(['Which spectrum should be imported? ( 1 to 'num2str(numel(ind2)-1)')'],'number of spectrum',1,{'1'});
%                 if isempty(eisnumber), return , end
%                 eisnumber = str2num(cell2mat(eisnumber));
%                 index = index(ind2(eisnumber):(ind2(eisnumber+1)-1));
%             end
%         end
        DRT_GUI.Messdaten.frequenz = messung.diga.daten.ActFreq(index)';
        DRT_GUI.Messdaten.omega = 2*pi*messung.diga.daten.ActFreq(index)';
        DRT_GUI.Messdaten.tau = 1./DRT_GUI.Messdaten.omega;
        DRT_GUI.Messdaten.Zreal = messung.diga.daten.Zreal1(index)';
        DRT_GUI.Messdaten.Zimg = messung.diga.daten.Zimg1(index)';
        if ~isempty(find(strcmp(fieldnames(DRT_GUI.Messdaten),'U1'), 1))
            DRT_GUI.Messdaten.U1 = messung.diga.daten.U1(index)';
        end
    else
        error('Dateiformat nicht bekannt!')
        return
    end
    %% Working with Master File
    EIS_start_idx=EIS_Measurement_start_idx(eisnumber);
    EIS_end_idx=EIS_Measurement_end_idx(eisnumber);
    if ~isempty(idx_Pulse)
        Pulse_start_idx=Pulse_Measurement_start_idx(find(Pulse_Measurement_start_idx>EIS_start_idx,1,'first'));
        if eisnumber < length(EIS_Measurement_start_idx)
            Pulse_end_idx=Pulse_Measurement_end_idx(find(Pulse_Measurement_end_idx<EIS_Measurement_start_idx(eisnumber+1),1,'last'));
        else
            Pulse_end_idx=Pulse_Measurement_end_idx(end);
        end
    end
    if eisnumber == 1
        EIS_vor_start_idx=1;
    else
        EIS_vor_start_idx=EIS_Measurement_start_idx(eisnumber-1);
    end
    
    EIS_CHA_before_idx=find(CHA_idx(EIS_vor_start_idx:EIS_start_idx)==1,1,'last')+EIS_vor_start_idx;
    EIS_DCH_before_idx=find(DCH_idx(EIS_vor_start_idx:EIS_start_idx)==1,1,'last')+EIS_vor_start_idx;
    Relax_start_idx=max(EIS_CHA_before_idx,EIS_DCH_before_idx);
    Relax_PAU_idx=find(PAU_idx(EIS_vor_start_idx:Relax_start_idx-1)==1,1,'last')+EIS_vor_start_idx;
    
    relax_time = master.diga.daten.Programmdauer(Relax_PAU_idx:EIS_start_idx)-master.diga.daten.Programmdauer(Relax_PAU_idx);
    relax_spannung = master.diga.daten.Spannung(Relax_PAU_idx:EIS_start_idx);
    relax_strom = master.diga.daten.Strom(Relax_PAU_idx:EIS_start_idx);    
    
    DRT_GUI.Messdaten.relax.zeit = relax_time;
    DRT_GUI.Messdaten.relax.spannung = relax_spannung;
    DRT_GUI.Messdaten.relax.strom = relax_strom;
    
    eis_time = master.diga.daten.Programmdauer(EIS_start_idx:EIS_end_idx)-master.diga.daten.Programmdauer(EIS_start_idx);
    eis_spannung = master.diga.daten.Spannung(EIS_start_idx:EIS_end_idx);
    eis_strom = master.diga.daten.Strom(EIS_start_idx:EIS_end_idx);
    
    DRT_GUI.Messdaten.eis.zeit = eis_time;
    DRT_GUI.Messdaten.eis.spannung = eis_spannung;
    DRT_GUI.Messdaten.eis.strom = eis_strom;
    
    pulse_time=[];
    if ~isempty(idx_Pulse)
        pulse_time = master.diga.daten.Programmdauer(Pulse_start_idx:Pulse_end_idx)-master.diga.daten.Programmdauer(Pulse_start_idx);
        pulse_spannung = master.diga.daten.Spannung(Pulse_start_idx:Pulse_end_idx);
        pulse_strom = master.diga.daten.Strom(Pulse_start_idx:Pulse_end_idx);

        DRT_GUI.Messdaten.pulse.zeit = pulse_time;
        DRT_GUI.Messdaten.pulse.spannung = pulse_spannung;
        DRT_GUI.Messdaten.pulse.strom = pulse_strom;
    end
    
    if isempty(pulse_time)
        vor_relax_time = master.diga.daten.Programmdauer(EIS_vor_start_idx:Relax_PAU_idx)-master.diga.daten.Programmdauer(EIS_vor_start_idx);
        vor_relax_spannung = master.diga.daten.Spannung(EIS_vor_start_idx:Relax_PAU_idx);
        vor_relax_strom = master.diga.daten.Strom(EIS_vor_start_idx:Relax_PAU_idx);

        DRT_GUI.Messdaten.vor_relax.zeit = vor_relax_time;
        DRT_GUI.Messdaten.vor_relax.spannung = vor_relax_spannung;
        DRT_GUI.Messdaten.vor_relax.strom = vor_relax_strom;
    else
        DRT_GUI.Messdaten.vor_relax.zeit = [];
        DRT_GUI.Messdaten.vor_relax.spannung = [];
        DRT_GUI.Messdaten.vor_relax.strom = [];
    end
    DRT_GUI.Testparameter.Cap=Kapazitaet;
    
    AhAkku_werte = master.diga.daten.AhAkku(EIS_Measurement_start_idx);
%     SOC=round(AhAkku_werte(eisnumber)/Kapazitaet*100*10)/10;
    SOC=round(AhAkku_werte(eisnumber)/Kapazitaet*100,2);
    SOC_array = [SOC_array, SOC];
    if ~isempty(idx_Pulse)
        Voltage_array = [Voltage_array, DRT_GUI.Messdaten.pulse.spannung(1)];
    else
        if ~isempty(DRT_GUI.Messdaten.relax.spannung)
            Voltage_array = [Voltage_array, DRT_GUI.Messdaten.relax.spannung(end)];
        else
            Voltage_array = [Voltage_array, master.diga.daten.Spannung(EIS_start_idx-1)];
        end
    end
    SOCfound = 1;
    set(app.SOCTextBox,'Value',num2str(SOC));
    fcn_SOCTextBox(app,event);
    
    DC_Current = sum(diff(DRT_GUI.Messdaten.eis.zeit).*DRT_GUI.Messdaten.eis.strom(2:end))./sum(diff(DRT_GUI.Messdaten.eis.zeit));
    DRT_GUI.Testparameter.AhSteps=zeros(1,numel(AhAkku_werte));
    
    if ~SOCfound
        while true
            SOC = str2double(inputdlg('SOC','SOC',[1],{'25'}));
            choice = num2str(SOC);
            if  strcmp(choice,'') , return,end
            if ~strcmp(choice,'NaN'), break, end;
        end
        set(app.SOCTextBox,'Value',choice)
        fcn_SOCTextBox(app,event);
        SOCfound = 1;
    end
    if ~Batteriefound
        while true
            BatterieName = inputdlg('BatterieName','Name',[1],{'Dummy'});
            choice = BatterieName;
            if  isempty(choice) ||strcmp(choice,'') , return,end
            if ~strcmp(choice,'NaN'), break, end;
        end
        set(app.BatterieTextBox,'Value',choice);
        fcn_BatterieTextBox(app, event);
        Batteriefound = 1;
    end
    DRT_GUI.Messdaten.DC_Overlay = DC_Current;
    
%     clear messung
    
    if ~isempty(find(strcmp(fieldnames(DRT_GUI.Messdaten),'DC_Overlay'),1)) && ~isempty(find(strcmp(fieldnames(DRT_GUI.Testparameter),'Cap'),1))
        if ~isempty(DRT_GUI.Testparameter.Zustand)
            DRT_GUI.Testparameter.Zustand = [DRT_GUI.Testparameter.Zustand '_' ];
        end
        Rounded_Current = round(4*DRT_GUI.Messdaten.DC_Overlay./DRT_GUI.Testparameter.Cap)/4;
        if Rounded_Current == 0 && DRT_GUI.Messdaten.DC_Overlay > 0.01
            Rounded_Current_Str= 'p0C_DC';
        elseif Rounded_Current == 0 && DRT_GUI.Messdaten.DC_Overlay < -0.01
            Rounded_Current_Str= 'm0C_DC';
        else
            Rounded_Current_Str= [strrep(num2str(Rounded_Current),'-','m') 'C_DC'];
        end
        DRT_GUI.Testparameter.Zustand = [DRT_GUI.Testparameter.Zustand Rounded_Current_Str];
    end
    set(app.ZustandTextBox,'Value',DRT_GUI.Testparameter.Zustand);
    fcn_ZustandTextBox(app, event);

    set(app.fminTextBox,'Value',num2str(min(DRT_GUI.Messdaten.frequenz)))
    set(app.fmaxTextBox,'Value',num2str(max(DRT_GUI.Messdaten.frequenz)))
    set(app.MessPunkteTextBox,'Value',num2str(numel(DRT_GUI.Messdaten.frequenz)))
    
    if mean(DRT_GUI.Messdaten.Zreal)>0.1 && strcmp(filename(end-3:end),'.mat')
        DRT_GUI.Messdaten.Zreal = DRT_GUI.Messdaten.Zreal/1000;
        DRT_GUI.Messdaten.Zimg = DRT_GUI.Messdaten.Zimg/1000;
    end
    DRT_GUI.Messdaten.Z = DRT_GUI.Messdaten.Zreal + 1i .* DRT_GUI.Messdaten.Zimg;
    if  isrow(DRT_GUI.Messdaten.Zreal)
        DRT_GUI.Messdaten.Zreal = transpose(DRT_GUI.Messdaten.Zreal);
    end
    if  isrow(DRT_GUI.Messdaten.Zimg)
        DRT_GUI.Messdaten.Zimg = transpose(DRT_GUI.Messdaten.Zimg);
    end
    if  isrow(DRT_GUI.Messdaten.frequenz)
        DRT_GUI.Messdaten.frequenz = transpose(DRT_GUI.Messdaten.frequenz);
    end
    if  isrow(DRT_GUI.Messdaten.omega)
        DRT_GUI.Messdaten.omega = transpose(DRT_GUI.Messdaten.omega);
    end
    if  isrow(DRT_GUI.Messdaten.tau)
        DRT_GUI.Messdaten.tau = transpose(DRT_GUI.Messdaten.tau);
    end
    if  isrow(DRT_GUI.Messdaten.Z)
        DRT_GUI.Messdaten.Z = transpose(DRT_GUI.Messdaten.Z);
    end
    Typ = 'Nyquist';
    hold(app.axes1,'off');
    fcn_plot_Auswaehl(app,DRT_GUI.Messdaten,[],[],DRT_GUI.Testparameter.Batterie,Typ);
    
    %initial fit
    fcn_ModellAktualisierenButton(app,event);
    fcn_DRT_Config_reload_Button(app, event);
    set(app.PunkteWegnehmenTextBox,'Value','')
    fcn_PunkteWegnehmeButton(app, event);
    fcn_PasteButton(app,event);
    set(app.GueltigeMessungCheck,'Value',1)
    fcn_GueltigeMessungCheck(app, event);
    fcn_FitButtonFunction(app, event);
    %fcn_FitButton_Callback(app, event)
    cla(app.axes3)
    if isfield(messung.diga,'NoEIS')
        load('FakeDRTData.mat')
        DRT_GUI.DRT = FakeDRTData;
    else
        fcn_DRT_Button(app, event);
    end
    clear messung
    fcn_CopyButton(app,event);
    if isempty(cell2mat(strfind(fieldnames(DRT_GUI.Testparameter),'Zustand'))) || isempty(DRT_GUI.Testparameter.Zustand)
        DRT_GUI.Testparameter.Zustand = 'default';
        set(app.ZustandTextBox,'Value','default')
    end
    choice = 'Ja';
    pfad = ['output' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Zustand '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad/' ...
        DRT_GUI.Testparameter.Batterie '_' DRT_GUI.Testparameter.Zustand '_' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad_' [strrep(num2str(DRT_GUI.Testparameter.SOC),'-','m')] 'SOC.mat'];
    
    if iscell(pfad) , pfad = cell2mat(pfad);end
    if ~isempty(dir(pfad))
        choice = questdlg('Die Datei existiert bereits. Soll sie berschrieben werden?', ...
            'berschreiben', ...
            'Ja','Nein','Nein');
    end
    if strcmp(choice,'Ja')
        fcn_SpeichernButton(app,event);
    end
    addpath(['output' '/' DRT_GUI.Testparameter.Batterie])
    disp(['Creation finished for ' num2str(DRT_GUI.Testparameter.Temperatur) '°C and ' num2str(DRT_GUI.Testparameter.SOC) '% SOC'])
end
disp('End of DRT_GUI creation')
%% Extract qOCV
if ~isempty(idx_QOCV)
    disp('Start of qOCV creation')
    idx_CHA =  contains(master.diga.daten.Zustand(idx_QOCV), 'CHA');
    idx_DCH =  contains(master.diga.daten.Zustand(idx_QOCV), 'DCH');
    OCV_Spannung = master.diga.daten.Spannung(idx_QOCV);    
    OCV_Akku = master.diga.daten.AhAkku(idx_QOCV);    
%   OCV_Akku = OCV_Akku - min(OCV_Akku);
    %create file structure for OCV
    if ~isempty(find(idx_CHA))
        OCV_Spannung_CHA = OCV_Spannung(find(idx_CHA));
        OCV_SOC_CHA = OCV_Akku(find(idx_CHA))./Kapazitaet;
        OCV_array{1,1} = QOCV;
        OCV_array{1,2} = Temperatur;
        OCV_array{1,3} = 'CH';
        OCV_array{1,1}.SOC = OCV_SOC_CHA*100;
        OCV_array{1,1}.voltage = OCV_Spannung_CHA;
    end
    if ~isempty(find(idx_DCH))
        OCV_Spannung_DCH = OCV_Spannung(find(idx_DCH));
        OCV_SOC_DCH = OCV_Akku(find(idx_DCH))./Kapazitaet;
        OCV_array{2,1} = QOCV;
        OCV_array{2,2} = Temperatur;
        OCV_array{2,3} = 'DCH';
        OCV_array{2,1}.SOC = fliplr(OCV_SOC_DCH)*100;
        OCV_array{2,1}.voltage = fliplr(OCV_Spannung_DCH);
    end
    %adjust for hysteresis errors
    OCV_file = [];
    min_SOC = min(OCV_array{1,1}.SOC(1),OCV_array{2,1}.SOC(1));
    max_SOC = max(OCV_array{1,1}.SOC(end),OCV_array{2,1}.SOC(end));
    SOC = min_SOC:0.01:max_SOC;
    
    qOCV_voltage_A = zeros(1,length(SOC));                
    [inputSOC,indices] = unique(OCV_array{1, 1}.SOC,'stable');
    voltage = OCV_array{1, 1}.voltage(indices);
    qOCV_voltage_A(1,:) = interp1(inputSOC,voltage,SOC,'spline');

    qOCV_voltage_B = zeros(1,length(SOC));                
    [inputSOC,indices] = unique(OCV_array{2, 1}.SOC  ,'stable');
    voltage = OCV_array{2, 1}.voltage(indices);
    qOCV_voltage_B(1,:) = interp1(inputSOC,voltage,SOC,'spline');

    qOCV_voltage = (qOCV_voltage_A+qOCV_voltage_B)./2;
    OCV_file{1,1} = QOCV;
    OCV_file{1,1}.SOC = SOC;
    OCV_file{1,1}.voltage = qOCV_voltage;
    OCV_file{1,2} = Temperatur;
    
    middle_SOC_index = floor(length(SOC_array)/2);
    [~,OCV_index_of_middle_SOC] = min(abs(SOC - SOC_array(middle_SOC_index)));
    OCV_file{1,1}.voltage = OCV_file{1,1}.voltage - (OCV_file{1,1}.voltage(OCV_index_of_middle_SOC)   - Voltage_array(middle_SOC_index));
    OCV_array = OCV_file;
    
    pfad = ['output' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Zustand '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad'];
    current_path = cd;
    cd(pfad)
    filename = [DRT_GUI.Testparameter.Batterie '_' strrep(num2str(round(DRT_GUI.Testparameter.Temperatur)),'-','m') 'grad_' 'qOCV-File'];
    filename = strrep(filename,'.','_');
    save(filename,'OCV_array');
    cd(current_path)
    disp('End of qOCV creation')
end
end
