function app = fcn_InitialFitforPCMDataButtonPushed(app, event)
FolderPath = uigetdir('Please select the Output-Folder that has to be checked!');
old_path = cd;
cd(FolderPath)
zustand_list = dir;
for zustand_iterator = 3:length(zustand_list)
    cd([FolderPath '\' zustand_list(zustand_iterator).name])
    temperature_list = [];
    temperature_list = dir;
    % Iterate over all temperature folders in corresponding state folder
    for temperature_iterator = 3:length(temperature_list)
        cd([FolderPath '\' zustand_list(zustand_iterator).name '\' temperature_list(temperature_iterator).name])
        SOC_list = [];
        SOC_list = dir('*SOC.mat');
        %Iterate over all SOCs
        for SOC_iterator = 1:length(SOC_list)
            DRT_GUI = [];
            load(SOC_list(SOC_iterator).name)
            cd(old_path)
            app = fcn_ModellAktualisierenButton(app,event);
            app = fcn_DRT_Config_Reload_Button(app, event);
            if app.GUI
                set(app.PunkteWegnehmenTextBox,'Value','')
            else
                app.PunkteWegnehmenTextBox.Value = '';
            end
            app = fcn_PunkteWegnehmenButton(app, event);
            %fcn_PasteButton_Callback(app,event)
            if app.GUI
                set(app.GueltigeMessungCheck,'Value',1)
            else
                app.PunkteWegnehmenTextBox.Value = 1;
            end            
            app = fcn_GueltigeMessungCheck(app, event);
            app = fcn_ModellAktualisierenButton(app,event);
            app = fcn_ModellAktualisierenButton(app,event);
            app = fcn_FitButtonFunction(app, event);
            if app.GUI
                cla(app.axes3)
            end
            app = fcn_DRTButton(app, event);
            app = fcn_CopyButton(app,event);
            if isempty(cell2mat(strfind(fieldnames(DRT_GUI.Testparameter),'Zustand'))) || isempty(DRT_GUI.Testparameter.Zustand)
                DRT_GUI.Testparameter.Zustand = 'default';
                if app.GUI
                    set(app.ZustandTextBox,'Value','default')
                else
                    app.ZustandTextBox.Value = 'default';
                end
            end
            pfad = ['output' '/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Zustand '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad/' ...
                DRT_GUI.Testparameter.Batterie '_' DRT_GUI.Testparameter.Zustand '_' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad_' [strrep(num2str(DRT_GUI.Testparameter.SOC),'-','m')] 'SOC.mat'];
            if iscell(pfad)
                pfad = cell2mat(pfad);
            end
            app = fcn_SpeichernButton(app,event);
        end
    end
end
disp('End of initial fitting')
end


