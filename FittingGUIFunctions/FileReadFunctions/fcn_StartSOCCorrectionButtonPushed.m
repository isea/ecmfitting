function app = fcn_StartSOCCorrectionButtonPushed(app, event)
global DRT_GUI
if app.CRateFlag && app.HysteresisFlag
    StateString = [DRT_GUI.Testparameter.Zustand_2 '_' DRT_GUI.Testparameter.Zustand];
elseif app.CRateFlag
    StateString = DRT_GUI.Testparameter.Zustand;
else
    StateString = DRT_GUI.Testparameter.Zustand_2;
end
%safe temperature and SOC to return at the end
original_temp=app.startingtemperatureDropDown.Value;
original_SOC=app.startingSOCDropDown.Value;

%sort temperatures
temperature_array=zeros(1,length(app.startingtemperatureDropDown.Items)-1);
for sort_var=1:length(app.startingtemperatureDropDown.Items)-1
    if contains(app.startingtemperatureDropDown.Items{sort_var+1},'m')
        temp_str=strcat('-',app.startingtemperatureDropDown.Items{sort_var+1}(2:end-4));
    else
        temp_str=app.startingtemperatureDropDown.Items{sort_var+1}(1:end-4);
    end
    temperature_array(sort_var)=str2num(temp_str);
end
[sorted_temperature_array,temperature_index_rearrangement_array]=sort(temperature_array);

%determine reference capacity
if isempty(find(sorted_temperature_array == app.referencetemperatureSOCCorrectionEditField.Value,1))
    msgbox('error: temperature was not found in the list of temperatures')
    return
end
index1=temperature_index_rearrangement_array(find(sorted_temperature_array == app.referencetemperatureSOCCorrectionEditField.Value));
app.TemperaturPopup.Value=app.TemperaturPopup.Items{1+index1};
app = fcn_TemperaturPopup(app, event);
app.SOCPopup.Value = app.SOCPopup.Items{2};
app = fcn_SOCPopup(app, event);
reference_capacity = DRT_GUI.Testparameter.Cap;

% start of new data set generation
if isempty(dir('output'))
    mkdir('output')
end
if isempty( dir(['output' '/' [DRT_GUI.Testparameter.Batterie, '_refSOC']]))
    mkdir(['output' '/' [DRT_GUI.Testparameter.Batterie, '_refSOC']])
end
if isempty( dir(['output' '/' [DRT_GUI.Testparameter.Batterie, '_refSOC'] '/' app.StateString]))
    mkdir(['output' '/' [DRT_GUI.Testparameter.Batterie, '_refSOC'] '/' app.StateString])
end
for temp_idx=1:length(sorted_temperature_array)
    % SOC arrays
    index1=temperature_index_rearrangement_array(temp_idx);
    app.TemperaturPopup.Value=app.TemperaturPopup.Items{1+index1};
    app = fcn_TemperaturPopup(app, event);
    SOC_array{temp_idx}=zeros(1,length(app.SOCPopup.Items)-1);
    for sort_var=1:length(app.SOCPopup.Items)-1
        if contains(app.SOCPopup.Items{sort_var+1},'m')
            SOC_str=strcat('-',app.SOCPopup.Items{sort_var+1}(2:end));
        else
            SOC_str=app.SOCPopup.Items{sort_var+1}(1:end);
        end
        SOC_array{temp_idx}(sort_var)=str2num(SOC_str);
    end
    [sorted_SOC_array{temp_idx},SOC_index_rearrangement_array{temp_idx}]=sort(SOC_array{temp_idx});
    
    %iterate over all SOCs and create new battery
    for soc_idx = 1:length(sorted_SOC_array{temp_idx})
        %Lade richtige Daten bei Wechsel des SOC
        index2=SOC_index_rearrangement_array{temp_idx}(soc_idx);
        if~strcmp(app.SOCPopup.Value,app.SOCPopup.Items{1+index2})
            app.SOCPopup.Value=app.SOCPopup.Items{1+index2};
            app = fcn_SOCPopup(app, event);
        end
        if (sorted_temperature_array(temp_idx) ~= app.referencetemperatureSOCCorrectionEditField.Value)
            Ah_discharged_capacity = DRT_GUI.Testparameter.Cap * (100 - DRT_GUI.Testparameter.SOC)/100;
            SOC_ref_capacity = ((reference_capacity - Ah_discharged_capacity)/reference_capacity) * 100;
            DRT_GUI.Testparameter.SOC = SOC_ref_capacity;
            DRT_GUI.Testparameter.Cap = reference_capacity;
        end
        if isempty( dir(['output' '/' [DRT_GUI.Testparameter.Batterie, '_refSOC'] '/' app.StateString '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad']))
            mkdir(['output' '/' [DRT_GUI.Testparameter.Batterie, '_refSOC'] '/' app.StateString '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad'])
        end
        save(['output' '/' [DRT_GUI.Testparameter.Batterie, '_refSOC'] '/' app.StateString '/' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad/' ...
            [DRT_GUI.Testparameter.Batterie, '_refSOC'] '_' StateString '_' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad_' [strrep(num2str(DRT_GUI.Testparameter.SOC),'-','m')] 'SOC.mat'],'DRT_GUI');
    end
end
addpath(['output' '/' [DRT_GUI.Testparameter.Batterie, '_refSOC']])
%return to original SOC and temperature
app.TemperaturPopup.Value=original_temp;
app = fcn_TemperaturPopup(app, event);
app.SOCPopup.Value = original_SOC;
app = fcn_SOCPopup(app, event);
end