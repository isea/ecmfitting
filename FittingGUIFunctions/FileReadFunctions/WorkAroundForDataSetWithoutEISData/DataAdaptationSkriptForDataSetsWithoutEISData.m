%Name der Pulsprozedur

%load master file
[file,path] = uigetfile();
master = load([path,file]);

%load dummy EIS_set
load('Dummy_EIS_DataSet.mat');

% Read all data points for a list of all used procedures
array_idx = 1;
procedure_array(1)={'A'};
for procedure_idx = 1:length(master.diga.daten.Prozedur)
    if ~ismember(master.diga.daten.Prozedur(procedure_idx),procedure_array)
        procedure_array(array_idx,1)=master.diga.daten.Prozedur(procedure_idx);
        procedure_array(array_idx,2)={'Choose'};
        array_idx=array_idx+1;
    end
end
    
%create figure to ask for precedure-functionality matching by the user
fig = uifigure;
fig.HandleVisibility = 'on';
lbl1 = uilabel(fig,'Text','Please select the appropriate functionalities !','InnerPosition',[30,330,300,22],'FontSize',14);
lbl2 = uilabel(fig,'Text',['To continue program' newline 'execution press any' newline 'button in the' newline 'command window'],'InnerPosition',[350,100,150,70],'FontSize',14);
uit = uitable(fig,'Data',procedure_array,'ColumnFormat',{[] {' ','InitialCharge','Cap-Test','EIS','Pulse','QOCV'}});
% btn = uibutton(fig,'Text','Selection finished','InnerPosition',[350,100,150,22],'ButtonPushedFcn', @(btn,event) SaveSelectionPushed(btn));
uit.ColumnEditable = [false,true];
pause
uit_Data = uit.Data;
close all
%find all indices of the Pulse profile
idx_Pulse_temp=cellfun(@(x) strcmp(x,'Pulse'),uit_Data,'UniformOutput',false);
idx_Pulse_temp=idx_Pulse_temp(:,2);
idx_Pulse_temp=find(not(cellfun(@(x) x==0,idx_Pulse_temp)));
idx_Pulse = [];
if ~isempty(idx_Pulse_temp)
    idx_Pulse=strcmp(master.diga.daten.Prozedur,uit_Data{idx_Pulse_temp,1});
else
    msgbox('No puls procedure chosen!')
    return
end

%find starts and ends of puls profiles
if ~isempty(idx_Pulse)
    Pulse_Measurement_start_idx=diff(idx_Pulse)>0;
    Pulse_Measurement_end_idx=diff(idx_Pulse)<0;
    Pulse_Measurement_start_idx=find(Pulse_Measurement_start_idx);
    Pulse_Measurement_end_idx=find(Pulse_Measurement_end_idx);
end

%create appropriate name string for Dummy EIS files
[DString DStringOrig] = FindDateInStr(file);
if isempty(DString)
    msgbox('Kein Datum gefunden')
end
date_start_idx = strfind(file,DStringOrig);
file_name_finish_idx = strfind(file(date_start_idx:end),'=');
file_name_finish_idx = file_name_finish_idx(1);
EIS_filename_start = [file(1:date_start_idx + file_name_finish_idx - 1) , 'Dummy_EIS_process_name='];
EIS_filename_end =   ' Format01=EISkanal 05-2.mat';
old_path = cd(path);

%Create files
for n = 1:length(Pulse_Measurement_start_idx)
    EIS_filename = [];
    if n<10
        EIS_filename_middle_part = ['EIS0000' ,num2str(n)];
    else
        EIS_filename_middle_part = ['EIS000' ,num2str(n)];
    end
    EIS_filename = [EIS_filename_start , EIS_filename_middle_part , EIS_filename_end];
    
    master.diga.daten.Prozedur{Pulse_Measurement_start_idx(n)-1} = 'EIS-Dummy';
    master.diga.daten.Prozedur{Pulse_Measurement_start_idx(n)-2} = 'EIS-Dummy';
    
    save(EIS_filename,'diga')
end
diga = master.diga;
save(file,'diga')
cd(old_path)

