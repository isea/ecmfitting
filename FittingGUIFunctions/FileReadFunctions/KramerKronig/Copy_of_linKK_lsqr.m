function app = linKK(app)
%Function to control the validity of Kramers Kronig relation similar to:
%https://www.iam.kit.edu/et/Lin-KK.php
%https://github.com/ECSHackWeek/impedance.py

%set adjustable parameters from GUI interface
app.LinKKLamp.Color = 'red';
pause(1)
if app.ComplexButton.Value
    mode = 'complex';
    use_real = 1;
    use_imag = 1;
elseif app.ImaginaryButton.Value
    mode = 'imag';
    use_real = 0;
    use_imag = 1;
else
    mode = 'real';
    use_real = 1;
    use_imag = 0;
end
Max_RC_count = app.MaxNumberofRCElementsEditField.Value;
overfitting_criterion = app.OverfittingCriterionEditField.Value;

%initialize data structures
global DRT_GUI
formula_prev = [];
formula = [];
Tau = [];
Tau_prev = [];
m_w = DRT_GUI.Messdaten.omega;
m_real = DRT_GUI.Messdaten.Zreal;
m_imag = DRT_GUI.Messdaten.Zimg;
index=find(-m_imag>0);
index=find(min(-m_imag(index)))+index(1)-1;
if index == 1
    Rser_Init= m_real(1);
else
    Rser_Init=m_real(index-1)+(m_real(index)-m_real(index-1))*(m_imag(index-1))/(-m_imag(index)+m_imag(index-1));
end
m_f = DRT_GUI.Messdaten.frequenz(find(DRT_GUI.Messdaten.Zimg<=0));
m_real = DRT_GUI.Messdaten.Zreal(find(DRT_GUI.Messdaten.Zimg<=0));
m_imag = DRT_GUI.Messdaten.Zimg(find(DRT_GUI.Messdaten.Zimg<=0));
m_w = DRT_GUI.Messdaten.omega(find(DRT_GUI.Messdaten.Zimg<=0));
m_Z = DRT_GUI.Messdaten.Z(find(DRT_GUI.Messdaten.Zimg<=0));
% interpolated_real = min(m_real):(max(m_real)-min(m_real))/30:max(m_real);
% interpolated_imag = interp1(m_real,m_imag,min(m_real):(max(m_real)-min(m_real))/30:max(m_real),'spline');
% interpolated_w = interp1(m_real,m_w,min(m_real):(max(m_real)-min(m_real))/30:max(m_real),'spline');
% m_w = transpose(interpolated_w);
% m_real = transpose(interpolated_real);
% m_imag = transpose(interpolated_imag);
% m_Z = m_real + 1i * m_imag;
% m_f = m_w/(2*pi);
% index=find(-m_imag>0);
% index=find(min(-m_imag(index)))+index(1)-1;
% if index == 1
%     Rser_Init= m_real(1);
% else
%     Rser_Init=m_real(index-1)+(m_real(index)-m_real(index-1))*(m_imag(index-1))/(-m_imag(index)+m_imag(index-1));
% end
% Rser_Init = 0;
number_of_added_ec_elements = 1;
if app.onCapacitanceButton.Value
    number_of_added_ec_elements = number_of_added_ec_elements + 1;
end
if app.onInductorButton.Value
    number_of_added_ec_elements = number_of_added_ec_elements + 1;
end

%fit an increasing number of RC Elements to spectrum until weight of negative
%resistors is 1 - overfitting_criterion
for RC_count = 1:Max_RC_count
    %determine logarithmically distributed time constants and build
    %coefficient matrix and right side vector
    coefficient_matrix = cell(1,10);
    coefficient_matrix{1} = zeros(length(m_Z),number_of_added_ec_elements+RC_count);
    coefficient_matrix{1}(1:length(m_real),1) = use_real*1;
    coefficient_matrix{1}(length(m_real)+1:length(m_real)+length(m_imag),1) = 0;
    formula = 'p(1)';
    if app.onCapacitanceButton.Value
        coefficient_matrix{1}(1:length(m_real),2) = use_real*0;
        coefficient_matrix{1}(length(m_real)+1:length(m_real)+length(m_imag),2) = use_imag*(-1./(m_w));
        formula = [formula, '+p(2)./(1i.*w)'];
    end
    if app.onInductorButton.Value
        if app.onCapacitanceButton.Value
            coefficient_matrix{1}(1:length(m_real),3) = use_real*0;
            coefficient_matrix{1}(length(m_real)+1:length(m_real)+length(m_imag),3) = use_imag*(m_w);
            formula = [formula, '+1i.*w.*p(3)'];
        else
            coefficient_matrix{1}(1:length(m_real),2) = use_real*0;
            coefficient_matrix{1}(length(m_real)+1:length(m_real)+length(m_imag),2) = use_imag*(m_w);
            formula = [formula, '+1i.*w.*p(2)'];
        end
    end
    Tau = zeros(1,RC_count);
    if RC_count == 1
        Tau(1) = 1/max(DRT_GUI.Messdaten.omega);
        coefficient_matrix{1}(1:length(m_real),number_of_added_ec_elements+1) = use_real*1./(1+(m_w.*Tau(1)).^2);
        coefficient_matrix{1}(length(m_real)+1:length(m_real)+length(m_imag),number_of_added_ec_elements+1) = use_imag*(-1.*m_w.*Tau(1))./(1+(m_w.*Tau(1)).^2);
        formula = [formula, '+p(',num2str(1+number_of_added_ec_elements),')./(1+1i.*w.*' , num2str(Tau(1)) ,')'];
    elseif RC_count == 2
        Tau(1) = 1/max(DRT_GUI.Messdaten.omega);
        Tau(2) = 1/min(DRT_GUI.Messdaten.omega);
        coefficient_matrix{1}(1:length(m_real),number_of_added_ec_elements+1) = use_real*1./(1+(m_w.*Tau(1)).^2);
        coefficient_matrix{1}(length(m_real)+1:length(m_real)+length(m_imag),number_of_added_ec_elements+1) = use_imag*(-1.*m_w.*Tau(1))./(1+(m_w.*Tau(1)).^2);
        coefficient_matrix{1}(1:length(m_real),number_of_added_ec_elements+2) = use_real*1./(1+(m_w.*Tau(2)).^2);
        coefficient_matrix{1}(length(m_real)+1:length(m_real)+length(m_imag),number_of_added_ec_elements+2) = use_imag*(-1.*m_w.*Tau(2))./(1+(m_w.*Tau(2)).^2);
        formula = [formula, '+p(',num2str(1+number_of_added_ec_elements),')./(1+1i.*w.*' , num2str(Tau(1)) ,')'];
        formula = [formula, '+p(',num2str(2+number_of_added_ec_elements),')./(1+1i.*w.*' , num2str(Tau(2)) ,')'];
    else
        Tau(1) = 1/max(DRT_GUI.Messdaten.omega);
        coefficient_matrix{1}(1:length(m_real),number_of_added_ec_elements+1) = use_real*1./(1+(m_w.*Tau(1)).^2);
        coefficient_matrix{1}(length(m_real)+1:length(m_real)+length(m_imag),number_of_added_ec_elements+1) = use_imag*(-1.*m_w.*Tau(1))./(1+(m_w.*Tau(1)).^2);
        formula = [formula, '+p(',num2str(1+number_of_added_ec_elements),')./(1+1i.*w.*' , num2str(Tau(1)) ,')'];
        for n = 2:RC_count-1
            Tau(n) = 10^(log10(1/max(DRT_GUI.Messdaten.omega))+ ((n - 1)/(RC_count - 1)) * log10((1/min(DRT_GUI.Messdaten.omega))/(1/max(DRT_GUI.Messdaten.omega))));
            coefficient_matrix{1}(1:length(m_real),number_of_added_ec_elements+n) = use_real*1./(1+(m_w.*Tau(n)).^2);
            coefficient_matrix{1}(length(m_real)+1:length(m_real)+length(m_imag),number_of_added_ec_elements+n) = use_imag*(-1.*m_w.*Tau(n))./(1+(m_w.*Tau(n)).^2);
            formula = [formula, '+p(' , num2str(n+number_of_added_ec_elements) , ')./(1+1i.*w.*' , num2str(Tau(n)) ,')'];
        end
        Tau(RC_count) = 1/min(DRT_GUI.Messdaten.omega);
        coefficient_matrix{1}(1:length(m_real),number_of_added_ec_elements+RC_count) = use_real*1./(1+(m_w.*Tau(RC_count)).^2);
        coefficient_matrix{1}(length(m_real)+1:length(m_real)+length(m_imag),number_of_added_ec_elements+RC_count) = use_imag*(-1.*m_w.*Tau(RC_count))./(1+(m_w.*Tau(RC_count)).^2);
        formula = [formula, '+p(' , num2str(RC_count+number_of_added_ec_elements) , ')./(1+1i.*w.*' , num2str(Tau(RC_count)) ,')'];
    end
    right_side_vector = [use_real*m_real;use_imag*m_imag];
    
    random_vectors = rand(9,RC_count);
    C_bool = app.onCapacitanceButton.Value;
    L_bool = app.onInductorButton.Value;
    for m = 2:10
        coefficient_matrix{m} = zeros(length(m_Z),number_of_added_ec_elements+RC_count);
        coefficient_matrix{m}(1:length(m_real),1) = use_real*1;
        coefficient_matrix{m}(length(m_real)+1:length(m_real)+length(m_imag),1) = 0;
        if C_bool
            coefficient_matrix{m}(1:length(m_real),2) = use_real*0;
            coefficient_matrix{m}(length(m_real)+1:length(m_real)+length(m_imag),2) = use_imag*(-1./(m_w));
        end
        if L_bool
            if C_bool 
                coefficient_matrix{m}(1:length(m_real),3) = use_real*0;
                coefficient_matrix{m}(length(m_real)+1:length(m_real)+length(m_imag),3) = use_imag*(m_w);
            else
                coefficient_matrix{m}(1:length(m_real),2) = use_real*0;
                coefficient_matrix{m}(length(m_real)+1:length(m_real)+length(m_imag),2) = use_imag*(m_w);
            end
        end
        
        Tau = 10^(log10(1/max(DRT_GUI.Messdaten.omega))+ log10((1/min(DRT_GUI.Messdaten.omega))/(1/max(DRT_GUI.Messdaten.omega)))) .* sort(random_vectors(m-1,:));
        if RC_count == 1
            coefficient_matrix{m}(1:length(m_real),number_of_added_ec_elements+1) = use_real*1./(1+(m_w.*Tau(1)).^2);
            coefficient_matrix{m}(length(m_real)+1:length(m_real)+length(m_imag),number_of_added_ec_elements+1) = use_imag*(-1.*m_w.*Tau(1))./(1+(m_w.*Tau(1)).^2);
        elseif RC_count == 2
            coefficient_matrix{m}(1:length(m_real),number_of_added_ec_elements+1) = use_real*1./(1+(m_w.*Tau(1)).^2);
            coefficient_matrix{m}(length(m_real)+1:length(m_real)+length(m_imag),number_of_added_ec_elements+1) = use_imag*(-1.*m_w.*Tau(1))./(1+(m_w.*Tau(1)).^2);
            coefficient_matrix{m}(1:length(m_real),number_of_added_ec_elements+2) = use_real*1./(1+(m_w.*Tau(2)).^2);
            coefficient_matrix{m}(length(m_real)+1:length(m_real)+length(m_imag),number_of_added_ec_elements+2) = use_imag*(-1.*m_w.*Tau(2))./(1+(m_w.*Tau(2)).^2);
         else
            coefficient_matrix{m}(1:length(m_real),number_of_added_ec_elements+1) = use_real*1./(1+(m_w.*Tau(1)).^2);
            coefficient_matrix{m}(length(m_real)+1:length(m_real)+length(m_imag),number_of_added_ec_elements+1) = use_imag*(-1.*m_w.*Tau(1))./(1+(m_w.*Tau(1)).^2);
            for n = 2:RC_count-1
                coefficient_matrix{m}(1:length(m_real),number_of_added_ec_elements+n) = use_real*1./(1+(m_w.*Tau(n)).^2);
                coefficient_matrix{m}(length(m_real)+1:length(m_real)+length(m_imag),number_of_added_ec_elements+n) = use_imag*(-1.*m_w.*Tau(n))./(1+(m_w.*Tau(n)).^2);
            end
            coefficient_matrix{m}(1:length(m_real),number_of_added_ec_elements+RC_count) = use_real*1./(1+(m_w.*Tau(RC_count)).^2);
            coefficient_matrix{m}(length(m_real)+1:length(m_real)+length(m_imag),number_of_added_ec_elements+RC_count) = use_imag*(-1.*m_w.*Tau(RC_count))./(1+(m_w.*Tau(RC_count)).^2);
        end
    end
    
    %specify initial values and boundaries
    p_init = 1e-3 * ones(RC_count+number_of_added_ec_elements,1);
    p_init(1) = Rser_Init;
    for n = number_of_added_ec_elements+1:length(p_init)
        p_init(n) = (max(m_real)+min(m_real))/2;
    end
    p_best = zeros(10,RC_count + number_of_added_ec_elements);
    relres_best = zeros(10,1);
    %fit model
    for n = 1:10
        options = optimset('MaxIter',2000000,'MaxFunEvals',2000000,'TolX',1e-10,'TolFun',1e-10,'Display','off');
        [p,flag,relres,iter,resvec] = lsqr(coefficient_matrix{n},right_side_vector,1e-10,2000000,[],[],p_init);
        %         p = coefficient_matrix\right_side_vector;
        p_best(n,:) = p;
        relres_best(n) = relres;
    end
    %Check if border to overfitting is reached
    [~,min_idx] = min(relres_best);
    p_opt = p_best(min_idx,:);
    
    negative_R_index = find(p_opt(1 + number_of_added_ec_elements:end)<0);
    positive_R_index = find(p_opt(1 + number_of_added_ec_elements:end)>=0);
    criterion = 1 - (sum(abs(p_opt(negative_R_index + number_of_added_ec_elements)))/sum(abs(p(positive_R_index + number_of_added_ec_elements))));
    if criterion <= overfitting_criterion
        break
    end
    p_prev = p_opt;
    Tau_prev = Tau;
    formula_prev = formula;
    coefficient_matrix_prev = coefficient_matrix;
end
p = p_prev;
Tau = Tau_prev;
formula = formula_prev;
coefficient_matrix = coefficient_matrix_prev;
app.linkk_formula = formula;
app.linkk_parameter_values = p;
set(app.resultingnumberofRCelementsEditField,'Value',RC_count - 1);
%plot in GUI
w = m_w;
f_real = real(eval(formula));
f_imag = imag(eval(formula));
FarbenLaden()

%Nyquist
cla(app.LinKKZUIAxes)
if strcmp(mode,'complex')
    app.LinKKZUIAxes.YLim=[floor(min(m_imag*100000))/100000, ceil(max(m_imag*100000))/100000];
    %app.LinKKZUIAxes.XLim = [floor(min(m_real*100000))/100000 (floor(min(m_real*100000))/10000+app.LinKKZUIAxes.YLim(2)-app.LinKKZUIAxes.YLim(1))];
    app.LinKKZUIAxes.XLimMode='auto';
    plot(app.LinKKZUIAxes,m_real,m_imag,'o','DisplayName','Measurement','color',RWTHBlau,'LineWidth',1,'MarkerSize',7);hold(app.LinKKZUIAxes,'on');
    plot(app.LinKKZUIAxes,f_real,f_imag,'x','DisplayName','Fit','color',RWTHRot,'LineWidth',1,'MarkerSize',7);
    grid(app.LinKKZUIAxes,'on'); hold(app.LinKKZUIAxes,'on'); set(app.LinKKZUIAxes,'ydir', 'reverse'); %axis(app.axes1,'equal');
    app.LinKKZUIAxes.XLabel.Interpreter='latex'; app.LinKKZUIAxes.YLabel.Interpreter='latex'; app.LinKKZUIAxes.XLabel.String='$\Re\{\underline Z\}$ in $\Omega$'; app.LinKKZUIAxes.YLabel.String='$\Im\{\underline Z\}$ in $\Omega$';
    h1 = legend(app.LinKKZUIAxes,'Measurement','Fit','Location','NorthWest');
    set(h1,'Interpreter','none');
end
%Bode
%Real part over frequency
cla(app.LinKKRealUIAxes)
if strcmp(mode,'complex') || strcmp(mode,'real')
    semilogx(app.LinKKRealUIAxes,m_f,real(m_Z),'o','color',RWTHBlau,'DisplayName','Measurement','LineWidth',1,'MarkerSize',7);hold(app.LinKKRealUIAxes,'on');
    semilogx(app.LinKKRealUIAxes,m_f,f_real,'o','color',RWTHRot,'DisplayName','Fit','LineWidth',1,'MarkerSize',7);
    set(app.LinKKRealUIAxes,'xdir','reverse'),grid(app.LinKKRealUIAxes,'on'), hold(app.LinKKRealUIAxes,'on');
    set(app.LinKKRealUIAxes,'TickLabelInterpreter','latex')
    h1 = legend(app.LinKKRealUIAxes,'show','Location','NorthWest');
    set(h1,'Interpreter','latex');
    ylabel(app.LinKKRealUIAxes,'$\Re\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
    xlabel(app.LinKKRealUIAxes,'$f$ in Hz','Interpreter','latex');
end

%Imaginary part over frequency
cla(app.LinKKImagUIAxes)
if strcmp(mode,'complex') || strcmp(mode,'imag')
    semilogx(app.LinKKImagUIAxes,m_f,imag(m_Z),'o','color',RWTHBlau,'DisplayName','Measurement','LineWidth',1,'MarkerSize',7);hold(app.LinKKImagUIAxes,'on');
    semilogx(app.LinKKImagUIAxes,m_f,f_imag,'o','color',RWTHRot,'DisplayName','Fit','LineWidth',1,'MarkerSize',7);
    set(app.LinKKImagUIAxes,'xdir','reverse'),grid(app.LinKKImagUIAxes,'on'), hold(app.LinKKImagUIAxes,'on');
    set(app.LinKKImagUIAxes,'TickLabelInterpreter','latex')
    h1 = legend(app.LinKKRealUIAxes,'show','Location','NorthWest');
    set(h1,'Interpreter','latex');
    ylabel(app.LinKKImagUIAxes,'$\Im\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
    xlabel(app.LinKKImagUIAxes,'$f$ in Hz','Interpreter','latex');
    set(app.LinKKImagUIAxes,'ydir', 'reverse');
end

%Residuals part over frequency
cla(app.LinKKResidualUIAxes)
delta_real = ((real(m_Z)-f_real)./abs(f_real+1i*f_imag))*100;
delta_imag = ((imag(m_Z)-f_imag)./abs(f_real+1i*f_imag))*100;
if strcmp(mode,'complex') || strcmp(mode,'real')
    semilogx(app.LinKKResidualUIAxes,m_f,delta_real,'o','color',RWTHBlau,'DisplayName','Real residual','LineWidth',1,'MarkerSize',7);hold(app.LinKKResidualUIAxes,'on');
end
if strcmp(mode,'complex') || strcmp(mode,'imag')
    semilogx(app.LinKKResidualUIAxes,m_f,delta_imag,'o','color',RWTHRot,'DisplayName','Imag residual','LineWidth',1,'MarkerSize',7);
end
set(app.LinKKResidualUIAxes,'xdir','reverse'),grid(app.LinKKResidualUIAxes,'on'); hold(app.LinKKResidualUIAxes,'on');
set(app.LinKKResidualUIAxes,'TickLabelInterpreter','latex')
h1 = legend(app.LinKKResidualUIAxes,'show','Location','NorthWest');
set(h1,'Interpreter','latex');
ylabel(app.LinKKResidualUIAxes,'Residuals in %','Interpreter','latex');
xlabel(app.LinKKResidualUIAxes,'$f$ in Hz','Interpreter','latex');

app.LinKKLamp.Color = 'green';
end