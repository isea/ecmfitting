function app = plot_linkk(app)
if strcmpi(app.BatterieNamePopup.Value,'empty')
    warning('no cell in output folder')
    return
end

%initialize data structures
global DRT_GUI
FarbenLaden()
m_f = DRT_GUI.Messdaten.frequenz(find(DRT_GUI.Messdaten.Zimg<=0));
m_real = DRT_GUI.Messdaten.Zreal(find(DRT_GUI.Messdaten.Zimg<=0));
m_imag = DRT_GUI.Messdaten.Zimg(find(DRT_GUI.Messdaten.Zimg<=0));
m_w = DRT_GUI.Messdaten.omega(find(DRT_GUI.Messdaten.Zimg<=0));
m_Z = DRT_GUI.Messdaten.Z(find(DRT_GUI.Messdaten.Zimg<=0));

%Nyquist
cla(app.LinKKZUIAxes)
app.LinKKZUIAxes.YLim=[floor(min(m_imag*100000))/100000, ceil(max(m_imag*100000))/100000];
%app.LinKKZUIAxes.XLim = [floor(min(m_real*100000))/100000 (floor(min(m_real*100000))/10000+app.LinKKZUIAxes.YLim(2)-app.LinKKZUIAxes.YLim(1))];
app.LinKKZUIAxes.XLimMode='auto';
plot(app.LinKKZUIAxes,m_real,m_imag,'o','DisplayName','Measurement','color',RWTHBlau,'LineWidth',1,'MarkerSize',7);hold(app.LinKKZUIAxes,'on');
grid(app.LinKKZUIAxes,'on'); hold(app.LinKKZUIAxes,'on'); set(app.LinKKZUIAxes,'ydir', 'reverse'); %axis(app.axes1,'equal');
app.LinKKZUIAxes.XLabel.Interpreter='latex'; app.LinKKZUIAxes.YLabel.Interpreter='latex'; app.LinKKZUIAxes.XLabel.String='$\Re\{\underline Z\}$ in $\Omega$'; app.LinKKZUIAxes.YLabel.String='$\Im\{\underline Z\}$ in $\Omega$';
h1 = legend(app.LinKKZUIAxes,'show','Location','NorthWest');
set(h1,'Interpreter','none');

%Bode
%Real part over frequency
cla(app.LinKKRealUIAxes)
semilogx(app.LinKKRealUIAxes,m_f,real(m_Z),'o','color',RWTHBlau,'DisplayName','Measurement','LineWidth',1,'MarkerSize',7);hold(app.LinKKRealUIAxes,'on');
set(app.LinKKRealUIAxes,'xdir','reverse'),grid(app.LinKKRealUIAxes,'on'), hold(app.LinKKRealUIAxes,'on');
set(app.LinKKRealUIAxes,'TickLabelInterpreter','latex')
h1 = legend(app.LinKKRealUIAxes,'show','Location','NorthWest');
set(h1,'Interpreter','latex');
ylabel(app.LinKKRealUIAxes,'$\Re\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
xlabel(app.LinKKRealUIAxes,'$f$ in Hz','Interpreter','latex');


%Imaginary part over frequency
cla(app.LinKKImagUIAxes)
semilogx(app.LinKKImagUIAxes,m_f,imag(m_Z),'o','color',RWTHBlau,'DisplayName','Measurement','LineWidth',1,'MarkerSize',7);hold(app.LinKKImagUIAxes,'on');
set(app.LinKKImagUIAxes,'xdir','reverse'),grid(app.LinKKImagUIAxes,'on'), hold(app.LinKKImagUIAxes,'on');
set(app.LinKKImagUIAxes,'TickLabelInterpreter','latex')
h1 = legend(app.LinKKRealUIAxes,'show','Location','NorthWest');
set(h1,'Interpreter','latex');
ylabel(app.LinKKImagUIAxes,'$\Im\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
xlabel(app.LinKKImagUIAxes,'$f$ in Hz','Interpreter','latex');
set(app.LinKKImagUIAxes,'ydir', 'reverse');


%Residuals part over frequency
cla(app.LinKKResidualUIAxes)
ylabel(app.LinKKResidualUIAxes,'Residuals in %','Interpreter','latex');
xlabel(app.LinKKResidualUIAxes,'$f$ in Hz','Interpreter','latex');

set(app.resultingnumberofRCelementsEditField,'Value',0);
end
