function [app,f_real,f_imag] = linKK(app)
%Function to control the validity of Kramers Kronig relation similar to:
%https://www.iam.kit.edu/et/Lin-KK.php
%https://github.com/ECSHackWeek/impedance.py

%set adjustable parameters from GUI interface
app.LinKKLamp.Color = 'red';
pause(1)
if app.ComplexButton.Value
    mode = 'complex';
elseif app.ImaginaryButton.Value
    mode = 'imag';
else
    mode = 'real';
end
Max_RC_count = app.MaxNumberofRCElementsEditField.Value;
overfitting_criterion = app.OverfittingCriterionEditField.Value;

%initialize data structures
global DRT_GUI
formula_prev = [];
formula = [];
Tau = [];
Tau_prev = [];
m_w = DRT_GUI.Messdaten.omega;
m_real = DRT_GUI.Messdaten.Zreal;
m_imag = DRT_GUI.Messdaten.Zimg;
index=find(-m_imag>0);
index=find(min(-m_imag(index)))+index(1)-1;
if index == 1
    Rser_Init= m_real(1);
else
    Rser_Init=m_real(index-1)+(m_real(index)-m_real(index-1))*(m_imag(index-1))/(-m_imag(index)+m_imag(index-1));
end
m_f = DRT_GUI.Messdaten.frequenz(find(DRT_GUI.Messdaten.Zimg<=0));
m_real = DRT_GUI.Messdaten.Zreal(find(DRT_GUI.Messdaten.Zimg<=0));
m_imag = DRT_GUI.Messdaten.Zimg(find(DRT_GUI.Messdaten.Zimg<=0));
m_w = DRT_GUI.Messdaten.omega(find(DRT_GUI.Messdaten.Zimg<=0));
m_Z = DRT_GUI.Messdaten.Z(find(DRT_GUI.Messdaten.Zimg<=0));
% interpolated_real = min(m_real):(max(m_real)-min(m_real))/30:max(m_real);
% interpolated_imag = interp1(m_real,m_imag,min(m_real):(max(m_real)-min(m_real))/30:max(m_real),'spline');
% interpolated_w = interp1(m_real,m_w,min(m_real):(max(m_real)-min(m_real))/30:max(m_real),'spline');
% m_w = transpose(interpolated_w);
% m_real = transpose(interpolated_real);
% m_imag = transpose(interpolated_imag);
% m_Z = m_real + 1i * m_imag;
% m_f = m_w/(2*pi);
% index=find(-m_imag>0);
% index=find(min(-m_imag(index)))+index(1)-1;
% if index == 1
%     Rser_Init= m_real(1);
% else
%     Rser_Init=m_real(index-1)+(m_real(index)-m_real(index-1))*(m_imag(index-1))/(-m_imag(index)+m_imag(index-1));
% end
% Rser_Init = 0;
number_of_added_ec_elements = 1;
if app.onCapacitanceButton.Value
    number_of_added_ec_elements = number_of_added_ec_elements + 1;
end
if app.onInductorButton.Value
    number_of_added_ec_elements = number_of_added_ec_elements + 1;
end

%fit an increasing number of RC Elements to spectrum until weight of negative
%resistors is 1 - overfitting_criterion 
for RC_count = 1:Max_RC_count
    %determine logarithmically distributed time constants and build model
    %formula
    formula = 'p(1)';
    if app.onCapacitanceButton.Value
%         formula = [formula, '+1./(1i.*w.*p(2))'];
        formula = [formula, '+p(2)./(1i.*w)'];
    end
    if app.onInductorButton.Value
        if app.onCapacitanceButton.Value
            formula = [formula, '+1i.*w.*p(3)'];
        else
            formula = [formula, '+1i.*w.*p(2)'];
        end
    end
    Tau = zeros(1,RC_count);
    if RC_count == 1
        Tau(1) = 1/max(DRT_GUI.Messdaten.omega);
        formula = [formula, '+p(',num2str(1+number_of_added_ec_elements),')./(1+1i.*w.*' , num2str(Tau(1)) ,')'];
    elseif RC_count == 2
        Tau(1) = 1/max(DRT_GUI.Messdaten.omega);
        formula = [formula, '+p(',num2str(1+number_of_added_ec_elements),')./(1+1i.*w.*' , num2str(Tau(1)) ,')'];
        Tau(2) = 1/min(DRT_GUI.Messdaten.omega);
        formula = [formula, '+p(',num2str(2+number_of_added_ec_elements),')./(1+1i.*w.*' , num2str(Tau(2)) ,')'];
    else
        Tau(1) = 1/max(DRT_GUI.Messdaten.omega);
        formula = [formula, '+p(',num2str(1+number_of_added_ec_elements),')./(1+1i.*w.*' , num2str(Tau(1)) ,')'];        
        for n = 2:RC_count-1          
            Tau(n) = 10^(log10(1/max(DRT_GUI.Messdaten.omega))+ ((n - 1)/(RC_count - 1)) * log10((1/min(DRT_GUI.Messdaten.omega))/(1/max(DRT_GUI.Messdaten.omega))));
            formula = [formula, '+p(' , num2str(n+number_of_added_ec_elements) , ')./(1+1i.*w.*' , num2str(Tau(n)) ,')'];
        end
        Tau(RC_count) = 1/min(DRT_GUI.Messdaten.omega);
        formula = [formula, '+p(' , num2str(RC_count+number_of_added_ec_elements) , ')./(1+1i.*w.*' , num2str(Tau(RC_count)) ,')'];
    end
    
    %specify initial values and boundaries
    p_min = -Inf * ones(1,RC_count+number_of_added_ec_elements);
    p_max = Inf * ones(1,RC_count+number_of_added_ec_elements);
    p_init = 1e-3 * ones(1,RC_count+number_of_added_ec_elements);
    p_init(1) = Rser_Init;
    p_min(1) = 0.99 * Rser_Init;
    p_max(1) = 1.01 * Rser_Init;
    if app.onCapacitanceButton.Value
        p_min(2) = 0;
    end
    if app.onInductorButton.Value
        if app.onCapacitanceButton.Value
            p_min(3) = 0;
        else
            p_min(2) = 0;
        end
    end
    for n = number_of_added_ec_elements+1:length(p_init)
        p_init(n) = (max(m_real)+min(m_real))/2;
    end
    
    %fit model
    options = optimset('MaxIter',2000000,'MaxFunEvals',2000000,'TolX',1e-10,'TolFun',1e-10,'Display','off');
%     if strcmp(mode,'complex')
%         [p,fval,~,~]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min, p_max,options,formula,'GloSea');
%     elseif strcmp(mode,'imag')
%         [p,fval,~,~]=function_fit_easyfit2(app,m_w,m_imag,p_init,@function_model_all_types2_imag, p_min, p_max,options,formula,'GloSea');        
%     else
%         [p,fval,~,~]=function_fit_easyfit2(app,m_w,m_real,p_init,@function_model_all_types2_real, p_min, p_max,options,formula,'GloSea');
%     end
    if strcmp(mode,'complex')
        [p1,fval1,~,~]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min, p_max,options,formula,[]);
        [p2,fval2,~,~]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min, p_max,options,formula,'PaSw');
        [p3,fval3,~,~]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min, p_max,options,formula,'GenAlg');
        [p4,fval4,~,~]=function_fit_easyfit2(app,m_w,[m_real, m_imag],p_init,@function_model_all_types2, p_min, p_max,options,formula,'GloSea');
    elseif strcmp(mode,'imag')
        [p1,fval1,~,~]=function_fit_easyfit2(app,m_w,m_imag,p_init,@function_model_all_types2_imag, p_min, p_max,options,formula,[]);
        [p2,fval2,~,~]=function_fit_easyfit2(app,m_w,m_imag,p_init,@function_model_all_types2_imag, p_min, p_max,options,formula,'PaSw');
        [p3,fval3,~,~]=function_fit_easyfit2(app,m_w,m_imag,p_init,@function_model_all_types2_imag, p_min, p_max,options,formula,'GenAlg');
        [p4,fval4,~,~]=function_fit_easyfit2(app,m_w,m_imag,p_init,@function_model_all_types2_imag, p_min, p_max,options,formula,'GloSea');
    else
        [p1,fval1,~,~]=function_fit_easyfit2(app,m_w,m_real,p_init,@function_model_all_types2_real, p_min, p_max,options,formula,[]);
        [p2,fval2,~,~]=function_fit_easyfit2(app,m_w,m_real,p_init,@function_model_all_types2_real, p_min, p_max,options,formula,'PaSw');
        [p3,fval3,~,~]=function_fit_easyfit2(app,m_w,m_real,p_init,@function_model_all_types2_real, p_min, p_max,options,formula,'GenAlg');
        [p4,fval4,~,~]=function_fit_easyfit2(app,m_w,m_real,p_init,@function_model_all_types2_real, p_min, p_max,options,formula,'GloSea');
    end
    [~,best_fit_idx] = min([fval1,fval2,fval3,fval4]);
    p = eval(['p',num2str(best_fit_idx)]);
    
    %Check if border to overfitting is reached
    negative_R_index = find(p(1 + number_of_added_ec_elements:end)<0);
    positive_R_index = find(p(1 + number_of_added_ec_elements:end)>=0);
    criterion = 1 - (sum(abs(p(negative_R_index + number_of_added_ec_elements)))/sum(abs(p(positive_R_index + number_of_added_ec_elements))));
    if criterion <= overfitting_criterion
        break
    end
    p_prev = p;
    Tau_prev = Tau;
    formula_prev = formula;
end
p = p_prev;
Tau = Tau_prev;
formula = formula_prev;
app.linkk_formula = formula;
app.linkk_parameter_values = p;
set(app.resultingnumberofRCelementsEditField,'Value',RC_count - 1);
%plot in GUI
w = m_w;
f_real = real(eval(formula));
f_imag = imag(eval(formula));
FarbenLaden()

%Nyquist
cla(app.LinKKZUIAxes)
if strcmp(mode,'complex')
    app.LinKKZUIAxes.YLim=[floor(min(m_imag*100000))/100000, ceil(max(m_imag*100000))/100000];
    %app.LinKKZUIAxes.XLim = [floor(min(m_real*100000))/100000 (floor(min(m_real*100000))/10000+app.LinKKZUIAxes.YLim(2)-app.LinKKZUIAxes.YLim(1))];
    app.LinKKZUIAxes.XLimMode='auto';
    plot(app.LinKKZUIAxes,m_real,m_imag,'o','DisplayName','Measurement','color',RWTHBlau,'LineWidth',1,'MarkerSize',7);hold(app.LinKKZUIAxes,'on');
    plot(app.LinKKZUIAxes,f_real,f_imag,'x','DisplayName','Fit','color',RWTHRot,'LineWidth',1,'MarkerSize',7);
    grid(app.LinKKZUIAxes,'on'); hold(app.LinKKZUIAxes,'on'); set(app.LinKKZUIAxes,'ydir', 'reverse'); %axis(app.axes1,'equal');
    app.LinKKZUIAxes.XLabel.Interpreter='latex'; app.LinKKZUIAxes.YLabel.Interpreter='latex'; app.LinKKZUIAxes.XLabel.String='$\Re\{\underline Z\}$ in $\Omega$'; app.LinKKZUIAxes.YLabel.String='$\Im\{\underline Z\}$ in $\Omega$'; 
    h1 = legend(app.LinKKZUIAxes,'Measurement','Fit','Location','NorthWest');
    set(h1,'Interpreter','none');
end
%Bode
%Real part over frequency
cla(app.LinKKRealUIAxes)
if strcmp(mode,'complex') || strcmp(mode,'real')
    semilogx(app.LinKKRealUIAxes,m_f,real(m_Z),'o','color',RWTHBlau,'DisplayName','Measurement','LineWidth',1,'MarkerSize',7);hold(app.LinKKRealUIAxes,'on');
    semilogx(app.LinKKRealUIAxes,m_f,f_real,'o','color',RWTHRot,'DisplayName','Fit','LineWidth',1,'MarkerSize',7);
    set(app.LinKKRealUIAxes,'xdir','reverse'),grid(app.LinKKRealUIAxes,'on'), hold(app.LinKKRealUIAxes,'on');
    set(app.LinKKRealUIAxes,'TickLabelInterpreter','latex')
    h1 = legend(app.LinKKRealUIAxes,'show','Location','NorthWest');
    set(h1,'Interpreter','latex');
    ylabel(app.LinKKRealUIAxes,'$\Re\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
    xlabel(app.LinKKRealUIAxes,'$f$ in Hz','Interpreter','latex');
end

%Imaginary part over frequency
cla(app.LinKKImagUIAxes)
if strcmp(mode,'complex') || strcmp(mode,'imag')
    semilogx(app.LinKKImagUIAxes,m_f,imag(m_Z),'o','color',RWTHBlau,'DisplayName','Measurement','LineWidth',1,'MarkerSize',7);hold(app.LinKKImagUIAxes,'on');
    semilogx(app.LinKKImagUIAxes,m_f,f_imag,'o','color',RWTHRot,'DisplayName','Fit','LineWidth',1,'MarkerSize',7);
    set(app.LinKKImagUIAxes,'xdir','reverse'),grid(app.LinKKImagUIAxes,'on'), hold(app.LinKKImagUIAxes,'on');
    set(app.LinKKImagUIAxes,'TickLabelInterpreter','latex')
    h1 = legend(app.LinKKRealUIAxes,'show','Location','NorthWest');
    set(h1,'Interpreter','latex');
    ylabel(app.LinKKImagUIAxes,'$\Im\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
    xlabel(app.LinKKImagUIAxes,'$f$ in Hz','Interpreter','latex');
    set(app.LinKKImagUIAxes,'ydir', 'reverse');
end

%Residuals part over frequency
cla(app.LinKKResidualUIAxes)
delta_real = ((real(m_Z)-f_real)./abs(f_real+1i*f_imag))*100;
delta_imag = ((imag(m_Z)-f_imag)./abs(f_real+1i*f_imag))*100;
if strcmp(mode,'complex') || strcmp(mode,'real')
    semilogx(app.LinKKResidualUIAxes,m_f,delta_real,'o','color',RWTHBlau,'DisplayName','Real residual','LineWidth',1,'MarkerSize',7);hold(app.LinKKResidualUIAxes,'on');
end
if strcmp(mode,'complex') || strcmp(mode,'imag')
    semilogx(app.LinKKResidualUIAxes,m_f,delta_imag,'o','color',RWTHRot,'DisplayName','Imag residual','LineWidth',1,'MarkerSize',7);
end
set(app.LinKKResidualUIAxes,'xdir','reverse'),grid(app.LinKKResidualUIAxes,'on'); hold(app.LinKKResidualUIAxes,'on');
set(app.LinKKResidualUIAxes,'TickLabelInterpreter','latex')
h1 = legend(app.LinKKResidualUIAxes,'show','Location','NorthWest');
set(h1,'Interpreter','latex');
ylabel(app.LinKKResidualUIAxes,'Residuals in %','Interpreter','latex');
xlabel(app.LinKKResidualUIAxes,'$f$ in Hz','Interpreter','latex');

app.LinKKLamp.Color = 'green';
end