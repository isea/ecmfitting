function [f_real,f_imag] = linKK_lsqr_independent(f,m_real,m_imag,ind,cap,mode)
%Function to control the validity of Kramers Kronig relation similar to:
%https://www.iam.kit.edu/et/Lin-KK.php
%https://github.com/ECSHackWeek/impedance.py

%set adjustable parameters from GUI interface
if strcmp(mode,'complex')
    use_real = 1;
    use_imag = 1;
elseif strcmp(mode,'imag')
    use_real = 0;
    use_imag = 1;
else
    mode = 'real';
    use_real = 1;
    use_imag = 0;
end
Max_RC_count = 50;
overfitting_criterion = 0.85;

%initialize data structures
formula_prev = [];
formula = [];
Tau = [];
Tau_prev = [];
index=find(-m_imag>0);
index=find(min(-m_imag(index)))+index(1)-1;
if index == 1
    Rser_Init= m_real(1);
else
    Rser_Init=m_real(index-1)+(m_real(index)-m_real(index-1))*(m_imag(index-1))/(-m_imag(index)+m_imag(index-1));
end
m_f = f(find(m_imag<=0));
m_real = m_real(find(m_imag<=0));
m_imag = m_imag(find(m_imag<=0));
m_w = 2 * pi .* m_f;
m_Z = m_real + 1i * m_imag;
number_of_added_ec_elements = 1;
if cap
    number_of_added_ec_elements = number_of_added_ec_elements + 1;
end
if ind
    number_of_added_ec_elements = number_of_added_ec_elements + 1;
end

%fit an increasing number of RC Elements to spectrum until weight of negative
%resistors is 1 - overfitting_criterion 
for RC_count = 1:Max_RC_count
    %determine logarithmically distributed time constants and build
    %coefficient matrix and right side vector as in literature
    coefficient_matrix{1} = zeros(length(m_Z),number_of_added_ec_elements+RC_count);
    coefficient_matrix{1}(1:length(m_real),1) = use_real*1;
    coefficient_matrix{1}(length(m_real)+1:length(m_real)+length(m_imag),1) = 0;
    formula{1} = 'p(1)';    
    if cap
        coefficient_matrix{1}(1:length(m_real),2) = use_real*0;
        coefficient_matrix{1}(length(m_real)+1:length(m_real)+length(m_imag),2) = use_imag*(-1./(m_w));    
        formula{1} = [formula{1}, '+p(2)./(1i.*w)'];
    end
    if ind
        if cap
            coefficient_matrix{1}(1:length(m_real),3) = use_real*0;
            coefficient_matrix{1}(length(m_real)+1:length(m_real)+length(m_imag),3) = use_imag*(m_w);   
            formula{1} = [formula{1}, '+1i.*w.*p(3)']; 
        else
            coefficient_matrix{1}(1:length(m_real),2) = use_real*0;
            coefficient_matrix{1}(length(m_real)+1:length(m_real)+length(m_imag),2) = use_imag*(m_w);   
            formula{1} = [formula{1}, '+1i.*w.*p(2)'];
        end
    end
    Tau = zeros(1,RC_count);
    if RC_count == 1
        Tau(1) = 1/max(m_w);
        coefficient_matrix{1}(1:length(m_real),number_of_added_ec_elements+1) = use_real*1./(1+(m_w.*Tau(1)).^2);
        coefficient_matrix{1}(length(m_real)+1:length(m_real)+length(m_imag),number_of_added_ec_elements+1) = use_imag*(-1.*m_w.*Tau(1))./(1+(m_w.*Tau(1)).^2);  
        formula{1} = [formula{1}, '+p(',num2str(1+number_of_added_ec_elements),')./(1+1i.*w.*' , num2str(Tau(1)) ,')'];
    elseif RC_count == 2
        Tau(1) = 1/max(m_w);
        Tau(2) = 1/min(m_w);
        coefficient_matrix{1}(1:length(m_real),number_of_added_ec_elements+1) = use_real*1./(1+(m_w.*Tau(1)).^2);
        coefficient_matrix{1}(length(m_real)+1:length(m_real)+length(m_imag),number_of_added_ec_elements+1) = use_imag*(-1.*m_w.*Tau(1))./(1+(m_w.*Tau(1)).^2);  
        coefficient_matrix{1}(1:length(m_real),number_of_added_ec_elements+2) = use_real*1./(1+(m_w.*Tau(2)).^2);
        coefficient_matrix{1}(length(m_real)+1:length(m_real)+length(m_imag),number_of_added_ec_elements+2) = use_imag*(-1.*m_w.*Tau(2))./(1+(m_w.*Tau(2)).^2); 
        formula{1} = [formula{1}, '+p(',num2str(1+number_of_added_ec_elements),')./(1+1i.*w.*' , num2str(Tau(1)) ,')'];
        formula{1} = [formula{1}, '+p(',num2str(2+number_of_added_ec_elements),')./(1+1i.*w.*' , num2str(Tau(2)) ,')'];
    else
        Tau(1) = 1/max(m_w); 
        coefficient_matrix{1}(1:length(m_real),number_of_added_ec_elements+1) = use_real*1./(1+(m_w.*Tau(1)).^2);
        coefficient_matrix{1}(length(m_real)+1:length(m_real)+length(m_imag),number_of_added_ec_elements+1) = use_imag*(-1.*m_w.*Tau(1))./(1+(m_w.*Tau(1)).^2);  
        formula{1} = [formula{1}, '+p(',num2str(1+number_of_added_ec_elements),')./(1+1i.*w.*' , num2str(Tau(1)) ,')']; 
        for n = 2:RC_count-1          
            Tau(n) = 10^(log10(1/max(m_w))+ ((n - 1)/(RC_count - 1)) * log10((1/min(m_w))/(1/max(m_w))));
            coefficient_matrix{1}(1:length(m_real),number_of_added_ec_elements+n) = use_real*1./(1+(m_w.*Tau(n)).^2);
            coefficient_matrix{1}(length(m_real)+1:length(m_real)+length(m_imag),number_of_added_ec_elements+n) = use_imag*(-1.*m_w.*Tau(n))./(1+(m_w.*Tau(n)).^2);  
            formula{1} = [formula{1}, '+p(' , num2str(n+number_of_added_ec_elements) , ')./(1+1i.*w.*' , num2str(Tau(n)) ,')'];
        end
        Tau(RC_count) = 1/min(m_w);
        coefficient_matrix{1}(1:length(m_real),number_of_added_ec_elements+RC_count) = use_real*1./(1+(m_w.*Tau(RC_count)).^2);
        coefficient_matrix{1}(length(m_real)+1:length(m_real)+length(m_imag),number_of_added_ec_elements+RC_count) = use_imag*(-1.*m_w.*Tau(RC_count))./(1+(m_w.*Tau(RC_count)).^2);  
        formula{1} = [formula{1}, '+p(' , num2str(RC_count+number_of_added_ec_elements) , ')./(1+1i.*w.*' , num2str(Tau(RC_count)) ,')'];
    end
    right_side_vector = [use_real*m_real;use_imag*m_imag];
    
    %different distributions    
    %2:
    coefficient_matrix{2} = zeros(length(m_Z),number_of_added_ec_elements+RC_count);
    coefficient_matrix{2}(1:length(m_real),1) = use_real*1;
    coefficient_matrix{2}(length(m_real)+1:length(m_real)+length(m_imag),1) = 0;
    formula{2} = 'p(1)';    
    if cap
        coefficient_matrix{2}(1:length(m_real),2) = use_real*0;
        coefficient_matrix{2}(length(m_real)+1:length(m_real)+length(m_imag),2) = use_imag*(-1./(m_w));    
        formula{2} = [formula{2}, '+p(2)./(1i.*w)'];
    end
    if ind
        if cap
            coefficient_matrix{2}(1:length(m_real),3) = use_real*0;
            coefficient_matrix{2}(length(m_real)+1:length(m_real)+length(m_imag),3) = use_imag*(m_w);   
            formula{2} = [formula{2}, '+1i.*w.*p(3)']; 
        else
            coefficient_matrix{2}(1:length(m_real),2) = use_real*0;
            coefficient_matrix{2}(length(m_real)+1:length(m_real)+length(m_imag),2) = use_imag*(m_w);   
            formula{2} = [formula{2}, '+1i.*w.*p(2)'];
        end
    end
    Tau = zeros(1,RC_count);
    for n = 1:RC_count          
        Tau(n) = 10^(log10(1/max(m_w))+ ((n-1)/(RC_count) + 1/2 * 1/(RC_count)) * log10((1/min(m_w))/(1/max(m_w))));
        coefficient_matrix{2}(1:length(m_real),number_of_added_ec_elements+n) = use_real*1./(1+(m_w.*Tau(n)).^2);
        coefficient_matrix{2}(length(m_real)+1:length(m_real)+length(m_imag),number_of_added_ec_elements+n) = use_imag*(-1.*m_w.*Tau(n))./(1+(m_w.*Tau(n)).^2);  
        formula{2} = [formula{2}, '+p(' , num2str(n+number_of_added_ec_elements) , ')./(1+1i.*w.*' , num2str(Tau(n)) ,')'];
    end    
    
    %3:
    coefficient_matrix{3} = zeros(length(m_Z),number_of_added_ec_elements+RC_count);
    coefficient_matrix{3}(1:length(m_real),1) = use_real*1;
    coefficient_matrix{3}(length(m_real)+1:length(m_real)+length(m_imag),1) = 0;
    formula{3} = 'p(1)';    
    if cap
        coefficient_matrix{3}(1:length(m_real),2) = use_real*0;
        coefficient_matrix{3}(length(m_real)+1:length(m_real)+length(m_imag),2) = use_imag*(-1./(m_w));    
        formula{3} = [formula{3}, '+p(2)./(1i.*w)'];
    end
    if ind
        if cap
            coefficient_matrix{3}(1:length(m_real),3) = use_real*0;
            coefficient_matrix{3}(length(m_real)+1:length(m_real)+length(m_imag),3) = use_imag*(m_w);   
            formula{3} = [formula{3}, '+1i.*w.*p(3)']; 
        else
            coefficient_matrix{3}(1:length(m_real),2) = use_real*0;
            coefficient_matrix{3}(length(m_real)+1:length(m_real)+length(m_imag),2) = use_imag*(m_w);   
            formula{3} = [formula{3}, '+1i.*w.*p(2)'];
        end
    end
    Tau = zeros(1,RC_count);
    for n = 1:RC_count          
        Tau(n) = 10^(log10(1/max(m_w))+ ((n-1)/(RC_count) + 1/4 * 1/(RC_count)) * log10((1/min(m_w))/(1/max(m_w))));
        coefficient_matrix{3}(1:length(m_real),number_of_added_ec_elements+n) = use_real*1./(1+(m_w.*Tau(n)).^2);
        coefficient_matrix{3}(length(m_real)+1:length(m_real)+length(m_imag),number_of_added_ec_elements+n) = use_imag*(-1.*m_w.*Tau(n))./(1+(m_w.*Tau(n)).^2);  
        formula{3} = [formula{3}, '+p(' , num2str(n+number_of_added_ec_elements) , ')./(1+1i.*w.*' , num2str(Tau(n)) ,')'];
    end    
    
    %4:
    coefficient_matrix{4} = zeros(length(m_Z),number_of_added_ec_elements+RC_count);
    coefficient_matrix{4}(1:length(m_real),1) = use_real*1;
    coefficient_matrix{4}(length(m_real)+1:length(m_real)+length(m_imag),1) = 0;
    formula{4} = 'p(1)';    
    if cap
        coefficient_matrix{4}(1:length(m_real),2) = use_real*0;
        coefficient_matrix{4}(length(m_real)+1:length(m_real)+length(m_imag),2) = use_imag*(-1./(m_w));    
        formula{4} = [formula{4}, '+p(2)./(1i.*w)'];
    end
    if ind
        if cap
            coefficient_matrix{4}(1:length(m_real),3) = use_real*0;
            coefficient_matrix{4}(length(m_real)+1:length(m_real)+length(m_imag),3) = use_imag*(m_w);   
            formula{4} = [formula{4}, '+1i.*w.*p(3)']; 
        else
            coefficient_matrix{4}(1:length(m_real),2) = use_real*0;
            coefficient_matrix{4}(length(m_real)+1:length(m_real)+length(m_imag),2) = use_imag*(m_w);   
            formula{4} = [formula{4}, '+1i.*w.*p(2)'];
        end
    end
    Tau = zeros(1,RC_count);
    for n = 1:RC_count          
        Tau(n) = 10^(log10(1/max(m_w))+ ((n-1)/(RC_count) + 3/4 * 1/(RC_count)) * log10((1/min(m_w))/(1/max(m_w))));
        coefficient_matrix{4}(1:length(m_real),number_of_added_ec_elements+n) = use_real*1./(1+(m_w.*Tau(n)).^2);
        coefficient_matrix{4}(length(m_real)+1:length(m_real)+length(m_imag),number_of_added_ec_elements+n) = use_imag*(-1.*m_w.*Tau(n))./(1+(m_w.*Tau(n)).^2);  
        formula{4} = [formula{4}, '+p(' , num2str(n+number_of_added_ec_elements) , ')./(1+1i.*w.*' , num2str(Tau(n)) ,')'];
    end    
    
    
    %specify initial values and boundaries
    p_init = 1e-3 * ones(RC_count+number_of_added_ec_elements,1);
    p_init(1) = Rser_Init;
    for n = number_of_added_ec_elements+1:length(p_init)
        p_init(n) = (max(m_real)+min(m_real))/2;
    end
    
    %fit model
%     options = optimset('MaxIter',2000000,'MaxFunEvals',2000000,'TolX',1e-10,'TolFun',1e-10,'Display','off');    
%     [p{1},flag{1},relres{1},iter{1},resvec{1}] = lsqr(coefficient_matrix{1},right_side_vector,1e-10,2000000,[],[],p_init);
%     [p{2},flag{2},relres{2},iter{2},resvec{2}] = lsqr(coefficient_matrix{2},right_side_vector,1e-10,2000000,[],[],p_init);
%     [p{3},flag{3},relres{3},iter{3},resvec{3}] = lsqr(coefficient_matrix{3},right_side_vector,1e-10,2000000,[],[],p_init);
%     [p{4},flag{4},relres{4},iter{4},resvec{4}] = lsqr(coefficient_matrix{4},right_side_vector,1e-10,2000000,[],[],p_init);
%     [~,best_fit_idx] = min([relres{1},relres{2},relres{3},relres{4}]);
%     p_best{RC_count} = p{best_fit_idx};

    %discern optimal fit via pserdo-inverse matrix multiplication
    pfit{1} = pinv(coefficient_matrix{1}) * right_side_vector;
    pfit{2} = pinv(coefficient_matrix{2}) * right_side_vector;
    pfit{3} = pinv(coefficient_matrix{3}) * right_side_vector;
    pfit{4} = pinv(coefficient_matrix{4}) * right_side_vector;
    for n2 = 1:4
        p = pfit{n2};
        w = m_w;
        Z_fit_real = real(eval(formula{n2}));
        Z_fit_imag = imag(eval(formula{n2}));
        RMSE{n2} = sqrt(sum((m_real - Z_fit_real).^2)/length(m_Z)) + sqrt(sum((m_imag - Z_fit_imag).^2)/length(m_Z));     
    end
    [~,best_fit_idx] = min([RMSE{1},RMSE{2},RMSE{3},RMSE{4}]);
    p_best{RC_count} = pfit{best_fit_idx};

       
    %Check if border to overfitting is reached
    negative_R_index = find(p_best{RC_count}([1 , 1 + number_of_added_ec_elements:end])<0);
    positive_R_index = find(p_best{RC_count}([1 , 1 + number_of_added_ec_elements:end])>=0);
    if ~isempty(find(positive_R_index == 1))
        positive_R_index(1) = -1 * (number_of_added_ec_elements-1);
        positive_R_index(2:end) =  positive_R_index(2:end) - 1;
        negative_R_index(:) = negative_R_index(:) - 1;
    else
        negative_R_index(1) = -1 * (number_of_added_ec_elements-1);
        negative_R_index(2:end) =  negative_R_index(2:end) - 1;
        positive_R_index(:) = positive_R_index(:) - 1;
    end
    criterion(RC_count) = 1 - (sum(abs(p_best{RC_count}(negative_R_index + number_of_added_ec_elements)))/sum(abs(p_best{RC_count}(positive_R_index + number_of_added_ec_elements))));
    formula_cell{RC_count} = formula{best_fit_idx};
    
    p_prev = p_best;   
    formula_prev = formula{best_fit_idx};
    coefficient_matrix_prev = coefficient_matrix{1};
end
best_idx = find(criterion>=0.85,1,'last');
% plot(1:Max_RC_count,criterion,'-X')
% p = p_prev;
p = p_best{best_idx};
formula_best = formula_prev;
formula_best = formula_cell{best_idx};
coefficient_matrix_best = coefficient_matrix_prev; 

%plot in GUI
w = m_w;
f_real = real(eval(formula_best));
f_imag = imag(eval(formula_best));

FarbenLaden()

%create figures
%Nyquist
h = figure('Name','LinKK Visualization','NumberTitle','off','UnitS','centimeters','Position',[0, 0, 25, 10]);
delta_real = ((real(m_Z)-f_real)./abs(f_real+1i*f_imag))*100;
delta_imag = ((imag(m_Z)-f_imag)./abs(f_real+1i*f_imag))*100;
semilogx(m_f,delta_real,'o-','color',RWTHBlau,'DisplayName','Real residual','LineWidth',1,'MarkerSize',7);hold(gca,'on');
semilogx(m_f,delta_imag,'o-','color',RWTHRot,'DisplayName','Imag residual','LineWidth',1,'MarkerSize',7);
set(gca,'xdir','reverse'),grid(gca,'on'); hold(gca,'on');
set(gca,'TickLabelInterpreter','latex')
h1 = legend(gca,'show','Location','NorthWest');
set(h1,'Interpreter','latex');
ylabel(gca,'Residuals in %','Interpreter','latex');
xlabel(gca,'$f$ in Hz','Interpreter','latex');
set(gca, 'YLim',[min(min(delta_imag),min(delta_real)) max(max(delta_imag),max(delta_real))])
end
