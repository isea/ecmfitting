function fcn_ReadCompleteTestsetofoneTemperatureButtonPushed(app, event);
global Pattern % Hierf�r m�ssen im config.m file Pattern f�r Batterie und masterfile angelegt werden
alter_pfad = pwd;
[filename, pathname] = uigetfile({'*.mat' '�Eis oder Eiskarte'; '*.txt;*.csv' 'Zahner';'*.log' 'EISlog';'*.xls;*.xlsx' 'Excel'}, 'Die Daten ausw�hlen.', 'MultiSelect','on');
cd(alter_pfad);
if isempty(filename) || (~iscell(filename) && sum(filename == 0) )
    error('no data were selected!!!');
    return
end
if ~iscell(filename)
    app.FileReadFiles(1) = filename;
else
    app.FileReadFiles = filename;
end
%% wenn AutoCap und vorhanden
% if app.AutoCapCheckBox
%% Auswertung Metadaten
% Bateriename
filename=app.FileReadFiles{1};
if strcmp(filename(end-3:end),'.mat')
    messung=load(strcat(pathname,filename));
else
    error('Read-in automated for Mat-Files only!!!');
    return
end
Batteriefound = 0;
for i = 1:numel(Pattern.Batterie)
    [k,l] = regexp(filename,Pattern.Batterie{i});
    if isempty(k) && Pattern.Batterie{i}(1)=='_'
        [k,l] = regexp(filename(1:numel(Pattern.Batterie{i})),Pattern.Batterie{i}(2:end));
    end
    if k > 0
        set(app.BatterieTextBox,'Value',filename(k:l));
        Batteriefound = 1;
        break
    end
end
if Batteriefound==0
    while true
        Batteriename= inputdlg('What is the batteries name?','battery name',[1],{'dummy'});
        if  isempty(Batteriename) ||    strcmp(Batteriename,'')
            return
        else
            Batteriename=Batteriename{1};
            old_path = cd;
            cd([old_path , '\' , 'FittingGUIFunctions\FileReadFunctions'])
            temp_table = readtable('Batterie.xlsx');
            temp_table.Batterie{length(temp_table.Batterie)+1,1} = Batteriename;
            writetable(temp_table,'Batterie.xlsx');
            Pattern.Batterie{length(temp_table.Batterie)+1} = Batteriename;
            cd(old_path)
            break
        end
    end
    set(app.BatterieTextBox,'Value',Batteriename);
    Batteriefound = 1;
end
% Datum
datefound = 0;
[DString DStringOrig] = FindDateInStr(filename);
if ~isempty(DString)
    datefound = 1;
    set(app.DatumTextBox,'Value',datestr(datenum(DString)))
    fcn_DatumTextBox_Callback(app,event)
end
%% Masterfile analysieren
% Masterfile finden und laden
set(app.MainTabGroupRight,'SelectedTab',app.FileReadTab)
masterfound = 0;
master_file = dir([pathname '*' DStringOrig '*.mat']);
for Master_idx = 1:numel(Pattern.MasterProgram)
    for j = 1:numel(master_file)
        if ~isempty(strfind(master_file(j).name,app.BatterieTextBox.Value)) && ~isempty(strfind(master_file(j).name,Pattern.MasterProgram{Master_idx})) && ~isempty(strfind(master_file(j).name,'Format01')) && isempty(strfind(master_file(j).name,'EIS00'))
            master = load([pathname master_file(j).name]);
            app.FileReadMasterFile=[pathname master_file(j).name];
            Master_Pattern = Pattern.MasterProgram{Master_idx};
            masterfound = 1;
            break
        end
    end
    if masterfound, break, end
end
if masterfound == 0 && strcmp('Ja',questdlg('No Digatron master file could be found. Do you want to search for it yourself?', ...
        'Masterfile', ...
        'Yes','No','No'))
    alter_pfad = pwd;
    [filename, pathname] = uigetfile({'*.mat' 'Matlab'}, 'Choose MasterFile.', 'MultiSelect','off');
    cd(alter_pfad);
    if isempty(filename) || (~iscell(filename) && sum(filename == 0) )
        masterfound = 0;
    else
        master = load([pathname filename]);
        app.FileReadMasterFile=[pathname master_file(j).name];
        masterfound = 1;
    end
    if masterfound
        Master_Pattern = inputdlg('What is the Master-programs name?');
        old_path = cd;
        cd([old_path , '\' , 'FittingGUIFunctions\FileReadFunctions'])
        temp_table = readtable('MasterProgram.xlsx');
        temp_table.MasterProgram{length(temp_table.MasterProgram)+1,1} = Master_Pattern{1};
        writetable(temp_table,'MasterProgram.xlsx');
        Pattern.MasterProgram{length(temp_table.MasterProgram)+1} = Master_Pattern{1};
        cd(old_path)
    end
end
% Masterfile indizieren
if masterfound
    app.FileReadTable.Data=[{'Initialize...'} {'Initialize...'}];
    app.FileReadTable.ColumnFormat=[{''} {''}];
    procedure_array={'A'};
    array_idx=1;
    for procedure_idx = 1:length(master.diga.daten.Prozedur)
        if ~ismember(master.diga.daten.Prozedur(procedure_idx),procedure_array)
            procedure_array(array_idx,1)=master.diga.daten.Prozedur(procedure_idx);
            procedure_array(array_idx,2)={'Choose'};
            array_idx=array_idx+1;
        end
    end
    
    app.FileReadTable.Data=procedure_array;
    app.FileReadTable.ColumnFormat={[] {' ','InitialCharge','Cap-Test','EIS','Pulse','QOCV'}};
    app.LoadIdentifiedDataButton.Enable=1;
end
end