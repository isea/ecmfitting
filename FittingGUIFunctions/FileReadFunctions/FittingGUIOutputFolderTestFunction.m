function FittingGUIOutputFolderTestFunction
Test = 1;
FolderPath = uigetdir('Please select the Output-Folder that has to be checked!');
old_path = cd;
cd(FolderPath)
zustand_list = dir;
% Iterate over all states
for zustand_iterator = 3:length(zustand_list)
    cd([FolderPath '\' zustand_list(zustand_iterator).name])
    temperature_list = [];
    temperature_list = dir;
    % Iterate over all temperature folders in corresponding state folder
    for temperature_iterator = 3:length(temperature_list)
        cd([FolderPath '\' zustand_list(zustand_iterator).name '\' temperature_list(temperature_iterator).name])
        SOC_list = [];
        SOC_list = dir('*SOC.mat');
        %Iterate over all SOCs
        for SOC_iterator = 1:length(SOC_list)
            DRT_GUI = [];
            load(SOC_list(SOC_iterator).name)
            %Check Metadata
            if ~isfield(DRT_GUI.Testparameter,'fileName') || (isfield(DRT_GUI.Testparameter,'fileName') && isempty(DRT_GUI.Testparameter.fileName))
                disp(['Die Datei' SOC_list(SOC_iterator).name ' enth�lt keinen Dateinamen'])
            end
            if ~isfield(DRT_GUI.Testparameter,'Messdatum')|| (isfield(DRT_GUI.Testparameter,'Messdatum') && isempty(DRT_GUI.Testparameter.Messdatum))
                disp(['Die Datei' SOC_list(SOC_iterator).name ' enth�lt kein Messdatum'])
            end
            if ~isfield(DRT_GUI.Testparameter,'Batterie')|| (isfield(DRT_GUI.Testparameter,'Batterie') && isempty(DRT_GUI.Testparameter.Batterie))
                disp(['Die Datei' SOC_list(SOC_iterator).name ' enth�lt keinen Batterienamen'])
            end
            if ~isfield(DRT_GUI.Testparameter,'Zustand')|| (isfield(DRT_GUI.Testparameter,'Zustand') && isempty(DRT_GUI.Testparameter.Zustand))
                disp(['Die Datei' SOC_list(SOC_iterator).name ' enth�lt keinen Zustand'])
            end
            if ~isfield(DRT_GUI.Testparameter,'Temperatur')|| (isfield(DRT_GUI.Testparameter,'Temperatur') && isempty(DRT_GUI.Testparameter.Temperatur))
                disp(['Die Datei' SOC_list(SOC_iterator).name ' enth�lt keine Temperatur'])
            end
            if ~isfield(DRT_GUI.Testparameter,'Cap')|| (isfield(DRT_GUI.Testparameter,'Cap') && isempty(DRT_GUI.Testparameter.Cap))
                disp(['Die Datei' SOC_list(SOC_iterator).name ' enth�lt keine Kapazit�t'])
            end
            if ~isfield(DRT_GUI.Testparameter,'SOC')|| (isfield(DRT_GUI.Testparameter,'SOC') && isempty(DRT_GUI.Testparameter.SOC))
                disp(['Die Datei' SOC_list(SOC_iterator).name ' enth�lt keinen SOC'])
            end
%             if ~isfield(DRT_GUI.Testparameter,'AhSteps')|| (isfield(DRT_GUI.Testparameter,'AhSteps') && isempty(DRT_GUI.Testparameter.AhSteps))
%                 disp(['Die Datei' SOC_list(SOC_iterator).name ' enth�lt keine AhSteps'])
%             end
            
            %Check for data in the file
            if isempty(DRT_GUI.Messdaten.Zreal)
                if isempty(DRT_GUI.Messdaten.pulse.zeit)
                    disp(['Die Datei' SOC_list(SOC_iterator).name ' enth�lt keine vollst�ndigen Datens�tze f�r die Parametrierung'])
                else
                    DRT_GUI.Messdaten.frequenz = [1000 ; 2000];
                    DRT_GUI.Messdaten.tau = [1000 ; 2000];
                    DRT_GUI.Messdaten.omega = 2*pi*[1000 ; 2000];
                    DRT_GUI.Messdaten.Zreal = [0; 0.00001];
                    DRT_GUI.Messdaten.Zimg = [0; 0.00001];
                    DRT_GUI.Messdaten.aktiv = [1 ; 1];
                    DRT_GUI.Messdaten.Z = [0+0i;0.00001+0.00001i];
                    DRT_GUI.Fit.frequenz = [1000 ; 2000];
                    DRT_GUI.Fit.tau = [1000 ; 2000];
                    DRT_GUI.Fit.omega = 2*pi*[1000 ; 2000];
                    DRT_GUI.Fit.Zreal = [0; 0.00001];
                    DRT_GUI.Fit.Zimg = [0; 0.00001];
                    DRT_GUI.Fit.Z = [0+0i;0.00001+0.00001i];
                    DRT_GUI.Korrigiert.frequenz = [1000 ; 2000];
                    DRT_GUI.Korrigiert.tau = [1000 ; 2000];
                    DRT_GUI.Korrigiert.omega = 2*pi*[1000 ; 2000];
                    DRT_GUI.Korrigiert.Zreal = [0; 0.00001];
                    DRT_GUI.Korrigiert.Zimg = [0; 0.00001];
                    DRT_GUI.Korrigiert.aktiv = [1 ; 1];
                    DRT_GUI.Korrigiert.Z = [0+0i;0.00001+0.00001i];
                end
            elseif length(DRT_GUI.Messdaten.frequenz)~=length(DRT_GUI.Messdaten.omega) || length(DRT_GUI.Messdaten.frequenz)~=length(DRT_GUI.Messdaten.Zreal) || length(DRT_GUI.Messdaten.frequenz)~=length(DRT_GUI.Messdaten.Zimg)
                disp(['Die Datei' SOC_list(SOC_iterator).name ' enth�lt Daten im Frequenzbereich unterscheidlicher L�nge!'])
            end
            
            if ~isfield(DRT_GUI,'DRT') || (isfield(DRT_GUI,'DRT') && isempty(DRT_GUI.DRT))
                disp(['Die Datei' SOC_list(SOC_iterator).name ' enth�lt keine DRT-Daten!'])
            end
            
            if ~isfield(DRT_GUI.Fit,'Implementierung') || (isfield(DRT_GUI.Fit,'Implementierung') && isempty(DRT_GUI.Fit.Implementierung))
                disp(['Die Datei' SOC_list(SOC_iterator).name ' enth�lt keine Datenstruktur f�r das Zeitbereichsfitting!'])
            end
            
            if ~isfield(DRT_GUI.Fit,'aktuell_Modell') || (isfield(DRT_GUI.Fit,'aktuell_Modell') && isempty(DRT_GUI.Fit.aktuell_Modell))
                disp(['Die Datei' SOC_list(SOC_iterator).name ' enth�lt kein Modell!'])
            end
        end
    end
end
cd(old_path)
end

