function app = fcn_FR_ModellnamePopup(app,event)
global ModelDaten
gui_objects = fieldnames(app);
Par_Radio = gui_objects(find(~cellfun(@isempty,strfind(gui_objects,'Par')) & ~cellfun(@isempty,strfind(gui_objects,'Radio'))));
for i = 1:numel(Par_Radio)
    app.(Par_Radio{i}).Value=0;
    app.(Par_Radio{i}).Visible='off';
end
if app.GUI
    Modellname = get(app.FR_ModellnamePopup,'Items');
    Modellname = get(app.FR_ModellnamePopup,'Value');
else
    Modellname = app.FR_ModellnamePopup.Items;
    Modellname = app.FR_ModellnamePopup.Value;
end
Modell = [];
for i = 1:numel(ModelDaten.Model)
    if ~isempty( ModelDaten.Model{i})
        if sum(ismember(fieldnames(ModelDaten.Model{i}),'Fit')) && ~isempty(ModelDaten.Model{i}.Fit)
            if strcmp(Modellname,ModelDaten.Model{i}.Fit.Modell.Modellname)
                Modell = ModelDaten.Model{i}.Fit.Modell;
                break
            end
        end
        
    end
end
if isempty(Modell),return,end
for i = 1:size(Modell.P_Name,2)
    if app.GUI
        set(app.(['Par' num2str(i) 'Radio']),'Text',Modell.P_Name(1,i));
        set(app.(['Par' num2str(i) 'Radio']),'Visible','on');
    else
        app.(['Par' num2str(i) 'Radio']).Text = Modell.P_Name(1,i);
        app.(['Par' num2str(i) 'Radio']).Visible = 'on';    
    end
end
%re-choose previous value if present
if isfield(ModelDaten,'ParName')
    gui_objects = fieldnames(app);
    Par_Radio = gui_objects(find(~cellfun(@isempty,strfind(gui_objects,'Par')) & ~cellfun(@isempty,strfind(gui_objects,'Radio'))));
    par_found = 0;
    for i = 1:numel(Par_Radio)        
        if strcmp(app.(Par_Radio{i}).Text{1},ModelDaten.ParName{1})
            set(app.(Par_Radio{i}),'Value',1);
            par_found = 1;
            break
        end    
    end
    if ~par_found
        ModelDaten.ParName{1} = app.(Par_Radio{1}).Text{1};
    end
end
%call functions to display chosen parameter values
if app.SurfacePlot
    app = fcn_FR_ExportParameterSurfacePlotFcn(app,event);
else
    app = fcn_FR_ParameterPanel_SelectionChangeFcn(app,event);
end
end