function app = fcn_FR_ParameterPanel_SelectionChangeFcn(app,event)
global ModelDaten
gui_objects = fieldnames(app);
Par_Radio = gui_objects(find(~cellfun(@isempty,strfind(gui_objects,'Par')) & ~cellfun(@isempty,strfind(gui_objects,'Radio'))));
if app.GUI
    ModelName = get(app.FR_ModellnamePopup,'Items');
    ModelName =  get(app.FR_ModellnamePopup,'Value');
    if get(app.FR_spline_check,'Value')
        methode = 'spline';
    else
        methode = 'linear';
    end
else
    ModelName = app.FR_ModellnamePopup.Items;
    ModelName =  app.FR_ModellnamePopup.Value;
    if app.FR_spline_check.Value
        methode = 'spline';
    else
        methode = 'linear';
    end
end
for i = 1:numel(Par_Radio)
    if app.GUI
        if get(app.(Par_Radio{i}),'Value')
            if strcmp(get(app.(Par_Radio{i}),'Visible'),'off')
                app.Par1Radio.Value=1;
                app.Par_Radio{i}.Value=0;
                ParNummer = find(~cellfun(@isempty,strfind(gui_objects,'Par1Radio')));
                ModelDaten.ParName = get(app.('Par1Radio'),'Text');                
            else
                ParNummer = i;
                ModelDaten.ParName=get(app.(Par_Radio{i}),'Text');
            end
            break            
        end
    else
        if app.(Par_Radio{i}).Value
            if strcmp(app.(Par_Radio{i}).Visible,'off')
                app.Par1Radio.Value=1;
                app.Par_Radio{i}.Value=0;
                ParNummer = find(~cellfun(@isempty,strfind(gui_objects,'Par1Radio')));
                ModelDaten.ParName = app.('Par1Radio').Text;
                
            else
                ParNummer = i;
                ModelDaten.ParName = app.(Par_Radio{i}).Text;
            end
            break
            
        end
    end
end
if strcmp(app.DisplayedStatesButtonGroup.SelectedObject.Text,'T-SOC')
    ModelDaten.Parameter = NaN(numel(ModelDaten.T),numel(ModelDaten.SOC));
else
    ModelDaten.Parameter = NaN(numel(ModelDaten.C.Values),numel(ModelDaten.T),numel(ModelDaten.SOC));
end

for i = find(~cellfun(@isempty,ModelDaten.Model(:)))'
    if sum(ismember(fieldnames(ModelDaten.Model{i}),'Fit')) && ~isempty(ModelDaten.Model{i}.Fit) && ...
            strcmp(ModelDaten.Model{i}.Fit.Modell.Modellname,ModelName) && ( ( sum(ismember(fieldnames(ModelDaten.Model{i}.Fit),'gueltig')) && ModelDaten.Model{i}.Fit.gueltig ) || get(app.FR_ungueltige_check,'Value'))
        ParIndex = find(ismember(ModelDaten.Model{i}.Fit.Modell.P_Name(1,:),ModelDaten.ParName));
        if ~isempty(ParIndex)
            ModelDaten.Parameter(i) = ModelDaten.Model{i}.Fit.Parameter(ParIndex);
%         else
%             ModelDaten.Parameter(i) = ModelDaten.Model{i}.Fit.Parameter(2);
        end
        
    end
end

if strcmp(app.DisplayedStatesButtonGroup.SelectedObject.Text,'T-SOC')
    colors = hsv(numel(ModelDaten.T));
else
    colors = hsv(numel(ModelDaten.C.Values));
end

if app.GUI
    if strcmp(app.DisplayedStatesButtonGroup.SelectedObject.Text,'T-SOC')
        hold(app.axes8,'off')
        for i =1:numel(ModelDaten.T)
            plot(app.axes8,ModelDaten.SOC',ModelDaten.Parameter(i,:)','o','Color',colors(i,:))
            hold(app.axes8,'on')
        end
        grid(app.axes8,'on')
        app.axes8.XLimMode = 'auto';
        app.axes8.YLimMode = 'auto';
        app.axes9.XLimMode = 'auto';
        app.axes9.YLimMode = 'auto';
        for i =1:numel(ModelDaten.T)
            if numel(find(~isnan(ModelDaten.Parameter(i,:))))>1
                x_hd = [min(ModelDaten.SOC(~isnan(ModelDaten.Parameter(i,:)))):1:max(ModelDaten.SOC(~isnan(ModelDaten.Parameter(i,:))))];
                if get(app.FR_extrapolieren_check,'Value'), x_hd = [0:1:min(x_hd) x_hd max(x_hd):1:100];end
                y_hd = interp1(ModelDaten.SOC(~isnan(ModelDaten.Parameter(i,:))),ModelDaten.Parameter(i,~isnan(ModelDaten.Parameter(i,:))),x_hd,methode,'extrap');
                plot(app.axes8,x_hd',y_hd,'Color',colors(i,:))
            end
        end
        app.axes8.XLabel.String='SOC in %';app.axes8.XLabel.Interpreter='latex';
        app.axes8.YLabel.String='';app.axes8.YLabel.Interpreter='latex';
        
        h8=legend(app.axes8,num2str(ModelDaten.T));
        set(h8,'Location','NorthEast')
        if numel(ModelDaten.T)>0
            hold(app.axes9,'off')
            
            SOCs = eval(['[' get(app.FR_SOCText,'Value') ']' ]);
            Values = nan(numel(ModelDaten.T),numel(SOCs));
            colors = hsv(numel(SOCs));
            for i = 1:numel(ModelDaten.T)
                if numel(find(~isnan(ModelDaten.Parameter(i,:))))>1
                    Values(i,:) = interp1(ModelDaten.SOC(~isnan(ModelDaten.Parameter(i,:))),ModelDaten.Parameter(i,~isnan(ModelDaten.Parameter(i,:))),SOCs,methode,'extrap');
                end
            end
            for i = 1:numel(SOCs)
                if get(app.FR_ArrheniusCheckbox,'Value')
                    semilogy(app.axes9,1./(ModelDaten.T+273.15),Values(:,i),'o','Color',colors(i,:))
                else
                    set(app.axes9,'YScale','lin')
                    plot(app.axes9,ModelDaten.T,Values(:,i),'o','Color',colors(i,:))
                end
                hold(app.axes9,'on')
            end
            for i = 1:numel(SOCs)
                if numel(find(~isnan(Values(:,i))))>1
                    if get(app.FR_ArrheniusCheckbox,'Value')
                        x_hd = min(ModelDaten.T):1:max(ModelDaten.T);
                        y_hd = exp(interp1(1./(ModelDaten.T+273.15),log(Values(:,i)),1./(x_hd'+273.15),methode));
                        semilogy(app.axes9,1./(x_hd'+273.15),y_hd,'Color',colors(i,:))
                    else
                        x_hd = min(ModelDaten.T):1:max(ModelDaten.T);
                        y_hd = interp1(ModelDaten.T,Values(:,i),x_hd,methode);
                        plot(app.axes9,x_hd',y_hd,'Color',colors(i,:))
                    end
                end
            end
            if get(app.FR_ArrheniusCheckbox,'Value')
                x_label =  ' 1/T in 1/K';
            else
                x_label =  'Temperature in $^{\circ}$C' ;
            end
            h9=legend(app.axes9,num2str(SOCs'));
            set(h9,'Location','NorthEast')
            %plot(repmat(ModelDaten.T',[numel(ModelDaten.SOC) 1])',ModelDaten.Parameter,'bo')
            grid(app.axes9,'on')
            app.axes9.XLabel.String=x_label;app.axes9.XLabel.Interpreter='latex';
            app.axes9.YLabel.String='';app.axes9.YLabel.Interpreter='latex';
        end
    elseif strcmp(app.DisplayedStatesButtonGroup.SelectedObject.Text,'C-SOC')
        temp_idx = find(str2num(app.StateDropDown.Value) == ModelDaten.T);
        hold(app.axes8,'off')
        for i =1:numel(ModelDaten.C.Values)
            for j = 1:length(ModelDaten.SOC)
                data(j) = ModelDaten.Parameter(i,temp_idx,j);
            end
            plot(app.axes8,ModelDaten.SOC',data','o','Color',colors(i,:))
            hold(app.axes8,'on')
        end
        grid(app.axes8,'on')
        app.axes8.XLimMode = 'auto';
        app.axes8.YLimMode = 'auto';
        app.axes9.XLimMode = 'auto';
        app.axes9.YLimMode = 'auto';
        for i =1:numel(ModelDaten.C.Values)
            for j = 1:length(ModelDaten.SOC)
                data(j) = ModelDaten.Parameter(i,temp_idx,j);
            end
            if numel(find(~isnan(data)))>1
                x_hd = [min(ModelDaten.SOC(~isnan(data))):1:max(ModelDaten.SOC(~isnan(data)))];
                y_hd = interp1(ModelDaten.SOC(~isnan(data)),data(~isnan(data)),x_hd,methode,'extrap');
                plot(app.axes8,x_hd',y_hd,'Color',colors(i,:))
            end
        end
        app.axes8.XLabel.String='SOC in %';app.axes8.XLabel.Interpreter='latex';
        app.axes8.YLabel.String='';app.axes8.YLabel.Interpreter='latex';
        
        h8=legend(app.axes8,num2str(ModelDaten.C.Values));
        set(h8,'Location','NorthEast')
        if numel(ModelDaten.C.Values)>0
            hold(app.axes9,'off')
            
            SOCs = eval(['[' get(app.FR_SOCText,'Value') ']' ]);
            Values = nan(numel(ModelDaten.C.Values),numel(SOCs));
            colors = hsv(numel(SOCs));
            for i = 1:numel(ModelDaten.C.Values)
                for j = 1:length(ModelDaten.SOC)
                    data(j) = ModelDaten.Parameter(i,temp_idx,j);
                end
                if numel(find(~isnan(data)))>1
                    Values(i,:) = interp1(ModelDaten.SOC(~isnan(data)),data(~isnan(data)),SOCs,methode,'extrap');
                end
            end
            for i = 1:numel(SOCs)
                set(app.axes9,'YScale','lin')
                plot(app.axes9,ModelDaten.C.Values,Values(:,i),'o','Color',colors(i,:))
                hold(app.axes9,'on')
            end  
            for i = 1:numel(SOCs)
                if numel(find(~isnan(Values(:,i))))>1                    
                    x_hd = min(ModelDaten.C.Values):0.1:max(ModelDaten.C.Values);
                    y_hd = interp1(ModelDaten.C.Values,Values(:,i),x_hd,methode);
                    plot(app.axes9,x_hd',y_hd,'Color',colors(i,:))
                end
            end
            x_label =  'I in C' ;
            h9=legend(app.axes9,num2str(SOCs'));
            set(h9,'Location','NorthEast')
            %plot(repmat(ModelDaten.T',[numel(ModelDaten.SOC) 1])',ModelDaten.Parameter,'bo')
            grid(app.axes9,'on')
            app.axes9.XLabel.String=x_label;app.axes9.XLabel.Interpreter='latex';
            app.axes9.YLabel.String='';app.axes9.YLabel.Interpreter='latex';
        end
    else
        soc_idx = find(str2num(app.StateDropDown.Value) == ModelDaten.SOC);
        hold(app.axes8,'off')
        for i =1:numel(ModelDaten.C.Values)
            for j = 1:length(ModelDaten.T)
                data(j) = ModelDaten.Parameter(i,j,soc_idx);
            end
            plot(app.axes8,ModelDaten.T',data','o','Color',colors(i,:))
            hold(app.axes8,'on')
        end
        grid(app.axes8,'on')
        app.axes8.XLimMode = 'auto';
        app.axes8.YLimMode = 'auto';
        app.axes9.XLimMode = 'auto';
        app.axes9.YLimMode = 'auto';
        for i =1:numel(ModelDaten.C.Values)
            for j = 1:length(ModelDaten.T)
                data(j) = ModelDaten.Parameter(i,j,soc_idx);
            end
            if numel(find(~isnan(data)))>1
                x_hd = [min(ModelDaten.T(~isnan(data))):1:max(ModelDaten.T(~isnan(data)))];
                y_hd = interp1(ModelDaten.T(~isnan(data)),data(~isnan(data)),x_hd,methode,'extrap');
                plot(app.axes8,x_hd',y_hd,'Color',colors(i,:))
            end
        end
        app.axes8.XLabel.String='T in °C';app.axes8.XLabel.Interpreter='latex';
        app.axes8.YLabel.String='';app.axes8.YLabel.Interpreter='latex';
        
        h8=legend(app.axes8,num2str(ModelDaten.C.Values));
        set(h8,'Location','NorthEast')
        if numel(ModelDaten.C.Values)>0
            hold(app.axes9,'off')
            
            TEMPSs = ModelDaten.T;
            Values = nan(numel(ModelDaten.C.Values),numel(TEMPSs));
            colors = hsv(numel(TEMPSs));
            for i = 1:numel(ModelDaten.C.Values)
                for j = 1:length(ModelDaten.T)
                    data(j) = ModelDaten.Parameter(i,j,soc_idx);
                end
                if numel(find(~isnan(data)))>1
                    Values(i,:) = interp1(ModelDaten.T(~isnan(data)),data(~isnan(data)),TEMPSs,methode,'extrap');
                end
            end
            for i = 1:numel(TEMPSs)
                set(app.axes9,'YScale','lin')
                plot(app.axes9,ModelDaten.C.Values,Values(:,i),'o','Color',colors(i,:))
                hold(app.axes9,'on')
            end  
            for i = 1:numel(TEMPSs)
                if numel(find(~isnan(Values(:,i))))>1                    
                    x_hd = min(ModelDaten.C.Values):0.1:max(ModelDaten.C.Values);
                    y_hd = interp1(ModelDaten.C.Values,Values(:,i),x_hd,methode);
                    plot(app.axes9,x_hd',y_hd,'Color',colors(i,:))
                end
            end
            x_label =  'I in C' ;
            h9=legend(app.axes9,num2str(TEMPSs'));
            set(h9,'Location','NorthEast')
            %plot(repmat(ModelDaten.T',[numel(ModelDaten.SOC) 1])',ModelDaten.Parameter,'bo')
            grid(app.axes9,'on')
            app.axes9.XLabel.String=x_label;app.axes9.XLabel.Interpreter='latex';
            app.axes9.YLabel.String='';app.axes9.YLabel.Interpreter='latex';
        end
    end
end

%if C rate dependence was parameterized in the TDF amke plot Panel visible

end