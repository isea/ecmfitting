output_folder = 'C:\Data\GitRepos\autofit\output';
old_path = cd;
cd(output_folder)
f_bat = dir;
for nbat = 27:length(f_bat)
    f = dir([output_folder '/' f_bat(nbat).name]);
    for n1 = 3:length(f)
       f1 = dir([output_folder '/' f_bat(nbat).name '/' f(n1).name]);
       for n2 = 3:length(f1)
           f2 = dir([output_folder '/' f_bat(nbat).name '/' f(n1).name '/' f1(n2).name '/*_Modell.mat']);
           for n3 = 1:length(f2)
               load([output_folder '/' f_bat(nbat).name '/' f(n1).name '/' f1(n2).name '/' f2(n3).name])
               load([output_folder '/' f_bat(nbat).name '/' f(n1).name '/' f1(n2).name '/' f2(n3).name(1:end-11) '.mat'])
               DRT_GUI.FR_Fit = Fit;
               save([output_folder '/' f_bat(nbat).name '/' f(n1).name '/' f1(n2).name '/' f2(n3).name(1:end-11) '.mat'],'DRT_GUI')
               delete([output_folder '/' f_bat(nbat).name '/' f(n1).name '/' f1(n2).name '/' f2(n3).name])
               clear DRT_GUI 
               clear Fit
           end
       end
    end
end