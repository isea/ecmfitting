function app = fcn_FR_aktualisieren_button(app,event)
Folders = {''};
f = dir('output/*');
for i = 1:numel(f)
    if ~strcmp(f(i).name,'.') && ~strcmp(f(i).name,'..') && f(i).isdir
        Folders = [Folders;f(i).name];
    end
end
if app.GUI
    set(app.FR_BatterieNamePopup,'Items',Folders);
    set(app.FR_BatterieNamePopup,'Value',app.FR_BatterieNamePopup.Items{1});
else
    app.FR_BatterieNamePopup.Items = Folders;
    app.FR_BatterieNamePopup.Value = app.FR_BatterieNamePopup.Items{1};    
end
app = fcn_FR_BatterieNamePopup(app,event);
end