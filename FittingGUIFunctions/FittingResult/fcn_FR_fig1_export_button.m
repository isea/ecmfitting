function fcn_FR_fig1_export_button(app,event)
global DRT_GUI
AltePosition = get(app.axes8,'InnerPosition');
AltePosition(1:2)=5;
AltePosition2=[AltePosition(1:2),AltePosition(3:4).*2+10];
newfig=figure('Units','pixels','Position',AltePosition2); %'Position',AltePosition+10,
plot(1,1);
neueAchse = gca;
cNames=fieldnames(app.axes8);
cValues=get(app.axes8,cNames);
%neueAchse=axes('Parent',newfig);
% for ix1=1:length(cNames)
%     try 
%         set(neueAchse,cNames{ix1},cValues{ix1})
%     catch
%     end
% end
plot(-rand(length(app.axes8.Children))*1e-3,'Parent',neueAchse);
for ix2=1:length(app.axes8.Children)
    cNames2=fieldnames(app.axes8.Children(ix2));
    cValues2=get(app.axes8.Children(ix2),cNames2);
    for ix3=1:length(cNames2)
        if strcmp(cNames2{ix3},'Parent'), continue; end
        try
            set(neueAchse.Children(ix2),cNames2{ix3},cValues2{ix3})
        catch
        end
    end
end
%set(neueAchse,'OuterPosition',AltePosition2)
h1=legend();
cNames3=fieldnames(app.axes8.Legend);
cValues3=get(app.axes8.Legend,cNames3);
for ix4=1:length(cNames3)
    try 
        set(h1,cNames3{ix4},cValues3{ix4})
    catch
    end
end
set(h1,'Location','NorthWest')
app.axes8.XLabel.String='SOC in %';app.axes8.XLabel.Interpreter='latex';
app.axes8.YLabel.String='';app.axes8.YLabel.Interpreter='latex';
end