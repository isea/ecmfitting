function fcn_FR_export_Lookups_button(app,event)

global ModelDaten
Batterien = get(app.FR_BatterieNamePopup,'Items');
Zustaende = get(app.FR_ZustandPopup,'Items');
if strcmp(get(app.FR_BatterieNamePopup,'Value'),app.FR_BatterieNamePopup.Items{1}) || strcmp(get(app.FR_ZustandPopup,'Value'),app.FR_ZustandPopup.Items{1}), return; end
if isempty(ModelDaten) || ~sum(ismember(fieldnames(ModelDaten),'Model')) || isempty(ModelDaten.Model), return, end
SOC = -5:5:105;
Fit={};
for i = find(~cellfun(@isempty,ModelDaten.Model(:)))'
    [T_i,SOC_i] = ind2sub(size(ModelDaten.Model),i);
    
    if ~isempty(ModelDaten.Model{i}.Fit) && sum(ismember(fieldnames(ModelDaten.Model{i}),'Fit')) && sum(ismember(fieldnames(ModelDaten.Model{i}.Fit),'gueltig')) && ModelDaten.Model{i}.Fit.gueltig
        if isempty(Fit) || ~sum(ismember(Fit(:,1),ModelDaten.Model{i}.Fit.Modell.Modellname))
            Fit = [Fit ; ModelDaten.Model{i}.Fit.Modell.ModellCell {ModelDaten.Model{i}.Fit.Modell.P_Name(1,:)} {ModelDaten.T} {reshape(ModelDaten.SOC,[1 numel(ModelDaten.SOC)])} {nan(numel(ModelDaten.T), numel(ModelDaten.SOC), numel(ModelDaten.Model{i}.Fit.Parameter))} {ModelDaten.T} {SOC} {nan(numel(ModelDaten.T), numel(SOC), numel(ModelDaten.Model{i}.Fit.Parameter))}];
            if iscell(Fit{size(Fit,1),4}), Fit{size(Fit,1),4}=cell2mat(Fit{size(Fit,1),4});end
            if iscell(Fit{size(Fit,1),5}), Fit{size(Fit,1),5}=cell2mat(Fit{size(Fit,1),5});end
            if iscell(Fit{size(Fit,1),6}), Fit{size(Fit,1),6}=cell2mat(Fit{size(Fit,1),6});end
            if iscell(Fit{size(Fit,1),7}), Fit{size(Fit,1),7}=logical(cell2mat(Fit{size(Fit,1),7}));end
        end
        Fit_index = find(ismember(Fit(:,1),ModelDaten.Model{i}.Fit.Modell.Modellname));
        for i_p = 1:numel(Fit{Fit_index,11}(T_i,SOC_i,:))
            ParName = Fit{8}(i_p);
            ParNameIndex = find( strcmp(ModelDaten.Model{i}.Fit.Modell.P_Name(1,:) , ParName),1);
            if  isempty(ParNameIndex) , continue , end
            Fit{Fit_index,11}(T_i,SOC_i,i_p) = ModelDaten.Model{i}.Fit.Parameter(ParNameIndex);
            if ModelDaten.Model{i}.Fit.Parameter_min(ParNameIndex)<Fit{Fit_index,5}(i_p)
                Fit{Fit_index,5}(i_p)=ModelDaten.Model{i}.Fit.Parameter_min(ParNameIndex);
            end
            if ModelDaten.Model{i}.Fit.Parameter_max(ParNameIndex)>Fit{Fit_index,6}(i_p)
                Fit{Fit_index,6}(i_p)=ModelDaten.Model{i}.Fit.Parameter_max(ParNameIndex);
            end
        end
        
    end
    
end
Fit = cell2struct(Fit,{'name','formel','P_name_string','P_init','P_min','P_max','P_fix','P_namen','T_mess','SOC_mess','P_fit','T_lookup','SOC_lookup','P_lookup'},2);
for i = 1:numel(Fit)
    min_SOC = Fit(i).SOC_mess(find(sum(isnan(Fit(i).P_fit(:,:,1)),1)>0,1,'first'));
    max_SOC = Fit(i).SOC_mess(find(sum(isnan(Fit(i).P_fit(:,:,1)),1)>0,1,'last'));
    %Fit(i).SOC_lookup = SOC(find(SOC<=min_SOC,1,'last')):5:SOC(find(SOC>=max_SOC,1,'first'));
    Fit(i).T_lookup = [Fit(i).T_mess(sum(~isnan(Fit(i).P_fit(:,:,1)),2)>0)];
    Fit(i).P_lookup = nan(numel(Fit(i).T_lookup)+1,numel(Fit(i).SOC_lookup),size(Fit(i).P_fit,3));
    for T_i = 1:numel(Fit(i).T_lookup)
        T_index = find(Fit(i).T_mess == Fit(i).T_lookup(T_i));
        for P_i = 1:size(Fit(i).P_fit,3)
            SOC_index = find(~isnan(Fit(i).P_fit(T_index,:,P_i)));
            if ~isempty(SOC_index)
                Fit(i).P_lookup(T_i,:,P_i) = interp1(Fit(i).SOC_mess(SOC_index),Fit(i).P_fit(T_index,SOC_index,P_i),Fit(i).SOC_lookup,'linear','extrap');
                Fit(i).P_lookup(T_i,Fit(i).P_lookup(T_i,:,P_i)<Fit(i).P_min(P_i),P_i) = Fit(i).P_min(P_i);
                Fit(i).P_lookup(T_i,Fit(i).P_lookup(T_i,:,P_i)>Fit(i).P_max(P_i),P_i) = Fit(i).P_max(P_i);
            end
        end
    end
    if numel(find(isnan(Fit(i).P_lookup(:)))) == numel(Fit(i).P_lookup(:)) , continue,end
    
    Fit(i).T_lookup = [Fit(i).T_lookup ;135];
    for P_i = 1:size(Fit(i).P_fit,3)
        for SOC_i = 1:numel(Fit(i).SOC_lookup)
            if numel(Fit(i).T_lookup(1:end-1)) ==1
                Fit(i).P_lookup(end,SOC_i,P_i) = Fit(i).P_lookup(1:end-1,SOC_i,P_i);
            else
                Fit(i).P_lookup(end,SOC_i,P_i) = interp1(Fit(i).T_lookup(1:end-1),Fit(i).P_lookup(1:end-1,SOC_i,P_i),Fit(i).T_lookup(end),'linear','extrap');
            end
        end
        Fit(i).P_lookup(end,Fit(i).P_lookup(end,:,P_i)<Fit(i).P_min(P_i),P_i) = Fit(i).P_min(P_i);
        Fit(i).P_lookup(end,Fit(i).P_lookup(end,:,P_i)>Fit(i).P_max(P_i),P_i) = Fit(i).P_max(P_i);
    end
end

filename = ['output/' get(app.BatterieNamePopup,'Value') '/'  get(app.ZustandPopup,'Value') '/'  get(app.BatterieNamePopup,'Value') '_' get(app.ZustandPopup,'Value')];
save( [filename '_modelexport.mat'],'Fit');
if ~isempty(dir([filename '_modelexport.xls']))
    delete([filename '_modelexport.xls']);
end


xlsCell = {};
maxCols=max(arrayfun(@(x) size(x.SOC_lookup,2),Fit))+1;
for i = 1:numel(Fit)
    if numel(find(isnan(Fit(i).P_lookup(:)))) == numel(Fit(i).P_lookup(:)) , continue,end
    
    xlsCell = [xlsCell ; Fit(i).name cell(1,maxCols-1)];
    for j = 1:numel(Fit(i).P_namen)
        xlsCell = [xlsCell ; Fit(i).P_namen(j) cell(1,maxCols-1)];
        xlsCell = [xlsCell ; [cell(1,1) num2cell(Fit(i).SOC_lookup) cell(1,maxCols-1-numel(Fit(i).SOC_lookup))]];
        xlsCell = [xlsCell ; num2cell(Fit(i).T_lookup) num2cell(Fit(i).P_lookup(:,:,j)) cell(numel(Fit(i).T_lookup),maxCols-1-numel(Fit(i).SOC_lookup))];
    end
end
xlswrite([filename '_modelexport.xls'],xlsCell,'Fit')
display(sprintf('In Datei %s geschrieben',[filename '_modelexport.xls']));
end