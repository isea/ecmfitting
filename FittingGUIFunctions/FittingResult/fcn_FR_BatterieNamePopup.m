function app = fcn_FR_BatterieNamePopup(app,event)
if strcmpi(app.BatterieNamePopup.Value,'empty')
    warning('no cell in output folder')
    return
end


global ModelDaten
global DRT_GUI
output_folder = cd;
output_folder = [output_folder '/' 'output' '/' get(app.FR_BatterieNamePopup,'Value')];
old_path = cd;
cd(output_folder)
f = dir;
for n1 = 3:length(f)
   f1 = dir([output_folder '/' f(n1).name]);
   for n2 = 3:length(f1)
       f2 = dir([output_folder '/' f(n1).name '/' f1(n2).name '/*_Modell.mat']);
       for n3 = 1:length(f2)
           load([output_folder '/' f(n1).name '/' f1(n2).name '/' f2(n3).name])
           load([output_folder '/' f(n1).name '/' f1(n2).name '/' f2(n3).name(1:end-11) '.mat'])
           DRT_GUI.FR_Fit = Fit;
           save([output_folder '/' f(n1).name '/' f1(n2).name '/' f2(n3).name(1:end-11) '.mat'],'DRT_GUI')
           delete([output_folder '/' f(n1).name '/' f1(n2).name '/' f2(n3).name])
           clear DRT_GUI 
           clear Fit
       end
   end
end
cd(old_path)
if app.GUI
    Batterien = get(app.FR_BatterieNamePopup,'Items');
    if isempty(Batterien) || numel(Batterien) == 1 ,return,end
    if strcmp(get(app.FR_BatterieNamePopup,'Value'),Batterien{1}) && ~isempty(DRT_GUI) &&...
            sum(ismember(fieldnames(DRT_GUI),'Testparameter')) && ~isempty(DRT_GUI.Testparameter) &&...
            sum(ismember(fieldnames(DRT_GUI.Testparameter),'Batterie')) && ~isempty(DRT_GUI.Testparameter.Batterie)
        if ~isempty(find(ismember(Batterien,DRT_GUI.Testparameter.Batterie)))
            set(app.FR_BatterieNamePopup,'Value',Batterien{find(ismember(Batterien,DRT_GUI.Testparameter.Batterie))})
        end
    end
    Folders = {''};
    Values = [];
    if ~strcmp(get(app.FR_BatterieNamePopup,'Value'),Batterien{1})
        f = dir(['output/' get(app.FR_BatterieNamePopup,'Value') '/*']);
        for i = 1:numel(f)
            if ~strcmp(f(i).name,'.') && ~strcmp(f(i).name,'..') && f(i).isdir
                Folders = [Folders;f(i).name];
                Values = [Values; str2num(strrep(strrep(f(i).name(1:end-4),'m','-'),'_','.'))];
            end
        end
    end

    set(app.FR_ZustandPopup,'Items',Folders);
    set(app.FR_ZustandPopup,'Value',app.FR_ZustandPopup.Items{1});
else
    Batterien = get(app.FR_BatterieNamePopup,'Items');
    if isempty(Batterien) || numel(Batterien) == 1 ,return,end
    if strcmp(app.FR_BatterieNamePopup.Value,Batterien{1}) && ~isempty(DRT_GUI) &&...
            sum(ismember(fieldnames(DRT_GUI),'Testparameter')) && ~isempty(DRT_GUI.Testparameter) &&...
            sum(ismember(fieldnames(DRT_GUI.Testparameter),'Batterie')) && ~isempty(DRT_GUI.Testparameter.Batterie)
        if ~isempty(find(ismember(Batterien,DRT_GUI.Testparameter.Batterie)))
            app.FR_BatterieNamePopup.Value=Batterien{find(ismember(Batterien,DRT_GUI.Testparameter.Batterie))};
        end
    end
    Folders = {''};
    if ~strcmp(app.FR_BatterieNamePopup.Value,Batterien{1})
        f = dir(['output/' get(app.FR_BatterieNamePopup,'Value') '/*']);
        for i = 1:numel(f)
            if ~strcmp(f(i).name,'.') && ~strcmp(f(i).name,'..') && f(i).isdir
                Folders = [Folders;f(i).name];
            end
        end
    end
    app.FR_ZustandPopup.Items = Folders;
    app.FR_ZustandPopup.Value = app.FR_ZustandPopup.Items{1};    
end



ModelDaten.C.Folders = Folders;
[sorted_values,idx_sort_values] = sort(Values);
ModelDaten.C.Values = sorted_values;
ModelDaten.C.Index = idx_sort_values;
[~,ModelDaten.C.actIndex] = min(abs(ModelDaten.C.Values));
app = fcn_FR_ZustandPopup(app,event);
end