function app = fcn_FR_ExportParameterSurfacePlotFcn(app,event)
global ModelDaten
global DRT_GUI
gui_objects = fieldnames(app);
Par_Radio = gui_objects(find(~cellfun(@isempty,strfind(gui_objects,'Par')) & ~cellfun(@isempty,strfind(gui_objects,'Radio'))));
if app.GUI
    ModelName = get(app.FR_ModellnamePopup,'Items');
    ModelName =  get(app.FR_ModellnamePopup,'Value');
    if get(app.FR_spline_check,'Value')
        methode = 'spline';
    else
        methode =  'linear';
    end
else
    ModelName = app.FR_ModellnamePopup.Items;
    ModelName =  app.FR_ModellnamePopup.Value;
    if app.FR_spline_check.Value
        methode = 'spline';
    else
        methode =  'linear';
    end
end
for i = 1:numel(Par_Radio)
    if app.GUI
        if get(app.(Par_Radio{i}),'Value')
            if strcmp(get(app.(Par_Radio{i}),'Visible'),'off')
                app.Par1Radio.Value=1;
                app.Par_Radio{i}.Value=0;
                ParNummer = find(~cellfun(@isempty,strfind(gui_objects,'Par1Radio')));
                ModelDaten.ParName = get(app.('Par1Radio'),'Text');

            else
                ParNummer = i;
                ModelDaten.ParName=get(app.(Par_Radio{i}),'Text');
            end
            break

        end
    else
        if app.(Par_Radio{i}).Value
            if strcmp(app.(Par_Radio{i}).Visible,'off')
                app.Par1Radio.Value=1;
                app.Par_Radio{i}.Value=0;
                ParNummer = find(~cellfun(@isempty,strfind(gui_objects,'Par1Radio')));
                ModelDaten.ParName = app.('Par1Radio').Text;

            else
                ParNummer = i;
                ModelDaten.ParName = app.(Par_Radio{i}).Text;
            end
            break

        end
    end
end

if strcmp(app.DisplayedStatesButtonGroup.SelectedObject.Text,'T-SOC')
    ModelDaten.Parameter = NaN(numel(ModelDaten.T),numel(ModelDaten.SOC));
else
    ModelDaten.Parameter = NaN(numel(ModelDaten.C.Values),numel(ModelDaten.T),numel(ModelDaten.SOC));
end

for i = find(~cellfun(@isempty,ModelDaten.Model(:)))'
    if sum(ismember(fieldnames(ModelDaten.Model{i}),'Fit')) && ~isempty(ModelDaten.Model{i}.Fit) && ...
            strcmp(ModelDaten.Model{i}.Fit.Modell.Modellname,ModelName) && ( ( sum(ismember(fieldnames(ModelDaten.Model{i}.Fit),'gueltig')) && ModelDaten.Model{i}.Fit.gueltig ) || get(app.FR_ungueltige_check,'Value'))
        ParIndex = find(ismember(ModelDaten.Model{i}.Fit.Modell.P_Name(1,:),ModelDaten.ParName));
        if ~isempty(ParIndex)
            ModelDaten.Parameter(i) = ModelDaten.Model{i}.Fit.Parameter(ParIndex);
%         else
%             ModelDaten.Parameter(i) = ModelDaten.Model{i}.Fit.Parameter(2);
        end
        
    end
end

if strcmp(app.DisplayedStatesButtonGroup.SelectedObject.Text,'T-SOC')
    colors = hsv(numel(ModelDaten.T));
else
    colors = hsv(numel(ModelDaten.C.Values));
end

if strcmp(app.DisplayedStatesButtonGroup.SelectedObject.Text,'T-SOC')
    for i =1:numel(ModelDaten.T)
        if numel(find(~isnan(ModelDaten.Parameter(i,:))))>1
            x_hd = [min(ModelDaten.SOC(~isnan(ModelDaten.Parameter(i,:)))):1:max(ModelDaten.SOC(~isnan(ModelDaten.Parameter(i,:))))];
            if get(app.FR_extrapolieren_check,'Value'), x_hd = [0:1:min(x_hd) x_hd max(x_hd):1:100];end
            y_hd = interp1(ModelDaten.SOC(~isnan(ModelDaten.Parameter(i,:))),ModelDaten.Parameter(i,~isnan(ModelDaten.Parameter(i,:))),x_hd,methode,'extrap');
            
        end
    end
    if numel(ModelDaten.T)>0
        SOCs = eval(['[' get(app.FR_SOCText,'Value') ']' ]);
        Values = nan(numel(ModelDaten.T),numel(SOCs));
        colors = hsv(numel(SOCs));
        for i = 1:numel(ModelDaten.T)
            if numel(find(~isnan(ModelDaten.Parameter(i,:))))>1
                Values(i,:) = interp1(ModelDaten.SOC(~isnan(ModelDaten.Parameter(i,:))),ModelDaten.Parameter(i,~isnan(ModelDaten.Parameter(i,:))),SOCs,methode,'extrap');
            end
        end
        for i = 1:numel(SOCs)
            if numel(find(~isnan(Values(:,i))))>1
                if get(app.FR_ArrheniusCheckbox,'Value')
                    x_hd = min(ModelDaten.T):1:max(ModelDaten.T);
                    y_hd = exp(interp1(1./(ModelDaten.T+273.15),log(Values(:,i)),1./(x_hd'+273.15),methode));
                else
                    x_hd = min(ModelDaten.T):1:max(ModelDaten.T);
                    y_hd = interp1(ModelDaten.T,Values(:,i),x_hd,methode);
                end
            end
        end        
    end
    [X,Y] = meshgrid(SOCs,ModelDaten.T);
    f = figure;
    g = surf(X,Y,Values);
    ylabel('Temperature / °C');
    xlabel( 'SOC / %');
    set ( gca, 'xdir', 'reverse' )
    set ( gca, 'ydir', 'reverse' )
elseif strcmp(app.DisplayedStatesButtonGroup.SelectedObject.Text,'C-SOC')
    temp_idx = find(str2num(app.StateDropDown.Value) == ModelDaten.T);    
    if numel(ModelDaten.C.Values)>0
        SOCs = eval(['[' get(app.FR_SOCText,'Value') ']' ]);
        Values = nan(numel(ModelDaten.C.Values),numel(SOCs));
        colors = hsv(numel(SOCs));
        for i = 1:numel(ModelDaten.C.Values)
            for j = 1:length(ModelDaten.SOC)
                data(j) = ModelDaten.Parameter(i,temp_idx,j);
            end
            if numel(find(~isnan(data)))>1
                Values(i,:) = interp1(ModelDaten.SOC(~isnan(data)),data(~isnan(data)),SOCs,methode,'extrap');
            end
        end           
    end
    [X,Y] = meshgrid(SOCs,ModelDaten.C.Values);
    f = figure;
    g = surf(X,Y,Values);
    ylabel('I / C');
    xlabel( 'SOC / %');
    set ( gca, 'xdir', 'reverse' )
    set ( gca, 'ydir', 'reverse' )
else
    soc_idx = find(str2num(app.StateDropDown.Value) == ModelDaten.SOC);    
    if numel(ModelDaten.C.Values)>0
        TEMPSs = ModelDaten.T;
        Values = nan(numel(ModelDaten.C.Values),numel(TEMPSs));
        colors = hsv(numel(TEMPSs));
        for i = 1:numel(ModelDaten.C.Values)
            for j = 1:length(ModelDaten.T)
                data(j) = ModelDaten.Parameter(i,j,soc_idx);
            end
            if numel(find(~isnan(data)))>1
                Values(i,:) = interp1(ModelDaten.T(~isnan(data)),data(~isnan(data)),TEMPSs,methode,'extrap');
            end
        end           
    end
    [X,Y] = meshgrid(ModelDaten.T,ModelDaten.C.Values);
    f = figure;
    g = surf(X,Y,Values);
    ylabel('I / C');
    xlabel( 'T / °C');
    set ( gca, 'xdir', 'reverse' )
    set ( gca, 'ydir', 'reverse' )
end

if isempty(dir('export'))
    mkdir('export')
end
if isempty( dir(['export' '/' DRT_GUI.Testparameter.Batterie]))
    mkdir(['export' '/' DRT_GUI.Testparameter.Batterie])
end
savefig(['export/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie '_' ...
    DRT_GUI.Testparameter.Zustand '_Parameter_'  ModelDaten.ParName{1}...
    '.fig'])
end