function fcn_FR_zustandplotbutton(app,event)
global ModelDaten
SOCs=[];
Temperaturen=[];
Werte={};
ZustandsNummern = {};
Zustandsnamen = get(app.FR_ZustandPopup,'Items');
Zustandsnamen = Zustandsnamen(2:end);
Modellnamenliste = get(app.FR_ModellnamePopup,'Items');
Modellname = get(app.FR_ModellnamePopup,'Value');
ParameterFelder = get(app.FR_ParameterPanel,'Children');

for i = 1:numel(ParameterFelder)
    if get(ParameterFelder(i),'Value')
        ParameterNummer=i;
    end
end
ParameterName = get(ParameterFelder(ParameterNummer),'Text');
for Z_i = 2:numel(Zustandsnamen)+1
    set(app.FR_ZustandPopup,'Value',app.FR_ZustandPopup.Items{Z_i})
    fcn_FR_ZustandPopup_Callback(app,event)
    Modellnamenliste = get(app.FR_ModellnamePopup,'Items');
    if isempty(find(strcmp(Modellnamenliste,Modellname), 1)), continue,end
    set(app.FR_ModellnamePopup,'Value',app.FR_ModellnamePopup.Items{find(strcmp(Modellnamenliste,Modellname), 1)})
    fcn_FR_ModellnamePopup_Callback(app,event)
    ParameterFelder = get(app.FR_ParameterPanel,'Children');
    set(ParameterFelder(ParameterNummer),'Value',1)
    fcn_FR_ParameterPanel_SelectionChangeFcn(app,event)
    for T_i = 1:numel(ModelDaten.T)
        for SOC_i = 1:numel(ModelDaten.SOC)
            if isnan(ModelDaten.Parameter(T_i,SOC_i)),continue, end
            SOC_Wert = 5 * round(ModelDaten.SOC(SOC_i)/5);
            T_Wert = 5 * round(ModelDaten.T(T_i)/5);
            SOC_T_Index = find(SOCs == SOC_Wert & Temperaturen == T_Wert,1);
            if isempty(SOC_T_Index)
                SOC_T_Index = numel(Werte)+1;
                SOCs(SOC_T_Index) = SOC_Wert;
                Temperaturen(SOC_T_Index) = T_Wert;
                Werte{SOC_T_Index} = [];
                ZustandsNummern{SOC_T_Index} = [];
            else
                
            end
            Werte{SOC_T_Index} = [Werte{SOC_T_Index} ModelDaten.Parameter(T_i,SOC_i)];
            ZustandsNummern{SOC_T_Index} = [ZustandsNummern{SOC_T_Index} Z_i-1];
            
        end
    end
    
end
figure('unit','normalized','Position',[0.1,0.1,0.8,0.8]);
cmap=colormap(lines((numel(SOCs))));
[SOCs ix ] = sort(SOCs,2);
ZustandsNummern = ZustandsNummern(ix);
Werte = Werte(ix);

for i = 1:numel(SOCs)
    FarbenIndex = i;
    if (~isempty(find(diff(Temperaturen),1)) && ~isempty(find(diff(SOCs),1))) || numel(SOCs)==1
        LegendenEintrag = sprintf('%0.0f%% SOC %0.0f�C',SOCs(i),Temperaturen(i));
    elseif isempty(find(diff(Temperaturen),1)) && ~isempty(find(diff(SOCs),1))
        LegendenEintrag = sprintf('%0.0f%% SOC',SOCs(i));
    elseif ~isempty(find(diff(Temperaturen),1)) && isempty(find(diff(SOCs),1))
        LegendenEintrag = sprintf('%0.0f�C',Temperaturen(i));
    end
    plot(ZustandsNummern{i},Werte{i},'DisplayName',LegendenEintrag ,'Color',cmap(FarbenIndex,:),'LineStyle','-','Marker','o')
    
    hold on
end
grid on
legend('show','location','northeast')
%xticklabel_rotate([1:numel(Zustandsnamen)],60,Zustandsnamen,'interpreter','none')

Batteriename = get(app.FR_BatterieNamePopup,'Items');
Batteriename = get(app.FR_BatterieNamePopup,'Value');
if isempty(dir('export'))
    mkdir('export')
end
if isempty( dir(['export' '/' Batteriename]))
    mkdir(['export' '/' Batteriename])
end
achsen = get(gcf,'Children');
achsen = achsen(isgraphics(achsen,'axes'));
for j = 1:numel(achsen)
    set(get(achsen(j),'xlabel'),'Fontname','arial','fontsize',15);
    set(get(achsen(j),'ylabel'),'Fontname','arial','fontsize',15);
    set(get(achsen(j),'zlabel'),'Fontname','arial','fontsize',15);
    set(achsen(j),'Fontname','Arial','Fontsize',16)
    linien = get(achsen(j),'Children');
    linien = linien(isgraphics(linien,'line'));
    for li = 1:numel(linien),
        if isgraphics(linien(li),'line')
            set(linien(li),'linewidth',2)
        end
    end
end
print(gcf,['export' '/' Batteriename '/' ParameterName{1} '.png'], '-dpng', '-r900');
title(ParameterName)
saveas(gcf,['export' '/' Batteriename '/' ParameterName{1} '.fig'])




if ~isempty(dir(['output/' Batteriename '/Zustandaliases.mat']))
    Zustandaliases = load(['output/' Batteriename '/Zustandaliases.mat']);
    Zustandaliases = Zustandaliases.Zustandaliases;
elseif ~isempty(dir(['output/' Batteriename '/Zustandaliases.xlsx']))
    [Zustandndata Zustandtext ZustandXLS] = xlsread(['output/' Batteriename '/Zustandaliases.xlsx']);
    for i = 2:size(ZustandXLS,2)
        Zustandaliases{1,i-1}.name=Zustandtext{1,i};
        Zustandaliases{1,i-1}.unit=Zustandtext{2,i};
        Zustandaliases{1,i-1}.value=cell2mat(ZustandXLS(3:end,i));
        Zustandaliases{1,i-1}.Zustands_name=ZustandXLS(3:end,1);
    end
elseif ~isempty(dir(['output/' Batteriename '/Zustandaliases.xls']))
    [Zustandndata Zustandtext ZustandXLS] = xlsread(['output/' Batteriename '/Zustandaliases.xlsx']);
    for i = 2:size(ZustandXLS,2)
        Zustandaliases{1,i-1}.name=Zustandtext{1,i};
        Zustandaliases{1,i-1}.unit=Zustandtext{2,i};
        Zustandaliases{1,i-1}.value=cell2mat(ZustandXLS(3:end,i));
        Zustandaliases{1,i-1}.Zustands_name=ZustandXLS(3:end,1);
    end
end
if ~isempty(dir(['output/' Batteriename '/Zustandaliases.mat'])) || ~isempty(dir(['output/' Batteriename '/Zustandaliases.xls'])) ||~isempty(dir(['output/' Batteriename '/Zustandaliases.xlsx']))
    
    for Z_i = 1:numel(Zustandaliases)
        cmap=colormap(lines((numel(SOCs))));
        figure;
        for SOC_T_Index = 1:numel(Werte)
            WerteNeu{SOC_T_Index} = [];
            ZustandsNummernNeu{SOC_T_Index} = [];
            for W_i = 1:numel(Zustandaliases{Z_i}.Zustands_name)
                
                AlteNummer = find(strcmp(Zustandsnamen,    Zustandaliases{Z_i}.Zustands_name{W_i}));
                if ~isempty(AlteNummer)
                    Alter_index = find(ZustandsNummern{SOC_T_Index}==AlteNummer,1,'first');
                    if ~isempty(Alter_index)
                        WerteNeu{SOC_T_Index} = [WerteNeu{SOC_T_Index} Werte{SOC_T_Index}(Alter_index)];
                        if iscell(Zustandaliases{Z_i}.value)
                            ZustandsNummernNeu{SOC_T_Index} = [ZustandsNummernNeu{SOC_T_Index} W_i];
                        else
                            ZustandsNummernNeu{SOC_T_Index} = [ZustandsNummernNeu{SOC_T_Index} Zustandaliases{Z_i}.value(W_i)];
                        end
                    end
                end
            end
            FarbenIndex = SOC_T_Index;
            if (~isempty(find(diff(Temperaturen),1)) && ~isempty(find(diff(SOCs),1))) || numel(SOCs)==1
                LegendenEintrag = sprintf('%0.0f%% SOC %0.0f�C',SOCs(SOC_T_Index),Temperaturen(SOC_T_Index));
            elseif isempty(find(diff(Temperaturen),1)) && ~isempty(find(diff(SOCs),1))
                LegendenEintrag = sprintf('%0.0f%% SOC',SOCs(SOC_T_Index));
            elseif ~isempty(find(diff(Temperaturen),1)) && isempty(find(diff(SOCs),1))
                LegendenEintrag = sprintf('%0.0f�C',Temperaturen(SOC_T_Index));
            end
            plot(ZustandsNummernNeu{SOC_T_Index},WerteNeu{SOC_T_Index},'DisplayName',LegendenEintrag ,'Color',cmap(FarbenIndex,:),'LineStyle','-','Marker','o')
            
            hold on
        end
        
        ylabel(ParameterName)
        if isempty(Zustandaliases{Z_i}.unit)
            xlabel(Zustandaliases{Z_i}.name)
        else
            xlabel([Zustandaliases{Z_i}.name ' in ' Zustandaliases{Z_i}.unit])
        end
        if strcmpi(Zustandaliases{Z_i}.name,'soh'),
            set(gca,'xdir','reverse')
        end
        grid on
        legend('show','location','northwest')
        if iscell(Zustandaliases{Z_i}.value)
            xticklabel_rotate([1:numel(Zustandaliases{Z_i}.value)],60,Zustandaliases{Z_i}.value,'interpreter','none')
        end
        achsen = get(gcf,'Children');
        achsen = achsen(isgraphics(achsen,'axes'));
        for j = 1:numel(achsen)
            set(get(achsen(j),'xlabel'),'Fontname','arial','fontsize',15);
            set(get(achsen(j),'ylabel'),'Fontname','arial','fontsize',15);
            set(get(achsen(j),'zlabel'),'Fontname','arial','fontsize',15);
            set(achsen(j),'Fontname','Arial','Fontsize',16)
            linien = get(achsen(j),'Children');
            linien = linien(isgraphics(linien,'line'));
            for li = 1:numel(linien),
                if isgraphics(linien(li),'line')
                    set(linien(li),'linewidth',2)
                end
            end
        end
        %         print(gcf,['export' '/' Batteriename '/' ParameterName{1} ' ' Zustandaliases{Z_i}.name '.png'], '-dpng', '-r900');
        title(ParameterName)
        saveas(gcf,['export' '/' Batteriename '/' ParameterName{1} ' ' Zustandaliases{Z_i}.name '.fig'])
        
        
    end
end
end