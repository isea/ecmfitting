function fcn_FR_allSOCButton(app,event)
Batterien = get(app.FR_BatterieNamePopup,'Items');
Zustaende = get(app.FR_ZustandPopup,'Items');
if strcmp(get(app.FR_BatterieNamePopup,'Value'),Batterien{1}) || strcmp(get(app.FR_ZustandPopup,'Value'),Zustaende{1}), return; end
SOCData = get(app.FR_SOCTable,'Data');
SOCData(:,2) = {true};
set(app.FR_SOCTable,'Data',SOCData);
end