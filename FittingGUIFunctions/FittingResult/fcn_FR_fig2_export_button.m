function fcn_FR_fig2_export_button(app,event)
global DRT_GUI
AltePosition = get(app.axes9,'InnerPosition');
AltePosition(1:2)=5;
AltePosition2=[AltePosition(1:2),AltePosition(3:4).*2+10];
newfig=figure('Units','pixels','Position',AltePosition2); %'Position',AltePosition+10,
plot(1,1);
neueAchse = gca;
cNames=fieldnames(app.axes9);
cValues=get(app.axes9,cNames);
%neueAchse=axes('Parent',newfig);
% for ix1=1:length(cNames)
%     try 
%         set(neueAchse,cNames{ix1},cValues{ix1})
%     catch
%     end
% end
plot(-rand(length(app.axes9.Children))*1e-3,'Parent',neueAchse);
for ix2=1:length(app.axes9.Children)
    cNames2=fieldnames(app.axes9.Children(ix2));
    cValues2=get(app.axes9.Children(ix2),cNames2);
    for ix3=1:length(cNames2)
        if strcmp(cNames2{ix3},'Parent'), continue; end
        try
            set(neueAchse.Children(ix2),cNames2{ix3},cValues2{ix3})
        catch
        end
    end
end
%set(neueAchse,'OuterPosition',AltePosition2)
h1=legend();
cNames3=fieldnames(app.axes9.Legend);
cValues3=get(app.axes9.Legend,cNames3);
for ix4=1:length(cNames3)
    try 
        set(h1,cNames3{ix4},cValues3{ix4})
    catch
    end
end
set(h1,'Location','NorthWest')
if get(app.FR_ArrheniusCheckbox,'Value')
    x_label =  ' 1/T in 1/K';
else
    x_label =  'Temperature in �C' ;
end
app.axes9.XLabel.String=x_label;app.axes9.XLabel.Interpreter='latex';
app.axes9.YLabel.String='';app.axes9.YLabel.Interpreter='latex';
end