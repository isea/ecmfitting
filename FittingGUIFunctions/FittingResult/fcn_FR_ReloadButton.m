function app = fcn_FR_ReloadButton(app,event)
global ModelDaten
% gui_objects = fieldnames(app);
if strcmp(app.DisplayedStatesButtonGroup.SelectedObject.Text,'T-SOC')
    if app.GUI
        Batterien = get(app.FR_BatterieNamePopup,'Items');
        Zustaende = get(app.FR_ZustandPopup,'Items');
        if strcmp(get(app.FR_BatterieNamePopup,'Value'),Batterien{1}) || strcmp(get(app.FR_ZustandPopup,'Value'),Zustaende{1}), return; end
        TemperaturData = get(app.FR_TemperaturTable,'Data');
    else
        Batterien = app.FR_BatterieNamePopup.Items;
        Zustaende = app.FR_ZustandPopup.Items;
        if strcmp(app.FR_BatterieNamePopup.Value,Batterien{1}) || strcmp(app.FR_ZustandPopup.Value,Zustaende{1}), return; end
        TemperaturData = app.FR_TemperaturTable.Data;
    end
    if sum([TemperaturData{:,2}])==0
        msgbox('Keine Temperatur ausgewählt ...')
        return
    end
    if ~isempty(TemperaturData)
    ModelDaten.T = cell2mat(TemperaturData(:,1));
    ModelDaten.T = ModelDaten.T(cell2mat(TemperaturData(:,2)));
    ModelDaten.T = sort(ModelDaten.T,2);
    end
    if app.GUI
        SOCData = get(app.FR_SOCTable,'Data');
    else
        SOCData = app.FR_SOCTable.Data;
    end
    if ~isempty(SOCData)
        ModelDaten.SOC=cell2mat(SOCData(:,1));
        ModelDaten.SOC = ModelDaten.SOC(cell2mat(SOCData(:,2)));
        ModelDaten.SOC = sort(ModelDaten.SOC,2);
        ModelDaten.Model = cell(numel(ModelDaten.T),numel(ModelDaten.SOC));
    end
    for i = 1:numel(ModelDaten.T)
        for j = 1:numel(ModelDaten.SOC)
            TString = [strrep(num2str(ModelDaten.T(i)),'-','m') 'grad'];
            SOCString = [strrep(num2str(ModelDaten.SOC(j)),'-','m') 'SOC'];
            if app.GUI
                f_alt = dir(['output/' get(app.FR_BatterieNamePopup,'Value') '/'  get(app.FR_ZustandPopup,'Value') '/' TString '/*_' SOCString '_Modell.mat']);                
                f = dir(['output/' get(app.FR_BatterieNamePopup,'Value') '/'  get(app.FR_ZustandPopup,'Value') '/' TString '/*_' SOCString '.mat']);
                if ~isempty(f_alt)                                
                    ModelDaten.Model{i,j} = load(['output/' get(app.FR_BatterieNamePopup,'Value') '/'  get(app.FR_ZustandPopup,'Value') '/' TString '/' f_alt.name]);                        
                elseif ~isempty(f)  
                    var = load(['output/' get(app.FR_BatterieNamePopup,'Value') '/'  get(app.FR_ZustandPopup,'Value') '/' TString '/' f.name]);
                    ModelDaten.Model{i,j}.Fit = var.DRT_GUI.FR_Fit;
                    clear var                    
                end
            else
                f_alt = dir(['output/' app.FR_BatterieNamePopup.Value '/'  app.FR_ZustandPopup.Value '/' TString '/*_' SOCString '_Modell.mat']);                
                f = dir(['output/' app.FR_BatterieNamePopup.Value '/'  app.FR_ZustandPopup.Value '/' TString '/*_' SOCString '.mat']);
                if ~isempty(f_alt)                    
                    ModelDaten.Model{i,j} = load(['output/' app.FR_BatterieNamePopup.Value '/'  app.FR_ZustandPopup.Value '/' TString '/' f.name]);
                elseif ~isempty(f)  
                    var = load(['output/' app.FR_BatterieNamePopup.Value '/'  app.FR_ZustandPopup.Value '/' TString '/' f.name]);
                    ModelDaten.Model{i,j}.Fit = var.DRT_GUI.FR_Fit;
                    clear var
                end
            end
        end
    end
    if isempty(ModelDaten) || ~sum(ismember(fieldnames(ModelDaten),'Model')), return,end
    ModellnameStrings = {};
    for i = 1:numel(ModelDaten.Model)
        if ~isempty( ModelDaten.Model{i})
            if sum(ismember(fieldnames(ModelDaten.Model{i}),'Fit')) && ~isempty(ModelDaten.Model{i}.Fit)
                try
                if ~sum(ismember(ModellnameStrings,ModelDaten.Model{i}.Fit.Modell.Modellname))
                    ModellnameStrings = [ ModellnameStrings ; ModelDaten.Model{i}.Fit.Modell.Modellname ];
                end
                catch 
                end
            end

        end
    end
    if app.GUI
        alter_ModellName = get(app.FR_ModellnamePopup,'Items');
        if ~isempty(alter_ModellName) , alter_ModellName = get(app.FR_ModellnamePopup,'Value');end
    else
        alter_ModellName = app.FR_ModellnamePopup.Items;
        if ~isempty(alter_ModellName) , alter_ModellName = app.FR_ModellnamePopup.Value;end
    end
    modellindex = find(ismember(ModellnameStrings,alter_ModellName));
    if isempty(modellindex),modellindex=1;end
    if app.GUI
        set(app.FR_ModellnamePopup,'Items',ModellnameStrings)
        set(app.FR_ModellnamePopup,'Value',app.FR_ModellnamePopup.Items{modellindex})
    else
        app.FR_ModellnamePopup.Items = ModellnameStrings;
        app.FR_ModellnamePopup.Value = app.FR_ModellnamePopup.Items{modellindex};
    end
elseif strcmp(app.DisplayedStatesButtonGroup.SelectedObject.Text,'C-SOC') || strcmp(app.DisplayedStatesButtonGroup.SelectedObject.Text,'C-T')
    ModelDaten.Model = cell(numel(ModelDaten.C.Values),numel(ModelDaten.T),numel(ModelDaten.SOC));
    for k = 1:numel(ModelDaten.C.Values)
        for i = 1:numel(ModelDaten.T)
            for j = 1:numel(ModelDaten.SOC)
                CString = [strrep(strrep(num2str(ModelDaten.C.Values(k)),'-','m'),'.','_') 'C_DC'];
                TString = [strrep(num2str(ModelDaten.T(i)),'-','m') 'grad'];
                SOCString = [strrep(num2str(ModelDaten.SOC(j)),'-','m') 'SOC'];
                if app.GUI
%                     f = dir(['output/' get(app.FR_BatterieNamePopup,'Value') '/'  CString '/' TString '/*_' SOCString '_Modell.mat']);
                    f = dir(['output/' get(app.FR_BatterieNamePopup,'Value') '/'  CString '/' TString '/*_' SOCString '.mat']);
                    if ~isempty(f)
                        var = load(['output/' get(app.FR_BatterieNamePopup,'Value') '/'  CString '/' TString '/' f.name]);
                        ModelDaten.Model{k,i,j}.Fit = var.DRT_GUI.FR_Fit;
                        clear var
                    end
                else
%                     f = dir(['output/' app.FR_BatterieNamePopup.Value '/'  CString '/' TString '/*_' SOCString '_Modell.mat']);
                    f = dir(['output/' get(app.FR_BatterieNamePopup,'Value') '/'  CString '/' TString '/*_' SOCString '.mat']);
                    if ~isempty(f)
                        var = load(['output/' app.FR_BatterieNamePopup.Value '/'  CString '/' TString '/' f.name]);
                        ModelDaten.Model{k,i,j} = var.DRT_GUI.FR_Fit;
                        clear var
                    end
                end
            end
        end
    end
    if isempty(ModelDaten) || ~sum(ismember(fieldnames(ModelDaten),'Model')), return,end
    ModellnameStrings = {};
    for i = 1:numel(ModelDaten.Model)
        if ~isempty( ModelDaten.Model{i})
            if sum(ismember(fieldnames(ModelDaten.Model{i}),'Fit')) && ~isempty(ModelDaten.Model{i}.Fit)
                if ~sum(ismember(ModellnameStrings,ModelDaten.Model{i}.Fit.Modell.Modellname))
                    ModellnameStrings = [ ModellnameStrings ; ModelDaten.Model{i}.Fit.Modell.Modellname ];
                end
            end

        end
    end
    if app.GUI
        alter_ModellName = get(app.FR_ModellnamePopup,'Items');
        if ~isempty(alter_ModellName) , alter_ModellName = get(app.FR_ModellnamePopup,'Value');end
    else
        alter_ModellName = app.FR_ModellnamePopup.Items;
        if ~isempty(alter_ModellName) , alter_ModellName = app.FR_ModellnamePopup.Value;end
    end
    modellindex = find(ismember(ModellnameStrings,alter_ModellName));
    if isempty(modellindex),modellindex=1;end
    if app.GUI
        set(app.FR_ModellnamePopup,'Items',ModellnameStrings)
        set(app.FR_ModellnamePopup,'Value',app.FR_ModellnamePopup.Items{modellindex})
    else
        app.FR_ModellnamePopup.Items = ModellnameStrings;
        app.FR_ModellnamePopup.Value = app.FR_ModellnamePopup.Items{modellindex};
    end
end
app = fcn_FR_ModellnamePopup(app,event);
end