function app = fcn_FR_ZustandPopup(app,event)
global DRT_GUI
global ModelDaten
if app.GUI
    Batterien = get(app.FR_BatterieNamePopup,'Items');
    Zustaende = get(app.FR_ZustandPopup,'Items');
    if strcmp(get(app.FR_ZustandPopup,'Value'),Zustaende{1}) && ~isempty(DRT_GUI) && ...
            sum(ismember(fieldnames(DRT_GUI),'Testparameter')) && ~isempty(DRT_GUI.Testparameter) &&...
            sum(ismember(fieldnames(DRT_GUI.Testparameter),'Zustand')) && ~isempty(DRT_GUI.Testparameter.Zustand)
        if ~isempty(find(ismember(Zustaende,DRT_GUI.Testparameter.Zustand)))
            set(app.FR_ZustandPopup,'Value',Zustaende{find(ismember(Zustaende,DRT_GUI.Testparameter.Zustand))})
        end
    end
    ModelDaten.C.actIndex = find(ModelDaten.C.Values == str2num(strrep(strrep(app.FR_ZustandPopup.Value(1:end-4),'m','-'),'_','.')));
    Folders = {};
    if ~strcmp((app.FR_ZustandPopup.Value),Zustaende{1})
        counter = 1;
        for n = transpose(ModelDaten.C.Index)
            Folders = {};
            f = dir(['output/' get(app.BatterieNamePopup,'Value') '/'  ModelDaten.C.Folders{n+1} '/*']);
            for i = 1:numel(f)
                if ~strcmp(f(i).name,'.') && ~strcmp(f(i).name,'..') && f(i).isdir
                    Folders = [Folders;f(i).name];
                end
            end            
            Temperaturen{counter} = zeros(size(Folders));
            SOCs{counter} = [];
            for i = 1:numel(Folders)
                Temperaturen{counter}(i) = str2num(strrep(strrep(Folders{i},'grad',''),'m','-'));
                SOCFiles = {};
%                 f = dir(['output/' get(app.FR_BatterieNamePopup,'Value') '/'  get(app.FR_ZustandPopup,'Value') '/' Folders{i} '/*_*SOC_Modell.mat']);
                f = dir(['output/' get(app.FR_BatterieNamePopup,'Value') '/'  get(app.FR_ZustandPopup,'Value') '/' Folders{i} '/*_*SOC.mat']);
                for j = 1:numel(f)
                    if ~strcmp(f(j).name,'.') && ~strcmp(f(j).name,'..') && ~f(j).isdir
%                         SOCFiles = [SOCFiles;strrep(strrep(strrep(strrep(strrep(f(j).name,Folders{i},''),get(app.FR_BatterieNamePopup,'Value'),''),get(app.FR_ZustandPopup,'Value'),''),'_',''),'Modell.mat','')];
                        SOCFiles = [SOCFiles;strrep(strrep(strrep(strrep(strrep(f(j).name,Folders{i},''),get(app.FR_BatterieNamePopup,'Value'),''),get(app.FR_ZustandPopup,'Value'),''),'_',''),'.mat','')];
                    end
                end
                for j = 1:numel(SOCFiles)
                    if ~sum(find(SOCs{counter} == str2num(strrep(strrep(SOCFiles{j},'SOC',''),'m','-'))))
                        SOCs{counter} = [SOCs{counter} ; str2num(strrep(strrep(SOCFiles{j},'SOC',''),'m','-'))];
                    end
                end
            end
            counter = counter + 1;
        end
    end
else
    Batterien = app.FR_BatterieNamePopup.Items;
    Zustaende = app.FR_ZustandPopup.Items;
    if strcmp(app.FR_ZustandPopup.Value,Zustaende{1}) && ~isempty(DRT_GUI) && ...
            sum(ismember(fieldnames(DRT_GUI),'Testparameter')) && ~isempty(DRT_GUI.Testparameter) &&...
            sum(ismember(fieldnames(DRT_GUI.Testparameter),'Zustand')) && ~isempty(DRT_GUI.Testparameter.Zustand)
        if ~isempty(find(ismember(Zustaende,DRT_GUI.Testparameter.Zustand)))
            app.FR_ZustandPopup.Value = Zustaende{find(ismember(Zustaende,DRT_GUI.Testparameter.Zustand))};
        end
    end
    Folders = {};
    if ~strcmp((app.FR_ZustandPopup.Value),Zustaende{1})
        counter = 1;
        for n = transpose(ModelDaten.C.Index)
            Folders = {};
            f = dir(['output/' app.BatterieNamePopup.Value '/'  ModelDaten.C.Folders{n+1} '/*']);
            for i = 1:numel(f)
                if ~strcmp(f(i).name,'.') && ~strcmp(f(i).name,'..') && f(i).isdir
                    Folders = [Folders;f(i).name];
                end
            end

            Temperaturen{counter} = zeros(size(Folders));
            SOCs{counter} = [];
            for i = 1:numel(Folders)
                Temperaturen{counter}(i) = str2num(strrep(strrep(Folders{i},'grad',''),'m','-'));
                SOCFiles = {};
                f = dir(['output/' app.FR_BatterieNamePopup.Value '/'  app.FR_ZustandPopup.Value '/' Folders{i} '/*_*SOC_Modell.mat']);
                for j = 1:numel(f)
                    if ~strcmp(f(j).name,'.') && ~strcmp(f(j).name,'..') && ~f(j).isdir
                        SOCFiles = [SOCFiles;strrep(strrep(strrep(strrep(strrep(f(j).name,Folders{i},''),app.FR_BatterieNamePopup.Value,''),app.FR_ZustandPopup.Value,''),'_',''),'Modell.mat','')];
                    end
                end
                for j = 1:numel(SOCFiles)
                    if ~sum(find(SOCs{counter} == str2num(strrep(strrep(SOCFiles{j},'SOC',''),'m','-'))))
                        SOCs{counter} = [SOCs{counter} ; str2num(strrep(strrep(SOCFiles{j},'SOC',''),'m','-'))];
                    end
                end
            end
            counter = counter + 1;
        end
    end
end
if strcmp(app.DisplayedStatesButtonGroup.SelectedObject.Text,'T-SOC')
    app.FR_TemperaturTable.ColumnName = [{'T'};{'Plot'}];
    app.FR_SOCTable.ColumnName = [{'SOC'};{'Plot'}];
    app.StateDropDown.Enable = 'off';
    Temperaturen = sort(Temperaturen{ModelDaten.C.actIndex},1);
    SOCs = sort(SOCs{ModelDaten.C.actIndex},1);
    Data = cell(size(SOCs,1),2);
    Data(:,1) = num2cell(SOCs);
    Data(:,2)  = {true};
    if app.GUI
        set(app.FR_SOCTable,'Data',Data )
    else
        app.FR_SOCTable.Data = Data;
    end
    Data = cell(size(Temperaturen,1),2);
    Data(:,1) = num2cell(Temperaturen);
    Data(:,2)  = {true};
    if app.GUI
        set(app.FR_TemperaturTable,'Data',Data )
    else
        app.FR_TemperaturTable.Data = Data ;
    end
elseif strcmp(app.DisplayedStatesButtonGroup.SelectedObject.Text,'C-SOC')
    app.FR_TemperaturTable.ColumnName = [{'C'};{'Plot'}];
    app.FR_SOCTable.ColumnName = [{'SOC'};{'Plot'}];
    app.StateDropDown.Enable = 'on';
    Temperaturen = sort(Temperaturen{ModelDaten.C.actIndex},1);
    SOCs = sort(SOCs{ModelDaten.C.actIndex},1);
    Crates = ModelDaten.C.Values;
    
    Data = cell(size(SOCs,1),2);
    Data(:,1) = num2cell(SOCs);
    Data(:,2)  = {true};
    if app.GUI
        set(app.FR_SOCTable,'Data',Data )
    else
        app.FR_SOCTable.Data = Data;
    end
    
    Data = cell(size(Crates,1),2);
    Data(:,1) = num2cell(Crates);
    Data(:,2)  = {true};
    if app.GUI
        set(app.FR_TemperaturTable,'Data',Data )
    else
        app.FR_TemperaturTable.Data = Data ;
    end
    
    Data = [];
    Data = cell(1,size(Temperaturen,1));
    for n = 1:length(Temperaturen)
        Data{n} = num2str(Temperaturen(n));
    end
    if app.GUI
        set(app.StateDropDown,'Items',Data )
    else
        app.StateDropDown.Items = Data ;
    end    
else
    app.FR_TemperaturTable.ColumnName = [{'C'};{'Plot'}];
    app.FR_SOCTable.ColumnName = [{'T'};{'Plot'}];
    app.StateDropDown.Enable = 'on';
    Temperaturen = sort(Temperaturen{ModelDaten.C.actIndex},1);
    SOCs = sort(SOCs{ModelDaten.C.actIndex},1);
    Crates = ModelDaten.C.Values;
    
    Data = cell(size(Temperaturen,1),2);
    Data(:,1) = num2cell(Temperaturen);
    Data(:,2)  = {true};
    if app.GUI
        set(app.FR_SOCTable,'Data',Data )
    else
        app.FR_SOCTable.Data = Data;
    end
    
    Data = cell(size(Crates,1),2);
    Data(:,1) = num2cell(Crates);
    Data(:,2)  = {true};
    if app.GUI
        set(app.FR_TemperaturTable,'Data',Data )
    else
        app.FR_TemperaturTable.Data = Data ;
    end
    
    Data = [];
    Data = cell(1,size(SOCs,1));
    for n = 1:length(SOCs)
        Data{n} = num2str(SOCs(n));
    end
    if app.GUI
        set(app.StateDropDown,'Items',Data )
    else
        app.StateDropDown.Items = Data ;
    end    
end    
app = fcn_FR_ReloadButton(app, event);
end