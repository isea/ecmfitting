function app = fcn_LHD_DRT_FitButtonPushed(app,event)
%identify model parameters that should be fitted
global DRT_GUI
process_counter = 1;
if strcmp(app.LHD_DRT_Model,'MF')
    for n =1:length(DRT_GUI.Fit.aktuell_Modell.ModellCell{8})
        if ~strcmp( DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n}.Zfun_MF,'0')
            process_indices(process_counter) = n;
            process_counter = process_counter + 1;
        end
    end
elseif strcmp(app.LHD_DRT_Model,'Complete')
    for n =1:length(DRT_GUI.Fit.aktuell_Modell.ModellCell{8})
        if ~strcmp( DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n}.Zfun_MF,'0')
            process_indices(process_counter) = n;
            process_counter = process_counter + 1;
        elseif ~strcmp( DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n}.Zfun_LF,'0')
            process_indices(process_counter) = n;
            process_counter = process_counter + 1;
        else
            process_indices(process_counter) = n;
            process_counter = process_counter + 1;
        end
    end
else
    process_indices = str2num(app.LHD_ECEindicesEditField.Value);
    process_counter = length(process_indices) + 1;
end
process_counter = process_counter - 1;
process_parameter = cell(1,length(DRT_GUI.Messdaten.RelaxEIS(1,:)));
max_number_secondary_maxima = 0;

%parameterize through DRT
for n = 1:length(DRT_GUI.Messdaten.RelaxEIS(1,:))
    %initialize clear data structures
    min_locs{n} = []; %indices of minima in the DRT
    min_pks{n} = []; %absolute values of minima in the DRT
    max_locs{n} = []; %indices of maxima in the DRT
    max_pks{n} = []; %absolute values of maxima in the DRT
    inflection_idx = [];
    secondary_maximum_time_constant_index{n} = [];
    secondary_maxima{n} = [];
    derivative_gamma = [];
    integration_limits{n} = [];
    integration_limit_secondary = [];
    temp_limit = [];
    delete_idx = [];
    delete_idx_lim = [];
    process_parameter{n} = cell(1,process_counter);
    
    %get data from DRT calculation
    tau = 1./app.LHD_DRT_Results{n}.Frequencies;
    gamma = app.LHD_DRT_Results{n}.gamma;
    gamma = transpose(gamma);
    tau = transpose(tau);
    
    
    %find maxima and minima of the gamma values  
    [max_pks{n},max_locs{n}] = findpeaks(gamma);
    [min_pks{n},min_locs{n}] = findpeaks(-1 * gamma);
    if min(max_locs{n}) < min(min_locs{n})
        min_locs{n} = [1, min_locs{n}];
    end
    if max(max_locs{n}) > max(min_locs{n})
        min_locs{n} = [min_locs{n}, length(gamma)];
    end
    for m = length(max_locs{n}):-1:1
        if max_pks{n}(m) < 1e-7
            max_locs{n}(m)=[];
            max_pks{n}(m)=[];

            if m == 1
                if ~isempty(max_locs{n})
                    min_locs{n}(2) = [];
                else
                    min_locs{n} = [];
                end                   
            elseif m == length(max_locs{n})
                if ~isempty(max_locs{n})
                    min_locs{n}(length(max_locs{n})-1) = [];
                else
                    min_locs{n} = [];
                end 
            else                    
                min_locs{n}(m) = [];                    
            end
        end
    end
    %save limits for integration
    for m = 1:length(max_locs{n})
        integration_limits{m} = [min_locs{n}(m) min_locs{n}(m+1)];
    end
    
    %find inflection points
    % inflection_idx represents integral boundary
%     inflection_idx = find(diff(sign(diff(diff(gamma)./diff(tau))))) + 1;
%     [~,inflection_idx] = findpeaks(-1*abs(diff(gamma)./diff(tau)));

    [~,inflection_idx] = findpeaks(diff(diff(gamma)./diff(tau)));
    if ~isempty(inflection_idx)
        for m = length(inflection_idx):-1:1
            %if the indices are directly adjacent to  a primary maximum or
            %minimum -> delete not sensible
            if ~isempty(find(abs(max_locs{n}-inflection_idx(m)) <= 1)) || ~isempty(find(abs(min_locs{n}-inflection_idx(m)) <= 1)) || gamma(inflection_idx(m) )< 1e-7
                inflection_idx(m) = [];
                continue
            end
            if gamma(inflection_idx) <= 1e-10
                inflection_idx(m) = [];
            end
        end
    end    
    
    %inflection_idx1 represents time constant of process
%     [~,secondary_maximum_time_constant_index{n}] = findpeaks(diff(diff(-1 * gamma)./diff(tau)));
%     [~,secondary_maximum_time_constant_index{n}] = findpeaks(diff(diff(gamma)./diff(tau)));

    secondary_maximum_time_constant_index{n} = find(diff(sign(diff(diff(gamma)./diff(tau))))) + 1;
    if ~isempty(secondary_maximum_time_constant_index{n})
        for m = length(secondary_maximum_time_constant_index{n}):-1:1
            %if the indices are directly adjacent to  a primary maximum or
            %minimum -> delete not sensible
            if ~isempty(find(abs(max_locs{n}-secondary_maximum_time_constant_index{n}(m)) <= 1)) || ~isempty(find(abs(min_locs{n}-secondary_maximum_time_constant_index{n}(m)) <= 1)) ...
                    || gamma(secondary_maximum_time_constant_index{n}(m)) < 1e-7
                secondary_maximum_time_constant_index{n}(m) = [];
            end
        end
        if gamma(secondary_maximum_time_constant_index{n}) <= 1e-10
            secondary_maximum_time_constant_index{n}(m) = [];
        end
    end
    
    if ~isempty(inflection_idx) && ~isempty(secondary_maximum_time_constant_index{n})
        %% filter inflection_idx and inflection_idx1 for sensible points
        %primary_max_idx = primary maximum corresponding to inflection_idx1
        %primary_max_idx_rev = inflection_idx1 corresponding to primary maximum
        integration_limit_secondary = cell(1,length(secondary_maximum_time_constant_index{n}));
        delete_idx = ones(1,length(secondary_maximum_time_constant_index{n}));
        primary_max_idx = cell(1,length(secondary_maximum_time_constant_index{n}));
        primary_max_idx_rev = cell(1,length(max_pks{n}));
        for m = 1:length(secondary_maximum_time_constant_index{n})
            %identify corresponding primary maximum
            for m1 = 1:length(min_locs{n})-1
                if min_locs{n}(m1) <= secondary_maximum_time_constant_index{n}(m)  && min_locs{n}(m1+1) >= secondary_maximum_time_constant_index{n}(m)
                    primary_max_idx{m} = m1;
                    primary_max_idx_rev{m1} = [primary_max_idx_rev{m1} m];
                    break
                end
            end
        end
        %Same thing for inflection_idx
        delete_idx_lim = ones(1,length(inflection_idx));
        primary_max_idx_lim = cell(1,length(inflection_idx));
        primary_max_idx_rev_lim = cell(1,length(max_pks{n}));
        for m = 1:length(inflection_idx)
            %identify corresponding primary maximum
            for m1 = 1:length(min_locs{n})-1
                if min_locs{n}(m1) <= inflection_idx(m)  && min_locs{n}(m1+1) >= inflection_idx(m)
                    primary_max_idx_lim{m} = m1;
                    primary_max_idx_rev_lim{m1} = [primary_max_idx_rev_lim{m1} m];
                    break
                end
            end
        end
        
        for n1 = 1:length(max_locs{n})
            %n index of the primary maximu
            %m index within the inflection_idx1 points smaller than the maximum
            %m2 index count right to the currently considered maximum
            smaller_idx = find(secondary_maximum_time_constant_index{n}(primary_max_idx_rev{n1})<max_locs{n}(n1));
            for m = length(smaller_idx):-1:1
                %if time constant of process is smaller than the corresponding primary maximum and there
                %is no integration limit bigger than it but smaller than the
                %primary max -> delete, not sensible
                relevant_point = [];
                if m ~= length(smaller_idx)
                    for m2 = m+1:length(smaller_idx)
                        if ~delete_idx(primary_max_idx_rev{n1}(smaller_idx(m2)))
                            relevant_point = secondary_maximum_time_constant_index{n}(primary_max_idx_rev{n1}(smaller_idx(m2)));
                            break
                        end
                    end
                    if isempty(relevant_point)
                        relevant_point =  max_locs{n}(n1);
                    end
                else
                    relevant_point =  max_locs{n}(n1);
                end
                
                if ~isempty(primary_max_idx_rev_lim{n1})
                    for m1 = 1:length(primary_max_idx_rev_lim{n1})
                        if inflection_idx(primary_max_idx_rev_lim{n1}(m1)) > secondary_maximum_time_constant_index{n}(primary_max_idx_rev{n1}(smaller_idx(m))) && inflection_idx(primary_max_idx_rev_lim{n1}(m1)) < relevant_point
                            delete_idx(primary_max_idx_rev{n1}(smaller_idx(m))) = 0;
                            integration_limit_secondary{smaller_idx(m)} = inflection_idx(primary_max_idx_rev_lim{n1}(m1));
                            break
                        end
                    end
                else
                    delete_idx(smaller_idx(m)) = 1;
                end
            end
            
            bigger_idx = find(secondary_maximum_time_constant_index{n}(primary_max_idx_rev{n1})>max_locs{n}(n1));
            for m = 1:length(bigger_idx)
                %if time constant of process is bigger than the corresponding primary maximum and there
                %is no integration limit smaller than it but bigger than the
                %primary max -> delete, not sensible
                relevant_point = [];
                if m ~= 1
                    for m2 = m-1:-1:1
                        if ~delete_idx(primary_max_idx_rev{n1}(bigger_idx(m2)))
                            relevant_point = secondary_maximum_time_constant_index{n}(primary_max_idx_rev{n1}(bigger_idx(m2)));
                            break
                        end
                    end
                    if isempty(relevant_point)
                        relevant_point =  max_locs{n}(n1);
                    end
                else
                    relevant_point =  max_locs{n}(n1);
                end
                
                if ~isempty(primary_max_idx_rev_lim{n1})
                    for m1 = length(primary_max_idx_rev_lim{n1}):-1:1
                        if inflection_idx(primary_max_idx_rev_lim{n1}(m1)) < secondary_maximum_time_constant_index{n}(primary_max_idx_rev{n1}(bigger_idx(m))) && inflection_idx(primary_max_idx_rev_lim{n1}(m1)) > relevant_point
                            delete_idx(primary_max_idx_rev{n1}(bigger_idx(m))) = 0;
                            integration_limit_secondary{bigger_idx(m)} = inflection_idx(primary_max_idx_rev_lim{n1}(m1));
                            break
                        end
                    end
                else
                    delete_idx(bigger_idx(m)) = 1;
                end
            end
        end
        
        %detect unused elements of inflection_idx
        for m = 1:length(inflection_idx)
            for m1 = 1:length(secondary_maximum_time_constant_index{n})
                if inflection_idx(m) == integration_limit_secondary{m1}
                    delete_idx_lim(m) = 0;
                end
            end
        end
        
        %delete all entries marked for deletion
        for m = length(delete_idx_lim):-1:1
            if delete_idx_lim(m)
                inflection_idx(m) = [];
            end
        end
        for m = length(delete_idx):-1:1
            if delete_idx(m)
                secondary_maximum_time_constant_index{n}(m) = [];
            end
        end
        
        %% assign secondary maxima to primary minima and sort in accordance to
        %their importance (least steep slope, not perfect at all)
        secondary_maxima{n} = cell(1,length(max_pks{n}));
        sec_max_int_limit{n} = cell(1,length(max_pks{n}));
        temp_limit = cell(1,length(max_pks{n}));
        if ~isempty(secondary_maximum_time_constant_index{n})
            for m = 2:length(min_locs{n})
                secondary_maxima{n}{m-1} = secondary_maximum_time_constant_index{n}(find(min_locs{n}(m) - secondary_maximum_time_constant_index{n} >= 0));
                temp_limit{m-1} = inflection_idx(find(min_locs{n}(m) - inflection_idx >= 0));
%                 inflection_idx(find(min_locs{n}(m) - secondary_maximum_time_constant_index{n} >= 0)) = [];
                inflection_idx(find(min_locs{n}(m) - inflection_idx >= 0)) = [];
                secondary_maximum_time_constant_index{n}(find(min_locs{n}(m) - secondary_maximum_time_constant_index{n} >= 0)) = [];
                for m1 = 1:length(secondary_maxima{n}{m-1})
                    if secondary_maxima{n}{m-1}(m1) > max_locs{n}(m-1)
                        sec_max_int_limit{n}{m-1}{m1} = [temp_limit{m-1}(m1)  integration_limits{m-1}(2)];
                    else
                        sec_max_int_limit{n}{m-1}{m1} = [integration_limits{m-1}(1)  temp_limit{m-1}(m1)];
                    end
                end
            end
        end
        derivative_gamma = diff(gamma);
        for m = 1:length(secondary_maxima{n})
            if ~isempty(secondary_maxima{n}{m})
                [~,rearrangement_idx] = sort(abs(derivative_gamma(secondary_maxima{n}{m})));
                secondary_maxima{n}{m}(rearrangement_idx) = secondary_maxima{n}{m};
                sec_max_int_limit{n}{m}(rearrangement_idx) = sec_max_int_limit{n}{m};
            end
        end
    else
        secondary_maxima{n} = cell(1,length(max_pks{n}));
        sec_max_int_limit{n} = cell(1,length(max_pks{n}));
    end
    for n1 = 1:length(max_pks{n})
        temp_secondary_maxima{n}{n1} = length( secondary_maxima{n}{n1});
    end
    temp_primary_maxima(n) = length(max_pks{n});
end

% delete_idx = [];
% delete_idx = cell(1,length(DRT_GUI.Messdaten.RelaxEIS(1,:)));
% for n = 1:length(DRT_GUI.Messdaten.RelaxEIS(1,:))
%     delete_idx{n} = ones(1,temp_primary_maxima(n));
% end
% %pre-processing:
% %find most consistant primary maxima
% if ~isempty(find(temp_primary_maxima~=min(temp_primary_maxima)))
%     adaptation_index = find(temp_primary_maxima~=min(temp_primary_maxima));
%     non_adaptation_index = find(temp_primary_maxima==min(temp_primary_maxima));    
%     
%     %find mean values of the maxima
%     for n1 = 1:min(temp_primary_maxima)
%         mean_value(n1) = 0;
%         for n = non_adaptation_index
%             tau = 1./app.LHD_DRT_Results{n}.Frequencies;
%             gamma = app.LHD_DRT_Results{n}.gamma;
%             gamma = transpose(gamma);
%             tau = transpose(tau);
%             mean_value(n1) = mean_value(n1) + tau(max_locs{n}(n1));
%         end   
%         mean_value(n1) = mean_value(n1)/length(non_adaptation_index);
%     end
%     
%     %find the ones that match     
%     for n = adaptation_index
%         tau = 1./app.LHD_DRT_Results{n}.Frequencies;
%         gamma = app.LHD_DRT_Results{n}.gamma;
%         gamma = transpose(gamma);
%         tau = transpose(tau);
%         for n1 = 1:min(temp_primary_maxima)  
%             [~,idx] = min(abs(tau(max_locs{n})-mean_value(n1)));
%             delete_idx{n}(idx) = 0;
%         end  
%     end
% end
% 
% % 
% % %find most consistant secondary maxima
% for n = 1:length(DRT_GUI.Messdaten.RelaxEIS(1,:))
%     tau = 1./app.LHD_DRT_Results{n}.Frequencies;
%     gamma = app.LHD_DRT_Results{n}.gamma;
%     gamma = transpose(gamma);
%     tau = transpose(tau);
%     [max_pks{n},max_locs{n}] = findpeaks(gamma);
%     [min_pks{n},min_locs{n}] = findpeaks(-1 * gamma);
%     if min(max_locs{n}) < min(min_locs{n})
%         min_locs{n} = [1, min_locs{n}];
%     end
%     if max(max_locs{n}) > max(min_locs{n})
%         min_locs{n} = [min_locs{n}, length(gamma)];
%     end
%     
%     % relate secondary maxima
%     temp_secondary_maxima
%     for n1 = 1:length(max_pks{n})
%         temp_secondary_maxima(n) = 1;
%     end
% end

for n = 1:length(DRT_GUI.Messdaten.RelaxEIS(1,:))
    %% determine Rs and Cs as in https://doi.org/10.3390/batteries7030052 
    tau = 1./app.LHD_DRT_Results{n}.Frequencies;
    gamma = app.LHD_DRT_Results{n}.gamma;
    gamma = transpose(gamma);
    tau = transpose(tau);
    
    temp_secondary_max = [];
    inverse_temp_secondary_max = [];
    if strcmp(app.LHD_DRT_Model,'MF')
        if length(max_locs{n})-1 == process_counter
            %case if number of maxima is equal to the number of processes
            for n1 = 2:length(max_locs{n})
                process_parameter{n}{n1-1}{1}(1) = tau(max_locs{n}(n1));
                process_parameter{n}{n1-1}{1}(2) = max_pks{n}(n1);
                process_parameter{n}{n1-1}{2} = integration_limits{n1};
                process_parameter{n}{n1-1}{3} = 1;
            end
        elseif length(max_locs{n})-1 > process_counter
            %case if more maxima than processes are detected
            %-> choose the peaks with the highest impacts
            [sorted_max,rearr_max] = sort(max_pks{n}(2:end), 'descend');
            [sorted_tau,rearr_tau] = sort(tau(max_locs{n}(1+rearr_max)));
            
            for n1 = 1:process_counter
                process_parameter{n}{n1}{1}(1) = sorted_tau(n1);
                process_parameter{n}{n1}{1}(2) = max_pks{n}(1+rearr_max(rearr_tau(n1)));
                process_parameter{n}{n1}{2} = integration_limits{1+rearr_max(rearr_tau(n1))};
                process_parameter{n}{n1}{3} = 1;
            end
        else
            process_deficit = process_counter - (length(max_locs{n})-1);
            deficit_compensation = 0;
            %case if fewer maxima than processes are detected
            counter = 1;
            for n1 = 2:length(max_locs{n})
                if ~isempty(secondary_maxima{n}{n1})
                    for n2 = 1:length(secondary_maxima{n}{n1})
                        % list all secondary maxima
                        temp_secondary_max(counter) = secondary_maxima{n}{n1}(n2);
                        temp_limits{counter} = sec_max_int_limit{n}{n1}{n2};
                        % map secondary maxima to primary maxima and index
                        inverse_temp_secondary_max{counter} = [n1 , n2];
                        counter = counter + 1;
                    end
                end
            end                       
            
            counter = counter - 1;
            if ~isempty(temp_secondary_max)
                %sort secondary maxima to use the most relevant ones first
                [sorted_temp_secondary_max,rearr_sorted_temp_secondary_max] = sort(gamma(temp_secondary_max),'descend');
                temp_secondary_max = temp_secondary_max(rearr_sorted_temp_secondary_max(1:process_deficit));
                temp_limits = temp_limits(rearr_sorted_temp_secondary_max(1:process_deficit));
                inverse_temp_secondary_max = inverse_temp_secondary_max{rearr_sorted_temp_secondary_max(1:process_deficit)};
                if length(temp_secondary_max) + (length(max_locs{n})-1) == process_counter
%                     concencatenated_max = [max_pks{n}(2:end), gamma(temp_secondary_max)];
%                     concencatenated_tau = [tau(max_locs{n}(2:end)), tau(temp_secondary_max)];
                    
                    [sorted_max,rearr_max] = sort([max_pks{n}(2:end), gamma(temp_secondary_max)], 'descend');
                    [sorted_tau,rearr_tau] = sort([tau(max_locs{n}(1+rearr_max(1:length(max_pks{n}(2:end))))), tau(temp_secondary_max)]);
                    
                    for n1 = 1:process_counter
                        process_parameter{n}{n1}{1}(1) = sorted_tau(n1);
                        if rearr_tau(n1) <= length(max_pks{n}(2:end))
                            process_parameter{n}{n1}{1}(2) = max_pks{n}(1+rearr_max(rearr_tau(n1)));
                            process_parameter{n}{n1}{2} = integration_limits{1+rearr_max(rearr_tau(n1))};
                        else
                            process_parameter{n}{n1}{1}(2) = gamma(temp_secondary_max(rearr_tau(n1)-length(max_pks{n}(2:end))));
                            process_parameter{n}{n1}{2} = temp_limits{rearr_tau(n1)-length(max_pks{n}(2:end))};
                        end
                        %if a secondary maximum is used, the integration
                        %limts are adjusted
                        if n1 > 1 %&& n1 < process_counter
                            if  process_parameter{n}{n1-1}{2}(2) == process_parameter{n}{n1}{2}(2)
                                process_parameter{n}{n1-1}{2}(2) = process_parameter{n}{n1}{2}(1)-1;
                            elseif process_parameter{n}{n1-1}{2}(1) == process_parameter{n}{n1}{2}(1)
                                process_parameter{n}{n1}{2}(1) = process_parameter{n}{n1-1}{2}(2)+1;
                            end
                        end
                        process_parameter{n}{n1}{3} = 1;
                    end
                elseif length(temp_secondary_max) + (length(max_locs{n})-1) > process_counter
                    %use the most influential peaks (integration not
                    %possible -> use peak height
                    [sorted_max,rearr_max] = sort([max_pks{n}(2:end), gamma(temp_secondary_max)], 'descend');
                    [sorted_tau,rearr_tau] = sort([tau(max_locs{n}(1+rearr_max(1:length(max_pks{n}(2:end))))), tau(temp_secondary_max)]);
                    for n1 = 1:process_counter
                        process_parameter{n}{n1}{1}(1) = sorted_tau(n1);
                        if rearr_tau(n1) <= length(max_pks{n}(2:end))
                            process_parameter{n}{n1}{1}(2) = max_pks{n}(1+rearr_max(rearr_tau(n1)));
                            process_parameter{n}{n1}{2} = integration_limits{1+rearr_max(rearr_tau(n1))};
                        else
                            process_parameter{n}{n1}{1}(2) = gamma(temp_secondary_max(rearr_tau(n1)-length(max_pks{n}(2:end))));
                            process_parameter{n}{n1}{2} = temp_limits{rearr_tau(n1)-length(max_pks{n}(2:end))};
                        end
                        %if a secondary maximum is used, the integration
                        %limts are adjusted
                        if n1 > 1
                            if  process_parameter{n}{n1-1}{2}(2) == process_parameter{n}{n1}{2}(2)
                                process_parameter{n}{n1-1}{2}(2) = process_parameter{n}{n1}{2}(1)-1;
                            elseif process_parameter{n}{n1-1}{2}(1) == process_parameter{n}{n1}{2}(1)
                                process_parameter{n}{n1}{2}(1) = process_parameter{n}{n1-1}{2}(2)+1;
                            end
                        end
                        process_parameter{n}{n1}{3} = 1;
                    end
                else
                    %not enough secondary maxima ->  multiple processes per
                    %peak
                    
                    %use the maxima that were found
                    [sorted_max,rearr_max] = sort([max_pks{n}(2:end), gamma(temp_secondary_max)], 'descend');
                    [sorted_tau,rearr_tau] = sort([tau(max_locs{n}(1+rearr_max(1:length(max_pks{n}(2:end))))), tau(temp_secondary_max)]);
                    for n1 = 1:length(temp_secondary_max) + (length(max_locs{n})-1)
                        process_parameter{n}{n1}{1}(1) = sorted_tau(n1);
                        if rearr_tau(n1) <= length(max_pks{n}(2:end))
                            process_parameter{n}{n1}{1}(2) = max_pks{n}(1+rearr_max(rearr_tau(n1)));
                            process_parameter{n}{n1}{2} = integration_limits{1+rearr_max(rearr_tau(n1))};
                        else
                            process_parameter{n}{n1}{1}(2) = gamma(temp_secondary_max(rearr_tau(n1)-length(max_pks{n}(2:end))));
                            process_parameter{n}{n1}{2} = temp_limits{rearr_tau(n1)-length(max_pks{n}(2:end))};
                        end
                        %if a secondary maximum is used, the integration
                        %limts are adjusted
                        if n1 > 1
                            if  process_parameter{n}{n1-1}{2}(2) == process_parameter{n}{n1}{2}(2)
                                process_parameter{n}{n1-1}{2}(2) = process_parameter{n}{n1}{2}(1)-1;
                            elseif process_parameter{n}{n1-1}{2}(1) == process_parameter{n}{n1}{2}(1)
                                process_parameter{n}{n1}{2}(1) = process_parameter{n}{n1-1}{2}(2)+1;
                            end
                        end
                        process_parameter{n}{n1}{3} = 1;
                    end
                    
                    %add more processes to peaks
                    for n3 = 1:100
                        for n1 = 1:length(temp_secondary_max) + (length(max_locs{n})-1)
                            if process_deficit == deficit_compensation
                                continue
                            else
                                process_parameter{n}{rearr_tau(n1)}{3} =  process_parameter{n}{rearr_tau(n1)}{3} + 1;
                                deficit_compensation = deficit_compensation + 1;
                            end
                        end
                        
                        if process_deficit == deficit_compensation
                            break
                        end
                    end
                end
            else
                %no secondary maxima detected -> multiple processes per
                %peak
                process_deficit = process_counter - (length(max_locs{n})-1);
                deficit_compensation = 0;
                for n1 = 2:length(max_locs{n})
                    process_parameter{n}{n1-1}{1}(1) = tau(max_locs{n}(n1));
                    process_parameter{n}{n1-1}{1}(2) = max_pks{n}(n1);
                    process_parameter{n}{n1-1}{2} = integration_limits{n1};
                    process_parameter{n}{n1-1}{3} = 1;
                end
                
                [sorted_max,rearr_max] = sort(max_pks{n}(2:end), 'descend');
                % iterate over the identified
                for n3 = 1:100
                    for n1 = 1:length(rearr_max)
                        if process_deficit == deficit_compensation
                            continue
                        else
                            process_parameter{n}{rearr_max(n1)}{3} =  process_parameter{n}{rearr_max(n1)}{3} + 1;
                            deficit_compensation = deficit_compensation + 1;
                        end
                    end
                    
                    if process_deficit == deficit_compensation
                        break
                    end
                end
            end
        end
    elseif strcmp(app.LHD_DRT_Model,'Custom')
        if length(max_locs{n}) == process_counter
            %case if number of maxima is equal to the number of processes
            counter = 1;
            for n1 = process_indices
                process_parameter{n}{counter}{1}(1) = tau(max_locs{n}(n1));
                process_parameter{n}{counter}{1}(2) = max_pks{n}(n1);
                process_parameter{n}{counter}{2} = integration_limits{n1};
                process_parameter{n}{counter}{3} = 1;
                counter = counter + 1;
            end
        elseif length(max_locs{n}) > process_counter
            %case if more maxima than processes are detected
            %-> choose the peaks with the highest impacts
            [sorted_max,rearr_max] = sort(max_pks{n}(1:end), 'descend');
            [sorted_tau,rearr_tau] = sort(tau(max_locs{n}(rearr_max)));
            counter = 1;
            for n1 = process_indices
                process_parameter{n}{counter}{1}(1) = sorted_tau(n1);
                process_parameter{n}{counter}{1}(2) = max_pks{n}(rearr_max(rearr_tau(n1)));
                process_parameter{n}{counter}{2} = integration_limits{rearr_max(rearr_tau(n1))};
                process_parameter{n}{counter}{3} = 1;
                counter = counter + 1;
            end
        else
            process_deficit = process_counter - (length(max_locs{n}));
            deficit_compensation = 0;
            %case if fewer maxima than processes are detected
            counter = 1;
            for n1 = 1:length(max_locs{n})
                if ~isempty(secondary_maxima{n}{n1})
                    for n2 = 1:length(secondary_maxima{n}{n1})
                        % list all secondary maxima
                        temp_secondary_max(counter) = secondary_maxima{n}{n1}(n2);
                        temp_limits{counter} = sec_max_int_limit{n}{n1}{n2};
                        % map secondary maxima to primary maxima and index
                        inverse_temp_secondary_max{counter} = [n1 , n2];
                        counter = counter + 1;
                    end
                end
            end
            counter = counter - 1;
            if ~isempty(temp_secondary_max)
                [sorted_temp_secondary_max,rearr_sorted_temp_secondary_max] = sort(gamma(temp_secondary_max),'descend');
                temp_secondary_max = temp_secondary_max(rearr_sorted_temp_secondary_max(1:process_deficit));
                temp_limits = temp_limits(rearr_sorted_temp_secondary_max(1:process_deficit));
                inverse_temp_secondary_max = inverse_temp_secondary_max{rearr_sorted_temp_secondary_max(1:process_deficit)};
                if length(temp_secondary_max) + (length(max_locs{n})) == process_counter
                    [sorted_max,rearr_max] = sort([max_pks{n}(1:end), gamma(temp_secondary_max)], 'descend');
                    [sorted_tau,rearr_tau] = sort([tau(max_locs{n}(rearr_max(1:length(max_pks{n}(1:end))))), tau(temp_secondary_max)]);
                    counter = 1;
                    for n1 = 1:process_indices
                        process_parameter{n}{counter}{1}(1) = sorted_tau(n1);
                        if rearr_tau(n1) <= length(max_pks{n}(1:end))
                            process_parameter{n}{counter}{1}(2) = max_pks{n}(rearr_max(rearr_tau(n1)));
                            process_parameter{n}{counter}{2} = integration_limits{rearr_max(rearr_tau(n1))};
                        else
                            process_parameter{n}{counter}{1}(2) = gamma(temp_secondary_max(rearr_tau(n1)-length(max_pks{n}(1:end))));
                            process_parameter{n}{counter}{2} = temp_limits{rearr_tau(n1)-length(max_pks{n}(1:end))};
                        end
                        %if a secondary maximum is used, the integration
                        %limts are adjusted
                        if counter > 1
                            if  process_parameter{n}{counter-1}{2}(2) == process_parameter{n}{n1}{2}(2)
                                process_parameter{n}{counter-1}{2}(2) = process_parameter{n}{n1}{2}(1)-1;
                            elseif process_parameter{n}{counter-1}{2}(1) == process_parameter{n}{n1}{2}(1)
                                process_parameter{n}{counter}{2}(1) = process_parameter{n}{n1-1}{2}(2)+1;
                            end
                        end
                        process_parameter{n}{counter}{3} = 1;
                        counter = counter + 1;
                    end
                elseif length(temp_secondary_max) + (length(max_locs{n})) > process_counter
                    %use the most influential peaks (integration not
                    %possible -> use peak height
                    [sorted_max,rearr_max] = sort([max_pks{n}(1:end), gamma(temp_secondary_max)], 'descend');
                    [sorted_tau,rearr_tau] = sort([tau(max_locs{n}(rearr_max(1:length(max_pks{n}(1:end))))), tau(temp_secondary_max)]);
                    counter = 1;
                    for n1 = 1:process_indices
                        process_parameter{n}{counter}{1}(1) = sorted_tau(n1);
                        if rearr_tau(n1) <= length(max_pks{n}(1:end))
                            process_parameter{n}{counter}{1}(2) = max_pks{n}(rearr_max(rearr_tau(n1)));
                            process_parameter{n}{counter}{2} = integration_limits{rearr_max(rearr_tau(n1))};
                        else
                            process_parameter{n}{counter}{1}(2) = gamma(temp_secondary_max(rearr_tau(n1)-length(max_pks{n}(1:end))));
                            process_parameter{n}{counter}{2} = temp_limits{rearr_tau(n1)-length(max_pks{n}(1:end))};
                        end
                        %if a secondary maximum is used, the integration
                        %limts are adjusted
                        if counter > 1
                            if  process_parameter{n}{counter-1}{2}(2) == process_parameter{n}{n1}{2}(2)
                                process_parameter{n}{counter-1}{2}(2) = process_parameter{n}{n1}{2}(1)-1;
                            elseif process_parameter{n}{counter-1}{2}(1) == process_parameter{n}{n1}{2}(1)
                                process_parameter{n}{counter}{2}(1) = process_parameter{n}{n1-1}{2}(2)+1;
                            end
                        end
                        process_parameter{n}{counter}{3} = 1;
                        counter = counter + 1;
                    end
                else
                    %not enough secondary maxima ->  multiple processes per
                    %peak
                    
                    %use the maxima that were found
                    [sorted_max,rearr_max] = sort([max_pks{n}(1:end), gamma(temp_secondary_max)], 'descend');
                    [sorted_tau,rearr_tau] = sort([tau(max_locs{n}(rearr_max(1:length(max_pks{n}(1:end))))), tau(temp_secondary_max)]);
                    counter = 1;
                    for n1 = 1:length(temp_secondary_max) + (length(max_locs{n}))
                        process_parameter{n}{n1}{1}(1) = sorted_tau(n1);
                        if rearr_tau(n1) <= length(max_pks{n}(1:end))
                            process_parameter{n}{n1}{1}(2) = max_pks{n}(rearr_max(rearr_tau(n1)));
                            process_parameter{n}{n1}{2} = integration_limits{rearr_max(rearr_tau(n1))};
                        else
                            process_parameter{n}{n1}{1}(2) = gamma(temp_secondary_max(rearr_tau(n1)-length(max_pks{n}(1:end))));
                            process_parameter{n}{n1}{2} = temp_limits{rearr_tau(n1)-length(max_pks{n}(1:end))};
                        end
                        %if a secondary maximum is used, the integration
                        %limts are adjusted
                        if n1 > 1
                            if  process_parameter{n}{n1-1}{2}(2) == process_parameter{n}{n1}{2}(2)
                                process_parameter{n}{n1-1}{2}(2) = process_parameter{n}{n1}{2}(1)-1;
                            elseif process_parameter{n}{n1-1}{2}(1) == process_parameter{n}{n1}{2}(1)
                                process_parameter{n}{n1}{2}(1) = process_parameter{n}{n1-1}{2}(2)+1;
                            end
                        end
                        process_parameter{n}{n1}{3} = 1;
                    end
                    
                    %add more processes to peaks
                    for n3 = 1:100
                        for n1 = 1:length(temp_secondary_max) + (length(max_locs{n}))
                            if process_deficit == deficit_compensation
                                continue
                            else
                                process_parameter{n}{rearr_tau(n1)}{3} =  process_parameter{n}{rearr_tau(n1)}{3} + 1;
                                deficit_compensation = deficit_compensation + 1;
                            end
                        end
                        
                        if process_deficit == deficit_compensation
                            break
                        end
                    end
                end
            else
                %no secondary maxima detected -> multiple processes per
                %peak
                process_deficit = process_counter - (length(max_locs{n}));
                deficit_compensation = 0;
                for n1 = 1:length(max_locs{n})
                    process_parameter{n}{n1}{1}(1) = tau(max_locs{n}(n1));
                    process_parameter{n}{n1}{1}(2) = max_pks{n}(n1);
                    process_parameter{n}{n1}{2} = integration_limits{n1};
                    process_parameter{n}{n1}{3} = 1;
                end
                
                [sorted_max,rearr_max] = sort(max_pks{n}(1:end), 'descend');
                % iterate over the identified
                for n3 = 1:100
                    for n1 = 1:length(rearr_max)
                        if process_deficit == deficit_compensation
                            continue
                        else
                            process_parameter{n}{rearr_max(n1)}{3} =  process_parameter{n}{rearr_max(n1)}{3} + 1;
                            deficit_compensation = deficit_compensation + 1;
                        end
                    end
                    
                    if process_deficit == deficit_compensation
                        break
                    end
                end
            end
        end
    else
        if length(max_locs{n}) == process_counter
            %case if number of maxima is equal to the number of processes
            for n1 = 1:length(max_locs{n})
                process_parameter{n}{n1}{1}(1) = tau(max_locs{n}(n1));
                process_parameter{n}{n1}{1}(2) = max_pks{n}(n1);
                process_parameter{n}{n1}{2} = integration_limits{n1};
                process_parameter{n}{n1}{3} = 1;
            end
        elseif length(max_locs{n}) > process_counter
            %case if more maxima than processes are detected
            %-> choose the peaks with the highest impacts
            [sorted_max,rearr_max] = sort(max_pks{n}(1:end), 'descend');
            [sorted_tau,rearr_tau] = sort(tau(max_locs{n}(rearr_max)));
            
            for n1 = 1:process_counter
                process_parameter{n}{n1}{1}(1) = sorted_tau(n1);
                process_parameter{n}{n1}{1}(2) = max_pks{n}(rearr_max(rearr_tau(n1)));
                process_parameter{n}{n1}{2} = integration_limits{rearr_max(rearr_tau(n1))};
                process_parameter{n}{n1}{3} = 1;
            end
        else
            process_deficit = process_counter - (length(max_locs{n}));
            deficit_compensation = 0;
            %case if fewer maxima than processes are detected
            counter = 1;
            for n1 = 1:length(max_locs{n})
                if ~isempty(secondary_maxima{n}{n1})
                    for n2 = 1:length(secondary_maxima{n}{n1})
                        % list all secondary maxima
                        temp_secondary_max(counter) = secondary_maxima{n}{n1}(n2);
                        temp_limits{counter} = sec_max_int_limit{n}{n1}{n2};
                        % map secondary maxima to primary maxima and index
                        inverse_temp_secondary_max{counter} = [n1 , n2];
                        counter = counter + 1;
                    end
                end
            end
            counter = counter - 1;
            if ~isempty(temp_secondary_max)
                [sorted_temp_secondary_max,rearr_sorted_temp_secondary_max] = sort(gamma(temp_secondary_max),'descend');
                temp_secondary_max = temp_secondary_max(rearr_sorted_temp_secondary_max(1:process_deficit));
                temp_limits = temp_limits(rearr_sorted_temp_secondary_max(1:process_deficit));
                inverse_temp_secondary_max = inverse_temp_secondary_max{rearr_sorted_temp_secondary_max(1:process_deficit)};
                if length(temp_secondary_max) + (length(max_locs{n})) == process_counter
                    [sorted_max,rearr_max] = sort([max_pks{n}(1:end), gamma(temp_secondary_max)], 'descend');
                    [sorted_tau,rearr_tau] = sort([tau(max_locs{n}(rearr_max(1:length(max_pks{n}(1:end))))), tau(temp_secondary_max)]);
                    
                    for n1 = 1:process_counter
                        process_parameter{n}{n1}{1}(1) = sorted_tau(n1);
                        if rearr_tau(n1) <= length(max_pks{n}(1:end))
                            process_parameter{n}{n1}{1}(2) = max_pks{n}(rearr_max(rearr_tau(n1)));
                            process_parameter{n}{n1}{2} = integration_limits{rearr_max(rearr_tau(n1))};
                        else
                            process_parameter{n}{n1}{1}(2) = gamma(temp_secondary_max(rearr_tau(n1)-length(max_pks{n}(1:end))));
                            process_parameter{n}{n1}{2} = temp_limits{rearr_tau(n1)-length(max_pks{n}(1:end))};
                        end
                        %if a secondary maximum is used, the integration
                        %limts are adjusted
                        if n1 > 1 %&& n1 < process_counter
                            if  process_parameter{n}{n1-1}{2}(2) == process_parameter{n}{n1}{2}(2)
                                process_parameter{n}{n1-1}{2}(2) = process_parameter{n}{n1}{2}(1)-1;
                            elseif process_parameter{n}{n1-1}{2}(1) == process_parameter{n}{n1}{2}(1)
                                process_parameter{n}{n1}{2}(1) = process_parameter{n}{n1-1}{2}(2)+1;
                            end
                        end
                        process_parameter{n}{n1}{3} = 1;
                    end
                elseif length(temp_secondary_max) + (length(max_locs{n})) > process_counter
                    %use the most influential peaks (integration not
                    %possible -> use peak height
                    [sorted_max,rearr_max] = sort([max_pks{n}(1:end), gamma(temp_secondary_max)], 'descend');
                    [sorted_tau,rearr_tau] = sort([tau(max_locs{n}(rearr_max(1:length(max_pks{n}(1:end))))), tau(temp_secondary_max)]);
                    for n1 = 1:process_counter
                        process_parameter{n}{n1}{1}(1) = sorted_tau(n1);
                        if rearr_tau(n1) <= length(max_pks{n}(2:end))
                            process_parameter{n}{n1}{1}(2) = max_pks{n}(rearr_max(rearr_tau(n1)));
                            process_parameter{n}{n1}{2} = integration_limits{rearr_max(rearr_tau(n1))};
                        else
                            process_parameter{n}{n1}{1}(2) = gamma(temp_secondary_max(rearr_tau(n1)-length(max_pks{n}(1:end))));
                            process_parameter{n}{n1}{2} = temp_limits{rearr_tau(n1)-length(max_pks{n}(1:end))};
                        end
                        %if a secondary maximum is used, the integration
                        %limts are adjusted
                        if n1 > 1
                            if  process_parameter{n}{n1-1}{2}(2) == process_parameter{n}{n1}{2}(2)
                                process_parameter{n}{n1-1}{2}(2) = process_parameter{n}{n1}{2}(1)-1;
                            elseif process_parameter{n}{n1-1}{2}(1) == process_parameter{n}{n1}{2}(1)
                                process_parameter{n}{n1}{2}(1) = process_parameter{n}{n1-1}{2}(2)+1;
                            end
                        end
                        process_parameter{n}{n1}{3} = 1;
                    end
                else
                    %not enough secondary maxima ->  multiple processes per
                    %peak
                    
                    %use the maxima that were found
                    [sorted_max,rearr_max] = sort([max_pks{n}(1:end), gamma(temp_secondary_max)], 'descend');
                    [sorted_tau,rearr_tau] = sort([tau(max_locs{n}(rearr_max(1:length(max_pks{n}(1:end))))), tau(temp_secondary_max)]);
                    for n1 = 1:length(temp_secondary_max) + (length(max_locs{n}))
                        process_parameter{n}{n1}{1}(1) = sorted_tau(n1);
                        if rearr_tau(n1) <= length(max_pks{n}(1:end))
                            process_parameter{n}{n1}{1}(2) = max_pks{n}(1+rearr_max(rearr_tau(n1)));
                            process_parameter{n}{n1}{2} = integration_limits{rearr_max(rearr_tau(n1))};
                        else
                            process_parameter{n}{n1}{1}(2) = gamma(temp_secondary_max(rearr_tau(n1)-length(max_pks{n}(1:end))));
                            process_parameter{n}{n1}{2} = temp_limits{rearr_tau(n1)-length(max_pks{n}(1:end))};
                        end
                        %if a secondary maximum is used, the integration
                        %limts are adjusted
                        if n1 > 1
                            if  process_parameter{n}{n1-1}{2}(2) == process_parameter{n}{n1}{2}(2)
                                process_parameter{n}{n1-1}{2}(2) = process_parameter{n}{n1}{2}(1)-1;
                            elseif process_parameter{n}{n1-1}{2}(1) == process_parameter{n}{n1}{2}(1)
                                process_parameter{n}{n1}{2}(1) = process_parameter{n}{n1-1}{2}(2)+1;
                            end
                        end
                        process_parameter{n}{n1}{3} = 1;
                    end
                    
                    %add more processes to peaks
                    for n3 = 1:100
                        for n1 = 1:length(temp_secondary_max) + (length(max_locs{n}))
                            if process_deficit == deficit_compensation
                                continue
                            else
                                process_parameter{n}{rearr_tau(n1)}{3} =  process_parameter{n}{rearr_tau(n1)}{3} + 1;
                                deficit_compensation = deficit_compensation + 1;
                            end
                        end
                        
                        if process_deficit == deficit_compensation
                            break
                        end
                    end
                end
            else
                %no secondary maxima detected -> multiple processes per
                %peak
                process_deficit = process_counter - (length(max_locs{n}));
                deficit_compensation = 0;
                for n1 = 1:length(max_locs{n})
                    process_parameter{n}{n1}{1}(1) = tau(max_locs{n}(n1));
                    process_parameter{n}{n1}{1}(2) = max_pks{n}(n1);
                    process_parameter{n}{n1}{2} = integration_limits{n1};
                    process_parameter{n}{n1}{3} = 1;
                end
                
                [sorted_max,rearr_max] = sort(max_pks{n}(1:end), 'descend');
                % iterate over the identified
                for n3 = 1:100
                    for n1 = 1:length(rearr_max)
                        if process_deficit == deficit_compensation
                            continue
                        else
                            process_parameter{n}{rearr_max(n1)}{3} =  process_parameter{n}{rearr_max(n1)}{3} + 1;
                            deficit_compensation = deficit_compensation + 1;
                        end
                    end
                    
                    if process_deficit == deficit_compensation
                        break
                    end
                end
            end
        end
    end
    
    %identify resistor via integration
    for n1 = length(process_parameter{n}):-1:1
        if ~isempty(process_parameter{n}{n1})
            process_parameter{n}{n1}{1}(2) = trapz(log(tau(process_parameter{n}{n1}{2}(1):process_parameter{n}{n1}{2}(2))),gamma(process_parameter{n}{n1}{2}(1):process_parameter{n}{n1}{2}(2)));
        end
    end
%     
    for n1 = length(process_parameter{n}):-1:1
        if ~isempty(process_parameter{n}{n1})
            if process_parameter{n}{n1}{3} > 1
                for n2 = length(process_parameter{n}):-1:n1
                    process_parameter{n}{n2-1+process_parameter{n}{n1}{3}} = process_parameter{n}{n2};
                end
                for n2 = process_parameter{n}{n1}{3}:-1:1
                    process_parameter{n}{n1+n2-1} = process_parameter{n}{n1};
                    process_parameter{n}{n1+n2-1}{1}(2) = process_parameter{n}{n1}{1}(2)/process_parameter{n}{n1}{3};
                    process_parameter{n}{n1+n2-1}{3} = 1;
                end
            end
        end
    end   
%     plot(tau,gamma)
%     hold on
%     grid on
%     for n1 = 1:length(process_parameter{n})
%         if ~isempty(process_parameter{n}{n1})
%             plot(process_parameter{n}{n1}{1}(1),process_parameter{n}{n1}{1}(2),'kX')
%         else
%             process_parameter{n}{n1} = [];
%         end
%     end
%     xlabel('$\tau / s$','Interpreter','latex');
%     ylabel('$\gamma (ln \tau)/\Omega$','Interpreter','latex');
%     set(gca,'xscale','log');
end
app.LHD_DRT_ParameterValues = process_parameter;
end


