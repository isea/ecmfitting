function app = fcn_LHD_CalculateDRTButtonPushed(app,event)
%check if data are present
global DRT_GUI
CRate_var = strrep(DRT_GUI.Testparameter.Zustand,'m','-');
CRate_var = str2num(strrep(CRate_var(1:end-4),'_','.'));
if ~isfield(DRT_GUI.Messdaten,'RelaxEIS') || isempty(DRT_GUI.Messdaten.RelaxEIS) || CRate_var == 0
    msgbox('No Data Present')
    return
end
app.LHD_DRT_Results = [];

%Default settings
% Default initialization
handles.data_exist          = false;                                                 % No data exists
handles.method_tag          = 'none';                                                % Method tag

% Type of DRT
handles.rbf_type            = 'Gaussian';                                              % Method of Discretization
handles.data_used           = 'Combined Re-Im Data';                                  % Data Used
handles.inductance          = 'Fitting w/o Inductance';                              % Inductanze Included
handles.der_used            = '1st-order';                                             % Regularization Derivative
handles.lambda              = 1e-3;                                                      % Regularization Parameter
handles.sample              = 2000;                                                      % Number of samples used for Bayesian

% Options for RBF
handles.shape_control       = 'FWHM Coefficient';                                 % RBFH Shape Cotrol
handles.coeff               = 0.5;

%Calculate DRT
for n = 1:length(DRT_GUI.Messdaten.RelaxEIS(1,:))
    DRT_data                    = [transpose(DRT_GUI.Messdaten.RelaxEIS{2,n}), transpose(DRT_GUI.Messdaten.RelaxEIS{5,n}),transpose(DRT_GUI.Messdaten.RelaxEIS{6,n})];
    handles.data_exist          = true;
    
    % find incorrect rows with zero frequency
    DRT_data(DRT_data(:,1)==0,:)    =[];
    
    %   flip freq, Z_prime and Z_double_prime so that data are in the desceding
    %   order of freq
    if DRT_data(1,1) < DRT_data(end,1)
        DRT_data                     = fliplr(DRT_data')';
    end
    handles.freq                    = DRT_data(:,1);
    handles.Z_prime_mat             = DRT_data(:,2);
    handles.Z_double_prime_mat      = DRT_data(:,3);
    
    %   save original freq, Z_prime and Z_double_prime
    handles.freq_0                  = handles.freq;
    handles.Z_prime_mat_0           = handles.Z_prime_mat;
    handles.Z_double_prime_mat_0    = handles.Z_double_prime_mat;
    handles.Z_exp                   = handles.Z_prime_mat(:)+ 1i*handles.Z_double_prime_mat(:);
    handles.method_tag              = 'none';
    
    handles = DRT_inductance_treatment(handles);
    if strcmp(app.LHD_DRT_Method,'Simple')
        handles = DRT_run_simple(handles);
    elseif strcmp(app.LHD_DRT_Method,'Bayesian')
        handles = DRT_run_Bayesian(handles);
    else
        handles = DRT_run_BHT(handles);
    end
    
    DRT_data                    = [];
    DRT_data.Z_0                = DRT_GUI.Messdaten.RelaxEIS{7,n};
    DRT_data.Zreal_0            = DRT_GUI.Messdaten.RelaxEIS{5,n};
    DRT_data.Zimg_0             = DRT_GUI.Messdaten.RelaxEIS{6,n};
    DRT_data.Frequencies_0       = DRT_GUI.Messdaten.RelaxEIS{2,n};
    
    DRT_data.Frequencies         = handles.freq_fine;
    
    DRT_data.gamma              = handles.gamma_ridge_fine;
    if isfield(handles, 'out_scores')
        DRT_data.EIS_scores         = handles.out_scores;
    end
    if isfield(handles, 'gamma_mean_fine')
        DRT_data.gamma_mean         = handles.gamma_mean_fine;
    end
    if isfield(handles, 'gamma_mean_fine_re')
        DRT_data.gamma_mean_real    = handles.gamma_mean_fine_re;
    end
    if isfield(handles, 'gamma_mean_fine_im')
        DRT_data.gamma_mean_img     = handles.gamma_mean_fine_im;
    end
    
    if isfield(handles, 'lower_bound_fine')
        DRT_data.CI.lower_bound     = handles.lower_bound_fine;
    end
    if isfield(handles, 'upper_bound_fine')
        DRT_data.CI.upper_bound     = handles.upper_bound_fine;
    end
    DRT_data.tau                = 1./ DRT_GUI.Messdaten.RelaxEIS{2,n};
    app.LHD_DRT_Results{n}      = DRT_data;
    DRT_data                    = [];
end

%Plot Results
if ~app.GlobalFitInProgress
    cla(app.axesCRateDRT);
    max_value = -Inf;
    for n = 1:length(DRT_GUI.Messdaten.RelaxEIS(1,:))
        max_value = max([max_value ceil(max(app.LHD_DRT_Results{n}.gamma*100000))/100000]);
    end
    app.axesCRateDRT.YLim=[0 max_value];
    app.axesCRateDRT.XLimMode='auto';
    hold(app.axesCRateDRT,'on');
    % app.axesCRateDRT.XLimMode='auto';
    for n = 1:length(DRT_GUI.Messdaten.RelaxEIS(1,:))
        plot(app.axesCRateDRT,1./app.LHD_DRT_Results{n}.Frequencies,app.LHD_DRT_Results{n}.gamma,'-','LineWidth',1)
    end
    hold(app.axesCRateDRT,'on'); grid(app.axesCRateDRT,'on');
    app.axesCRateDRT.XLabel.Interpreter='latex'; app.axesCRateDRT.YLabel.Interpreter='latex'; app.axesCRateDRT.XLabel.String='$\tau/s$'; app.axesCRateDRT.YLabel.String='$\gamma (ln \tau)/\Omega$';
    for n = 1:length(DRT_GUI.Messdaten.RelaxEIS(1,:))
        legend_struct{n} = num2str(DRT_GUI.Messdaten.RelaxEIS{1,n}) ;
    end
    h1 = legend(app.axesCRate,legend_struct,'Location','NorthWest');
    set(h1,'Interpreter','none');
    set(app.axesCRateDRT,'xscale','log');
end
end

