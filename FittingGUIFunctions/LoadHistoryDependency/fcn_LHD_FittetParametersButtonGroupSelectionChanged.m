function app = fcn_LHD_FittetParametersButtonGroupSelectionChanged(app, event);      
if strcmp(app.LHD_FittetParametersButtonGroup.SelectedObject.Text,'MF - Model')
    app.LHD_DRT_Model = 'MF';
    if app.GUI
        set(app.LHD_ECEindicesEditField,'Editable','off')
    else
        app.LHD_ECEindicesEditField.Editable = 'off';
    end
elseif strcmp(app.LHD_FittetParametersButtonGroup.SelectedObject.Text,'Complete')
    app.LHD_DRT_Model = 'Complete';
    if app.GUI
        set(app.LHD_ECEindicesEditField,'Editable','off')
    else
        app.LHD_ECEindicesEditField.Editable = 'off';
    end
else 
    app.LHD_DRT_Model = 'Custom';
    if app.GUI
        set(app.LHD_ECEindicesEditField,'Editable','on')
    else
        app.LHD_ECEindicesEditField.Editable = 'on';
    end
end
fcn_LHD_UpdateParameterTable(app,event);
app.LHD_DRT_ParameterValues = [];
end

