function app = fcn_LHD_ExportaxesCRateRelaxDep(app, event)
global DRT_GUI
if isempty(app.LHD_DRT_ParameterValues)
    msgbox('There are no DRT fitting results')
    return
end

%compile data
index1 = [];
index1 = app.LHD_Depiction_Index;
if isempty(index1)
    return
end

for n = 1:length(app.LHD_ParameterTable_2.Data(:,1))
    if app.LHD_ParameterTable_2.Data{n,2}
        index2 = n;
    end
end

if index2 == 1
    index2 = 2;
else
    index2 = 1;
end

for n = 1:length(DRT_GUI.Messdaten.RelaxEIS(1,:))
    time(n) = DRT_GUI.Messdaten.RelaxEIS{1,n};
    data(n) = app.LHD_DRT_ParameterValues{n}{index1}{1}(index2);
end

h = figure('Name','LHD Parameter Course');
plot(gca,time,data,'-X')
hold(gca,'on'); grid(gca,'on'); set(gca,'FontSize',14);
xlabel('$t / s$','Interpreter','latex');
ylabel('$Parameter Value$','Interpreter','latex');
end

