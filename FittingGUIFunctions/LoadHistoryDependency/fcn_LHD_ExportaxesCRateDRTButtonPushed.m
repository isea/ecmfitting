function app = fcn_LHD_ExportaxesCRateDRTButtonPushed(app, event);
global DRT_GUI
figure('Name','LHD DRT','NumberTitle','off')
max_value = -Inf;
for n = 1:length(DRT_GUI.Messdaten.RelaxEIS(1,:))
    max_value = max([max_value ceil(max(app.LHD_DRT_Results{n}.gamma*100000))/100000]);
end
set(gca,'YLim',[0 max_value]);
set(gca,'XLimMode','auto');
hold(gca,'on');
for n = 1:length(DRT_GUI.Messdaten.RelaxEIS(1,:))
    plot(gca,1./app.LHD_DRT_Results{n}.Frequencies,app.LHD_DRT_Results{n}.gamma,'-','LineWidth',1)
end
hold(gca,'on'); grid(gca,'on');  %axis(app.axes1,'equal');
xlabel('$\tau / s$','Interpreter','latex');
ylabel('$\gamma (ln \tau)/\Omega$','Interpreter','latex');
for n = 1:length(DRT_GUI.Messdaten.RelaxEIS(1,:))
   legend_struct{n} = num2str(DRT_GUI.Messdaten.RelaxEIS{1,n}) ;
end
h1 = legend(gca,legend_struct,'Location','NorthEast');
set(h1,'Interpreter','none');
set(gca,'xscale','log');
end

