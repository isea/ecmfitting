function [app, p_best] = fcn_LHD_FitTimeConstantButtonPushed(app,event)
global DRT_GUI
for n = 1:length(app.LHD_ParameterTable.Data(:,1))
    if app.LHD_ParameterTable.Data{n,2}
        index1 = n;
        continue
    end
end
for n = 1:length(app.LHD_ParameterTable_2.Data(:,1))
    if app.LHD_ParameterTable_2.Data{n,2}
        index2 = n;
        continue
    end
end
if index2 == 1
    index2 = 2;
else
    index2 = 1;
end
for n = 1:length(DRT_GUI.Messdaten.RelaxEIS(1,:))
    time(n) = DRT_GUI.Messdaten.RelaxEIS{1,n};
    data(n) = app.LHD_DRT_ParameterValues{n}{index1}{1}(index2);
end
if ~isempty(find(diff(sign(diff(data)))))
    pos_idx = find(sign(diff(data))==1);
    neg_idx = find(sign(diff(data))==-1);
    if length(pos_idx) > length(data)/2
        corr_sign = 1;
    elseif length(neg_idx) > length(data)/2 
        corr_sign = -1;
    else
        if length(pos_idx) > length(neg_idx)
            corr_sign = 1;
        elseif length(neg_idx) > length(pos_idx)
            corr_sign = -1;
        else
            return
        end
    end
    idx = find(sign(diff(data))~=corr_sign);
    if length(idx) == 1
        time(idx) = [];
        data(idx) = [];
    elseif length(idx) > length(data)/2
        time(~idx) = [];
        data(~idx) = [];    
    else
        time(idx) = [];
        data(idx) = [];
    end   
end

options = optimset('MaxIter',2000000,'MaxFunEvals',2000000,'TolX',1e-10,'TolFun',1e-10);
% formula = ['p(1) * exp(-w*p(2)) + ' num2str(mean(data(end)))];
% [p_best,fval,exitflag,output]=function_fit_easyfit2(app,time,[data zeros(size(data))],[0, 0, 0, 0],@function_model_all_types2, [0, 0, 0, 0], Inf * ones(1,4) ,options, formula);
if data(end) < data(1)
    formula = ['p(1) * exp(-w.*p(2)) + p(3) * exp(-w.*p(4)) +' num2str(mean(data(end)))];
    if abs((data(1)-data(end))/data(end))>1/100        
    %     formula = ['p(1) * exp(-w*p(2)) + p(3) * exp(-w*p(4)) + p(5)'];
        [p_best,fval,exitflag,output]=function_fit_easyfit2(app,time,[data zeros(size(data))],[0, 0, 0, 0],@function_model_all_types2, [0, 1e-9, 0, 1e-9], Inf * ones(1,4) ,options, formula);
    else
        p_best = [0.5, 0 , 0.5, 0];        
    end
else
    formula = ['p(1) * (1 - exp(-w.*p(2))) + p(3) * (1 - exp(-w.*p(4))) +' num2str(mean(data(1)))];
    if abs((data(end)-data(1))/data(1))>1/100        
        [p_best,fval,exitflag,output]=function_fit_easyfit2(app,time,[data zeros(size(data))],[0, 0, 0, 0],@function_model_all_types2, [0, 1e-9, 0, 1e-9], Inf * ones(1,4) ,options, formula);
    else
         p_best = [0.5, 0 , 0.5, 0];     
    end
end
p = p_best;
w = time;
f_real = real(eval(formula));
f_imag = imag(eval(formula));
if ~app.GlobalFitInProgress
    figure('Name','LHD Time Constant Fit','NumberTitle','off');
    plot(time,data,'-X','LineWidth',2)
    hold(gca,'on');grid(gca,'on');
    plot(time,f_real,'LineWidth',2)
    legend('Data','Fit')
    xlabel('t / s')
    ylabel('Parameter Value')
end
total = (p_best(1)+p_best(3));
p_best(1) = p_best(1)/total;
p_best(2) = p_best(2);
p_best(3) = p_best(3)/total;
p_best(4) = p_best(4);
if p_best(2) > p_best(4)
    temp_best = [p_best(1) p_best(2)];
    p_best(1) = p_best(3);
    p_best(2) = p_best(4);
    p_best(3) = temp_best(1);
    p_best(4) = temp_best(2);
end

%Fill Table
TableData = cell(4,2);
TableData{1,1} = 'f1';
TableData{2,1} = 'Tau1';
TableData{3,1} = 'f2';
TableData{4,1} = 'Tau2';

TableData{1,2} = p_best(1);
TableData{2,2} = p_best(2)';
TableData{3,2} = p_best(3);
TableData{4,2} = p_best(4);
set(app.LHD_OSM_ParameterTable,'Data',TableData)
end

