function app = fcn_LHD_LinKKButtonPushed(app, event)
global DRT_GUI
for n = 1:length(DRT_GUI.Messdaten.RelaxEIS(1,:))
    
    [app,f_real,f_imag] = linKK_lsqr_ind(app,transpose(DRT_GUI.Messdaten.RelaxEIS{2,n}),transpose(DRT_GUI.Messdaten.RelaxEIS{5,n}),transpose(DRT_GUI.Messdaten.RelaxEIS{6,n}));
    m_real = transpose(DRT_GUI.Messdaten.RelaxEIS{5,n});
    m_imag = transpose(DRT_GUI.Messdaten.RelaxEIS{6,n});
    m_f = transpose(DRT_GUI.Messdaten.RelaxEIS{2,n});
    m_Z = transpose(DRT_GUI.Messdaten.RelaxEIS{7,n});
    
    %create output folder or stop if data are non-existent
    if isempty(DRT_GUI) || isempty(cell2mat(strfind(fieldnames(DRT_GUI),'Messdaten')))
        return
    end
    if isempty(dir('export'))
        mkdir('export')
    end
    if isempty( dir(['export' '/' DRT_GUI.Testparameter.Batterie]))
        mkdir(['export' '/' DRT_GUI.Testparameter.Batterie])
    end
    FarbenLaden();
    
    %create figures
    %Nyquist
    h = figure('Name','LinKK Visualization','NumberTitle','off','UnitS','centimeters','Position',[0, 0, 25, 25]);
    subplot(4,5,[4:5 9:10])
    plot(m_real,m_imag,'o','DisplayName','Measurement','color',RWTHBlau,'LineWidth',1,'MarkerSize',7);hold(gca,'on');
    plot(f_real,f_imag,'x','DisplayName','Fit','color',RWTHRot,'LineWidth',1,'MarkerSize',7);
    set(gca,'YLim',[floor(min(m_imag*100000))/100000, ceil(max(m_imag*100000))/100000]);
    set(gca,'XLimMode','auto');
    grid on; axis square; axis equal; set(gca,'ydir', 'reverse');
    xlabel('$\Re\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
    ylabel('$\Im\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');

    %real part over frequency
    subplot(4,5,1:3)
    semilogx(m_f,real(m_Z),'o','color',RWTHBlau,'DisplayName','Measurement','LineWidth',1,'MarkerSize',7);hold(gca,'on');
    semilogx(m_f,f_real,'o','color',RWTHRot,'DisplayName','Fit','LineWidth',1,'MarkerSize',7);
    set(gca,'xdir','reverse'),grid(gca,'on'), hold(gca,'on');
    set(gca,'TickLabelInterpreter','latex')
    h1 = legend(gca,'show','Location','NorthWest');
    set(h1,'Interpreter','latex');
    ylabel(gca,'$\Re\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
    xlabel(gca,'$f$ in Hz','Interpreter','latex');

    %imag part over frequency
    subplot(4,5,6:8)
    semilogx(m_f,imag(m_Z),'o','color',RWTHBlau,'DisplayName','Measurement','LineWidth',1,'MarkerSize',7);hold(gca,'on');
    semilogx(m_f,f_imag,'o','color',RWTHRot,'DisplayName','Fit','LineWidth',1,'MarkerSize',7);
    set(gca,'xdir','reverse'),grid(gca,'on'), hold(gca,'on');
    set(gca,'TickLabelInterpreter','latex')
    h1 = legend(gca,'show','Location','NorthWest');
    set(h1,'Interpreter','latex');
    ylabel(gca,'$\Im\{\underline{Z}\}$ in $\Omega$','Interpreter','latex');
    xlabel(gca,'$f$ in Hz','Interpreter','latex');
    set(gca,'ydir', 'reverse');

    %residuals over freqency
    subplot(4,5,11:20)
    delta_real = ((real(m_Z)-f_real)./abs(f_real+1i*f_imag))*100;
    delta_imag = ((imag(m_Z)-f_imag)./abs(f_real+1i*f_imag))*100;
    semilogx(m_f,delta_real,'o','color',RWTHBlau,'DisplayName','Real residual','LineWidth',1,'MarkerSize',7);hold(gca,'on');
    semilogx(m_f,delta_imag,'o','color',RWTHRot,'DisplayName','Imag residual','LineWidth',1,'MarkerSize',7);
    set(gca,'xdir','reverse'),grid(gca,'on'); hold(gca,'on');
    set(gca,'TickLabelInterpreter','latex')
    h1 = legend(gca,'show','Location','NorthWest');
    set(h1,'Interpreter','latex');
    ylabel(gca,'Residuals in %','Interpreter','latex');
    xlabel(gca,'$f$ in Hz','Interpreter','latex');
    set(gca, 'YLim',[min(min(delta_imag),min(delta_real)) max(max(delta_imag),max(delta_real))])
    savefig(h,['export/' DRT_GUI.Testparameter.Batterie '/' DRT_GUI.Testparameter.Batterie '_LinKK_' ...
                app.StateString '_' strrep(num2str(DRT_GUI.Testparameter.Temperatur),'-','m') 'grad_'...
                strrep(num2str(DRT_GUI.Testparameter.SOC),'-','m') 'SOC_'...
                num2str(DRT_GUI.Messdaten.RelaxEIS{1,n}) '.fig'])
    close(h)
end
end

