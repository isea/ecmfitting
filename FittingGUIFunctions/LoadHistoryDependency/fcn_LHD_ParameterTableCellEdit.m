function app = fcn_LHD_ParameterTableCellEdit(app, event)
%Fit selected parameter as a function of time in axesCRateRelaxDep 
global DRT_GUI
if isempty(app.LHD_DRT_ParameterValues)
    msgbox('There are no DRT fitting results')
    return
end

%uncheck all other checkboxes
aim = app.LHD_ParameterTable.Data{event.Indices(1),2};
if aim 
    for n = 1:length(app.LHD_ParameterTable.Data(:,1))
        if event.Indices(1) ~= n
            app.LHD_ParameterTable.Data{n,2} = false;              
        end
    end
end

%is any checkbox checked?
checked_boxes = 0;
for n = 1:length(app.LHD_ParameterTable.Data(:,1))
    if app.LHD_ParameterTable.Data{n,2}
        checked_boxes = 1;
        continue
    end
end
if ~checked_boxes
    return
end

%identify ESB Element
ESB_index = app.LHD_ECE_Indices(event.Indices(1));
app.LHD_Depiction_Index = event.Indices(1);

%update LHD_ParameterTable_2
TableData(1,1)= {'R'};
TableData(1,2)= {[false]};
TableData(2,1)= {'Tau'};
TableData(2,2)= {[false]};
if app.GUI
    set(app.LHD_ParameterTable_2,'Data',TableData);
    set(app.LHD_ParameterTable_2,'ColumnEditable',true);
else
    app.LHD_ParameterTable_2.Data = TableData;
    app.LHD_ParameterTable_2.ColumnEditable = true;
end
end

