function [Data_lookup] = convert_Fit_into_DataArray_LHD(app,lhd,SOC,temp,SOH,stepsize,check,Hyst,Crate);
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here
newTemp = [];
newCrate = [];
newHyst = [];
try
    if app.HysteresisFlag && app.CRateFlag
        param_length = 0;
        for m = 1:length(Hyst)
            for n = 1:length(app.sorted_crate{m})
                for n1 = 1:length(app.sorted_temp{m}{n})
                    for n2 = 1:length(app.sorted_soc{m}{n}{n1})
                        if ~isempty(lhd(m,n,n2,n1,SOH).Parameter)
                            param_length = length(lhd(m,n,n2,n1,SOH).Parameter);
                            break;
                        end
                    end
                end
            end
        end
        
        newHyst = Hyst;
        %which temperatures and SOCs are interpolated
        newSOC = [-10:stepsize:100];
        if check
            temp1=[newSOC,SOC];
            newSOC=unique(temp1);
        end
        iSOC = [];
        
        newTemp = [floor(min(temp)/10)*10:10:ceil(max(temp)/10)*10];
        iTemp = [];
        tTemp = [];
        
        newCrate = [floor(min(Crate)) :ceil(max(Crate))];
        tCrate = [];
        iCrate = [];
        
        Data_lookup = zeros(length(Hyst),length(newCrate),length(newTemp),length(newSOC),param_length);
        
        %iterate over all Crates
        for n = 1:length(Hyst)
            for i=1:length(app.sorted_crate{n})
                tCrate(i) = app.sorted_crate{n}(i);
                iCrate(i) = find(Crate == app.sorted_crate{n}(i));
            end
            Data_lookup1{n} = [];
            for n1 = iCrate
                for i=1:length(app.sorted_temp{n}{n1})
                    tTemp(i) = app.sorted_temp{n}{n1}(i);
                    iTemp(i) = find(temp == app.sorted_temp{n}{n1}(i));
                end
                Data_lookup1{n} = zeros(1,length(Crate),length(temp),length(newSOC),param_length);
                
                % Gehe durch alle Temperaturen
                for itemp = iTemp
                    % Entferne leere Einträge
                    j=1;
                    tSOC = [];
                    iSOC = [];
                    for i=1:length(SOC)
                        if ~isempty(lhd(n,n1,i,itemp,SOH).Parameter)
                            tSOC(j) = SOC(i);
                            iSOC(j) = i;
                            j =j+1;
                        end
                    end
                    
                    %find parameter length
                    for i = iSOC
                        if ~isempty(lhd(n,n1,i,itemp,SOH).Parameter)
                            param_length = length(lhd(n,n1,i,itemp,SOH).Parameter);
                            break;
                        end
                    end
                    Data_lookup = zeros(length(Hyst),length(newCrate),length(newTemp),length(newSOC),param_length);
                    
                    %Alle vorhandenen Daten sammeln
                    %Durch alle Parameter iterieren
                    for k=1:param_length
                        param = zeros(1,length(tSOC));
                        for i=1:length(tSOC)
                            param(i) = lhd(n,n1,iSOC(i),itemp,SOH).Parameter(k);
                        end
                        if length(tSOC) ==1
                            % use constant parameter over SOC range, if only one SOC was
                            % tested
                            new_param = interp1([min(newSOC);tSOC],[param;param],newSOC,'linear','extrap');
                        else
                            new_param = interp1(tSOC,param,newSOC,'linear','extrap');
                        end
                        for n2 = 1:length(new_param)
                            if new_param(n2) < 0
                                new_param(n2) = 1e-9;
                            end
                        end
                        Data_lookup(n,n1,itemp,:,k) = new_param;
                    end
                end
                
                for i1 = 1:length(newSOC)
                    %Alle vorhandenen Daten sammeln
                    %Durch alle Parameter iterieren
                    for k=1:param_length
                        param = zeros(1,length(tTemp));
                        for i=1:length(tTemp)
                            param(i) = Data_lookup1(n,n1,i,i1,k);
                        end
                        if length(tTemp) ==1
                            % use constant parameter over SOC range, if only one SOC was
                            % tested
                            new_param = interp1([min(newTemp);tTemp],[param;param],newTemp,'linear','extrap');
                        else
                            new_param = interp1(tTemp,param,newTemp,'linear','extrap');
                        end
                        for n2 = 1:length(new_param)
                            if new_param(n2) < 0
                                new_param(n2) = 1e-9;
                            end
                        end
                        Data_lookup(n,n1,:,i1,k) = new_param;
                    end
                end
            end
            
            for i2 = 1:length(newTemp)
                for i1 = 1:length(newSOC)
                    %Alle vorhandenen Daten sammeln
                    %Durch alle Parameter iterieren
                    for k=1:param_length
                        param = zeros(1,length(tCrate));
                        for i=1:length(tCrate)
                            param(i) = Data_lookup1(n,i,i2,i1,k);
                        end
                        if length(tCrate) ==1
                            % use constant parameter over SOC range, if only one SOC was
                            % tested
                            new_param = interp1([min(newCrate);tCrate],[param;param],newCrate,'linear','extrap');
                        else
                            new_param = interp1(tCrate,param,newCrate,'linear','extrap');
                        end
                        for n2 = 1:length(new_param)
                            if new_param(n2) < 0
                                new_param(n2) = 1e-9;
                            end
                        end
                        Data_lookup(n,:,i2,i1,k) = new_param;
                    end
                end
            end
        end
    elseif app.HysteresisFlag && length(Hyst) > 1
        newCrate = 0;
        newHyst = Hyst;
        %which temperatures and SOCs are interpolated
        newSOC = [-10:stepsize:100];
        if check
            temp1=[newSOC,SOC];
            newSOC=unique(temp1);
        end
        iSOC = [];
        
        newTemp = [floor(min(temp)/10)*10:10:ceil(max(temp)/10)*10];
        iTemp = [];
        tTemp = [];
        
        param_length = 0;
        for n = 1:length(Crate)
            for n1 = 1:length(app.sorted_temp{1}{n})
                for n2 = 1:length(app.sorted_soc{1}{n}{n1})
                    if ~isempty(lhd(1,n,n2,n1,SOH).Parameter)
                        param_length = length(lhd(1,n,n2,n1,SOH).Parameter);
                        break;
                    end
                end
            end
        end
        Data_lookup = zeros(length(Hyst),1,length(newTemp),length(newSOC),param_length);
        
        %iterate over all Crates
        for n = 1:length(Hyst)
            for i=1:length(app.sorted_temp{n}{1})
                tTemp(i) = app.sorted_temp{n}{1}(i);
                iTemp(i) = find(temp == app.sorted_temp{n}{1}(i));
            end
            Data_lookup1 = zeros(length(Hyst),1,length(temp),length(newSOC),param_length);
            
            % Gehe durch alle Temperaturen
            for itemp = iTemp
                % Entferne leere Einträge
                j=1;
                tSOC = [];
                iSOC = [];
                for i=1:length(SOC)
                    if ~isempty(lhd(n,1,i,itemp,SOH).Parameter)
                        tSOC(j) = SOC(i);
                        iSOC(j) = i;
                        j =j+1;
                    end
                end
                
                %find parameter length
                for i = iSOC
                    if ~isempty(lhd(n,1,i,itemp,SOH).Parameter)
                        param_length = length(lhd(n,1,i,itemp,SOH).Parameter);
                        break;
                    end
                end
                
                %Alle vorhandenen Daten sammeln
                %Durch alle Parameter iterieren
                for k=1:param_length
                    param = zeros(1,length(tSOC));
                    for i=1:length(tSOC)
                        param(i) = lhd(n,1,iSOC(i),itemp,SOH).Parameter(k);
                    end
                    if length(tSOC) ==1
                        % use constant parameter over SOC range, if only one SOC was
                        % tested
                        new_param = interp1([min(newSOC);tSOC],[param;param],newSOC,'linear','extrap');
                    else
                        new_param = interp1(tSOC,param,newSOC,'linear','extrap');
                    end
                    for n1 = 1:length(new_param)
                        if new_param(n1) < 0
                            new_param(n1) = 1e-9;
                        end
                    end
                    Data_lookup1(n,1,itemp,:,k) = new_param;
                end
            end
            
            for i1 = 1:length(newSOC)
                %Alle vorhandenen Daten sammeln
                %Durch alle Parameter iterieren
                for k=1:param_length
                    param = zeros(1,length(tTemp));
                    for i=1:length(tTemp)
                        param(i) = Data_lookup1(n,1,i,i1,k);
                    end
                    if length(tTemp) ==1
                        % use constant parameter over SOC range, if only one SOC was
                        % tested
                        new_param = interp1([min(newTemp);tTemp],[param;param],newTemp,'linear','extrap');
                    else
                        new_param = interp1(tTemp,param,newTemp,'linear','extrap');
                    end
                    for n1 = 1:length(new_param)
                        if new_param(n1) < 0
                            new_param(n1) = 1e-9;
                        end
                    end
                    Data_lookup(n,1,:,i1,k) = new_param;
                end
            end
        end
    elseif app.CRateFlag && length(Crate) > 1
        newCrate = Crate;
        newHyst = 0;
        %which temperatures and SOCs are interpolated
        newSOC = [-10:stepsize:100];
        if check
            temp1=[newSOC,SOC];
            newSOC=unique(temp1);
        end
        iSOC = [];
        
        newTemp = [floor(min(temp)/10)*10:10:ceil(max(temp)/10)*10];
        iTemp = [];
        tTemp = [];
        
        param_length = 0;
        for n = 1:length(Crate)
            for n1 = 1:length(app.sorted_temp{1}{n})
                for n2 = 1:length(app.sorted_soc{1}{n}{n1})
                    if ~isempty(lhd(1,n,n2,n1,SOH).Parameter)
                        param_length = length(lhd(1,n,n2,n1,SOH).Parameter);
                        break;
                    end
                end
            end
        end
        Data_lookup = zeros(1,length(Crate)-1,length(newTemp),length(newSOC),param_length);
        counter = 1;
        %iterate over all Crates
        for n = 1:length(Crate)           
           %if Crate(n) ~= 0
                for i=1:length(app.sorted_temp{1}{n})
                    tTemp(i) = app.sorted_temp{1}{n}(i);
                    iTemp(i) = find(temp == app.sorted_temp{1}{n}(i));
                end
                Data_lookup1 = zeros(1,length(Crate)-1,length(temp),length(newSOC),param_length);
                
                % Gehe durch alle Temperaturen
                for itemp = iTemp
                    % Entferne leere Einträge
                    j=1;
                    tSOC = [];
                    iSOC = [];
                    for i=1:length(SOC)
                        if ~isempty(lhd(1,n,i,itemp,SOH).Parameter)
                            tSOC(j) = SOC(i);
                            iSOC(j) = i;
                            j =j+1;
                        end
                    end
                    
                    %find parameter length
                    for i = iSOC
                        if ~isempty(lhd(1,n,i,itemp,SOH).Parameter)
                            param_length = length(lhd(1,n,i,itemp,SOH).Parameter);
                            break;
                        end
                    end
                    
                    %Alle vorhandenen Daten sammeln
                    %Durch alle Parameter iterieren
                    for k=1:param_length
                        param = zeros(1,length(tSOC));
                        for i=1:length(tSOC)
                            param(i) = lhd(1,n,iSOC(i),itemp,SOH).Parameter(k);
                        end
                        if length(tSOC) ==1
                            % use constant parameter over SOC range, if only one SOC was
                            % tested
                            new_param = interp1([min(newSOC);tSOC],[param;param],newSOC,'linear','extrap');
                        else
                            new_param = interp1(tSOC,param,newSOC,'linear','extrap');
                        end
                        for n1 = 1:length(new_param)
                            if new_param(n1) < 0
                                new_param(n1) = 1e-9;
                            end
                        end
                        Data_lookup1(1,counter,itemp,:,k) = new_param;
                    end
                end
                
                for i1 = 1:length(newSOC)
                    %Alle vorhandenen Daten sammeln
                    %Durch alle Parameter iterieren
                    for k=1:param_length
                        param = zeros(1,length(tTemp));
                        for i=1:length(tTemp)
                            param(i) = Data_lookup1(1,counter,i,i1,k);
                        end
                        if length(tTemp) ==1
                            % use constant parameter over SOC range, if only one SOC was
                            % tested
                            new_param = interp1([min(newTemp);tTemp],[param;param],newTemp,'linear','extrap');
                        else
                            new_param = interp1(tTemp,param,newTemp,'linear','extrap');
                        end
                        for n1 = 1:length(new_param)
                            if new_param(n1) < 0
                                new_param(n1) = 1e-9;
                            end
                        end
                        Data_lookup(1,counter,:,i1,k) = new_param;
                    end
                end
                counter = counter + 1;
            %end
        end
    else
        newCrate = 0;
        newHyst = 0;
        %considered SOCs
        newSOC = [-10:stepsize:100];
        if check
            temp1=[newSOC,SOC];
            newSOC=unique(temp1);
        end
        iSOC = [];
        %note that all different SOCs are present so if one temp has 3.5345
        % as their first SOC, probably no other temp has a value for
        % exactly this SOC
        for i=1:length(temp)
            if ~isempty(lhd(1,1,1,i,SOH).Parameter)
                param_length = length(lhd(1,1,1,i,SOH).Parameter);
                break;
            end
        end
        Data_lookup = zeros(1,1,length(temp),length(newSOC),param_length);
        
        % Gehe durch alle Temperaturen
        for itemp=1:length(temp)
            % Entferne leere Einträge
            j=1;
            tSOC = [];
            for i=1:length(SOC)
                if ~isempty(lhd(1,1,i,itemp,SOH).Parameter)
                    tSOC(j) = SOC(i);
                    iSOC(j) = i;
                    j =j+1;
                end
            end
            %Alle vorhandenen Daten sammeln
            %Durch alle Parameter iterieren
            for k=1:param_length
                param = zeros(1,length(tSOC));
                for i=1:length(tSOC)
                    param(i) = lhd(1,1,iSOC(i),itemp,SOH).Parameter(k);
                end
                if length(tSOC) ==1
                    % use constant parameter over SOC range, if only one SOC was
                    % tested
                    new_param = interp1([min(newSOC);tSOC],[param;param],newSOC,'linear','extrap');
                else
                    new_param = interp1(tSOC,param,newSOC,'linear','extrap');
                end
                for n = 1:length(new_param)
                    if new_param(n) < 0
                        new_param(n) = 1e-9;
                    end
                end
                Data_lookup(1,1,itemp,:,k) = new_param;
            end
        end
    end
catch
    
end
end

