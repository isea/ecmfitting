function app = fcn_LHD_TabInitialization(app,event)
global DRT_GUI
%check whether data 
CRate_var = strrep(DRT_GUI.Testparameter.Zustand,'m','-');
CRate_var = str2num(strrep(CRate_var(1:end-4),'_','.'));
if ~isfield(DRT_GUI.Messdaten,'RelaxEIS') || isempty(DRT_GUI.Messdaten.RelaxEIS) || CRate_var == 0
    for n = 1:length(app.LoadHistoryTab.Children)
        if isprop(app.LoadHistoryTab.Children(n),'Enable')
            app.LoadHistoryTab.Children(n).Enable = 'off';
        end
    end
    msgbox('No Data Present')
    return
else
    for n = 1:length(app.LoadHistoryTab.Children)
        if isprop(app.LoadHistoryTab.Children(n),'Enable')
            app.LoadHistoryTab.Children(n).Enable = 'on';
        end
    end
end
cla(app.axesCRate);
app.axesCRate.YLim=[min(DRT_GUI.Messdaten.RelaxEIS{6,1}*100000)/100000 ceil(max(DRT_GUI.Messdaten.RelaxEIS{6,1}*100000))/100000];
app.axesCRate.XLimMode='auto';
hold(app.axesCRate,'on');
% app.axesCRate.XLimMode='auto';
for n = 1:length(DRT_GUI.Messdaten.RelaxEIS(1,:))
    plot(app.axesCRate,DRT_GUI.Messdaten.RelaxEIS{5,n},DRT_GUI.Messdaten.RelaxEIS{6,n},'o','LineWidth',1,'MarkerSize',7)
end
hold(app.axesCRate,'on'); grid(app.axesCRate,'on'); set(app.axesCRate,'ydir', 'reverse'); %axis(app.axes1,'equal');
app.axesCRate.XLabel.Interpreter='latex'; app.axesCRate.YLabel.Interpreter='latex'; app.axesCRate.XLabel.String='$\Re\{\underline Z\}$ in $\Omega$'; app.axesCRate.YLabel.String='$\Im\{\underline Z\}$ in $\Omega$'; 
for n = 1:length(DRT_GUI.Messdaten.RelaxEIS(1,:))
   legend_struct{n} = num2str(DRT_GUI.Messdaten.RelaxEIS{1,n}) ;
end
h1 = legend(app.axesCRate,legend_struct,'Location','NorthWest');
set(h1,'Interpreter','none');

fcn_LHD_UpdateParameterTable(app,event);
end

