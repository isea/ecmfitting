function app = fcn_LHD_DRTButtonGroupSelectionChanged(app, event);
    if strcmp(app.LHD_DRTButtonGroup.SelectedObject.Text,'Simple')
        app.LHD_DRT_Method = 'Simple';
    elseif strcmp(app.LHD_DRTButtonGroup.SelectedObject.Text,'Bayesian')
        app.LHD_DRT_Method = 'Bayesian';
    else
        app.LHD_DRT_Method = 'BayesianHilbertTransform';  
    end
end