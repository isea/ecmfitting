function app = fcn_LHD_ParameterTable_2CellEdit(app, event)
global DRT_GUI
if isempty(app.LHD_DRT_ParameterValues)
    msgbox('There are no DRT fitting results')
    return
end

%uncheck all other checkboxes
aim = app.LHD_ParameterTable_2.Data{event.Indices(1),2};
if aim 
    for n = 1:length(app.LHD_ParameterTable_2.Data(:,1))
        if event.Indices(1) ~= n
            app.LHD_ParameterTable_2.Data{n,2} = false;              
        end
    end
end

%is any checkbox checked?
checked_boxes = 0;
for n = 1:length(app.LHD_ParameterTable_2.Data(:,1))
    if app.LHD_ParameterTable_2.Data{n,2}
        checked_boxes = 1;
        continue
    end
end
if ~checked_boxes
    return
end

%compile data
index1 = app.LHD_Depiction_Index;
if event.Indices(1) == 1
    index2 = 2;
else
    index2 = 1;
end
for n = 1:length(DRT_GUI.Messdaten.RelaxEIS(1,:))
    time(n) = DRT_GUI.Messdaten.RelaxEIS{1,n};
    data(n) = app.LHD_DRT_ParameterValues{n}{index1}{1}(index2);
end

%plot 
if ~app.GlobalFitInProgress
    cla(app.axesCRateRelaxDep)
    plot(app.axesCRateRelaxDep,time,data,'-X')
    hold(app.axesCRateRelaxDep,'on'); grid(app.axesCRateRelaxDep,'on'); set(app.axesCRateRelaxDep,'FontSize',14);
    xlabel(app.axesCRateRelaxDep,'$t / s$','Interpreter','latex');
    ylabel(app.axesCRateRelaxDep,'$Parameter Value$','Interpreter','latex');
end
end

