function app = fcn_LHD_UpdateParameterTable(app,event);
global DRT_GUI
counter = 1;
indices = [];
if strcmp(app.LHD_DRT_Model,'MF')
    for n =1:length(DRT_GUI.Fit.aktuell_Modell.ModellCell{8})      
        if ~strcmp( DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n}.Zfun_MF,'0')
            TableData(counter,1) = {DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n}.Funktionsname};
            TableData(counter,2 )= {[false]};
            indices = [indices n];
            counter = counter + 1;
        end           
    end
elseif strcmp(app.LHD_DRT_Model,'Complete')
    for n =1:length(DRT_GUI.Fit.aktuell_Modell.ModellCell{8})      
        TableData(counter,1) = {DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n}.Funktionsname};
        TableData(counter,2 )= {[false]};
        indices = [indices n];
        counter = counter + 1;             
    end
else
    ECE_indices = str2num(app.LHD_ECEindicesEditField.Value);
    for n = ECE_indices     
        TableData(counter,1) = {DRT_GUI.Fit.aktuell_Modell.ModellCell{8}{n}.Funktionsname};
        TableData(counter,2 )= {[false]};
        indices = [indices n];
        counter = counter + 1;             
    end
end
if ~exist('TableData','var')
    TableData = [];
end
if app.GUI
    set(app.LHD_ParameterTable,'Data',TableData);
    set(app.LHD_ParameterTable,'ColumnEditable',true);
else
    app.LHD_ParameterTable.Data = TableData;
    app.LHD_ParameterTable.ColumnEditable = true;
end
app.LHD_ECE_Indices = indices;
end

