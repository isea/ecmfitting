function app = fcn_LHD_ExportaxesCRateButtonPushed(app, event);
global DRT_GUI
figure('Name','LHD Nyquist','NumberTitle','off')
set(gca,'YLim',[min(DRT_GUI.Messdaten.RelaxEIS{6,1}*100000)/100000 ceil(max(DRT_GUI.Messdaten.RelaxEIS{6,1}*100000))/100000]);
set(gca,'XLimMode','auto');
hold(gca,'on');
for n = 1:length(DRT_GUI.Messdaten.RelaxEIS(1,:))
    plot(gca,DRT_GUI.Messdaten.RelaxEIS{5,n},DRT_GUI.Messdaten.RelaxEIS{6,n},'o','LineWidth',1,'MarkerSize',7)
end
hold(gca,'on'); grid(gca,'on'); set(gca,'ydir', 'reverse'); %axis(app.axes1,'equal');
xlabel('$\Re\{\underline Z\}$ in $\Omega$','Interpreter','latex');
ylabel('$\Im\{\underline Z\}$ in $\Omega$','Interpreter','latex');
for n = 1:length(DRT_GUI.Messdaten.RelaxEIS(1,:))
   legend_struct{n} = num2str(DRT_GUI.Messdaten.RelaxEIS{1,n}) ;
end
h1 = legend(gca,legend_struct,'Location','NorthWest');
set(h1,'Interpreter','none');
end

