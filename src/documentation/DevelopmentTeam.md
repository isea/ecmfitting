Development Team
============

+ Heiko Witzenhausen (Main Developer, Fitting Algorithms, DRT)
+ Stephan Bihn (Main Developer, Graphical User Interface, Automatisation)
+ Jonas Rinner (Main Developer, Load-History Dependency)