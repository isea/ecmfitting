function [new_colormap] = create_personalized_colorbar(old_colormap)
%Funktion erstellt eine Colormap, mit den richtigen Unterteilungen
%   old_colormap = Colormap die angepasst werden soll
%   new_colormap = Colormap die richtige Abschnitte besitzt

new_colormap = old_colormap;

new_colormap(1:4,:) = repmat([0,1,0],4,1); 
new_colormap(5:8,:) = repmat([0,0.5,0],4,1); 
new_colormap(9:12,:) = repmat([1,1,0],4,1); 
new_colormap(13:16,:) = repmat([0.5,0.5,0],4,1); 
new_colormap(17:20,:) = repmat([1,0,0],4,1); 
new_colormap(21:24,:) = repmat([0.5,0,0],4,1);
new_colormap(25:28,:) = repmat([1,0,1],4,1);
new_colormap(29:32,:) = repmat([0.5,0,0.5],4,1);
new_colormap(33:36,:) = repmat([0,0,1],4,1);
new_colormap(37:40,:) = repmat([0,0,0.5],4,1);
new_colormap(41:44,:) = repmat([0,1,1],4,1);
new_colormap(45:48,:) = repmat([0,0.5,0.5],4,1);
new_colormap(49:52,:) = repmat([0.75,0.75,0.75],4,1);
new_colormap(53:56,:) = repmat([0.5,0.5,0.5],4,1);
new_colormap(57:60,:) = repmat([0.25,0.25,0.25],4,1);
new_colormap(61:64,:) = repmat([0,0,0],4,1);

end

