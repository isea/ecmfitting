master=load('C:\hwi\Sam_I3_001\HV-Modal=SAM_I3_01=EIS_Param_025grad=Model Parameterization=2015-10-08 142525=hwi_nmc_eisdchcha_07=TS063298  Format01=Kreis 6-004.mat');
QOCV_Messung=load('C:\hwi\Sam_I3_001\HV-Modal=SAM_I3_01=QOCV_025grad=Model Parameterization=2016-02-10 161412=hwi_nmc_qocv_1=TS073815  Format01=Kreis 6-004.mat');

StepIndex = [ 1 1+find(abs(diff(master.diga.daten.Schritt)) > 0) numel(master.diga.daten.Schritt)+1];
master.diga.daten.AhAkku(1:(StepIndex(2)-1))=master.diga.daten.AhStep(1:(StepIndex(2)-1));
for i_step = 2:(numel(StepIndex)-1)
    if strcmp(master.diga.daten.Zustand(StepIndex(i_step)),'DCH')
        master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1)) = ...
            master.diga.daten.AhAkku(StepIndex(i_step)-1) - master.diga.daten.AhStep(StepIndex(i_step):(StepIndex(i_step+1)-1));
    else
        master.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1)) = ...
            master.diga.daten.AhAkku(StepIndex(i_step)-1) + master.diga.daten.AhStep(StepIndex(i_step):(StepIndex(i_step+1)-1));
    end
end
master.diga.daten.AhAkku = master.diga.daten.AhAkku-max(master.diga.daten.AhAkku(indexes));

StepIndex = [ 1 1+find(abs(diff(QOCV_Messung.diga.daten.Schritt)) > 0) numel(QOCV_Messung.diga.daten.Schritt)+1];
QOCV_Messung.diga.daten.AhAkku(1:(StepIndex(2)-1))=QOCV_Messung.diga.daten.AhStep(1:(StepIndex(2)-1));
for i_step = 2:(numel(StepIndex)-1)
    if strcmp(QOCV_Messung.diga.daten.Zustand(StepIndex(i_step)),'DCH')
        QOCV_Messung.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1)) = ...
            QOCV_Messung.diga.daten.AhAkku(StepIndex(i_step)-1) - QOCV_Messung.diga.daten.AhStep(StepIndex(i_step):(StepIndex(i_step+1)-1));
    else
        QOCV_Messung.diga.daten.AhAkku(StepIndex(i_step):(StepIndex(i_step+1)-1)) = ...
            QOCV_Messung.diga.daten.AhAkku(StepIndex(i_step)-1) + QOCV_Messung.diga.daten.AhStep(StepIndex(i_step):(StepIndex(i_step+1)-1));
    end
end
QOCV_Messung.diga.daten.AhAkku = QOCV_Messung.diga.daten.AhAkku-max(QOCV_Messung.diga.daten.AhAkku);
            
hystlad = struct();
hystlad.index = [7.153e5+147:7.195e5];
hystlad.filterpunkte = 600;

hystlad.t = (master.diga.daten.Programmdauer(hystlad.index)-master.diga.daten.Programmdauer(hystlad.index(1)))/1000;
[hystlad.t,ia,ic] = unique(hystlad.t);
hystlad.dt = [diff(hystlad.t)];
hystlad.Q = (master.diga.daten.AhStep(hystlad.index(ia))-master.diga.daten.AhStep(hystlad.index(ia(1))))*3600;
hystlad.U = master.diga.daten.Spannung(hystlad.index(ia));
hystlad.AhAkku = master.diga.daten.AhAkku(hystlad.index(ia));

hystlad.U_filt=filtfilt(ones(1,hystlad.filterpunkte)/sum(ones(1,hystlad.filterpunkte)),1,hystlad.U);
hystlad.dU=diff(hystlad.U_filt);
hystlad.Q_filt=filtfilt(ones(1,hystlad.filterpunkte)/sum(ones(1,hystlad.filterpunkte)),1,hystlad.Q);
hystlad.dQ = [diff(hystlad.Q_filt)];

hystlad.cap=hystlad.dQ./hystlad.dU;
%q=abs(ahStep(1:end-1));
% qlr=fliplr(q);
% figure; plot(abs(1-qlr*3.35/(3.35*3.2)*100),cap*3600)

figure
plot(hystlad.AhAkku,hystlad.U_filt);
hold all; grid on

figure
plot(hystlad.AhAkku(1:end-1),hystlad.cap,'display',num2str(hystlad.filterpunkte));
hold all; grid on

% figure
% plot(hystlad.t(1:end)/3600,hystlad.Q/40/3600*100);
% hold all; grid on ;

%plot(master.diga.daten.Spannung);



qocv_lad = struct();
qocv_lad.index = [91391:170531];
qocv_lad.filterpunkte = 600;

qocv_lad.t = (QOCV_Messung.diga.daten.Programmdauer(qocv_lad.index)-QOCV_Messung.diga.daten.Programmdauer(qocv_lad.index(1)))/1000;
[qocv_lad.t,ia,ic] = unique(qocv_lad.t);
qocv_lad.dt = [diff(qocv_lad.t)];
qocv_lad.Q = (QOCV_Messung.diga.daten.AhStep(qocv_lad.index(ia))-QOCV_Messung.diga.daten.AhStep(qocv_lad.index(ia(1))))*3600;
qocv_lad.U = QOCV_Messung.diga.daten.Spannung(qocv_lad.index(ia));
% figure
% plot(qocv_lad.t(1:end)/3600,qocv_lad.U);
% hold all; grid on

qocv_lad.U_filt=filtfilt(ones(1,qocv_lad.filterpunkte)/sum(ones(1,qocv_lad.filterpunkte)),1,qocv_lad.U);
qocv_lad.dU=diff(qocv_lad.U_filt);
qocv_lad.Q_filt=filtfilt(ones(1,qocv_lad.filterpunkte)/sum(ones(1,qocv_lad.filterpunkte)),1,qocv_lad.Q);
qocv_lad.AhAkku = QOCV_Messung.diga.daten.AhAkku(qocv_lad.index(ia));

qocv_lad.dQ = [diff(qocv_lad.Q_filt)];

qocv_lad.cap=qocv_lad.dQ./qocv_lad.dU;

figure
plot(qocv_lad.AhAkku(1:end-1),qocv_lad.cap,'display',num2str(hystlad.filterpunkte));
hold all; grid on

figure
plot(qocv_lad.AhAkku,qocv_lad.U_filt);
hold all; grid on

