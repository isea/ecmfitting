function [A] = loescheDoppelteEintraege(x)
% A ist die Matrix, die entsteht wenn alle doppelt 
% vorkommenen Zeilen der Matrix 'x' gel�scht werden

gr=size(x);
A=[];
 
for i=1:gr(1)
    if ~isnan(x(i,:))
        A=[A;x(i,:)];
        for j=i+1:gr(1)
            if x(j,:)==x(i,:)
                x(j,:)=NaN;
            end
        end
    end
end
 

end

