function [] = test_Fit_Voigt_DRT()
close all
% andere Formulierung aus Modern Aspects of Electrochemistry no. 32 S.181/182 
% Implementierung als Kettenleiter
Rser = 0.0026847;
R1 = 0.0016782;
Tau1 = 110.1485;

Sigma1 = R1./sqrt(2.*Tau1);
CLim1 = Tau1./R1;
w = 10.^(9:-.05:-9);
Z_coth =Sigma1 ./ w.^0.5 .* (1-1i) .* coth((2.*1i.*w).^0.5.*CLim1.*Sigma1)...
    -1./(1i.*w.*CLim1);
%Z_coth(w>1e6) = 0;
%Z_coth(w<1e-6) = real(Z_coth(find(w<=1e-6,1,'last')));
Z_app = zeros(size(Z_coth));
figure;
plot(real(Z_coth),imag(Z_coth),'bo')
grid on;hold on; axis square; axis equal; set(gca,'ydir', 'reverse');
n = 100;
C_Ladder = CLim1 / n;
R_Ladder = Sigma1^2 .* 2 .* CLim1 ./ (n-1);
Z_app = Laddernetwork(ones(n-1,numel(w))*R_Ladder , repmat(1./(1i.*w.*C_Ladder),n,1))...
        -1./(1i.*w.*CLim1);
%Z_app(w>1e6) = 0;
%Z_app(w<1e-6) = real(Z_app(find(w<=1e-6,1,'last')));

%ACHTUNG
%Z_app = Z_coth;
plot(real(Z_app),imag(Z_app),'rx')
    title('Warburg Diffusion coth als Kettenleiter (Aspects of Electrochemistry)','Fontname','arial','fontsize',18)
    grid on;hold on; axis square; axis equal; set(gca,'ydir', 'reverse');

    kinder = get(gca,'Children');
    for i = kinder, set(i,'linewidth',2),end
    set(gca,'Fontname','Arial','Fontsize',16)
    xlabel('Re [\Omega]','Fontname','arial','fontsize',18)
    ylabel('Im [\Omega]','Fontname','arial','fontsize',18)


tic
R_Voigt = [];
Tau_Voigt = [];
for i_RC = 1:10
        %  Z_app = 1./(1+1i*(w*10).^1);
    
    ZeroPadding = 20;
    InterpolationsFaktor = 10 ;
    Schwingfaktor = 1;
    FilterFaktor_ext = 0.4;
    FilterFaktor_int = FilterFaktor_ext / InterpolationsFaktor * Schwingfaktor;

    [freq_ext, Z_ext] = extrapolatesignal(flipdim(w'/(2*pi),1),flipdim(reshape(Z_app,numel(Z_app),1),1),[ZeroPadding ZeroPadding]);
    %freq_ext = flipdim(w'/(2*pi),1);
    %Z_ext = flipdim(reshape(Z_app,numel(Z_app),1),1);
    [freq_int,Z_int_imag]=interpolate_signal(InterpolationsFaktor,imag(Z_ext),freq_ext);

    [x_int,DRT_int]=makeDRT(Z_int_imag'*InterpolationsFaktor,freq_int',true,FilterFaktor_int);
    DRT_int = DRT_int/Schwingfaktor;
    g_int = real(DRT_int);
    Peaks = flipdim(peakfinder(g_int,max(g_int)/5),1);
    
    sortPeaks = flipdim(sortrows([Peaks g_int(Peaks)],2),1);
    Peaks = sortPeaks(:,1);
    if numel(Peaks)>3, Peaks=Peaks(1:3);end
     
    
%     [x,DRT_ext]=makeDRT(imag(Z_ext),freq_ext,true,FilterFaktor_ext);
%     g_ext = real(DRT_ext);

%     figure
%     hold off;
%     [AX, H1, H2 ]=plotyy(1./(2*pi*freq_int'),g_int',1./w,imag(Z_app),@semilogx);
%     grid on; hold on
%     %semilogx(1./(2*pi*freq_ext'),g_ext','-k');
%     semilogx(1./(2*pi*freq_int(Peaks)'),g_int(Peaks)','or');
%     xlabel('tau')
%     h1 = legend('EI-DRT','Location','NorthWest');
%     set(get(AX(2),'Ylabel'),'String','Imag(Z)') 
%     set(AX(2),'ydir', 'reverse');
%     set(h1,'Interpreter','none');
%     fprintf('max(Z_app):%8.3e\n',max(real(Z_app)))
    found = 0;
    for i_peaks = 1:numel(Peaks)
        tau_max = 1./(2*pi*freq_int(Peaks(i_peaks)));
        tau_max = tau_max(1);
        
        R_max = fminbnd(@(x) OptimValueRC(x,Z_app,tau_max,w),0,max(-imag(Z_app)),optimset('TolX',1e-7,'Display','off'));
        %fprintf('Tau:%8.3e  R:%8.3e  g:%8.3e\n',tau_max,R_max,g_int(Peaks(i_peaks)))
        Z_app = Z_app-R_max./(1+1i.*w.*tau_max);
        if R_max > 0.01*(sum(R_Voigt)+max(real(Z_app)))
            found = 1;
            if isempty(Tau_Voigt)
                Tau_Voigt = tau_max;
                R_Voigt = R_max;
            else
                diffTau = abs(Tau_Voigt-tau_max);
                index = find(diffTau == min(diffTau),1,'first'); 
                if diffTau(index)/tau_max<0.1
                    R_Voigt(index) = R_Voigt(index) + R_max;
                else
                    Tau_Voigt = [Tau_Voigt tau_max];
                    R_Voigt = [R_Voigt R_max];
                end
            end
        else
            
        end
    end
    if found == 0, 
        break
    end
    %figure
end
toc
num2cell([R_Voigt; Tau_Voigt])'
Z_Voigt = 0;
for k = 1:numel(R_Voigt)
    Z_Voigt = Z_Voigt + R_Voigt(k)./(1+1i.*w.*Tau_Voigt(k));
end

figure;
plot(real(Z_coth),imag(Z_coth),'bo')
grid on;hold on; axis square; axis equal; set(gca,'ydir', 'reverse');

C_Ladder = CLim1 / n;
R_Ladder = Sigma1^2 .* 2 .* CLim1 ./ (n-1);
Z_app = Laddernetwork(ones(n-1,numel(w))*R_Ladder , repmat(1./(1i.*w.*C_Ladder),n,1))...
        -1./(1i.*w.*CLim1);
plot(real(Z_app),imag(Z_app),'rx')
plot(real(Z_Voigt),imag(Z_Voigt),'kx')

    title('Warburg Diffusion coth als Kettenleiter (Aspects of Electrochemistry)','Fontname','arial','fontsize',18)
    grid on;hold on; axis square; axis equal; set(gca,'ydir', 'reverse');

    kinder = get(gca,'Children');
    for i = kinder, set(i,'linewidth',2),end
    set(gca,'Fontname','Arial','Fontsize',16)
    xlabel('Re [\Omega]','Fontname','arial','fontsize',18)
    ylabel('Im [\Omega]','Fontname','arial','fontsize',18)


%  [x,DRT]=makeDRT(imag(Z_ext),freq_ext);
%  tau = 1./(2*pi*freq_ext);

function [TargetValue] = OptimValueRC(R,Z,tau,w)
    DiffValue = imag(Z)-imag(R./(1+1i.*w.*tau));
    TargetValue=sum(DiffValue(DiffValue<0).^2);
    TargetValue=TargetValue+sum(DiffValue(DiffValue>0).^2*10);
    

   
