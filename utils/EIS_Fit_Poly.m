config;
global Modellliste;
pfad = 'output/hwi_Panasonic_3.2Ah_009/0C_DC/';
ModellName = '2Particles';
Modellindex = find(strcmp(Modellliste.Modell(:,1),ModellName),1);


Modelle = {};
f = dir([pfad '*']);
EIS = {};
for i_T = 1:numel(f)
    if f(i_T).isdir && ~strcmp('.',f(i_T).name) && ~strcmp('..',f(i_T).name)
        f2 = dir([pfad f(i_T).name '/*SOC.mat']);
        for i_SOC = 1:numel(f2)
            if ~f2(i_SOC).isdir
                temp = load([pfad f(i_T).name '/' f2(i_SOC).name]);
                
                if isempty(Modelle)
                    Modelle = temp.DRT_GUI.Fit.aktuell_Modell.ModellCell;
                    EIS{end+1}={};
                end
                index = find(strcmp(Modelle(:,1),temp.DRT_GUI.Fit.aktuell_Modell.Modellname),1);    
                if isempty(index)
                    Modelle(end+1,:) = temp.DRT_GUI.Fit.aktuell_Modell.ModellCell;
                    EIS{end+1}={};
                    index = find(strcmp(Modelle(:,1),temp.DRT_GUI.Fit.aktuell_Modell.Modellname),1);    
                end
                
                if temp.DRT_GUI.Fit.gueltig && ...
                        numel(temp.DRT_GUI.Fit.Parameter)==size(Modelle{index,4},2);
                    EIS{index}{end+1}= temp.DRT_GUI;
                end
            end
        end 
    end
end

index = find(strcmp(Modelle(:,1),ModellName),1);
EIS = EIS{index};
Modell = Modelle(index,:);
Modell = cell2struct(Modellliste.Modell(Modellindex,:),{'name','formel','P_Name_string','P_init','P_min','P_max','P_fix'},2);
Modell.P_Name = textscan(strrep(Modell.P_Name_string,' ',''),'%s','delimiter',',');
Modell.P_Name = Modell.P_Name{1}';

SOC_p = [];%zeros(numel(EIS),1);
Temperatur = [];%zeros(numel(EIS),1);
P = [];% zeros(numel(EIS),numel(Modell.P_Name));
for i_EIS = 1:numel(EIS)
    if EIS{i_EIS}.Testparameter.Temperatur==10
        SOC_p(end+1) = EIS{i_EIS}.Testparameter.SOC;
        Temperatur(end+1) = EIS{i_EIS}.Testparameter.Temperatur;
        P(end+1,:) = EIS{i_EIS}.Fit.Parameter;
    end
end
    
Ts = Temperatur([1 1+ find(diff(Temperatur)~=0)]);

for i = 11%1:numel(Modell.P_Name)
    %formula.string='p(1)+p(2).*SOC+p(3).*SOC.*SOC+p(4).*SOC.*SOC.*SOC*0+p(5).*SOC.*SOC.*SOC.*SOC*0';%+p(6).*exp(p(7).*(T-p(8)))';
    formula.string='p(1)+p(2).*SOC+p(4).*exp(p(5).*(SOC-p(6)))+p(7).*exp(p(8).*(SOC-p(9)))+p(10).*exp(p(11).*(T-p(12)))'; 
    formula.Temperatur = Temperatur';
    formula.SOC = SOC_p';
    p_init = ones(1,12)*1e-4;
    p_min = ones(1,12)*-inf;
    p_max = ones(1,12)*inf;
    
    p_init(8) = 1;
    p_init(9) = 40;
    p_min(9) = 10;
    p_max(9) = 60;
    p_init(5) = 1;
    p_init(6) = 70;
    p_min(6) = 50;
    p_max(6) = 95;
    p_init(11) = -1;
    
    options = optimset('MaxIter',20000,'MaxFunEvals',20000,'TolX',1e-8,'TolFun',1e-8);

    
    [p_best,fval,exitflag,output]=function_fit_easyfit2(0,[P(:,i), zeros(size(P(:,i)))],p_init,@function_w_SOC_T, p_min, p_max ,options, formula);
    
    p = p_best;
    SOC = 1:100;
    T = 10;
    SOC_p = SOC_p(find(Temperatur==T));
    P_j = P(find(Temperatur==T),i);
    P_best = eval(formula.string);
    figure
        [SOC_j,  sortindex] = sort(SOC_p');
        
        SOC_j = SOC_j';
        P_j = P_j(sortindex)';
        %Poly = polyfit(SOC_j,P_j,2);
        plot(SOC_j,P_j,'o')
        hold on
         %plot(10:100,pchip(SOC_j,P_j,10:100),'r')
        %plot(10:100,polyval(Poly,10:100),'k');
    plot(SOC,P_best,'m')
       
    p_init = p_best;
end










