function output_txt = Text_with_Frequency(obj,event_obj)
% Display the position of the data cursor
% obj          Currently not used (empty)
% event_obj    Handle to event object
% output_txt   Data cursor text string (string or cell array of strings).
global DRT_GUI
pos = get(event_obj,'Position');
messindex = find(DRT_GUI.Messdaten.Zreal==pos(1) & DRT_GUI.Messdaten.Zimg==pos(2));
if isempty(messindex)
    fitindex = find(DRT_GUI.lowFreqFit.Zreal==pos(1) & DRT_GUI.lowFreqFit.Zimg==pos(2));
output_txt = {['Freq   : ',num2str(DRT_GUI.lowFreqFit.frequenz(fitindex),4)],...
    ['Real(Z): ',num2str(pos(1),4)],...
    ['Imag(Z): ',num2str(pos(2),4)]};
else
    
output_txt = {['Freq   : ',num2str(DRT_GUI.Messdaten.frequenz(find(DRT_GUI.Messdaten.Zreal==pos(1) & DRT_GUI.Messdaten.Zimg==pos(2))),4)],...
    ['Real(Z): ',num2str(pos(1),4)],...
    ['Imag(Z): ',num2str(pos(2),4)],...
    ['Index  : ',num2str(find(DRT_GUI.Messdaten.Zreal==pos(1) & DRT_GUI.Messdaten.Zimg==pos(2)))]};
end
% If there is a Z-coordinate in the position, display it as well
if length(pos) > 2
    output_txt{end+1} = ['Z: ',num2str(pos(3),4)];
end
