function [counter_fuer_zahl] = find_haeufung(zahl,array,proz_fehler)
%Funktion soll Anzahl der Vorkommen einer Zahl,die mit einem prozentualen
%Fehler behaftet ist, in einem Array zur�ckgeben

%Legt die Schranken fest in denen die Zahlen liegen d�rfen
linke_schranke = zahl - zahl*proz_fehler;
rechte_schranke = zahl + zahl*proz_fehler;


%Pr�fe auf Zahl im Array
counter_fuer_zahl = 0;
for k = 1:length(array)
    if (array(k,1)) >= linke_schranke && array(k,1) <= rechte_schranke
        counter_fuer_zahl = counter_fuer_zahl + array(k,2);
    end
end
