close all
frequenz = [ power(10, -16:0.2:17) ]';
% frequenz = [0 power(10,-0.5:0.1:5) inf]';
RC1 = makezarc (1.6, 1/(power(10,4)), 1, frequenz);
RC2 = makezarc (0.8,  1/ (3 * power(10,3)), 1, frequenz);
RC3 = makezarc (1.6, 1/ power(10,3), 1, frequenz);

Q1 = makezarc( 1.2, 1/ 25,0.85, frequenz);
Q2 = makezarc( 0.8, 1/ 7,0.95, frequenz);
Q3 = makezarc( 2, 1,0.7, frequenz);


Z =RC1+RC2+RC3+Q1 + Q2 + Q3;

InterpolationsFaktor = 7;
[freq_ext, Z_ext] = extrapolatesignal(frequenz,(Z),[5 5]);

[freq_int,Z_int_imag]=interpolate_signal(InterpolationsFaktor,imag(Z_ext),freq_ext);
figure;semilogx(frequenz,imag(Z),'-x');  grid on ;hold on;
semilogx(freq_int,Z_int_imag,'r-');
semilogx(freq_ext,imag(Z_ext),'k-o')

[x_int,DRT_int]=makeDRT(Z_int_imag'*InterpolationsFaktor,freq_int',true,0.3);
figure;
semilogx(freq_int,real(DRT_int),'-r');
grid on; hold on


frequenz = [ power(10, -16:0.05:17) ]';
% frequenz = [0 power(10,-0.5:0.1:5) inf]';
RC1 = makezarc (1.6, 1/(power(10,4)), 1, frequenz);
RC2 = makezarc (0.8,  1/ (3 * power(10,3)), 1, frequenz);
RC3 = makezarc (1.6, 1/ power(10,3), 1, frequenz);

Q1 = makezarc( 1.2, 1/ 25,0.85, frequenz);
Q2 = makezarc( 0.8, 1/ 7,0.95, frequenz);
Q3 = makezarc( 2, 1,0.7, frequenz);


Z =RC1+RC2+RC3+Q1 + Q2 + Q3;

[freq_ext, Z_ext] = extrapolatesignal(frequenz,(Z),[5 5]);

[x,DRT]=makeDRT(imag(Z_ext),freq_ext,true,0.5);
semilogx(freq_ext,real(DRT));
