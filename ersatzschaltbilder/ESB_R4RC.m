function [ ModellZeile ] = ESB_R4RC(Implementierung)

%% Funktion zur Erstellung von Ersatzschaltbildern. 
% Bitte zuerst die schnellen Elemente, dann die Langsamen eintragen

Name = 'R4RC';

ModellZeile={Name,'','',{},{},{},{},{}};

%Rser 
Modell = Implementierung.Rser;
Modell.Name = 'Rser';
Appendix = 'ser';
HFMFLF = 'HF' ; % entweder 'Auto' oder 'HF' oder 'MF' oder 'LF'
Modell.Plot = 0;
Startwerte = {  1e-3    };
Minimum = {     1e-7    };
Maximum = {     100     };
Fix = {         0       };
ModellZeile = AddESBElement(ModellZeile,Modell,Appendix,HFMFLF,Startwerte,Minimum,Maximum,Fix);





% Elektrische Anbindung oder Deckschicht Anode
% Modell = Implementierung.RC_Tau;
% Modell.Name = 'RC_';
% Appendix = '_fast';
% HFMFLF = 'HF' ; % entweder 'Auto' oder 'HF' oder 'MF' oder 'LF'
% Modell.Plot = 1;
% %               R       Tau  
% Startwerte = {  0    1e-4  };
% Minimum =    {  1e-7    1e-7   };
% Maximum =    {  100     9e-4 };
% Fix =        {  1      1   };
% ModellZeile = AddESBElement(ModellZeile,Modell,Appendix,HFMFLF,Startwerte,Minimum,Maximum,Fix);
% 



% RC1 
Modell = Implementierung.RC_Tau;
Modell.Name = 'RC1';
Modell.NewParName= { 'R1' , 'Tau1'};  
HFMFLF = 'MF' ; % entweder 'Auto' oder 'HF' oder 'MF' oder 'LF'
Modell.Plot = 1;
%               R       Tau     
Startwerte = {  1e-3    1e-3   };
Minimum = {     1e-7    1e-5    };
Maximum = {     100     1e-2   };
Fix = {         0       0     };
ModellZeile = AddESBElement(ModellZeile,Modell,Appendix,HFMFLF,Startwerte,Minimum,Maximum,Fix);

% RC2
Modell = Implementierung.RC_Tau;
Modell.Name = 'RC2';
Modell.NewParName= { 'R2' , 'Tau2'};  
HFMFLF = 'MF' ; % entweder 'Auto' oder 'HF' oder 'MF' oder 'LF'
Modell.Plot = 1;
%               R       Tau     
Startwerte = {  1e-3    100   };
Minimum = {     1e-7    1    };
Maximum = {     100     1e4   };
Fix = {         0       0     };
ModellZeile = AddESBElement(ModellZeile,Modell,Appendix,HFMFLF,Startwerte,Minimum,Maximum,Fix);
% RC3
Modell = Implementierung.RC_Tau;
Modell.Name = 'RC3';
Modell.NewParName= { 'R3' , 'Tau3'};  
HFMFLF = 'LF' ; % entweder 'Auto' oder 'HF' oder 'MF' oder 'LF'
Modell.Plot = 1;
%               R       Tau     
Startwerte = {  1e-3    100   };
Minimum = {     1e-7    1    };
Maximum = {     100     1e4   };
Fix = {         0       0     };
ModellZeile = AddESBElement(ModellZeile,Modell,Appendix,HFMFLF,Startwerte,Minimum,Maximum,Fix);

% RC4
Modell = Implementierung.RC_Tau;
Modell.Name = 'RC4';
Modell.NewParName= { 'R4' , 'Tau4'};  
HFMFLF = 'LF' ; % entweder 'Auto' oder 'HF' oder 'MF' oder 'LF'
Modell.Plot = 1;
%               R       Tau     
Startwerte = {  1e-3    1000   };
Minimum = {     1e-7    1    };
Maximum = {     100     1e4   };
Fix = {         0       0     };
ModellZeile = AddESBElement(ModellZeile,Modell,Appendix,HFMFLF,Startwerte,Minimum,Maximum,Fix);
end