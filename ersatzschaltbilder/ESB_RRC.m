function [ ModellZeile ] = ESB_RRC(Implementierung)

%% Funktion zur Erstellung von Ersatzschaltbildern. 
% Bitte zuerst die schnellen Elemente, dann die Langsamen eintragen

Name = 'RRC';

ModellZeile={Name,'','',{},{},{},{},{}};

%Rser 
Modell = Implementierung.Rser;
Modell.Name = 'Rser';
Appendix = 'ser';
HFMFLF = 'HF' ; % entweder 'Auto' oder 'HF' oder 'MF' oder 'LF'
Modell.Plot = 0;
Startwerte = {  1e-3    };
Minimum = {     1e-7    };
Maximum = {     100     };
Fix = {         0       };
ModellZeile = AddESBElement(ModellZeile,Modell,Appendix,HFMFLF,Startwerte,Minimum,Maximum,Fix);





% Elektrische Anbindung oder Deckschicht Anode
% Modell = Implementierung.RC_Tau;
% Modell.Name = 'RC_';
% Appendix = '_fast';
% HFMFLF = 'HF' ; % entweder 'Auto' oder 'HF' oder 'MF' oder 'LF'
% Modell.Plot = 1;
% %               R       Tau  
% Startwerte = {  0    1e-4  };
% Minimum =    {  1e-7    1e-7   };
% Maximum =    {  100     9e-4 };
% Fix =        {  1      1   };
% ModellZeile = AddESBElement(ModellZeile,Modell,Appendix,HFMFLF,Startwerte,Minimum,Maximum,Fix);
% 



% RC1 
Modell = Implementierung.RC_Tau;
Modell.Name = 'RC1';
Modell.NewParName= { 'R1' , 'Tau1'};  
HFMFLF = 'Auto' ; % entweder 'Auto' oder 'HF' oder 'MF' oder 'LF'
Modell.Plot = 1;
%               R       Tau     
Startwerte = {  1e-3    1e-3   };
Minimum = {     1e-7    1e-5    };
Maximum = {     100     1e-2   };
Fix = {         0       0     };
ModellZeile = AddESBElement(ModellZeile,Modell,Appendix,HFMFLF,Startwerte,Minimum,Maximum,Fix);
% 
% % RC2
% Modell = Implementierung.RC_Tau;
% Modell.Name = 'RC2';
% Modell.NewParName= { 'R2' , 'Tau2'};  
% HFMFLF = 'Auto' ; % entweder 'Auto' oder 'HF' oder 'MF' oder 'LF'
% Modell.Plot = 1;
% %               R       Tau     
% Startwerte = {  1e-3    100   };
% Minimum = {     1e-7    1    };
% Maximum = {     100     1e4   };
% Fix = {         0       0     };
% ModellZeile = AddESBElement(ModellZeile,Modell,Appendix,HFMFLF,Startwerte,Minimum,Maximum,Fix);
% 
% % Cser
% Modell = Implementierung.Cser;
% Modell.Name = 'Cser';
% Modell.NewParName= { 'Cser'};  
% HFMFLF = 'Auto' ; % entweder 'Auto' oder 'HF' oder 'MF' oder 'LF'
% Modell.Plot = 1;
% %               Cser            
% Startwerte = {  1e-7       };
% Minimum = {     1e-17      };
% Maximum = {     1e25       };
% Fix = {         0          };
% ModellZeile = AddESBElement(ModellZeile,Modell,Appendix,HFMFLF,Startwerte,Minimum,Maximum,Fix);
% 
% 

end

